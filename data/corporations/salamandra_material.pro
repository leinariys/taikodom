<root xmlns="http://scene.gametoolkit.hoplon.com/scene"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://scene.gametoolkit.hoplon.com/scene ../scene.xsd">
	<includes>
		<include file="data/gfx/flat_normal_no_alpha.dds"/>
		<include file="data/shaders/light_shader.pro"/>
		<include file="data/corporations/salamandra_decal.dds"/>
	</includes>
	<objects>
		<FloatParameter name="fresnelExp_material_salamandra"
			parameterName="fresnelExp"
			value="1"/>
		<TiledTexture name="salamandra_clamped"
			texture="data/corporations/salamandra_decal.dds"
			wrap="CLAMP_TO_BORDER"/>
		<Material name="salamandra_material"
			diffuseTexture="salamandra_clamped"
			normalTexture="data/gfx/flat_normal_no_alpha.dds"
			shader="cubemap_light_alpha_blend_shader">
			<shaderParameter index="0" name="fresnelExp_material_salamandra"/>
		</Material>
	</objects>
</root>
