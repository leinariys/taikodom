<root xmlns="http://scene.gametoolkit.hoplon.com/scene"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://scene.gametoolkit.hoplon.com/scene ../scene.xsd">
	<includes>
		<include file="data/gfx/flat_normal_no_alpha.dds"/>
		<include file="data/shaders/light_shader.pro"/>
		<include file="data/corporations/restritivistas_decal.dds"/>
	</includes>
	<objects>
		<FloatParameter name="fresnelExp_material_restritivistas"
			parameterName="fresnelExp"
			value="1"/>
		<TiledTexture name="restritivistas_clamped"
			texture="data/corporations/restritivistas_decal.dds"
			wrap="CLAMP_TO_BORDER"/>
		<Material name="restritivistas_material"
			diffuseTexture="restritivistas_clamped"
			normalTexture="data/gfx/flat_normal_no_alpha.dds"
			shader="cubemap_light_alpha_blend_shader">
			<shaderParameter index="0" name="fresnelExp_material_restritivistas"/>
		</Material>
	</objects>
</root>
