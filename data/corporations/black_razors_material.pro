<root xmlns="http://scene.gametoolkit.hoplon.com/scene"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://scene.gametoolkit.hoplon.com/scene ../scene.xsd">
	<includes>
		<include file="data/gfx/flat_normal_no_alpha.dds"/>
		<include file="data/shaders/light_shader.pro"/>
		<include file="data/corporations/black_razors_decal.dds"/>
	</includes>
	<objects>
		<FloatParameter name="fresnelExp_material_black_razors"
			parameterName="fresnelExp"
			value="1"/>
		<TiledTexture name="black_razors_clamped"
			texture="data/corporations/black_razors_decal.dds"
			wrap="CLAMP_TO_BORDER"/>
		<Material name="black_razors_material"
			diffuseTexture="black_razors_clamped"
			normalTexture="data/gfx/flat_normal_no_alpha.dds"
			shader="cubemap_light_alpha_blend_shader">
			<shaderParameter index="0" name="fresnelExp_material_black_razors"/>
		</Material>
	</objects>
</root>
