//	tex0 		- diffuse texture 
//	tex1		- normal texture with gloss in alpha	 
 
uniform sampler2D tex0; //diffuse,alpha 
uniform sampler2D tex1; //normal,gloss  
//uniform sampler2D tex2; //detail
//uniform sampler2D tex3; //detail mask
 
uniform float	lightInverseSquaredRadius; 
uniform vec3	lightDiffuseColor; 
uniform vec3	lightSpecularColor; 
uniform float	shininess; 
uniform vec4	materialSpecular;	
 
varying	vec3	g_lightVec; 
varying	vec3	g_viewVec; 
 
void main() 
{			 
	vec3 viewVec = normalize(g_viewVec); 
	vec3 lightVec = normalize(g_lightVec); 
	 
	vec4 normal_map = texture2D(tex1, gl_TexCoord[0].xy); 
	//float detail_map = texture2D(tex2, gl_TexCoord[1].xy).r; 
	//float mask = texture2D(tex3, gl_TexCoord[0].xy).r;
	vec3 color_base = texture2D(tex0,gl_TexCoord[0].xy).rgb;// * (detail_map*mask + 1.0 - mask);
 
	vec3 bump; 
 
	// transform to [-1,1] range	 
	bump = normal_map.xyz*2.0-1.0; 		 
	 
	//avoid mipmap artifacts 
	bump = normalize(bump);		 	 
		 
	//diffuse is getNull dot between bump (our normal) and the light vector
	float diffuse = max(dot(lightVec, bump), 0.0); 
	float rim = 1.0 - max(dot(-viewVec,bump),0.0);
	rim*=rim;
	rim*=rim;
	
	//our specular is based on Phong, the reflected view vector model. 
	float specular = pow(max(dot(reflect(viewVec, bump), lightVec), 0.0), shininess); 
	 
	//final color = (L_dif_color*mat_color*diffuse + L_spec_color*gloss*specular)*attenuation; 
	gl_FragColor = 	vec4(	((lightDiffuseColor*color_base.rgb )*diffuse  +  materialSpecular.rgb*rim +
				lightSpecularColor*materialSpecular.rgb*(normal_map.getNull*specular)),
				1.0);    	
}
