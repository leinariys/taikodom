<root xmlns="http://scene.gametoolkit.hoplon.com/scene"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://scene.gametoolkit.hoplon.com/scene ../scene.xsd">
    <includes>
        <include file="data/shaders/cristal_shader.frag"/>
        <include file="data/shaders/cristal_shader.vert"/>
        <include file="data/shaders/cubemap_light.frag"/>
        <include file="data/shaders/cubemap_light.vert"/>
        <include file="data/shaders/cubemap_light_detail.frag"/>
        <include file="data/shaders/cubemap_light_detail.vert"/>
        <include file="data/shaders/cubemap_light_simple.vert"/>
        <include file="data/shaders/cubemap_main_light.frag"/>
        <include file="data/shaders/cubemap_main_light_lf_specular.frag"/>
        <include file="data/shaders/directional_light.frag"/>
        <include file="data/shaders/directional_light.vert"/>
        <include file="data/shaders/directional_light_detail.frag"/>
        <include file="data/shaders/directional_light_detail.vert"/>
        <include file="data/shaders/directional_light_planet.frag"/>
        <include file="data/shaders/point_light.frag"/>
        <include file="data/shaders/point_light.vert"/>
        <include file="data/shaders/point_light_detail.frag"/>
        <include file="data/shaders/point_light_detail.vert"/>
        <include file="data/shaders/spot_light.frag"/>
        <include file="data/shaders/spot_light.vert"/>
        <include file="data/shaders/spot_light_detail.frag"/>
        <include file="data/shaders/spot_light_detail.vert"/>
    </includes>
    <objects>
    	
    	<Shader name="TranspS2">
            <pass index="0"
                name="TranspS2_Pass"/>
      </Shader>
      <ShaderPass name="TranspS2_Pass"
            alphaTestEnabled="1"
            blendEnabled="1"	    
            dstBlend="ONE_MINUS_SRC_COLOR">
            <channelTextureSet envMode="MODULATE"
                glChannel="0"
                texChannel="8"
                texCoordSet="0"
                />
      </ShaderPass>
    	<Shader name="TranspS">
            <pass index="0"
                name="TranspS_Pass"/>
      </Shader>
      <ShaderPass name="TranspS_Pass"
            alphaTestEnabled="1"
            blendEnabled="1"	    
            dstBlend="ONE_MINUS_SRC_ALPHA">
            <channelTextureSet envMode="MODULATE"
                glChannel="0"
                texChannel="8"
                texCoordSet="0"
                />
      </ShaderPass>
    	
    	<ShaderPass name="ShaderPass_volLight"
    		blendEnabled="1"
            cullFaceEnabled="0"
            depthMask="0"
            fogEnabled="1"
            srcBlend="SRC_ALPHA"
            dstBlend="ONE_MINUS_SRC_ALPHA">
            <channelTextureSet envMode="MODULATE"
                glChannel="0"
                texChannel="6"
                texCoordSet="0"/>
            <channelTextureSet envMode="ADD"
                glChannel="1"
                texChannel="8"
                texCoordSet="0"/>
        </ShaderPass>
        <Shader name="Shader_volLight"
            fallback="default_light_shader_simple">
            <pass index="0"
                name="ShaderPass_volLight"/>
            <pass index="1"
                name="point_light_pass"/>
            <pass index="2"
                name="directional_light_pass"/>
            <pass index="3"
                name="spot_light_pass"/>
        </Shader>
    
    	<ShaderPass name="ShaderPass_transDif2MIX"
    		cullFaceEnabled="0"
            blendEnabled="1"
            fogEnabled="1"
            srcBlend="SRC_COLOR"
            dstBlend="ONE_MINUS_SRC_ALPHA"
            useSceneAmbientColor="1">
            <channelTextureSet envMode="MODULATE"
                glChannel="0"
                texChannel="8"
                texCoordSet="0"/>
            <channelTextureSet envMode="MODULATE"
                glChannel="1"
                texChannel="6"
                texCoordSet="0"/>
        </ShaderPass>
        <Shader name="Shader_transDif2MIX"
            fallback="default_light_shader_simple">
            <pass index="0"
                name="ShaderPass_transDif2MIX"/>
            <pass index="1"
                name="point_light_pass"/>
            <pass index="2"
                name="directional_light_pass"/>
            <pass index="3"
                name="spot_light_pass"/>
        </Shader>
        
        <ShaderPass name="ShaderPass_transDif3MIX"
            blendEnabled="1"
            fogEnabled="1"
            srcBlend="SRC_ALPHA"
            dstBlend="DST_ALPHA"
            useSceneAmbientColor="1">
            <channelTextureSet envMode="ADD"
                glChannel="0"
                texChannel="8"
                texCoordSet="0"/>
            <channelTextureSet envMode="ADD"
                glChannel="1"
                texChannel="6"
                texCoordSet="0"/>
            <channelTextureSet envMode="ADD"
                glChannel="2"
                texChannel="4"
                texCoordSet="0"/>
        </ShaderPass>
        <Shader name="Shader_transDif3MIX"
            fallback="default_light_shader_simple">
            <pass index="0"
                name="ShaderPass_transDif3MIX"/>
            <pass index="1"
                name="point_light_pass"/>
    
            <pass index="2"
                name="spot_light_pass"/>
        </Shader>
        
        <ShaderPass name="ShaderPass_VIDRO"
            blendEnabled="1"
            fogEnabled="1"
            srcBlend="ONE_MINUS_SRC_ALPHA"
            useSceneAmbientColor="1">
            <channelTextureSet envMode="MODULATE"
                glChannel="0"
                texChannel="8"
                texCoordSet="0"/>
        </ShaderPass>
        <ShaderPass name="ShaderPass_VIDRO_double_face"
            blendEnabled="1"
            cullFaceEnabled="0"
            depthMask="0"
            fogEnabled="1"
            srcBlend="SRC_ALPHA">
            <channelTextureSet envMode="MODULATE"
                glChannel="0"
                texChannel="8"
                texCoordSet="0"/>
        </ShaderPass>
        <Shader name="Shader_VIDRO"
            fallback="default_light_shader_simple">
            <pass index="0"
                name="ShaderPass_VIDRO"/>
            <pass index="1"
                name="point_light_pass"/>
            <pass index="2"
                name="directional_light_pass"/>
            <pass index="3"
                name="spot_light_pass"/>
        </Shader>
        <Shader name="Shader_VIDRO_double_face"
            fallback="default_light_shader_simple">
            <pass index="0"
                name="ShaderPass_VIDRO_double_face"/>
        </Shader>             
        <Shader name="cristal_shader"
                quality="1"
                fallback="default_light_shader_simple">
            <pass index="0"
                name="cristal_shader_pass"/>
        </Shader>
        <ShaderPass name="cristal_shader_pass"
            program="cristal_shader_program">
            <channelTextureSet envMode="MODULATE"
                glChannel="0"
                texChannel="0"
                texCoordSet="0"/>
            <channelTextureSet envMode="MODULATE"
                glChannel="1"
                texChannel="8"
                texCoordSet="0"/>
            <channelTextureSet envMode="MODULATE"
                glChannel="2"
                texChannel="12"
                texCoordSet="0"/>
        </ShaderPass>
        <ShaderProgram name="cristal_shader_program">
            <shaderProgramObject index="0"
                name="data/shaders/cristal_shader.frag"/>
            <shaderProgramObject index="1"
                name="data/shaders/cristal_shader.vert"/>
        </ShaderProgram>
        <Shader name="cubemap_light_alpha_blend_shader"
            fallback="default_light_shader"
            quality="0">
            <pass index="0"
                name="cubemap_light_shader_alpha_blend_pass"/>
            <pass index="1"
                name="directional_light_pass"/>
            <pass index="2"
                name="point_light_pass"/>
        </Shader>
        <Shader name="cubemap_light_alpha_shader"
            fallback="default_light_shader_simple"
            quality="0">
            <pass index="0"
                name="cubemap_light_shader_alpha_pass"/>
            <pass index="1"
                name="point_light_pass"/>
            <pass index="2"
                name="directional_light_pass"/>
            <pass index="3"
                name="spot_light_pass"/>
        </Shader>
        <Shader name="cubemap_light_alpha_shader_double_face"
            fallback="default_light_shader_simple_dual"
            quality="0">
            <pass index="0"
                name="cubemap_light_shader_alpha_pass_double_face"/>
            <pass index="1"
                name="point_light_pass"/>
            <pass index="2"
                name="directional_light_pass_double_face"/>
            <pass index="3"
                name="spot_light_pass"/>
        </Shader>
        <Shader name="cubemap_light_shader"
            fallback="default_light_shader"
            quality="0">
            <pass index="0"
                name="cubemap_light_shader_pass"/>
            <pass index="1"
                name="point_light_pass"/>
            <pass index="2"
                name="directional_light_pass"/>
            <pass index="3"
                name="spot_light_pass"/>
        </Shader>
        <ShaderPass name="cubemap_light_shader_alpha_blend_pass"
            alphaRef="0.5"
            alphaTestEnabled="1"
            blendEnabled="1"
            depthMask="0"
            dstBlend="ONE_MINUS_SRC_ALPHA"
            program="cubemap_light_shader_program">
            <channelTextureSet envMode="MODULATE"
                glChannel="0"
                texChannel="0"
                texCoordSet="0"/>
            <channelTextureSet envMode="ADD"
                glChannel="1"
                texChannel="8"
                texCoordSet="0"/>
            <channelTextureSet envMode="MODULATE"
                glChannel="2"
                texChannel="1"
                texCoordSet="0"/>
            <channelTextureSet envMode="ADD"
                glChannel="3"
                texChannel="10"
                texCoordSet="0"/>
            <channelTextureSet envMode="ADD"
                glChannel="4"
                texChannel="12"
                texCoordSet="0"/>
        </ShaderPass>
        <ShaderPass name="cubemap_light_shader_alpha_pass"
            alphaTestEnabled="1"
            program="cubemap_light_shader_program">
            <channelTextureSet envMode="MODULATE"
                glChannel="0"
                texChannel="0"
                texCoordSet="0"/>
            <channelTextureSet envMode="ADD"
                glChannel="1"
                texChannel="8"
                texCoordSet="0"/>
            <channelTextureSet envMode="MODULATE"
                glChannel="2"
                texChannel="1"
                texCoordSet="0"/>
            <channelTextureSet envMode="ADD"
                glChannel="3"
                texChannel="10"
                texCoordSet="0"/>
            <channelTextureSet envMode="ADD"
                glChannel="4"
                texChannel="12"
                texCoordSet="0"/>
        </ShaderPass>
        <ShaderPass name="cubemap_light_shader_alpha_pass_double_face"
            alphaTestEnabled="1"
            cullFaceEnabled="0"
            program="cubemap_light_shader_program">
            <channelTextureSet envMode="MODULATE"
                glChannel="0"
                texChannel="0"
                texCoordSet="0"/>
            <channelTextureSet envMode="ADD"
                glChannel="1"
                texChannel="8"
                texCoordSet="0"/>
            <channelTextureSet envMode="MODULATE"
                glChannel="2"
                texChannel="1"
                texCoordSet="0"/>
            <channelTextureSet envMode="ADD"
                glChannel="3"
                texChannel="10"
                texCoordSet="0"/>
            <channelTextureSet envMode="ADD"
                glChannel="4"
                texChannel="12"
                texCoordSet="0"/>
        </ShaderPass>
        <Shader name="cubemap_light_shader_detail"
            fallback="default_light_shader_simple"
            quality="0">
            <pass index="0"
                name="cubemap_light_shader_pass_detail"/>
            <pass index="1"
                name="point_light_pass_detail"/>
            <pass index="2"
                name="directional_light_pass_detail"/>
            <pass index="3"
                name="spot_light_pass_detail"/>
        </Shader>
        <ShaderPass name="cubemap_light_shader_pass"
            program="cubemap_light_shader_program">
            <channelTextureSet envMode="MODULATE"
                glChannel="0"
                texChannel="0"
                texCoordSet="0"/>
            <channelTextureSet envMode="ADD"
                glChannel="1"
                texChannel="8"
                texCoordSet="0"/>
            <channelTextureSet envMode="MODULATE"
                glChannel="2"
                texChannel="1"
                texCoordSet="0"/>
            <channelTextureSet envMode="ADD"
                glChannel="3"
                texChannel="10"
                texCoordSet="0"/>
            <channelTextureSet envMode="ADD"
                glChannel="4"
                texChannel="12"
                texCoordSet="0"/>
        </ShaderPass>
        <ShaderPass name="cubemap_light_shader_pass_detail"
            program="cubemap_light_shader_program_detail">
            <channelTextureSet envMode="MODULATE"
                glChannel="0"
                texChannel="0"
                texCoordSet="0"/>
            <channelTextureSet envMode="MODULATE"
                glChannel="1"
                texChannel="8"
                texCoordSet="0"/>
            <channelTextureSet envMode="MODULATE"
                glChannel="2"
                texChannel="1"
                texCoordSet="0"/>
            <channelTextureSet envMode="MODULATE"
                glChannel="3"
                texChannel="10"
                texCoordSet="0"/>
            <channelTextureSet envMode="MODULATE"
                glChannel="4"
                texChannel="12"
                texCoordSet="0"/>
            <channelTextureSet envMode="MODULATE"
                glChannel="5"
                texChannel="9"
                texCoordSet="0"/>
        </ShaderPass>
        <ShaderPass name="cubemap_light_shader_pass_simple"
            program="cubemap_light_shader_program_simple"
            useDiffuseCubemap="1"
            useReflectCubemap="1">
            <channelTextureSet envMode="MODULATE"
                glChannel="0"
                texChannel="0"
                texCoordSet="0"/>
            <channelTextureSet envMode="ADD"
                glChannel="1"
                texChannel="8"
                texCoordSet="0"/>
            <channelTextureSet envMode="MODULATE"
                glChannel="2"
                texChannel="1"
                texCoordSet="0"/>
            <channelTextureSet envMode="ADD"
                glChannel="3"
                texChannel="10"
                texCoordSet="0"/>
        </ShaderPass>
        <ShaderProgram name="cubemap_light_shader_program">
            <shaderProgramObject index="0"
                name="data/shaders/cubemap_light.vert"/>
            <shaderProgramObject index="1"
                name="data/shaders/cubemap_light.frag"/>
        </ShaderProgram>
        <ShaderProgram name="cubemap_light_shader_program_detail">
            <shaderProgramObject index="0"
                name="data/shaders/cubemap_light_detail.vert"/>
            <shaderProgramObject index="1"
                name="data/shaders/cubemap_light_detail.frag"/>
        </ShaderProgram>
        <ShaderProgram name="cubemap_light_shader_program_simple">
            <shaderProgramObject index="0"
                name="data/shaders/cubemap_light_simple.vert"/>
        </ShaderProgram>
        <Shader name="cubemap_light_shader_simple"
            fallback="default_light_shader_simple"
            quality="2">
            <pass index="0"
                name="cubemap_light_shader_pass_simple"/>
            <pass index="1"
                name="vertex_light_shader_pass"/>
        </Shader>
        <ShaderPass name="cubemap_main_light_lf_specular_pass"
            program="cubemap_main_light_shader_lf_specular_program">
            <channelTextureSet envMode="MODULATE"
                glChannel="0"
                texChannel="0"
                texCoordSet="0"/>
            <channelTextureSet envMode="ADD"
                glChannel="1"
                texChannel="8"
                texCoordSet="0"/>
            <channelTextureSet envMode="MODULATE"
                glChannel="2"
                texChannel="1"
                texCoordSet="0"/>
            <channelTextureSet envMode="ADD"
                glChannel="3"
                texChannel="10"
                texCoordSet="0"/>
            <channelTextureSet envMode="ADD"
                glChannel="4"
                texChannel="12"
                texCoordSet="0"/>
        </ShaderPass>
        <Shader name="cubemap_main_light_shader"
            fallback="cubemap_light_shader"
            quality="0">
            <pass index="0"
                name="cubemap_main_light_shader_pass"/>
            <pass index="1"
                name="point_light_pass"/>
            <pass index="2"
                name="spot_light_pass"/>
        </Shader>
        <Shader name="cubemap_main_light_shader_lf_specular"        	
            fallback="cubemap_main_light_shader"
            quality="0">
            <pass index="0"
                name="cubemap_main_light_lf_specular_pass"/>
            <pass index="1"
                name="point_light_pass"/>
        </Shader>
        <ShaderProgram name="cubemap_main_light_shader_lf_specular_program">
            <shaderProgramObject index="0"
                name="data/shaders/cubemap_main_light_lf_specular.frag"/>
            <shaderProgramObject index="1"
                name="data/shaders/cubemap_light.vert"/>
        </ShaderProgram>
        <ShaderPass name="cubemap_main_light_shader_pass"
            program="cubemap_main_light_shader_program">
            <channelTextureSet envMode="MODULATE"
                glChannel="0"
                texChannel="0"
                texCoordSet="0"/>
            <channelTextureSet envMode="ADD"
                glChannel="1"
                texChannel="8"
                texCoordSet="0"/>
            <channelTextureSet envMode="MODULATE"
                glChannel="2"
                texChannel="1"
                texCoordSet="0"/>
            <channelTextureSet envMode="ADD"
                glChannel="3"
                texChannel="10"
                texCoordSet="0"/>
            <channelTextureSet envMode="ADD"
                glChannel="4"
                texChannel="12"
                texCoordSet="0"/>
        </ShaderPass>
        <ShaderProgram name="cubemap_main_light_shader_program">
            <shaderProgramObject index="0"
                name="data/shaders/cubemap_light.vert"/>
            <shaderProgramObject index="1"
                name="data/shaders/cubemap_main_light.frag"/>
        </ShaderProgram>
        <Shader name="default_light_shader"
            fallback="default_light_shader_simple"
            quality="1">
            <pass index="0"
                name="first_pass"/>
            <pass index="1"
                name="point_light_pass"/>
            <pass index="2"
                name="directional_light_pass"/>
            <pass index="3"
                name="spot_light_pass"/>
        </Shader>
        <Shader name="default_light_shader_detail"
            fallback="default_light_shader_simple"
            quality="0">
            <pass index="0"
                name="first_pass_detail"/>
            <pass index="1"
                name="point_light_pass_detail"/>
            <pass index="2"
                name="directional_light_pass_detail"/>
            <pass index="3"
                name="spot_light_pass_detail"/>
        </Shader>
        <Shader name="default_light_shader_simple">
            <pass index="0"
                name="vertex_light_shader_pass"/>
        </Shader>
      <Shader name="default_light_shader_simple_dual">
        <pass index="0"
            name="vertex_light_shader_pass_dual"/>
      </Shader>
        <ShaderPass name="directional_light_pass"
            blendEnabled="1"
            depthMask="0"
            normalArrayEnabled="1"
            program="directional_light_program"
            useLights="DIRECTIONAL_LIGHT">
            <channelTextureSet envMode="MODULATE"
                glChannel="0"
                texChannel="8"
                texCoordSet="0"/>
            <channelTextureSet envMode="MODULATE"
                glChannel="1"
                texChannel="12"
                texCoordSet="0"/>
        </ShaderPass>
        <ShaderPass name="directional_light_pass_detail"
            blendEnabled="1"
            depthMask="0"
            normalArrayEnabled="1"
            program="directional_light_program_detail"
            useLights="DIRECTIONAL_LIGHT">
            <channelTextureSet envMode="MODULATE"
                glChannel="0"
                texChannel="8"
                texCoordSet="0"/>
            <channelTextureSet envMode="MODULATE"
                glChannel="1"
                texChannel="12"
                texCoordSet="0"/>
            <channelTextureSet envMode="MODULATE"
                glChannel="2"
                texChannel="9"
                texCoordSet="0"/>
        </ShaderPass>
        <ShaderPass name="directional_light_pass_double_face"
            alphaTestEnabled="1"
            blendEnabled="1"
            cullFaceEnabled="0"
            depthMask="0"
            normalArrayEnabled="1"
            program="directional_light_program"
            useLights="DIRECTIONAL_LIGHT">
            <channelTextureSet envMode="MODULATE"
                glChannel="0"
                texChannel="8"
                texCoordSet="0"/>
            <channelTextureSet envMode="MODULATE"
                glChannel="1"
                texChannel="12"
                texCoordSet="0"/>
        </ShaderPass>
        <ShaderPass name="directional_light_planet_pass"
            blendEnabled="1"
            depthMask="0"
            depthRangeMin="1"
            normalArrayEnabled="1"
            program="directional_light_planet_program"
            useLights="DIRECTIONAL_LIGHT">
            <channelTextureSet envMode="MODULATE"
                glChannel="0"
                texChannel="8"
                texCoordSet="0"/>
            <channelTextureSet envMode="MODULATE"
                glChannel="1"
                texChannel="12"
                texCoordSet="0"/>
        </ShaderPass>
        <ShaderProgram name="directional_light_planet_program">
            <shaderProgramObject index="0"
                name="data/shaders/directional_light_detail.vert"/>
            <shaderProgramObject index="1"
                name="data/shaders/directional_light_planet.frag"/>
        </ShaderProgram>
        <ShaderProgram name="directional_light_program">
            <shaderProgramObject index="0"
                name="data/shaders/directional_light.vert"/>
            <shaderProgramObject index="1"
                name="data/shaders/directional_light.frag"/>
        </ShaderProgram>
        <ShaderProgram name="directional_light_program_detail">
            <shaderProgramObject index="0"
                name="data/shaders/directional_light_detail.vert"/>
            <shaderProgramObject index="1"
                name="data/shaders/directional_light_detail.frag"/>
        </ShaderProgram>
        <ShaderPass name="first_pass"
            fogEnabled="1"
            useSceneAmbientColor="1">
            <channelTextureSet envMode="MODULATE"
                glChannel="0"
                texChannel="8"
                texCoordSet="0"/>
            <channelTextureSet envMode="ADD"
                glChannel="1"
                texChannel="10"
                texCoordSet="0"/>
        </ShaderPass>
        <ShaderPass name="first_pass_detail"
            useSceneAmbientColor="1">
            <channelTextureSet envMode="MODULATE"
                glChannel="0"
                texChannel="8"
                texCoordSet="0"/>
            <channelTextureSet envMode="MODULATE"
                glChannel="1"
                texChannel="9"
                texCoordSet="0"/>
            <channelTextureSet envMode="ADD"
                glChannel="2"
                texChannel="10"
                texCoordSet="0"/>
        </ShaderPass>
        <Shader name="no_light_shader">
            <pass index="0"
                name="first_pass"/>
        </Shader>
        <ShaderPass name="point_light_pass"
            blendEnabled="1"
            depthMask="0"
            normalArrayEnabled="1"
            program="point_light_program"
            useLights="POINT_LIGHT">
            <channelTextureSet envMode="MODULATE"
                glChannel="0"
                texChannel="8"
                texCoordSet="0"/>
            <channelTextureSet envMode="MODULATE"
                glChannel="1"
                texChannel="12"
                texCoordSet="0"/>
        </ShaderPass>
        <ShaderPass name="point_light_pass_detail"
            blendEnabled="1"
            depthMask="0"
            normalArrayEnabled="1"
            program="point_light_program_detail"
            useLights="POINT_LIGHT">
            <channelTextureSet envMode="MODULATE"
                glChannel="0"
                texChannel="8"
                texCoordSet="0"/>
            <channelTextureSet envMode="MODULATE"
                glChannel="1"
                texChannel="12"
                texCoordSet="0"/>
            <channelTextureSet envMode="MODULATE"
                glChannel="2"
                texChannel="9"
                texCoordSet="0"/>
        </ShaderPass>
        <ShaderProgram name="point_light_program">
            <shaderProgramObject index="0"
                name="data/shaders/point_light.vert"/>
            <shaderProgramObject index="1"
                name="data/shaders/point_light.frag"/>
        </ShaderProgram>
        <ShaderProgram name="point_light_program_detail">
            <shaderProgramObject index="0"
                name="data/shaders/point_light_detail.vert"/>
            <shaderProgramObject index="1"
                name="data/shaders/point_light_detail.frag"/>
        </ShaderProgram>
        <ShaderPass name="spot_light_pass"
            blendEnabled="1"
            depthMask="0"
            normalArrayEnabled="1"
            program="spot_light_program"
            useLights="SPOT_LIGHT">
            <channelTextureSet envMode="MODULATE"
                glChannel="0"
                texChannel="8"
                texCoordSet="0"/>
            <channelTextureSet envMode="MODULATE"
                glChannel="1"
                texChannel="12"
                texCoordSet="0"/>
        </ShaderPass>
        <ShaderPass name="spot_light_pass_detail"
            blendEnabled="1"
            depthMask="0"
            normalArrayEnabled="1"
            program="spot_light_program_detail"
            useLights="SPOT_LIGHT">
            <channelTextureSet envMode="MODULATE"
                glChannel="0"
                texChannel="8"
                texCoordSet="0"/>
            <channelTextureSet envMode="MODULATE"
                glChannel="1"
                texChannel="12"
                texCoordSet="0"/>
            <channelTextureSet envMode="MODULATE"
                glChannel="2"
                texChannel="9"
                texCoordSet="0"/>
        </ShaderPass>
        <ShaderProgram name="spot_light_program">
            <shaderProgramObject index="0"
                name="data/shaders/spot_light.vert"/>
            <shaderProgramObject index="1"
                name="data/shaders/spot_light.frag"/>
        </ShaderProgram>
        <ShaderProgram name="spot_light_program_detail">
            <shaderProgramObject index="0"
                name="data/shaders/spot_light_detail.vert"/>
            <shaderProgramObject index="1"
                name="data/shaders/spot_light_detail.frag"/>
        </ShaderProgram>
        <ShaderPass name="vertex_light_shader_pass"
            alphaTestEnabled="1"
            fogEnabled="1"
            normalArrayEnabled="1"
            useFixedFunctionLighting="1"
            useLights="POINT_LIGHT">
            <channelTextureSet envMode="MODULATE"
                glChannel="0"
                texChannel="8"
                texCoordSet="0"/>
            <channelTextureSet envMode="ADD"
                glChannel="1"
                texChannel="10"
                texCoordSet="0"/>
        </ShaderPass>
      <ShaderPass name="vertex_light_shader_pass_dual"
           alphaTestEnabled="1"
           fogEnabled="1"
           cullFaceEnabled="false" 
           normalArrayEnabled="1"
           useFixedFunctionLighting="1"
           useLights="POINT_LIGHT">
        <channelTextureSet envMode="MODULATE"
            glChannel="0"
            texChannel="8"
            texCoordSet="0"/>
        <channelTextureSet envMode="ADD"
            glChannel="1"
            texChannel="10"
            texCoordSet="0"/>
      </ShaderPass>
        <ShaderPass name="vertex_light_shader_planet"
            blendEnabled="1"
            depthRangeMin="1"
            dstBlend="ZERO"
            normalArrayEnabled="1"
            useFixedFunctionLighting="1">
            <channelTextureSet envMode="MODULATE"
                glChannel="0"
                texChannel="8"
                texCoordSet="0"/>
        </ShaderPass>
    </objects>
</root>
