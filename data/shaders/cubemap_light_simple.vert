attribute vec4 tangent; 

uniform mat4 objectTransform;
uniform vec3 cameraPositionWorldSpace;

varying	vec3 g_viewVec; 

void main()
{
	gl_Position = ftransform();		

	mat3 tempRotation = objectTransform;
	
	vec3 g_normal = tempRotation*gl_Normal.xyz;
	g_normal = normalize(g_normal);

	vec4 worldSpaceVert = objectTransform*vec4(gl_Vertex.xyz,1.0); 

	vec3 camVec = worldSpaceVert.xyz - cameraPositionWorldSpace;
	camVec = normalize(camVec);
	camVec.z = -camVec.z;
	
	vec3 texCoord = reflect(camVec,g_normal);
	
	vec3 binormal = cross(gl_Normal.xyz,tangent.xyz)*tangent.w; 

	mat3 TBN_Matrix =  mat3(tempRotation*tangent.xyz,tempRotation*binormal, g_normal);
	g_viewVec = (-camVec) * TBN_Matrix ;	
	//g_viewVec = camVec;

	//reflect
	gl_TexCoord[0] = vec4(texCoord,1.0);
	//diffuse map
	gl_TexCoord[1] = vec4(gl_MultiTexCoord0.xyz,1.0);	
	//diffuse cubemap
	gl_TexCoord[2] = vec4(g_normal,1.0);
	//emissive
	gl_TexCoord[3] = vec4(gl_MultiTexCoord0.xyz,1.0);
	
	float col = max(0.0,dot(camVec,g_normal));

	col = 1.0 - col;
	col*=col;

	col*=3.0;
	//col = 1.0;
	gl_FrontColor = vec4(col,col,col,col);


}


