<?xml version="1.0"?>

<root
	xmlns="http://scene.gametoolkit.hoplon.com/scene"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://scene.gametoolkit.hoplon.com/scene ../scene.xsd">

 
  <includes>    
  
    <include file = "data/shaders/atmosphere_simple_earth.vert" />
    <include file = "data/shaders/atmosphere_simple_venus.vert" />
    <include file = "data/shaders/atmosphere_simple_mars.vert" />
</includes>

<objects>
  <ShaderProgram name="atmosphere_shader_program_earth">
    <shaderProgramObject name="data/shaders/atmosphere_simple_earth.vert"/>
  </ShaderProgram>

    <ShaderProgram name="atmosphere_shader_program_venus">
    <shaderProgramObject name="data/shaders/atmosphere_simple_venus.vert"/>
  </ShaderProgram>

  
    <ShaderProgram name="atmosphere_shader_program_mars">
    <shaderProgramObject name="data/shaders/atmosphere_simple_mars.vert"/>
  </ShaderProgram>

  <ShaderPass name="atmosphere_pass_earth"
              blendEnabled="true"
              srcBlend="ONE"
              dstBlend="ONE"
              depthMask="false"
              depthRangeMin="1.0"
              program="atmosphere_shader_program_earth"
              cullFaceEnabled="true"
              cullFace="FRONT"
              useLights="DIRECTIONAL_LIGHT"           
              >
  </ShaderPass>

 <ShaderPass name="atmosphere_pass_venus"
              blendEnabled="true"
              srcBlend="ONE"
              dstBlend="ONE"
              depthMask="false"
              depthRangeMin="1.0"
              program="atmosphere_shader_program_venus"
              cullFaceEnabled="true"
              cullFace="FRONT"
              useLights="DIRECTIONAL_LIGHT"           
              >
  </ShaderPass>

   <ShaderPass name="atmosphere_pass_mars"
              blendEnabled="true"
              srcBlend="ONE"
              dstBlend="ONE"
              depthMask="false"
              depthRangeMin="1.0"
              program="atmosphere_shader_program_mars"
              cullFaceEnabled="true"
              cullFace="FRONT"
              useLights="DIRECTIONAL_LIGHT"           
              >
  </ShaderPass>

</objects>

</root>