<root xmlns="http://scene.gametoolkit.hoplon.com/scene"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://scene.gametoolkit.hoplon.com/scene ../scene.xsd">
    <includes>
        <include file="data/shaders/particle_view_align.vert"/>
    </includes>
    <objects>
        <ShaderProgram name="part_view_align_shader_program">
            <shaderProgramObject index="0"
                name="data/shaders/particle_view_align.vert"/>
        </ShaderProgram>
        <Shader name="particle_add_shader">
            <pass index="0"
                name="particle_add_shader_pass"/>
        </Shader>
        <ShaderPass name="particle_add_shader_pass"
            alphaTestEnabled="1"
            blendEnabled="1"
            colorArrayEnabled="1"
            cullFaceEnabled="0"
            depthMask="0"
            srcBlend="SRC_ALPHA">
            <channelTextureSet envMode="MODULATE"
                glChannel="0"
                texChannel="8"
                texCoordSet="0"/>
        </ShaderPass>
        <Shader name="particle_add_view_aligned_shader"
            fallback="particle_add_shader">
            <pass index="0"
                name="particle_add_view_aligned_shader_pass"/>
        </Shader>
        <ShaderPass name="particle_add_view_aligned_shader_pass"
            alphaTestEnabled="1"
            blendEnabled="1"
            colorArrayEnabled="1"
            cullFaceEnabled="0"
            depthMask="0"
            program="part_view_align_shader_program"
            srcBlend="SRC_ALPHA">
            <channelTextureSet envMode="MODULATE"
                glChannel="0"
                texChannel="8"
                texCoordSet="0"/>
        </ShaderPass>
        <Shader name="particle_alpha_shader">
            <pass index="0"
                name="particle_apha_shader_pass"/>
        </Shader>
        <Shader name="particle_alpha_view_aligned_shader"
            fallback="particle_alpha_shader">
            <pass index="0"
                name="particle_alpha_view_aligned_shader_pass"/>
        </Shader>
        <ShaderPass name="particle_alpha_view_aligned_shader_pass"
            alphaTestEnabled="1"
            blendEnabled="1"
            colorArrayEnabled="1"
            cullFaceEnabled="0"
            depthMask="0"
            dstBlend="ONE_MINUS_SRC_ALPHA"
            program="part_view_align_shader_program"
            srcBlend="SRC_ALPHA">
            <channelTextureSet envMode="MODULATE"
                glChannel="0"
                texChannel="8"
                texCoordSet="0"/>
        </ShaderPass>
        <ShaderPass name="particle_apha_shader_pass"
            alphaTestEnabled="1"
            blendEnabled="1"
            colorArrayEnabled="1"
            cullFaceEnabled="0"
            depthMask="0"
            dstBlend="ONE_MINUS_SRC_ALPHA"
            srcBlend="SRC_ALPHA">
            <channelTextureSet envMode="MODULATE"
                glChannel="0"
                texChannel="8"
                texCoordSet="0"/>
        </ShaderPass>
        <Shader name="particle_dualblend_noalpha">
            <pass index="0"
                name="particle_add_shader_pass"/>
        </Shader>
        <ShaderPass name="particle_dualblend_noalpha_pass"
            blendEnabled="1"
            colorArrayEnabled="1"
            cullFaceEnabled="0"
            depthMask="0">
            <channelTextureSet envMode="MODULATE"
                glChannel="0"
                texChannel="8"
                texCoordSet="0"/>
        </ShaderPass>
        <ShaderPass name="particle_sht_dualblend_pass"
            alphaTestEnabled="1"
            blendEnabled="1"
            cullFace="FRONT_AND_BACK"
            cullFaceEnabled="0"
            depthMask="0">
            <channelTextureSet envMode="MODULATE"
                glChannel="0"
                texChannel="8"
                texCoordSet="0"/>
        </ShaderPass>
        <ShaderPass name="particle_sht_dualblend_prot_pass"
            alphaTestEnabled="1"
            blendEnabled="1"
            colorArrayEnabled="1"
            cullFace="FRONT_AND_BACK"
            cullFaceEnabled="0"
            depthMask="0">
            <channelTextureSet envMode="MODULATE"
                glChannel="0"
                texChannel="8"
                texCoordSet="0"/>
        </ShaderPass>
        <Shader name="particle_sht_dualblend_prot_shader">
            <pass index="0"
                name="particle_sht_dualblend_prot_pass"/>
        </Shader>
        <Shader name="particle_sht_dualblend_shader">
            <pass index="0"
                name="particle_sht_dualblend_pass"/>
        </Shader>
    </objects>
</root>
