attribute vec4 tangent; 
 
//light position 
uniform vec3 lightPositionWorldSpace; 
//camera position 
uniform vec3 cameraPositionWorldSpace; 
uniform mat4 objectTransform; 
 
varying	vec3 g_lightVec; 
varying	vec3 g_viewVec; 
varying	vec3 g_atten; 
 
 
void main() 
{ 
	gl_Position = ftransform(); 
	gl_TexCoord[0] = gl_TextureMatrix[0]*gl_MultiTexCoord0; 
	//detail texture
	gl_TexCoord[1] = gl_TextureMatrix[2]*vec4(gl_MultiTexCoord0.xyz,1.0);
	 
	vec4 binormal = vec4(cross(gl_Normal.xyz,tangent.xyz)*tangent.w, 1.0); 
	 
 
	vec4 worldSpaceVert = objectTransform*vec4(gl_Vertex.xyz,1.0); 
	mat4 tempRotation = objectTransform;
	
	tempRotation[3] = vec4(0,0,0,1);
	vec3 wTangent = (tempRotation*tangent).xyz;
	vec3 wBinormal = (tempRotation*binormal).xyz;
	vec3 wNormal = (tempRotation*vec4(gl_Normal,1.0)).xyz;
	
	mat3 TBN_Matrix =  mat3(wTangent,wBinormal,	wNormal);
							
	g_atten = (lightPositionWorldSpace.xyz - worldSpaceVert.xyz); 
	g_lightVec =  g_atten * TBN_Matrix; 
	 
	g_viewVec = ( worldSpaceVert.xyz - cameraPositionWorldSpace.xyz) * TBN_Matrix ;		 
}