uniform vec3 lightDirectionWorldSpace; 
uniform vec3 cameraPositionWorldSpace;
uniform vec3 lightDiffuseColor;

uniform mat4 objectTransform;

void main()
{
	vec3 atmColor1 = vec3(1.7,1.2,0.5);
	vec3 atmColor2 = vec3(0.9,0.8,0.4);

	//extruding our vert along its normal
	vec4 npos = vec4(gl_Vertex.xyz + gl_Normal*40.0,1.0);

	//calculating projected vert
	gl_Position = gl_ModelViewProjectionMatrix * npos;

	//use this to calculate our normals in world-space
	mat3 tempRotation = mat3(objectTransform[0].xyz,objectTransform[1].xyz,objectTransform[2].xyz);
	vec3 worldNormal = normalize(tempRotation * gl_Normal.xyz);
	
	//silhouete
	//vert in world-space
	vec3 wsVert = (objectTransform * npos).xyz;
	//vector from vert->camera
	vec3 viewVec = normalize(wsVert - cameraPositionWorldSpace);

	//fade out in the edge
	float edge = max(0.0,dot(worldNormal,viewVec));	
	float invEdge = 1.0 - edge;
	float tempEdge = invEdge;
	invEdge*=invEdge*1.3;
	invEdge = max(0.0,invEdge-0.5);
	
	edge= clamp(0.5 - invEdge,0.0,1.0);
	edge*=edge*edge;
	invEdge = clamp(tempEdge*5.0-2.60,0.0,1.0);
	edge*=invEdge;

	//modify atm color and intensity with respect to light source too
	float lAtten = clamp(1.25 + dot(worldNormal , lightDirectionWorldSpace), 0.0 , 1.0);
	
	vec3 color = edge * lAtten * mix(atmColor2,atmColor1, lAtten);

	gl_FrontColor = vec4(color,1.0);
}


