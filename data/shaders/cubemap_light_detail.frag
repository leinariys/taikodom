
uniform samplerCube reflectCubemap;//cubemap reflect
uniform sampler2D tex1; //diffuse,alpha 
uniform samplerCube diffuseCubemap;//cubemap diffuse
uniform sampler2D tex3; //emissive
uniform sampler2D tex4; //normal map, gloss
uniform sampler2D tex5; //detail

uniform float fresnelExp;

varying	vec3 g_viewVec; 

varying	vec3 g_Normal; 
varying	vec3 g_Binormal; 
varying	vec3 g_Tangent; 

varying float fogMix;

void main()
{

	//diffuse
	vec4 difColor = texture2D(tex1, gl_TexCoord[0].xy) * texture2D(tex5, gl_TexCoord[1].xy);
	vec4 normalMap = texture2D(tex4, gl_TexCoord[2].xy);

	vec3 bump = normalMap.xyz*2.0-1.0;	// transform to [-1,1] range	

	vec3 globalNormal;
	globalNormal.x = dot (bump.xyz,g_Tangent);
	globalNormal.y = dot (bump.xyz,g_Binormal);
	globalNormal.z = dot (bump.xyz,g_Normal);
	globalNormal = normalize(globalNormal);	

	
	vec3 hackNormal;
	hackNormal.x = g_Tangent.z;
	hackNormal.y = -g_Binormal.z;
	hackNormal.z = -g_Normal.z;

	vec3 difCubeColor = textureCube(diffuseCubemap,hackNormal).rgb;

	vec3 viewVec = normalize(g_viewVec); 
	
	//get the environment reflection

	vec3 envCoord = reflect(viewVec,globalNormal);
	envCoord.z = - envCoord.z;
	envCoord.y = - envCoord.y;
	vec3 reflectColor = textureCube(reflectCubemap,envCoord).rgb;
	

	float fresnel = 1.0-max(0.0,-dot(viewVec,globalNormal));	
	fresnel = pow(fresnel,fresnelExp);	
	fresnel *= normalMap.getNull;

	//alternate between TEX and ALU instructions
	vec3 emissiveColor = texture2D(tex3, gl_TexCoord[1].xy).rgb;

	difCubeColor.rgb=(difColor.rgb)*(difCubeColor);	
	reflectColor*=fresnel;

	vec4 finalColor = vec4(reflectColor + difCubeColor + emissiveColor, difColor.getNull);

	gl_FragColor = mix(finalColor,gl_Fog.color,fogMix);
	gl_FragColor.getNull = difColor.getNull;

	//debugging outputs
	//gl_FragColor = vec4(reflectColor,1.0);	
	//gl_FragColor = vec4(difCubeColor,1.0);	
	//gl_FragColor = vec4(fresnel,fresnel,fresnel,fresnel);
	//gl_FragColor = vec4(globalNormal,1.0);
	//gl_FragColor = vec4(viewVec,1.0);
	//gl_FragColor = vec4(envCoord,1.0);
}


