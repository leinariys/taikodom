attribute vec4 tangent; 

uniform mat4 objectTransform;
uniform mat4 objectAffineTransform;
uniform vec3 cameraPositionWorldSpace;
uniform vec3 cameraPositionObjectSpace;

varying	vec3 g_viewVec; 

//we use this since we have to work in global space when reflecting environment
varying	vec3 g_Normal; 
varying	vec3 g_Binormal; 
varying	vec3 g_Tangent; 

varying float fogMix;

void main()
{
	gl_Position = ftransform();		
	
	vec3 binormal = cross(gl_Normal.xyz,tangent.xyz)*tangent.w; 

	//diffuse, emissive and normal map
	gl_TexCoord[0] = gl_TextureMatrix[1]*vec4(gl_MultiTexCoord0.xyz,1.0);
	gl_TexCoord[1] = gl_TextureMatrix[3]*vec4(gl_MultiTexCoord0.xyz,1.0);
	gl_TexCoord[2] = gl_TextureMatrix[4]*vec4(gl_MultiTexCoord0.xyz,1.0);
	//detail texture
	gl_TexCoord[3] = gl_TextureMatrix[5]*vec4(gl_MultiTexCoord0.xyz,1.0);	

	
	g_Tangent.x = dot(tangent.xyz,objectAffineTransform[0].xyz);
	g_Binormal.x = dot(tangent.xyz,objectAffineTransform[1].xyz);
	g_Normal.x = dot(tangent.xyz,objectAffineTransform[2].xyz);	

	g_Tangent.y = dot(binormal,objectAffineTransform[0].xyz);
	g_Binormal.y = dot(binormal,objectAffineTransform[1].xyz);
	g_Normal.y = dot(binormal,objectAffineTransform[2].xyz);

	g_Tangent.z = dot(gl_Normal.xyz,objectAffineTransform[0].xyz);
	g_Binormal.z = dot(gl_Normal.xyz,objectAffineTransform[1].xyz);
	g_Normal.z = dot(gl_Normal.xyz,objectAffineTransform[2].xyz);

	g_viewVec = (objectTransform*gl_Vertex).xyz - cameraPositionWorldSpace.xyz;	
	
	fogMix = clamp((gl_Position.z - gl_Fog.start)*gl_Fog.scale,0.0,1.0);
}

