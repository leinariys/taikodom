<?xml version="1.0"?>	



<root
    xmlns="http://scene.gametoolkit.hoplon.com/scene"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://scene.gametoolkit.hoplon.com/scene ../scene.xsd">

    <objects>


	<ShaderPass name="loot_transp_pass" 
      blendEnabled="true"
      depthMask="true"
      srcBlend="SRC_ALPHA"
      dstBlend="ONE_MINUS_SRC_ALPHA">
      <channelTextureSet glChannel="0" texChannel="8" texCoordSet="0" envMode="MODULATE"/>
   	</ShaderPass>

    <Shader name="loot_transp_pass_shader">
      <pass name="loot_transp_pass" />
    </Shader>


</objects>

</root>