uniform mat4 projectTransform;
uniform mat4 objectTransform;

uniform vec3 cameraPositionWorldSpace;

varying vec3 camVec;

varying vec3 wNormal;
varying float fogMix;

void main()
{
	gl_Position = ftransform();		
	
	//diffuse 
	gl_TexCoord[0] = gl_TextureMatrix[0]*vec4(gl_MultiTexCoord0.xyz,1.0);
	//normal
	gl_TexCoord[2] = vec4(gl_Normal,1.0);	
	
	mat4 tempRotation = objectTransform;
	
	tempRotation[3] = vec4(0,0,0,1);
	wNormal = (tempRotation*vec4(gl_Normal,1.0)).xyz;
	
	//our cubemap texcoords are the vector from camera to vertex
	vec4 vPos = objectTransform * vec4(gl_Vertex.xyz,1.0);
	camVec = cameraPositionWorldSpace - vPos.xyz;
	
	fogMix = 1.0 - clamp((gl_Position.z - gl_Fog.start)*gl_Fog.scale,0.0,1.0);	
}


