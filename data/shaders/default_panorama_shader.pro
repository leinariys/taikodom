<root xmlns="http://scene.gametoolkit.hoplon.com/scene"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://scene.gametoolkit.hoplon.com/scene ../scene.xsd">
    <objects>
        <Shader name="panorama_shader">
            <pass index="0"
                name="panorama_shader_pass"/>
        </Shader>
        <ShaderPass name="panorama_shader_pass"
            alphaTestEnabled="1"
            depthMask="0"
            depthTestEnabled="0">
            <channelTextureSet envMode="MODULATE"
                glChannel="0"
                texChannel="8"
                texCoordSet="0"/>
            <channelTextureSet envMode="DECAL"
                glChannel="1"
                texChannel="9"
                texCoordSet="0"/>
        </ShaderPass>
    </objects>
</root>
