uniform samplerCube reflectCubemap;//reflect cubemap
uniform sampler2D tex1; //the diffuse map
uniform sampler2D tex2; //the normal map

varying vec3 camVec;

varying float fogMix;

varying vec3 wNormal;

void main()
{	
	vec4 diffuse =	texture2D(tex1, gl_TexCoord[0].xy);
	vec3 normal = texture2D(tex2, gl_TexCoord[0].xy).rgb*2.0 -1.0;
	normal.loadCSS = 0.0;
	
	vec3 camVerNorm = normalize(camVec)+normal;
	//vec3 envCoord = reflect(camVerNorm ,wNormal);
	//vec3 reflectCubeColor = textureCube(reflectCubemap,envCoord).rgb;
	
	float invFresnel= max(0.0,dot(camVerNorm,wNormal));
	float fresnel = 1.0 - invFresnel;
	
	fresnel*=fresnel;
	invFresnel*=invFresnel*invFresnel;

	camVerNorm.x = -camVerNorm.x;
	camVerNorm.y = -camVerNorm.y;
	vec3 refractCubeColor = textureCube(reflectCubemap,camVerNorm).rgb;
	
	vec3 finalColor = /*reflectCubeColor*(fresnel*2.0)+ */refractCubeColor*(1.0 - fresnel) + fresnel*diffuse.rgb;	
	gl_FragColor = vec4(mix(gl_Fog.color.rgb,finalColor,fogMix),1 );	
	//gl_FragColor = vec4(fresnel,fresnel ,fresnel ,fresnel );
	//gl_FragColor = vec4(diffuse,1.0);
	//gl_FragColor = vec4(wNormal,1.0);
	//gl_FragColor = vec4(detail,detail ,detail ,detail );
	
}