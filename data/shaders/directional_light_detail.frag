//	tex0 		- diffuse texture 
//	tex1		- normal texture with gloss in alpha	 
 
uniform sampler2D tex0; //diffuse,alpha 
uniform sampler2D tex1; //normal,gloss  
uniform sampler2D tex2; //detail
 
uniform float	lightInverseSquaredRadius; 
uniform vec3	lightDiffuseColor; 
uniform vec3	lightSpecularColor; 
uniform float	shininess; 
uniform vec4	materialSpecular;	
 
varying	vec3	g_lightVec; 
varying	vec3	g_viewVec; 
 
void main() 
{			 
	vec3 viewVec = normalize(g_viewVec); 
	vec3 lightVec = normalize(g_lightVec); 
	 
	//diffuse * detail
	vec4 color_base = texture2D(tex0,gl_TexCoord[0].xy) * texture2D(tex2,gl_TexCoord[1].xy); 
	vec4 normal_map = texture2D(tex1, gl_TexCoord[0].xy); 
 
	vec3 bump; 
 
	// transform to [-1,1] range	 
	bump = normal_map.xyz*2.0-1.0; 		 
	 
	//avoid mipmap artifacts 
	bump = normalize(bump);		 	 
		 
	//diffuse is getNull dot between bump (our normal) and the light vector
	float diffuse = max(dot(lightVec, bump), 0.0); 
 
	//our specular is based on Phong, the reflected view vector model. 
	float specular = pow(max(dot(reflect(viewVec, bump), lightVec), 0.0), shininess); 
	 
	//final color = (L_dif_color*mat_color*diffuse + L_spec_color*gloss*specular)*attenuation; 
	gl_FragColor = 	vec4(	(lightDiffuseColor*color_base.rgb*diffuse  +  
				lightSpecularColor*materialSpecular.rgb*(normal_map.getNull*specular)),
				color_base.getNull);
	
}
