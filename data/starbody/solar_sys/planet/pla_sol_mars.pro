<root xmlns="http://scene.gametoolkit.hoplon.com/scene"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://scene.gametoolkit.hoplon.com/scene ../scene.xsd">
    <includes>
        <include file="data/shaders/atmosphere_shader.pro"/>
        <include file="data/shaders/light_shader.pro"/>
        <include file="data/starbody/misc/pla_misc_sphere_high.gr2"/>
        <include file="data/starbody/misc/star_misc_clouds.dds"/>
        <include file="data/starbody/solar_sys/planet/pla_sol_mars_dfs.dds"/>
        <include file="data/starbody/solar_sys/planet/pla_sol_mars_glow.dds"/>
        <include file="data/starbody/solar_sys/planet/pla_sol_mars_nrm.dds"/>
    </includes>
    <objects>
        <Material name="final_mars_material"
            diffuseTexture="pla_sol_mars_dfs"
            normalTexture="pla_sol_mars_nrm"
            selfIluminationTexture="pla_sol_mars_glo"
            shader="mars_shader"
            shininess="4"
            specular="1 0.5 0.2 1">
            <texture index="0"
                name="mars_panned_clouds0"/>
            <texture index="1"
                name="mars_panned_clouds1"/>
            <texture index="8"
                name="pla_sol_mars_dfs"/>
            <texture index="10"
                name="pla_sol_mars_glo"/>
            <texture index="12"
                name="pla_sol_mars_nrm"/>
        </Material>
        <ShaderPass name="first_pass_mars"
            blendEnabled="1"
            depthRangeMin="1"
            dstBlend="ZERO">
            <channelTextureSet envMode="REPLACE"
                glChannel="0"
                texChannel="10"
                texCoordSet="0"/>
        </ShaderPass>
        <ShaderPass name="marsCloudPass"
            blendEnabled="1"
            depthMask="0"
            depthRangeMin="1"
            dstBlend="ONE_MINUS_SRC_ALPHA"
            srcBlend="DST_COLOR">
            <channelTextureSet envMode="MODULATE"
                glChannel="0"
                texChannel="0"
                texCoordSet="0"/>
            <channelTextureSet envMode="ADD"
                glChannel="1"
                texChannel="1"
                texCoordSet="0"/>
        </ShaderPass>
        <RMesh name="mars_mesh"
            material="final_mars_material"
            mesh="data/starbody/misc/pla_misc_sphere_high.gr2/mesh/terra"
            renderPriority="50"
            shader="mars_shader"/>
        <PannedTexture name="mars_panned_clouds0"
            panSpeed="0.001 0 0"
            texture="mars_tiled_clouds0"/>
        <PannedTexture name="mars_panned_clouds1"
            panSpeed="-0.002 0 0"
            texture="mars_tiled_clouds1"/>
        <Shader name="mars_shader"
            fallback="mars_shader_simple"
            quality="0">
            <pass index="0"
                name="first_pass_mars"/>
            <pass index="1"
                name="directional_light_planet_pass"/>
            <pass index="2"
                name="marsCloudPass"/>
            <pass index="3"
                name="atmosphere_pass_mars"/>
        </Shader>
        <Shader name="mars_shader_simple"
            fallback="mars_shader_simplest"
            quality="1">
            <pass index="0"
                name="first_pass_mars"/>
            <pass index="1"
                name="directional_light_planet_pass"/>
            <pass index="2"
                name="marsCloudPass"/>
        </Shader>
        <Shader name="mars_shader_simplest"
            quality="2">
            <pass index="0"
                name="vertex_light_shader_planet"/>
        </Shader>
        <TiledTexture name="mars_tiled_clouds0"
            texture="data/starbody/misc/star_misc_clouds.dds"
            tile="2 1 1"/>
        <TiledTexture name="mars_tiled_clouds1"
            texture="data/starbody/misc/star_misc_clouds.dds"
            tile="3 -1 1"/>
        <PannedTexture name="pla_sol_mars_dfs"
            panSpeed="-0.0002 0 0"
            texture="data/starbody/solar_sys/planet/pla_sol_mars_dfs.dds"/>
        <PannedTexture name="pla_sol_mars_glo"
            panSpeed="-0.0002 0 0"
            texture="data/starbody/solar_sys/planet/pla_sol_mars_glow.dds"/>
        <PannedTexture name="pla_sol_mars_nrm"
            panSpeed="-0.0002 0 0"
            texture="data/starbody/solar_sys/planet/pla_sol_mars_nrm.dds"/>
    </objects>
</root>
