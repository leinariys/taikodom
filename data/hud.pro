<?xml version="1.0"?>	

<root
	xmlns="http://scene.gametoolkit.hoplon.com/scene"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://scene.gametoolkit.hoplon.com/scene scene.xsd">
  
<includes>
<include file="data/gui/imageset/imageset_hud/target_aimprediction.png"/>
<include file="data/gui/imageset/imageset_hud/hud_target_aim_normal.png"/>
<include file="data/gui/imageset/imageset_hud/target_station.png"/>
<include file="data/gui/imageset/imageset_hud/target_waypoint.png"/>
<include file="data/gui/imageset/imageset_hud/target_portal.png"/>
<include file="data/gui/imageset/imageset_hud/target_loot.png"/>
<include file="data/gui/imageset/imageset_hud/target_corner.png"/>
<include file="data/gui/imageset/imageset_hud/target_boss.png"/>
<include file="data/gui/imageset/imageset_hud/target_asteroid.png"/>
<include file="data/gui/imageset/imageset_hud/target_player.png"/>
<include file="data/gui/imageset/imageset_hud/target_npc.png"/>
<include file="data/gui/imageset/imageset_hud/hud_infinite_aim.png"/>
</includes>
<objects>


<RHudMark name="target_turret"
	mark="data/gui/imageset/imageset_hud/target_corner.png/material"
	lock="data/gui/imageset/imageset_hud/target_corner.png/material"
	markOut="data/gui/imageset/imageset_hud/target_corner.png/material"
	markSelected="data/gui/imageset/imageset_hud/target_corner.png/material"
	maxLockSize="200"
	minLockSize="1"
	areaRadius="250"
	centerColor="0 0 0 0"	
	outColor="1 1 1 1"
	satellitesColor="1 1 1 0.5"
	selectedColor="1 1 1 1"	
	showWhenOffscreen="false"
	maxVisibleDistance="0"
/>

<RHudMark name="target_waypoint"
	mark="data/gui/imageset/imageset_hud/target_waypoint.png/material"
	lock="data/gui/imageset/imageset_hud/target_waypoint.png/material"
	markOut="data/gui/imageset/imageset_hud/target_waypoint.png/material"
	markSelected="data/gui/imageset/imageset_hud/target_waypoint.png/material"	
	maxLockSize="60"
	minLockSize="20"
	areaRadius="250"
	centerColor="0.7 0.7 0.7 0.7"	
	outColor="1 1 1 1"
	satellitesColor="1 1 0 0"
	selectedColor="1 1 1 0.5"	
/>

<RHudMark name="target_aimprediction"
	mark="data/gui/imageset/imageset_hud/target_aimprediction.png/material"
	lock="data/gui/imageset/imageset_hud/target_aimprediction.png/material"
	markOut="data/gui/imageset/imageset_hud/target_aimprediction.png/material"
	markSelected="data/gui/imageset/imageset_hud/target_aimprediction.png/material"
	maxLockSize="60"
	minLockSize="20"
	areaRadius="250"
	centerColor="0.7 0.7 0.7 0.7"	
	outColor="0.5 0.5 0.5 0.5"
	satellitesColor="1 1 0 0"
	selectedColor="1 1 1 0.5"	
/>

<RHudMark name="target_portal"
	mark="data/gui/imageset/imageset_hud/target_portal.png/material"
	lock="data/gui/imageset/imageset_hud/target_portal.png/material"
	markOut="data/gui/imageset/imageset_hud/target_portal.png/material"
	markSelected="data/gui/imageset/imageset_hud/target_portal.png/material"	
	maxLockSize="60"
	minLockSize="20"
	areaRadius="250"
	centerColor="0.7 0.7 0.7 0.7"
	outColor="1 1 1 1"
	satellitesColor="0.5 0.5 0.5 0.5"
	selectedColor="1 1 1 0.5"	
/>


<RHudMark name="target_station"
	mark="data/gui/imageset/imageset_hud/target_station.png/material"
	lock="data/gui/imageset/imageset_hud/target_station.png/material"
	markOut="data/gui/imageset/imageset_hud/target_station.png/material"
	markSelected="data/gui/imageset/imageset_hud/target_station.png/material"	
	maxLockSize="60"
	minLockSize="20"
	areaRadius="250"
	centerColor="0.7 0.7 0.7 0.7"	
	outColor="1 1 1 1"
	satellitesColor="0.5 0.5 0.5 0.5"
	selectedColor="1 1 1 0.5"	
/>


<RHudMark name="target_loot"
	mark="data/gui/imageset/imageset_hud/target_loot.png/material"
	lock="data/gui/imageset/imageset_hud/target_corner.png/material"
	markOut="data/gui/imageset/imageset_hud/target_loot.png/material"
	markSelected="data/gui/imageset/imageset_hud/target_loot.png/material"
	maxLockSize="16"
	minLockSize="16"
	areaRadius="250"
	centerColor="0.7 0.7 0.7 0.7"	
	outColor="1 1 1 1"
	satellitesColor="0.7 0.7 0.7 0.7"
	selectedColor="0.7 0.7 0.7 0.7"	
/>

<RHudMark name="target_boss"
	mark="data/gui/imageset/imageset_hud/target_boss.png/material"
	lock="data/gui/imageset/imageset_hud/target_boss.png/material"
	markOut="data/gui/imageset/imageset_hud/target_boss.png/material"
	markSelected="data/gui/imageset/imageset_hud/target_boss.png/material"
	maxLockSize="60"
	minLockSize="20"
	areaRadius="250"
	centerColor="0.7 0.7 0.7 0.7"	
	outColor="1 1 1 1"
	satellitesColor="0.5 0.5 0.5 0.5"
	selectedColor="1 1 1 0.5"	
/>

<RHudMark name="target_asteroid"
	mark="data/gui/imageset/imageset_hud/target_asteroid.png/material"
	lock="data/gui/imageset/imageset_hud/target_asteroid.png/material"
	markOut="data/gui/imageset/imageset_hud/target_asteroid.png/material"
	markSelected="data/gui/imageset/imageset_hud/target_asteroid.png/material"	
	maxLockSize="60"
	minLockSize="20"
	areaRadius="250"
	centerColor="0.7 0.7 0.7 0.7"	
	outColor="1 1 1 1"
	satellitesColor="0.5 0.5 0.5 0.5"
	selectedColor="1 1 1 0.5"	
/>


<RHudMark name="target_player"
	mark="data/gui/imageset/imageset_hud/target_player.png/material"
	lock="data/gui/imageset/imageset_hud/target_player.png/material"
	markOut="data/gui/imageset/imageset_hud/target_player.png/material"
	markSelected="data/gui/imageset/imageset_hud/target_player.png/material"
	maxLockSize="60"
	minLockSize="20"
	areaRadius="250"
	centerColor="0.7 0.7 0.7 0.7"
	outColor="1 1 1 1"
	satellitesColor="1 1 0 0"
	selectedColor="1 1 1 0.5"			
/>


<RHudMark name="target_npc"
	mark="data/gui/imageset/imageset_hud/target_npc.png/material"
	lock="data/gui/imageset/imageset_hud/target_npc.png/material"
	markOut="data/gui/imageset/imageset_hud/target_npc.png/material"
	markSelected="data/gui/imageset/imageset_hud/target_npc.png/material"
	maxLockSize="60"
	minLockSize="20"
	areaRadius="250"
	centerColor="0.7 0.7 0.7 0.7"
	outColor="1 1 1 1"
	satellitesColor="1 0 0 0"
	selectedColor="1 1 1 0.5"			
/>


<RAim name="hud_infinity_aim"
	material="data/gui/imageset/imageset_hud/hud_infinite_aim.png/material"

/>

<RAim name="hud_target_aim"
	material="data/gui/imageset/imageset_hud/hud_target_aim_normal.png/material"

/>

 <ShaderPass name="aim_shader_pass"
            blendEnabled="1"
            dstBlend="ONE_MINUS_SRC_ALPHA"
            srcBlend="ONE_MINUS_DST_COLOR">
            <channelTextureSet envMode="MODULATE"
                glChannel="0"
                texChannel="8"
                texCoordSet="0"/>
        </ShaderPass>
<Shader name="aim_shader">
    <pass index="0"
        name="aim_shader_pass"/>
</Shader>
</objects>
</root>