<?xml version="1.0"?>
<root
	xmlns="http://scene.gametoolkit.hoplon.com/scene"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://scene.gametoolkit.hoplon.com/scene ../scene.xsd">
  
	<includes>
		<include file="data/audio/interface/01_level_up.wav"/>
		<include file="data/audio/interface/02_reward_aquired.wav"/>
		<include file="data/audio/interface/02_xp_aquired.wav"/>
		<include file="data/audio/interface/03_event_message.wav"/>
		<include file="data/audio/interface/04_system_message.wav"/>
		<include file="data/audio/interface/05_message_typing_loop.wav"/>
		<include file="data/audio/interface/05_npc_chat_popup.wav"/>
		<include file="data/audio/interface/06_error_message.wav"/>
		<include file="data/audio/interface/07_mode_change.wav"/>
		<include file="data/audio/interface/08_equip_a.wav"/>
		<include file="data/audio/interface/08_equip_b.wav"/>
		<include file="data/audio/interface/08_equip_c.wav"/>
		<include file="data/audio/interface/08_equip_d.wav"/>
		<include file="data/audio/interface/09_reammo.wav"/>
		<include file="data/audio/interface/10_weapon_change.wav"/>
		<include file="data/audio/interface/11_weapon_empty_a.wav"/>
		<include file="data/audio/interface/11_weapon_empty_b.wav"/>
		<include file="data/audio/interface/12_drag_start.wav"/>
		<include file="data/audio/interface/12_drop_fail.wav"/>
		<include file="data/audio/interface/12_drop_success.wav"/>
		<include file="data/audio/interface/13_ship_fade.wav"/>
		<include file="data/audio/interface/14_mouse_click.wav"/>
		<include file="data/audio/interface/14_mouse_over.wav"/>
		<include file="data/audio/interface/15_window_open.wav"/>
		<include file="data/audio/interface/16_window_close.wav"/>
		<include file="data/audio/interface/17_chat_send.wav"/>
		<include file="data/audio/interface/17_quick_chat_popup.wav"/>
		<include file="data/audio/interface/18_login_typing.wav"/>
		<include file="data/audio/interface/18_mouse_click_login.wav"/>
		<include file="data/audio/interface/19_panel_colapse.wav"/>
		<include file="data/audio/interface/20_panel_expand.wav"/>
		<include file="data/audio/interface/21_undock.wav"/>
		<include file="data/audio/interface/22_panel_pin.wav"/>
		<include file="data/audio/interface/23_panel_unpin.wav"/>
		<include file="data/audio/interface/24_panel_drag_start.wav"/>
		<include file="data/audio/interface/24_panel_drop_success.wav"/>
		<include file="data/audio/interface/25_scroll_bar_loop.wav"/>
		<include file="data/audio/interface/25_scroll_bar_mouse_click.wav"/>
		<include file="data/audio/interface/25_scroll_bar_mouse_over.wav"/>
		<include file="data/audio/interface/26_radio_click.wav"/>
		<include file="data/audio/interface/26_radio_over.wav"/>
		<include file="data/audio/interface/27_bomber_select.wav"/>
		<include file="data/audio/interface/27_explorer_select.wav"/>
		<include file="data/audio/interface/27_fighter_select.wav"/>
		<include file="data/audio/interface/27_freighter_select.wav"/>
		<include file="data/audio/interface/28_loot.wav"/>

		<include file="data/audio/interface/npc_welcome.wav"/>

		<include file="data/audio/interface/gate_enter.wav"/>
		<include file="data/audio/interface/gate_exit.wav"/>

		<include file="data/audio/interface/missile_pursuit_loop.wav"/>
		<include file="data/audio/interface/missile_search_far_loop.wav"/>
		<include file="data/audio/interface/missile_search_near_loop.wav"/>
		<include file="data/audio/interface/missile_target_lock.wav"/>

		<include file="data/audio/interface/shield_empty.wav"/>
		<include file="data/audio/interface/shield_full.wav"/>

		<include file="data/audio/interface/specials/special_afterburner_charged.wav"/>
		<include file="data/audio/interface/specials/special_afterburner_empty.wav"/>

		<include file="data/audio/interface/specials/special_hazardshield_loop.wav"/>

		<include file="data/audio/interface/specials/special_recon_mode_activate.wav"/>
		<include file="data/audio/interface/specials/special_vault_open.wav"/>


	</includes>

	<objects>

		<!-- InterfaceSFX sounds -->

		<SSoundSource name = "sfx_level_up"
			buffer = "data/audio/interface/01_level_up.wav"
			is3d = "false"
			loopCount="1"
			/>
		<SSoundSource name = "sfx_xp_won"
			buffer = "data/audio/interface/02_xp_aquired.wav"
			is3d = "false"
			loopCount="1"
			/>
		<SSoundSource name = "sfx_reward_won"
			buffer = "data/audio/interface/02_reward_aquired.wav"
			is3d = "false"
			loopCount="1"
			/>

		<SSoundSource name = "sfx_mode_change"
			buffer = "data/audio/interface/07_mode_change.wav"
			is3d = "false"
			loopCount="1"
			/>
		<SSoundSource name = "sfx_loot_taken"
			buffer = "data/audio/interface/28_loot.wav"
			is3d = "false"
			loopCount="1"
			/>

		<SSoundSource name = "sfx_event_message"
			buffer = "data/audio/interface/03_event_message.wav"
			is3d = "false"
			loopCount="1"
			/>
		<SSoundSource name = "sfx_message_type"
			buffer = "data/audio/interface/04_system_message.wav"
			is3d = "false"
			loopCount="1"
			/>
		<SSoundSource name = "sfx_npc_chat_loop"
			buffer = "data/audio/interface/05_message_typing_loop.wav"
			is3d = "false"
			loopCount="0"
			/>

		<SSoundSource name = "sfx_equip_amplifier"
			buffer = "data/audio/interface/08_equip_a.wav"
			is3d = "false"
			loopCount="1"
			/>
		<SSoundSource name = "sfx_equip_energy"
			buffer = "data/audio/interface/08_equip_b.wav"
			is3d = "false"
			loopCount="1"
			/>
		<SSoundSource name = "sfx_equip_projectile"
			buffer = "data/audio/interface/08_equip_c.wav"
			is3d = "false"
			loopCount="1"
			/>
		<SSoundSource name = "sfx_equip_launcher"
			buffer = "data/audio/interface/08_equip_d.wav"
			is3d = "false"
			loopCount="1"
			/>

		<SSoundSource name = "sfx_equip_reammo"
			buffer = "data/audio/interface/09_reammo.wav"
			is3d = "false"
			loopCount="1"
			/>
		<SSoundSource name = "sfx_equip_change"
			buffer = "data/audio/interface/10_weapon_change.wav"
			is3d = "false"
			loopCount="1"
			/>

		<SSoundSource name = "sfx_equip_empty_a"
			buffer = "data/audio/interface/11_weapon_empty_a.wav"
			is3d = "false"
			loopCount="1"
			/>
		<SSoundSource name = "sfx_equip_empty_b"
			buffer = "data/audio/interface/11_weapon_empty_b.wav"
			is3d = "false"
			loopCount="1"
			/>

		<SSoundSource name = "sfx_chat_send"
			buffer = "data/audio/interface/17_chat_send.wav"
			is3d = "false"
			loopCount="1"
			/>
		<SSoundSource name = "sfx_ship_window_fade"
			buffer = "data/audio/interface/13_ship_fade.wav"
			is3d = "false"
			loopCount="1"
			/>


		<!-- Interface Sounds (css) -->
		<SSoundSource name = "sfx_npc_chat_popup"
			buffer = "data/audio/interface/05_npc_chat_popup.wav"
			is3d = "false"
			loopCount="1"
			/>
		<SSoundSource name = "sfx_quick_chat_popup"
			buffer = "data/audio/interface/17_quick_chat_popup.wav"
			is3d = "false"
			loopCount="1"
			/>

		<SSoundSource name = "sfx_error_message"
			buffer = "data/audio/interface/06_error_message.wav"
			is3d = "false"
			loopCount="1"
			/>

		<SSoundSource name = "sfx_undock_click"
			buffer = "data/audio/interface/21_undock.wav"
			is3d = "false"
			loopCount="1"
			/>

		<SSoundSource name = "sfx_login_typing"
			buffer = "data/audio/interface/18_login_typing.wav"
			is3d = "false"
			loopCount="1"
			/>

		<SSoundSource name = "sfx_drag_start"
			buffer = "data/audio/interface/12_drag_start.wav"
			is3d = "false"
			loopCount="1"
			/>
		<SSoundSource name = "sfx_drop_fail"
			buffer = "data/audio/interface/12_drop_fail.wav"
			is3d = "false"
			loopCount="1"
			/>
		<SSoundSource name = "sfx_drop_success"
			buffer = "data/audio/interface/12_drop_success.wav"
			is3d = "false"
			loopCount="1"
			/>

		<SSoundSource name = "sfx_mouse_click"
			buffer = "data/audio/interface/14_mouse_click.wav"
			is3d = "false"
			loopCount="1"
			/>
		<SSoundSource name = "sfx_mouse_click_login"
			buffer = "data/audio/interface/18_mouse_click_login.wav"
			is3d = "false"
			loopCount="1"
			/>
		<SSoundSource name = "sfx_mouse_over"
			buffer = "data/audio/interface/14_mouse_over.wav"
			is3d = "false"
			loopCount="1"
			/>

		<SSoundSource name = "sfx_window_open"
			buffer = "data/audio/interface/15_window_open.wav"
			is3d = "false"
			loopCount="1"
			/>
		<SSoundSource name = "sfx_window_close"
			buffer = "data/audio/interface/16_window_close.wav"
			is3d = "false"
			loopCount="1"
			/>

		<SSoundSource name = "sfx_panel_colapse"
			buffer = "data/audio/interface/19_panel_colapse.wav"
			is3d = "false"
			loopCount="1"
			/>
		<SSoundSource name = "sfx_panel_expand"
			buffer = "data/audio/interface/20_panel_expand.wav"
			is3d = "false"
			loopCount="1"
			/>
		<SSoundSource name = "sfx_panel_pin"
			buffer = "data/audio/interface/22_panel_pin.wav"
			is3d = "false"
			loopCount="1"
			/>
		<SSoundSource name = "sfx_panel_unpin"
			buffer = "data/audio/interface/23_panel_unpin.wav"
			is3d = "false"
			loopCount="1"
			/>
		<SSoundSource name = "sfx_panel_drag_start"
			buffer = "data/audio/interface/24_panel_drag_start.wav"
			is3d = "false"
			loopCount="1"
			/>
		<SSoundSource name = "sfx_panel_drop_success"
			buffer = "data/audio/interface/24_panel_drop_success.wav"
			is3d = "false"
			loopCount="1"
			/>

		<SSoundSource name = "sfx_scroll_loop"
			buffer = "data/audio/interface/25_scroll_bar_loop.wav"
			is3d = "false"
			loopCount="0"
			/>
		<SSoundSource name = "sfx_scroll_click"
			buffer = "data/audio/interface/25_scroll_bar_mouse_click.wav"
			is3d = "false"
			loopCount="1"
			/>
		<SSoundSource name = "sfx_scroll_over"
			buffer = "data/audio/interface/25_scroll_bar_mouse_over.wav"
			is3d = "false"
			loopCount="1"
			/>

		<SSoundSource name = "sfx_radio_click"
			buffer = "data/audio/interface/26_radio_click.wav"
			is3d = "false"
			loopCount="1"
			/>
		<SSoundSource name = "sfx_radio_over"
			buffer = "data/audio/interface/26_radio_over.wav"
			is3d = "false"
			loopCount="1"
			/>

		<SSoundSource name = "sfx_bomber_click"
			buffer = "data/audio/interface/27_bomber_select.wav"
			is3d = "false"
			loopCount="1"
			/>
		<SSoundSource name = "sfx_explorer_click"
			buffer = "data/audio/interface/27_explorer_select.wav"
			is3d = "false"
			loopCount="1"
			/>
		<SSoundSource name = "sfx_fighter_click"
			buffer = "data/audio/interface/27_fighter_select.wav"
			is3d = "false"
			loopCount="1"
			/>
		<SSoundSource name = "sfx_freighter_click"
			buffer = "data/audio/interface/27_freighter_select.wav"
			is3d = "false"
			loopCount="1"
			/>

		<SSoundSource name = "sfx_welcome_aboard"
			buffer = "data/audio/interface/npc_welcome.wav"
			is3d = "false"
			loopCount="1"
			/>

		<SSoundSource name = "sfx_gate_enter"
			buffer = "data/audio/interface/gate_enter.wav"
			is3d = "false"
			loopCount="1"
			/>
		<SSoundSource name = "sfx_gate_exit"
			buffer = "data/audio/interface/gate_exit.wav"
			is3d = "false"
			loopCount="1"
			/>

		<SSoundSource name = "sfx_missile_pursuit_loop"
			buffer = "data/audio/interface/missile_pursuit_loop.wav"
			is3d = "false"
			loopCount="0"
			/>
		<SSoundSource name = "sfx_missile_search_far"
			buffer = "data/audio/interface/missile_search_far_loop.wav"
			is3d = "false"
			loopCount="0"
			/>
		<SSoundSource name = "sfx_missile_search_near"
			buffer = "data/audio/interface/missile_search_near_loop.wav"
			is3d = "false"
			loopCount="0"
			/>
		<SSoundSource name = "sfx_missile_target_lock"
			buffer = "data/audio/interface/missile_target_lock.wav"
			is3d = "false"
			loopCount="1"
			/>

		<SSoundSource name = "sfx_shield_empty"
			buffer = "data/audio/interface/shield_empty.wav"
			is3d = "false"
			loopCount="1"
			/>
		<SSoundSource name = "sfx_shield_full"
			buffer = "data/audio/interface/shield_full.wav"
			is3d = "false"
			loopCount="1"
			/>

		<!-- Special Abilities: BOOST -->
		<SSoundSource name = "sfx_special_boost_full"
			buffer = "data/audio/interface/specials/special_afterburner_charged.wav"
			is3d = "false"
			loopCount="1"
			/>
		<SSoundSource name = "sfx_special_boost_empty"
			buffer = "data/audio/interface/specials/special_afterburner_empty.wav"
			is3d = "false"
			loopCount="1"
			/>

		<!-- Special Abilities: HAZARD SHIELD -->
		<SSoundSource name = "sfx_special_hazard_loop"
			buffer = "data/audio/interface/specials/special_hazardshield_loop.wav"
			is3d = "false"
			loopCount="0"
			/>

		<!-- Special Abilities: RECON MODE -->
		<SSoundSource name = "sfx_special_recon_activate"
			buffer = "data/audio/interface/specials/special_recon_mode_activate.wav"
			is3d = "false"
			loopCount="1"
			/>

		<!-- Special Abilities: VAULT -->
		<SSoundSource name = "sfx_special_vault_open"
			buffer = "data/audio/interface/specials/special_vault_open.wav"
			is3d = "false"
			loopCount="1"
			/>

	</objects>
	
</root>		
