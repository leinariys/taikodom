<?xml version="1.0"?>	

<root
	xmlns="http://scene.gametoolkit.hoplon.com/scene"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://scene.gametoolkit.hoplon.com/scene ../scene.xsd">
  
<includes>
	<include file="data/audio/music/spacer_industrial.mp3"/>
	<include file="data/audio/music/spacer_research.mp3"/>
	<include file="data/audio/music/spacer_military.mp3"/>
	<include file="data/audio/music/spacer_habitat.mp3"/>
	<include file="data/audio/music/belter_industrial.mp3"/>
	<include file="data/audio/music/belter_extract.mp3"/>
	<include file="data/audio/music/belter_habitat.mp3"/>
	<include file="data/audio/music/belter_military.mp3"/>
	<include file="data/audio/music/neutral_habitat.mp3"/>
	<include file="data/audio/music/neutral_conflictarea.mp3"/>
	<include file="data/audio/music/neutral_dangerarea.mp3"/>
	<include file="data/audio/music/neutral_industrial.mp3"/>
	<include file="data/audio/music/neutral_military.mp3"/>  
	<include file = "data/audio/music/main_theme.mp3" />
  
	<include file = "data/audio/music/hellgate.mp3" />
	<include file = "data/audio/music/neverwhere.mp3" />
	<include file = "data/audio/music/ursulla.mp3" />
  
	<include file = "data/audio/music/01_avatar_creation.mp3" />
	<include file = "data/audio/music/02_nursery_loop.ogg" />
	<include file = "data/audio/music/03_inside_station.mp3" />
	<include file = "data/audio/music/04_login_screen.mp3" />
</includes>

<objects>
	<SMultiLayerMusic name = "avatar_creation" >
		<music>
			<SMusic name = "01_avatar_creation" 
				buffer = "data/audio/music/01_avatar_creation.mp3" loopDelay="30.0"/>
		</music>
	</SMultiLayerMusic>

	<SMusic name = "nod_spa_birth_ambience" 
		buffer = "data/audio/music/02_nursery_loop.ogg"/>

	<SMultiLayerMusic name = "inside_station" >
		<music>
			<SMusic name = "03_inside_station" buffer = "data/audio/music/03_inside_station.mp3" loopDelay="85.0"/>
		</music>
	</SMultiLayerMusic>

	<SMultiLayerMusic name = "login_screen" >
		<music>
			<SMusic name = "04_login_screen" 
				buffer = "data/audio/music/04_login_screen.mp3" loopDelay="30.0"/>
		</music>
	</SMultiLayerMusic>

    <SMultiLayerMusic name = "conflict_zone0" >
        <music>
           <SMusic name = "neutral_conflictarea" buffer = "data/audio/music/neutral_conflictarea.mp3" />
        </music>
    </SMultiLayerMusic>

    <SMultiLayerMusic name = "mainMusic" >
      <music>
	   	<SMusic name = "mainMusic_0" buffer = "data/audio/music/main_theme.mp3" />	
      </music>
    </SMultiLayerMusic>

    <SMultiLayerMusic name = "nod_spa_moon_music" >
      <music>
        <SMusic name = "neutral_habitat" buffer = "data/audio/music/neutral_habitat.mp3" />
      </music>
    </SMultiLayerMusic>
    
    <SMultiLayerMusic name = "nod_spa_urano_music" >
      <music>
        <SMusic name = "neutral_habitat" buffer = "data/audio/music/neutral_habitat.mp3" />
      </music>
    </SMultiLayerMusic>
    
    <SMultiLayerMusic name = "nod_spa_earth_music" >
	  <music>
	    <SMusic name = "spacer_research" buffer = "data/audio/music/spacer_research.mp3" />		
	  </music>
    </SMultiLayerMusic>
    
    <SMultiLayerMusic name = "nod_spa_titan_music" >
      <music>
        <SMusic name = "spacer_research" buffer = "data/audio/music/spacer_research.mp3" />
      </music>
    </SMultiLayerMusic>

    <SMultiLayerMusic name = "nod_spa_mars_music" >
      <music>
        <SMusic name = "neutral_industrial" buffer = "data/audio/music/neutral_industrial.mp3" />
      </music>
    </SMultiLayerMusic>

    <SMultiLayerMusic name = "nod_spa_venus_music" >
      <music>
        <SMusic name = "spacer_industrial" buffer = "data/audio/music/spacer_industrial.mp3" />
      </music>
    </SMultiLayerMusic>

    <SMultiLayerMusic name = "nod_spa_mercury_music" >
      <music>
        <SMusic name = "neutral_dangerarea" buffer = "data/audio/music/neutral_dangerarea.mp3" />
      </music>
    </SMultiLayerMusic>


    <SMultiLayerMusic name = "nod_bel_ceres_music" >
      <music>
        <SMusic name = "belter_extract" buffer = "data/audio/music/belter_extract.mp3" />
      </music>
    </SMultiLayerMusic>

    <SMultiLayerMusic name = "nod_spa_neptune_music" >
      <music>
        <SMusic name = "neutral_dangerarea" buffer = "data/audio/music/neutral_dangerarea.mp3" />
      </music>
    </SMultiLayerMusic>

    <SMultiLayerMusic name = "nod_bel_europa_music" >
      <music>
        <SMusic name = "belter_industrial" buffer = "data/audio/music/belter_industrial.mp3" />
      </music>
    </SMultiLayerMusic>

    <SMultiLayerMusic name = "nod_spa_jupiter_music" >
      <music>
        <SMusic name = "spacer_military" buffer = "data/audio/music/spacer_military.mp3" />
      </music>
    </SMultiLayerMusic>

    <SMultiLayerMusic name = "nod_bel_pluton_music" >
      <music>
        <SMusic name = "belter_military" buffer = "data/audio/music/belter_military.mp3" />
      </music>
    </SMultiLayerMusic>
    
    <SMultiLayerMusic name = "nod_bel_sedna_music" >
      <music>
        <SMusic name = "spacer_habitat" buffer = "data/audio/music/spacer_habitat.mp3" />
      </music>
    </SMultiLayerMusic>
    
    <SMultiLayerMusic name = "nod_bel_iceball_music" >
      <music>
        <SMusic name = "belter_extract" buffer = "data/audio/music/belter_extract.mp3" />
      </music>
    </SMultiLayerMusic>    
    
    <SMultiLayerMusic name = "nod_spa_plaza_music" >
      <music>
        <SMusic name = "spacer_research" buffer = "data/audio/music/spacer_research.mp3" />
      </music>
    </SMultiLayerMusic>
    
    <SMultiLayerMusic name = "nod_spa_deadend_music" >
      <music>
        <SMusic name = "belter_habitat" buffer = "data/audio/music/belter_habitat.mp3" />
      </music>
    </SMultiLayerMusic>
    
    <SMultiLayerMusic name = "nod_spa_zerbini_music" >
      <music>
        <SMusic name = "neutral_military" buffer = "data/audio/music/neutral_military.mp3" />
      </music>
    </SMultiLayerMusic>
    
    <SMultiLayerMusic name = "nod_bel_lastchance_music" >
      <music>
        <SMusic name = "spacer_industrial" buffer = "data/audio/music/spacer_industrial.mp3" />
      </music>
    </SMultiLayerMusic>
    
    <SMultiLayerMusic name = "nod_bel_neverwhere_music" >
      <music>
        <SMusic name = "neverwhere" buffer = "data/audio/music/neverwhere.mp3" />
      </music>
    </SMultiLayerMusic>
    
    <SMultiLayerMusic name = "nod_bel_corona_music" >
      <music>
        <SMusic name = "neutral_dangerarea" buffer = "data/audio/music/neutral_dangerarea.mp3" />
      </music>
    </SMultiLayerMusic>
    
    <SMultiLayerMusic name = "nod_xen_hellgate_music" >
      <music>
        <SMusic name = "hellgate" buffer = "data/audio/music/hellgate.mp3" />
      </music>
    </SMultiLayerMusic>

    <SMultiLayerMusic name = "nod_bel_ursulla_music" >
      <music>
        <SMusic name = "ursulla" buffer = "data/audio/music/ursulla.mp3" />
      </music>
    </SMultiLayerMusic>
  </objects>

</root>
