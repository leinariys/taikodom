<?xml version="1.0"?>	

<root
	xmlns="http://scene.gametoolkit.hoplon.com/scene"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://scene.gametoolkit.hoplon.com/scene scene.xsd">
  

<includes>
<include file="data/dummy_data.gr2" />
<include file="data/shaders/loot_shader.pro" />
<include file="data/dummy_sound.wav" />
</includes>

<objects>

<RMesh	name="dummy_mesh"
	shader="loot_transp_pass_shader" 
	mesh="data/dummy_data.gr2/mesh/dummy_mesh" 
	useLighting="0"
	scaling="20 30 60"/>

	
<RGroup name="dummy_group">	
	<child name="dummy_mesh" />
</RGroup>


<SSoundSource 
	name = "dummy_sound" 
	buffer="data/dummy_sound.wav"	      
	distances = "100.0 5000.0" />

</objects>

</root>