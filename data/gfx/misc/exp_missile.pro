<root xmlns="http://scene.gametoolkit.hoplon.com/scene"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://scene.gametoolkit.hoplon.com/scene ../scene.xsd">
    <includes>
        <include file="data/gfx/misc/black_smoke.dds"/>
        <include file="data/gfx/misc/fire_01.dds"/>
        <include file="data/gfx/misc/glow_01.dds"/>
        <include file="data/shaders/particle_shaders.pro"/>
    </includes>
    <objects>
        <RGroup name="gfx_exp_missile"
            allowSelection="0">
            <child index="0"
                name="exp_missile_sparks"/>
            <child index="1"
                name="exp_missile_smoke"/>
            <child index="2"
                name="exp_missile_fire"/>
            <child index="3"
                name="exp_missile_glow"/>
        </RGroup>
        <RParticleSystem name="exp_missile_fire"
            allowSelection="0"
            initialBurst="50"
            initialMaxAngle="180"
            material="data/gfx/misc/fire_01.dds/material"
            maxParticles="15"
            newPartsSecond="5"
            particlesFinalSize="30"
            particlesFinalSizeVariation="5"
            particlesInitialColor="1 0.501961 0 1"
            particlesInitialSize="2"
            particlesLifeTimeVariation="1"
            particlesVelocity="0 0 -40"
            particlesVelocityVariation="10 10 10"
            systemLifeTime="0.1"/>
        <RParticleSystem name="exp_missile_glow"
            allowSelection="0"
            initialBurst="1"
            material="data/gfx/misc/glow_01.dds/material"
            maxParticles="1"
            newPartsSecond="1"
            particlesFinalSize="100"
            particlesInitialColor="1 0.803922 0.607843 1"
            particlesInitialSize="70"
            particlesMaxLifeTime="0.1"
            systemLifeTime="0.1"/>
        <RParticleSystem name="exp_missile_smoke"
            allowSelection="0"
            initialBurst="20"
            initialMaxAngle="180"
            material="exp_missile_smokemat"
            maxParticles="5"
            maxRotationSpeed="30"
            newPartsSecond="5"
            particlesFinalSize="60"
            particlesInitialColor="1 1 1 0.5"
            particlesInitialSize="5"
            particlesMaxLifeTime="3"
            particlesVelocity="0 0 -40"
            particlesVelocityVariation="20 20 20"
            systemLifeTime="0.1"/>
        <Material name="exp_missile_smokemat"
            diffuseTexture="data/gfx/misc/black_smoke.dds"
            normalTexture="Default Resource"
            selfIluminationTexture="Default Resource"
            shader="particle_alpha_shader">
            <texture index="8"
                name="data/gfx/misc/black_smoke.dds"/>
            <texture index="10"
                name="Default Resource"/>
            <texture index="12"
                name="Default Resource"/>
        </Material>
        <RParticleSystem name="exp_missile_sparks"
            alignToMovement="1"
            allowSelection="0"
            initialBurst="50"
            initialMaxAngle="360"
            material="data/gfx/misc/glow_01.dds/material"
            maxParticles="10"
            newPartsSecond="10"
            particlesFinalSize="3"
            particlesInitialColor="1 0.501961 0 1"
            particlesInitialSize="2"
            particlesLifeTimeVariation="0.3"
            particlesMaxLifeTime="0.7"
            particlesVelocity="0 0 -80"
            particlesVelocityVariation="80 80 10"
            systemLifeTime="0.1"/>
    </objects>
</root>
