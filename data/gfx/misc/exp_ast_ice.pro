<root xmlns="http://scene.gametoolkit.hoplon.com/scene"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://scene.gametoolkit.hoplon.com/scene ../scene.xsd">
    <includes>
        <include file="data/gfx/misc/black_smoke.dds"/>
        <include file="data/gfx/misc/glow_01.dds"/>
    </includes>
    <objects>
        <RParticleSystem name="exp_ast_ice_glow"
            allowSelection="0"
            material="data/gfx/misc/glow_01.dds/material"
            maxParticles="1"
            particlesFinalSize="1000"
            particlesInitialSize="5000"
            particlesMaxLifeTime="0.3"
            systemLifeTime="0.1"/>
        <RParticleSystem name="exp_ast_ice_med_glow"
            allowSelection="0"
            material="data/gfx/misc/glow_01.dds/material"
            maxParticles="1"
            particlesFinalSize="500"
            particlesInitialSize="2500"
            particlesMaxLifeTime="0.3"
            systemLifeTime="0.1"/>
        <RParticleSystem name="exp_ast_ice_med_smoke"
            allowSelection="0"
            initialMaxAngle="360"
            material="data/gfx/misc/black_smoke.dds/material"
            maxParticles="3"
            particlesFinalSize="1000"
            particlesFinalSizeVariation="500"
            particlesInitialSize="750"
            particlesLifeTimeVariation="2"
            particlesMaxLifeTime="5"
            systemLifeTime="0.1"/>
        <RParticleSystem name="exp_ast_ice_small_glow"
            allowSelection="0"
            material="data/gfx/misc/glow_01.dds/material"
            maxParticles="1"
            particlesFinalSize="250"
            particlesInitialSize="1250"
            particlesMaxLifeTime="0.3"
            systemLifeTime="0.1"/>
        <RParticleSystem name="exp_ast_ice_small_smoke"
            allowSelection="0"
            initialMaxAngle="360"
            material="data/gfx/misc/black_smoke.dds/material"
            maxParticles="3"
            particlesFinalSize="500"
            particlesFinalSizeVariation="500"
            particlesInitialSize="325"
            particlesLifeTimeVariation="2"
            particlesMaxLifeTime="5"
            systemLifeTime="0.1"/>
        <RParticleSystem name="exp_ast_ice_smoke"
            allowSelection="0"
            initialMaxAngle="360"
            material="data/gfx/misc/black_smoke.dds/material"
            maxParticles="3"
            particlesFinalSize="2000"
            particlesFinalSizeVariation="500"
            particlesInitialSize="1500"
            particlesLifeTimeVariation="2"
            particlesMaxLifeTime="5"
            systemLifeTime="0.1"/>
        <RGroup name="gfx_exp_ast_ice"
            allowSelection="0">
            <child index="0"
                name="exp_ast_ice_glow"/>
            <child index="1"
                name="exp_ast_ice_smoke"/>
        </RGroup>
        <RGroup name="gfx_exp_ast_ice_med"
            allowSelection="0">
            <child index="0"
                name="exp_ast_ice_med_glow"/>
            <child index="1"
                name="exp_ast_ice_med_smoke"/>
        </RGroup>
        <RGroup name="gfx_exp_ast_ice_small"
            allowSelection="0">
            <child index="0"
                name="exp_ast_ice_small_glow"/>
            <child index="1"
                name="exp_ast_ice_small_smoke"/>
        </RGroup>
    </objects>
</root>
