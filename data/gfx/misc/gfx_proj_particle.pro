<root xmlns="http://scene.gametoolkit.hoplon.com/scene"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://scene.gametoolkit.hoplon.com/scene ../scene.xsd">
    <includes>
        <include file="data/gfx/misc/magnetic_head.dds"/>
        <include file="data/gfx/misc/neutron_head.dds"/>
        <include file="data/gfx/misc/part_shot.dds"/>
        <include file="data/gfx/misc/particle_gun_flowershot.dds"/>
        <include file="data/gfx/misc/particle_gun_hit001.dds"/>
        <include file="data/gfx/misc/particle_gun_muzzle001.dds"/>
        <include file="data/gfx/misc/particle_gun_shot001.dds"/>
        <include file="data/gfx/shot/flowershot.gr2"/>
        <include file="data/shaders/particle_shaders.pro"/>
    </includes>
    <objects>
        <RGroup name="gfx_sht_particle_01"
            allowSelection="0"/>
        <RGroup name="gfx_hit_particle"
            allowSelection="0"
            maxVisibleDistance="1500">
            <child index="0"
                name="gfx_hit_particle_part1"/>
            <child index="1"
                name="gfx_hit_particle_part3"/>
        </RGroup>
        <RParticleSystem name="gfx_hit_particle_part1"
            alignment="USER_ALIGNED"
            allowSelection="0"
            followEmitter="1"
            initialBurst="3"
            initialMaxAngle="360"
            material="particle_gun_hit001"
            maxParticles="1"
            newPartsSecond="5"
            particlesFinalColor="0 0 0 1"
            particlesFinalSize="8"
            particlesInitialSize="3"
            particlesLifeTimeVariation="0.025"
            particlesMaxLifeTime="0.1"
            particlesVelocity="0 0 10"
            particlesVelocityVariation="0 0 2.5"
            systemLifeTime="0.125"
            transform="-1 -1.50996e-007 1.50996e-007 0 -1.50996e-007 1 -1.50996e-007 0 -1.50996e-007 -1.50996e-007 -1 0 0 0 0 1"/>
        <RParticleSystem name="gfx_hit_particle_part3"
            allowSelection="0"
            followEmitter="1"
            initialBurst="2"
            initialMaxAngle="360"
            material="particle_magnetic_head_mat"
            maxParticles="5"
            newPartsSecond="20"
            particlesAcceleration="0 0 10"
            particlesFinalSize="10"
            particlesInitialSize="5"
            particlesMaxLifeTime="0.05"
            particlesVelocity="0 0 -10"
            systemLifeTime="0.125"/>
        <RParticleSystem name="gfx_hit_particle_prot_part1"
            alignment="USER_ALIGNED"
            allowSelection="0"
            followEmitter="1"
            initialBurst="3"
            initialMaxAngle="360"
            material="particle_gun_hit001"
            maxParticles="1"
            newPartsSecond="5"
            particlesColorVariation="1 0 0 0"
            particlesFinalColor="1 0 0.501961 0"
            particlesFinalSize="10"
            particlesInitialColor="1 0 0.501961 1"
            particlesInitialSize="3"
            particlesLifeTimeVariation="0.025"
            particlesMaxLifeTime="0.1"
            particlesVelocity="0 0 -10"
            particlesVelocityVariation="0 0 2.5"
            systemLifeTime="0.125"/>
        <RParticleSystem name="gfx_hit_particle_prot_part3"
            allowSelection="0"
            followEmitter="1"
            initialBurst="2"
            initialMaxAngle="360"
            material="particle_magnetic_head_mat"
            maxParticles="5"
            newPartsSecond="20"
            particlesColorVariation="1 0 0.501961 0"
            particlesFinalColor="0.501961 0 1 0"
            particlesFinalSize="10"
            particlesInitialColor="0.501961 0 1 1"
            particlesInitialSize="0"
            particlesMaxLifeTime="0.1"
            particlesVelocity="0 0 -50"
            position="0 0 1.5"
            systemLifeTime="0.125"
            transform="1 0 0 0 -0 1 0 0 0 -0 1 1.5 0 0 0 1"/>
        <RGroup name="gfx_hit_particleprot"
            allowSelection="0"
            maxVisibleDistance="1500">
            <child index="0"
                name="gfx_hit_particle_prot_part1"/>
            <child index="1"
                name="gfx_hit_particle_prot_part3"/>
        </RGroup>
        <RGroup name="gfx_muz_particle"
            allowSelection="0"
            maxVisibleDistance="600">
            <child index="0"
                name="gfx_muz_particle_part1"/>
            <child index="1"
                name="gfx_muz_particle_part2"/>
        </RGroup>
        <RParticleSystem name="gfx_muz_particle_part1"
            alignment="AXIS_ALIGNED"
            allowSelection="0"
            followEmitter="1"
            initialBurst="3"
            initialDelay="0.05"
            material="data/gfx/misc/particle_gun_muzzle001.dds/material"
            maxParticles="5"
            newPartsSecond="100"
            particlesFinalSize="25"
            particlesInitialSize="2.5"
            particlesMaxLifeTime="0.15"
            particlesVelocity="0 0 -150"
            systemLifeTime="0.15"/>
        <RParticleSystem name="gfx_muz_particle_part2"
            allowSelection="0"
            followEmitter="1"
            material="data/gfx/misc/magnetic_head.dds/material"
            maxParticles="1"
            newPartsSecond="100"
            particlesFinalColor="1 1 1 0"
            particlesFinalSize="2.5"
            particlesInitialSize="2.5"
            particlesMaxLifeTime="0.25"
            systemLifeTime="0.15"/>
        <RParticleSystem name="gfx_muz_particle_prot_part1"
            alignment="AXIS_ALIGNED"
            allowSelection="0"
            followEmitter="1"
            initialBurst="3"
            initialDelay="0.05"
            material="data/gfx/misc/particle_gun_muzzle001.dds/material"
            maxParticles="5"
            newPartsSecond="100"
            particlesColorVariation="1 0 0.501961 0"
            particlesFinalColor="0.501961 0 1 0"
            particlesFinalSize="25"
            particlesInitialColor="0.501961 0 1 1"
            particlesInitialSize="2.5"
            particlesMaxLifeTime="0.15"
            particlesVelocity="0 0 -150"
            systemLifeTime="0.15"/>
        <RParticleSystem name="gfx_muz_particle_prot_part2"
            allowSelection="0"
            followEmitter="1"
            material="data/gfx/misc/magnetic_head.dds/material"
            maxParticles="1"
            newPartsSecond="100"
            particlesColorVariation="1 0 0.501961 0"
            particlesFinalColor="0.501961 0 1 0"
            particlesFinalSize="3"
            particlesFinalSizeVariation="1"
            particlesInitialColor="0.501961 0 1 1"
            particlesInitialSize="3"
            particlesInitialSizeVariation="1"
            particlesMaxLifeTime="0.25"
            systemLifeTime="0.15"/>
        <RGroup name="gfx_muz_particleprot"
            allowSelection="0"
            maxVisibleDistance="600">
            <child index="0"
                name="gfx_muz_particle_prot_part1"/>
            <child index="1"
                name="gfx_muz_particle_prot_part2"/>
        </RGroup>
        <RFlowerShot name="gfx_sht_particle"
            material="particle_bodymat"
            maxVisibleDistance="3000"
            shotMesh="data/gfx/shot/flowershot.gr2/mesh/flowershot"
            shotSize="5 5 50"/>
        <RParticleSystem name="gfx_sht_particle_part1"
            alignment="AXIS_ALIGNED"
            allowSelection="0"
            followEmitter="1"
            initialBurst="5"
            material="data/gfx/misc/particle_gun_shot001.dds/material"
            maxParticles="25"
            newPartsSecond="10"
            particlesAcceleration="0 0 150"
            particlesFinalSize="10"
            particlesFinalSizeVariation="2.5"
            particlesInitialSize="25"
            particlesInitialSizeVariation="2.5"
            particlesVelocityVariation="0 0 10"/>
        <RParticleSystem name="gfx_sht_particle_part2"
            allowSelection="0"
            followEmitter="1"
            material="data/gfx/misc/magnetic_head.dds/material"
            particlesAcceleration="0 0 50"
            particlesFinalSize="0"
            particlesFinalSizeVariation="1"
            particlesInitialSize="3"
            particlesInitialSizeVariation="1"
            particlesVelocity="0 0 10"
            position="0 0 -16.5"
            transform="1 0 0 0 -0 1 0 0 0 -0 1 -16.5 0 0 0 1"/>
        <RFlowerShot name="gfx_sht_particleprot"
            material="particle_bodymat"
            maxVisibleDistance="3000"
            primitiveColor="0.501961 0 1 1"
            shotMesh="data/gfx/shot/flowershot.gr2/mesh/flowershot"
            shotSize="5 5 50"/>
        <Material name="particle_bodymat"
            diffuseTexture="data/gfx/misc/particle_gun_flowershot.dds"
            shader="particle_sht_dualblend_shader">
            <texture index="8"
                name="data/gfx/misc/particle_gun_flowershot.dds"/>
        </Material>
        <Material name="particle_gun_hit001"
            diffuseTexture="data/gfx/misc/particle_gun_hit001.dds"
            normalTexture="Default Resource"
            selfIluminationTexture="Default Resource"
            shader="particle_sht_dualblend_shader">
            <texture index="8"
                name="data/gfx/misc/particle_gun_hit001.dds"/>
            <texture index="10"
                name="Default Resource"/>
            <texture index="12"
                name="Default Resource"/>
        </Material>
        <Material name="particle_head_mat"
            diffuseTexture="data/gfx/misc/neutron_head.dds"
            shader="particle_sht_dualblend_shader">
            <texture index="8"
                name="data/gfx/misc/neutron_head.dds"/>
        </Material>
        <Material name="particle_magnetic_head_mat"
            diffuseTexture="data/gfx/misc/magnetic_head.dds"
            normalTexture="Default Resource"
            selfIluminationTexture="Default Resource"
            shader="particle_sht_dualblend_shader">
            <texture index="8"
                name="data/gfx/misc/magnetic_head.dds"/>
            <texture index="10"
                name="Default Resource"/>
            <texture index="12"
                name="Default Resource"/>
        </Material>
    </objects>
</root>
