<root xmlns="http://scene.gametoolkit.hoplon.com/scene"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://scene.gametoolkit.hoplon.com/scene ../scene.xsd">
    <includes>
        <include file="data/gfx/misc/sun_flare_01.dds"/>
        <include file="data/gfx/misc/sun_yellow.dds"/>
        <include file="data/gfx/misc/sun_yellow_ref.dds"/>
    </includes>
    <objects>
        <Shader name="nuclear_shader01">
            <pass index="0"
                name="nuclear_shader_pass01"/>
        </Shader>
        <ShaderPass name="nuclear_shader_pass01"
            blendEnabled="1"
            cullFaceEnabled="0"
            depthMask="0"
            depthRangeMin="1"
            depthTestEnabled="0">
            <channelTextureSet envMode="MODULATE"
                glChannel="0"
                texChannel="8"
                texCoordSet="0"/>
        </ShaderPass>
        <Shader name="planet_shader01">
            <pass index="0"
                name="planet_shader_pass01"/>
        </Shader>
        <ShaderPass name="planet_shader_pass01"
            blendEnabled="1"
            cullFaceEnabled="0"
            depthMask="0"
            depthRangeMin="1">
            <channelTextureSet envMode="MODULATE"
                glChannel="0"
                texChannel="8"
                texCoordSet="0"/>
        </ShaderPass>
        <RGroup name="sun_flare01"
            allowSelection="0">
            <child index="0"
                name="sun_flare_nuclearlight01"/>
        </RGroup>
        <RBillboard name="sun_flare_billboard01"
            lastFrameRendered="234990"
            material="sun_flare_material01"
            shader="planet_shader01"
            size="200 200"/>
        <Material name="sun_flare_material01"
            diffuseTexture="data/gfx/misc/sun_yellow.dds"
            shader="planet_shader01">
            <texture index="8"
                name="data/gfx/misc/sun_yellow.dds"/>
        </Material>
        <RNuclearLight name="sun_flare_nuclearlight01"
            defaultObject="sun_flare_billboard01"
            nuclearBillboard="sun_glow_billboard01"
            objectBillboard="sun_ref_billboard01"
            occlusionRatio="0.7"
            shader="nuclear_shader01"/>
        <RBillboard name="sun_glow_billboard01"
            lastFrameRendered="234990"
            material="sun_glow_material01"
            primitiveColor="1.29863 1.29863 1.29863 1"
            renderPriority="-10"
            shader="nuclear_shader01"
            size="800 800"/>
        <Material name="sun_glow_material01"
            diffuseTexture="data/gfx/misc/sun_flare_01.dds"
            normalTexture="Default Normal Texture"
            selfIluminationTexture="Default Self Illumination Texture"
            shader="nuclear_shader01">
            <texture index="8"
                name="data/gfx/misc/sun_flare_01.dds"/>
           
        </Material>
        <RBillboard name="sun_ref_billboard01"
            lastFrameRendered="234990"
            material="sun_flare_material01"
            occlusionQueryEnabled="1"
            size="100 100"/>
        <Material name="sun_ref_material01"
            diffuseTexture="data/gfx/misc/sun_yellow_ref.dds"
            shader="nuclear_shader01">
            <texture index="8"
                name="data/gfx/misc/sun_yellow_ref.dds"/>
        </Material>
    </objects>
</root>
