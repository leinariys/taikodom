<root xmlns="http://scene.gametoolkit.hoplon.com/scene"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://scene.gametoolkit.hoplon.com/scene ../scene.xsd">
    <includes>
        <include file="data/gfx/misc/gfx_proj_ballistic.pro"/>
        <include file="data/gfx/misc/gfx_proj_blaster.pro"/>
        <include file="data/gfx/misc/gfx_proj_gauss.pro"/>
        <include file="data/gfx/misc/gfx_proj_magnetic.pro"/>
        <include file="data/gfx/misc/gfx_proj_neutron.pro"/>
        <include file="data/gfx/misc/gfx_proj_particle.pro"/>
        <include file="data/gfx/misc/gfx_proj_plasma.pro"/>
        <include file="data/gfx/misc/gfx_proj_rail.pro"/>
        <include file="data/gfx/misc/gfx_proj_scatter.pro"/>
    </includes>
</root>
