<root xmlns="http://scene.gametoolkit.hoplon.com/scene"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://scene.gametoolkit.hoplon.com/scene ../scene.xsd">
    <includes>
        <include file="data/gfx/trail/trail_blue.dds"/>
        <include file="data/gfx/trail/traildot_blue.dds"/>
        <include file="data/maps/fig_spa_barracuda/fig_spa_barracuda_dfs.dds"/>
        <include file="data/maps/fig_spa_barracuda/fig_spa_barracuda_glo.dds"/>
        <include file="data/maps/fig_spa_barracuda/fig_spa_barracuda_nrm.dds"/>
        <include file="data/meshes/fig_spa_barracuda/fig_spa_barracuda.gr2"/>
        <include file="data/meshes/fig_spa_barracuda/fig_spa_barracuda_lod1.gr2"/>
        <include file="data/meshes/fig_spa_barracuda/fig_spa_barracuda_lod2.gr2"/>
        <include file="data/shaders/light_shader.pro"/>
        <include file="data/trails.pro"/>
        <include file="data/gfx/engine_emmiters/main_engine_emmiter_fig.pro"/>
    </includes>
    <objects>
        <RGroup name="fig_spa_barracuda"
            allowSelection="0">
            <child index="0"
                name="fig_spa_barracuda_LODS"/>
        </RGroup>
        <RLod name="fig_spa_barracuda_LODS"
            distanceLod0="120"
            distanceLod1="240"
            distanceLod2="480"
            objectLod0="fig_spa_barracuda_lod0"
            objectLod1="fig_spa_barracuda_lod1"
            objectLod2="fig_spa_barracuda_lod2"/>
        <RMesh name="fig_spa_barracuda_lod0"
            lastFrameRendered="100175"
            material="material_fig_spa_barracuda"
            mesh="data/meshes/fig_spa_barracuda/fig_spa_barracuda.gr2/mesh/fig_spa_barracuda"/>
        <RMesh name="fig_spa_barracuda_lod1"
            lastFrameRendered="1467"
            material="material_fig_spa_barracuda"
            mesh="data/meshes/fig_spa_barracuda/fig_spa_barracuda_lod1.gr2/mesh/fig_spa_barracuda_lod1"/>
        <RMesh name="fig_spa_barracuda_lod2"
            lastFrameRendered="402"
            material="material_fig_spa_barracuda"
            mesh="data/meshes/fig_spa_barracuda/fig_spa_barracuda_lod2.gr2/mesh/fig_spa_barracuda_lod2"/>
        <RTrail name="fig_spa_barracuda_trail_blue"
            billboardMaterial="data/gfx/trail/traildot_blue.dds/material"
            billboardSize="0 0"
            lastFrameRendered="82892"
            lifeTime="1"
            material="data/gfx/trail/trail_blue.dds/material"
            minSpeed="0.1"
            trailWidth="3.2"
            useLighting="0">
            <child index="0"
                name="main_engine_emmiter_fig_med_blue"/>
        </RTrail>
        <RTrail name="fig_spa_barracuda_trail_blue_med"
            billboardMaterial="data/gfx/trail/traildot_blue.dds/material"
            billboardSize="0 0"
            lastFrameRendered="82892"
            lifeTime="0.8"
            material="data/gfx/trail/trail_blue.dds/material"
            minSpeed="0.1"
            trailWidth="1.5"
            useLighting="0">
            <child index="0"
                name="main_engine_emmiter_fig_aux_blue"/>
        </RTrail>
        <RTrail name="fig_spa_barracuda_trail_blue_small"
            billboardMaterial="data/gfx/trail/traildot_blue.dds/material"
            billboardSize="0 0"
            lastFrameRendered="82892"
            lifeTime="0.6"
            material="data/gfx/trail/trail_blue.dds/material"
            minSpeed="0.1"
            trailWidth="0.8"
            useLighting="0">
            <child index="0"
                name="main_engine_emmiter_fig_aux_blue"/>
        </RTrail>
        <FloatParameter name="fresnelExp_material_fig_spa_barracuda"
            locationId="2"
            parameterName="fresnelExp"
            value="1"/>
        <Material name="material_fig_spa_barracuda"
            diffuseTexture="data/maps/fig_spa_barracuda/fig_spa_barracuda_dfs.dds"
            normalTexture="data/maps/fig_spa_barracuda/fig_spa_barracuda_nrm.dds"
            selfIluminationTexture="data/maps/fig_spa_barracuda/fig_spa_barracuda_glo.dds"
            shader="cubemap_main_light_shader">
            <shaderParameter index="0"
                name="fresnelExp_material_fig_spa_barracuda"/>
            <texture index="8"
                name="data/maps/fig_spa_barracuda/fig_spa_barracuda_dfs.dds"/>
            <texture index="10"
                name="data/maps/fig_spa_barracuda/fig_spa_barracuda_glo.dds"/>
            <texture index="12"
                name="data/maps/fig_spa_barracuda/fig_spa_barracuda_nrm.dds"/>            
        </Material>
    </objects>
</root>
