<root xsi:schemaLocation="http://scene.gametoolkit.hoplon.com/scene ../scene.xsd" xmlns="http://scene.gametoolkit.hoplon.com/scene" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <includes>
        <include file="data/gfx/misc/dotsmoke.dds" />
        <include file="data/maps/amm_min_contramed1/amm_min_contramed1_dfs_01.dds" />
        <include file="data/maps/amm_min_contramed1/amm_min_contramed1_glo_01.dds" />
        <include file="data/maps/amm_min_contramed1/amm_min_contramed1_nrm_01.dds" />
        <include file="data/meshes/amm_min_contramed1/amm_min_contramed1.gr2" />
        <include file="data/meshes/amm_min_contramed1/amm_min_contramed1_lod1.gr2" />
        <include file="data/meshes/amm_min_contramed1/amm_min_contramed1_lod2.gr2" />
        <include file="data/shaders/light_shader.pro" />
        <include file="data/trails.pro" />
    </includes>
    <objects>
        <RParticleSystem allowSelection="0" lastFrameRendered="81757" material="data/gfx/misc/dotsmoke.dds/material" name="smoke_part_02" newPartsSecond="10" particlesFinalColor="1 1 1 0" particlesFinalSize="4" particlesFinalSizeVariation="2" particlesInitialColor="0.501961 1 1 1" particlesMaxLifeTime="0.5" particlesVelocity="2 0 0" particlesVelocityVariation="2 2 2" position="0 5 0" />
        <RParticleSystem allowSelection="0" lastFrameRendered="81757" material="data/gfx/misc/dotsmoke.dds/material" name="smoke_part_01" newPartsSecond="10" particlesFinalColor="1 1 1 0" particlesFinalSize="4" particlesFinalSizeVariation="2" particlesInitialColor="0.501961 1 1 1" particlesMaxLifeTime="0.5" particlesVelocity="2 0 0" particlesVelocityVariation="2 2 2" position="0 -5 0" />
        <RGroup allowSelection="0" name="amm_min_contramed1">
            <child name="rotor_contramed1" />
        </RGroup>
        <RMesh lastFrameRendered="81757" material="material_amm_min_contramed1_01" mesh="data/meshes/amm_min_contramed1/amm_min_contramed1.gr2/mesh/amm_min_contramed1" name="amm_min_contramed1_01" />
      
        <RLod distanceLod0="50" distanceLod1="200" name="amm_min_contramed1_LODS" objectLod0="amm_min_contramed1_lod0" objectLod1="amm_min_contramed1_lod1" />
      
        <RGroup name="amm_min_contramed1_lod0">
            <child name="amm_min_contramed1_01" />
        </RGroup>
        <RGroup name="amm_min_contramed1_lod1">
            <child name="amm_min_contramed1_01" />
        </RGroup>
        <RMesh name="amm_min_contramed1_lod1_01" />
        <RGroup name="amm_min_contramed1_lod2">
            <child name="amm_min_contramed1_01" />
        </RGroup>
        <RMesh name="amm_min_contramed1_lod2_01" />
        <FloatParameter locationId="2" name="fresnelExp_material_amm_min_contramed1" parameterName="fresnelExp" value="1" />
        <Material diffuseTexture="data/maps/amm_min_contramed1/amm_min_contramed1_dfs_01.dds" name="material_amm_min_contramed1_01" normalTexture="data/maps/amm_min_contramed1/amm_min_contramed1_nrm_01.dds" selfIluminationTexture="data/maps/amm_min_contramed1/amm_min_contramed1_glo_01.dds" shader="cubemap_light_shader">
            <shaderParameter name="fresnelExp_material_amm_min_contramed1" />
            <texture name="data/maps/amm_min_contramed1/amm_min_contramed1_dfs_01.dds" />
            <texture name="data/maps/amm_min_contramed1/amm_min_contramed1_glo_01.dds" />
            <texture name="data/maps/amm_min_contramed1/amm_min_contramed1_nrm_01.dds" />
        </Material>
        <RRotor angularVelocity="10 50 200" name="rotor_contramed1">
            <child name="amm_min_contramed1_LODS" />
            <child name="smoke_part_02" />
            <child name="smoke_part_01" />
        </RRotor>
    </objects>
</root>