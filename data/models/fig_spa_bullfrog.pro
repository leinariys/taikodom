<root xmlns="http://scene.gametoolkit.hoplon.com/scene"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://scene.gametoolkit.hoplon.com/scene ../scene.xsd">
    <includes>
        <include file="data/gfx/engine_emmiters/main_engine_emmiter_fig.pro"/>
        <include file="data/gfx/trail/trail_blue.dds"/>
        <include file="data/gfx/trail/traildot_blue.dds"/>
        <include file="data/maps/fig_spa_bullfrog/fig_spa_bullfrog_dfs.dds"/>
        <include file="data/maps/fig_spa_bullfrog/fig_spa_bullfrog_glo.dds"/>
        <include file="data/maps/fig_spa_bullfrog/fig_spa_bullfrog_nrm.dds"/>
        <include file="data/maps/fig_spa_bullfrog/fig_spa_bullfrog_submaps_dfs.dds"/>
        <include file="data/maps/fig_spa_bullfrog/fig_spa_bullfrog_submaps_glo.dds"/>
        <include file="data/maps/fig_spa_bullfrog/fig_spa_bullfrog_submaps_nrm.dds"/>
        <include file="data/meshes/fig_spa_bullfrog/fig_spa_bullfrog.gr2"/>
        <include file="data/meshes/fig_spa_bullfrog/fig_spa_bullfrog_lod1.gr2"/>
        <include file="data/meshes/fig_spa_bullfrog/fig_spa_bullfrog_lod2.gr2"/>
        <include file="data/shaders/light_shader.pro"/>
        <include file="data/trails.pro"/>
    </includes>
    <objects>
        <RGroup name="fig_spa_bullfrog"
            allowSelection="0">
            <child index="0"
                name="fig_spa_bullfrog_LODS"/>
        </RGroup>
        <RMesh name="fig_spa_bullfrog_01"
            material="material_fig_spa_bullfrog"
            mesh="data/meshes/fig_spa_bullfrog/fig_spa_bullfrog.gr2/mesh/0"/>
        <RMesh name="fig_spa_bullfrog_02"
            material="material_fig_spa_bullfrog_submaps"
            mesh="data/meshes/fig_spa_bullfrog/fig_spa_bullfrog.gr2/mesh/1"/>
        <RMesh name="fig_spa_bullfrog_03"
            material="material_fig_spa_bullfrog_submaps"
            mesh="data/meshes/fig_spa_bullfrog/fig_spa_bullfrog.gr2/mesh/2"/>
        <RLod name="fig_spa_bullfrog_LODS"
            distanceLod0="200"
            distanceLod1="400"
            distanceLod2="600"
            objectLod0="fig_spa_bullfrog_lod0"
            objectLod1="fig_spa_bullfrog_lod1"
            objectLod2="fig_spa_bullfrog_lod2"/>
        <RGroup name="fig_spa_bullfrog_lod0">
            <child index="0"
                name="fig_spa_bullfrog_01"/>
            <child index="1"
                name="fig_spa_bullfrog_02"/>
            <child index="2"
                name="fig_spa_bullfrog_03"/>
        </RGroup>
        <RGroup name="fig_spa_bullfrog_lod1">
            <child index="0"
                name="fig_spa_bullfrog_lod1_01"/>
            <child index="1"
                name="fig_spa_bullfrog_lod1_02"/>
            <child index="2"
                name="fig_spa_bullfrog_lod1_03"/>
        </RGroup>
        <RMesh name="fig_spa_bullfrog_lod1_01"
            material="material_fig_spa_bullfrog"
            mesh="data/meshes/fig_spa_bullfrog/fig_spa_bullfrog_lod1.gr2/mesh/0"/>
        <RMesh name="fig_spa_bullfrog_lod1_02"
            material="material_fig_spa_bullfrog_submaps"
            mesh="data/meshes/fig_spa_bullfrog/fig_spa_bullfrog_lod1.gr2/mesh/1"/>
        <RMesh name="fig_spa_bullfrog_lod1_03"
            material="material_fig_spa_bullfrog_submaps"
            mesh="data/meshes/fig_spa_bullfrog/fig_spa_bullfrog_lod1.gr2/mesh/2"/>
        <RGroup name="fig_spa_bullfrog_lod2">
            <child index="0"
                name="fig_spa_bullfrog_lod2_01"/>
            <child index="1"
                name="fig_spa_bullfrog_lod2_02"/>
            <child index="2"
                name="fig_spa_bullfrog_lod2_03"/>
        </RGroup>
        <RMesh name="fig_spa_bullfrog_lod2_01"
            material="material_fig_spa_bullfrog"
            mesh="data/meshes/fig_spa_bullfrog/fig_spa_bullfrog_lod2.gr2/mesh/0"/>
        <RMesh name="fig_spa_bullfrog_lod2_02"
            material="material_fig_spa_bullfrog_submaps"
            mesh="data/meshes/fig_spa_bullfrog/fig_spa_bullfrog_lod2.gr2/mesh/1"/>
        <RMesh name="fig_spa_bullfrog_lod2_03"
            material="material_fig_spa_bullfrog_submaps"
            mesh="data/meshes/fig_spa_bullfrog/fig_spa_bullfrog_lod2.gr2/mesh/2"/>
        <RTrail name="fig_spa_bullfrog_trail_blue"
            billboardMaterial="data/gfx/trail/traildot_blue.dds/material"
            billboardSize="3 3"
            lifeTime="3"
            material="data/gfx/trail/trail_blue.dds/material"
            minSpeed="0.1"
            trailWidth="1.5"
            useLighting="0">
            <child index="0"
                name="main_engine_emmiter_fig_med_blue"/>
        </RTrail>
        <RTrail name="fig_spa_bullfrog_trail_blue_small"
            billboardMaterial="data/gfx/trail/traildot_blue.dds/material"
            billboardSize="3 3"
            lifeTime="2"
            material="data/gfx/trail/trail_blue.dds/material"
            minSpeed="0.1"
            trailWidth="0.8"
            useLighting="0">
            <child index="0"
                name="main_engine_emmiter_fig_aux_blue"/>
        </RTrail>
        <FloatParameter name="fresnelExp_material_fig_spa_bullfrog"
            locationId="2"
            parameterName="fresnelExp"
            value="1"/>
        <FloatParameter name="fresnelExp_material_fig_spa_bullfrog_submaps"
            locationId="2"
            parameterName="fresnelExp"
            value="1"/>
        <Material name="material_fig_spa_bullfrog"
            diffuseTexture="data/maps/fig_spa_bullfrog/fig_spa_bullfrog_dfs.dds"
            normalTexture="data/maps/fig_spa_bullfrog/fig_spa_bullfrog_nrm.dds"
            selfIluminationTexture="data/maps/fig_spa_bullfrog/fig_spa_bullfrog_glo.dds"
            shader="cubemap_main_light_shader"
            shininess="8">
            <shaderParameter index="0"
                name="fresnelExp_material_fig_spa_bullfrog"/>
            <texture index="8"
                name="data/maps/fig_spa_bullfrog/fig_spa_bullfrog_dfs.dds"/>
            <texture index="10"
                name="data/maps/fig_spa_bullfrog/fig_spa_bullfrog_glo.dds"/>
            <texture index="12"
                name="data/maps/fig_spa_bullfrog/fig_spa_bullfrog_nrm.dds"/>
            <texture index="16"
                name="data/maps/fig_spa_bullfrog/fig_spa_bullfrog_dfs.dds"/>
            <texture index="17"
                name="data/maps/fig_spa_bullfrog/fig_spa_bullfrog_glo.dds"/>
            <texture index="18"
                name="data/maps/fig_spa_bullfrog/fig_spa_bullfrog_nrm.dds"/>
        </Material>
        <Material name="material_fig_spa_bullfrog_submaps"
            diffuseTexture="data/maps/fig_spa_bullfrog/fig_spa_bullfrog_submaps_dfs.dds"
            normalTexture="data/maps/fig_spa_bullfrog/fig_spa_bullfrog_submaps_nrm.dds"
            selfIluminationTexture="data/maps/fig_spa_bullfrog/fig_spa_bullfrog_submaps_glo.dds"
            shader="cubemap_main_light_shader"
            shininess="8">
            <shaderParameter index="0"
                name="fresnelExp_material_fig_spa_bullfrog_submaps"/>
            <texture index="8"
                name="data/maps/fig_spa_bullfrog/fig_spa_bullfrog_submaps_dfs.dds"/>
            <texture index="10"
                name="data/maps/fig_spa_bullfrog/fig_spa_bullfrog_submaps_glo.dds"/>
            <texture index="12"
                name="data/maps/fig_spa_bullfrog/fig_spa_bullfrog_submaps_nrm.dds"/>
            <texture index="16"
                name="data/maps/fig_spa_bullfrog/fig_spa_bullfrog_submaps_dfs.dds"/>
            <texture index="17"
                name="data/maps/fig_spa_bullfrog/fig_spa_bullfrog_submaps_glo.dds"/>
            <texture index="18"
                name="data/maps/fig_spa_bullfrog/fig_spa_bullfrog_submaps_nrm.dds"/>
        </Material>
    </objects>
</root>
