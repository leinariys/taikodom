<?xml version="1.0"?>	

<root
	xmlns="http://scene.gametoolkit.hoplon.com/scene"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://scene.gametoolkit.hoplon.com/scene scene.xsd">
  
  <includes>
    <include file="data/gfx/trail/traildot_blue.dds"/>
    <include file="data/gfx/trail/traildot_red.dds"/>
    <include file="data/gfx/trail/traildot_green.dds"/>
    <include file="data/gfx/trail/traildot_yellow.dds"/>
    <include file="data/gfx/trail/trail_blue.dds"/>
    <include file="data/gfx/trail/trail_red.dds"/>
    <include file="data/gfx/trail/trail_purp.dds"/>
    <include file="data/gfx/trail/trail_green.dds"/>
    <include file="data/gfx/trail/trail_yellow.dds"/>
    <include file="data/gfx/trail/trail_redx.dds" />
  </includes>
  
  <objects>
    <RTrail name="trail_purp"
          billboardSize="3 3"
          material="data/gfx/trail/trail_purp.dds/material"
          billboardMaterial="data/gfx/trail/traildot_blue.dds/material"
          primitiveColor="1 1 1 1"
          trailMaterial="data/gfx/trail/trail_purp.dds/material"
          useLighting="0"
          trailWidth="10"
          lifeTime="10"
          minSpeed="0.1"/>
  	
      <RTrail name="trail_blue"
          billboardSize="3 3"
          material="data/gfx/trail/trail_blue.dds/material"
          billboardMaterial="data/gfx/trail/traildot_blue.dds/material"
          primitiveColor="1 1 1 1"
          trailMaterial="data/gfx/trail/trail_blue.dds/material"
          useLighting="0"
          trailWidth="3"
          lifeTime="10"
          minSpeed="0.1"/>
          
      <RTrail name="trail_redx"
          billboardSize="3 3"
          material="data/gfx/trail/trail_redx.dds/material"
          billboardMaterial="data/gfx/trail/traildot_red.dds/material"
          primitiveColor="1 1 1 1"
          trailMaterial="data/gfx/trail/trail_redx.dds/material"
          useLighting="0"
          trailWidth="3"
          lifeTime="10"
          minSpeed="0.1"/>
  	
      <RTrail name="trail_red"
          billboardSize="3 3"
          material="data/gfx/trail/trail_red.dds/material"
          billboardMaterial="data/gfx/trail/traildot_red.dds/material"
          primitiveColor="1 1 1 1"
          trailMaterial="data/gfx/trail/trail_red.dds/material"
          useLighting="0"
          trailWidth="10"
          lifeTime="10"
          minSpeed="0.1"/>
  	
      <RTrail name="trail_green"
          billboardSize="3 3"
          material="data/gfx/trail/trail_green.dds/material"
          billboardMaterial="data/gfx/trail/traildot_green.dds/material"
          primitiveColor="1 1 1 1"
          trailMaterial="data/gfx/trail/trail_green.dds/material"
          useLighting="0"
          trailWidth="10"
          lifeTime="10"
          minSpeed="0.1"/>
  	
      <RTrail name="trail_yellow"
          billboardSize="3 3"
          material="data/gfx/trail/trail_yellow.dds/material"
          billboardMaterial="data/gfx/trail/traildot_yellow.dds/material"
          primitiveColor="1 1 1 1"
          trailMaterial="data/gfx/trail/trail_yellow.dds/material"
          useLighting="0"
          trailWidth="10"
          lifeTime="10"
          minSpeed="0.1"/>		
  </objects>
</root>
