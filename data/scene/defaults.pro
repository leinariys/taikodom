<?xml version="1.0"?>	

<root
	xmlns="http://scene.gametoolkit.hoplon.com/scene"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://scene.gametoolkit.hoplon.com/scene ../scene.xsd">
  
    <includes>
        <include file="data/starbody/misc/stardust_dust.dds"/>
        <include file="data/starbody/misc/stardust_dust_tracer.dds"/>
    </includes>
    <objects>
        <RSpacedust name="default_spacedust"
            billboardSize="100 100"
            material="data/starbody/misc/stardust_dust.dds/material"
            regionSize="500"
            fadeOut="0.6"
            fadeIn="0.3"
            primitiveColor="0.2 0.3 0.3 0.2"/>
  
          <RSpacedust name="default_spacedust_tracer"
            billboardSize="6 0.18"
            fadeIn="0.3"
            fadeOut="0.6"
            lastFrameRendered="138933"
            material="data/starbody/misc/stardust_dust_tracer.dds/material"
            primitiveColor="0.8 0.8 0.8 0.8"
	    	numBillboards="25"
            regionSize="350"/>
  	</objects>            
</root>
