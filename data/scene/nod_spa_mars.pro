<root xmlns="http://scene.gametoolkit.hoplon.com/scene"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://scene.gametoolkit.hoplon.com/scene ../scene.xsd">
    <includes>
        <include file="data/cubemap/nod_spa_mars_cubemap.dds"/>
        <include file="data/cubemap/nod_spa_mars_cubemap_diffuse.dds"/>
        <include file="data/gfx/misc/sun_flare_01.pro"/>
        <include file="data/scene/defaults.pro"/>
        <include file="data/shaders/default_panorama_shader.pro"/>
        <include file="data/starbody/misc/stars.dds"/>
        <include file="data/starbody/misc/stb_misc_skybox.gr2"/>
        <include file="data/starbody/skybox/pan_stb_type06.dds"/>
        <include file="data/starbody/solar_sys/planet/pla_sol_mars.pro"/>
    </includes>
    <objects>
        <RSceneAreaInfo name="nod_bel_mars1_info"
            ambientColor="0.180392 0.180392 0.180392 1"
            diffuseCubemap="data/cubemap/nod_spa_mars_cubemap_diffuse.dds"
            fogColor="0.0627451 0.0941176 0.0627451 1"
            fogEnd="80000"
            fogStart="10000"
            reflectiveCubemap="data/cubemap/nod_spa_mars_cubemap.dds"/>
        <RGroup name="nod_spa_mars"
            allowSelection="0">
            <child index="0"
                name="nod_spa_mars1_panorama"/>
            <child index="1"
                name="nod_spa_mars1_spacedust"/>
            <child index="2"
                name="nod_bel_mars1_info"/>
        </RGroup>
        <RLight mainLight="true"  name="nod_spa_mars1_light1"
            allowSelection="0"
            cutoff="0.781298"
            diffuseColor="0.8 0.976471 0.905882 0"
            scaling="1 1.00001 1.00001"
            transform="0.212862 -0.808742 -0.548315 0 -0.912905 0.0354161 -0.406643 0 0.348281 0.587117 -0.730763 0 0 0 0 1"
            type="2"/>
        <RGroup name="nod_spa_mars1_mars"
            position="-100 120 1500"
            transform="1 0 0 -100 -0 1 0 120 0 -0 1 1500 0 0 0 1">
            <child index="0"
                name="mars_mesh"/>
        </RGroup>
        <RPanorama name="nod_spa_mars1_panorama"
            position="40 20 40"
            shader="Default Resource"
            transform="1 0 0 40 -0 1 0 20 0 -0 1 40 0 0 0 1">
            <child index="0"
                name="nod_spa_mars1_mars"/>
            <child index="1"
                name="nod_spa_mars1_skybox"/>
            <child index="2"
                name="nod_spa_mars1_light1"/>
            <child index="3"
                name="nod_spa_mars1_sunflare"/>
        </RPanorama>
        <RMesh name="nod_spa_mars1_skybox"
            lastFrameRendered="93848"
            material="nod_spa_mars1_skyboxmat"
            mesh="data/starbody/misc/stb_misc_skybox.gr2/mesh/pan_msh"
            renderPriority="100"
            scaling="1 1 1"
            transform="0.906308 1.36848e-007 0.422618 0 2.14809e-007 -1 -1.36849e-007 0 0.422618 2.14809e-007 -0.906308 0 0 0 0 1"/>
        <Material name="nod_spa_mars1_skyboxmat"
            diffuseDetailTexture="nod_spa_marte1_startile"
            diffuseTexture="data/starbody/skybox/pan_stb_type06.dds"
            normalTexture="Default Resource"
            selfIluminationTexture="Default Resource"
            shader="panorama_shader">
            <texture index="8"
                name="data/starbody/skybox/pan_stb_type06.dds"/>
            <texture index="9"
                name="nod_spa_marte1_startile"/>
            <texture index="10"
                name="Default Resource"/>
            <texture index="12"
                name="Default Resource"/>
        </Material>
        <RSpacedust name="nod_spa_mars1_spacedust"
            billboardSize="100 100"
            fadeIn="0.3"
            fadeOut="0.6"
            lastFrameRendered="93848"
            material="data/starbody/misc/stardust_dust.dds/material"
            primitiveColor="0.568627 0.74902 0.662745 0.2"
            regionSize="500"/>
        <RGroup name="nod_spa_mars1_sunflare"
            position="800 0 -100"
            transform="1 0 0 800 -0 1 0 0 0 -0 1 -100 0 0 0 1">
            <child index="0"
                name="sun_flare01"/>
        </RGroup>
        <TiledTexture name="nod_spa_marte1_startile"
            texture="data/starbody/misc/stars.dds"
            tile="10 10 10"/>
    </objects>
</root>
