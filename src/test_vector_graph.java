import com.hoplon.geometry.Vec3f;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Отслеживание судов
 * class wx
 * Построение крафака движение коробля, с постепенным построением
 */
public class test_vector_graph {
    public static test_vector_graph bDN;
    public static JFrame BE;
    static Vec3f line_force2 = new Vec3f();
    static Vec3f line_des = new Vec3f();
    static Vec3f line_cur = new Vec3f();
    static Vec3f line_x = new Vec3f();
    static Vec3f line_y = new Vec3f();
    static Vec3f line_z = new Vec3f();
    static String title_text = "";
    private c bDL = new c();
    /**
     * Шаг построения навых ыекторов
     * milliseconds
     */
    private Timer timer_gr = new Timer(1000, new ActionListener() {
        public void actionPerformed(ActionEvent var1) {
            test_vector_graph.this.repaint();
        }
    });

    public test_vector_graph() {
        this.timer_gr.start();
    }

    public static void main(String[] var0) throws IOException {
  /*    Thread var1 = new Thread()
      {
         public void run() {

            SwingUtilities.invokeLater(new Runnable()
            {
               public void run()
               {
                  try {
                     UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                  } catch (Exception var1) {
                  }
*/
        test_vector_graph.BE = new JFrame("ShipTrack");
        //Этот setDefaultCloseOperation()метод используется для указания одной из нескольких опций кнопки закрытия.
        // Используйте одну из следующих констант, чтобы указать свой выбор:
        test_vector_graph.BE.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//выход из приложения
        test_vector_graph.bDN = new test_vector_graph();
        test_vector_graph.bDN.a(test_vector_graph.BE.getContentPane());
        test_vector_graph.BE.pack();
        test_vector_graph.BE.setVisible(true);
            /*   }
            });

         }
      };

      var1.setDaemon(true);
      var1.start();*/
        LineNumberReader var2 = new LineNumberReader(new FileReader("test.txt"));
        Pattern var3 = Pattern.compile("cur=([E0-9.-]+), ([E0-9.-]+), ([E0-9.-]+) des=([E0-9.-]+), ([E0-9.-]+), ([E0-9.-]+) force=([E0-9.-]+), ([E0-9.-]+), ([E0-9.-]+) ([E0-9.-]+), ([E0-9.-]+), ([E0-9.-]+) X=([E0-9.-]+), ([E0-9.-]+), ([E0-9.-]+) Y=([E0-9.-]+), ([E0-9.-]+), ([E0-9.-]+) Z=([E0-9.-]+), ([E0-9.-]+), ([E0-9.-]+) yawAng=([E0-9.-]+) pitchAng=([E0-9.-]+)");

        String var4;
        while ((var4 = var2.readLine()) != null) {
            Matcher var5 = var3.matcher(var4);
            if (var5.matches()) {
                line_cur = new Vec3f(Float.parseFloat(var5.group(1)), Float.parseFloat(var5.group(2)), Float.parseFloat(var5.group(3)));//cur
                line_des = new Vec3f(Float.parseFloat(var5.group(4)), Float.parseFloat(var5.group(5)), Float.parseFloat(var5.group(6)));//des
                line_force2 = new Vec3f(Float.parseFloat(var5.group(10)), Float.parseFloat(var5.group(11)), Float.parseFloat(var5.group(12)));//force 2
                line_x = new Vec3f(Float.parseFloat(var5.group(13)), Float.parseFloat(var5.group(14)), Float.parseFloat(var5.group(15)));//X
                line_y = new Vec3f(Float.parseFloat(var5.group(16)), Float.parseFloat(var5.group(17)), Float.parseFloat(var5.group(18)));//Y
                line_z = new Vec3f(Float.parseFloat(var5.group(19)), Float.parseFloat(var5.group(20)), Float.parseFloat(var5.group(21)));//Z
                title_text = var4.replaceAll("\\) ", ")\n");

                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        test_vector_graph.bDN.repaint();
                    }
                });
            }
        }
    }

    public void repaint() {
        this.bDL.repaint();
    }

    /**
     * @param var1 Панель бля отричовки
     */
    public void a(Container var1) {
        var1.setLayout(new BoxLayout(var1, 3));
        this.bDL = new c();
        var1.add(this.bDL);
        this.bDL.setAlignmentX(0.0F);
    }

    class c extends JComponent {
        private final float ifF = 0.70710677F;
        private final float ifG = 0.033333335F;
        private final float ifH = 20.0F;
        private final float ifI = 20.0F;
        Dimension preferredSize = new Dimension(600, 600);
        Color gridColor;
        int ifJ;
        int ifK;

        public c() {
            this.setBackground(Color.WHITE);
            this.setOpaque(true);
        }

        public Dimension getPreferredSize() {
            return this.preferredSize;
        }

        protected void paintComponent(Graphics var1) {
            if (this.isOpaque()) {
                var1.setColor(this.getBackground());
                var1.fillRect(0, 0, this.getWidth(), this.getHeight());
            }

            var1.setColor(Color.BLACK);
            int var2 = (Math.min(this.getSize().width, this.getSize().height) - 100) / 2;
            this.a(var1, new Vec3f((float) (-var2) / ifG, 0.0F, 0.0F), new Vec3f((float) var2 / ifG, 0.0F, 0.0F), "X");
            this.a(var1, new Vec3f(0.0F, (float) (-var2) / ifG, 0.0F), new Vec3f(0.0F, (float) var2 / ifG, 0.0F), "Y");
            this.a(var1, new Vec3f(0.0F, 0.0F, (float) (-var2) / ifG), new Vec3f(0.0F, 0.0F, (float) var2 / ifG), "Z");
            var1.setColor(Color.RED);
            Vec3f var3 = new Vec3f(test_vector_graph.line_force2);
            var3.scale(10.0F);
            this.a(var1, var3);
            var1.setColor(Color.BLUE);
            Vec3f var4 = new Vec3f(test_vector_graph.line_x);
            var4.scale(500.0F);
            this.a(var1, var3, var3.i(var4), "x");
            var4 = new Vec3f(test_vector_graph.line_y);
            var4.scale(500.0F);
            this.a(var1, var3, var3.i(var4), "y");
            var4 = new Vec3f(test_vector_graph.line_z);
            var4.scale(500.0F);
            this.a(var1, var3, var3.i(var4), "z");
            var1.setColor(Color.GREEN);
            Vec3f var5 = new Vec3f(test_vector_graph.line_des);
            var5.scale(5.0F);
            this.a(var1, var3, var3.i(var5), (String) null);
            this.a(var1, var3.i(var5));
            var1.setColor(Color.BLACK);
            String[] var6 = test_vector_graph.title_text.split("\n");
            int var7 = 10;
            String[] var11 = var6;
            int var10 = var6.length;

            for (int var9 = 0; var9 < var10; ++var9) {
                String var8 = var11[var9];
                var1.drawString(var8, 10, var7);
                var7 += var1.getFontMetrics().getHeight();
            }

        }

        private void a(Graphics var1, Vec3f var2) {
            Point var3 = this.bE(var2);
            Color var4 = var1.getColor();
            var1.setColor(new Color(Math.min(var4.getRed() + (255 - var4.getRed()) / 2, 255), Math.min(var4.getGreen() + (255 - var4.getGreen()) / 2, 255), Math.min(var4.getBlue() + (255 - var4.getBlue()) / 2, 255)));
            this.a(var1, new Vec3f(var2.x, -5.0F, 0.0F), new Vec3f(var2.x, 5.0F, 0.0F));
            this.a(var1, new Vec3f(-5.0F, var2.y, 0.0F), new Vec3f(5.0F, var2.y, 0.0F));
            this.a(var1, new Vec3f(0.0F, -5.0F, var2.z), new Vec3f(0.0F, 5.0F, var2.z));
            this.a(var1, new Vec3f(var2.x, 0.0F, 0.0F), new Vec3f(var2.x, 0.0F, var2.z));
            this.a(var1, new Vec3f(0.0F, 0.0F, var2.z), new Vec3f(var2.x, 0.0F, var2.z));
            this.a(var1, new Vec3f(var2.x, 0.0F, var2.z), new Vec3f(var2.x, var2.y, var2.z));
            var1.setColor(var4);
            var1.drawOval(var3.x - 1, var3.y - 1, 2, 2);
        }

        private void a(Graphics var1, Vec3f var2, Vec3f var3) {
            Point var4 = this.bE(var2);
            Point var5 = this.bE(var3);
            var1.drawLine(var4.x, var4.y, var5.x, var5.y);
        }

        private void a(Graphics var1, Vec3f var2, Vec3f var3, String var4) {
            this.a(var1, var2, var3);
            Point var5 = this.bE(var3);
            var1.drawOval(var5.x - 1, var5.y - 1, 2, 2);
            if (var4 != null) {
                var1.drawString(var4, var5.x + 10, var5.y);
            }

        }

        private Point bE(Vec3f var1) {
            Point var2 = new Point();
            var2.x = (int) ((float) (this.getSize().width / 2) + var1.x * ifG);
            var2.y = (int) ((float) (this.getSize().height / 2) - var1.y * ifG);
            if (var1.z != 0.0F) {
                var2.x = (int) ((float) var2.x - var1.z * ifG * ifF);
                var2.y = (int) ((float) var2.y + var1.z * ifG * ifF);
            }

            return var2;
        }
    }
}
