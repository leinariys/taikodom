import all.LogPrinter;
import all.OrientationBase;
import all.WatchDog;
import com.sun.opengl.util.Animator;
import taikodom.render.RenderGui;
import taikodom.render.RenderView;
import taikodom.render.SceneView;
import taikodom.render.StepContext;
import taikodom.render.camera.Camera;
import taikodom.render.camera.ViewCameraControl;
import taikodom.render.enums.BlendType;
import taikodom.render.enums.GeometryType;
import taikodom.render.gui.GLine;
import taikodom.render.gui.GuiScene;
import taikodom.render.loader.FileSceneLoader;
import taikodom.render.scene.Scene;
import taikodom.render.shaders.Shader;
import taikodom.render.shaders.ShaderPass;
import taikodom.render.textures.ChannelInfo;
import taikodom.render.textures.Material;
import taikodom.render.textures.Texture;

import javax.media.opengl.*;
import javax.vecmath.Vector2f;
import java.awt.*;
import java.awt.event.*;
import java.io.PrintWriter;

/**
 * Тест  Ресурсов
 * Создание мира из различных ресурсов, сцены, кораблей, планет
 */
public class test_GLineTest extends Frame implements MouseListener, MouseMotionListener, MouseWheelListener, GLEventListener {
    static test_GLineTest demo;
    int fps;
    int secondCounter;
    SceneView sceneView = new SceneView();
    boolean lockedTarget;
    GLine line;
    StepContext stepContext;
    int mousex;
    int mousey;
    Texture dustTex;
    Texture trailTex;
    int displayCount = 0;
    long timeToDisplay = 0L;
    boolean isRendering = false;
    private Scene scene;
    private RenderView rv;
    private long before = System.nanoTime();
    private long startTime = System.nanoTime();
    private FileSceneLoader loader;
    private Camera camera;
    private ViewCameraControl cameraControl = new ViewCameraControl();
    private String resourceDir;
    private Vector2f center;
    private OrientationBase cameraOrientation;
    private OrientationBase finalCameraOrientation;
    private GuiScene guiScene = new GuiScene("GuiScene");
    private RenderGui renderGui = new RenderGui();
    private WatchDog watchDog;

    /**
     * Конструктор класса
     */
    public test_GLineTest() {
        super("Basic GLine Demo");
        this.setLayout(new BorderLayout());
        this.setSize(720, 640);//Размер эерана
        this.setLocation(400, 200);//размещение на мониторе
        this.setAlwaysOnTop(true);//всегда находиться над другими окнами
        this.setVisible(true);
        this.setupJOGL();
        this.addWindowListener(new WindowAdapter()//Дейчтвия с окном
        {
            public void windowClosing(WindowEvent var1) {
                System.exit(0);
            }

            public void windowClosed(WindowEvent var1) {
                System.exit(0);
            }
        });
    }

    public static void main(String[] var0) {
        demo = new test_GLineTest();
        demo.resourceDir = ".";
        // demo.resourceDir = "../taikodom.client.resource/";
        if (var0.length > 0) {
            demo.resourceDir = var0[0];
        }
        demo.setVisible(true);
    }

    /**
     * Настройка  Java OpenGL
     */
    private void setupJOGL() {
        /**GLEventListener - Чтобы ваша программа могла использовать графический API JOGL
         */
        GLCapabilities var1 = new GLCapabilities();//Определяет набор возможностей OpenGL.
        var1.setDoubleBuffered(true);//Включает или отключает двойную буферизацию.
        var1.setHardwareAccelerated(true);//Включает или отключает аппаратное ускорение.

        GLCanvas var2 = new GLCanvas(var1);
        var2.enableInputMethods(true);//Включает или отключает поддержку метода ввода для этого компонента.
        //Подписываемся на события
        var2.addGLEventListener(this);//Добавляет данный слушатель в конец этой очереди.
        var2.addMouseMotionListener(this);
        var2.addMouseListener(this);
        var2.addMouseWheelListener(this);

        this.add(var2, "Center");
        //слушатель курсора
        var2.addMouseMotionListener(new MouseMotionListener() {
            @Override
            public void mouseDragged(MouseEvent e) {
            }

            @Override
            public void mouseMoved(MouseEvent var1) {
                test_GLineTest.this.mousex = var1.getX();
                test_GLineTest.this.mousey = var1.getY();
            }
        });
        //Слушатель клавиатуры
        var2.addKeyListener(new KeyListener() {
            @Override
            public void keyPressed(KeyEvent var1) {
                if (var1.getKeyCode() == 87) {//VK_W
                    ++test_GLineTest.this.center.y;
                }
                if (var1.getKeyCode() == 83) {//VK_S
                    --test_GLineTest.this.center.y;
                }
                if (var1.getKeyCode() == 65) {//VK_A
                    --test_GLineTest.this.center.x;
                }
                if (var1.getKeyCode() == 68) {//VK_D
                    ++test_GLineTest.this.center.x;
                }
                //test_GLineTest.this.center.x = test_GLineTest.this.center.x < 0.0F ? 0.0F : test_GLineTest.this.center.x;
                //test_GLineTest.this.center.y = test_GLineTest.this.center.y < 0.0F ? 0.0F : test_GLineTest.this.center.y;
                System.out.println("center " + test_GLineTest.this.center);
            }

            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
            }
        });
        Animator var3 = new Animator(var2);
        var3.start();
    }

    /**
     * Он вызывается объектом интерфейса GLAutoDrawable сразу после инициализации GLContext OpenGL.
     *
     * @param var1 Поверхность для рисования OpenGL и примитивы рисования(линии треугольники).
     */
    public void init(GLAutoDrawable var1) {
        try {
            this.rv = new RenderView(var1.getContext());
            this.rv.setViewport(0, 0, var1.getWidth(), var1.getHeight());
            var1.setGL(new DebugGL(var1.getGL()));
            this.rv.getDrawContext().setCurrentFrame(1000L);
            GL var2 = var1.getGL();
            this.rv.setGL(var2);
            this.renderGui.getGuiContext().setDc(this.rv.getDrawContext());
            this.rv.getDrawContext().setRenderView(this.rv);
            this.loader = new FileSceneLoader(this.resourceDir);
            System.loadLibrary("granny2");
            System.loadLibrary("mss32");
            System.loadLibrary("binkw32");
            System.loadLibrary("utaikodom.render");
            this.loader.loadFile("data/shaders/light_shader.pro");
            this.loader.loadFile("data/models/fig_spa_bullfrog.pro");//Корабль
            this.loader.loadFile("data/models/fig_spa_barracuda.pro");
            this.loader.loadFile("data/scene/nod_spa_mars.pro");//сцена космоса марс скайбокс, звёзды, бланеты
            this.loader.loadFile("data/trails.pro");
            this.loader.loadFile("data/effects.pro");
            this.loader.loadFile("data/models/amm_min_contramed1.pro");
            this.loader.loadFile("data/hud.pro");//интерфейс
            //this.loader.loadFile("data/projectiles.pro");
            this.loader.loadMissing();

            Shader var3 = new Shader();
            var3.setName("Default billboard shader");
            ShaderPass var4 = new ShaderPass();
            var4.setName("Default billboard pass");
            ChannelInfo var5 = new ChannelInfo();
            var5.setTexChannel(8);
            var4.setChannelTextureSet(0, var5);
            var4.setBlendEnabled(true);
            var4.setAlphaTestEnabled(true);
            var4.setDstBlend(BlendType.ONE_MINUS_SRC_ALPHA);
            var4.setSrcBlend(BlendType.ONE);
            var4.setCullFaceEnabled(true);
            var4.getRenderStates().setCullFaceEnabled(true);
            var3.addPass(var4);

            Material var6 = new Material();
            ShaderPass var7 = new ShaderPass();
            var7.setName("Default billboard pass");
            ChannelInfo var8 = new ChannelInfo();
            var8.setTexChannel(8);
            var7.setChannelTextureSet(0, var8);
            var7.setBlendEnabled(true);
            var7.setAlphaTestEnabled(true);
            var7.setDstBlend(BlendType.ONE_MINUS_SRC_ALPHA);
            var7.setSrcBlend(BlendType.ONE);
            var7.setCullFaceEnabled(true);
            var7.getRenderStates().setCullFaceEnabled(true);
            var6.setShader(var3);

            //Настройка камеры
            this.camera = this.sceneView.getCamera();
            this.camera.setFarPlane(400000.0F);//дальность прорисовки
            this.camera.setNearPlane(1.0F);//близость прорисовки
            this.camera.setFovY(75.0F);//Угол обзора
            this.camera.setAspect(1.3333F);
            this.camera.getTransform().setTranslation(20.0D, 20.0D, 20.0D);

            this.scene = this.sceneView.getScene();
            this.center = new Vector2f(0, 0);// new Vector2f(this.getSize().width/2, this.getSize().height/2); //new aKa(320.0F, 310.0F);
            this.cameraOrientation = new OrientationBase();
            this.finalCameraOrientation = new OrientationBase();
            this.cameraControl.setCamera(this.camera);

            //Отрисовка линий
            this.line = new GLine();
            this.line.setMaterial(var6);
            this.line.createCircle(this.center, 100.0F);//Окружность
            this.line.createSquare(this.center, 150.0F);//Квадрат
            this.line.createRectangle(this.center, 200.0F, 250.0F);//Прямоугольник
            this.line.createElipse(this.center, 100.0F, 250.0F, 5.0f);//Эллипс
            this.line.setGeometryType(GeometryType.LINES);
            this.guiScene.addChild(this.line);

            System.nanoTime();
        } catch (Exception var9) {
            var9.printStackTrace();
            System.exit(0);
        }

    }

    /**
     * Вызывается постоянно. Этот метод содержит логику, используемую для рисования графических элементов с использованием OpenGL API.
     *
     * @param var1 Поверхность для рисования OpenGL и примитивы рисования(линии треугольники).
     */
    public void display(GLAutoDrawable var1) {
        try {
            long var2 = System.currentTimeMillis();
            if (this.watchDog == null) {
                this.watchDog = new WatchDog();
                PrintWriter var4 = LogPrinter.createLogFile("client-watchdog", "log");
                var4.println("Starting GLine demo");
                this.watchDog.setPrintWriter(var4);
                this.watchDog.ht(200L);//сон потока
                this.watchDog.K(2.0E10D);
                this.watchDog.start();
            }

            this.rv.getDrawContext().setViewport(0, 0, this.getSize().width, this.getSize().height);
            this.rv.getRenderInfo().setDrawAABBs(false);//отрисовка
            long var7 = var2 - this.startTime;
            this.camera.getTransform().setIdentity();
            this.cameraOrientation.a(this.cameraOrientation, this.finalCameraOrientation, 0.1F);
            this.camera.setOrientation(this.cameraOrientation);
            this.camera.calculateProjectionMatrix();
            if (this.stepContext == null) {
                this.stepContext = new StepContext();
            }

            this.stepContext.setCamera(this.camera);
            this.stepContext.setDeltaTime((float) (var2 - this.before));
            this.scene.step(this.stepContext);
            this.guiScene.step(this.stepContext);
            this.timeToDisplay += var2 - this.before;
            this.rv.render(this.sceneView, (double) ((float) var7 * 1.0E-9F), (float) (var2 - this.before) * 1.0E-9F);
            this.renderGui.render(this.guiScene);
            this.before = var2;
            if (this.timeToDisplay > 1000000000L) {
                this.timeToDisplay = 0L;
            }

        } catch (Exception var6) {
            var6.printStackTrace();
            System.exit(0);
        }
    }

    /**
     * Он вызывается объектом интерфейса GLAutoDrawable во время первой перерисовки после изменения размера компонента.
     * Он также вызывается всякий раз, когда положение компонента в окне изменяется.
     *
     * @param glAutoDrawable Поверхность для рисования для команд OpenGL.
     * @param x
     * @param y
     * @param width
     * @param height
     */
    public void reshape(GLAutoDrawable glAutoDrawable, int x, int y, int width, int height) {
        if (this.camera != null) {
            this.camera.setAspect((float) width / (float) height);
        }

        this.sceneView.setViewport(x, y, width, height);
        this.renderGui.resize(x, y, width, height);
    }

    /**
     * Вызывается при извлечении, когда изменился режим отображения или устройство отображения, связанное с GLAutoDrawable.
     * Два логических параметра указывают типы изменений, которые произошли.
     *
     * @param glAutoDrawable Поверхность для рисования для команд OpenGL.
     * @param modeChanged    Примером изменения режима отображения является изменение глубины бита (например, от 32-битного до 16-битного цвета) на мониторе, на котором в настоящее время отображается GLAutoDrawable.
     * @param deviceChanged  Примером изменения отображения устройства является то, что пользователь перетаскивает окно, содержащее GLAutoDrawable, с одного монитора на другой в настройке с несколькими мониторами.
     */
    public void displayChanged(GLAutoDrawable glAutoDrawable, boolean modeChanged, boolean deviceChanged) {
        this.rv.setViewport(0, 0, glAutoDrawable.getWidth(), glAutoDrawable.getHeight());
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseDragged(MouseEvent e) {

    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        if (e.getScrollType() == 0) {
            int var2 = e.getWheelRotation();
            if (var2 > 0) {
                this.cameraControl.zoomOut((float) var2);
            } else {
                this.cameraControl.zoomIn((float) (-var2));
            }
        }
    }
}
