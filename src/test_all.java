import all.*;

import javax.swing.*;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import java.awt.*;

public class test_all {

    public static void main(String[] var0) {
        System.out.println("Start test");
        loadXml();
    }


    private static void loadXml() {

        JFrame.setDefaultLookAndFeelDecorated(true);
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createGUI();
            }
        });
    }

    private static void swingGUI() {

        JFrame frame = new JFrame("Test frame");
        frame.setPreferredSize(new Dimension(500, 500));

        System.out.println("getLayout " + frame.getContentPane().getLayout());

        frame.getContentPane().setLayout(new FlowLayout());
        System.out.println("getLayout " + frame.getContentPane().getLayout());

        JScrollPane JScrollPan = new JScrollPane();
        frame.getContentPane().add(JScrollPan);

        JPanel JPane = new JPanel();
        JPane.setSize(250, 100);
        JPane.setBorder(new CompoundBorder(new EmptyBorder(12, 12, 12, 12), new TitledBorder("Рамка с заголовком")));


        frame.getContentPane().add(JPane);
        JButton JButto = new JButton();
        JButto.setText("JButton");
        JPane.add(JButto);


      /*  JTogglePanel JTooglePan = new TogglePanel();
        frame.getContentPane().add(TogglePane);*/
        JRootPane JRootPan = new JRootPane();
        frame.getContentPane().add(JRootPan);
        JInternalFrame JInternalFram = new JInternalFrame();
        frame.getContentPane().add(JInternalFram);
        JSplitPane JSplitPan = new JSplitPane();
        frame.getContentPane().add(JSplitPan);
        JLayeredPane JLayeredPan = new JLayeredPane();
        frame.getContentPane().add(JLayeredPan);
        JOptionPane JOptionPan = new JOptionPane();
        frame.getContentPane().add(JOptionPan);
        JLabel JLabe = new JLabel();
        frame.getContentPane().add(JLabe);
        JComboBox JComboBo = new JComboBox();
        frame.getContentPane().add(JComboBo);
        JMenuBar JMenuBa = new JMenuBar();
        frame.getContentPane().add(JMenuBa);
        JToolBar JToolBa = new JToolBar();
        frame.getContentPane().add(JToolBa);
        JProgressBar JProgressBa = new JProgressBar();
        frame.getContentPane().add(JProgressBa);
        JSlider JSlide = new JSlider();
        frame.getContentPane().add(JSlide);
        JScrollBar JScrollBa = new JScrollBar();
        frame.getContentPane().add(JScrollBa);
        JPopupMenu JPopupMen = new JPopupMenu();
        frame.getContentPane().add(JPopupMen);
        JFileChooser JFileChoose = new JFileChooser();
        frame.getContentPane().add(JFileChoose);
        JColorChooser JColorChoose = new JColorChooser();
        frame.getContentPane().add(JColorChoose);
        JTree JTre = new JTree();
        frame.getContentPane().add(JTre);
        JTable JTabl = new JTable();
        frame.getContentPane().add(JTabl);
        JList JLis = new JList();
        frame.getContentPane().add(JLis);
        JTextField JTextFiel = new JTextField();
        frame.getContentPane().add(JTextFiel);
        JTextArea JTextAre = new JTextArea();
        frame.getContentPane().add(JTextAre);
      /*  JTextEditorPane JTextEditorPan = new JTextEditorPane();
        frame.getContentPane().add(JTextEditorPan);*/
      /*  JTextPane JTextPan = new JTextPane();
        frame.getContentPane().add(JTextPan);
        JPasswordField JPasswordFiel = new JPasswordField();
        frame.getContentPane().add(JPasswordFiel);
        JFormattedTextField JFormattedTextFiel = new JFormattedTextField();
        frame.getContentPane().add(JFormattedTextFiel);
        JToggleButton JToggleButto = new JToggleButton();
        frame.getContentPane().add(JToggleButto);

        JMenuItem JMenuIte = new JMenuItem();
        frame.getContentPane().add(JMenuIte);
        JCheckBox JCheckBo = new JCheckBox();
        frame.getContentPane().add(JCheckBo);
        JRadioButton JRadioButto = new JRadioButton();
        frame.getContentPane().add(JRadioButto);
        JMenu JMen = new JMenu();
        frame.getContentPane().add(JMen);
        JViewport JViewpor = new JViewport();
        frame.getContentPane().add(JViewpor);
        JToolTip JToolTi = new JToolTip();
        frame.getContentPane().add(JToolTi);
        JTableHeader JTableHeade = new JTableHeader();
        frame.getContentPane().add(JTableHeade);
        JTabbedPane JTabbedPan = new JTabbedPane();
        frame.getContentPane().add(JTabbedPan);
        JSpinner JSpinne = new JSpinner();
        frame.getContentPane().add(JSpinne);
        JSeparator JSeparato = new JSeparator();
        frame.getContentPane().add(JSeparato);
        JLayer JLaye = new JLayer();
        frame.getContentPane().add(JLaye);*/

        System.out.println("ComponentCount " + frame.getContentPane().getComponentCount());
/*
        RepaintManager.currentManager(frame.getRootPane()).setDoubleBufferingEnabled(false);
        ((JComponent)frame.getContentPane()).setDebugGraphicsOptions(DebugGraphics.FLASH_OPTION);
*/
        frame.pack();
        frame.setVisible(true);
    }


    private static void createGUI() {
        JFrame frame = new JFrame("Test frame");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setPreferredSize(new Dimension(500, 500));
        frame.getRootPane().setBounds(0, 0, 800, 600);

        String var1 = System.getProperty("gametoolkit.client.render.rootpath", ".");
        FileControl rootPathRender = new FileControl(var1);

        JDesktopPane jDesktopPane1 = new JDesktopPane();
        jDesktopPane1.setName("!!!jDesktopPane222222!!!");
        frame.setContentPane(jDesktopPane1);
        jDesktopPane1.setBounds(0, 0, 800, 600);

        BaseUItegXML baseUItegXML = new BaseUItegXML();
        LoaderImageBikSwfPng loaderImageBikSwfPng = new LoaderImageBikSwfPng((EngineGraphics) null);
        loaderImageBikSwfPng.setRootPathRender(rootPathRender);
        baseUItegXML.setLoaderImage(loaderImageBikSwfPng);

        avI cssNode = new CssNode();
        cssNode = baseUItegXML.loadCSS((Object) null, (String) "res://data/styles/core.css");
        baseUItegXML.setCssNodeCore(cssNode);
        baseUItegXML.setRootPane(frame.getRootPane());
        baseUItegXML.getRootPane().setName("!!!!!RootPane!!!!!");


        System.out.println("getRootPane " + frame.getRootPane());
        System.out.println("getContentPane " + frame.getContentPane());
        System.out.println("getLayout " + frame.getContentPane().getLayout());

        JButton JButto = new JButton();
        (JButto).setText("JButton");
        (JButto).setBounds(10, 10, 50, 20);

        //Component login = baseUItegXML.CreateJComponent(LoginAddon.class, "login.xml");
        System.out.println("login " + frame.getContentPane().getComponents());
        //((JPanel)frame.getContentPane()[0]).findJComponent("string");
        /*     frame.getContentPane().add(login);
         */

        frame.getContentPane().add(JButto);
        //frame.getRootPane().add(((JButton)JButto.wz().getCurrentComponent()));


        JTextArea JCheckBo = new JTextArea();
        JCheckBo.setText("setTextsetText");
        JCheckBo.setBounds(60, 30, 50, 20);
        frame.getContentPane().add(JCheckBo);


        //JGLDesktop polotnoJGL = new JGLDesktop();
        //JDesktopPane jDesk = polotnoJGL.getDesktopPane();

        //Тест отрисовки
/*
        RepaintManager.currentManager(frame.getRootPane()).setDoubleBufferingEnabled(false);
        ((JComponent)frame.getContentPane()).setDebugGraphicsOptions(DebugGraphics.FLASH_OPTION);
*/

        frame.pack();
        frame.setVisible(true);


    }

    public static Component findJComponent(JPanel the, String var1) {
        Component var2;
        Component[] var5 = the.getComponents();
        int var4 = var5.length;

        for (int var7 = 0; var7 < var4; ++var7) {
            var2 = var5[var7];
            System.out.println("findJComponent2 " + var2);
            if (var1.equals(var2.getName())) {
                return var2;
            }

            if (var2 instanceof IContainerCustomWrapper) {
                Component var6 = ((IContainerCustomWrapper) var2).findJComponent(var1);
                if (var6 != null) {
                    return var6;
                }
            }
        }
        return null;
    }


}

