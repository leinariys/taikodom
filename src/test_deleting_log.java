import java.io.File;
import java.io.FilenameFilter;

/**
 * Удаление некоторых видов логов
 */
public class test_deleting_log {
    public static void main(String[] var0) {
        System.out.println("Transaction logs:");
        System.out.println("--------------------------------------");
        File var1 = new File("data");
        var1.list(new FilenameFilter() {
            public boolean accept(File var1, String var2) {
                if (var2.endsWith(".log"))// конец имени .log
                {
                    File var3 = new File(var1, var2);
                    System.out.println("deleting " + var3);
                    var3.delete();
                }
                return false;
            }
        });

        System.out.println();
        System.out.println("App logs:");
        System.out.println("--------------------------------------");
        File var2 = new File("log");
        var2.list(new FilenameFilter() {
            public boolean accept(File var1, String var2) {
                if (var2.endsWith(".log.csv"))// конец имени .log.csv
                {
                    File var3 = new File(var1, var2);
                    System.out.println("deleting " + var3);
                    var3.delete();
                }
                return false;
            }
        });

        System.out.println();
        System.out.println("Chat logs:");
        System.out.println("--------------------------------------");
        File var3 = new File("log");
        var3.list(new FilenameFilter() {
            public boolean accept(File var1, String var2) {
                if (var2.startsWith("chat") && var2.endsWith(".log"))//начало имени chat  конец имени .log
                {
                    File var3 = new File(var1, var2);
                    System.out.println("deleting " + var3);
                    var3.delete();
                }
                return false;
            }
        });
    }
}
