import javax.swing.*;
import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;

public class test_JFrame extends JFrame {
    public test_JFrame() {
        this.setVisible(true);
        this.add(new JPanel() {
            public void paint(Graphics var1) {
                Graphics2D var2 = (Graphics2D) var1;//Квадрат в котором текст тест
                var2.setColor(Color.red);
                var2.draw(new Rectangle(10, 10, 200, 200));

                Font var3 = this.getFont();
                var2.setFont(var3);
                var2.drawString("exp", 0, 50);

                FontRenderContext var4 = var2.getFontRenderContext();
                var2.getFontMetrics();

                GlyphVector var5 = var3.createGlyphVector(var4, "teste".toCharArray());
                var2.drawGlyphVector(var5, 20.0F, 20.0F);//текст тест вверху слеав маленький
                var2.draw(new Rectangle(10, 10, 200, 200));
                var2.translate(100, 100);// смещение тескта тест х2
                var2.scale(10.0D, 10.0D);//масштаб
                var2.setStroke(new BasicStroke(0.2F));//жирность рамки
                var2.draw(var5.getOutline());
                var2.translate(0, 10);
                var2.fill(var5.getOutline());
            }
        });
        this.setBounds(100, 100, 400, 400);
    }

    public static void main(String[] var0) {
        new test_JFrame();
    }
}
