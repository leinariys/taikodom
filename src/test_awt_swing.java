import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class test_awt_swing {
    private static Frame emy;
    private static JFrame emz;

    public static void main(String[] var0) {
        emz = new JFrame() {// swing
            protected void processEvent(AWTEvent var1) {
                if (var1 instanceof MouseEvent) {
                    MouseEvent var2 = (MouseEvent) var1;
                    Component var3 = test_awt_swing.a(var2.getX() - test_awt_swing.emz.getInsets().left, var2.getY() - test_awt_swing.emz.getInsets().top, test_awt_swing.emy, true);
                    if (var3 == null) {
                        return;
                    }

                    MouseEvent var4 = new MouseEvent(var3, var2.getID(), var2.getWhen(), var2.getModifiersEx() | var2.getModifiers(), var2.getX() - test_awt_swing.emz.getInsets().left, var2.getY() - test_awt_swing.emz.getInsets().top, var2.getXOnScreen(), var2.getYOnScreen(), var2.getClickCount(), var2.isPopupTrigger(), var2.getButton());
                    var3.dispatchEvent(var4);
                }

                boolean var10000 = var1 instanceof KeyEvent;
            }
        };
        emz.enableInputMethods(true);
        emz.setDefaultCloseOperation(3);
        emz.setBounds(100, 100, 1050, 800);
        emz.setLayout(new BorderLayout());

        emy = new Frame() {//awt
            public boolean isShowing() {
                return true;
            }

            public boolean isVisible() {
                return !this.isFocusableWindow() || !(new Throwable()).getStackTrace()[1].getMethodName().startsWith("requestFocus");
            }

            public boolean isFocused() {
                return true;
            }
        };
        emy.setBounds(10, 10, 400, 400);
        JButton var1 = new JButton("test");
        JPanel var2 = new JPanel();
        emy.add(var2);
        var2.setLayout(new GridLayout(0, 1));
        var2.setBounds(0, 0, 400, 400);
        var2.add(var1);
        JTextField var3 = new JTextField();
        var3.setText("teste");
        emy.setFocusableWindowState(false);
        emy.setUndecorated(true);
        emy.setVisible(true);//добавил

        JPanel var4 = new JPanel() {
            public void paint(Graphics var1) {
                var1.translate(test_awt_swing.emy.getComponent(0).getX(), test_awt_swing.emy.getComponent(0).getY());
                test_awt_swing.emy.getComponent(0).paint(var1);
            }
        };
        emz.addMouseListener(new MouseAdapter() {
        });
        emz.addMouseMotionListener(new MouseMotionAdapter() {
        });
        emz.setLayout(new BorderLayout());
        emz.add(var4);
        emz.setVisible(true);
        (new Timer(10, new ActionListener() {
            public void actionPerformed(ActionEvent var1) {
                test_awt_swing.emz.repaint();
            }
        })).start();
    }

    private static Component a(int var0, int var1, Component var2, boolean var3) {
        if (var3 && var2 instanceof JRootPane) {
            JRootPane var4 = (JRootPane) var2;
            var2 = var4.getContentPane();
        }

        Object var11 = var2;
        if (!((Component) var2).contains(var0, var1)) {
            var11 = null;
        } else {
            synchronized (((Component) var2).getTreeLock()) {
                if (var2 instanceof Container) {
                    Container var6 = (Container) var2;
                    int var7 = var6.getComponentCount();

                    for (int var8 = 0; var8 < var7; ++var8) {
                        Component var9 = var6.getComponent(var8);
                        if (var9 != null && var9.isVisible() && var9.contains(var0 - var9.getX(), var1 - var9.getY())) {
                            var11 = var9;
                            break;
                        }
                    }
                }
            }
        }

        if (var11 != null) {
            if (var2 instanceof JTabbedPane && var11 != var2) {
                var11 = ((JTabbedPane) var2).getSelectedComponent();
            }

            var0 -= ((Component) var11).getX();
            var1 -= ((Component) var11).getY();
        }

        return (Component) (var11 != var2 && var11 != null ? a(var0, var1, (Component) var11, var3) : var11);
    }
}
