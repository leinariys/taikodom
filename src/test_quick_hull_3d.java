import all.RY;
import all.ajK;
import all.bc_q;
import com.hoplon.geometry.Vec3f;
import quickhull3d.Point3d;
import quickhull3d.QuickHull3D;
import taikodom.render.RenderView;
import taikodom.render.camera.Camera;
import taikodom.render.enums.GeometryType;
import taikodom.render.scene.ObjectFactory;
import taikodom.render.scene.RExpandableMesh;
import taikodom.render.scene.RenderObject;
import taikodom.render.scene.Scene;
import taikodom.render.textures.Material;

import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.vecmath.Tuple3f;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * Не разобрался чёрный экран
 * class parseStyleSheet
 */
public class test_quick_hull_3d extends Frame implements GLEventListener {

    private static final float TU = 1.0F;
    public static Vec3f TT = new Vec3f(0.0F, 1.0F, 0.0F);
    public static Vec3f TV = new Vec3f(0.0F, 0.5F, 0.5F);
    public static Vec3f TW = new Vec3f(0.0F, 0.0F, 1.0F);
    public static Vec3f TX = new Vec3f(1.0F, 0.0F, 0.0F);
    private final List Uc = new ArrayList();
    private final List Ud = new ArrayList();
    private final List Ue = new ArrayList();
    private final List Uf = new ArrayList();
    private final List Ug = new ArrayList();
    private long before = System.nanoTime();
    private Camera camera;
    private GLCanvas canvas;
    private QuickHull3D TY;
    private Material TZ;
    private boolean cW = true;
    private boolean Ua = true;
    private boolean Ub = true;
    private RenderView rv;
    private Scene scene;
    private Vec3f Uh = new Vec3f();
    private float Ui = 0.0F;//Текущий угол поворота

    /**
     *
     */
    public test_quick_hull_3d() {
        super("EPA Debugger");
        this.setLayout(new BorderLayout());
        this.setSize(720, 640);
        this.setLocation(40, 40);
        this.setVisible(true);
        this.setupJOGL();

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent var1) {
                System.exit(0);
            }

            @Override
            public void windowClosing(WindowEvent var1) {
                System.exit(0);
            }
        });

        this.canvas.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent var1) {

                if (var1.getKeyCode() == 88)//x
                {
                    test_quick_hull_3d.this.Ui = (test_quick_hull_3d.this.Ui + 2.0F) % 360.0F;
                }

                if (var1.getKeyCode() == 90)//z
                {
                    test_quick_hull_3d.this.Ui = (test_quick_hull_3d.this.Ui - 2.0F) % 360.0F;
                }

            }

            @Override
            public void keyReleased(KeyEvent var1) {
                if (var1.getKeyCode() == 67)//CreateJComponent
                {
                    test_quick_hull_3d.this.Ua = !test_quick_hull_3d.this.Ua;
                }

                if (var1.getKeyCode() == 86)//
                {
                    test_quick_hull_3d.this.Ub = !test_quick_hull_3d.this.Ub;
                }

            }
        });
    }

    /**
     * @param var0
     */
    public static void main(String[] var0) {
        test_quick_hull_3d var1 = new test_quick_hull_3d();
        var1.setVisible(true);
    }

    /**
     * @param var1
     * @param var2
     * @param var3
     * @param var4
     */
    public void a(ajK var1, float var2, float var3, float var4) {
        var2 /= 2.0F;
        var3 /= 2.0F;
        var4 /= 2.0F;
        Vec3f[] var5 = new Vec3f[]{new Vec3f(var2, -var3, -var4), new Vec3f(var2, -var3, var4), new Vec3f(-var2, -var3, var4), new Vec3f(-var2, -var3, -var4), new Vec3f(var2, var3, -var4), new Vec3f(var2, var3, var4), new Vec3f(-var2, var3, var4), new Vec3f(-var2, var3, -var4), new Vec3f(-var2, -var3, -var4), new Vec3f(var2, -var3, -var4), new Vec3f(var2, var3, -var4), new Vec3f(-var2, var3, -var4), new Vec3f(-var2, -var3, var4), new Vec3f(var2, -var3, var4), new Vec3f(var2, var3, var4), new Vec3f(-var2, var3, var4)};
        Vec3f[] var9 = var5;
        int var8 = var5.length;

        for (int var7 = 0; var7 < var8; ++var7) {
            Vec3f var6 = var9[var7];
            Vec3f var10 = var1.c((Tuple3f) var6);
            this.Ud.add(var10);
        }

    }

    /**
     * @param var1
     * @param var2
     * @param var3
     * @param var4
     * @param var5
     */
    public void a(ajK var1, RY var2, float var3, float var4, float var5) {
        RExpandableMesh var6 = new RExpandableMesh();
        Vec3f[] var10;
        int var9 = (var10 = var2.bsr()).length;

        int var8;
        for (var8 = 0; var8 < var9; ++var8) {
            Vec3f var7 = var10[var8];
            var6.addVertexWithColor(var7.x, var7.y, var7.z, var3, var4, var5, 1.0F);
        }

        int[] var11;
        var9 = (var11 = var2.bss()).length;

        for (var8 = 0; var8 < var9; ++var8) {
            int var12 = var11[var8];
            var6.addIndex(var12);
        }

        bc_q var13 = new bc_q();
        var13.set(var1);
        var6.setTransform(var13);
        this.Ug.add(var6);
    }

    /**
     * @param var1
     */
    @Override
    public void display(GLAutoDrawable var1) {
        this.update();
        if (this.cW) {
            ajK var2 = new ajK();
            Vec3f var3 = new Vec3f(0.0F, 5.0F, 40.0F);
            var2.rotateY(this.Ui);
            this.camera.getTransform().set3x3(var2);
            this.camera.getTransform().multiply3x3(var3, var3);
            this.camera.getTransform().setTranslation(var3);
            this.camera.calculateProjectionMatrix();
        }

        try {
            long var5 = System.nanoTime();
            this.rv.render(this.scene, this.camera, (double) System.currentTimeMillis(), (float) (var5 - this.before) * 1.0E-9F);
            this.before = var5;
        } catch (Exception var4) {
            var4.printStackTrace();
            System.exit(0);
        }

    }

    /**
     * @param var1
     * @param var2
     * @param var3
     */
    @Override
    public void displayChanged(GLAutoDrawable var1, boolean var2, boolean var3) {
        this.rv.setViewport(0, 0, var1.getWidth(), var1.getHeight());
    }

    /**
     * @param var1
     * @param var2
     * @param var3
     * @param var4
     * @param var5
     * @param var6
     */
    private void a(RExpandableMesh var1, Vec3f var2, int var3, float var4, float var5, float var6) {
        var1.addVertexWithColor(var2.x - 1.0F, var2.y, var2.z, var4, var5, var6, 1.0F);
        var1.addVertexWithColor(var2.x + 1.0F, var2.y, var2.z, var4, var5, var6, 1.0F);
        var1.addIndex(var3);
        var1.addIndex(var3 + 1);
        var1.addVertexWithColor(var2.x, var2.y - 1.0F, var2.z, var4, var5, var6, 1.0F);
        var1.addVertexWithColor(var2.x, var2.y + 1.0F, var2.z, var4, var5, var6, 1.0F);
        var1.addIndex(var3 + 2);
        var1.addIndex(var3 + 3);
        var1.addVertexWithColor(var2.x, var2.y, var2.z - 1.0F, var4, var5, var6, 1.0F);
        var1.addVertexWithColor(var2.x, var2.y, var2.z + 1.0F, var4, var5, var6, 1.0F);
        var1.addIndex(var3 + 4);
        var1.addIndex(var3 + 5);
    }

    /**
     * @param var1
     * @param var2
     * @param var3
     */
    private void a(RExpandableMesh var1, List var2, int var3) {
        Vec3f var4 = (Vec3f) var2.get(var3);
        Vec3f var5 = (Vec3f) var2.get(var3 + 1);
        Vec3f var6 = (Vec3f) var2.get(var3 + 2);
        Vec3f var7 = (Vec3f) var2.get(var3 + 3);
        var1.addVertexWithColor(var4.x, var4.y, var4.z, TV.x, TV.y, TV.z, 1.0F);
        var1.addVertexWithColor(var5.x, var5.y, var5.z, TV.x, TV.y, TV.z, 1.0F);
        var1.addVertexWithColor(var6.x, var6.y, var6.z, TV.x, TV.y, TV.z, 1.0F);
        var1.addVertexWithColor(var7.x, var7.y, var7.z, TV.x, TV.y, TV.z, 1.0F);
        var1.addIndex(var3);
        var1.addIndex(var3 + 1);
        var1.addIndex(var3 + 2);
        var1.addIndex(var3 + 3);
    }

    /**
     * @param var1
     * @param var2
     * @param var3
     * @param var4
     * @param var5
     * @param var6
     * @param var7
     * @param var8
     */
    private void a(RExpandableMesh var1, Vec3f var2, Vec3f var3, Vec3f var4, int var5, float var6, float var7, float var8) {
        var1.addVertexWithColor(var2.x, var2.y, var2.z, var6, var7, var8, 1.0F);
        var1.addVertexWithColor(var3.x, var3.y, var3.z, var6, var7, var8, 1.0F);
        var1.addVertexWithColor(var4.x, var4.y, var4.z, var6, var7, var8, 1.0F);
        var1.addIndex(var5);
        var1.addIndex(var5 + 1);
        var1.addIndex(var5 + 2);
    }

    /**
     * @return
     */
    public List yc() {
        return this.Uc;
    }

    /**
     * @return
     */
    public List yd() {
        return this.Ud;
    }

    /**
     * Вызывается путем выталкивания сразу после инициализации контекста OpenGL.
     *
     * @param var1
     */
    @Override
    public void init(GLAutoDrawable var1) {
        try {
            this.rv = new RenderView(var1);
            this.scene = new Scene("111");
            System.loadLibrary("granny2");
            // System.loadLibrary("utaikodom.render");
            this.TZ = ObjectFactory.getDefault().createDefaultMaterial();
            this.camera = new Camera();
            this.camera.getTransform().setTranslation(0.0D, 0.0D, 20.0D);
        } catch (Exception var3) {
            var3.printStackTrace();
            System.exit(0);
        }

    }

    /**
     * Вызывается при рисовании во время первой перерисовки после изменения размера компонента.
     *
     * @param var1
     * @param var2
     * @param var3
     * @param var4
     * @param var5
     */
    @Override
    public void reshape(GLAutoDrawable var1, int var2, int var3, int var4, int var5) {
        if (this.camera != null) {
            this.camera.setAspect((float) var4 / (float) var5);
        }

        this.rv.setViewport(0, 0, var4, var5);
    }

    /**
     * @param var1
     */
    public void a(QuickHull3D var1) {
        this.TY = var1;
    }

    /**
     *
     */
    private void setupJOGL() {
        GLCapabilities var1 = new GLCapabilities();//Определяет набор возможностей OpenGL.
        var1.setDoubleBuffered(true);//Включает или отключает двойную буферизацию.
        var1.setHardwareAccelerated(true);//Включает или отключает аппаратное ускорение.
        var1.setSampleBuffers(true);//Указывает, должны ли быть выделены буферы выборки для полного сглаживания сцены (FSAA) для этой возможности.
        var1.setHardwareAccelerated(true);//Включает или отключает аппаратное ускорение.
        var1.setNumSamples(8);//Если буферы выборки включены, указывается количество выделенных буферов. По умолчанию используется значение 2.
        this.canvas = new GLCanvas(var1);
        this.canvas.enableInputMethods(true);//Включает или отключает поддержку метода ввода для этого компонента.
        this.canvas.addGLEventListener(this);//Добавляет данный слушатель в конец этой очереди.
        this.add(this.canvas, "Center");
      /*
      Animator var2 = new Animator(this.canvas);
      var2.setRunAsFastAsPossible(false);
      var2.start();*/
    }

    /**
     *
     */
    private void update() {
        this.scene.removeAllChildren();
        if (this.Ua) {
            this.ye();
        }

        this.yh();
        this.yi();
        this.yg();
        if (this.Ub) {
            this.yf();
        }

    }

    /**
     *
     */
    private void ye() {
        if (this.TY != null) {
            Point3d[] var1 = this.TY.getVertices();
            int[][] var2 = this.TY.getFaces();
            RExpandableMesh var3 = new RExpandableMesh();
            var3.setMaterial(this.TZ);
            if (this.TY.getFaces()[0].length == 4) {
                var3.getMesh().setGeometryType(GeometryType.QUADS);
            } else {
                var3.getMesh().setGeometryType(GeometryType.TRIANGLES);
            }

            Point3d[] var7 = var1;
            int var6 = var1.length;

            int var5;
            for (var5 = 0; var5 < var6; ++var5) {
                Point3d var4 = var7[var5];
                var3.addVertexWithColor((float) var4.x, (float) var4.y, (float) var4.z, 1.0F, 1.0F, 0.0F, 1.0F);
            }

            int[][] var13 = var2;
            var6 = var2.length;

            for (var5 = 0; var5 < var6; ++var5) {
                int[] var12 = var13[var5];
                int[] var11 = var12;
                int var10 = var12.length;

                for (int var9 = 0; var9 < var10; ++var9) {
                    int var8 = var11[var9];
                    var3.addIndex(var8);
                }
            }

            this.scene.addChild(var3);
        }

    }

    /**
     *
     */
    private void yf() {
        if (this.Ug.size() > 0) {
            Iterator var2 = this.Ug.iterator();

            while (var2.hasNext()) {
                RenderObject var1 = (RenderObject) var2.next();
                var1.setMaterial(this.TZ);
                this.scene.addChild(var1);
            }
        }
    }

    /**
     *
     */
    private void yg() {
        if (this.Uc.size() > 0) {
            RExpandableMesh var1 = new RExpandableMesh();
            var1.setMaterial(this.TZ);
            int var2 = 0;

            for (Iterator var4 = this.Uc.iterator(); var4.hasNext(); var2 += 6) {
                Vec3f var3 = (Vec3f) var4.next();
                this.a(var1, var3, var2, TT.x, TT.y, TT.z);
            }

            this.a(var1, this.Uh, var2, 1.0F, 0.0F, 1.0F);
            var1.getMesh().setGeometryType(GeometryType.LINES);
            this.scene.addChild(var1);
        }
    }

    /**
     *
     */
    private void yh() {
        int var1 = this.Ud.size();
        if (var1 > 0) {
            RExpandableMesh var2 = new RExpandableMesh();
            var2.setMaterial(this.TZ);

            for (int var3 = 0; var3 < var1; var3 += 4) {
                this.a(var2, this.Ud, var3);
            }

            var2.getMesh().setGeometryType(GeometryType.QUADS);
            this.scene.addChild(var2);
        }
    }

    /**
     *
     */
    private void yi() {
        int var1 = this.Ue.size();
        if (var1 > 0) {
            RExpandableMesh var2 = new RExpandableMesh();
            var2.setMaterial(this.TZ);

            for (int var3 = 0; var3 < var1; var3 += 3) {
                Vec3f var4 = (Vec3f) this.Uf.get(var3);
                this.a(var2, (Vec3f) this.Ue.get(var3), (Vec3f) this.Ue.get(var3 + 1), (Vec3f) this.Ue.get(var3 + 2), var3, var4.x, var4.y, var4.z);
            }
            this.scene.addChild(var2);
        }
    }

    /**
     * @param var1
     * @param var2
     * @param var3
     * @param var4
     */
    public void a(Vec3f var1, Vec3f var2, Vec3f var3, Vec3f var4) {
        this.Ue.add(var1);
        this.Ue.add(var2);
        this.Ue.add(var3);
        this.Uf.add(var4);
        this.Uf.add(new Vec3f());
        this.Uf.add(new Vec3f());
    }

    /**
     * @param var1
     * @param var2
     * @param var3
     * @param var4
     * @param var5
     * @param var6
     */
    public void a(Vec3f var1, Vec3f var2, Vec3f var3, float var4, float var5, float var6) {
        this.a(new Vec3f(var1), new Vec3f(var2), new Vec3f(var3), new Vec3f(var4, var5, var6));
    }

    /**
     * @param var1
     * @param var2
     * @param var3
     */
    public void a(Vec3f var1, Vec3f var2, Vec3f var3) {
        this.a(new Vec3f(var1), new Vec3f(var2), new Vec3f(var3), TW);
    }

    /**
     * @param var1
     * @param var2
     * @param var3
     */
    public void b(Vec3f var1, Vec3f var2, Vec3f var3) {
        this.a(new Vec3f(var1), new Vec3f(var2), new Vec3f(var3), TX);
    }

    /**
     *
     */
    public void yj() {
        this.Ue.clear();
        this.Uf.clear();
    }
}
