import all.LogPrinter;
import all.OrientationBase;
import all.Vector3dOperations;
import all.WatchDog;
import com.hoplon.geometry.Vec3f;
import com.sun.opengl.util.Animator;
import taikodom.render.RenderGui;
import taikodom.render.RenderView;
import taikodom.render.SceneView;
import taikodom.render.StepContext;
import taikodom.render.camera.Camera;
import taikodom.render.camera.ViewCameraControl;
import taikodom.render.enums.BlendType;
import taikodom.render.gui.GAim;
import taikodom.render.gui.GuiScene;
import taikodom.render.loader.FileSceneLoader;
import taikodom.render.scene.*;
import taikodom.render.shaders.Shader;
import taikodom.render.shaders.ShaderPass;
import taikodom.render.textures.ChannelInfo;
import taikodom.render.textures.Material;
import taikodom.render.textures.Texture;

import javax.media.opengl.*;
import java.awt.*;
import java.awt.event.*;
import java.io.PrintWriter;

/**
 * RayTest
 */
public class test_RayTest extends Frame implements MouseListener, MouseMotionListener, MouseWheelListener, GLEventListener {
    static test_RayTest demo;
    final SceneView sceneView = new SceneView();
    int fps;
    int secondCounter;
    boolean lockedTarget;
    GAim aim;
    StepContext stepContext;
    int mousex;
    int mousey;
    Texture dustTex;
    Texture trailTex;
    int displayCount = 0;
    long timeToDisplay = 0L;
    boolean isRendering = false;
    private Scene scene;
    private RenderView rv;
    private long before = System.nanoTime();
    private long startTime = System.nanoTime();
    private FileSceneLoader loader;
    private Camera camera;
    private ViewCameraControl cameraControl = new ViewCameraControl();
    private String resourceDir;
    private Vector3dOperations cameraPosition;
    private Vector3dOperations craftPosition;
    private Vector3dOperations tempVect = new Vector3dOperations();
    private Vec3f craftRotation;
    private OrientationBase cameraOrientation;
    private OrientationBase finalCameraOrientation;
    private GuiScene guiScene = new GuiScene("GuiScene");
    private RenderGui renderGui = new RenderGui();
    private Vec3f Dir = new Vec3f();
    private RRay ray;
    private SceneObject bull;
    private SceneObject barracuda;
    private double craftSpeed = 0.0D;
    private WatchDog watchDog;
    private Vec3f craftVelocity;

    /**
     * Конструктор класса
     */
    public test_RayTest() {
        super("Basic Aim Demo");
        this.setLayout(new BorderLayout());
        this.setSize(720, 640);//Размер эерана
        this.setLocation(400, 200);//размещение на мониторе
        //this.setAlwaysOnTop(true);//всегда находиться над другими окнами
        this.setVisible(true);
        this.setupJOGL();
        this.addWindowListener(new WindowAdapter()//Дейчтвия с окном
        {
            public void windowClosing(WindowEvent var1) {
                System.exit(0);
            }

            public void windowClosed(WindowEvent var1) {
                System.exit(0);
            }
        });
    }

    public static void main(String[] var0) {
        demo = new test_RayTest();
        demo.resourceDir = ".";// = "../taikodom.client.resource/";
        if (var0.length > 0) {
            demo.resourceDir = var0[0];
        }
        demo.setVisible(true);
        System.out.println("DEBUG2 - main setVisible");
    }

    /**
     * Настройка  Java OpenGL
     */
    private void setupJOGL() {
        /**GLEventListener - Чтобы ваша программа могла использовать графический API JOGL
         */
        GLCapabilities var1 = new GLCapabilities();//Определяет набор возможностей OpenGL.
        var1.setDoubleBuffered(true);//Включает или отключает двойную буферизацию.
        var1.setHardwareAccelerated(true);//Включает или отключает аппаратное ускорение.

        GLCanvas var2 = new GLCanvas(var1);
        var2.enableInputMethods(true);//Включает или отключает поддержку метода ввода для этого компонента.
        //Подписываемся на события
        var2.addGLEventListener(this);//Добавляет данный слушатель в конец этой очереди.
        var2.addMouseMotionListener(this);
        var2.addMouseListener(this);
        var2.addMouseWheelListener(this);

        this.add(var2, "Center");
        //слушатель курсора
        var2.addMouseMotionListener(new MouseMotionListener() {
            @Override
            public void mouseDragged(MouseEvent e) {
            }

            @Override
            public void mouseMoved(MouseEvent var1) {
                test_RayTest.this.mousex = var1.getX();
                test_RayTest.this.mousey = var1.getY();
            }
        });
        //Слушатель клавиатуры
        var2.addMouseListener(new MouseListener() {
            public void mouseClicked(MouseEvent var1) {
                test_RayTest.this.tempVect.setVec3f(test_RayTest.this.Dir);
                test_RayTest.this.tempVect.scale(-50.0D);
                test_RayTest.this.tempVect.add(test_RayTest.this.craftPosition);
                test_RayTest.this.ray.trigger(test_RayTest.this.tempVect, test_RayTest.this.Dir, 100.0F);
            }

            public void mouseEntered(MouseEvent var1) {
            }

            public void mouseExited(MouseEvent var1) {
            }

            public void mousePressed(MouseEvent var1) {
            }

            public void mouseReleased(MouseEvent var1) {
            }
        });
        //Слушатель клавиатуры
        var2.addKeyListener(new KeyListener() {
            public void keyPressed(KeyEvent var1) {
                if (var1.getKeyCode() == 87) {//VK_W
                    test_RayTest.this.craftSpeed = test_RayTest.this.craftSpeed - 0.009999999776482582D;
                }

                if (var1.getKeyCode() == 83) {//VK_S
                    test_RayTest.this.craftSpeed = test_RayTest.this.craftSpeed + 0.009999999776482582D;
                }

                if (var1.getKeyCode() == 65) {//VK_A
                    ++test_RayTest.this.craftRotation.x;
                }

                if (var1.getKeyCode() == 68) {//VK_D
                    --test_RayTest.this.craftRotation.x;
                }

                if (var1.getKeyCode() == 49) {//VK_1
                    test_RayTest.this.lockedTarget = !test_RayTest.this.lockedTarget;
                    if (test_RayTest.this.lockedTarget) {
                        System.out.println("Target locked");
                        test_RayTest.this.aim.setTarget(test_RayTest.this.barracuda);
                    } else {
                        System.out.println("Target unocked");
                        test_RayTest.this.aim.setTarget((SceneObject) null);
                    }
                }

            }

            public void keyTyped(KeyEvent var1) {
            }

            public void keyReleased(KeyEvent var1) {
            }

        });
        Animator var3 = new Animator(var2);
        var3.start();
    }

    /**
     * Он вызывается объектом интерфейса GLAutoDrawable сразу после инициализации GLContext OpenGL.
     *
     * @param var1 Поверхность для рисования OpenGL и примитивы рисования(линии треугольники).
     */
    public void init(GLAutoDrawable var1) {
        try {
            this.rv = new RenderView(var1.getContext());
            this.rv.setViewport(0, 0, var1.getWidth(), var1.getHeight());
            var1.setGL(new DebugGL(var1.getGL()));
            this.rv.getDrawContext().setCurrentFrame(1000L);
            GL var2 = var1.getGL();
            this.rv.setGL(var2);
            this.renderGui.getGuiContext().setDc(this.rv.getDrawContext());
            this.rv.getDrawContext().setRenderView(this.rv);
            this.loader = new FileSceneLoader(this.resourceDir);
            System.loadLibrary("granny2");
            System.loadLibrary("mss32");
            System.loadLibrary("binkw32");
            System.loadLibrary("utaikodom.render");
            this.loader.loadFile("data/shaders/light_shader.pro");
            this.loader.loadFile("data/models/fig_spa_bullfrog.pro");//Корабль
            this.loader.loadFile("data/models/fig_spa_barracuda.pro");
            this.loader.loadFile("data/scene/nod_spa_mars.pro");//сцена космоса марс скайбокс, звёзды, бланеты
            this.loader.loadFile("data/trails.pro");
            this.loader.loadFile("data/effects.pro");
            this.loader.loadFile("data/models/amm_min_contramed1.pro");
            this.loader.loadFile("data/hud.pro");//интерфейс
            this.loader.loadFile("data/projectiles.pro");
            this.loader.loadFile("data/dummy_data.pro");
            this.loader.loadMissing();

            Texture var3 = (Texture) this.loader.instantiateAsset("data/gfx/misc/rail_gun.dds");
            Shader var4 = new Shader();
            var4.setName("Default billboard shader");
            ShaderPass var5 = new ShaderPass();
            var5.setName("Default billboard pass");
            ChannelInfo var6 = new ChannelInfo();
            var6.setTexChannel(8);
            var5.setChannelTextureSet(0, var6);
            var5.setBlendEnabled(true);
            var5.setAlphaTestEnabled(true);
            var5.setDepthMask(false);
            var5.setDstBlend(BlendType.ONE);
            var5.setSrcBlend(BlendType.SRC_ALPHA);
            var5.setCullFaceEnabled(false);
            var4.addPass(var5);
            Material var7 = new Material();
            var7.setShader(var4);
            var7.setDiffuseTexture(var3);

            //Настройка камеры
            this.camera = this.sceneView.getCamera();
            this.camera.setFarPlane(400000.0F);//дальность прорисовки
            this.camera.setNearPlane(1.0F);//близость прорисовки
            this.camera.setFovY(75.0F);//Угол обзора
            this.camera.setAspect(1.3333F);
            this.camera.getTransform().setTranslation(20.0D, 20.0D, 20.0D);

            this.scene = this.sceneView.getScene();
            this.craftRotation = new Vec3f();
            this.craftPosition = new Vector3dOperations();
            this.cameraPosition = new Vector3dOperations();
            this.craftVelocity = new Vec3f();
            this.cameraOrientation = new OrientationBase();
            this.finalCameraOrientation = new OrientationBase();
            this.cameraControl.setCamera(this.camera);

            //Создание объектов на сцене
            this.bull = (SceneObject) this.loader.instantiateAsset("fig_spa_bullfrog");//Мой
            RTrail var8 = (RTrail) this.loader.instantiateAsset("trail_green");//выхлоп движка
            var8.setLifeTime(10000.0F);
            var8.setTrailWidth(8.0F);
            this.bull.addChild(var8);
            var8 = (RTrail) this.loader.instantiateAsset("trail_blue");
            var8.setPosition(0.0D, 0.0D, -5.0D);
            this.bull.addChild(var8);
            this.ray = new RRay();//луч выстрел
            RBillboard var9 = new RBillboard();
            var9.setMaterial(var7);
            var9.setSize(10.5F, 10.0F);
            this.ray.setRay(var9, 30000.0F);
            this.bull.addChild(this.ray);

            this.barracuda = (SceneObject) this.loader.instantiateAsset("fig_spa_barracuda");//Цель
            this.barracuda.setPosition(0.0F, 0.0F, -100.0F);

            SceneObject var10 = (SceneObject) this.loader.instantiateAsset("nod_spa_mars");//кабмап

            this.scene.addChild(var10);
            this.scene.addChild(this.bull);
            this.scene.addChild(this.barracuda);


            this.aim = (GAim) this.loader.instantiateAsset("hud_target_aim");//целеуказатель
            this.aim.setSceneView(this.sceneView);
            this.aim.setShip(this.bull, this.sceneView);
            this.guiScene.addChild(this.aim);

            System.nanoTime();
        } catch (Exception var11) {
            var11.printStackTrace();
            System.exit(0);
        }

    }

    /**
     * Вызывается постоянно. Этот метод содержит логику, используемую для рисования графических элементов с использованием OpenGL API.
     *
     * @param var1 Поверхность для рисования OpenGL и примитивы рисования(линии треугольники).
     */
    public void display(GLAutoDrawable var1) {
        try {
            long var2 = System.currentTimeMillis();
            if (this.watchDog == null) {
                this.watchDog = new WatchDog();
                PrintWriter var4 = LogPrinter.createLogFile("client-watchdog", "log");
                var4.println("Starting RTrail test");
                this.watchDog.setPrintWriter(var4);
                this.watchDog.ht(200L);//сон потока
                this.watchDog.K(2.0E10D);
                this.watchDog.start();
            }

            this.rv.getDrawContext().setViewport(0, 0, this.getSize().width, this.getSize().height);
            this.rv.getRenderInfo().setDrawAABBs(true);//отрисовка
            long var7 = var2 - this.startTime;
            this.craftRotation.x -= (float) (this.mousex - demo.getWidth() / 2) * 0.001F;
            this.craftRotation.y -= (float) (this.mousey - demo.getHeight() / 2) * 0.001F;
            this.finalCameraOrientation.h((double) this.craftRotation.y * 0.017453292519943295D, (double) this.craftRotation.x * 0.017453292519943295D, (double) this.craftRotation.z);
            this.Dir.set(0.0F, 0.0F, 1.0F);
            this.bull.getTransform().multiply3x3(this.Dir, this.Dir);
            this.bull.getTransform().setOrientation(this.finalCameraOrientation);
            this.craftVelocity.set((float) this.craftSpeed * this.Dir.x, (float) this.craftSpeed * this.Dir.y, (float) this.craftSpeed * this.Dir.z);
            this.craftPosition.add(this.craftVelocity);
            this.cameraPosition.set(0.0D, 7.0D, 20.0D);
            this.bull.getTransform().multiply3x4(this.cameraPosition, this.cameraPosition);
            this.bull.setPosition(this.craftPosition);
            this.bull.setVelocity(this.craftVelocity);
            this.camera.getTransform().setIdentity();
            this.cameraOrientation.a(this.cameraOrientation, this.finalCameraOrientation, 0.05F);
            this.camera.setOrientation(this.cameraOrientation);
            this.camera.getTransform().setTranslation(this.cameraPosition);
            this.camera.calculateProjectionMatrix();
            if (this.stepContext == null) {
                this.stepContext = new StepContext();
            }

            this.stepContext.setCamera(this.camera);
            this.stepContext.setDeltaTime((float) (var2 - this.before));
            this.scene.step(this.stepContext);
            this.guiScene.step(this.stepContext);
            this.timeToDisplay += var2 - this.before;
            this.rv.render(this.sceneView, (double) ((float) var7 * 1.0E-9F), (float) (var2 - this.before) * 1.0E-9F);
            this.renderGui.render(this.guiScene);
            this.before = var2;
            if (this.timeToDisplay > 1000000000L) {
                this.timeToDisplay = 0L;
            }

        } catch (Exception var6) {
            var6.printStackTrace();
            System.exit(0);
        }
    }

    /**
     * Он вызывается объектом интерфейса GLAutoDrawable во время первой перерисовки после изменения размера компонента.
     * Он также вызывается всякий раз, когда положение компонента в окне изменяется.
     *
     * @param glAutoDrawable Поверхность для рисования для команд OpenGL.
     * @param x
     * @param y
     * @param width
     * @param height
     */
    public void reshape(GLAutoDrawable glAutoDrawable, int x, int y, int width, int height) {
        if (this.camera != null) {
            this.camera.setAspect((float) width / (float) height);
        }

        this.sceneView.setViewport(0, 0, width, height);
        this.renderGui.resize(x, y, width, height);
    }

    /**
     * Вызывается при извлечении, когда изменился режим отображения или устройство отображения, связанное с GLAutoDrawable.
     * Два логических параметра указывают типы изменений, которые произошли.
     *
     * @param glAutoDrawable Поверхность для рисования для команд OpenGL.
     * @param modeChanged    Примером изменения режима отображения является изменение глубины бита (например, от 32-битного до 16-битного цвета) на мониторе, на котором в настоящее время отображается GLAutoDrawable.
     * @param deviceChanged  Примером изменения отображения устройства является то, что пользователь перетаскивает окно, содержащее GLAutoDrawable, с одного монитора на другой в настройке с несколькими мониторами.
     */
    public void displayChanged(GLAutoDrawable glAutoDrawable, boolean modeChanged, boolean deviceChanged) {
        this.rv.setViewport(0, 0, glAutoDrawable.getWidth(), glAutoDrawable.getHeight());
    }

    public void mouseMoved(MouseEvent var1) {
    }

    public void mouseClicked(MouseEvent var1) {
    }

    public void mouseEntered(MouseEvent var1) {
    }

    public void mouseExited(MouseEvent var1) {
    }

    public void mousePressed(MouseEvent var1) {
    }

    public void mouseReleased(MouseEvent var1) {
    }

    public void mouseWheelMoved(MouseWheelEvent var1) {
        if (var1.getScrollType() == 0) {
            int var2 = var1.getWheelRotation();
            if (var2 > 0) {
                this.cameraControl.zoomOut((float) var2);
            } else {
                this.cameraControl.zoomIn((float) (-var2));
            }
        }

    }

    public void mouseDragged(MouseEvent var1) {
    }
}
