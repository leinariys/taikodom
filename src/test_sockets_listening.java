import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

//endPage.java
//
public class test_sockets_listening {
    private static byte[] data = new byte[255];

    public static void main(String[] paramArrayOfString) throws IOException {
        for (int i = 0; i < data.length; i++) {
            data[i] = ((byte) i);
        }
        ServerSocketChannel localServerSocketChannel = ServerSocketChannel.open();
        localServerSocketChannel.configureBlocking(false);
        localServerSocketChannel.socket().bind(new java.net.InetSocketAddress(8989));
        Selector localSelector = Selector.open();
        localServerSocketChannel.register(localSelector, 16);
        Iterator localIterator;

        System.out.println("localServerSocketChannel " + localServerSocketChannel.toString());
        while (true) {
            localSelector.select();
            Set localSet = localSelector.selectedKeys();
            localIterator = localSet.iterator();
            System.out.println("localIterator " + localIterator.toString());
            while (localIterator.hasNext()) {
                SelectionKey localSelectionKey1 = (SelectionKey) localIterator.next();
                localIterator.remove();
                try {
                    SocketChannel localSocketChannel;
                    ByteBuffer localByteBuffer;
                    if (localSelectionKey1.isAcceptable()) {
                        localSocketChannel = localServerSocketChannel.accept();
                        System.out.println("Accepted connection from " + localSocketChannel);
                        localSocketChannel.configureBlocking(false);
                        localByteBuffer = ByteBuffer.wrap(data);
                        SelectionKey localSelectionKey2 = localSocketChannel.register(localSelector, 4);
                        localSelectionKey2.attach(localByteBuffer);
                    } else if (localSelectionKey1.isWritable()) {
                        localSocketChannel = (SocketChannel) localSelectionKey1.channel();
                        localByteBuffer = (ByteBuffer) localSelectionKey1.attachment();
                        if (!localByteBuffer.hasRemaining()) {
                            localByteBuffer.rewind();
                        }
                        localSocketChannel.write(localByteBuffer);
                    }
                } catch (IOException localIOException1) {
                    localSelectionKey1.cancel();
                    System.out.println("localIOException1 " + localIOException1.toString());
                    try {
                        localSelectionKey1.channel().close();
                        System.out.println("localIOException1 " + localIOException1.toString());
                    } catch (IOException localIOException2) {
                        System.out.println("localIOException2 " + localIOException2.toString());
                    }
                }
            }
        }
    }
}

