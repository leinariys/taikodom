import all.PoolThread;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class test_sockets_client {

    public static void main(String[] var0) {
        PoolThread.g(new Runnable() {
            public void run() {
                try {
                    new test_sockets_client.b();
                } catch (IOException var2) {
                    var2.printStackTrace();
                }

            }
        });
        PoolThread.sleep(1000L);
        try {
            new test_sockets_client.c();
        } catch (IOException var2) {
            var2.printStackTrace();
        }
    }

    public static class c {
        public c() throws IOException {
            Socket var1 = new Socket("localhost", 8989);
            InputStream var2 = var1.getInputStream();
            OutputStream var3 = var1.getOutputStream();
            long var4 = 0L;
            long var6 = System.currentTimeMillis();

            while (true) {
                long var8;
                do {
                    var3.write(1);
                    var3.flush();
                    var2.read();
                    ++var4;
                    var8 = System.currentTimeMillis();
                } while (var8 - var6 <= 1000L);

                System.out.println("---> " + var4);
                var6 = var8;
                var4 = 0L;
            }
        }
    }

    public static class b {
        public b() throws IOException {
            ServerSocket var1 = new ServerSocket(8989, 100);
            Socket var2 = var1.accept();
            InputStream var3 = var2.getInputStream();
            OutputStream var4 = var2.getOutputStream();

            while (true) {
                var3.read();
                var4.write(1);
                var4.flush();
            }
        }
    }
}
