import all.amC;
import all.cO_q;
import com.sun.opengl.util.FPSAnimator;
import taikodom.render.DrawContext;
import taikodom.render.RenderView;
import taikodom.render.gl.GLWrapper;

import javax.media.opengl.*;
import javax.swing.*;
import java.awt.*;

/**
 * https://www.tutorialspoint.com/jogl/jogl_api_basic_template.htm
 * class no
 */
public class test_GLCanvas {
    /**
     * @param var0
     */
    public static void main(String[] var0) {
        JFrame jFrame = new JFrame();//Создать окно

        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jFrame.setBounds(100, 100, 1050, 800);
        jFrame.setLayout(new BorderLayout());


        /**
         * GLCapabilities указывает неизменный набор возможностей OpenGL.
         */
        GLCapabilities glCapabilities = new GLCapabilities();
        glCapabilities.setDoubleBuffered(true);
        glCapabilities.setHardwareAccelerated(true);
        glCapabilities.setDepthBits(24);
        glCapabilities.setStencilBits(8);
        glCapabilities.setRedBits(8);
        glCapabilities.setBlueBits(8);
        glCapabilities.setGreenBits(8);
        glCapabilities.setAlphaBits(8);

        /**GLCanvas и GLJPanel или JGLDesktop(своя реализация) - это два основных класса графического интерфейс
         * Всякий раз, когда вы сталкиваетесь с проблемами с GLJCanvas , вы должны использовать класс GLJPanel .
         */
        final GLJPanel gljPanel = new GLJPanel(glCapabilities);
        /**
         * GLEventListener - Чтобы ваша программа могла использовать графический API JOGL
         */
        gljPanel.addGLEventListener(new GLEventListener() {
            private DrawContext dc;
            private cO_q gvG;

            /**
             * Он вызывается объектом интерфейса GLAutoDrawable сразу после инициализации GLContext OpenGL.
             * @param var1  Поверхность для рисования OpenGL и примитивы рисования(линии треугольники).
             */
            public void init(GLAutoDrawable var1) {
                RenderView var2 = new RenderView(gljPanel);
                this.dc = var2.getDrawContext();
                this.dc.setGL(new GLWrapper(gljPanel.getGL()));
                this.dc.setCurrentFrame(10L);
                RepaintManager.setCurrentManager(new amC());//Менеджер перерисовки
                (new FPSAnimator(gljPanel, 200, true)).start();
                this.gvG = new cO_q("Tester");
                this.gvG.getDesktopPane().setSize(500, 500);

                JComboBox var4x = new JComboBox();
                this.gvG.getDesktopPane().add(var4x);
                var4x.setBounds(100, 100, 100, 50);//Перемещает и изменяет размер этого компонента.

                JButton var3x = new JButton("bla");
                this.gvG.getDesktopPane().add(var3x);
                var3x.setBounds(10, 10, 50, 20);//Перемещает и изменяет размер этого компонента.

                this.gvG.c(800, 600, true);

            }

            /**
             *этот метод содержит логику, используемую для рисования графических элементов с использованием OpenGL API.
             * @param var1 Поверхность для рисования OpenGL и примитивы рисования(линии треугольники).
             */
            public void display(GLAutoDrawable var1) {
                /**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
                 * седержит методы для рисования примитивов (линии треугольники)*/
                javax.media.opengl.GL var2 = this.dc.getGL();

                var2.glBegin(GL.GL_TRIANGLES);//Этот метод запускает процесс рисования
                var2.glColor3f(1.0f, 0.0f, 0.0f);   // Red
                var2.glVertex3f(0.5f, 0.7f, 0.0f);    // Top
                var2.glColor3f(0.0f, 1.0f, 0.0f);     // green
                var2.glVertex3f(-0.2f, -0.50f, 0.0f); // Bottom Left
                var2.glColor3f(0.0f, 0.0f, 1.0f);     // blue
                var2.glVertex3f(0.5f, -0.5f, 0.0f);   // Bottom Right
                var2.glEnd();


                var2.glClearColor(0.0F, 150.0F, 0.5F, 0.0F);//фон
                //glCapabilities.glClear(GL.GL_COLOR_BUFFER_BIT);//очистить буферы до заданных значений
                if (this.dc != null) {
                    this.gvG.draw(this.dc);
                }
            }


            /**
             *
             * @param glAutoDrawable Поверхность для рисования для команд OpenGL.
             * @param b
             * @param b1
             */
            @Override
            public void displayChanged(GLAutoDrawable glAutoDrawable, boolean b, boolean b1) {
            }

            /**
             * Он вызывается объектом интерфейса GLAutoDrawable во время первой перерисовки после изменения размера компонента.
             * Он также вызывается всякий раз, когда положение компонента в окне изменяется.
             * @param glAutoDrawable Поверхность для рисования для команд OpenGL.
             * @param x
             * @param y
             * @param width
             * @param height
             */
            @Override
            public void reshape(GLAutoDrawable glAutoDrawable, int x, int y, int width, int height) {
            }

        });

        jFrame.add(gljPanel);
        jFrame.setVisible(true);
    }
}
