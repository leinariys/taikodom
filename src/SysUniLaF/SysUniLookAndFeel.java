package SysUniLaF;

import javax.swing.*;
import javax.swing.plaf.basic.BasicLookAndFeel;

public class SysUniLookAndFeel extends BasicLookAndFeel {

    private UIDefaults uiDefaults;

    @Override
    public String getID() {
        return "SysUni";
    }

    @Override
    public String getName() {
        return "SysUni Look And Feel";
    }

    @Override
    public String getDescription() {
        return "SysUni Look And Feel";
    }

    @Override
    public boolean isNativeLookAndFeel() {
        return true;
    }

    @Override
    public boolean isSupportedLookAndFeel() {
        return true;
    }


    //для «подмены» UI класса
    @Override
    protected void initClassDefaults(UIDefaults var1) {
        super.initClassDefaults(var1);
        Object[] uiDefaults = new Object[]
                {
                        "ButtonUI", ButtonUI.class.getName()
                };
        var1.putDefaults(uiDefaults);
    }

    public UIDefaults getDefaults() {
        if (this.uiDefaults == null) {
            this.uiDefaults = new UIDefaults(610, 0.75F);
            this.initClassDefaults(this.uiDefaults);//для «подмены» UI класса
            this.initSystemColorDefaults(this.uiDefaults); // стандартные цвета различных элементых
            this.initComponentDefaults(this.uiDefaults);//определяются шрифты, цвета, горячие клавиши, бордеры и прочие мелочи
        }

        return this.uiDefaults;
    }
}
