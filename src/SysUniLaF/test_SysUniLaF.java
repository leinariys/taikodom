package SysUniLaF;

import javax.swing.*;
import javax.swing.table.JTableHeader;
import java.awt.*;

public class test_SysUniLaF {

    public static void main(String[] var0) {
        System.out.println("Начало теста");
        JFrame.setDefaultLookAndFeelDecorated(true);
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createGUI();
            }
        });
    }


    private static void createGUI() {

        try {
            if (!(UIManager.getLookAndFeel() instanceof SysUniLookAndFeel)) {
                UIManager.setLookAndFeel(new SysUniLookAndFeel());
            }
        } catch (UnsupportedLookAndFeelException var2) {
            var2.printStackTrace();
        }


        JFrame frame = new JFrame("Test frame");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setPreferredSize(new Dimension(500, 800));
        frame.getRootPane().setBounds(0, 0, 800, 800);
        // frame.getRootPane().setBackground(new Color(0x00D823));


        JDesktopPane jDesktopPane1 = new JDesktopPane();
        jDesktopPane1.setName("!!!jDesktopPane1!!!");
        frame.setContentPane(jDesktopPane1);
        jDesktopPane1.setBounds(0, 0, 500, 500);
        // jDesktopPane1.setBackground(new Color(0x09D8D5));


        JButton JButto = new JButton();
        JButto.setText("JButton");
        JButto.setBounds(10, 0, 80, 20);
        jDesktopPane1.add(JButto);

        JCheckBox JCheckBo = new JCheckBox();
        JCheckBo.setText("JCheckBox");
        JCheckBo.setBounds(10, 20, 80, 20);
        jDesktopPane1.add(JCheckBo);

        JColorChooser JColorChoose = new JColorChooser();
        JColorChoose.setBounds(900, 0, 300, 300);
        jDesktopPane1.add(JColorChoose);

        JFormattedTextField JFormattedTextFiel = new JFormattedTextField();
        JFormattedTextFiel.setText("JFormattedTextField");
        JFormattedTextFiel.setBounds(10, 40, 120, 20);
        jDesktopPane1.add(JFormattedTextFiel);


        JMenuBar JMenuBa = new JMenuBar();
        frame.setJMenuBar(JMenuBa);
        JMenu JMenu1 = new JMenu("JMenu1");
        JMenuBa.add(JMenu1);
        JMenu JMenu2 = new JMenu("JMenu2");
        JMenu1.add(JMenu2);
        JMenuItem JMenuIte = new JMenuItem("Text file");
        JMenu2.add(JMenuIte);
        JRadioButtonMenuItem JRadioButtonMenuIte = new JRadioButtonMenuItem("JRadioButtonMenuItem");
        JMenu2.add(JRadioButtonMenuIte);


        ButtonGroup group = new ButtonGroup();
        JRadioButton smallButton = new JRadioButton("Small", false);
        smallButton.setBounds(10, 60, 80, 20);
        jDesktopPane1.add(smallButton);
        group.add(smallButton);
        JRadioButton mediumButton = new JRadioButton("Medium", true);
        mediumButton.setBounds(10, 80, 80, 20);
        jDesktopPane1.add(mediumButton);
        group.add(mediumButton);


        JToggleButton JToggleButto = new JToggleButton("Medium", true);
        JToggleButto.setBounds(10, 100, 80, 20);
        jDesktopPane1.add(JToggleButto);


        JMenuItem cutAction = new JMenuItem("Cut");
        JMenu editMenu = new JMenu("Edit");
        editMenu.add(cutAction);
        JPopupMenu JPopupMen = new JPopupMenu("JPopupMenu");  //контекстное меню
        JPopupMen.setBounds(10, 120, 80, 20);
        JPopupMen.add(cutAction);
        jDesktopPane1.setComponentPopupMenu(JPopupMen);


        JProgressBar JProgressBa = new JProgressBar();
        JProgressBa.setBounds(10, 140, 300, 20);
        JProgressBa.setValue(10);
        JProgressBa.setStringPainted(true);
        JProgressBa.setString("setString");
        jDesktopPane1.add(JProgressBa);

        JScrollBar JScrollBa = new JScrollBar();
        JScrollBa.setBounds(10, 160, 300, 40);
        jDesktopPane1.add(JScrollBa);

        JScrollPane JScrollPan = new JScrollPane();
        JScrollPan.setBounds(10, 200, 300, 40);
        jDesktopPane1.add(JScrollPan);

        JSplitPane JSplitPan = new JSplitPane();
        JSplitPan.setBounds(10, 240, 300, 20);
        jDesktopPane1.add(JSplitPan);

        JSlider JSlide = new JSlider(0, 80, 20);
        JSlide.setBounds(10, 260, 300, 20);
        jDesktopPane1.add(JSlide);

        JSeparator JSeparato = new JSeparator(JSeparator.VERTICAL);
        JSeparato.setBounds(10, 280, 300, 20);
        jDesktopPane1.add(JSeparato);


        SpinnerModel model = new SpinnerNumberModel(500.0, 0.0, 1000.0, 0.625);
        JSpinner JSpinne = new JSpinner(model);
        JSpinne.setBounds(10, 300, 80, 20);
        jDesktopPane1.add(JSpinne);


//  ToolBarSeparator

        JTabbedPane JTabbedPan = new JTabbedPane();
        JTabbedPan.addTab("Вкладка 0", new JPanel());
        JTabbedPan.addTab("Вкладка 1", new JPanel());
        JTabbedPan.addTab("Вкладка 2", new JPanel());
        JTabbedPan.setBounds(180, 10, 180, 120);
        jDesktopPane1.add(JTabbedPan);

        JTextArea JTextAre = new JTextArea();
        JTextAre.setText("JTextArea");
        JTextAre.setBounds(10, 320, 80, 20);
        jDesktopPane1.add(JTextAre);


        JTextField JTextFiel = new JTextField();
        JTextFiel.setText("JTextField");
        JTextFiel.setBounds(10, 340, 80, 20);
        jDesktopPane1.add(JTextFiel);

        JPasswordField JPasswordFiel = new JPasswordField();
        JPasswordFiel.setText("JPasswordField");
        JPasswordFiel.setBounds(10, 360, 80, 20);
        jDesktopPane1.add(JPasswordFiel);

        JTextPane JTextPan = new JTextPane();
        JTextPan.setText("JTextPane");
        JTextPan.setBounds(10, 380, 80, 20);
        jDesktopPane1.add(JTextPan);

        JEditorPane JEditorPan = new JEditorPane();
        JEditorPan.setText("JEditorPane");
        JEditorPan.setBounds(10, 400, 80, 20);
        jDesktopPane1.add(JEditorPan);

        JTree tree1 = new JTree(new String[]{"Напитки", "Сладости"});
        tree1.setBounds(10, 420, 180, 120);
        jDesktopPane1.add(tree1);

        JLabel JLabe = new JLabel();
        JLabe.setText("JLabel");
        JLabe.setBounds(10, 540, 80, 20);
        jDesktopPane1.add(JLabe);

        JList JLis = new JList(new String[]{"Напитки", "Сладости"});
        JLis.setBounds(10, 560, 80, 60);
        jDesktopPane1.add(JLis);

        // Создание панелей инструментов
        JToolBar tbCommon = new JToolBar();
        tbCommon.addSeparator();
        tbCommon.add(new JButton("Стиль"));
        tbCommon.addSeparator();
        tbCommon.setBounds(10, 620, 80, 20);
        jDesktopPane1.add(tbCommon);


        JToolTip JToolTi = new JToolTip();
        JToolTi.setToolTipText("JToolTip");
        JToolTi.setBounds(10, 640, 80, 20);
        jDesktopPane1.add(JToolTi);

        String[] items = {"Элемент списка 1", "Элемент списка 2", "Элемент списка 3"};
        JComboBox JComboBo = new JComboBox(items);
        JComboBo.setBounds(10, 660, 180, 20);
        jDesktopPane1.add(JComboBo);

        // Данные для таблицы
        String[][] data = {{"2016.04.05", "68.6753", "78.1662"},
                {"2016.04.06", "68.8901", "78.2798"},
                {"2016.04.07", "68.5215", "78.1662"}};
        // Названия столбцов
        String[] columnNames = {"Дата", "Курс USD", "Курс EUR"};

        JTable JTabl = new JTable(data, columnNames);
        JTableHeader header = JTabl.getTableHeader();
        if (header != null) {
            header.setReorderingAllowed(false);
            header.setResizingAllowed(false);
        }
        JTabl.setBounds(10, 680, 300, 100);
        jDesktopPane1.add(JTabl);


        JInternalFrame internalFrame = new JInternalFrame("Can Do All", true, true, true, true);
        internalFrame.setBounds(25, 25, 200, 100);
        JLabel label = new JLabel(internalFrame.getTitle(), JLabel.CENTER);
        internalFrame.add(label, BorderLayout.CENTER);
        internalFrame.setVisible(true);
        jDesktopPane1.add(internalFrame);

        JOptionPane JOptionPan = new JOptionPane();
        JOptionPan.setBounds(100, 300, 300, 100);
        jDesktopPane1.add(JOptionPan);


        JPanel JPane = new JPanel();
        JPane.setBackground(new Color(0xD800C6));
        JPane.setBounds(200, 400, 100, 100);
        jDesktopPane1.add(JPane);


        JLabel dogLabel = new JLabel("dsfdsgsdfg");
        JViewport viewport = new JViewport();
        viewport.setView(dogLabel);
        jDesktopPane1.setBounds(200, 500, 100, 100);
        jDesktopPane1.add(viewport);

        InputMap inputMap = viewport.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        KeyStroke upKey = KeyStroke.getKeyStroke("UP");
        inputMap.put(upKey, "up");
        KeyStroke downKey = KeyStroke.getKeyStroke("DOWN");
        inputMap.put(downKey, "down");


        //Тест отрисовки
        /*
        RepaintManager.currentManager(frame.getRootPane()).setDoubleBufferingEnabled(false);
        ((JComponent)jDesktopPane1).setDebugGraphicsOptions(DebugGraphics.FLASH_OPTION);
*/
        frame.pack();
        frame.setVisible(true);
    }
}
