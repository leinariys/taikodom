package com.hoplon.geometry;

import all.Vector3dOperations;
import all.Vm_q;
import all.afA_q;
import all.att;

import java.io.Serializable;

public class Vec3l implements afA_q, Serializable {
    private static final long serialVersionUID = 1L;
    public long x;
    public long y;
    public long z;

    public Vec3l(Vec3l var1) {
        this.x = var1.x;
        this.y = var1.y;
        this.z = var1.z;
    }

    public Vec3l(long var1, long var3, long var5) {
        this.x = var1;
        this.y = var3;
        this.z = var5;
    }

    public Vec3l() {
    }

    public static long a(Vec3l var0, Vec3l var1) {
        return (var0.x - var1.x) * (var0.x - var1.x) + (var0.y - var1.y) * (var0.y - var1.y) + (var0.z - var1.z) * (var0.z - var1.z);
    }

    public void a(Vec3l var1) {
        this.x = var1.x;
        this.y = var1.y;
        this.z = var1.z;
    }

    public Vec3l b(Vec3l var1) {
        return new Vec3l(this.x + var1.x, this.y + var1.y, this.z + var1.z);
    }

    public Vec3l c(Vec3l var1) {
        return new Vec3l(this.x - var1.x, this.y - var1.y, this.z - var1.z);
    }

    public boolean equals(Object var1) {
        if (!(var1 instanceof Vec3l)) {
            return false;
        } else {
            Vec3l var2 = (Vec3l) var1;
            return this.x == var2.x && this.y == var2.y && this.z == var2.z;
        }
    }

    public int hashCode() {
        return (int) (this.x + this.y ^ this.y + this.z >>> 32);
    }

    public String toString() {
        return "(" + this.x + ", " + this.y + ", " + this.z + ")";
    }

    public long dfU() {
        return this.x * this.x + this.y * this.y + this.z * this.z;
    }

    public Vec3f dfV() {
        return new Vec3f((float) this.x, (float) this.y, (float) this.z);
    }

    public Vector3dOperations dfR() {
        return new Vector3dOperations((double) this.x, (double) this.y, (double) this.z);
    }

    public void readExternal(Vm_q var1) {
        this.x = var1.gY("x");
        this.y = var1.gY("y");
        this.z = var1.gY("z");
    }

    public void writeExternal(att var1) {
        var1.a("x", this.x);
        var1.a("y", this.y);
        var1.a("z", this.z);
    }
}
