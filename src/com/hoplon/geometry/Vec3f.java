package com.hoplon.geometry;

import all.*;

import javax.vecmath.Tuple3d;
import javax.vecmath.Tuple3f;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;

//TODO на удаление
public final class Vec3f extends Vector3f implements te_w, afA_q {

    public Vec3f() {
    }

    public Vec3f(float[] var1) {
        super(var1);
    }

    public Vec3f(Vector3f var1) {
        super(var1);
    }

    public Vec3f(Vector3d var1) {
        super(var1);
    }

    public Vec3f(Tuple3f var1) {
        super(var1);
    }

    public Vec3f(Tuple3d var1) {
        super(var1);
    }

    public Vec3f(float var1, float var2, float var3) {
        super(var1, var2, var3);
    }

    public Vec3f(Vector3dOperations var1) {
        this.x = (float) var1.x;
        this.y = (float) var1.y;
        this.z = (float) var1.z;
    }

    public Vec3f(IV var1) {
        this.x = (float) var1.x;
        this.y = (float) var1.y;
        this.z = (float) var1.z;
    }

    public Vec3f(IW var1) {
        this.x = var1.x;
        this.y = var1.y;
        this.z = var1.z;
    }

    public static float L(Vec3f var0, Vec3f var1) {
        return (float) Math.sqrt((double) ((var0.x - var1.x) * (var0.x - var1.x) + (var0.y - var1.y) * (var0.y - var1.y) + (var0.z - var1.z) * (var0.z - var1.z)));
    }

    public static float M(Vec3f var0, Vec3f var1) {
        return (var0.x - var1.x) * (var0.x - var1.x) + (var0.y - var1.y) * (var0.y - var1.y) + (var0.z - var1.z) * (var0.z - var1.z);
    }

    public static Vec3f r(Vec3f var0, Vec3f var1, Vec3f var2) {
        Vec3f var3 = var1.j(var0);
        float var4 = var3.length();
        var3 = var3.mS(1.0F / var4);
        Vec3f var5 = var2.j(var0);
        float var6 = var3.dot(var5);
        if (var6 <= 0.0F) {
            return new Vec3f(var0);
        } else {
            return var6 >= 1.0F ? new Vec3f(var1) : var0.i(var3.mS(var6 * var4));
        }
    }

    public static float s(Vec3f var0, Vec3f var1, Vec3f var2) {
        Vec3f var3 = r(var0, var1, var2);
        return L(var3, var2);
    }

    public static Vec3f b(ajK var0, Tuple3f var1) {
        float var2 = var1.x;
        float var3 = var1.y;
        float var4 = var1.z;
        float var5 = var2 * var0.m00 + var3 * var0.m01 + var4 * var0.m02;
        float var6 = var2 * var0.m10 + var3 * var0.m11 + var4 * var0.m12;
        float var7 = var2 * var0.m20 + var3 * var0.m21 + var4 * var0.m22;
        return new Vec3f(var5, var6, var7);
    }

    public static Vec3f c(ajK var0, Tuple3f var1) {
        float var2 = var1.x;
        float var3 = var1.y;
        float var4 = var1.z;
        float var5 = var2 * var0.m00 + var3 * var0.m01 + var4 * var0.m02 + var0.m03;
        float var6 = var2 * var0.m10 + var3 * var0.m11 + var4 * var0.m12 + var0.m13;
        float var7 = var2 * var0.m20 + var3 * var0.m21 + var4 * var0.m22 + var0.m23;
        return new Vec3f(var5, var6, var7);
    }

    public static Vec3f b(float var0, Tuple3f var1, Tuple3f var2) {
        return (new Vec3f()).a(var0, var1, var2);
    }

    public static Vec3f d(Tuple3d var0, Tuple3d var1) {
        return (new Vec3f()).c(var0, var1);
    }

    public Vec3f mS(float var1) {
        float var2 = this.x * var1;
        float var3 = this.y * var1;
        float var4 = this.z * var1;
        return new Vec3f(var2, var3, var4);
    }

    public Vec3f f(Tuple3f var1) {
        return new Vec3f(this.x * var1.x, this.y * var1.y, this.z * var1.z);
    }

    public Vec3f g(Tuple3f var1) {
        this.x *= var1.x;
        this.y *= var1.y;
        this.z *= var1.z;
        return this;
    }

    public void h(Tuple3f var1) {
        this.x *= var1.x;
        this.y *= var1.y;
        this.z *= var1.z;
    }

    public Vec3f i(Tuple3f var1) {
        return new Vec3f(this.x + var1.x, this.y + var1.y, this.z + var1.z);
    }

    public Vec3f j(Tuple3f var1) {
        return new Vec3f(this.x - var1.x, this.y - var1.y, this.z - var1.z);
    }

    public void sub(Tuple3d var1, Tuple3d var2) {
        this.x = (float) (var1.x - var2.x);
        this.y = (float) (var1.y - var2.y);
        this.z = (float) (var1.z - var2.z);
    }

    public void cox() {
        float var1 = this.length();
        if (var1 > 0.0F) {
            this.x /= var1;
            this.y /= var1;
            this.z /= var1;
        }

    }

    public Vec3f b(Vector3f var1) {
        return new Vec3f(this.y * var1.z - this.z * var1.y, this.z * var1.x - this.x * var1.z, this.x * var1.y - this.y * var1.x);
    }

    public String toString() {
        return "(" + this.x + ", " + this.y + ", " + this.z + ")";
    }

    public Vec3f dfO() {
        double var1 = Math.sqrt((double) (this.x * this.x + this.y * this.y + this.z * this.z));
        if (var1 > 0.0D) {
            float var3 = (float) (1.0D / var1);
            float var4 = this.x * var3;
            float var5 = this.y * var3;
            float var6 = this.z * var3;
            return new Vec3f(var4, var5, var6);
        } else {
            return new Vec3f(this);
        }
    }

    public Vec3f dfP() {
        double var1 = Math.sqrt((double) (this.x * this.x + this.y * this.y + this.z * this.z));
        if (var1 > 0.0D) {
            float var3 = (float) (1.0D / var1);
            this.x *= var3;
            this.y *= var3;
            this.z *= var3;
        }

        return this;
    }

    public Vec3f dfQ() {
        return new Vec3f(this);
    }

    public float get(int var1) {
        switch (var1) {
            case 0:
                return this.x;
            case 1:
                return this.y;
            case 2:
                return this.z;
            default:
                throw new IllegalArgumentException();
        }
    }

    public void set(int var1, float var2) {
        switch (var1) {
            case 0:
                this.x = var2;
                break;
            case 1:
                this.y = var2;
                break;
            case 2:
                this.z = var2;
                break;
            default:
                throw new IllegalArgumentException();
        }

    }

    public int hashCode() {
        return (Float.floatToIntBits(this.x) * 31 + Float.floatToIntBits(this.y)) * 31 + Float.floatToIntBits(this.z);
    }

    public boolean equals(Object var1) {
        if (var1 == null) {
            return false;
        } else if (var1.getClass() != this.getClass()) {
            return false;
        } else {
            Vec3f var2 = (Vec3f) var1;
            return var2.x == this.x && var2.y == this.y && var2.z == this.z;
        }
    }

    public float a(IW var1) {
        return this.x * var1.x + this.y * var1.y + this.z * var1.z;
    }

    public float ada() {
        return this.x;
    }

    public float adb() {
        return this.y;
    }

    public float adc() {
        return this.z;
    }

    public Vector3dOperations dfR() {
        return new Vector3dOperations((double) this.x, (double) this.y, (double) this.z);
    }

    public boolean anA() {
        float var1 = this.x + this.y + this.z;
        return !Float.isNaN(var1) && var1 != Float.NEGATIVE_INFINITY && var1 != Float.POSITIVE_INFINITY;
    }

    public boolean isZero() {
        if (this.x != 0.0F) {
            return false;
        } else if (this.y != 0.0F) {
            return false;
        } else {
            return this.z == 0.0F;
        }
    }

    public Vec3f a(float var1, Tuple3f var2) {
        this.x = var2.x * var1;
        this.y = var2.y * var1;
        this.z = var2.z * var1;
        return this;
    }

    public Vec3f b(float var1, Tuple3f var2) {
        return (new Vec3f(this)).a(var1, var2);
    }

    public Vec3f mT(float var1) {
        this.x *= var1;
        this.y *= var1;
        this.z *= var1;
        return this;
    }

    public Vector3dOperations d(Tuple3d var1) {
        return new Vector3dOperations((double) this.x + var1.x, (double) this.y + var1.y, (double) this.z + var1.z);
    }

    public Vec3f v(float var1, float var2, float var3) {
        float var4 = this.y * var3 - this.z * var2;
        float var5 = this.z * var1 - this.x * var3;
        float var6 = this.x * var2 - this.y * var1;
        this.x = var4;
        this.y = var5;
        this.z = var6;
        return this;
    }

    public Vec3f w(float var1, float var2, float var3) {
        float var4 = this.y * var3 - this.z * var2;
        float var5 = this.z * var1 - this.x * var3;
        float var6 = this.x * var2 - this.y * var1;
        return new Vec3f(var4, var5, var6);
    }

    public void x(float var1, float var2, float var3) {
        float var4 = this.y * var3 - this.z * var2;
        float var5 = this.z * var1 - this.x * var3;
        float var6 = this.x * var2 - this.y * var1;
        this.x = var4;
        this.y = var5;
        this.z = var6;
    }

    public void c(Vector3f var1) {
        this.x(var1.x, var1.y, var1.z);
    }

    public Vec3f d(Vector3f var1) {
        return this.v(var1.x, var1.y, var1.z);
    }

    public float y(float var1, float var2, float var3) {
        return this.x * var1 + this.y * var2 + this.z * var3;
    }

    public Vec3f k(Tuple3f var1) {
        float var2 = (float) Math.pow((double) this.x, (double) var1.x);
        float var3 = (float) Math.pow((double) this.y, (double) var1.y);
        float var4 = (float) Math.pow((double) this.z, (double) var1.z);
        return new Vec3f(var2, var3, var4);
    }

    public Vec3f l(Tuple3f var1) {
        this.x = (float) Math.pow((double) this.x, (double) var1.x);
        this.y = (float) Math.pow((double) this.y, (double) var1.y);
        this.z = (float) Math.pow((double) this.z, (double) var1.z);
        return this;
    }

    public void m(Tuple3f var1) {
        this.x = (float) Math.pow((double) this.x, (double) var1.x);
        this.y = (float) Math.pow((double) this.y, (double) var1.y);
        this.z = (float) Math.pow((double) this.z, (double) var1.z);
    }

    public Vec3f dfS() {
        return new Vec3f(-this.x, -this.y, -this.z);
    }

    public Vec3f dfT() {
        this.negate();
        return this;
    }

    public Vec3f n(Tuple3f var1) {
        this.x = var1.x;
        this.y = var1.y;
        this.z = var1.z;
        return this;
    }

    public Vec3f z(float var1, float var2, float var3) {
        this.x = var1;
        this.y = var2;
        this.z = var3;
        return this;
    }

    public Vec3f o(Tuple3f var1) {
        this.x -= var1.x;
        this.y -= var1.y;
        this.z -= var1.z;
        return this;
    }

    public void A(float var1, float var2, float var3) {
        this.x -= var1;
        this.y -= var2;
        this.z -= var3;
    }

    public Vec3f B(float var1, float var2, float var3) {
        this.x -= var1;
        this.y -= var2;
        this.z -= var3;
        return this;
    }

    public Vec3f C(float var1, float var2, float var3) {
        return new Vec3f(this.x -= var1, this.y - var2, this.z -= var3);
    }

    public Vec3f p(Tuple3f var1) {
        this.x += var1.x;
        this.y += var1.y;
        this.z += var1.z;
        return this;
    }

    public Vec3f D(float var1, float var2, float var3) {
        this.x += var1;
        this.y += var2;
        this.z += var3;
        return this;
    }

    public void E(float var1, float var2, float var3) {
        this.x += var1;
        this.y += var2;
        this.z += var3;
    }

    public Vec3f F(float var1, float var2, float var3) {
        return new Vec3f(this.x + var1, this.y + var2, this.z + var3);
    }

    public Vec3f c(Tuple3f var1, Tuple3f var2) {
        this.x = var1.x - var2.x;
        this.y = var1.y - var2.y;
        this.z = var1.z - var2.z;
        return this;
    }

    public Vec3f d(Tuple3f var1, Tuple3f var2) {
        return new Vec3f(var1.x - var2.x, var1.y - var2.y, var1.z - var2.z);
    }

    public Vec3f b(Vector3f var1, Vector3f var2) {
        float var3 = var1.y * var2.z - var1.z * var2.y;
        float var4 = var1.z * var2.x - var1.x * var2.z;
        float var5 = var1.x * var2.y - var1.y * var2.x;
        this.x = var3;
        this.y = var4;
        this.z = var5;
        return this;
    }

    public Vec3f c(Vector3f var1, Vector3f var2) {
        float var3 = var1.y * var2.z - var1.z * var2.y;
        float var4 = var1.z * var2.x - var1.x * var2.z;
        float var5 = var1.x * var2.y - var1.y * var2.x;
        return new Vec3f(var3, var4, var5);
    }

    public float bF(Vec3f var1) {
        float var2 = this.y * var1.z - this.z * var1.y;
        float var3 = this.z * var1.x - this.x * var1.z;
        float var4 = this.x * var1.y - this.y * var1.x;
        return var1.x * var2 + var1.y * var3 + var1.z * var4;
    }

    public void a(ajK var1, Tuple3f var2) {
        float var3 = var2.x;
        float var4 = var2.y;
        float var5 = var2.z;
        this.x = var3 * var1.m00 + var4 * var1.m01 + var5 * var1.m02;
        this.y = var3 * var1.m10 + var4 * var1.m11 + var5 * var1.m12;
        this.z = var3 * var1.m20 + var4 * var1.m21 + var5 * var1.m22;
    }

    public void a(ajD var1, Tuple3f var2) {
        float var3 = var2.x;
        float var4 = var2.y;
        float var5 = var2.z;
        this.x = var3 * var1.m00 + var4 * var1.m01 + var5 * var1.m02;
        this.y = var3 * var1.m10 + var4 * var1.m11 + var5 * var1.m12;
        this.z = var3 * var1.m20 + var4 * var1.m21 + var5 * var1.m22;
    }

    public Vec3f p(ajK var1) {
        float var2 = this.x * var1.m00 + this.y * var1.m01 + this.z * var1.m02;
        float var3 = this.x * var1.m10 + this.y * var1.m11 + this.z * var1.m12;
        float var4 = this.x * var1.m20 + this.y * var1.m21 + this.z * var1.m22;
        return new Vec3f(var2, var3, var4);
    }

    public Vec3f d(ajK var1, Tuple3f var2) {
        this.a(var1, var2);
        return this;
    }

    public Vec3f q(ajK var1) {
        this.r(var1);
        return this;
    }

    public void r(ajK var1) {
        this.a((ajK) var1, this);
    }

    public void e(ajK var1, Tuple3f var2) {
        float var3 = var2.x;
        float var4 = var2.y;
        float var5 = var2.z;
        this.x = var3 * var1.m00 + var4 * var1.m01 + var5 * var1.m02 + var1.m03;
        this.y = var3 * var1.m10 + var4 * var1.m11 + var5 * var1.m12 + var1.m13;
        this.z = var3 * var1.m20 + var4 * var1.m21 + var5 * var1.m22 + var1.m23;
    }

    public Vec3f f(ajK var1, Tuple3f var2) {
        this.e(var1, var2);
        return this;
    }

    public Vec3f a(float var1, Tuple3f var2, Tuple3f var3) {
        this.scaleAdd(var1, var2, var3);
        return this;
    }

    public Vec3f c(float var1, Tuple3f var2) {
        this.scaleAdd(var1, var2);
        return this;
    }

    public Vec3f d(float var1, Tuple3f var2) {
        return (new Vec3f()).a(var1, this, var2);
    }

    public boolean isValid() {
        return !Float.isNaN(this.x) && !Float.isInfinite(this.x) && !Float.isNaN(this.y) && !Float.isInfinite(this.y) && !Float.isNaN(this.z) && !Float.isInfinite(this.z);
    }

    public Vec3f c(Tuple3d var1, Tuple3d var2) {
        this.x = (float) (var1.x - var2.x);
        this.y = (float) (var1.y - var2.y);
        this.z = (float) (var1.z - var2.z);
        return this;
    }

    public void readExternal(Vm_q var1) {
        this.x = var1.gZ("x");
        this.y = var1.gZ("y");
        this.z = var1.gZ("z");
    }

    public void writeExternal(att var1) {
        var1.a("x", this.x);
        var1.a("y", this.y);
        var1.a("z", this.z);
    }

}
