package com.hoplon.geometry;

import all.*;

import java.io.Serializable;


public class BoundingBox implements afA_q, Serializable {
    private Vec3f maxes;
    private Vec3f mins;

    public BoundingBox() {
        this.maxes = new Vec3f(Float.NEGATIVE_INFINITY, Float.NEGATIVE_INFINITY,
                Float.NEGATIVE_INFINITY);
        this.mins = new Vec3f(Float.POSITIVE_INFINITY, Float.POSITIVE_INFINITY,
                Float.POSITIVE_INFINITY);
    }

    public BoundingBox(Vec3f paramVec3f1, Vec3f paramVec3f2) {
        this.maxes = paramVec3f2;
        this.mins = paramVec3f1;
    }

    public BoundingBox(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6) {
        this(new Vec3f(paramFloat1, paramFloat2, paramFloat3), new Vec3f(paramFloat4, paramFloat5, paramFloat6));
    }

    public BoundingBox(float[] paramArrayOfFloat) {
        this.mins = new Vec3f(paramArrayOfFloat[0], paramArrayOfFloat[1], paramArrayOfFloat[2]);
        this.maxes = new Vec3f(paramArrayOfFloat[3], paramArrayOfFloat[4], paramArrayOfFloat[5]);
    }


    public boolean equals(Object paramObject) {
        if (paramObject == this)
            return true;
        if (paramObject == null)
            return false;
        if (paramObject.getClass() != getClass())
            return false;
        BoundingBox localBoundingBox = (BoundingBox) paramObject;
        return (this.maxes.equals(localBoundingBox.maxes)) && (this.mins.equals(localBoundingBox.mins));
    }

    public void c(BoundingBox paramBoundingBox) {
        this.mins.set(paramBoundingBox.dqP());
        this.maxes.set(paramBoundingBox.dqQ());
    }

    public void g(aLH paramaLH) {
        this.mins.set(paramaLH.dim());
        this.maxes.set(paramaLH.din());
    }

    public void bP(Vec3f paramVec3f) {
        this.mins = paramVec3f;
    }

    public Vec3f dqP() {
        return this.mins;
    }

    public void bQ(Vec3f paramVec3f) {
        this.maxes = paramVec3f;
    }

    public Vec3f dqQ() {
        return this.maxes;
    }

    public float dio() {
        float f = this.maxes.x - this.mins.x;
        return f > 1.0E26D ? 0.0F : Math.max(0.0F, f);
    }

    public float dip() {
        float f = this.maxes.y - this.mins.y;
        return f > 1.0E26D ? 0.0F : Math.max(0.0F, f);
    }

    public float diq() {
        float f = this.maxes.z - this.mins.z;
        return f > 1.0E26D ? 0.0F : Math.max(0.0F, f);
    }

    public float dir() {
        float f1 = dio();
        float f2 = dip();
        return (float) Math.sqrt(f1 * f1 + f2 * f2);
    }

    public float dis() {
        float f1 = dip();
        float f2 = diq();
        return (float) Math.sqrt(f2 * f2 + f1 * f1);
    }

    public float length() {
        return (float) Math.sqrt(lengthSquared());
    }

    public float lengthSquared() {
        float f1 = dio();
        float f2 = dip();
        float f3 = diq();
        return f1 * f1 + f2 * f2 + f3 * f3;
    }

    public float dit() {
        return Vec3f.L(this.mins, this.maxes) * 0.5F;
    }

    public float diu() {
        return Math.max(this.mins.lengthSquared(), this.maxes.lengthSquared());
    }


    public boolean a(Pt paramPt) {
        float f1 = 0.0F;

        float f2;
        if (paramPt.bny().x < this.mins.x) {
            f2 = paramPt.bny().x - this.mins.x;
            f1 = f2 * f2;
        }
        if (paramPt.bny().y < this.mins.y) {
            f2 = paramPt.bny().y - this.mins.y;
            f1 += f2 * f2;
        }
        if (paramPt.bny().z < this.mins.z) {
            f2 = paramPt.bny().z - this.mins.z;
            f1 += f2 * f2;
        }

        if (paramPt.bny().x > this.maxes.x) {
            f2 = paramPt.bny().x - this.maxes.x;
            f1 += f2 * f2;
        }
        if (paramPt.bny().y > this.maxes.y) {
            f2 = paramPt.bny().y - this.maxes.y;
            f1 += f2 * f2;
        }
        if (paramPt.bny().z > this.maxes.z) {
            f2 = paramPt.bny().z - this.maxes.z;
            f1 += f2 * f2;
        }

        return f1 <= paramPt.getRadius() * paramPt.getRadius();
    }

    public boolean d(BoundingBox paramBoundingBox) {
        return a(paramBoundingBox);
    }

    public boolean a(BoundingBox paramBoundingBox) {
        Vec3f localVec3f1 = paramBoundingBox.dqQ();
        Vec3f localVec3f2 = this.mins;
        if (localVec3f2.x > localVec3f1.x)
            return false;
        Vec3f localVec3f3 = paramBoundingBox.dqP();
        Vec3f localVec3f4 = this.maxes;
        if (localVec3f4.x < localVec3f3.x)
            return false;
        if (localVec3f2.y > localVec3f1.y)
            return false;
        if (localVec3f4.y < localVec3f3.y)
            return false;
        if (localVec3f2.z > localVec3f1.z)
            return false;
        if (localVec3f4.z < localVec3f3.z) {
            return false;
        }
        return true;
    }


    public gu_g a(Vec3f paramVec3f1, Vec3f paramVec3f2, boolean paramBoolean) {
        gu_g localgu = new gu_g(paramVec3f1, paramVec3f2);

        int i = 1;
        float f2 = 0.0F;
        float f1;
        if (paramVec3f1.x < this.mins.x) {
            f1 = this.mins.x - paramVec3f1.x;
            if (f1 > localgu.vR().x) {
                return localgu;
            }
            f1 /= localgu.vR().x;
            i = 0;
            f2 = -1.0F;
        } else if (paramVec3f1.x > this.maxes.x) {
            f1 = this.maxes.x - paramVec3f1.x;
            if (f1 < localgu.vR().x) {
                return localgu;
            }
            f1 /= localgu.vR().x;
            i = 0;
            f2 = 1.0F;
        } else {
            f1 = -1.0F;
        }

        float f4 = 0.0F;
        float f3;
        if (paramVec3f1.y < this.mins.y) {
            f3 = this.mins.y - paramVec3f1.y;
            if (f3 > localgu.vR().y) {
                return localgu;
            }
            f3 /= localgu.vR().y;
            i = 0;
            f4 = -1.0F;
        } else if (paramVec3f1.y > this.maxes.y) {
            f3 = this.maxes.y - paramVec3f1.y;
            if (f3 < localgu.vR().y) {
                return localgu;
            }
            f3 /= localgu.vR().y;
            i = 0;
            f4 = 1.0F;
        } else {
            f3 = -1.0F;
        }

        float f6 = 0.0F;
        float f5;
        if (paramVec3f1.z < this.mins.z) {
            f5 = this.mins.z - paramVec3f1.z;
            if (f5 > localgu.vR().z) {
                return localgu;
            }
            f5 /= localgu.vR().z;
            i = 0;
            f6 = -1.0F;
        } else if (paramVec3f1.z > this.maxes.z) {
            f5 = this.maxes.z - paramVec3f1.z;
            if (f5 < localgu.vR().z) {
                return localgu;
            }
            f5 /= localgu.vR().z;
            i = 0;
            f6 = 1.0F;
        } else {
            f5 = -1.0F;
        }
        if (i != 0) {
            if (paramBoolean) {
                localgu.l(localgu.vR().dfS().dfP());
                localgu.ac(0.0F);
                localgu.setIntersected(true);
                return localgu;
            }
            return localgu;
        }


        int j = 0;
        float f7 = f1;
        if (f3 > f7) {
            j = 1;
            f7 = f3;
        }
        if (f5 > f7) {
            j = 2;
            f7 = f5;
        }
        float f8;
        float f9;
        switch (j) {
            case 0:
                f8 = paramVec3f1.y + localgu.vR().y * f7;
                if ((f8 < this.mins.y) || (f8 > this.maxes.y))
                    return localgu;
                f9 = paramVec3f1.z + localgu.vR().z * f7;
                if ((f9 < this.mins.z) || (f9 > this.maxes.z))
                    return localgu;
                localgu.b(f2, 0.0F, 0.0F);
                break;


            case 1:
                f8 = paramVec3f1.x + localgu.vR().x * f7;
                if ((f8 < this.mins.x) || (f8 > this.maxes.x)) {
                    return localgu;
                }
                f9 = paramVec3f1.z + localgu.vR().z * f7;
                if ((f9 < this.mins.z) || (f9 > this.maxes.z)) {
                    return localgu;
                }
                localgu.b(0.0F, f4, 0.0F);
                break;


            case 2:
                f8 = paramVec3f1.x + localgu.vR().x * f7;
                if ((f8 < this.mins.x) || (f8 > this.maxes.x)) {
                    return localgu;
                }
                f9 = paramVec3f1.y + localgu.vR().y * f7;
                if ((f9 < this.mins.y) || (f9 > this.maxes.y)) {
                    return localgu;
                }
                localgu.b(0.0F, 0.0F, f6);
        }


        localgu.ac(f7);
        localgu.setIntersected(true);
        return localgu;
    }

    public void reset() {
        this.mins.x = (this.mins.y = this.mins.z = Float.POSITIVE_INFINITY);
        this.maxes.x = (this.maxes.y = this.maxes.z = Float.NEGATIVE_INFINITY);
    }

    public void bR(Vec3f paramVec3f) {
        this.maxes.x = (this.mins.x = paramVec3f.x);
        this.maxes.y = (this.mins.y = paramVec3f.y);
        this.maxes.z = (this.mins.z = paramVec3f.z);
    }

    public void addPoint(Vec3f paramVec3f) {
        if (paramVec3f.x < this.mins.x)
            this.mins.x = paramVec3f.x;
        if (paramVec3f.y < this.mins.y)
            this.mins.y = paramVec3f.y;
        if (paramVec3f.z < this.mins.z) {
            this.mins.z = paramVec3f.z;
        }
        if (paramVec3f.x > this.maxes.x)
            this.maxes.x = paramVec3f.x;
        if (paramVec3f.y > this.maxes.y)
            this.maxes.y = paramVec3f.y;
        if (paramVec3f.z > this.maxes.z)
            this.maxes.z = paramVec3f.z;
    }

    public void addPoint(float paramFloat1, float paramFloat2, float paramFloat3) {
        if (paramFloat1 < this.mins.x)
            this.mins.x = paramFloat1;
        if (paramFloat2 < this.mins.y)
            this.mins.y = paramFloat2;
        if (paramFloat3 < this.mins.z) {
            this.mins.z = paramFloat3;
        }
        if (paramFloat1 > this.maxes.x)
            this.maxes.x = paramFloat1;
        if (paramFloat2 > this.maxes.y)
            this.maxes.y = paramFloat2;
        if (paramFloat3 > this.maxes.z) {
            this.maxes.z = paramFloat3;
        }
    }

    public void a(ajK paramajK, BoundingBox paramBoundingBox) {
        Vec3f localVec3f2 = new Vec3f(this.maxes.x, this.maxes.y, this.maxes.z);
        Vec3f localVec3f3 = new Vec3f(this.maxes.x, this.maxes.y, this.mins.z);
        Vec3f localVec3f4 = new Vec3f(this.maxes.x, this.mins.y, this.mins.z);
        Vec3f localVec3f5 = new Vec3f(this.maxes.x, this.mins.y, this.maxes.z);
        Vec3f localVec3f6 = new Vec3f(this.mins.x, this.maxes.y, this.maxes.z);
        Vec3f localVec3f7 = new Vec3f(this.mins.x, this.maxes.y, this.mins.z);
        Vec3f localVec3f8 = new Vec3f(this.mins.x, this.mins.y, this.mins.z);
        Vec3f localVec3f9 = new Vec3f(this.mins.x, this.mins.y, this.maxes.z);
        paramBoundingBox.reset();

        Vec3f localVec3f1 = paramajK.c(localVec3f2);
        paramBoundingBox.addPoint(localVec3f1);

        localVec3f1 = paramajK.c(localVec3f3);
        paramBoundingBox.addPoint(localVec3f1);

        localVec3f1 = paramajK.c(localVec3f4);
        paramBoundingBox.addPoint(localVec3f1);

        localVec3f1 = paramajK.c(localVec3f5);
        paramBoundingBox.addPoint(localVec3f1);

        localVec3f1 = paramajK.c(localVec3f6);
        paramBoundingBox.addPoint(localVec3f1);

        localVec3f1 = paramajK.c(localVec3f7);
        paramBoundingBox.addPoint(localVec3f1);

        localVec3f1 = paramajK.c(localVec3f8);
        paramBoundingBox.addPoint(localVec3f1);

        localVec3f1 = paramajK.c(localVec3f9);
        paramBoundingBox.addPoint(localVec3f1);
    }

    public void a(bc_q parambc, aLH paramaLH) {
        Vector3dOperations localaJR1 = new Vector3dOperations(this.maxes.x, this.maxes.y, this.maxes.z);
        Vector3dOperations localaJR2 = new Vector3dOperations(this.maxes.x, this.maxes.y, this.mins.z);
        Vector3dOperations localaJR3 = new Vector3dOperations(this.maxes.x, this.mins.y, this.mins.z);
        Vector3dOperations localaJR4 = new Vector3dOperations(this.maxes.x, this.mins.y, this.maxes.z);
        Vector3dOperations localaJR5 = new Vector3dOperations(this.mins.x, this.maxes.y, this.maxes.z);
        Vector3dOperations localaJR6 = new Vector3dOperations(this.mins.x, this.maxes.y, this.mins.z);
        Vector3dOperations localaJR7 = new Vector3dOperations(this.mins.x, this.mins.y, this.mins.z);
        Vector3dOperations localaJR8 = new Vector3dOperations(this.mins.x, this.mins.y, this.maxes.z);
        paramaLH.reset();

        parambc.a(localaJR1);
        paramaLH.aG(localaJR1);

        parambc.a(localaJR2);
        paramaLH.aG(localaJR2);

        parambc.a(localaJR3);
        paramaLH.aG(localaJR3);

        parambc.a(localaJR4);
        paramaLH.aG(localaJR4);

        parambc.a(localaJR5);
        paramaLH.aG(localaJR5);

        parambc.a(localaJR6);
        paramaLH.aG(localaJR6);

        parambc.a(localaJR7);
        paramaLH.aG(localaJR7);

        parambc.a(localaJR8);
        paramaLH.aG(localaJR8);
    }


    public void b(ajK paramajK, BoundingBox paramBoundingBox) {
        Vec3f localVec3f2 = new Vec3f(this.maxes.x, this.maxes.y, this.maxes.z);
        Vec3f localVec3f3 = new Vec3f(this.maxes.x, this.maxes.y, this.mins.z);
        Vec3f localVec3f4 = new Vec3f(this.maxes.x, this.mins.y, this.mins.z);
        Vec3f localVec3f5 = new Vec3f(this.maxes.x, this.mins.y, this.maxes.z);
        Vec3f localVec3f6 = new Vec3f(this.mins.x, this.maxes.y, this.maxes.z);
        Vec3f localVec3f7 = new Vec3f(this.mins.x, this.maxes.y, this.mins.z);
        Vec3f localVec3f8 = new Vec3f(this.mins.x, this.mins.y, this.mins.z);
        Vec3f localVec3f9 = new Vec3f(this.mins.x, this.mins.y, this.maxes.z);

        Vec3f localVec3f1 = paramajK.c(localVec3f2);
        paramBoundingBox.addPoint(localVec3f1);

        localVec3f1 = paramajK.c(localVec3f3);
        paramBoundingBox.addPoint(localVec3f1);

        localVec3f1 = paramajK.c(localVec3f4);
        paramBoundingBox.addPoint(localVec3f1);

        localVec3f1 = paramajK.c(localVec3f5);
        paramBoundingBox.addPoint(localVec3f1);

        localVec3f1 = paramajK.c(localVec3f6);
        paramBoundingBox.addPoint(localVec3f1);

        localVec3f1 = paramajK.c(localVec3f7);
        paramBoundingBox.addPoint(localVec3f1);

        localVec3f1 = paramajK.c(localVec3f8);
        paramBoundingBox.addPoint(localVec3f1);

        localVec3f1 = paramajK.c(localVec3f9);
        paramBoundingBox.addPoint(localVec3f1);
    }

    public void c(ajK paramajK, BoundingBox paramBoundingBox) {
        float f1 = Math.abs(paramajK.m00);
        float f2 = Math.abs(paramajK.m01);
        float f3 = Math.abs(paramajK.m02);
        float f4 = Math.abs(paramajK.m10);
        float f5 = Math.abs(paramajK.m11);
        float f6 = Math.abs(paramajK.m12);
        float f7 = Math.abs(paramajK.m20);
        float f8 = Math.abs(paramajK.m21);
        float f9 = Math.abs(paramajK.m22);
        float f10 = dio();
        float f11 = dip();
        float f12 = diq();
        float f13 = (f10 * f1 + f11 * f2 + f12 * f3) / 2.0F;
        float f14 = (f10 * f4 + f11 * f5 + f12 * f6) / 2.0F;
        float f15 = (f10 * f7 + f11 * f8 + f12 * f9) / 2.0F;
        float f16 = paramajK.m03;
        float f17 = paramajK.m13;
        float f18 = paramajK.m23;
        paramBoundingBox.maxes.x = (f16 + f13);
        paramBoundingBox.maxes.y = (f17 + f14);
        paramBoundingBox.maxes.z = (f18 + f15);
        paramBoundingBox.mins.x = (f16 - f13);
        paramBoundingBox.mins.y = (f17 - f14);
        paramBoundingBox.mins.z = (f18 - f15);
    }

    public void a(OrientationBase paramaoY, Vec3f paramVec3f, BoundingBox paramBoundingBox) {
        paramBoundingBox.reset();


        float f1 = paramaoY.x;
        float f2 = paramaoY.y;
        float f3 = paramaoY.z;
        float f4 = paramaoY.w;
        float f5 = f4 * f4 + f1 * f1 + f2 * f2 + f3 * f3;
        if (!OrientationBase.isZero(1.0F - f5)) {
            float f6 = (float) Math.sqrt(f5);
            f1 /= f6;
            f2 /= f6;
            f3 /= f6;
            f4 /= f6;
        }
        float f6 = f1 * f1;
        float f7 = f1 * f2;
        float f8 = f1 * f3;
        float f9 = f4 * f1;
        float f10 = f2 * f2;
        float f11 = f2 * f3;
        float f12 = f2 * f4;
        float f13 = f3 * f3;
        float f14 = f3 * f4;
        float f15 = Math.abs(1.0F - 2.0F * (f10 + f13));
        float f16 = Math.abs(2.0F * (f7 - f14));
        float f17 = Math.abs(2.0F * (f8 + f12));
        float f18 = Math.abs(2.0F * (f7 + f14));
        float f19 = Math.abs(1.0F - 2.0F * (f6 + f13));
        float f20 = Math.abs(2.0F * (f11 - f9));
        float f21 = Math.abs(2.0F * (f8 - f12));
        float f22 = Math.abs(2.0F * (f11 + f9));
        float f23 = Math.abs(1.0F - 2.0F * (f6 + f10));
        float f24 = dio();
        float f25 = dip();
        float f26 = diq();
        float f27 = (f24 * f15 + f25 * f16 + f26 * f17) / 2.0F;
        float f28 = (f24 * f18 + f25 * f19 + f26 * f20) / 2.0F;
        float f29 = (f24 * f21 + f25 * f22 + f26 * f23) / 2.0F;
        float f30 = paramVec3f.x;
        float f31 = paramVec3f.y;
        float f32 = paramVec3f.z;
        paramBoundingBox.maxes.x = (f30 + f27);
        paramBoundingBox.maxes.y = (f31 + f28);
        paramBoundingBox.maxes.z = (f32 + f29);
        paramBoundingBox.mins.x = (f30 - f27);
        paramBoundingBox.mins.y = (f31 - f28);
        paramBoundingBox.mins.z = (f32 - f29);
    }

    public void a(OrientationBase paramaoY, Vector3dOperations paramaJR, aLH paramaLH) {
        paramaLH.reset();

        float f1 = paramaoY.x;
        float f2 = paramaoY.y;
        float f3 = paramaoY.z;
        float f4 = paramaoY.w;
        float f5 = f4 * f4 + f1 * f1 + f2 * f2 + f3 * f3;
        if (!OrientationBase.isZero(1.0F - f5)) {
            float f6 = (float) Math.sqrt(f5);
            f1 /= f6;
            f2 /= f6;
            f3 /= f6;
            f4 /= f6;
        }
        float f6 = f1 * f1;
        float f7 = f1 * f2;
        float f8 = f1 * f3;
        float f9 = f4 * f1;
        float f10 = f2 * f2;
        float f11 = f2 * f3;
        float f12 = f2 * f4;
        float f13 = f3 * f3;
        float f14 = f3 * f4;
        float f15 = Math.abs(1.0F - 2.0F * (f10 + f13));
        float f16 = Math.abs(2.0F * (f7 - f14));
        float f17 = Math.abs(2.0F * (f8 + f12));
        float f18 = Math.abs(2.0F * (f7 + f14));
        float f19 = Math.abs(1.0F - 2.0F * (f6 + f13));
        float f20 = Math.abs(2.0F * (f11 - f9));
        float f21 = Math.abs(2.0F * (f8 - f12));
        float f22 = Math.abs(2.0F * (f11 + f9));
        float f23 = Math.abs(1.0F - 2.0F * (f6 + f10));
        float f24 = dio();
        float f25 = dip();
        float f26 = diq();
        float f27 = (f24 * f15 + f25 * f16 + f26 * f17) / 2.0F;
        float f28 = (f24 * f18 + f25 * f19 + f26 * f20) / 2.0F;
        float f29 = (f24 * f21 + f25 * f22 + f26 * f23) / 2.0F;
        double d1 = paramaJR.x;
        double d2 = paramaJR.y;
        double d3 = paramaJR.z;
        paramaLH.A(d1 + f27, d2 + f28, d3 + f29);
        paramaLH.B(d1 - f27, d2 - f28, d3 - f29);
    }

    public String toString() {
        return
                "[(" + this.mins.x + ", " + this.mins.y + ", " + this.mins.z + ") -> (" + this.maxes.x + ", " + this.maxes.y + ", " + this.maxes.z + ")]";
    }

    public int hashCode() {
        return this.mins.hashCode() * this.maxes.hashCode();
    }

    public BoundingBox dqR() {
        return new BoundingBox(this.mins, this.maxes);
    }

    public Vec3f bny() {
        Vec3f localVec3f = new Vec3f();
        localVec3f = this.maxes.j(this.mins);
        localVec3f = localVec3f.mS(0.5F);
        return this.mins.i(localVec3f);
    }

    public void G(float paramFloat1, float paramFloat2, float paramFloat3) {
        this.maxes.x = paramFloat1;
        this.maxes.y = paramFloat2;
        this.maxes.z = paramFloat3;
    }

    public void H(float paramFloat1, float paramFloat2, float paramFloat3) {
        this.mins.x = paramFloat1;
        this.mins.y = paramFloat2;
        this.mins.z = paramFloat3;
    }

    public void e(BoundingBox paramBoundingBox) {
        Vec3f localVec3f1 = paramBoundingBox.dqQ();
        Vec3f localVec3f2 = paramBoundingBox.dqP();
        G(localVec3f1.x, localVec3f1.y, localVec3f1.z);
        H(localVec3f2.x, localVec3f2.y, localVec3f2.z);
    }

    public void f(BoundingBox paramBoundingBox) {
        addPoint(paramBoundingBox.maxes);
        addPoint(paramBoundingBox.mins);
    }

    public void readExternal(Vm_q paramVmQ) {
        this.mins = ((Vec3f) paramVmQ.he("mins"));
        this.maxes = ((Vec3f) paramVmQ.he("maxes"));
    }

    public void writeExternal(att paramatt) {
        paramatt.a("mins", this.mins);
        paramatt.a("maxes", this.maxes);
    }
}
