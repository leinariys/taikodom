package com.hoplon.geometry;

import all.aJF;

import javax.vecmath.*;
import java.nio.FloatBuffer;

public class Color extends aJF {
    private static final long serialVersionUID = -4649326232529180793L;

    /**
     * @deprecated
     */
    @Deprecated
    public Color() {
        super(0.0F, 0.0F, 0.0F, 0.0F);
    }

    public Color(float var1, float var2, float var3, float var4) {
        super(var1, var2, var3, var4);
    }

    public Color(float[] var1) {
        super(var1);
    }

    public Color(Tuple3f var1) {
        super(var1);
    }

    public Color(Tuple4d var1) {
        super(var1);
    }

    public Color(Tuple4f var1) {
        super(var1);
    }

    public Color(Vector4d var1) {
        super(var1);
    }

    public Color(Vector4f var1) {
        super(var1);
    }

    public static Color interpolate(Color var0, Color var1, float var2) {
        Color var3 = new Color(0.0F, 0.0F, 0.0F, 0.0F);
        var3.x = var1.x * var2 + var0.x * (1.0F - var2);
        var3.y = var1.y * var2 + var0.y * (1.0F - var2);
        var3.z = var1.z * var2 + var0.z * (1.0F - var2);
        var3.w = var1.w * var2 + var0.w * (1.0F - var2);
        return var3;
    }

    public float getRed() {
        return this.x;
    }

    public void setRed(float var1) {
        this.x = var1;
    }

    public float getGreen() {
        return this.y;
    }

    public void setGreen(float var1) {
        this.y = var1;
    }

    public float getBlue() {
        return this.z;
    }

    public void setBlue(float var1) {
        this.z = var1;
    }

    public float getAlpha() {
        return this.w;
    }

    public void setAlpha(float var1) {
        this.z = var1;
    }

    public Color multiply(Color var1) {
        if (var1 == null) {
            return this;
        } else {
            Color var2 = new Color(0.0F, 0.0F, 0.0F, 0.0F);
            var2.x = this.x * var1.x;
            var2.y = this.y * var1.y;
            var2.z = this.z = var1.z;
            var2.w = this.w = var1.w;
            return var2;
        }
    }

    public FloatBuffer fillFloatBuffer(FloatBuffer var1) {
        var1.clear();
        var1.put(this.x);
        var1.put(this.y);
        var1.put(this.z);
        var1.put(this.w);
        var1.flip();
        return var1;
    }
}
