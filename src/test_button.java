import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Демонстрация удалений кнопки
 */
public class test_button {
    public static void main(String[] var0) {
        JFrame var1 = new JFrame("bla");
        var1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//выход из приложения
        var1.setLayout(new BorderLayout());

        final JPanel var2 = new JPanel();

        final JButton var4 = new JButton("bt2");
        JButton var3 = new JButton("bt");
        var3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent var1) {
                var2.remove(var4);
                var2.updateUI();//Добавал, иначе оставалась картинка кнопки
            }
        });

        var2.setLayout(new FlowLayout());
        var2.add(var3);
        var2.add(var4);
        var1.add(var2);
        var1.setBounds(400, 400, 200, 200);
        var1.show();
        var1.validate();
        var2.validate();
        var2.setLayout((LayoutManager) null);
    }
}
