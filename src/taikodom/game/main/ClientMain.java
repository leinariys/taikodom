package taikodom.game.main;

import all.*;
import org.apache.commons.logging.Log;
import taikodom.infra.script.I18NString;
import taikodom.render.SceneEvent;
import taikodom.render.loader.provider.ImageLoader;

import javax.imageio.ImageIO;
import javax.media.opengl.Threading;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Locale;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

//import taikodom.infra.comm.network.ServerLoginException;

public class ClientMain {

    private static final boolean dDi = false;
    private static final Log logger = LogPrinter.setClass(ClientMain.class);
    private static final String clientDomain = "com.hoplon.Taikodom";

    /**
     * Диагностика работы потоков
     */
    private static WatchDog watchDog;

    /**
     * Логирование часов потока
     */
    private static aDi dTn;

    /**
     * Ссылка на класс клиента
     */
    private static ClientMain clientMain;

    /**
     * Это единственно запущенный клиент? true - незапущен  false - запущен
     */
    private static boolean isRunClient;

    /**
     * Ссылка на файл, для сигнализации, что одна версия клиента уже запущена
     */
    private static Object dTp;
    /**
     * Корневой путь. Каталог с кодом отрисовки графики
     */
    private static ain rootPathRender;
    /**
     * Каталог главный, rootpath
     */
    private static ain rootPath;
    /**
     * Очередь и создание потоков
     */
    public ThreadPoolExecutor threadPool;
    /**
     * Блокировжик ресурса
     */
    public RP dTs;
    /**
     * Графический движок
     */
    public EngineGraphics engineGraphics;
    private EngineGame engineGame;
    /**
     * Список задач для очереди потоков
     */
    private aiD dTt;
    private boolean dTu;
    /**
     * Менеджер аддонов
     */
    private AddonManager addonManager;
    private int dTw;
    private long dTx;
    private long startTime;


    public ClientMain() {
        this.threadPool = new ThreadPoolExecutor(1, 1, 60L, TimeUnit.SECONDS, new LinkedBlockingQueue(), new PoolThread.ThreadFac("Worker", true));
        this.dTs = new RP();
        this.dTt = new aiD(this);
        //Завасти очередь потоков
        this.threadPool.execute(new Runnable() {
            public void run() {
            }
        });
    }

    public static void main(String[] var0) throws IOException {
        logger.info("Starting ClientMain");
        rootPath = new FileControl(".");//Создаём рутт каталог
        String var1 = System.getProperty("gametoolkit.client.render.rootpath", ".");
        logger.info("Client MAIN: Loading path '" + var1 + "'");
        rootPathRender = new FileControl(var1);

        FileControl var2 = new FileControl(System.getProperty("physics.resources.rootpath", "."));
        hU.setRootPathResource(var2);
        URL.setURLStreamHandlerFactory(hP.ze());//Задает URL-адрес приложения для Java Virtual Machine.

        logger.debug("step1");
        hP.ze().a("res", new StreamRootPathRender(rootPathRender));

        logger.debug("step2");
        isRunClient = !LockInstanceApp.isCopyApp(clientDomain, System.getProperty("user.dir"));

        logger.debug("step3");
        dTp = LockInstanceApp.setMarkOriginalTime(clientDomain, System.getProperty("user.dir"));

        logger.debug("step4");
        if (!isRunClient) {
            logger.warn("Client already running");
        }
        clientMain = new ClientMain();

        logger.debug("step5");
        watchDog = new WatchDog();

        logger.debug("step6");
        logger.debug("step6.1");
        PrintWriter var3;
        if (isRunClient) {//Если это единственный клиент
            logger.debug("step6.2");
            var3 = gq("client-watchdog");
        } else {//Если это не единственный клиент
            logger.debug("step6.3");
            var3 = LogPrinter.createLogFile("client-watchdog", "log");
        }
        var3.println("Starting taikodom client 1.0.4933.55");
        watchDog.setPrintWriter(var3);

        logger.debug("step7");//Запустить watchdog ?
        if ("true".equalsIgnoreCase(System.getProperty("watchdog.enable"))) {
            watchDog.start();
        }

        logger.debug("step8");
        dTn = new aDi("Glitch Watch Thread") {
            protected long cR(long var1) {
                long var3 = ClientMain.clientMain.dTx;
                return var1 - var3;
            }
        };

        logger.debug("step9");
        if (isRunClient) {//Если это единственный клиент
            var3 = gq("client-glitch");
        } else {//Если это не единственный клиент
            var3 = LogPrinter.createLogFile("client-glitch", "log");
        }
        var3.println("Starting taikodom client 1.0.4933.55");
        dTn.setPrintWriter(var3);

        logger.debug("step10");
        if ("true".equalsIgnoreCase(System.getProperty("glitchLog.enable"))) {
            dTn.start();
        }

        logger.debug("step11");
        ImagesetGui.check();//Проверка ресурсов
        ar.bxC();
/*
      //TODO удалить
      Thread var4 = new Thread("Class loading") {
         public void run() {
            ajs var1 = (new SH()).ate();
            long var2 =  System.nanoTime();
            Iterator var5 = var1.aXV().iterator();//Получили список классов
            while(var5.hasNext())
            {
               String var4 = (String)var5.next(); //all.zc
               try
               {
                  for(Class var6 = Class.forName(var4); var6 != null; var6 = var6.getSuperclass())
                  {
                     var6.getAnnotations();
                     Field[] var10;
                     int var9 = (var10 = var6.getDeclaredFields()).length;
                     for(int var8 = 0; var8 < var9; ++var8)
                     {
                        Field var7 = var10[var8];
                        var7.getType().getAnnotations();
                        if ((double)(System.nanoTime() - var2) > 1.0E7D)
                        {
                           PoolThread.sleep(3L * (long)((double)(System.nanoTime() - var2) * 1.0E-6D));
                           var2 = System.nanoTime();
                        }
                     }
                  }
                  if ((double)(System.nanoTime() - var2) > 1.0E7D)
                  {
                     PoolThread.sleep(3L * (long)((double)(System.nanoTime() - var2) * 1.0E-6D));
                     var2 = System.nanoTime();
                  }
               } catch (Exception var11)
               {
                  System.out.println("Exception Class loading "+var11.toString());//ДЕБАГ
               }
            }
            ClientMain.logger.info("Done loading classes");
         }
      };
      var4.setDaemon(true);
      var4.start();
*/
        logger.info("Starting taikodom client 1.0.4933.55");


//     dTo.gr_q("binkw32");
//     dTo.gr_q("SDL");
//     dTo.gr_q("granny2");
//     dTo.gr_q("grn2gr2");
//     dTo.gr_q("mss32");
//     dTo.gr_q("utaikodom.render");
//     dTo.gr_q("libgameswf");
//     dTo.gr_q("utaikodom.render.swf");

        Threading.disableSingleThreading();
        clientMain.run();//Запускаем окно
    }

    /**
     * Логирование если это единственно запущенный клиент
     *
     * @param var0
     * @return
     * @throws IOException
     */
    private static PrintWriter gq(String var0) throws IOException {
        logger.debug("createWriter: " + var0);
        String var1 = System.getProperty("log-dir", "log");
        logger.debug("logDir: " + var0);
        File var2 = new File(var1 + "/" + var0 + ".log");
        var2.getParentFile().mkdirs();
        logger.debug("logFile: " + var2.getCanonicalPath());
        if (var2.exists()) {
            logger.debug("file already exists");//Файл уже существует
            var2.delete();
            logger.debug("deleted");
        }

        logger.debug("file already exists");
        PrintWriter var3 = new PrintWriter(new FileWriter(var2), true);
        logger.debug("done createWriter");
        return var3;
    }

    public static ClientMain getClientMain() {
        return clientMain;
    }

    private void gr_q(String var1) {
        ain var2 = rootPath.concat("./libs/natives/" + var1 + ".dll");
        if (var2.exists()) {//Нашло библиотеку
            System.load(var2.getFile().getAbsolutePath());
        } else {
            //Не нашло библиотеку
            System.loadLibrary(var1);
        }
    }

    /**
     * Установка языка
     * СОздание движка отрисовки
     * Установка настройки  отрисовки
     */
    private void boL() {

        this.engineGame = new EngineGame(rootPath, rootPathRender);//Передача ссылок на каталок
        this.engineGame.fn(true);
        this.engineGame.a((Executor) this.dTt);//Передача списка задач
        ConfigManager var1 = ConfigManager.initConfigManager();//Создали/получили обёртку настроек
        this.ConfigManagerLoad(var1);//Загружаем настройки из файлов
        String var2 = "pt";
        Locale var3 = Locale.getDefault();//getDefault=ru_RU getCountry=RU getLanguage=ru
        if ("BR".equals(var3.getCountry()) || var3.getLanguage().equals("pt")) {
            var2 = "pt";
        }

        try {
            ConfigManagerSection var4 = var1.getSection(setKeyValue.user_language.getValue());//Получаем настройки раздела user
            I18NString.setCurrentLocation(var4.getValuePropertyString(setKeyValue.user_language, var2)); //Установить значение языка
        } catch (InvalidSectionNameException var10) {
            logger.error("Could not retrieve 'language' entry from configuration file!");
        } catch (ConfigManagerPropertyDifference var11) {
            logger.error("Entry 'language' in configuration file has wrong type (should be string)!");
        }


        this.engineGame.setConfigManager(var1);//передали менеджер настроек
        this.engineGame.getVerbose();//Достали настройку [console] verbose
        final Thread var12 = Thread.currentThread();

        this.engineGame.setEventManager(new EventManager() {
            @Override
            public void d(final String var1, final Object var2) {
                if (Thread.currentThread() == var12)//Проверяет что это поток main
                {
                    ClientMain.this.dTt.execute(new Runnable() {
                        public void run() {
                            j(var1, var2);
                        }
                    });
                } else {
                    super.d(var1, var2);
                }
            }

            void j(String var1, Object var2) {
                super.d(var1, var2);
            }

            public void c(final String var1, final Object var2) {
                ClientMain.this.dTt.execute(new Runnable() {
                    public void run() {
                        j(var1, var2);
                    }
                });
            }
        });

        //Создаём движок отрисовки
        this.engineGraphics = new EngineGraphics(this.engineGame.getEventManager());

        try {//Передача иконок
            this.engineGraphics.j(Arrays.asList(
                    ImageIO.read(this.getClass().getResource("taikodom_icon_16.png")),
                    ImageIO.read(this.getClass().getResource("taikodom_icon_32.png")),
                    ImageIO.read(this.getClass().getResource("taikodom_icon_64.png"))
            ));
        } catch (IOException e) {
            e.printStackTrace();
        }


        try {//Передаём настройки раздела  [render]
            //Настройки графики
            //engineVolume, enableGlow, shaderQuality, hardwareMouse, usePixelLight,
            // yres, refresh, anisotropicFilter, musicVolume, xres, textureQuality,
            // guiContrastBlocker, useVBO, dynamicTextureRate, fullscreen, guiVolume,
            // lodQuality, textureFilter, environmentFxQuality, fxVolume, antiAliasing,
            // postProcessingFx, showSelectionOutline, ambienceVolume
            this.engineGraphics.setConfigSection(var1.getSection("render"), rootPathRender);
        } catch (InvalidSectionNameException var8) {
            logger.error("Couldn't initialize the render module.");
            var8.printStackTrace();
        } catch (Exception var9) {
            logger.error("Couldn't initialize the render module.");
            var9.printStackTrace();
        }

        this.engineGame.setEngineGraphics(this.engineGraphics);

        LoaderTrail var5 = new LoaderTrail(this.engineGraphics);
        var5.setExecutor(this.dTt);
        var5.a(this.dTs.cYc());
        this.engineGame.a(this.dTs);
        this.engineGame.a((ILoaderTrail) var5);
        this.engineGame.getLoaderTrail().start();

        aBg var6 = new aBg(true);
        this.engineGame.a(var6);
        this.engineGraphics.bV("data/hud.pro");//загрузка интерфайса коробля

        GUIModule var7 = new GUIModule();//Загрузка ресурсов
        var7.setProperties(rootPathRender, "/data/gui/imageset/", "/data/fonts/", this.engineGraphics, var6);
        this.engineGame.setGUIModule(var7);

        this.startTime = atM.currentTimeMillis();
        this.boM();

    }

    private void boM() {
        try {
            BaseUItegXML baseUItegXML = new BaseUItegXML();
            final oE var2 = new oE();
            this.engineGame.a(var2);
            this.engineGame.alf().b("FileChangeCheckerTimer", new aen() {
                public void run() {
                    ClientMain.this.engineGame.cno().run();
                }
            }, 1000L);

            // метод из класса перепределён? class LoaderCss
            baseUItegXML.setLoaderCss(new LoaderCss() {
                public avI loadCSS(URL var1) throws IOException {
                    if ("res".equals(var1.getProtocol())) {
                        String var2x = var1.toString();
                        //  var2x.replaceAll("^res://", "") =  data/styles/core.css
                        var1 = ClientMain.rootPathRender.concat(var2x.replaceAll("^res://", "")).getFile().toURI().toURL();
                        //var1 = file:/C:/Projects/Java/TaikodomGameLIVE/./data/styles/core.css
                    }

                    final CssNode var3 = new CssNode();
                    var3.loadFileCSS(var1.openStream());
                    if ("file".equals(var1.getProtocol())) {
                        var2.a(var1, new ahq_q() {
                            @Override
                            public void a(URL var1) {
                                try {
                                    ClientMain.this.dTs.cYc().brL();

                                    try {
                                        var3.loadFileCSS(var1.openStream());
                                        Iterator var3x = ComponentManager.Vn().iterator();

                                        while (var3x.hasNext()) {
                                            aeK var2x = (aeK) var3x.next();
                                            var2x.clear();
                                            var2x.getCurrentComponent().invalidate();
                                            var2x.getCurrentComponent().repaint();
                                        }
                                    } finally {
                                        ClientMain.this.dTs.cYc().brK();
                                    }
                                } catch (IOException var9) {
                                    var9.printStackTrace();
                                } /*catch (InterruptedException var10) {
                           return;
                        }*/

                            }
                        });
                    }

                    return var3;
                }
            });

            //Содержит шрифты tahoma, xirod
            avI cssNode = baseUItegXML.loadCSS((Object) null, (String) "res://data/styles/core.css");
            baseUItegXML.setCssNodeCore(cssNode);
            baseUItegXML.setRootPane(this.engineGraphics.aee().getRootPane());
            this.engineGraphics.aee().getContentPane().setLayout(null);

            baseUItegXML.setLoaderImage(new ILoaderImageInterface() {

                public Image getImage(String var1) {
                    if (var1 == null) {
                        return null;
                    } else if (var1.equals("none")) {
                        return null;
                    } else {
                        Image var2 = ClientMain.this.engineGame.bhd().adz().getImage(var1);
                        return var2;
                    }
                }

                public Cursor getNull(String var1, Point var2) {
                    return ClientMain.this.engineGame.bhd().adz().getNull(var1, var2);
                }
            });


            baseUItegXML.a(new age_q() {
                /** Загрузка шрифта
                 * @param var1 Имя шрифта, tahoma
                 * @param var2 размер шрифта  11
                 * @param var3  стиль шрифта 0
                 * @param var4 false
                 * @return
                 */
                public Font b(String var1, int var2, int var3, boolean var4) {
                    if (var1 == null) {
                        return null;
                    } else if (var1.equals("none")) {
                        return null;
                    } else {
                        Font var5 = ClientMain.this.engineGame.bhd().ddG().b(var1, var3, var2, var4);
                        return var5;
                    }
                }
            });


            DefaultKeyboardFocusManager var4 = new DefaultKeyboardFocusManager() {
                public boolean dispatchEvent(AWTEvent var1) {
                    if (ClientMain.this.engineGraphics != null && ClientMain.this.engineGraphics.aee() != null) {
                        synchronized (ClientMain.this.engineGraphics.aee().getTreeLock()) {
                            boolean var4;
                            /*try {*/
                            ClientMain.this.dTs.cYc().brK();

                            try {
                                var4 = super.dispatchEvent(var1);
                            } finally {
                                ClientMain.this.dTs.cYc().brL();
                            }
                     /*} catch (InterruptedException var9) {
                        var9.printStackTrace();
                        return false;
                     }*/

                            return var4;
                        }
                    } else {
                        return super.dispatchEvent(var1);
                    }
                }
            };


            var4.setDefaultFocusTraversalPolicy(new LayoutFocusTraversalPolicy());
            KeyboardFocusManager.setCurrentKeyboardFocusManager(var4);

            RepaintManager.setCurrentManager(new amC());
            this.addonManager = new AddonManager(new aUM(this.engineGame));
            this.addonManager.b(baseUItegXML);//Базовые элементы интерфейса
            this.engineGame.a(this.addonManager);
            if ((new File("addons")).exists()) {
                this.addonManager.g(new File("addons")); //addons\taikodom.xml
            } else {
                // this.addonManager.parseSelectors(new File("../utaikodom.game.addons/addons"));
            }

        } catch (Exception var5) {
            logger.warn("Error loading addon manager", var5);
        }

    }

    /**
     * Запуск окна
     */
    public void run() {
        try {
            this.boL();//создание движка отрисовки
            this.dTs.h(Thread.currentThread());
            acs.eb(false);//Настраиваем Блакировщик потока доступ к графическому движку например из аддона class InterfaceSFXAddon
            acs.a(this.engineGame);
        } catch (Exception var2) {
            logger.error("Error initializing modules. Client will shutdown.", var2);
            var2.printStackTrace();
            return;
        }

        if (this.addonManager != null) {
            this.addonManager.aVS();
        }

        if (null != null) {
            while (true) {
                try {
                    this.K(acF.fdL, acF.fdM);
                    break;
                } catch (Exception var4) {
                    logger.info("could not connect trying again");//не удалось подключиться снова
                    PoolThread.sleep(500L);//millis
                }
            }
        }


        this.dTw = 0;
        logger.info("TaikodomClient version [1.0.4933.55] is entering main loop");
        this.engineGame.getEventManager().publish(new aUU((short) 0));
        this.engineGraphics = this.engineGame.getEngineGraphics();

        try {
            this.dTx = atM.currentTimeMillis() - 10L;
            watchDog.f((Thread) null);

            while (this.engineGame.getExit() && this.boN()) {
            }
        } catch (Exception var3) {
            var3.printStackTrace();
        }

        Thread var1 = new Thread() {
            public void run() {
                try {
                    Thread.sleep(8000L);
                } catch (InterruptedException var2) {
                    var2.printStackTrace();
                }

                System.exit(0);
            }
        };
        var1.setDaemon(true);
        var1.start();
        if (this.engineGame.getLoaderTrail() != null) {
            logger.info("releasing resources...");
            this.engineGame.getLoaderTrail().dispose();
        }

        if (this.engineGraphics != null) {
            this.engineGraphics.close();
        }

        logger.info(" *** terminated *** ");
    }

    /**
     * В бесонечном цикле
     *
     * @return
     */
    private boolean boN() {
        long var1 = System.nanoTime();
        if (KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner() == null) {
            synchronized (this.engineGraphics.aee().getTreeLock()) {
                if (KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner() == null) {
                    this.engineGraphics.aee().requestFocus();
                }
            }
        }

        if (this.engineGraphics != null && this.engineGraphics.aeu())//true false
        {
            this.engineGame.disconnect();
            this.engineGame.setExit();
            return false;
        } else if (this.engineGraphics.aeB())//false
        {

            this.engineGraphics.aeC();//Начать проигрывание видео заставки

            try {
                this.engineGraphics.swapBuffers();
            } catch (RuntimeException var20) {
                logger.error(var20);
            }

            return true;
        } else {
            this.dTs.cXZ();

            float var3;
            long var4;
            try {
                var4 = atM.currentTimeMillis();

                if (this.engineGame.aPT() != null && this.engineGame.bhc() != null && !this.engineGame.aPT().isUp() && !this.dTu) {//Ставим задачу для потока постоянно проверять не требуется ли разорвать соединение
                    this.dTu = true;
                    this.dTt.execute(new Runnable() {
                        public void run() {
                            try {
                                ClientMain.this.boP();
                            } finally {
                                ClientMain.this.dTu = false;
                            }
                        }
                    });
                }

                var3 = (float) (var4 - this.dTx) * 0.001F;
                this.dTx = var4;
                if (var3 <= 1.0E-8F) {
                    var3 = 1.0E-8F;
                }


                Threading.disableSingleThreading();
                this.hm(var3);
                this.engineGraphics.aew();
                if (this.addonManager != null) {
                    float finalVar = var3;
                    this.dTt.execute(new Runnable() {
                        public void run() {
                            ClientMain.this.addonManager.step(finalVar);//выполнение аддонов
                        }
                    });
                }

                ArrayList var6 = new ArrayList();
                var6.addAll(this.engineGraphics.aeo());
                Iterator var8 = var6.iterator();
                SceneEvent var7;
                while (var8.hasNext()) {
                    var7 = (SceneEvent) var8.next();
                    SceneEvent finalVar = var7;
                    this.dTt.execute(new Runnable() {
                        public void run() {
                            ClientMain.this.engineGame.getEventManager().d(finalVar.getMessage(), finalVar.getObject());
                        }
                    });
                }

                var8 = this.engineGame.bhd().aeo().iterator();

                while (true) {
                    if (!var8.hasNext()) {
                        this.engineGame.bhd().aeo().clear();
                        this.engineGame.getLoaderTrail().vu();
                        this.all(this.dTt, var3);
                        break;
                    }

                    var7 = (SceneEvent) var8.next();
                    SceneEvent finalVar1 = var7;
                    this.dTt.execute(new Runnable() {
                        public void run() {
                            ClientMain.this.engineGame.getEventManager().d(finalVar1.getMessage(), finalVar1.getObject());
                        }
                    });
                }
            } finally {
                this.dTs.cYb();
            }

            try {
                this.engineGraphics.swapBuffers();
            } catch (RuntimeException var21) {
                logger.error(var21);
            }

            this.hl(var3);
            this.dTs.cXZ();

            try {/*
            if (this.engineGame.ala() != null)
            {
               this.engineGame.ala().aLW().dvJ();
               this.engineGame.ala().aLW().gZ();
            }
*/
                this.all(this.engineGraphics, (float) (var4 - this.startTime) * 0.001F, var3);
                BaseUItegXML.thisClass.a(this.engineGraphics.getRenderView());
                ImageLoader.processDestroyList(this.engineGraphics.getRenderView().getDrawContext());
                ImageLoader.processForcedUpdate();
                this.engineGraphics.aeE();
                ++this.dTw;
            } finally {
                this.dTs.cYb();
            }

            this.boO();
            long var25 = (long) (7.0D - (double) (System.nanoTime() - var1) * 1.0E-6D);
            PoolThread.sleep(Math.max(2L, Math.min(7L, var25)));
            return true;
        }
    }

    private void all(EngineGraphics var1, float var2, float var3) {
        var1.step(var3);
        var1.i(var2, var3);
        var1.adW();
    }

    private void hl(float var1) {
        try {
            this.engineGame.bhd().step(var1);
        } catch (Exception var3) {
            System.err.println("Ignoring exception: " + var3.getMessage());
            var3.printStackTrace();
        }

    }

    private void boO() {
        try {
            this.engineGame.bhd().render();
        } catch (Exception var2) {
            System.err.println("Ignoring exception: " + var2.getMessage());
            var2.printStackTrace();
        }

    }

    private void all(aiD var1, float var2) {
        if (this.engineGame.bhc() != null) {
            this.engineGame.bhc().b(var2, var1);
            var1.cCM();
        }

    }

    private void hm(float var1) {
        if (this.engineGame.bhc() != null) {
            this.dTt.execute(new Runnable() {
                public void run() {
                    ClientMain.this.engineGame.bhc().bGS().gZ();
                }
            });
        }

        this.engineGame.alf().a((long) (var1 * 1000.0F), this.dTt);
        this.dTt.cCM();
    }

    /**
     * Отключение от сервера
     */
    private void boP() {
        this.engineGame.cleanUp();
        this.engineGame.disconnect();
        logger.info("*** disconnected from server *** ");
        this.engineGame.getEventManager().h(new RJ());
    }

    /**
     * Подключению к серверу логинизации
     *
     * @param var1 username
     * @param var2 password
     */
    public void K(String var1, String var2) {
        this.all(var1, var2, false);
    }

    /**
     * Подключению к серверу логинизации
     *
     * @param var1 username
     * @param var2 password
     * @param var3
     */
    public void all(String var1, String var2, boolean var3) {
        this.engineGame.d((ahG) null);
        String var4 = "localhost";//Адрес подключения по умолчанию
        int var5 = 15225;//Порт подключения по умолчанию
        ConfigManager var6 = this.engineGame.getConfigManager();//Получили настройки со всеми разделами

        try {//пробуем достать настройки
            ConfigManagerSection var7 = var6.getSection(setKeyValue.network_server.getValue());//Получили Параметры раздела настроек
            var4 = var7.getValuePropertyString(setKeyValue.network_server, var4);//Например
            var5 = var7.getValuePropertyInteger(setKeyValue.network_tcpPort, Integer.valueOf(var5)).intValue();
        } catch (RuntimeException var30) {
            logger.info(var30);
        }

        Ta var34 = new Ta("ClientMain.doLogin");
        String[] var8 = var4.split(",");
        int var9 = 0;
        boolean var10 = false;
        boolean var11 = false;
        ahG var12 = null;


        while (!var10) {
            var11 = this.dTs.cYc().brL();//блокировщик потока

            try {
                this.engineGame.getEventManager().h(new wv(10));
                logger.info("Trying to_q connect to_q " + var8[var9] + ":" + var5);//Попытка to_q подключиться к_q
                //TODO Здесь надо цикл сделать , чтобы каждый адрес в var8 проверило на подключение
                try {
                    var12 = afq_q.a(var8[var9], var5, var1, var2, var3, "1.0.4933.55");
                } catch (Exception var266) {
                    logger.error(var266);
                }
                this.engineGame.d(var12);
                var10 = true;
            } /*catch (cU_q var32)
         {
            this.engineGame.aVU().parseStyleSheet(new wv(1));
            ++var9;
            if (var9 >= var8.length)
            {
               throw new ServerLoginException();
            }
         }*/ finally {
                if (var11) {
                    this.dTs.cYc().brK();//блокировщик потока
                }

            }
        }


        this.engineGame.getEventManager().h(new wv(11));
        if (var10) {
            var4 = var8[var9];

            for (int var13 = 1; var13 < var8.length; ++var13) {
                ++var9;
                var9 %= var8.length;
                var4 = var4 + "," + var8[var9];
            }

            ConfigManagerSection var35 = var6.getSection(setKeyValue.network_server.getValue());
            var35.setPropertyString(setKeyValue.network_server, var4);

            try {
                var6.loadConfigIni("config/user.ini");
            } catch (IOException var29) {
                logger.error(var29);
            }
        }


        var11 = this.dTs.cYc().brL();

        ar var36;
        try {
            SH var14 = new SH();
            var14.c(var12);
            var14.b(Lt.duw);
            // var14.ai(DN.class);
            var14.ev(false);
            var14.ew(false);
            var14.ac(this.engineGame);
            logger.info("Getting environment");
            this.engineGame.getEventManager().h(new wv(12));
        /* var36 = new ar(var14, new ar.all()
         {
            public void s(String var1) {
               ClientMain.this.engineGame.aVU().parseStyleSheet(new wv(14, var1));
            }

            public void all(String var1, float var2, float var3) {
               ClientMain.this.engineGame.aVU().parseStyleSheet(new wv(14, var1, var2, var3));
            }
         });
         var36.init();*/
            this.engineGame.getEventManager().h(new wv(13));
            logger.info("Environment received");
        } finally {
            if (var11) {
                this.dTs.cYc().brK();
            }

            this.engineGame.d(var12);
        }

        // var36.setGreen(this.dTs.cYc());
        var34.buE();


     /* if (this.engineGame.ala() != null)
      {
         throw new IllegalStateException("Environment not yet disposed");//Окружающая среда еще не расположена
      } else */
        {
            //  this.engineGame.CreateJComponent((Jz)var36);
            // aDk var37 = this.engineGame.bhe();
       /*  if (var37.cST())
         {//Редактор не может войти в клиент
            throw new RuntimeException("########################################\createProcessingInstructionSelector Editor não pode logar test_GLCanvas cliente!!!!\createProcessingInstructionSelector#######################################\createProcessingInstructionSelector ");
         } else */
            {
                var34.buE();
                this.engineGame.getEventManager().h(new wv(20));
                var34.buE();
                String var15 = null;

                // try {
                var15 = var6.kX("client.id");
                // var37.lP(var15);
     /*       } catch (IOException var28) {

               var15 = var37.cTp();
            }
*/
                logger.info("Log in with: " + /*var37.getFullName() +*/ " - GUID: " + (var15 != null ? var15 : "not available"));
                var34.buE();
                var34.buE();
                this.engineGame.c(var12);
            }
        }

    }

    /**
     * Загрузка настроек
     *
     * @param var1 Менеджер настроек
     */
    private void ConfigManagerLoad(ConfigManager var1) {
        try {//Загрузка настроек по умолчанию
            var1.loadConfig("config/defaults.ini");
            logger.trace("  Default configuration file loaded");
        } catch (IOException var5) {
            logger.error("  Unable to_q load configuration file", var5);
        }
        var1.setMarkAsDefaultConfig();
        try {//Загрузка настроек пользователя
            var1.loadConfig("config/user.ini");
            logger.trace("  User configuration file loaded");
        } catch (IOException var4) {
            logger.warn("  User configuration file not loaded (does not exist?)");
        }
        try {//Загрузка настроек разработчика
            var1.loadConfig("config/devel.ini");
            logger.warn("  Development configuration file loaded");
        } catch (IOException var3) {
            logger.warn("  Development configuration file not loaded (does not exist?)");
        }
    }


}
