package taikodom.geom;

import all.OrientationBase;
import all.ajK;
import com.hoplon.geometry.Vec3f;

import javax.vecmath.Quat4f;

/**
 * @deprecated Deprecated
 */
@Deprecated
public final class Orientation extends OrientationBase {

    public static final float PRECISION = 1.0E-5F;

    public Orientation() {
    }

    protected Orientation(Vec3f var1, double var2) {
        super(var1, var2);
    }

    public Orientation(double var1, double var3, double var5) {
        super(var1, var3, var5);
    }

    public Orientation(OrientationBase var1) {
        super(var1);
    }

    public Orientation(ajK var1) {
        super(var1);
    }

    public Orientation(float var1, float var2, float var3, float var4) {
        super(var2, var3, var4, var1);
    }

    public static Orientation a(Vec3f var0, double var1) {
        return new Orientation(var0, var1);
    }

    public static Orientation b(Vec3f var0, double var1) {
        return new Orientation(var0, var1 * 3.141592653589793D / 180.0D);
    }

    public static Orientation a(double var0, double var2, double var4) {
        return new Orientation(var0, var2, var4);
    }

    public static Orientation b(double var0, double var2, double var4) {
        return new Orientation(var0 * 3.141592653589793D / 180.0D, var2 * 3.141592653589793D / 180.0D, var4 * 3.141592653589793D / 180.0D);
    }

    public boolean equals(Object var1) {
        return !(var1 instanceof OrientationBase) ? false : this.b((OrientationBase) var1);
    }

    public boolean b(OrientationBase var1) {
        return this.d(this.x, var1.x) && this.d(this.y, var1.y) && this.d(this.z, var1.z) && this.d(this.w, var1.w);
    }

    private boolean d(float var1, float var2) {
        return Math.abs(var1 - var2) < PRECISION;
    }

    public Orientation d(Orientation var1) {
        return new Orientation(super.d((OrientationBase) var1));
    }

    public Orientation yY() {
        return new Orientation(super.zd());
    }

    public Orientation yZ() {
        return new Orientation(super.zc());
    }

    public Orientation za() {
        return new Orientation(super.zb());
    }

    public Orientation a(Orientation var1, float var2) {
        return new Orientation(OrientationBase.a(this, var1, var2, new OrientationBase()));
    }

    public Orientation a(Orientation var1, Orientation var2, float var3) {
        return new Orientation(OrientationBase.a(var1, var2, var3, new OrientationBase()));
    }

    public Orientation e(Orientation var1) {
        return new Orientation(super.d((Quat4f) var1));
    }
}
