package taikodom.infra.enviroment.jmx;

import all.*;
import taikodom.infra.script.Measure;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.util.*;
import java.util.Map.Entry;

public class EnvironmentConsole implements aMi, EnvironmentConsoleMBean {
    private final Jz cVF;
    long cVG;
    long cVH;
    ThreadMXBean cVI;
    Map cVJ;
    int cVK;
    long cVL = System.currentTimeMillis();
    long cVM = System.currentTimeMillis();
    private long cVN;
    private long cVO = System.currentTimeMillis();
    private long cVP = 0L;
    private long cVQ = System.currentTimeMillis();
    private float cVR = 0.0F;

    public EnvironmentConsole(Jz var1) {
        this.cVF = var1;
        this.cVG = 0L;
        this.cVH = -1L;
        this.cVI = ManagementFactory.getThreadMXBean();
        this.cVJ = new HashMap();
        if (this.cVI.isThreadCpuTimeSupported()) {
            this.cVI.setThreadCpuTimeEnabled(true);
        } else {
            System.err.println("CPU Time not supported!");
        }

        this.cVK = ManagementFactory.getOperatingSystemMXBean().getAvailableProcessors();
    }

    public float getTrans_rerunPerSecond() {
        long var1 = 0L;
        azC var3 = this.cVF.bGr();
        var1 += var3.hap.getAndSet(0L);
        long var4 = System.currentTimeMillis();
        long var6 = var4 - this.cVM;
        float var8 = (float) var1 / ((float) var6 / 1000.0F);
        this.cVM = var4;
        return var8;
    }

    public long getTrans_averageTime() {
        azC var1 = this.cVF.bGr();
        long var2 = var1.ham.getAndSet(0L);
        long var4 = var1.han.getAndSet(0L);
        if (var4 != 0L) {
            this.cVG = var2 / var4;
        }

        return this.cVG;
    }

    public float getTrans_perSecond() {
        long var2 = System.currentTimeMillis();
        long var4 = var2 - this.cVL;
        var4 = var4 == 0L ? 1L : var4;
        azC var6 = this.cVF.bGr();
        float var1 = (float) var6.haw.getAndSet(0L) / ((float) var4 / 1000.0F);
        this.cVL = var2;
        return var1;
    }

    public long getTrans_maxTime() {
        long var1 = this.cVF.bGr().hav.get();
        return var1 != Long.MIN_VALUE ? var1 : 0L;
    }

    public long getTrans_minTime() {
        long var1 = this.cVF.bGr().hau.get();
        return var1 != Long.MAX_VALUE ? var1 : 0L;
    }

    public double getTrans_stdDeviation() {
        long var1 = 0L;
        long var3 = 0L;
        long var5 = 0L;
        azC var7 = this.cVF.bGr();
        var1 += var7.har.get();
        var3 += var7.has.get();
        var5 += var7.haq.get();
        double var8 = Math.sqrt(((double) var3 - (double) (var1 * var1) / (double) var5) / (double) var5);
        return var8;
    }

    public float getDataReq_perSecond() {
        long var1 = 0L;
        azC var3 = this.cVF.bGr();
        var1 += var3.hat.getAndSet(0L);
        long var4 = System.currentTimeMillis();
        long var6 = var4 - this.cVN;
        float var8 = (float) var1 / ((float) var6 / 1000.0F);
        this.cVN = var4;
        return var8;
    }

    public float getCPU_usage() {
        if (this.cVH == -1L) {
            this.cVH = System.nanoTime();
            return 0.0F;
        } else {
            long[] var1 = this.cVI.getAllThreadIds();
            long var2 = 0L;
            long[] var8 = var1;
            int var7 = var1.length;

            long var4;
            for (int var6 = 0; var6 < var7; ++var6) {
                var4 = var8[var6];
                long var9 = this.cVI.getThreadCpuTime(var4);
                if (var9 == -1L) {
                    this.cVJ.remove(var4);
                } else {
                    Long var11 = (Long) this.cVJ.get(var4);
                    if (var11 != null) {
                        var2 += var9 - var11.longValue();
                        this.cVJ.put(var4, var9);
                    } else {
                        this.cVJ.put(var4, new Long(0L));
                    }
                }
            }

            var4 = System.nanoTime();
            long var12 = var4 - this.cVH;
            this.cVH = var4;
            float var13 = (float) var2 * 100.0F / (float) var12 / (float) this.cVK;
            if (var13 > 100.0F) {
                var13 = 100.0F;
            } else if (var13 < 0.0F) {
                var13 = 0.0F;
            }

            return var13;
        }
    }

    public SortedMap getTransactionalMethods() {
        List var1 = this.getTransactionalData();
        TreeMap var2 = new TreeMap();
        Iterator var4 = var1.iterator();

        while (var4.hasNext()) {
            Measure var3 = (Measure) var4.next();
            var2.put(var3.getClassName() + "." + var3.getName(), var3);
        }

        return var2;
    }

    public String getTransactionalReport() {
        String var1 = this.getRealTransactionalReport();
        return var1;
    }

    public String getRealTransactionalReport() {
        try {
            List var1 = this.getTransactionalData();
            long var2 = 0L;
            long var4 = 0L;
            uu var6 = new uu();
            StringBuilder var7 = new StringBuilder();
            Iterator var9 = var1.iterator();

            while (var9.hasNext()) {
                Measure var8 = (Measure) var9.next();
                var2 += var8.enC;
                var4 += (long) var8.enA;
                var7.append(var6.c("method", var8.getClassName() + "." + var8.getName(), "tran. time", var8.enC, "tran. count", var8.enA, "avg tran. time", var8.eno, "perc of up time", var8.enu, "exec. count", var8.enp, "exec. time", var8.enq, "avg exec. time", var8.ene, "tran. reexec. count", var8.enB, "avg reexec. perc.", var8.eni, "avg objs touched in tran.", var8.enh, "avg objs created in tran.", var8.enK, "avg write locks tran.", var8.enJ, "avg read locks in tran.", var8.enI, "avg objs changed in tran.", var8.enH, "avg tran. methods calls", var8.enl, "avg tran. gets", var8.enj, "avg tran. sets", var8.enm, "avg objs touched in postponed", var8.eng, "avg tran. sets in postponed", var8.enn, "avg tran. gets in postponed", var8.enk, "avg infra Methods calls", var8.enf)).append("\n");
            }

            var7.append(var6.c("method", "----> Total", "tran. time", var2, "tran. count", var4, "avg tran. time", (float) var2 / (float) var4, "perc of up time", 100.0F * ((float) var2 / ((float) this.getUpTimeInMillis() * 1000.0F)))).append("\n");
            var7.append("Obs. Times in micro seconds").append("\r\n");
            var7.append("Up time: " + this.getUpTime()).append("\r\n");
            String var11 = var7.toString();
            return var11;
        } catch (RuntimeException var10) {
            var10.printStackTrace();
            return var10.toString();
        }
    }

    private List getTransactionalData() {
        ArrayList var1 = new ArrayList();
        Iterator var3 = ZS.eRE.entrySet().iterator();

        while (var3.hasNext()) {
            Entry var2 = (Entry) var3.next();
            if (var2.getValue() instanceof fm_q) {
                fm_q var4 = (fm_q) var2.getValue();
                if (var4.getTransactionCount() > 0) {
                    Measure var5 = new Measure();
                    var5.name = var4.name();
                    var5.className = var4.hD().getName();
                    var5.enC = var4.getTransactionTime();
                    var5.enu = 100.0F * ((float) var5.enC / ((float) this.getUpTimeInMillis() * 1000.0F));
                    var5.enA = var4.getTransactionCount();
                    var5.eno = (float) var5.enC / (float) var5.enA;
                    var5.enp = var4.getExecuteCount();
                    var5.enq = var4.getExecuteTime();
                    var5.ene = (float) var5.enq / (float) var5.enp;
                    var5.enB = (long) (var5.enp - var5.enA);
                    var5.eni = 100.0F * (float) (var5.enp - var5.enA) / (float) var5.enA;
                    var5.ent = var4.getObjectsTouchedInTransaction();
                    var5.enx = var4.getTransactionalMethodsCalled();
                    var5.env = var4.getTransactionalGets();
                    var5.eny = var4.getTransactionalSets();
                    var5.ens = var4.getObjectsTouchedInPostponed();
                    var5.enz = var4.getTransactionalSetsInPostponed();
                    var5.enw = var4.getTransactionalGetsInPostponed();
                    var5.enr = var4.getInfraMethodsCalled();
                    var5.enh = (float) var4.getObjectsTouchedInTransaction() / (float) var5.enA;
                    var5.enl = (float) var4.getTransactionalMethodsCalled() / (float) var5.enA;
                    var5.enj = (float) var4.getTransactionalGets() / (float) var5.enA;
                    var5.enm = (float) var4.getTransactionalSets() / (float) var5.enA;
                    var5.eng = (float) var4.getObjectsTouchedInPostponed() / (float) var5.enA;
                    var5.enn = (float) var4.getTransactionalSetsInPostponed() / (float) var5.enA;
                    var5.enk = (float) var4.getTransactionalGetsInPostponed() / (float) var5.enA;
                    var5.enf = (float) var4.getInfraMethodsCalled() / (float) var5.enA;
                    var5.enK = (float) var4.getObjectsCreatedInTransaction() / (float) var5.enA;
                    var5.enJ = (float) var4.getWriteLocksInTransaction() / (float) var5.enA;
                    var5.enI = (float) var4.getReadLocksInTransaction() / (float) var5.enA;
                    var5.enH = (float) var4.getObjectsChangedInTransaction() / (float) var5.enA;
                    var1.add(var5);
                }
            }
        }

        Collections.sort(var1, new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                return a((Measure) o1, (Measure) o2);
            }

            public int a(Measure var1, Measure var2) {
                long var3 = var1.enC - var2.enC;
                return -(var3 > 0L ? 1 : (var3 == 0L ? 0 : -1));
            }
        });
        return var1;
    }

    public String getUpTime() {
        long var1 = System.currentTimeMillis() - this.cVF.eKo;
        String var3 = String.format("%2d %02d:%02d:%02d ", var1 / 86400000L, var1 / 3600000L % 24L, var1 / 60000L % 60L, var1 / 1000L % 60L);
        System.out.println(var3);
        return var3;
    }

    public long getUpTimeInMillis() {
        return System.currentTimeMillis() - this.cVF.eKo;
    }

    public int getScriptObjectCount() {
        return this.cVF.getScriptObjectCount();
    }

    public long getPullCount() {
        long var1 = 0L;

        Long var3;
        for (Iterator var4 = af.dx().values().iterator(); var4.hasNext(); var1 += var3.longValue()) {
            var3 = (Long) var4.next();
        }

        return var1;
    }

    public float getPulls_perSecond() {
        long var1 = this.getPullCount();
        long var3 = var1 - this.cVP;
        this.cVP = var1;
        long var5 = System.currentTimeMillis();
        long var7 = var5 - this.cVO;
        var7 = var7 == 0L ? 1L : var7;
        this.cVO = var5;
        return (float) var3 / ((float) var7 / 1000.0F);
    }

    public float getWriteLocks_perSecond() {
        long var1 = System.currentTimeMillis();
        long var3 = var1 - this.cVQ;
        var3 = var3 == 0L ? 1L : var3;
        this.cVQ = var1;
        float var5 = 0.0F;

        Measure var6;
        for (Iterator var7 = this.getTransactionalData().iterator(); var7.hasNext(); var5 += var6.enJ * (float) var6.enA) {
            var6 = (Measure) var7.next();
        }

        float var8 = var5 - this.cVR;
        this.cVR = var5;
        return var8 / ((float) var3 / 1000.0F);
    }

    public int getPendingRequestsCount() {
        return this.cVF.bHg();
    }
}
