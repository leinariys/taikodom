package taikodom.infra.script;

//TODO как пример данных
public class Measure implements MeasureMBean {
    public float ene;
    public float enf;
    public float eng;
    public float enh;
    public float eni;
    public float enj;
    public float enk;
    public float enl;
    public float enm;
    public float enn;
    public float eno;
    public String className;
    public int enp;
    public long enq;
    public int enr;
    public String name;
    public int ens;
    public int ent;
    public float enu;
    public int env;
    public int enw;
    public int enx;
    public int eny;
    public int enz;
    public int enA;
    public long enB;
    public long enC;
    public float enH;
    public float enI;
    public float enJ;
    public float enK;
    private int enD;
    private int enE;
    private int enF;
    private int enG;

    public float getAvgObjectsChangedInTransaction() {
        return this.enH;
    }

    public void setAvgObjectsChangedInTransaction(float var1) {
        this.enH = var1;
    }

    public float getAvgReadLocksInTransaction() {
        return this.enI;
    }

    public void setAvgReadLocksInTransaction(float var1) {
        this.enI = var1;
    }

    public float getAvgWriteLocksInTransaction() {
        return this.enJ;
    }

    public void setAvgWriteLocksInTransaction(float var1) {
        this.enJ = var1;
    }

    public float getAvgObjectsCreatedInTransaction() {
        return this.enK;
    }

    public void setAvgObjectsCreatedInTransaction(float var1) {
        this.enK = var1;
    }

    public float getAvgExecuteTime() {
        return this.ene;
    }

    public void setAvgExecuteTime(float var1) {
        this.ene = var1;
    }

    public float getAvgInfraMethodsCalled() {
        return this.enf;
    }

    public void setAvgInfraMethodsCalled(float var1) {
        this.enf = var1;
    }

    public float getAvgObjectsTouchedInPostponed() {
        return this.eng;
    }

    public void setAvgObjectsTouchedInPostponed(float var1) {
        this.eng = var1;
    }

    public float getAvgObjectsTouchedInTransaction() {
        return this.enh;
    }

    public void setAvgObjectsTouchedInTransaction(float var1) {
        this.enh = var1;
    }

    public float getAvgReexecutePerc() {
        return this.eni;
    }

    public void setAvgReexecutePerc(float var1) {
        this.eni = var1;
    }

    public float getAvgTransactionalGets() {
        return this.enj;
    }

    public void setAvgTransactionalGets(float var1) {
        this.enj = var1;
    }

    public float getAvgTransactionalGetsInPostponed() {
        return this.enk;
    }

    public void setAvgTransactionalGetsInPostponed(float var1) {
        this.enk = var1;
    }

    public float getAvgTransactionalMethodsCalled() {
        return this.enl;
    }

    public void setAvgTransactionalMethodsCalled(float var1) {
        this.enl = var1;
    }

    public float getAvgTransactionalSets() {
        return this.enm;
    }

    public void setAvgTransactionalSets(float var1) {
        this.enm = var1;
    }

    public float getAvgTransactionalSetsInPostponed() {
        return this.enn;
    }

    public void setAvgTransactionalSetsInPostponed(float var1) {
        this.enn = var1;
    }

    public float getAvgTransactionTime() {
        return this.eno;
    }

    public void setAvgTransactionTime(float var1) {
        this.eno = var1;
    }

    public String getClassName() {
        return this.className;
    }

    public void setClassName(String var1) {
        this.className = var1;
    }

    public int getExecuteCount() {
        return this.enp;
    }

    public void setExecuteCount(int var1) {
        this.enp = var1;
    }

    public long getExecuteTime() {
        return this.enq;
    }

    public void setExecuteTime(long var1) {
        this.enq = var1;
    }

    public int getInfraMethodsCalled() {
        return this.enr;
    }

    public void setInfraMethodsCalled(int var1) {
        this.enr = var1;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String var1) {
        this.name = var1;
    }

    public int getObjectsTouchedInPostponed() {
        return this.ens;
    }

    public void setObjectsTouchedInPostponed(int var1) {
        this.ens = var1;
    }

    public int getObjectsTouchedInTransaction() {
        return this.ent;
    }

    public void setObjectsTouchedInTransaction(int var1) {
        this.ent = var1;
    }

    public float getPercUpTime() {
        return this.enu;
    }

    public void setPercUpTime(float var1) {
        this.enu = var1;
    }

    public int getTransactionalGets() {
        return this.env;
    }

    public void setTransactionalGets(int var1) {
        this.env = var1;
    }

    public int getTransactionalGetsInPostponed() {
        return this.enw;
    }

    public void setTransactionalGetsInPostponed(int var1) {
        this.enw = var1;
    }

    public int getTransactionalMethodsCalled() {
        return this.enx;
    }

    public void setTransactionalMethodsCalled(int var1) {
        this.enx = var1;
    }

    public int getTransactionalSets() {
        return this.eny;
    }

    public void setTransactionalSets(int var1) {
        this.eny = var1;
    }

    public int getTransactionalSetsInPostponed() {
        return this.enz;
    }

    public void setTransactionalSetsInPostponed(int var1) {
        this.enz = var1;
    }

    public int getTransactionCount() {
        return this.enA;
    }

    public void setTransactionCount(int var1) {
        this.enA = var1;
    }

    public long getTransactionReexecuteCount() {
        return this.enB;
    }

    public void setTransactionReexecuteCount(long var1) {
        this.enB = var1;
    }

    public long getTransactionTime() {
        return this.enC;
    }

    public void setTransactionTime(long var1) {
        this.enC = var1;
    }

    public int getObjectsChangedInTransaction() {
        return this.enG;
    }

    public void setObjectsChangedInTransaction(int var1) {
        this.enG = var1;
    }

    public int getReadLocksInTransaction() {
        return this.enF;
    }

    public void setReadLocksInTransaction(int var1) {
        this.enF = var1;
    }

    public int getWriteLocksInTransaction() {
        return this.enE;
    }

    public void setWriteLocksInTransaction(int var1) {
        this.enE = var1;
    }

    public int getObjectsCreatedInTransaction() {
        return this.enD;
    }

    public void setObjectsCreatedInTransaction(int var1) {
        this.enD = var1;
    }
}
