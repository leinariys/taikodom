package taikodom.infra.script;

//TODO На удаление
public interface MeasureMBean {
    float getAvgObjectsChangedInTransaction();

    float getAvgReadLocksInTransaction();

    float getAvgWriteLocksInTransaction();

    float getAvgObjectsCreatedInTransaction();

    float getAvgExecuteTime();

    float getAvgInfraMethodsCalled();

    float getAvgObjectsTouchedInPostponed();

    float getAvgObjectsTouchedInTransaction();

    float getAvgReexecutePerc();

    float getAvgTransactionalGets();

    float getAvgTransactionalGetsInPostponed();

    float getAvgTransactionalMethodsCalled();

    float getAvgTransactionalSets();

    float getAvgTransactionalSetsInPostponed();

    float getAvgTransactionTime();

    String getClassName();

    int getExecuteCount();

    long getExecuteTime();

    int getInfraMethodsCalled();

    String getName();

    int getObjectsTouchedInPostponed();

    int getObjectsTouchedInTransaction();

    float getPercUpTime();

    int getTransactionalGets();

    int getTransactionalGetsInPostponed();

    int getTransactionalMethodsCalled();

    int getTransactionalSets();

    int getTransactionalSetsInPostponed();

    int getTransactionCount();

    long getTransactionReexecuteCount();

    long getTransactionTime();

    int getObjectsChangedInTransaction();

    int getReadLocksInTransaction();

    int getWriteLocksInTransaction();

    int getObjectsCreatedInTransaction();
}
