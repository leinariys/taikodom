package taikodom.infra.script.ai.behaviours;

public enum AimQuality {
    FIRE_ONLY_ON_TARGET,
    FIRE_NEAR_TARGET,
    FIRE_PREDICTING_MOVEMENT;
}
