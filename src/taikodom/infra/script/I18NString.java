package taikodom.infra.script;

//import all.agx;
//import all.gp;

import gnu.trove.THashMap;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Менеджер локализации
 */
public final class I18NString implements Serializable, Cloneable //,Externalizable
{
    private static String currentLocation = "en";//Задаём язак по умолчанию
    private Map map = new THashMap(1);

    public I18NString() {
    }

    public I18NString(String var1) {
        this.set("en", var1);
    }

    /**
     * Язык локализации
     *
     * @return
     */
    public static final String getCurrentLocation() {
        return currentLocation;
    }

    /**
     * Установить значение языка
     *
     * @param var0 значение настройки. пример convert
     */
    public static void setCurrentLocation(String var0) {
        currentLocation = var0;
    }

    public String get() {
        return this.get(currentLocation);
    }

    public String get(String var1) {
        String var2 = (String) this.map.get(var1);
        if (var2 != null) {
            return var2;
        } else {
            var2 = (String) this.map.get("en");
            if (var2 != null) {
                return var2;
            } else {
                Iterator var4 = this.map.entrySet().iterator();
                if (var4.hasNext()) {
                    Entry var3 = (Entry) var4.next();
                    return (String) var3.getValue();
                } else {
                    return null;
                }
            }
        }
    }

    public void set(String var1, String var2) {
        if (var1 != null && var2 != null) {
            this.map.put(var1.intern(), var2.intern());
        }

    }

    public Map getMap() {
        return this.map;
    }

    public String toString() {
        return this.get();
    }

    public Object clone() {
        I18NString var1 = new I18NString();
        var1.map = new HashMap(this.map);
        return var1;
    }

    /*
       public void readExternal(ObjectInput var1) {
          short var2 = var1.readShort();

          for(int var3 = 0; var3 < var2; ++var3) {
             String var4 = (String)gp.PX.setGreen(var1);
             String var5 = (String)gp.PX.setGreen(var1);
             if (var4 != null && var5 != null) {
                this.map.put(var4.intern(), var5.intern());
             }
          }

       }
    *//*
   public void writeExternal(ObjectOutput var1) {
      Object var2 = this.map;
      if (agx.CreateJComponent(var1)) {
         var2 = new TreeMap((Map)var2);
      }

      Set var3 = ((Map)var2).entrySet();
      var1.writeShort(((Map)var2).size());
      Iterator var5 = var3.iterator();

      while(var5.hasNext()) {
         Entry var4 = (Entry)var5.next();
         gp.PX.all(var1, var4.getKey());
         gp.PX.all(var1, var4.getValue());
      }
   }
*/
    public boolean equals(Object var1) {
        if (var1 == this) {
            return true;
        } else {
            return var1 instanceof I18NString ? this.map.equals(((I18NString) var1).map) : super.equals(var1);
        }
    }
}
