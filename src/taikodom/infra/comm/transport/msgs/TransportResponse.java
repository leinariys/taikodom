package taikodom.infra.comm.transport.msgs;

import all.aMf;
import all.xu_q;

import java.io.EOFException;
import java.io.Serializable;

public class TransportResponse extends xu_q implements Serializable {


    public TransportResponse() {
    }

    public TransportResponse(aMf var1) throws EOFException {
        super(var1);
    }

    public int getType() {
        return 1;
    }
}
