package taikodom.infra.comm.transport.msgs;

import all.aMf;
import all.xu_q;

import java.io.EOFException;
import java.io.Serializable;
import java.util.concurrent.atomic.AtomicInteger;

public class TransportCommand extends xu_q implements Serializable {

    private static AtomicInteger integer = new AtomicInteger();

    public TransportCommand() {
        int var1 = integer.incrementAndGet();
        this.setMessageId(var1);
        this.setThreadId(var1);
    }

    public TransportCommand(aMf var1) throws EOFException {
        super(var1);
    }

    public TransportResponse createResponse() {
        TransportResponse var1 = new TransportResponse();
        var1.setThreadId(this.getThreadId());
        var1.setMessageId(this.getMessageId());
        return var1;
    }

    public TransportCommand createNestedCommand() {
        TransportCommand var1 = new TransportCommand();
        var1.setThreadId(this.getThreadId());
        return var1;
    }

    public int getType() {
        return 0;
    }
}
