package taikodom.infra.comm.tcpnio;

import all.LZ;
import all.agj_q;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;

public class NIOBandwidthMeter implements NIOBandwidthMeterMBean {
    public static final long gpi = 10000L;
    private LZ gpg;
    private ObjectName gph = null;
    private agj_q gpj = new agj_q(10000L);
    private agj_q gpk = new agj_q(10000L);
    private agj_q gpl = new agj_q(10000L);
    private agj_q gpm = new agj_q(10000L);
    private agj_q gpn = new agj_q(10000L);
    private agj_q gpo = new agj_q(10000L);

    public NIOBandwidthMeter(LZ var1) {
        this.gpg = var1;
        this.cqY();
    }

    public void cqY() {
        MBeanServer var1 = ManagementFactory.getPlatformMBeanServer();
        if (var1 != null) {
            try {
                this.gph = new ObjectName("Network:name=NIO-" + this.gpg.getId());
                if (!var1.isRegistered(this.gph)) {
                    var1.registerMBean(this, this.gph);
                }
            } catch (Exception var3) {
                var3.printStackTrace();
            }

        }
    }

    public void cqZ() {
        MBeanServer var1 = ManagementFactory.getPlatformMBeanServer();
        if (var1 != null) {
            try {
                if (!var1.isRegistered(this.gph)) {
                    return;
                }

                var1.unregisterMBean(this.gph);
            } catch (Exception var3) {
                var3.printStackTrace();
            }

        }
    }

    public void dump() {
        System.out.printf("%6.0f %6.0f %6.0f %6.0f %6.0f %6.0f\n", this.getCompressedBytesSentPerSecond(), this.getUncompressedBytesSentPerSecond(), this.getCompressedBytesReceivedPerSecond(), this.getUncompressedBytesReceivedPerSecond(), this.getSentMessageCountPerSecond(), this.getReceivedMessageCountPerSecond());
    }

    public float getCompressedBytesReceivedPerSecond() {
        return this.gpj.hf((long) this.gpg.ayj());
    }

    public float getCompressedBytesSentPerSecond() {
        return this.gpk.hf((long) this.gpg.gG());
    }

    public float getReceivedMessageCountPerSecond() {
        return this.gpn.hf((long) this.gpg.ayk());
    }

    public float getSentMessageCountPerSecond() {
        return this.gpo.hf((long) this.gpg.gH());
    }

    public float getUncompressedBytesReceivedPerSecond() {
        return this.gpl.hf((long) this.gpg.ayl());
    }

    public float getUncompressedBytesSentPerSecond() {
        return this.gpm.hf((long) this.gpg.gI());
    }
}
