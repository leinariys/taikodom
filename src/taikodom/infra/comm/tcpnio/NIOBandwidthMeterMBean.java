package taikodom.infra.comm.tcpnio;

public interface NIOBandwidthMeterMBean {
    float getReceivedMessageCountPerSecond();

    float getSentMessageCountPerSecond();

    float getCompressedBytesReceivedPerSecond();

    float getCompressedBytesSentPerSecond();

    float getUncompressedBytesReceivedPerSecond();

    float getUncompressedBytesSentPerSecond();
}
