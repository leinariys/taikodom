package taikodom.render;

import taikodom.render.camera.Camera;
import taikodom.render.gui.GuiScene;
import taikodom.render.primitives.TreeRecord;
import taikodom.render.scene.Viewport;
import taikodom.render.shaders.RenderStates;
import taikodom.render.shaders.Shader;
import taikodom.render.shaders.ShaderPass;

import javax.media.opengl.GL;

public class RenderGui {
    private RenderStates defaultStates = new RenderStates();
    private GuiContext guiContext = new GuiContext();
    private Camera camera = new Camera();

    public RenderGui() {
        this.camera.setOrthogonal(true);
        this.camera.setFarPlane(1.0F);
        this.camera.setNearPlane(1.0F);
    }

    private void unbindPrimitive() {
        this.guiContext.getDc().getGL().glBindBufferARB(34962, 0);
        this.guiContext.getDc().getGL().glBindBufferARB(34963, 0);
    }

    public void render(GuiScene var1) {
        DrawContext var2 = this.guiContext.getDc();
        /**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
         * седержит методы для рисования примитивов (линии треугольники)*/
        GL var3 = var2.getGL();
        this.guiContext.clearRecords();
        RenderStates var4 = this.defaultStates;
        Viewport var5 = this.guiContext.viewPort;
        var3.glViewport(var5.x, var5.y, var5.width, var5.height);
        var2.setViewport(var5.x, var5.y, var5.width, var5.height);

        var1.render(this.guiContext);
        this.defaultStates.blindlyUpdateGLStates(var2);
        var3.glLoadIdentity();
        this.camera.updateGLProjection(var2);
        var3.glEnableClientState(32884);

        try {
            for (int var6 = 0; var6 < this.guiContext.getRecords().size(); ++var6) {
                TreeRecord var7 = this.guiContext.getRecords().get(var6);
                Shader var8 = var7.shader;

                for (int var9 = 0; var9 < var7.material.getShader().passCount(); ++var9) {
                    this.unbindPrimitive();
                    ShaderPass var10 = var2.currentPass = var8.getPass(var9);
                    var10.updateStates(var2, var4);
                    var4 = var2.currentPass.getRenderStates();
                    var4.fillPrimitiveStates(var2.primitiveState);
                    var4.fillMaterialStates(var2.materialState);
                    var2.getGL().glMatrixMode(5890);
                    var3.glLoadIdentity();
                    var7.material.bind(var2);
                    var2.getGL().glMatrixMode(5888);
                    var2.floatBuffer60Temp0.clear();
                    var3.glPushMatrix();
                    var3.glMultMatrixf(var7.gpuTransform.b(var2.floatBuffer60Temp0));
                    var3.glColor4f(var7.color.x, var7.color.y, var7.color.z, var7.color.w);
                    var7.primitive.bind(var2);
                    var7.primitive.draw(var2);
                    var10.unbind(var2);
                    var3.glPopMatrix();
                }
            }
        } catch (Exception var14) {
            var14.printStackTrace();
        } finally {
            var2.getGL().glMatrixMode(5890);
            var3.glLoadIdentity();
            var2.getGL().glMatrixMode(5888);
            var3.glDisableClientState(32884);
            this.defaultStates.blindlyUpdateGLStates(var2);
        }

    }

    public void resize(int var1, int var2, int var3, int var4) {
        this.camera.setOrthoWindow(0.0F, (float) var3, 0.0F, (float) var4);
        this.guiContext.setViewPort(var1, var2, var3, var4);
    }

    public GuiContext getGuiContext() {
        return this.guiContext;
    }

    public void setGuiContext(GuiContext var1) {
        this.guiContext = var1;
    }
}
