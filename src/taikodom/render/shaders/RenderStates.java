package taikodom.render.shaders;

import taikodom.render.DrawContext;
import taikodom.render.MaterialDrawingStates;
import taikodom.render.PrimitiveDrawingState;
import taikodom.render.enums.BlendType;
import taikodom.render.enums.CullFace;
import taikodom.render.enums.DepthFuncType;
import taikodom.render.gl.GLSupportedCaps;
import taikodom.render.textures.ChannelInfo;

import javax.media.opengl.GL;
import java.util.ArrayList;
import java.util.List;

public class RenderStates {
    protected float alphaRef = 0.0F;
    protected DepthFuncType alphaFunc;
    protected boolean alphaTestEnabled;
    protected BlendType dstBlend;
    protected BlendType srcBlend;
    protected boolean blendEnabled;
    protected boolean colorMask;
    protected CullFace cullFace;
    protected boolean cullFaceEnabled;
    protected boolean depthMask;
    protected boolean depthTestEnabled;
    protected DepthFuncType depthFunc;
    protected float depthRangeMin;
    protected float depthRangeMax;
    protected boolean glLightingEnabled;
    protected boolean fogEnabled = false;
    protected boolean alphaMask;
    protected boolean lineMode = false;
    protected float lineWidth = 1.0F;
    protected List channels = new ArrayList();
    protected boolean tangentArrayEnabled;
    protected int tangentChannel;
    protected boolean normalArrayEnabled;
    protected boolean colorArrayEnabled;

    public RenderStates() {
        this.alphaFunc = DepthFuncType.GREATER;
        this.alphaTestEnabled = false;
        this.dstBlend = BlendType.ONE;
        this.srcBlend = BlendType.ONE;
        this.blendEnabled = false;
        this.colorMask = true;
        this.alphaMask = true;
        this.cullFace = CullFace.BACK;
        this.cullFaceEnabled = true;
        this.depthMask = true;
        this.depthTestEnabled = true;
        this.depthFunc = DepthFuncType.LEQUAL;
        this.depthRangeMin = 0.0F;
        this.depthRangeMax = 1.0F;
        this.tangentChannel = 0;
        this.tangentArrayEnabled = false;
        this.normalArrayEnabled = false;
        this.colorArrayEnabled = false;
        this.glLightingEnabled = false;
    }

    public float getAlphaRef() {
        return this.alphaRef;
    }

    public void setAlphaRef(float var1) {
        this.alphaRef = var1;
    }

    public DepthFuncType getAlphaFunc() {
        return this.alphaFunc;
    }

    public void setAlphaFunc(DepthFuncType var1) {
        this.alphaFunc = var1;
    }

    public boolean isAlphaTestEnabled() {
        return this.alphaTestEnabled;
    }

    public void setAlphaTestEnabled(boolean var1) {
        this.alphaTestEnabled = var1;
    }

    public BlendType getDstBlend() {
        return this.dstBlend;
    }

    public void setDstBlend(BlendType var1) {
        this.dstBlend = var1;
    }

    public BlendType getSrcBlend() {
        return this.srcBlend;
    }

    public void setSrcBlend(BlendType var1) {
        this.srcBlend = var1;
    }

    public boolean isBlendEnabled() {
        return this.blendEnabled;
    }

    public void setBlendEnabled(boolean var1) {
        this.blendEnabled = var1;
    }

    public boolean isColorMask() {
        return this.colorMask;
    }

    public void setColorMask(boolean var1) {
        this.colorMask = var1;
    }

    public CullFace getCullFace() {
        return this.cullFace;
    }

    public void setCullFace(CullFace var1) {
        this.cullFace = var1;
    }

    public boolean isCullFaceEnabled() {
        return this.cullFaceEnabled;
    }

    public void setCullFaceEnabled(boolean var1) {
        this.cullFaceEnabled = var1;
    }

    public boolean isDepthMask() {
        return this.depthMask;
    }

    public void setDepthMask(boolean var1) {
        this.depthMask = var1;
    }

    public boolean isDepthTestEnabled() {
        return this.depthTestEnabled;
    }

    public void setDepthTestEnabled(boolean var1) {
        this.depthTestEnabled = var1;
    }

    public DepthFuncType getDepthFunc() {
        return this.depthFunc;
    }

    public void setDepthFunc(DepthFuncType var1) {
        this.depthFunc = var1;
    }

    public float getDepthRangeMin() {
        return this.depthRangeMin;
    }

    public void setDepthRangeMin(float var1) {
        this.depthRangeMin = var1;
    }

    public float getDepthRangeMax() {
        return this.depthRangeMax;
    }

    public void setDepthRangeMax(float var1) {
        this.depthRangeMax = var1;
    }

    public boolean isGlLightingEnabled() {
        return this.glLightingEnabled;
    }

    public void setGlLightingEnabled(boolean var1) {
        this.glLightingEnabled = var1;
        if (var1) {
            this.normalArrayEnabled = true;
        }

    }

    public boolean isFogEnabled() {
        return this.fogEnabled;
    }

    public void setFogEnabled(boolean var1) {
        this.fogEnabled = var1;
    }

    public boolean isLineMode() {
        return this.lineMode;
    }

    public void setLineMode(boolean var1) {
        this.lineMode = var1;
    }

    public float getLineWidth() {
        return this.lineWidth;
    }

    public void setLineWidth(float var1) {
        this.lineWidth = var1;
    }

    public boolean isTangentArrayEnabled() {
        return this.tangentArrayEnabled;
    }

    public void setTangentArrayEnabled(boolean var1) {
        this.tangentArrayEnabled = var1;
    }

    public int getTangentChannel() {
        return this.tangentChannel;
    }

    public void setTangentChannel(int var1) {
        this.tangentChannel = var1;
    }

    public boolean isNormalArrayEnabled() {
        return this.normalArrayEnabled;
    }

    public void setNormalArrayEnabled(boolean var1) {
        this.normalArrayEnabled = var1 || this.glLightingEnabled;
    }

    public boolean isColorArrayEnabled() {
        return this.colorArrayEnabled;
    }

    public void setColorArrayEnabled(boolean var1) {
        this.colorArrayEnabled = var1;
    }

    public List getChannels() {
        return this.channels;
    }

    public void fillPrimitiveStates(PrimitiveDrawingState var1) {
        var1.useColorArray = this.colorArrayEnabled;
        var1.useNormalArray = this.normalArrayEnabled;
        var1.useTangentArray = this.tangentArrayEnabled;
        var1.tangentChannel = this.tangentChannel;
        var1.setStates(this.channels);
    }

    public void updateGLStates(DrawContext var1, RenderStates var2) {
        /**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
         * седержит методы для рисования примитивов (линии треугольники)*/
        GL var3 = var1.getGL();
        if (var2.lineMode) {
            if (!this.lineMode) {
                var3.glPolygonMode(1032, 6914);
                var3.glDisable(10754);
                var3.glPolygonOffset(0.0F, 0.0F);
            }
        } else if (this.lineMode) {
            var3.glPolygonMode(1032, 6913);
            var3.glEnable(10754);
            var3.glPolygonOffset(-0.1F, 0.0F);
            var3.glLineWidth(this.lineWidth);
        }

        if (var2.alphaTestEnabled) {
            if (!this.alphaTestEnabled) {
                var3.glDisable(3008);
            } else if (var2.alphaFunc != this.alphaFunc || var2.alphaRef != this.alphaRef) {
                var3.glAlphaFunc(this.alphaFunc.glEquivalent(), this.alphaRef);
            }
        } else if (this.alphaTestEnabled) {
            var3.glEnable(3008);
            var3.glAlphaFunc(this.alphaFunc.glEquivalent(), this.alphaRef);
        }

        if (var2.blendEnabled) {
            if (!this.blendEnabled) {
                var3.glDisable(3042);
            } else if (var2.dstBlend != this.dstBlend || var2.srcBlend != this.srcBlend) {
                var3.glBlendFunc(this.srcBlend.glEquivalent(), this.dstBlend.glEquivalent());
            }
        } else if (this.blendEnabled) {
            var3.glEnable(3042);
            var3.glBlendFunc(this.srcBlend.glEquivalent(), this.dstBlend.glEquivalent());
        }

        boolean var4 = false;
        if (var2.alphaMask) {
            if (!this.alphaMask) {
                var4 = true;
            }
        } else if (this.alphaMask) {
            var4 = true;
        }

        if (var2.colorMask) {
            if (!this.colorMask) {
                var3.glColorMask(false, false, false, this.alphaMask);
            } else if (var4) {
                var3.glColorMask(true, true, true, this.alphaMask);
            }
        } else if (this.colorMask) {
            var3.glColorMask(true, true, true, this.alphaMask);
        } else if (var4) {
            var3.glColorMask(false, false, false, this.alphaMask);
        }

        if (var2.cullFaceEnabled) {
            if (!this.cullFaceEnabled) {
                var3.glDisable(2884);
            } else if (var2.cullFace != this.cullFace) {
                var3.glCullFace(this.cullFace.glEquivalent());
            }
        } else if (this.cullFaceEnabled) {
            var3.glEnable(2884);
            var3.glCullFace(this.cullFace.glEquivalent());
        }

        if (var2.depthMask) {
            if (!this.depthMask) {
                var3.glDepthMask(false);
            }
        } else if (this.depthMask) {
            var3.glDepthMask(true);
        }

        if (var2.depthTestEnabled) {
            if (!this.depthTestEnabled) {
                var3.glDisable(2929);
            } else if (var2.depthFunc != this.depthFunc) {
                var3.glDepthFunc(this.depthFunc.glEquivalent());
            }
        } else if (this.depthTestEnabled) {
            var3.glEnable(2929);
            var3.glDepthFunc(this.depthFunc.glEquivalent());
        }

        var3.glDepthRange((double) this.depthRangeMin, (double) this.depthRangeMax);
        if (var2.normalArrayEnabled) {
            if (!this.normalArrayEnabled) {
                var3.glDisableClientState(32885);
            }
        } else if (this.normalArrayEnabled) {
            var3.glEnableClientState(32885);
        }

        if (var2.colorArrayEnabled) {
            if (!this.colorArrayEnabled) {
                var3.glDisableClientState(32886);
            }
        } else if (this.colorArrayEnabled) {
            var3.glEnableClientState(32886);
        }

        if (var2.tangentArrayEnabled) {
            if (this.tangentArrayEnabled) {
                if (this.tangentChannel != var2.tangentChannel) {
                    var3.glDisableVertexAttribArrayARB(var2.tangentChannel);
                    var3.glEnableVertexAttribArrayARB(this.tangentChannel);
                }
            } else {
                var3.glDisableVertexAttribArrayARB(var2.tangentChannel);
            }
        } else if (this.tangentArrayEnabled) {
            var3.glEnableVertexAttribArrayARB(this.tangentChannel);
        }

        if (this.glLightingEnabled) {
            var3.glEnable(2896);
            var3.glEnable(2903);
            var3.glEnable(2977);
        } else {
            var3.glDisable(2896);
            var3.glDisable(2903);
            var3.glDisable(2977);
        }

        var3.glMatrixMode(5890);

        int var5;
        ChannelInfo var6;
        for (var5 = 0; var5 < var2.numTextureUsed(); ++var5) {
            var3.glActiveTexture('蓀' + var2.channelMapping(var5));
            var3.glClientActiveTexture('蓀' + var2.channelMapping(var5));
            var6 = var2.getChannel(var5);
            this.disableAutoTexCoords(var3, var6);
            var3.glDisableClientState(32888);
            var3.glTexEnvi(8960, 8704, 8448);
            var3.glDisable(3553);
            var3.glDisable(34067);
            var3.glLoadIdentity();
        }

        for (var5 = 0; var5 < this.numTextureUsed(); ++var5) {
            var3.glActiveTexture('蓀' + this.channelMapping(var5));
            var3.glClientActiveTexture('蓀' + this.channelMapping(var5));
            var6 = (ChannelInfo) this.channels.get(var5);
            this.setupAutomaticTexGen(var1, var3, var6);
            if (!var6.isAutoCoord() && this.getChannel(var5).getTexCoord() != -1) {
                var3.glEnableClientState(32888);
            }

            var3.glTexEnvi(8960, 8704, this.texEnvMode(var5));
        }

        var3.glMatrixMode(5888);
        var3.glActiveTexture(33984);
        var3.glClientActiveTexture(33984);
        if (this.fogEnabled) {
            var3.glEnable(2912);
        } else {
            var3.glDisable(2912);
        }

    }

    private void disableAutoTexCoords(GL var1, ChannelInfo var2) {
        if (var2.isSTexCoordGeneration()) {
            var1.glDisable(3168);
        }

        if (var2.isTTexCoordGeneration()) {
            var1.glDisable(3169);
        }

        if (var2.isRTexCoordGeneration()) {
            var1.glDisable(3170);
        }

    }

    private void setupAutomaticTexGen(DrawContext var1, GL var2, ChannelInfo var3) {
        if (var3.isSTexCoordGeneration()) {
            var2.glEnable(3168);
            var2.glTexGeni(8192, 9472, 9217);
            var2.glTexGenfv(8192, 9473, var3.getSEquation().fillBuffer(var1.floatBuffer60Temp0));
        }

        if (var3.isTTexCoordGeneration()) {
            var2.glEnable(3169);
            var2.glTexGeni(8193, 9472, 9217);
            var2.glTexGenfv(8193, 9473, var3.getTEquation().fillBuffer(var1.floatBuffer60Temp0));
        }

        if (var3.isRTexCoordGeneration()) {
            var2.glEnable(3170);
            var2.glTexGeni(8194, 9472, 9217);
            var2.glTexGenfv(8194, 9473, var3.getREquation().fillBuffer(var1.floatBuffer60Temp0));
        }

    }

    private int texEnvMode(int var1) {
        return this.getChannel(var1).getTexEnvMode().glEquivalent();
    }

    private int channelMapping(int var1) {
        return var1 >= this.channels.size() ? 0 : this.getChannel(var1).getChannelMapping();
    }

    public void blindlyUpdateGLStates(DrawContext var1) {
        /**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
         * седержит методы для рисования примитивов (линии треугольники)*/
        GL var2 = var1.getGL();
        if (this.lineMode) {
            var2.glPolygonMode(1032, 6913);
            var2.glEnable(10754);
            var2.glPolygonOffset(-0.1F, 0.0F);
            var2.glLineWidth(this.lineWidth);
        } else {
            var2.glPolygonMode(1032, 6914);
            var2.glDisable(10754);
            var2.glPolygonOffset(0.0F, 0.0F);
            var2.glLineWidth(1.0F);
        }

        if (this.alphaTestEnabled) {
            var2.glEnable(3008);
            var2.glAlphaFunc(this.alphaFunc.glEquivalent(), this.alphaRef);
        } else {
            var2.glDisable(3008);
        }

        if (this.blendEnabled) {
            var2.glEnable(3042);
            var2.glBlendFunc(this.srcBlend.glEquivalent(), this.dstBlend.glEquivalent());
        } else {
            var2.glDisable(3042);
        }

        if (this.colorMask) {
            var2.glColorMask(true, true, true, true);
        } else {
            var2.glColorMask(false, false, false, false);
        }

        if (this.cullFaceEnabled) {
            var2.glEnable(2884);
            var2.glCullFace(this.cullFace.glEquivalent());
        } else {
            var2.glDisable(2884);
        }

        var2.glDepthMask(this.depthMask);
        if (this.depthTestEnabled) {
            var2.glEnable(2929);
            var2.glDepthFunc(this.depthFunc.glEquivalent());
        } else {
            var2.glDisable(2929);
        }

        var2.glDepthRange((double) this.depthRangeMin, (double) this.depthRangeMax);
        if (this.normalArrayEnabled) {
            var2.glEnableClientState(32885);
        } else {
            var2.glDisableClientState(32885);
        }

        if (this.colorArrayEnabled) {
            var2.glEnableClientState(32886);
        } else {
            var2.glDisableClientState(32886);
        }

        if (this.tangentArrayEnabled) {
            var2.glEnableVertexAttribArrayARB(this.tangentChannel);
        } else {
            var2.glDisableVertexAttribArrayARB(this.tangentChannel);
        }

        if (this.glLightingEnabled) {
            var2.glEnable(2896);
            var2.glEnable(2903);
            var2.glEnable(2977);
        } else {
            var2.glDisable(2896);
            var2.glDisable(2903);
            var2.glDisable(2977);
        }

        var2.glMatrixMode(5890);

        int var3;
        for (var3 = 0; var3 < GLSupportedCaps.getMaxTexChannels(); ++var3) {
            var2.glActiveTexture('蓀' + var3);
            var2.glClientActiveTexture('蓀' + var3);
            var2.glDisableClientState(32888);
            var2.glTexEnvi(8960, 8704, 8448);
            var2.glDisable(3553);
            var2.glDisable(34067);
            var2.glDisable(3168);
            var2.glDisable(3169);
            var2.glDisable(3170);
            var2.glLoadIdentity();
        }

        var2.glMatrixMode(5888);

        for (var3 = 0; var3 < this.numTextureUsed(); ++var3) {
            var2.glActiveTexture('蓀' + this.channelMapping(var3));
            var2.glClientActiveTexture('蓀' + this.channelMapping(var3));
            var2.glEnableClientState(32888);
            var2.glTexEnvi(8960, 8704, this.texEnvMode(var3));
            this.setupAutomaticTexGen(var1, var2, (ChannelInfo) this.channels.get(var3));
        }

        for (var3 = 0; var3 < this.numTextureUsed(); ++var3) {
            var2.glActiveTexture('蓀' + this.channelMapping(var3));
            var2.glEnable(3553);
        }

        var2.glActiveTexture(33984);
        var2.glClientActiveTexture(33984);
        if (this.fogEnabled) {
            var2.glEnable(2912);
        } else {
            var2.glDisable(2912);
        }

    }

    public void fillMaterialStates(MaterialDrawingStates var1) {
        var1.numTextures = this.numTextureUsed();
        var1.setStates(this.channels);
    }

    public ChannelInfo getChannel(int var1) {
        return (ChannelInfo) this.channels.get(var1);
    }

    public void setChannel(int var1, ChannelInfo var2) {
        if (var1 == -1) {
            var1 = this.channels.size();
        }

        while (this.channels.size() <= var1) {
            this.channels.add(new ChannelInfo());
        }

        ((ChannelInfo) this.channels.get(var1)).assign(var2);
    }

    protected int numTextureUsed() {
        return this.channels.size();
    }

    public void updateDepthStates(DrawContext var1) {
        /**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
         * седержит методы для рисования примитивов (линии треугольники)*/
        GL var2 = var1.getGL();
        if (this.depthMask) {
            var2.glDepthMask(true);
        } else {
            var2.glDepthMask(false);
        }

        if (this.depthTestEnabled) {
            var2.glEnable(2929);
            var2.glDepthFunc(this.depthFunc.glEquivalent());
        } else {
            var2.glDisable(2929);
        }

        var2.glDepthRange((double) this.depthRangeMin, (double) this.depthRangeMax);
    }

    public boolean isAlphaMask() {
        return this.alphaMask;
    }

    public void setAlphaMask(boolean var1) {
        this.alphaMask = var1;
    }
}
