package taikodom.render.shaders;

import all.ajK;
import taikodom.render.DrawContext;
import taikodom.render.loader.RenderAsset;
import taikodom.render.shaders.parameters.ShaderParameter;

public class Matrix4fParameter extends ShaderParameter {
    private final ajK value = new ajK();

    public Matrix4fParameter() {
    }

    public Matrix4fParameter(Matrix4fParameter var1) {
        super(var1);
        this.value.set(var1.value);
    }

    public RenderAsset cloneAsset() {
        return new Matrix4fParameter(this);
    }

    public ajK getValue() {
        return this.value;
    }

    public void setValue(ajK var1) {
        this.value.set(var1);
    }

    public boolean bind(DrawContext var1) {
        if (var1.debugDraw()) {
            var1.logBind(this);
        }

        if (this.locationId == -1) {
            return false;
        } else {
            var1.getGL().glUniformMatrix4fv(this.locationId, 1, false, this.value.b(var1.floatBuffer60Temp0));
            return true;
        }
    }

    protected void doAssign(ShaderParameter var1) {
        this.setValue(((Matrix4fParameter) var1).getValue());
    }
}
