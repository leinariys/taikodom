package taikodom.render.shaders;

import taikodom.render.DrawContext;
import taikodom.render.enums.BlendType;
import taikodom.render.enums.CullFace;
import taikodom.render.enums.DepthFuncType;
import taikodom.render.enums.LightType;
import taikodom.render.loader.RenderAsset;
import taikodom.render.textures.ChannelInfo;

/**
 * Реализация класса указанного в имени тегов из файлов с расширением .pro
 */
public class ShaderPass extends RenderAsset {
    protected ShaderProgram program;
    protected RenderStates renderStates = new RenderStates();
    protected LightType useLights;
    protected boolean useSceneAmbientColor;
    protected boolean compiled;
    protected boolean validated;
    protected boolean useReflectCubemap;
    protected boolean useDiffuseCubemap;

    public ShaderPass() {
        this.useLights = LightType.NONE;
    }

    public void releaseReferences() {
        super.releaseReferences();
        this.program = null;
    }

    public ShaderProgram getProgram() {
        return this.program;
    }

    public void setProgram(ShaderProgram var1) {
        this.program = var1;
    }

    public LightType getUseLights() {
        return this.useLights;
    }

    public void setUseLights(LightType var1) {
        this.useLights = var1;
    }

    public boolean getUseFixedFunctionLighting() {
        return this.renderStates.glLightingEnabled;
    }

    public void setUseFixedFunctionLighting(boolean var1) {
        this.renderStates.setGlLightingEnabled(var1);
    }

    public boolean getUseSceneAmbientColor() {
        return this.useSceneAmbientColor;
    }

    public void setUseSceneAmbientColor(boolean var1) {
        this.useSceneAmbientColor = var1;
    }

    public boolean getUseReflectCubemap() {
        return this.useReflectCubemap;
    }

    public void setUseReflectCubemap(boolean var1) {
        this.useReflectCubemap = var1;
    }

    public boolean getUseDiffuseCubemap() {
        return this.useDiffuseCubemap;
    }

    public void setUseDiffuseCubemap(boolean var1) {
        this.useDiffuseCubemap = var1;
    }

    public RenderStates getRenderStates() {
        return this.renderStates;
    }

    public boolean isCompiled() {
        return this.compiled;
    }

    public boolean isValidated() {
        return this.validated;
    }

    public void updateStates(DrawContext var1, RenderStates var2) {
        if (var1.debugDraw()) {
            var1.logBind(this);
        }

        if (this.program != null) {
            this.program.bind(var1);
        }

        this.renderStates.updateGLStates(var1, var2);
        var1.incNumShadersBind();
    }

    public void unbind(DrawContext var1) {
        if (this.program != null) {
            if (var1.debugDraw()) {
                var1.logUnbind(this);
            }

            this.program.unbind(var1);
        }

    }

    public void updateShaderProgramAttribs(DrawContext var1) {
        if (this.program != null) {
            this.program.updateShaderProgramAttribs(var1);
        }

    }

    public void updateMaterialProgramAttribs(DrawContext var1) {
        if (this.program != null) {
            this.program.updateMaterialProgramAttribs(var1);
        }

    }

    public void updatePrimitiveProgramAttribs(DrawContext var1) {
        if (this.program != null) {
            this.program.updatePrimitiveProgramAttribs(var1);
        }

    }

    public RenderAsset cloneAsset() {
        return this;
    }

    public boolean isAlphaTestEnabled() {
        return this.renderStates.isAlphaTestEnabled();
    }

    public void setAlphaTestEnabled(boolean var1) {
        this.renderStates.setAlphaTestEnabled(var1);
    }

    public boolean isBlendEnabled() {
        return this.renderStates.isBlendEnabled();
    }

    public void setBlendEnabled(boolean var1) {
        this.renderStates.setBlendEnabled(var1);
    }

    public boolean isColorArrayEnabled() {
        return this.renderStates.isColorArrayEnabled();
    }

    public void setColorArrayEnabled(boolean var1) {
        this.renderStates.setColorArrayEnabled(var1);
    }

    public boolean isColorMask() {
        return this.renderStates.isColorMask();
    }

    public void setColorMask(boolean var1) {
        this.renderStates.setColorMask(var1);
    }

    public boolean isAlphaMask() {
        return this.renderStates.isColorMask();
    }

    public void setAlphaMask(boolean var1) {
        this.renderStates.setAlphaMask(var1);
    }

    public boolean isCullFaceEnabled() {
        return this.renderStates.isCullFaceEnabled();
    }

    public void setCullFaceEnabled(boolean var1) {
        this.renderStates.setCullFaceEnabled(var1);
    }

    public boolean isDepthMask() {
        return this.renderStates.isDepthMask();
    }

    public void setDepthMask(boolean var1) {
        this.renderStates.setDepthMask(var1);
    }

    public boolean isDepthTestEnabled() {
        return this.renderStates.isDepthTestEnabled();
    }

    public void setDepthTestEnabled(boolean var1) {
        this.renderStates.setDepthTestEnabled(var1);
    }

    public boolean isFogEnabled() {
        return this.renderStates.isFogEnabled();
    }

    public void setFogEnabled(boolean var1) {
        this.renderStates.setFogEnabled(var1);
    }

    public boolean isGlLightingEnabled() {
        return this.renderStates.isGlLightingEnabled();
    }

    public void setGlLightingEnabled(boolean var1) {
        this.renderStates.setGlLightingEnabled(var1);
    }

    public boolean isLineMode() {
        return this.renderStates.isLineMode();
    }

    public void setLineMode(boolean var1) {
        this.renderStates.setLineMode(var1);
    }

    public boolean isNormalArrayEnabled() {
        return this.renderStates.isNormalArrayEnabled();
    }

    public void setNormalArrayEnabled(boolean var1) {
        this.renderStates.setNormalArrayEnabled(var1);
    }

    public boolean isTangentArrayEnabled() {
        return this.renderStates.isTangentArrayEnabled();
    }

    public void setTangentArrayEnabled(boolean var1) {
        this.renderStates.setTangentArrayEnabled(var1);
    }

    public float getAlphaRef() {
        return this.renderStates.getAlphaRef();
    }

    public void setAlphaRef(float var1) {
        this.renderStates.setAlphaRef(var1);
    }

    public float getDepthRangeMax() {
        return this.renderStates.getDepthRangeMax();
    }

    public void setDepthRangeMax(float var1) {
        this.renderStates.setDepthRangeMax(var1);
    }

    public float getDepthRangeMin() {
        return this.renderStates.getDepthRangeMin();
    }

    public void setDepthRangeMin(float var1) {
        this.renderStates.setDepthRangeMin(var1);
    }

    public float getLineWidth() {
        return this.renderStates.getLineWidth();
    }

    public void setLineWidth(float var1) {
        this.renderStates.setLineWidth(var1);
    }

    public DepthFuncType getAlphaFunc() {
        return this.renderStates.getAlphaFunc();
    }

    public void setAlphaFunc(DepthFuncType var1) {
        this.renderStates.setAlphaFunc(var1);
    }

    public CullFace getCullFace() {
        return this.renderStates.getCullFace();
    }

    public void setCullFace(CullFace var1) {
        this.renderStates.setCullFace(var1);
    }

    public DepthFuncType getDepthFunc() {
        return this.renderStates.getDepthFunc();
    }

    public void setDepthFunc(DepthFuncType var1) {
        this.renderStates.setDepthFunc(var1);
    }

    public BlendType getDstBlend() {
        return this.renderStates.getDstBlend();
    }

    public void setDstBlend(BlendType var1) {
        this.renderStates.setDstBlend(var1);
    }

    public BlendType getSrcBlend() {
        return this.renderStates.getSrcBlend();
    }

    public void setSrcBlend(BlendType var1) {
        this.renderStates.setSrcBlend(var1);
    }

    public int getTangentChannel() {
        return this.renderStates.getTangentChannel();
    }

    public void setTangentChannel(int var1) {
        this.renderStates.setTangentChannel(var1);
    }

    public ChannelInfo getChannelTextureSet(int var1) {
        return this.renderStates.getChannel(var1);
    }

    public void setChannelTextureSet(int var1, ChannelInfo var2) {
        this.renderStates.setChannel(var1, var2);
    }

    public int channelsCount() {
        return this.renderStates.getChannels().size();
    }

    public boolean compile(DrawContext var1) {
        if (!this.compiled) {
            this.compiled = true;
            if (this.program != null && !this.program.compile(this, var1)) {
                return false;
            } else {
                this.validated = true;
                return true;
            }
        } else {
            return this.validated;
        }
    }

    public void clearTexChannels() {
        this.renderStates.channels.clear();
        this.compiled = false;
        this.validated = false;
    }
}
