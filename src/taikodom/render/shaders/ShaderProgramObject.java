package taikodom.render.shaders;

import all.LogPrinter;
import com.sun.opengl.util.BufferUtil;
import taikodom.render.DrawContext;
import taikodom.render.loader.RenderAsset;

import javax.media.opengl.GL;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;

/**
 * Обёртка шедера
 */
public class ShaderProgramObject extends RenderAsset {
    private static LogPrinter logger = LogPrinter.K(ShaderProgramObject.class);
    /**
     * локальное имя файла
     */
    protected String name;
    /**
     *
     */
    private int objectId;
    /**
     *
     */
    private int shaderType;
    /**
     * содержимое файла
     */
    private String sourceCode;

    /**
     *
     */
    public ShaderProgramObject() {
        this.objectId = -1;
        this.shaderType = 0;
    }

    /**
     * @param var1 Тип шейдера .vert = 35633  .frog = 35632
     * @param var2 Содержимое файла
     */
    public ShaderProgramObject(int var1, String var2) {
        this.shaderType = var1;
        this.sourceCode = var2;
        this.objectId = -1;
    }

    /**
     * @param var0
     * @param var1
     * @return
     */
    static boolean checkLogInfo(GL var0, int var1) {
        IntBuffer var2 = BufferUtil.newIntBuffer(1);
        var0.glGetObjectParameterivARB(var1, 35716, var2);
        int var3 = var2.get();
        if (var3 <= 1) {
            return true;
        } else {
            ByteBuffer var4 = BufferUtil.newByteBuffer(var3);
            var2.flip();
            var0.glGetInfoLogARB(var1, var3, var2, var4);
            byte[] var5 = new byte[var3];
            var4.get(var5);
            var0.glGetObjectParameterivARB(var1, 35713, var2);
            var3 = var2.get();
            return var3 <= 1;
        }
    }

    public int getShaderType() {
        return this.shaderType;
    }

    public void setShaderType(int var1) {
        this.shaderType = var1;
    }

    /**
     * @return
     */
    public int getObjectId() {
        return this.objectId;
    }

    public String getSourceCode() {
        return this.sourceCode;
    }

    public void setSourceCode(String var1) {
        this.sourceCode = var1;
    }

    /**
     * @param var1
     * @return
     */
    boolean compile(DrawContext var1) {
        if (this.shaderType != 0 && this.sourceCode != null && this.sourceCode.length() != 0) {
            if (var1.debugDraw()) {
                var1.logCompile(this);
            }
            /**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
             * седержит методы для рисования примитивов (линии треугольники)*/
            GL var2 = var1.getGL();
            int var3 = var2.glCreateShaderObjectARB(this.shaderType);
            if (var3 == 0) {
                return false;
            } else {
                var2.glShaderSourceARB(var3, 1, new String[]{this.sourceCode}, (int[]) null, 0);
                var2.glCompileShaderARB(var3);
                if (!checkLogInfo(var2, var3)) {
                    return false;
                } else {
                    this.objectId = var3;
                    return true;
                }
            }
        } else {
            return false;
        }
    }

    public RenderAsset cloneAsset() {
        return this;
    }
}
