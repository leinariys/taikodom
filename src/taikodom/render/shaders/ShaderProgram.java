package taikodom.render.shaders;

import all.LogPrinter;
import com.sun.opengl.util.BufferUtil;
import taikodom.render.DrawContext;
import taikodom.render.gl.GLSupportedCaps;
import taikodom.render.loader.RenderAsset;
import taikodom.render.shaders.parameters.*;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Реализация класса указанного в имени тегов из файлов с расширением .pro
 */
public class ShaderProgram extends RenderAsset {
    private static LogPrinter logger = LogPrinter.K(ShaderProgram.class);
    protected String name;
    private List objects = new ArrayList();
    private boolean hasError;
    private int programId;
    private List shaderAutoParams = new ArrayList();
    private ArrayList materialAutoParams = new ArrayList();
    private ArrayList primitiveAutoParams = new ArrayList();
    private List manualParams = new ArrayList();
    private boolean useDiffuseCubemap;
    private boolean useReflectCubemap;
    private int tangentChannel;
    private boolean useTangentArray;
    private boolean useNormalArray;
    private boolean compiled;

    static boolean checkLogInfo(GL var0, int var1) {
        IntBuffer var2 = BufferUtil.newIntBuffer(1);
        var0.glGetObjectParameterivARB(var1, 35714, var2);
        int var3 = var2.get();
        if (var3 <= 1) {
            return true;
        } else {
            ByteBuffer var4 = BufferUtil.newByteBuffer(var3);
            var2.flip();
            var0.glGetInfoLogARB(var1, var3, var2, var4);
            byte[] var5 = new byte[var3];
            var4.get(var5);
            return false;
        }
    }

    public void addProgramObject(ShaderProgramObject var1) {
        this.objects.add(var1);
    }

    public boolean compile(ShaderPass var1, DrawContext var2) {
        if (!GLSupportedCaps.isGlsl()) {
            this.compiled = true;
            this.hasError = true;
        }

        if (this.compiled) {
            if (var1 != null) {
                var1.setUseDiffuseCubemap(this.useDiffuseCubemap);
                var1.setUseReflectCubemap(this.useReflectCubemap);
                var1.setNormalArrayEnabled(this.useNormalArray);
                var1.setTangentArrayEnabled(this.useTangentArray);
                var1.setTangentChannel(this.tangentChannel);
            }

            return !this.hasError;
        } else {
            if (var2.debugDraw()) {
                var2.logCompile(this);
            }

            this.compiled = true;
            this.hasError = true;
            if (this.objects.size() == 0) {
                return false;
            } else {
                /**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
                 * седержит методы для рисования примитивов (линии треугольники)*/
                GL var3 = var2.getGL();
                this.programId = var3.glCreateProgramObjectARB();
                if (this.programId == 0) {
                    return false;
                } else {
                    Iterator var5 = this.objects.iterator();

                    while (var5.hasNext()) {
                        ShaderProgramObject var4 = (ShaderProgramObject) var5.next();
                        if (!var4.compile(var2)) {
                            return false;
                        }

                        var3.glAttachObjectARB(this.programId, var4.getObjectId());
                    }

                    var3.glLinkProgram(this.programId);
                    var3.glValidateProgramARB(this.programId);
                    if (checkLogInfo(var3, this.programId)) {
                        this.findParameters(var1, var2);
                        this.compiled = true;
                        this.hasError = false;
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        }
    }

    public void findParameters(ShaderPass var1, DrawContext var2) {
        if (var1 != null) {
            /**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
             * седержит методы для рисования примитивов (линии треугольники)*/
            GL var3 = var2.getGL();
            int[] var4 = new int[3];
            byte[] var5 = new byte[520];
            var3.glGetObjectParameterivARB(this.programId, 35718, var4, 0);
            int var6 = var4[0];

            int var7;
            int var8;
            for (var7 = 0; var7 < var6; ++var7) {
                var3.glGetActiveUniform(this.programId, var7, var5.length, var4, 0, var4, 1, var4, 2, var5, 0);
                var8 = var4[0];
                int var9 = var4[2];

                String var10;
                try {
                    var10 = new String(var5, 0, var8, "utf-8");
                } catch (UnsupportedEncodingException var14) {
                    var14.printStackTrace();
                    continue;
                }

                final GlobalShaderParameter var11 = var2.sceneConfig.getGlobalShaderParameter(var10);
                if (var11 != null) {
                    ShaderParameter var12 = new ShaderParameter() {
                        public boolean bind(DrawContext var1) {
                            if (var1.debugDraw()) {
                                var1.logBind(this);
                            }

                            var11.bind(this.locationId, var1);
                            return true;
                        }

                        public RenderAsset cloneAsset() {
                            return this;
                        }

                        protected void doAssign(ShaderParameter var1) {
                        }
                    };
                    var12.setLocationId(var3.glGetUniformLocation(this.programId, var10));
                    var12.setName(var10);
                    var12.setParameterName(var10);
                    if (var11.getLayer() == 0) {
                        this.shaderAutoParams.add(var12);
                    } else if (var11.getLayer() == 1) {
                        this.materialAutoParams.add(var12);
                    } else if (var11.getLayer() == 2) {
                        this.primitiveAutoParams.add(var12);
                    }

                    if ("reflectCubemap".equals(var10)) {
                        this.useReflectCubemap = true;
                        var1.setUseReflectCubemap(true);
                    } else if ("diffuseCubemap".equals(var10)) {
                        this.useDiffuseCubemap = true;
                        var1.setUseDiffuseCubemap(true);
                    }
                } else if (!var10.startsWith("getGL")) {
                    Object var18;
                    switch (var9) {
                        case 5124:
                            var18 = new IntParameter();
                            break;
                        case 5126:
                            var18 = new FloatParameter();
                            break;
                        case 35664:
                            var18 = new Vec2fParameter();
                            break;
                        case 35665:
                            var18 = new Vec3fParameter();
                            break;
                        case 35666:
                            var18 = new ColorParameter();
                            break;
                        case 35676:
                            var18 = new Matrix4fParameter();
                            break;
                        default:
                            var18 = null;
                    }

                    if (var18 != null) {
                        ((ShaderParameter) var18).setLocationId(var3.glGetUniformLocation(this.programId, var10));
                        ((ShaderParameter) var18).setName(var10);
                        ((ShaderParameter) var18).setParameterName(var10);
                        this.manualParams.add(var18);
                    }
                }
            }

            var3.glGetObjectParameterivARB(this.programId, 35721, var4, 0);
            var6 = var4[0];

            for (var7 = 0; var7 < var6; ++var7) {
                var3.glGetActiveAttrib(this.programId, var7, var5.length, var4, 0, var4, 1, var4, 2, var5, 0);
                var8 = var4[0];

                String var15;
                try {
                    var15 = new String(var5, 0, var8, "utf-8");
                } catch (UnsupportedEncodingException var13) {
                    var13.printStackTrace();
                    continue;
                }

                final GlobalShaderParameter var16 = var2.sceneConfig.getGlobalShaderParameter(var15);
                if (var16 != null) {
                    ShaderParameter var17 = new ShaderParameter() {
                        public boolean bind(DrawContext var1) {
                            if (var1.debugDraw()) {
                                var1.logBind(this);
                            }

                            var16.bind(this.locationId, var1);
                            return true;
                        }

                        public RenderAsset cloneAsset() {
                            return this;
                        }

                        protected void doAssign(ShaderParameter var1) {
                        }
                    };
                    var17.setLocationId(var3.glGetAttribLocation(this.programId, var15));
                    var17.setName(var15);
                    var17.setParameterName(var15);
                    if (var16.getLayer() == 0) {
                        this.shaderAutoParams.add(var17);
                    } else if (var16.getLayer() == 1) {
                        this.materialAutoParams.add(var17);
                    } else if (var16.getLayer() == 2) {
                        this.primitiveAutoParams.add(var17);
                    }
                }

                if ("tangent".equals(var15)) {
                    this.tangentChannel = var3.glGetAttribLocation(this.programId, var15);
                    this.useTangentArray = true;
                    if (var1 != null) {
                        var1.getRenderStates().setTangentArrayEnabled(true);
                        var1.getRenderStates().setTangentChannel(this.tangentChannel);
                    }
                } else if ("gl_Normal".equals(var15)) {
                    this.useNormalArray = true;
                    if (var1 != null) {
                        var1.getRenderStates().setNormalArrayEnabled(true);
                    }
                }
            }

            this.unbind(var2);
        }
    }

    public void unbind(DrawContext var1) {
        var1.getGL().glUseProgramObjectARB(0);
    }

    public void bind(DrawContext var1) {
        this.compile(var1.currentPass, var1);
        if (var1.debugDraw()) {
            var1.logBind(this);
        }

        if (!this.hasError) {
            var1.getGL().glUseProgramObjectARB(this.programId);
        }

    }

    public RenderAsset cloneAsset() {
        return this;
    }

    /**
     * @deprecated
     */
    @Deprecated
    public ShaderProgramObject getShaderProgramObject(int var1) {
        return (ShaderProgramObject) this.objects.get(var1);
    }

    /**
     * @deprecated
     */
    @Deprecated
    public void setShaderProgramObject(int var1, ShaderProgramObject var2) {
        this.objects.add(var2);
    }

    public int getManualParamsCount() {
        return this.manualParams.size();
    }

    public ShaderParameter getManualParameter(int var1) {
        return (ShaderParameter) this.manualParams.get(var1);
    }

    public void updateShaderProgramAttribs(DrawContext var1) {
        for (int var2 = 0; var2 < this.shaderAutoParams.size(); ++var2) {
            ((ShaderParameter) this.shaderAutoParams.get(var2)).bind(var1);
        }

    }

    public void updateMaterialProgramAttribs(DrawContext var1) {
        for (int var2 = 0; var2 < this.materialAutoParams.size(); ++var2) {
            ((ShaderParameter) this.materialAutoParams.get(var2)).bind(var1);
        }

    }

    public void updatePrimitiveProgramAttribs(DrawContext var1) {
        for (int var2 = 0; var2 < this.primitiveAutoParams.size(); ++var2) {
            ((ShaderParameter) this.primitiveAutoParams.get(var2)).bind(var1);
        }

    }

    public int shaderProgramObjectCount() {
        return this.objects.size();
    }

    public void releaseReferences() {
        super.releaseReferences();
    }

    public void clear() {
        this.objects.clear();
        GL var1 = GLU.getCurrentGL();
        var1.glDeleteObjectARB(this.programId);
        this.programId = 0;
        this.useTangentArray = false;
        this.tangentChannel = 0;
        this.compiled = false;
        this.hasError = false;
    }
}
