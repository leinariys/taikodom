package taikodom.render.shaders.parameters;

import com.hoplon.geometry.Color;
import taikodom.render.DrawContext;
import taikodom.render.loader.RenderAsset;

/**
 * Реализация класса указанного в имени тегов из файлов с расширением .pro
 */
public class ColorParameter extends ShaderParameter {
    private final Color value = new Color(0.0F, 0.0F, 0.0F, 0.0F);

    public ColorParameter() {
    }

    public ColorParameter(ColorParameter var1) {
        super(var1);
        this.value.set(var1.value);
    }

    public RenderAsset cloneAsset() {
        return new ColorParameter(this);
    }

    public Color getValue() {
        return this.value;
    }

    public void setValue(Color var1) {
        this.value.set(var1);
    }

    public void setValue(float var1, float var2, float var3, float var4) {
        this.value.set(var1, var2, var3, var4);
    }

    public boolean bind(DrawContext var1) {
        if (var1.debugDraw()) {
            var1.logBind(this);
        }

        if (this.locationId == -1) {
            return false;
        } else {
            var1.getGL().glUniform4f(this.locationId, this.value.x, this.value.y, this.value.z, this.value.w);
            return true;
        }
    }

    protected void doAssign(ShaderParameter var1) {
        this.setValue(((ColorParameter) var1).getValue());
    }
}
