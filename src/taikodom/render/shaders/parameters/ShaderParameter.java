package taikodom.render.shaders.parameters;

import taikodom.render.DrawContext;
import taikodom.render.NotExported;
import taikodom.render.loader.RenderAsset;

public abstract class ShaderParameter extends RenderAsset {
    protected int locationId = -1;
    private String parameterName = "unnamed";

    public ShaderParameter() {
    }

    protected ShaderParameter(ShaderParameter var1) {
        super(var1);
        this.locationId = var1.locationId;
        this.name = var1.name;
        this.parameterName = var1.parameterName;
    }

    public abstract boolean bind(DrawContext var1);

    public String getParameterName() {
        return this.parameterName;
    }

    public void setParameterName(String var1) {
        this.parameterName = var1;
    }

    public void assignValue(ShaderParameter var1) {
        if (var1.locationId != -1) {
            this.locationId = var1.locationId;
        }

        this.doAssign(var1);
    }

    protected abstract void doAssign(ShaderParameter var1);

    public String toString() {
        return this.locationId + ":" + this.parameterName;
    }

    @NotExported
    public int getLocationId() {
        return this.locationId;
    }

    @NotExported
    public void setLocationId(int var1) {
        this.locationId = var1;
    }
}
