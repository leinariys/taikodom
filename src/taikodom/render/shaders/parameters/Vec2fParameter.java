package taikodom.render.shaders.parameters;

import all.aQKa;
import taikodom.render.DrawContext;
import taikodom.render.loader.RenderAsset;

/**
 * Реализация класса указанного в имени тегов из файлов с расширением .pro
 */
public class Vec2fParameter extends ShaderParameter {
    private final aQKa value = new aQKa();

    public Vec2fParameter() {
    }

    public Vec2fParameter(Vec2fParameter var1) {
        super(var1);
        this.value.set(var1.value);
    }

    public RenderAsset cloneAsset() {
        return new Vec2fParameter(this);
    }

    public aQKa getValue() {
        return this.value;
    }

    public void setValue(aQKa var1) {
        this.value.set(var1);
    }

    public void setValue(float var1, float var2) {
        this.value.set(var1, var2);
    }

    public boolean bind(DrawContext var1) {
        if (var1.debugDraw()) {
            var1.logBind(this);
        }

        if (this.locationId == -1) {
            return false;
        } else {
            var1.getGL().glUniform2f(this.locationId, this.value.x, this.value.y);
            return true;
        }
    }

    protected void doAssign(ShaderParameter var1) {
        this.setValue(((Vec2fParameter) var1).getValue());
    }
}
