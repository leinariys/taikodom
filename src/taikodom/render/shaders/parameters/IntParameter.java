package taikodom.render.shaders.parameters;

import taikodom.render.DrawContext;
import taikodom.render.loader.RenderAsset;

/**
 * Реализация класса указанного в имени тегов из файлов с расширением .pro
 */
public class IntParameter extends ShaderParameter {
    protected int value;

    public IntParameter() {
    }

    public IntParameter(IntParameter var1) {
        super(var1);
        this.value = var1.value;
    }

    public RenderAsset cloneAsset() {
        return new IntParameter(this);
    }

    public int getValue() {
        return this.value;
    }

    public void setValue(int var1) {
        this.value = var1;
    }

    public boolean bind(DrawContext var1) {
        if (var1.debugDraw()) {
            var1.logBind(this);
        }

        if (this.locationId == -1) {
            return false;
        } else {
            var1.getGL().glUniform1i(this.locationId, this.value);
            return true;
        }
    }

    protected void doAssign(ShaderParameter var1) {
        this.setValue(((IntParameter) var1).getValue());
    }
}
