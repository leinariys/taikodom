package taikodom.render.shaders.parameters;

import com.hoplon.geometry.Vec3f;
import taikodom.render.DrawContext;
import taikodom.render.loader.RenderAsset;

/**
 * Реализация класса указанного в имени тегов из файлов с расширением .pro
 */
public class Vec3fParameter extends ShaderParameter {
    private final Vec3f value = new Vec3f();

    public Vec3fParameter() {
    }

    public Vec3fParameter(Vec3fParameter var1) {
        super(var1);
        this.value.set(var1.value);
    }

    public RenderAsset cloneAsset() {
        return new Vec3fParameter(this);
    }

    public Vec3f getValue() {
        return this.value;
    }

    public void setValue(Vec3f var1) {
        this.value.set(var1);
    }

    public void setValue(float var1, float var2, float var3) {
        this.value.set(var1, var2, var3);
    }

    public boolean bind(DrawContext var1) {
        if (var1.debugDraw()) {
            var1.logBind(this);
        }

        if (this.locationId == -1) {
            return false;
        } else {
            try {
                var1.getGL().glUniform3f(this.locationId, this.value.x, this.value.y, this.value.z);
                return true;
            } catch (Exception var3) {
                throw new RuntimeException("Error trying to_q bind parameter: " + this.getName() + " at location: " + this.locationId, var3);
            }
        }
    }

    protected void doAssign(ShaderParameter var1) {
        this.setValue(((Vec3fParameter) var1).getValue());
    }
}
