package taikodom.render.shaders.parameters;

import taikodom.render.DrawContext;

/**
 *
 */
public abstract class GlobalShaderParameter {
    private int layer;
    private String name;

    public GlobalShaderParameter(int var1, String var2) {
        this.layer = var1;
        this.setName(var2);
    }

    public abstract void bind(int var1, DrawContext var2);

    public int getLayer() {
        return this.layer;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String var1) {
        this.name = var1;
    }
}
