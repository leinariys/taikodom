package taikodom.render.shaders;

import all.LogPrinter;
import taikodom.render.DrawContext;
import taikodom.render.loader.RenderAsset;

import java.util.ArrayList;
import java.util.List;

/**
 * Реализация класса указанного в имени тегов из файлов с расширением .pro
 */
public class Shader extends RenderAsset {
    private static LogPrinter logger = LogPrinter.K(Shader.class);
    protected List shaderPasses = new ArrayList();
    protected boolean compiled;
    protected boolean validated;
    protected int quality = -1;
    protected Shader fallback;
    protected int priority;

    public boolean isCompiled() {
        return this.compiled;
    }

    public void setCompiled(boolean var1) {
        this.compiled = var1;
    }

    public boolean isValidated() {
        return this.validated;
    }

    public void setValidated(boolean var1) {
        this.validated = var1;
    }

    public Shader getFallback() {
        return this.fallback;
    }

    public void setFallback(Shader var1) {
        this.fallback = var1;
    }

    public void addPass(ShaderPass var1) {
        this.shaderPasses.add(var1);
    }

    public Shader getBestShader(DrawContext var1) {
        if (!this.compiled) {
            this.compile(var1);
        }

        Shader var2 = this;
        int var3 = 0;

        do {
            if ((var2.quality == -1 || var2.quality >= var1.getMaxShaderQuality()) && var2.validated) {
                return var2;
            }

            Shader var4 = var2;
            var2 = var2.getFallback();
            if (var2 == null) {
                return var4;
            }

            ++var3;
        } while (var3 != 15);

        throw new IllegalStateException("Infinite fallback loop in " + var2.getName() + " shader.");
    }

    public List getShaderPasses() {
        return this.shaderPasses;
    }

    public int getQuality() {
        return this.quality;
    }

    public void setQuality(int var1) {
        this.quality = var1;
    }

    public void compile(DrawContext var1) {
        if (var1.debugDraw()) {
            var1.logCompile(this);
        }

        if (!this.compiled) {
            this.compiled = true;
            this.validated = true;

            for (int var2 = 0; var2 < this.shaderPasses.size(); ++var2) {
                if (!((ShaderPass) this.shaderPasses.get(var2)).compile(var1)) {
                    this.validated = false;
                }
            }

            if (this.fallback != null) {
                this.fallback.compile(var1);
            }
        }

    }

    public ShaderPass getPass(int var1) {
        return (ShaderPass) this.shaderPasses.get(var1);
    }

    public void setPass(int var1, ShaderPass var2) {
        if (this.shaderPasses.size() != var1 && var1 != -1) {
            this.shaderPasses.set(var1, var2);
        } else {
            this.shaderPasses.add(var2);
        }

    }

    public RenderAsset cloneAsset() {
        return this;
    }

    public int passCount() {
        return this.shaderPasses.size();
    }

    public void clearPasses() {
        this.shaderPasses.clear();
    }

    public void releaseReferences() {
        super.releaseReferences();
        this.shaderPasses.clear();
        this.fallback = null;
    }

    public int getPriority() {
        return this.priority;
    }

    public void setPriority(int var1) {
        this.priority = var1;
    }
}
