package taikodom.render.gl;

import javax.media.opengl.GL;
import java.nio.*;

public class GLWrapperBase implements GL {
    protected GL gl;

    public GLWrapperBase(GL var1) {
        this.gl = var1;
    }

    public Object getPlatformGLExtensions() {
        return this.gl.getPlatformGLExtensions();
    }

    public void glAccum(int var1, float var2) {
        this.gl.glAccum(var1, var2);
    }

    public void glActiveStencilFaceEXT(int var1) {
        this.gl.glActiveStencilFaceEXT(var1);
    }

    public void glActiveTexture(int var1) {
        this.gl.glActiveTexture(var1);
    }

    public void glActiveVaryingNV(int var1, byte[] var2, int var3) {
        this.gl.glActiveVaryingNV(var1, var2, var3);
    }

    public void glActiveVaryingNV(int var1, ByteBuffer var2) {
        this.gl.glActiveVaryingNV(var1, var2);
    }

    public ByteBuffer glAllocateMemoryNV(int var1, float var2, float var3, float var4) {
        return this.gl.glAllocateMemoryNV(var1, var2, var3, var4);
    }

    public void glAlphaFragmentOp1ATI(int var1, int var2, int var3, int var4, int var5, int var6) {
        this.gl.glAlphaFragmentOp1ATI(var1, var2, var3, var4, var5, var6);
    }

    public void glAlphaFragmentOp2ATI(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9) {
        this.gl.glAlphaFragmentOp2ATI(var1, var2, var3, var4, var5, var6, var7, var8, var9);
    }

    public void glAlphaFragmentOp3ATI(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10, int var11, int var12) {
        this.gl.glAlphaFragmentOp3ATI(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11, var12);
    }

    public void glAlphaFunc(int var1, float var2) {
        this.gl.glAlphaFunc(var1, var2);
    }

    public void glApplyTextureEXT(int var1) {
        this.gl.glApplyTextureEXT(var1);
    }

    public boolean glAreProgramsResidentNV(int var1, int[] var2, int var3, byte[] var4, int var5) {
        return this.gl.glAreProgramsResidentNV(var1, var2, var3, var4, var5);
    }

    public boolean glAreProgramsResidentNV(int var1, IntBuffer var2, ByteBuffer var3) {
        return this.gl.glAreProgramsResidentNV(var1, var2, var3);
    }

    public boolean glAreTexturesResident(int var1, int[] var2, int var3, byte[] var4, int var5) {
        return this.gl.glAreTexturesResident(var1, var2, var3, var4, var5);
    }

    public boolean glAreTexturesResident(int var1, IntBuffer var2, ByteBuffer var3) {
        return this.gl.glAreTexturesResident(var1, var2, var3);
    }

    public void glArrayElement(int var1) {
        this.gl.glArrayElement(var1);
    }

    public void glArrayObjectATI(int var1, int var2, int var3, int var4, int var5, int var6) {
        this.gl.glArrayObjectATI(var1, var2, var3, var4, var5, var6);
    }

    public void glAsyncMarkerSGIX(int var1) {
        this.gl.glAsyncMarkerSGIX(var1);
    }

    public void glAttachObjectARB(int var1, int var2) {
        this.gl.glAttachObjectARB(var1, var2);
    }

    public void glAttachShader(int var1, int var2) {
        this.gl.glAttachShader(var1, var2);
    }

    /**
     * Этот метод запускает процесс рисования
     */
    public void glBegin(int var1) {
        this.gl.glBegin(var1);
    }

    public void glBeginFragmentShaderATI() {
        this.gl.glBeginFragmentShaderATI();
    }

    public void glBeginOcclusionQueryNV(int var1) {
        this.gl.glBeginOcclusionQueryNV(var1);
    }

    public void glBeginQuery(int var1, int var2) {
        this.gl.glBeginQuery(var1, var2);
    }

    public void glBeginQueryARB(int var1, int var2) {
        this.gl.glBeginQueryARB(var1, var2);
    }

    public void glBeginTransformFeedbackNV(int var1) {
        this.gl.glBeginTransformFeedbackNV(var1);
    }

    public void glBeginVertexShaderEXT() {
        this.gl.glBeginVertexShaderEXT();
    }

    public void glBindAttribLocation(int var1, int var2, String var3) {
        this.gl.glBindAttribLocation(var1, var2, var3);
    }

    public void glBindAttribLocationARB(int var1, int var2, String var3) {
        this.gl.glBindAttribLocationARB(var1, var2, var3);
    }

    public void glBindBuffer(int var1, int var2) {
        this.gl.glBindBuffer(var1, var2);
    }

    public void glBindBufferARB(int var1, int var2) {
        this.gl.glBindBufferARB(var1, var2);
    }

    public void glBindBufferBaseNV(int var1, int var2, int var3) {
        this.gl.glBindBufferBaseNV(var1, var2, var3);
    }

    public void glBindBufferOffsetNV(int var1, int var2, int var3, int var4) {
        this.gl.glBindBufferOffsetNV(var1, var2, var3, var4);
    }

    public void glBindBufferRangeNV(int var1, int var2, int var3, int var4, int var5) {
        this.gl.glBindBufferRangeNV(var1, var2, var3, var4, var5);
    }

    public void glBindFragDataLocationEXT(int var1, int var2, ByteBuffer var3) {
        this.gl.glBindFragDataLocationEXT(var1, var2, var3);
    }

    public void glBindFragDataLocationEXT(int var1, int var2, byte[] var3, int var4) {
        this.gl.glBindFragDataLocationEXT(var1, var2, var3, var4);
    }

    public void glBindFragmentShaderATI(int var1) {
        this.gl.glBindFragmentShaderATI(var1);
    }

    public void glBindFramebufferEXT(int var1, int var2) {
        this.gl.glBindFramebufferEXT(var1, var2);
    }

    public int glBindLightParameterEXT(int var1, int var2) {
        return this.gl.glBindLightParameterEXT(var1, var2);
    }

    public int glBindMaterialParameterEXT(int var1, int var2) {
        return this.gl.glBindMaterialParameterEXT(var1, var2);
    }

    public int glBindParameterEXT(int var1) {
        return this.gl.glBindParameterEXT(var1);
    }

    public void glBindProgramARB(int var1, int var2) {
        this.gl.glBindProgramARB(var1, var2);
    }

    public void glBindProgramNV(int var1, int var2) {
        this.gl.glBindProgramNV(var1, var2);
    }

    public void glBindRenderbufferEXT(int var1, int var2) {
        this.gl.glBindRenderbufferEXT(var1, var2);
    }

    public int glBindTexGenParameterEXT(int var1, int var2, int var3) {
        return this.gl.glBindTexGenParameterEXT(var1, var2, var3);
    }

    public void glBindTexture(int var1, int var2) {
        this.gl.glBindTexture(var1, var2);
    }

    public int glBindTextureUnitParameterEXT(int var1, int var2) {
        return this.gl.glBindTextureUnitParameterEXT(var1, var2);
    }

    public void glBindVertexArrayAPPLE(int var1) {
        this.gl.glBindVertexArrayAPPLE(var1);
    }

    public void glBindVertexShaderEXT(int var1) {
        this.gl.glBindVertexShaderEXT(var1);
    }

    public void glBitmap(int var1, int var2, float var3, float var4, float var5, float var6, long var7) {
        this.gl.glBitmap(var1, var2, var3, var4, var5, var6, var7);
    }

    public void glBitmap(int var1, int var2, float var3, float var4, float var5, float var6, ByteBuffer var7) {
        this.gl.glBitmap(var1, var2, var3, var4, var5, var6, var7);
    }

    public void glBitmap(int var1, int var2, float var3, float var4, float var5, float var6, byte[] var7, int var8) {
        this.gl.glBitmap(var1, var2, var3, var4, var5, var6, var7, var8);
    }

    public void glBlendColor(float var1, float var2, float var3, float var4) {
        this.gl.glBlendColor(var1, var2, var3, var4);
    }

    public void glBlendEquation(int var1) {
        this.gl.glBlendEquation(var1);
    }

    public void glBlendEquationSeparate(int var1, int var2) {
        this.gl.glBlendEquationSeparate(var1, var2);
    }

    public void glBlendEquationSeparateEXT(int var1, int var2) {
        this.gl.glBlendEquationSeparateEXT(var1, var2);
    }

    public void glBlendFunc(int var1, int var2) {
        this.gl.glBlendFunc(var1, var2);
    }

    public void glBlendFuncSeparate(int var1, int var2, int var3, int var4) {
        this.gl.glBlendFuncSeparate(var1, var2, var3, var4);
    }

    public void glBlendFuncSeparateEXT(int var1, int var2, int var3, int var4) {
        this.gl.glBlendFuncSeparateEXT(var1, var2, var3, var4);
    }

    public void glBlendFuncSeparateINGR(int var1, int var2, int var3, int var4) {
        this.gl.glBlendFuncSeparateINGR(var1, var2, var3, var4);
    }

    public void glBlitFramebufferEXT(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10) {
        this.gl.glBlitFramebufferEXT(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10);
    }

    public void glBufferData(int var1, int var2, Buffer var3, int var4) {
        this.gl.glBufferData(var1, var2, var3, var4);
    }

    public void glBufferDataARB(int var1, int var2, Buffer var3, int var4) {
        this.gl.glBufferDataARB(var1, var2, var3, var4);
    }

    public void glBufferParameteriAPPLE(int var1, int var2, int var3) {
        this.gl.glBufferParameteriAPPLE(var1, var2, var3);
    }

    public int glBufferRegionEnabled() {
        return this.gl.glBufferRegionEnabled();
    }

    public void glBufferSubData(int var1, int var2, int var3, Buffer var4) {
        this.gl.glBufferSubData(var1, var2, var3, var4);
    }

    public void glBufferSubDataARB(int var1, int var2, int var3, Buffer var4) {
        this.gl.glBufferSubDataARB(var1, var2, var3, var4);
    }

    public void glCallList(int var1) {
        this.gl.glCallList(var1);
    }

    public void glCallLists(int var1, int var2, Buffer var3) {
        this.gl.glCallLists(var1, var2, var3);
    }

    public int glCheckFramebufferStatusEXT(int var1) {
        return this.gl.glCheckFramebufferStatusEXT(var1);
    }

    public void glClampColorARB(int var1, int var2) {
        this.gl.glClampColorARB(var1, var2);
    }

    public void glClear(int var1) {
        this.gl.glClear(var1);
    }

    public void glClearAccum(float var1, float var2, float var3, float var4) {
        this.gl.glClearAccum(var1, var2, var3, var4);
    }

    public void glClearColor(float var1, float var2, float var3, float var4) {
        this.gl.glClearColor(var1, var2, var3, var4);
    }

    public void glClearColorIiEXT(int var1, int var2, int var3, int var4) {
        this.gl.glClearColorIiEXT(var1, var2, var3, var4);
    }

    public void glClearColorIuiEXT(int var1, int var2, int var3, int var4) {
        this.gl.glClearColorIuiEXT(var1, var2, var3, var4);
    }

    public void glClearDepth(double var1) {
        this.gl.glClearDepth(var1);
    }

    public void glClearDepthdNV(double var1) {
        this.gl.glClearDepthdNV(var1);
    }

    public void glClearIndex(float var1) {
        this.gl.glClearIndex(var1);
    }

    public void glClearStencil(int var1) {
        this.gl.glClearStencil(var1);
    }

    public void glClientActiveTexture(int var1) {
        this.gl.glClientActiveTexture(var1);
    }

    public void glClientActiveVertexStreamATI(int var1) {
        this.gl.glClientActiveVertexStreamATI(var1);
    }

    public void glClipPlane(int var1, double[] var2, int var3) {
        this.gl.glClipPlane(var1, var2, var3);
    }

    public void glClipPlane(int var1, DoubleBuffer var2) {
        this.gl.glClipPlane(var1, var2);
    }

    public void glColor3b(byte var1, byte var2, byte var3) {
        this.gl.glColor3b(var1, var2, var3);
    }

    public void glColor3bv(ByteBuffer var1) {
        this.gl.glColor3bv(var1);
    }

    public void glColor3bv(byte[] var1, int var2) {
        this.gl.glColor3bv(var1, var2);
    }

    public void glColor3d(double var1, double var3, double var5) {
        this.gl.glColor3d(var1, var3, var5);
    }

    public void glColor3dv(DoubleBuffer var1) {
        this.gl.glColor3dv(var1);
    }

    public void glColor3dv(double[] var1, int var2) {
        this.gl.glColor3dv(var1, var2);
    }

    public void glColor3f(float var1, float var2, float var3) {
        this.gl.glColor3f(var1, var2, var3);
    }

    public void glColor3fVertex3fSUN(float var1, float var2, float var3, float var4, float var5, float var6) {
        this.gl.glColor3fVertex3fSUN(var1, var2, var3, var4, var5, var6);
    }

    public void glColor3fVertex3fvSUN(float[] var1, int var2, float[] var3, int var4) {
        this.gl.glColor3fVertex3fvSUN(var1, var2, var3, var4);
    }

    public void glColor3fVertex3fvSUN(FloatBuffer var1, FloatBuffer var2) {
        this.gl.glColor3fVertex3fvSUN(var1, var2);
    }

    public void glColor3fv(float[] var1, int var2) {
        this.gl.glColor3fv(var1, var2);
    }

    public void glColor3fv(FloatBuffer var1) {
        this.gl.glColor3fv(var1);
    }

    public void glColor3hNV(short var1, short var2, short var3) {
        this.gl.glColor3hNV(var1, var2, var3);
    }

    public void glColor3hvNV(short[] var1, int var2) {
        this.gl.glColor3hvNV(var1, var2);
    }

    public void glColor3hvNV(ShortBuffer var1) {
        this.gl.glColor3hvNV(var1);
    }

    public void glColor3i(int var1, int var2, int var3) {
        this.gl.glColor3i(var1, var2, var3);
    }

    public void glColor3iv(int[] var1, int var2) {
        this.gl.glColor3iv(var1, var2);
    }

    public void glColor3iv(IntBuffer var1) {
        this.gl.glColor3iv(var1);
    }

    public void glColor3s(short var1, short var2, short var3) {
        this.gl.glColor3s(var1, var2, var3);
    }

    public void glColor3sv(ShortBuffer var1) {
        this.gl.glColor3sv(var1);
    }

    public void glColor3sv(short[] var1, int var2) {
        this.gl.glColor3sv(var1, var2);
    }

    public void glColor3ub(byte var1, byte var2, byte var3) {
        this.gl.glColor3ub(var1, var2, var3);
    }

    public void glColor3ubv(ByteBuffer var1) {
        this.gl.glColor3ubv(var1);
    }

    public void glColor3ubv(byte[] var1, int var2) {
        this.gl.glColor3ubv(var1, var2);
    }

    public void glColor3ui(int var1, int var2, int var3) {
        this.gl.glColor3ui(var1, var2, var3);
    }

    public void glColor3uiv(int[] var1, int var2) {
        this.gl.glColor3uiv(var1, var2);
    }

    public void glColor3uiv(IntBuffer var1) {
        this.gl.glColor3uiv(var1);
    }

    public void glColor3us(short var1, short var2, short var3) {
        this.gl.glColor3us(var1, var2, var3);
    }

    public void glColor3usv(ShortBuffer var1) {
        this.gl.glColor3usv(var1);
    }

    public void glColor3usv(short[] var1, int var2) {
        this.gl.glColor3usv(var1, var2);
    }

    public void glColor4b(byte var1, byte var2, byte var3, byte var4) {
        this.gl.glColor4b(var1, var2, var3, var4);
    }

    public void glColor4bv(ByteBuffer var1) {
        this.gl.glColor4bv(var1);
    }

    public void glColor4bv(byte[] var1, int var2) {
        this.gl.glColor4bv(var1, var2);
    }

    public void glColor4d(double var1, double var3, double var5, double var7) {
        this.gl.glColor4d(var1, var3, var5, var7);
    }

    public void glColor4dv(DoubleBuffer var1) {
        this.gl.glColor4dv(var1);
    }

    public void glColor4dv(double[] var1, int var2) {
        this.gl.glColor4dv(var1, var2);
    }

    public void glColor4f(float var1, float var2, float var3, float var4) {
        this.gl.glColor4f(var1, var2, var3, var4);
    }

    public void glColor4fNormal3fVertex3fSUN(float var1, float var2, float var3, float var4, float var5, float var6, float var7, float var8, float var9, float var10) {
        this.gl.glColor4fNormal3fVertex3fSUN(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10);
    }

    public void glColor4fNormal3fVertex3fvSUN(float[] var1, int var2, float[] var3, int var4, float[] var5, int var6) {
        this.gl.glColor4fNormal3fVertex3fvSUN(var1, var2, var3, var4, var5, var6);
    }

    public void glColor4fNormal3fVertex3fvSUN(FloatBuffer var1, FloatBuffer var2, FloatBuffer var3) {
        this.gl.glColor4fNormal3fVertex3fvSUN(var1, var2, var3);
    }

    public void glColor4fv(float[] var1, int var2) {
        this.gl.glColor4fv(var1, var2);
    }

    public void glColor4fv(FloatBuffer var1) {
        this.gl.glColor4fv(var1);
    }

    public void glColor4hNV(short var1, short var2, short var3, short var4) {
        this.gl.glColor4hNV(var1, var2, var3, var4);
    }

    public void glColor4hvNV(ShortBuffer var1) {
        this.gl.glColor4hvNV(var1);
    }

    public void glColor4hvNV(short[] var1, int var2) {
        this.gl.glColor4hvNV(var1, var2);
    }

    public void glColor4i(int var1, int var2, int var3, int var4) {
        this.gl.glColor4i(var1, var2, var3, var4);
    }

    public void glColor4iv(IntBuffer var1) {
        this.gl.glColor4iv(var1);
    }

    public void glColor4iv(int[] var1, int var2) {
        this.gl.glColor4iv(var1, var2);
    }

    public void glColor4s(short var1, short var2, short var3, short var4) {
        this.gl.glColor4s(var1, var2, var3, var4);
    }

    public void glColor4sv(ShortBuffer var1) {
        this.gl.glColor4sv(var1);
    }

    public void glColor4sv(short[] var1, int var2) {
        this.gl.glColor4sv(var1, var2);
    }

    public void glColor4ub(byte var1, byte var2, byte var3, byte var4) {
        this.gl.glColor4ub(var1, var2, var3, var4);
    }

    public void glColor4ubVertex2fSUN(byte var1, byte var2, byte var3, byte var4, float var5, float var6) {
        this.gl.glColor4ubVertex2fSUN(var1, var2, var3, var4, var5, var6);
    }

    public void glColor4ubVertex2fvSUN(ByteBuffer var1, FloatBuffer var2) {
        this.gl.glColor4ubVertex2fvSUN(var1, var2);
    }

    public void glColor4ubVertex2fvSUN(byte[] var1, int var2, float[] var3, int var4) {
        this.gl.glColor4ubVertex2fvSUN(var1, var2, var3, var4);
    }

    public void glColor4ubVertex3fSUN(byte var1, byte var2, byte var3, byte var4, float var5, float var6, float var7) {
        this.gl.glColor4ubVertex3fSUN(var1, var2, var3, var4, var5, var6, var7);
    }

    public void glColor4ubVertex3fvSUN(ByteBuffer var1, FloatBuffer var2) {
        this.gl.glColor4ubVertex3fvSUN(var1, var2);
    }

    public void glColor4ubVertex3fvSUN(byte[] var1, int var2, float[] var3, int var4) {
        this.gl.glColor4ubVertex3fvSUN(var1, var2, var3, var4);
    }

    public void glColor4ubv(ByteBuffer var1) {
        this.gl.glColor4ubv(var1);
    }

    public void glColor4ubv(byte[] var1, int var2) {
        this.gl.glColor4ubv(var1, var2);
    }

    public void glColor4ui(int var1, int var2, int var3, int var4) {
        this.gl.glColor4ui(var1, var2, var3, var4);
    }

    public void glColor4uiv(int[] var1, int var2) {
        this.gl.glColor4uiv(var1, var2);
    }

    public void glColor4uiv(IntBuffer var1) {
        this.gl.glColor4uiv(var1);
    }

    public void glColor4us(short var1, short var2, short var3, short var4) {
        this.gl.glColor4us(var1, var2, var3, var4);
    }

    public void glColor4usv(short[] var1, int var2) {
        this.gl.glColor4usv(var1, var2);
    }

    public void glColor4usv(ShortBuffer var1) {
        this.gl.glColor4usv(var1);
    }

    public void glColorFragmentOp1ATI(int var1, int var2, int var3, int var4, int var5, int var6, int var7) {
        this.gl.glColorFragmentOp1ATI(var1, var2, var3, var4, var5, var6, var7);
    }

    public void glColorFragmentOp2ATI(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10) {
        this.gl.glColorFragmentOp2ATI(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10);
    }

    public void glColorFragmentOp3ATI(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10, int var11, int var12, int var13) {
        this.gl.glColorFragmentOp3ATI(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11, var12, var13);
    }

    public void glColorMask(boolean var1, boolean var2, boolean var3, boolean var4) {
        this.gl.glColorMask(var1, var2, var3, var4);
    }

    public void glColorMaskIndexedEXT(int var1, boolean var2, boolean var3, boolean var4, boolean var5) {
        this.gl.glColorMaskIndexedEXT(var1, var2, var3, var4, var5);
    }

    public void glColorMaterial(int var1, int var2) {
        this.gl.glColorMaterial(var1, var2);
    }

    public void glColorPointer(int var1, int var2, int var3, long var4) {
        this.gl.glColorPointer(var1, var2, var3, var4);
    }

    public void glColorPointer(int var1, int var2, int var3, Buffer var4) {
        this.gl.glColorPointer(var1, var2, var3, var4);
    }

    public void glColorSubTable(int var1, int var2, int var3, int var4, int var5, long var6) {
        this.gl.glColorSubTable(var1, var2, var3, var4, var5, var6);
    }

    public void glColorSubTable(int var1, int var2, int var3, int var4, int var5, Buffer var6) {
        this.gl.glColorSubTable(var1, var2, var3, var4, var5, var6);
    }

    public void glColorTable(int var1, int var2, int var3, int var4, int var5, long var6) {
        this.gl.glColorTable(var1, var2, var3, var4, var5, var6);
    }

    public void glColorTable(int var1, int var2, int var3, int var4, int var5, Buffer var6) {
        this.gl.glColorTable(var1, var2, var3, var4, var5, var6);
    }

    public void glColorTableEXT(int var1, int var2, int var3, int var4, int var5, Buffer var6) {
        this.gl.glColorTableEXT(var1, var2, var3, var4, var5, var6);
    }

    public void glColorTableParameterfv(int var1, int var2, FloatBuffer var3) {
        this.gl.glColorTableParameterfv(var1, var2, var3);
    }

    public void glColorTableParameterfv(int var1, int var2, float[] var3, int var4) {
        this.gl.glColorTableParameterfv(var1, var2, var3, var4);
    }

    public void glColorTableParameteriv(int var1, int var2, int[] var3, int var4) {
        this.gl.glColorTableParameteriv(var1, var2, var3, var4);
    }

    public void glColorTableParameteriv(int var1, int var2, IntBuffer var3) {
        this.gl.glColorTableParameteriv(var1, var2, var3);
    }

    public void glCombinerInputNV(int var1, int var2, int var3, int var4, int var5, int var6) {
        this.gl.glCombinerInputNV(var1, var2, var3, var4, var5, var6);
    }

    public void glCombinerOutputNV(int var1, int var2, int var3, int var4, int var5, int var6, int var7, boolean var8, boolean var9, boolean var10) {
        this.gl.glCombinerOutputNV(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10);
    }

    public void glCombinerParameterfNV(int var1, float var2) {
        this.gl.glCombinerParameterfNV(var1, var2);
    }

    public void glCombinerParameterfvNV(int var1, FloatBuffer var2) {
        this.gl.glCombinerParameterfvNV(var1, var2);
    }

    public void glCombinerParameterfvNV(int var1, float[] var2, int var3) {
        this.gl.glCombinerParameterfvNV(var1, var2, var3);
    }

    public void glCombinerParameteriNV(int var1, int var2) {
        this.gl.glCombinerParameteriNV(var1, var2);
    }

    public void glCombinerParameterivNV(int var1, int[] var2, int var3) {
        this.gl.glCombinerParameterivNV(var1, var2, var3);
    }

    public void glCombinerParameterivNV(int var1, IntBuffer var2) {
        this.gl.glCombinerParameterivNV(var1, var2);
    }

    public void glCombinerStageParameterfvNV(int var1, int var2, FloatBuffer var3) {
        this.gl.glCombinerStageParameterfvNV(var1, var2, var3);
    }

    public void glCombinerStageParameterfvNV(int var1, int var2, float[] var3, int var4) {
        this.gl.glCombinerStageParameterfvNV(var1, var2, var3, var4);
    }

    public void glCompileShader(int var1) {
        this.gl.glCompileShader(var1);
    }

    public void glCompileShaderARB(int var1) {
        this.gl.glCompileShaderARB(var1);
    }

    public void glCompressedTexImage1D(int var1, int var2, int var3, int var4, int var5, int var6, long var7) {
        this.gl.glCompressedTexImage1D(var1, var2, var3, var4, var5, var6, var7);
    }

    public void glCompressedTexImage1D(int var1, int var2, int var3, int var4, int var5, int var6, Buffer var7) {
        this.gl.glCompressedTexImage1D(var1, var2, var3, var4, var5, var6, var7);
    }

    public void glCompressedTexImage2D(int var1, int var2, int var3, int var4, int var5, int var6, int var7, Buffer var8) {
        this.gl.glCompressedTexImage2D(var1, var2, var3, var4, var5, var6, var7, var8);
    }

    public void glCompressedTexImage2D(int var1, int var2, int var3, int var4, int var5, int var6, int var7, long var8) {
        this.gl.glCompressedTexImage2D(var1, var2, var3, var4, var5, var6, var7, var8);
    }

    public void glCompressedTexImage3D(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, long var9) {
        this.gl.glCompressedTexImage3D(var1, var2, var3, var4, var5, var6, var7, var8, var9);
    }

    public void glCompressedTexImage3D(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, Buffer var9) {
        this.gl.glCompressedTexImage3D(var1, var2, var3, var4, var5, var6, var7, var8, var9);
    }

    public void glCompressedTexSubImage1D(int var1, int var2, int var3, int var4, int var5, int var6, long var7) {
        this.gl.glCompressedTexSubImage1D(var1, var2, var3, var4, var5, var6, var7);
    }

    public void glCompressedTexSubImage1D(int var1, int var2, int var3, int var4, int var5, int var6, Buffer var7) {
        this.gl.glCompressedTexSubImage1D(var1, var2, var3, var4, var5, var6, var7);
    }

    public void glCompressedTexSubImage2D(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, Buffer var9) {
        this.gl.glCompressedTexSubImage2D(var1, var2, var3, var4, var5, var6, var7, var8, var9);
    }

    public void glCompressedTexSubImage2D(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, long var9) {
        this.gl.glCompressedTexSubImage2D(var1, var2, var3, var4, var5, var6, var7, var8, var9);
    }

    public void glCompressedTexSubImage3D(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10, long var11) {
        this.gl.glCompressedTexSubImage3D(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11);
    }

    public void glCompressedTexSubImage3D(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10, Buffer var11) {
        this.gl.glCompressedTexSubImage3D(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11);
    }

    public void glConvolutionFilter1D(int var1, int var2, int var3, int var4, int var5, Buffer var6) {
        this.gl.glConvolutionFilter1D(var1, var2, var3, var4, var5, var6);
    }

    public void glConvolutionFilter1D(int var1, int var2, int var3, int var4, int var5, long var6) {
        this.gl.glConvolutionFilter1D(var1, var2, var3, var4, var5, var6);
    }

    public void glConvolutionFilter2D(int var1, int var2, int var3, int var4, int var5, int var6, Buffer var7) {
        this.gl.glConvolutionFilter2D(var1, var2, var3, var4, var5, var6, var7);
    }

    public void glConvolutionFilter2D(int var1, int var2, int var3, int var4, int var5, int var6, long var7) {
        this.gl.glConvolutionFilter2D(var1, var2, var3, var4, var5, var6, var7);
    }

    public void glConvolutionParameterf(int var1, int var2, float var3) {
        this.gl.glConvolutionParameterf(var1, var2, var3);
    }

    public void glConvolutionParameterfv(int var1, int var2, FloatBuffer var3) {
        this.gl.glConvolutionParameterfv(var1, var2, var3);
    }

    public void glConvolutionParameterfv(int var1, int var2, float[] var3, int var4) {
        this.gl.glConvolutionParameterfv(var1, var2, var3, var4);
    }

    public void glConvolutionParameteri(int var1, int var2, int var3) {
        this.gl.glConvolutionParameteri(var1, var2, var3);
    }

    public void glConvolutionParameteriv(int var1, int var2, IntBuffer var3) {
        this.gl.glConvolutionParameteriv(var1, var2, var3);
    }

    public void glConvolutionParameteriv(int var1, int var2, int[] var3, int var4) {
        this.gl.glConvolutionParameteriv(var1, var2, var3, var4);
    }

    public void glCopyColorSubTable(int var1, int var2, int var3, int var4, int var5) {
        this.gl.glCopyColorSubTable(var1, var2, var3, var4, var5);
    }

    public void glCopyColorTable(int var1, int var2, int var3, int var4, int var5) {
        this.gl.glCopyColorTable(var1, var2, var3, var4, var5);
    }

    public void glCopyConvolutionFilter1D(int var1, int var2, int var3, int var4, int var5) {
        this.gl.glCopyConvolutionFilter1D(var1, var2, var3, var4, var5);
    }

    public void glCopyConvolutionFilter2D(int var1, int var2, int var3, int var4, int var5, int var6) {
        this.gl.glCopyConvolutionFilter2D(var1, var2, var3, var4, var5, var6);
    }

    public void glCopyPixels(int var1, int var2, int var3, int var4, int var5) {
        this.gl.glCopyPixels(var1, var2, var3, var4, var5);
    }

    public void glCopyTexImage1D(int var1, int var2, int var3, int var4, int var5, int var6, int var7) {
        this.gl.glCopyTexImage1D(var1, var2, var3, var4, var5, var6, var7);
    }

    public void glCopyTexImage2D(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8) {
        this.gl.glCopyTexImage2D(var1, var2, var3, var4, var5, var6, var7, var8);
    }

    public void glCopyTexSubImage1D(int var1, int var2, int var3, int var4, int var5, int var6) {
        this.gl.glCopyTexSubImage1D(var1, var2, var3, var4, var5, var6);
    }

    public void glCopyTexSubImage2D(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8) {
        this.gl.glCopyTexSubImage2D(var1, var2, var3, var4, var5, var6, var7, var8);
    }

    public void glCopyTexSubImage3D(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9) {
        this.gl.glCopyTexSubImage3D(var1, var2, var3, var4, var5, var6, var7, var8, var9);
    }

    public int glCreateProgram() {
        return this.gl.glCreateProgram();
    }

    public int glCreateProgramObjectARB() {
        return this.gl.glCreateProgramObjectARB();
    }

    public int glCreateShader(int var1) {
        return this.gl.glCreateShader(var1);
    }

    public int glCreateShaderObjectARB(int var1) {
        return this.gl.glCreateShaderObjectARB(var1);
    }

    public void glCullFace(int var1) {
        this.gl.glCullFace(var1);
    }

    public void glCullParameterdvEXT(int var1, DoubleBuffer var2) {
        this.gl.glCullParameterdvEXT(var1, var2);
    }

    public void glCullParameterdvEXT(int var1, double[] var2, int var3) {
        this.gl.glCullParameterdvEXT(var1, var2, var3);
    }

    public void glCullParameterfvEXT(int var1, float[] var2, int var3) {
        this.gl.glCullParameterfvEXT(var1, var2, var3);
    }

    public void glCullParameterfvEXT(int var1, FloatBuffer var2) {
        this.gl.glCullParameterfvEXT(var1, var2);
    }

    public void glCurrentPaletteMatrixARB(int var1) {
        this.gl.glCurrentPaletteMatrixARB(var1);
    }

    public void glDeformSGIX(int var1) {
        this.gl.glDeformSGIX(var1);
    }

    public void glDeformationMap3dSGIX(int var1, double var2, double var4, int var6, int var7, double var8, double var10, int var12, int var13, double var14, double var16, int var18, int var19, double[] var20, int var21) {
        this.gl.glDeformationMap3dSGIX(var1, var2, var4, var6, var7, var8, var10, var12, var13, var14, var16, var18, var19, var20, var21);
    }

    public void glDeformationMap3dSGIX(int var1, double var2, double var4, int var6, int var7, double var8, double var10, int var12, int var13, double var14, double var16, int var18, int var19, DoubleBuffer var20) {
        this.gl.glDeformationMap3dSGIX(var1, var2, var4, var6, var7, var8, var10, var12, var13, var14, var16, var18, var19, var20);
    }

    public void glDeformationMap3fSGIX(int var1, float var2, float var3, int var4, int var5, float var6, float var7, int var8, int var9, float var10, float var11, int var12, int var13, float[] var14, int var15) {
        this.gl.glDeformationMap3fSGIX(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11, var12, var13, var14, var15);
    }

    public void glDeformationMap3fSGIX(int var1, float var2, float var3, int var4, int var5, float var6, float var7, int var8, int var9, float var10, float var11, int var12, int var13, FloatBuffer var14) {
        this.gl.glDeformationMap3fSGIX(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11, var12, var13, var14);
    }

    public void glDeleteAsyncMarkersSGIX(int var1, int var2) {
        this.gl.glDeleteAsyncMarkersSGIX(var1, var2);
    }

    public void glDeleteBufferRegion(int var1) {
        this.gl.glDeleteBufferRegion(var1);
    }

    public void glDeleteBuffers(int var1, IntBuffer var2) {
        this.gl.glDeleteBuffers(var1, var2);
    }

    public void glDeleteBuffers(int var1, int[] var2, int var3) {
        this.gl.glDeleteBuffers(var1, var2, var3);
    }

    public void glDeleteBuffersARB(int var1, IntBuffer var2) {
        this.gl.glDeleteBuffersARB(var1, var2);
    }

    public void glDeleteBuffersARB(int var1, int[] var2, int var3) {
        this.gl.glDeleteBuffersARB(var1, var2, var3);
    }

    public void glDeleteFencesAPPLE(int var1, int[] var2, int var3) {
        this.gl.glDeleteFencesAPPLE(var1, var2, var3);
    }

    public void glDeleteFencesAPPLE(int var1, IntBuffer var2) {
        this.gl.glDeleteFencesAPPLE(var1, var2);
    }

    public void glDeleteFencesNV(int var1, IntBuffer var2) {
        this.gl.glDeleteFencesNV(var1, var2);
    }

    public void glDeleteFencesNV(int var1, int[] var2, int var3) {
        this.gl.glDeleteFencesNV(var1, var2, var3);
    }

    public void glDeleteFragmentShaderATI(int var1) {
        this.gl.glDeleteFragmentShaderATI(var1);
    }

    public void glDeleteFramebuffersEXT(int var1, IntBuffer var2) {
        this.gl.glDeleteFramebuffersEXT(var1, var2);
    }

    public void glDeleteFramebuffersEXT(int var1, int[] var2, int var3) {
        this.gl.glDeleteFramebuffersEXT(var1, var2, var3);
    }

    public void glDeleteLists(int var1, int var2) {
        this.gl.glDeleteLists(var1, var2);
    }

    public void glDeleteObjectARB(int var1) {
        this.gl.glDeleteObjectARB(var1);
    }

    public void glDeleteOcclusionQueriesNV(int var1, IntBuffer var2) {
        this.gl.glDeleteOcclusionQueriesNV(var1, var2);
    }

    public void glDeleteOcclusionQueriesNV(int var1, int[] var2, int var3) {
        this.gl.glDeleteOcclusionQueriesNV(var1, var2, var3);
    }

    public void glDeleteProgram(int var1) {
        this.gl.glDeleteProgram(var1);
    }

    public void glDeleteProgramsARB(int var1, IntBuffer var2) {
        this.gl.glDeleteProgramsARB(var1, var2);
    }

    public void glDeleteProgramsARB(int var1, int[] var2, int var3) {
        this.gl.glDeleteProgramsARB(var1, var2, var3);
    }

    public void glDeleteProgramsNV(int var1, int[] var2, int var3) {
        this.gl.glDeleteProgramsNV(var1, var2, var3);
    }

    public void glDeleteProgramsNV(int var1, IntBuffer var2) {
        this.gl.glDeleteProgramsNV(var1, var2);
    }

    public void glDeleteQueries(int var1, int[] var2, int var3) {
        this.gl.glDeleteQueries(var1, var2, var3);
    }

    public void glDeleteQueries(int var1, IntBuffer var2) {
        this.gl.glDeleteQueries(var1, var2);
    }

    public void glDeleteQueriesARB(int var1, int[] var2, int var3) {
        this.gl.glDeleteQueriesARB(var1, var2, var3);
    }

    public void glDeleteQueriesARB(int var1, IntBuffer var2) {
        this.gl.glDeleteQueriesARB(var1, var2);
    }

    public void glDeleteRenderbuffersEXT(int var1, int[] var2, int var3) {
        this.gl.glDeleteRenderbuffersEXT(var1, var2, var3);
    }

    public void glDeleteRenderbuffersEXT(int var1, IntBuffer var2) {
        this.gl.glDeleteRenderbuffersEXT(var1, var2);
    }

    public void glDeleteShader(int var1) {
        this.gl.glDeleteShader(var1);
    }

    public void glDeleteTextures(int var1, int[] var2, int var3) {
        this.gl.glDeleteTextures(var1, var2, var3);
    }

    public void glDeleteTextures(int var1, IntBuffer var2) {
        this.gl.glDeleteTextures(var1, var2);
    }

    public void glDeleteVertexArraysAPPLE(int var1, IntBuffer var2) {
        this.gl.glDeleteVertexArraysAPPLE(var1, var2);
    }

    public void glDeleteVertexArraysAPPLE(int var1, int[] var2, int var3) {
        this.gl.glDeleteVertexArraysAPPLE(var1, var2, var3);
    }

    public void glDeleteVertexShaderEXT(int var1) {
        this.gl.glDeleteVertexShaderEXT(var1);
    }

    public void glDepthBoundsEXT(double var1, double var3) {
        this.gl.glDepthBoundsEXT(var1, var3);
    }

    public void glDepthBoundsdNV(double var1, double var3) {
        this.gl.glDepthBoundsdNV(var1, var3);
    }

    public void glDepthFunc(int var1) {
        this.gl.glDepthFunc(var1);
    }

    public void glDepthMask(boolean var1) {
        this.gl.glDepthMask(var1);
    }

    public void glDepthRange(double var1, double var3) {
        this.gl.glDepthRange(var1, var3);
    }

    public void glDepthRangedNV(double var1, double var3) {
        this.gl.glDepthRangedNV(var1, var3);
    }

    public void glDetachObjectARB(int var1, int var2) {
        this.gl.glDetachObjectARB(var1, var2);
    }

    public void glDetachShader(int var1, int var2) {
        this.gl.glDetachShader(var1, var2);
    }

    public void glDetailTexFuncSGIS(int var1, int var2, FloatBuffer var3) {
        this.gl.glDetailTexFuncSGIS(var1, var2, var3);
    }

    public void glDetailTexFuncSGIS(int var1, int var2, float[] var3, int var4) {
        this.gl.glDetailTexFuncSGIS(var1, var2, var3, var4);
    }

    public void glDisable(int var1) {
        this.gl.glDisable(var1);
    }

    public void glDisableClientState(int var1) {
        this.gl.glDisableClientState(var1);
    }

    public void glDisableIndexedEXT(int var1, int var2) {
        this.gl.glDisableIndexedEXT(var1, var2);
    }

    public void glDisableVariantClientStateEXT(int var1) {
        this.gl.glDisableVariantClientStateEXT(var1);
    }

    public void glDisableVertexAttribAPPLE(int var1, int var2) {
        this.gl.glDisableVertexAttribAPPLE(var1, var2);
    }

    public void glDisableVertexAttribArray(int var1) {
        this.gl.glDisableVertexAttribArray(var1);
    }

    public void glDisableVertexAttribArrayARB(int var1) {
        this.gl.glDisableVertexAttribArrayARB(var1);
    }

    public void glDrawArrays(int var1, int var2, int var3) {
        this.gl.glDrawArrays(var1, var2, var3);
    }

    public void glDrawArraysInstancedEXT(int var1, int var2, int var3, int var4) {
        this.gl.glDrawArraysInstancedEXT(var1, var2, var3, var4);
    }

    public void glDrawBuffer(int var1) {
        this.gl.glDrawBuffer(var1);
    }

    public void glDrawBufferRegion(int var1, int var2, int var3, int var4, int var5, int var6, int var7) {
        this.gl.glDrawBufferRegion(var1, var2, var3, var4, var5, var6, var7);
    }

    public void glDrawBuffers(int var1, int[] var2, int var3) {
        this.gl.glDrawBuffers(var1, var2, var3);
    }

    public void glDrawBuffers(int var1, IntBuffer var2) {
        this.gl.glDrawBuffers(var1, var2);
    }

    public void glDrawBuffersARB(int var1, int[] var2, int var3) {
        this.gl.glDrawBuffersARB(var1, var2, var3);
    }

    public void glDrawBuffersARB(int var1, IntBuffer var2) {
        this.gl.glDrawBuffersARB(var1, var2);
    }

    public void glDrawBuffersATI(int var1, int[] var2, int var3) {
        this.gl.glDrawBuffersATI(var1, var2, var3);
    }

    public void glDrawBuffersATI(int var1, IntBuffer var2) {
        this.gl.glDrawBuffersATI(var1, var2);
    }

    public void glDrawElementArrayAPPLE(int var1, int var2, int var3) {
        this.gl.glDrawElementArrayAPPLE(var1, var2, var3);
    }

    public void glDrawElementArrayATI(int var1, int var2) {
        this.gl.glDrawElementArrayATI(var1, var2);
    }

    public void glDrawElements(int var1, int var2, int var3, long var4) {
        this.gl.glDrawElements(var1, var2, var3, var4);
    }

    public void glDrawElements(int var1, int var2, int var3, Buffer var4) {
        this.gl.glDrawElements(var1, var2, var3, var4);
    }

    public void glDrawElementsInstancedEXT(int var1, int var2, int var3, Buffer var4, int var5) {
        this.gl.glDrawElementsInstancedEXT(var1, var2, var3, var4, var5);
    }

    public void glDrawMeshArraysSUN(int var1, int var2, int var3, int var4) {
        this.gl.glDrawMeshArraysSUN(var1, var2, var3, var4);
    }

    public void glDrawPixels(int var1, int var2, int var3, int var4, Buffer var5) {
        this.gl.glDrawPixels(var1, var2, var3, var4, var5);
    }

    public void glDrawPixels(int var1, int var2, int var3, int var4, long var5) {
        this.gl.glDrawPixels(var1, var2, var3, var4, var5);
    }

    public void glDrawRangeElementArrayAPPLE(int var1, int var2, int var3, int var4, int var5) {
        this.gl.glDrawRangeElementArrayAPPLE(var1, var2, var3, var4, var5);
    }

    public void glDrawRangeElementArrayATI(int var1, int var2, int var3, int var4) {
        this.gl.glDrawRangeElementArrayATI(var1, var2, var3, var4);
    }

    public void glDrawRangeElements(int var1, int var2, int var3, int var4, int var5, Buffer var6) {
        this.gl.glDrawRangeElements(var1, var2, var3, var4, var5, var6);
    }

    public void glDrawRangeElements(int var1, int var2, int var3, int var4, int var5, long var6) {
        this.gl.glDrawRangeElements(var1, var2, var3, var4, var5, var6);
    }

    public void glEdgeFlag(boolean var1) {
        this.gl.glEdgeFlag(var1);
    }

    public void glEdgeFlagPointer(int var1, Buffer var2) {
        this.gl.glEdgeFlagPointer(var1, var2);
    }

    public void glEdgeFlagPointer(int var1, long var2) {
        this.gl.glEdgeFlagPointer(var1, var2);
    }

    public void glEdgeFlagv(byte[] var1, int var2) {
        this.gl.glEdgeFlagv(var1, var2);
    }

    public void glEdgeFlagv(ByteBuffer var1) {
        this.gl.glEdgeFlagv(var1);
    }

    public void glElementPointerAPPLE(int var1, Buffer var2) {
        this.gl.glElementPointerAPPLE(var1, var2);
    }

    public void glElementPointerATI(int var1, Buffer var2) {
        this.gl.glElementPointerATI(var1, var2);
    }

    public void glElementPointerATI(int var1, long var2) {
        this.gl.glElementPointerATI(var1, var2);
    }

    public void glEnable(int var1) {
        this.gl.glEnable(var1);
    }

    public void glEnableClientState(int var1) {
        this.gl.glEnableClientState(var1);
    }

    public void glEnableIndexedEXT(int var1, int var2) {
        this.gl.glEnableIndexedEXT(var1, var2);
    }

    public void glEnableVariantClientStateEXT(int var1) {
        this.gl.glEnableVariantClientStateEXT(var1);
    }

    public void glEnableVertexAttribAPPLE(int var1, int var2) {
        this.gl.glEnableVertexAttribAPPLE(var1, var2);
    }

    public void glEnableVertexAttribArray(int var1) {
        this.gl.glEnableVertexAttribArray(var1);
    }

    public void glEnableVertexAttribArrayARB(int var1) {
        this.gl.glEnableVertexAttribArrayARB(var1);
    }

    public void glEnd() {
        this.gl.glEnd();
    }

    public void glEndFragmentShaderATI() {
        this.gl.glEndFragmentShaderATI();
    }

    public void glEndList() {
        this.gl.glEndList();
    }

    public void glEndOcclusionQueryNV() {
        this.gl.glEndOcclusionQueryNV();
    }

    public void glEndQuery(int var1) {
        this.gl.glEndQuery(var1);
    }

    public void glEndQueryARB(int var1) {
        this.gl.glEndQueryARB(var1);
    }

    public void glEndTransformFeedbackNV() {
        this.gl.glEndTransformFeedbackNV();
    }

    public void glEndVertexShaderEXT() {
        this.gl.glEndVertexShaderEXT();
    }

    public void glEvalCoord1d(double var1) {
        this.gl.glEvalCoord1d(var1);
    }

    public void glEvalCoord1dv(DoubleBuffer var1) {
        this.gl.glEvalCoord1dv(var1);
    }

    public void glEvalCoord1dv(double[] var1, int var2) {
        this.gl.glEvalCoord1dv(var1, var2);
    }

    public void glEvalCoord1f(float var1) {
        this.gl.glEvalCoord1f(var1);
    }

    public void glEvalCoord1fv(FloatBuffer var1) {
        this.gl.glEvalCoord1fv(var1);
    }

    public void glEvalCoord1fv(float[] var1, int var2) {
        this.gl.glEvalCoord1fv(var1, var2);
    }

    public void glEvalCoord2d(double var1, double var3) {
        this.gl.glEvalCoord2d(var1, var3);
    }

    public void glEvalCoord2dv(double[] var1, int var2) {
        this.gl.glEvalCoord2dv(var1, var2);
    }

    public void glEvalCoord2dv(DoubleBuffer var1) {
        this.gl.glEvalCoord2dv(var1);
    }

    public void glEvalCoord2f(float var1, float var2) {
        this.gl.glEvalCoord2f(var1, var2);
    }

    public void glEvalCoord2fv(float[] var1, int var2) {
        this.gl.glEvalCoord2fv(var1, var2);
    }

    public void glEvalCoord2fv(FloatBuffer var1) {
        this.gl.glEvalCoord2fv(var1);
    }

    public void glEvalMapsNV(int var1, int var2) {
        this.gl.glEvalMapsNV(var1, var2);
    }

    public void glEvalMesh1(int var1, int var2, int var3) {
        this.gl.glEvalMesh1(var1, var2, var3);
    }

    public void glEvalMesh2(int var1, int var2, int var3, int var4, int var5) {
        this.gl.glEvalMesh2(var1, var2, var3, var4, var5);
    }

    public void glEvalPoint1(int var1) {
        this.gl.glEvalPoint1(var1);
    }

    public void glEvalPoint2(int var1, int var2) {
        this.gl.glEvalPoint2(var1, var2);
    }

    public void glExecuteProgramNV(int var1, int var2, FloatBuffer var3) {
        this.gl.glExecuteProgramNV(var1, var2, var3);
    }

    public void glExecuteProgramNV(int var1, int var2, float[] var3, int var4) {
        this.gl.glExecuteProgramNV(var1, var2, var3, var4);
    }

    public void glExtractComponentEXT(int var1, int var2, int var3) {
        this.gl.glExtractComponentEXT(var1, var2, var3);
    }

    public void glFeedbackBuffer(int var1, int var2, FloatBuffer var3) {
        this.gl.glFeedbackBuffer(var1, var2, var3);
    }

    public void glFinalCombinerInputNV(int var1, int var2, int var3, int var4) {
        this.gl.glFinalCombinerInputNV(var1, var2, var3, var4);
    }

    public void glFinish() {
        this.gl.glFinish();
    }

    public int glFinishAsyncSGIX(IntBuffer var1) {
        return this.gl.glFinishAsyncSGIX(var1);
    }

    public int glFinishAsyncSGIX(int[] var1, int var2) {
        return this.gl.glFinishAsyncSGIX(var1, var2);
    }

    public void glFinishFenceAPPLE(int var1) {
        this.gl.glFinishFenceAPPLE(var1);
    }

    public void glFinishFenceNV(int var1) {
        this.gl.glFinishFenceNV(var1);
    }

    public void glFinishObjectAPPLE(int var1, int var2) {
        this.gl.glFinishObjectAPPLE(var1, var2);
    }

    public void glFinishRenderAPPLE() {
        this.gl.glFinishRenderAPPLE();
    }

    public void glFinishTextureSUNX() {
        this.gl.glFinishTextureSUNX();
    }

    public void glFlush() {
        this.gl.glFlush();
    }

    public void glFlushMappedBufferRangeAPPLE(int var1, int var2, int var3) {
        this.gl.glFlushMappedBufferRangeAPPLE(var1, var2, var3);
    }

    public void glFlushPixelDataRangeNV(int var1) {
        this.gl.glFlushPixelDataRangeNV(var1);
    }

    public void glFlushRasterSGIX() {
        this.gl.glFlushRasterSGIX();
    }

    public void glFlushRenderAPPLE() {
        this.gl.glFlushRenderAPPLE();
    }

    public void glFlushVertexArrayRangeAPPLE(int var1, Buffer var2) {
        this.gl.glFlushVertexArrayRangeAPPLE(var1, var2);
    }

    public void glFlushVertexArrayRangeNV() {
        this.gl.glFlushVertexArrayRangeNV();
    }

    public void glFogCoordPointer(int var1, int var2, Buffer var3) {
        this.gl.glFogCoordPointer(var1, var2, var3);
    }

    public void glFogCoordPointer(int var1, int var2, long var3) {
        this.gl.glFogCoordPointer(var1, var2, var3);
    }

    public void glFogCoordPointerEXT(int var1, int var2, Buffer var3) {
        this.gl.glFogCoordPointerEXT(var1, var2, var3);
    }

    public void glFogCoordPointerEXT(int var1, int var2, long var3) {
        this.gl.glFogCoordPointerEXT(var1, var2, var3);
    }

    public void glFogCoordd(double var1) {
        this.gl.glFogCoordd(var1);
    }

    public void glFogCoorddEXT(double var1) {
        this.gl.glFogCoorddEXT(var1);
    }

    public void glFogCoorddv(double[] var1, int var2) {
        this.gl.glFogCoorddv(var1, var2);
    }

    public void glFogCoorddv(DoubleBuffer var1) {
        this.gl.glFogCoorddv(var1);
    }

    public void glFogCoorddvEXT(DoubleBuffer var1) {
        this.gl.glFogCoorddvEXT(var1);
    }

    public void glFogCoorddvEXT(double[] var1, int var2) {
        this.gl.glFogCoorddvEXT(var1, var2);
    }

    public void glFogCoordf(float var1) {
        this.gl.glFogCoordf(var1);
    }

    public void glFogCoordfEXT(float var1) {
        this.gl.glFogCoordfEXT(var1);
    }

    public void glFogCoordfv(FloatBuffer var1) {
        this.gl.glFogCoordfv(var1);
    }

    public void glFogCoordfv(float[] var1, int var2) {
        this.gl.glFogCoordfv(var1, var2);
    }

    public void glFogCoordfvEXT(FloatBuffer var1) {
        this.gl.glFogCoordfvEXT(var1);
    }

    public void glFogCoordfvEXT(float[] var1, int var2) {
        this.gl.glFogCoordfvEXT(var1, var2);
    }

    public void glFogCoordhNV(short var1) {
        this.gl.glFogCoordhNV(var1);
    }

    public void glFogCoordhvNV(ShortBuffer var1) {
        this.gl.glFogCoordhvNV(var1);
    }

    public void glFogCoordhvNV(short[] var1, int var2) {
        this.gl.glFogCoordhvNV(var1, var2);
    }

    public void glFogFuncSGIS(int var1, float[] var2, int var3) {
        this.gl.glFogFuncSGIS(var1, var2, var3);
    }

    public void glFogFuncSGIS(int var1, FloatBuffer var2) {
        this.gl.glFogFuncSGIS(var1, var2);
    }

    public void glFogf(int var1, float var2) {
        this.gl.glFogf(var1, var2);
    }

    public void glFogfv(int var1, FloatBuffer var2) {
        this.gl.glFogfv(var1, var2);
    }

    public void glFogfv(int var1, float[] var2, int var3) {
        this.gl.glFogfv(var1, var2, var3);
    }

    public void glFogi(int var1, int var2) {
        this.gl.glFogi(var1, var2);
    }

    public void glFogiv(int var1, int[] var2, int var3) {
        this.gl.glFogiv(var1, var2, var3);
    }

    public void glFogiv(int var1, IntBuffer var2) {
        this.gl.glFogiv(var1, var2);
    }

    public void glFragmentColorMaterialSGIX(int var1, int var2) {
        this.gl.glFragmentColorMaterialSGIX(var1, var2);
    }

    public void glFragmentLightModelfSGIX(int var1, float var2) {
        this.gl.glFragmentLightModelfSGIX(var1, var2);
    }

    public void glFragmentLightModelfvSGIX(int var1, FloatBuffer var2) {
        this.gl.glFragmentLightModelfvSGIX(var1, var2);
    }

    public void glFragmentLightModelfvSGIX(int var1, float[] var2, int var3) {
        this.gl.glFragmentLightModelfvSGIX(var1, var2, var3);
    }

    public void glFragmentLightModeliSGIX(int var1, int var2) {
        this.gl.glFragmentLightModeliSGIX(var1, var2);
    }

    public void glFragmentLightModelivSGIX(int var1, IntBuffer var2) {
        this.gl.glFragmentLightModelivSGIX(var1, var2);
    }

    public void glFragmentLightModelivSGIX(int var1, int[] var2, int var3) {
        this.gl.glFragmentLightModelivSGIX(var1, var2, var3);
    }

    public void glFragmentLightfSGIX(int var1, int var2, float var3) {
        this.gl.glFragmentLightfSGIX(var1, var2, var3);
    }

    public void glFragmentLightfvSGIX(int var1, int var2, FloatBuffer var3) {
        this.gl.glFragmentLightfvSGIX(var1, var2, var3);
    }

    public void glFragmentLightfvSGIX(int var1, int var2, float[] var3, int var4) {
        this.gl.glFragmentLightfvSGIX(var1, var2, var3, var4);
    }

    public void glFragmentLightiSGIX(int var1, int var2, int var3) {
        this.gl.glFragmentLightiSGIX(var1, var2, var3);
    }

    public void glFragmentLightivSGIX(int var1, int var2, int[] var3, int var4) {
        this.gl.glFragmentLightivSGIX(var1, var2, var3, var4);
    }

    public void glFragmentLightivSGIX(int var1, int var2, IntBuffer var3) {
        this.gl.glFragmentLightivSGIX(var1, var2, var3);
    }

    public void glFragmentMaterialfSGIX(int var1, int var2, float var3) {
        this.gl.glFragmentMaterialfSGIX(var1, var2, var3);
    }

    public void glFragmentMaterialfvSGIX(int var1, int var2, FloatBuffer var3) {
        this.gl.glFragmentMaterialfvSGIX(var1, var2, var3);
    }

    public void glFragmentMaterialfvSGIX(int var1, int var2, float[] var3, int var4) {
        this.gl.glFragmentMaterialfvSGIX(var1, var2, var3, var4);
    }

    public void glFragmentMaterialiSGIX(int var1, int var2, int var3) {
        this.gl.glFragmentMaterialiSGIX(var1, var2, var3);
    }

    public void glFragmentMaterialivSGIX(int var1, int var2, IntBuffer var3) {
        this.gl.glFragmentMaterialivSGIX(var1, var2, var3);
    }

    public void glFragmentMaterialivSGIX(int var1, int var2, int[] var3, int var4) {
        this.gl.glFragmentMaterialivSGIX(var1, var2, var3, var4);
    }

    public void glFrameZoomSGIX(int var1) {
        this.gl.glFrameZoomSGIX(var1);
    }

    public void glFramebufferRenderbufferEXT(int var1, int var2, int var3, int var4) {
        this.gl.glFramebufferRenderbufferEXT(var1, var2, var3, var4);
    }

    public void glFramebufferTexture1DEXT(int var1, int var2, int var3, int var4, int var5) {
        this.gl.glFramebufferTexture1DEXT(var1, var2, var3, var4, var5);
    }

    public void glFramebufferTexture2DEXT(int var1, int var2, int var3, int var4, int var5) {
        this.gl.glFramebufferTexture2DEXT(var1, var2, var3, var4, var5);
    }

    public void glFramebufferTexture3DEXT(int var1, int var2, int var3, int var4, int var5, int var6) {
        this.gl.glFramebufferTexture3DEXT(var1, var2, var3, var4, var5, var6);
    }

    public void glFramebufferTextureEXT(int var1, int var2, int var3, int var4) {
        this.gl.glFramebufferTextureEXT(var1, var2, var3, var4);
    }

    public void glFramebufferTextureFaceEXT(int var1, int var2, int var3, int var4, int var5) {
        this.gl.glFramebufferTextureFaceEXT(var1, var2, var3, var4, var5);
    }

    public void glFramebufferTextureLayerEXT(int var1, int var2, int var3, int var4, int var5) {
        this.gl.glFramebufferTextureLayerEXT(var1, var2, var3, var4, var5);
    }

    public void glFreeObjectBufferATI(int var1) {
        this.gl.glFreeObjectBufferATI(var1);
    }

    public void glFrontFace(int var1) {
        this.gl.glFrontFace(var1);
    }

    public void glFrustum(double var1, double var3, double var5, double var7, double var9, double var11) {
        this.gl.glFrustum(var1, var3, var5, var7, var9, var11);
    }

    public int glGenAsyncMarkersSGIX(int var1) {
        return this.gl.glGenAsyncMarkersSGIX(var1);
    }

    public void glGenBuffers(int var1, int[] var2, int var3) {
        this.gl.glGenBuffers(var1, var2, var3);
    }

    public void glGenBuffers(int var1, IntBuffer var2) {
        this.gl.glGenBuffers(var1, var2);
    }

    public void glGenBuffersARB(int var1, int[] var2, int var3) {
        this.gl.glGenBuffersARB(var1, var2, var3);
    }

    public void glGenBuffersARB(int var1, IntBuffer var2) {
        this.gl.glGenBuffersARB(var1, var2);
    }

    public void glGenFencesAPPLE(int var1, int[] var2, int var3) {
        this.gl.glGenFencesAPPLE(var1, var2, var3);
    }

    public void glGenFencesAPPLE(int var1, IntBuffer var2) {
        this.gl.glGenFencesAPPLE(var1, var2);
    }

    public void glGenFencesNV(int var1, int[] var2, int var3) {
        this.gl.glGenFencesNV(var1, var2, var3);
    }

    public void glGenFencesNV(int var1, IntBuffer var2) {
        this.gl.glGenFencesNV(var1, var2);
    }

    public int glGenFragmentShadersATI(int var1) {
        return this.gl.glGenFragmentShadersATI(var1);
    }

    public void glGenFramebuffersEXT(int var1, IntBuffer var2) {
        this.gl.glGenFramebuffersEXT(var1, var2);
    }

    public void glGenFramebuffersEXT(int var1, int[] var2, int var3) {
        this.gl.glGenFramebuffersEXT(var1, var2, var3);
    }

    public int glGenLists(int var1) {
        return this.gl.glGenLists(var1);
    }

    public void glGenOcclusionQueriesNV(int var1, IntBuffer var2) {
        this.gl.glGenOcclusionQueriesNV(var1, var2);
    }

    public void glGenOcclusionQueriesNV(int var1, int[] var2, int var3) {
        this.gl.glGenOcclusionQueriesNV(var1, var2, var3);
    }

    public void glGenProgramsARB(int var1, int[] var2, int var3) {
        this.gl.glGenProgramsARB(var1, var2, var3);
    }

    public void glGenProgramsARB(int var1, IntBuffer var2) {
        this.gl.glGenProgramsARB(var1, var2);
    }

    public void glGenProgramsNV(int var1, IntBuffer var2) {
        this.gl.glGenProgramsNV(var1, var2);
    }

    public void glGenProgramsNV(int var1, int[] var2, int var3) {
        this.gl.glGenProgramsNV(var1, var2, var3);
    }

    public void glGenQueries(int var1, int[] var2, int var3) {
        this.gl.glGenQueries(var1, var2, var3);
    }

    public void glGenQueries(int var1, IntBuffer var2) {
        this.gl.glGenQueries(var1, var2);
    }

    public void glGenQueriesARB(int var1, IntBuffer var2) {
        this.gl.glGenQueriesARB(var1, var2);
    }

    public void glGenQueriesARB(int var1, int[] var2, int var3) {
        this.gl.glGenQueriesARB(var1, var2, var3);
    }

    public void glGenRenderbuffersEXT(int var1, int[] var2, int var3) {
        this.gl.glGenRenderbuffersEXT(var1, var2, var3);
    }

    public void glGenRenderbuffersEXT(int var1, IntBuffer var2) {
        this.gl.glGenRenderbuffersEXT(var1, var2);
    }

    public int glGenSymbolsEXT(int var1, int var2, int var3, int var4) {
        return this.gl.glGenSymbolsEXT(var1, var2, var3, var4);
    }

    public void glGenTextures(int var1, int[] var2, int var3) {
        this.gl.glGenTextures(var1, var2, var3);
    }

    public void glGenTextures(int var1, IntBuffer var2) {
        this.gl.glGenTextures(var1, var2);
    }

    public void glGenVertexArraysAPPLE(int var1, int[] var2, int var3) {
        this.gl.glGenVertexArraysAPPLE(var1, var2, var3);
    }

    public void glGenVertexArraysAPPLE(int var1, IntBuffer var2) {
        this.gl.glGenVertexArraysAPPLE(var1, var2);
    }

    public int glGenVertexShadersEXT(int var1) {
        return this.gl.glGenVertexShadersEXT(var1);
    }

    public void glGenerateMipmapEXT(int var1) {
        this.gl.glGenerateMipmapEXT(var1);
    }

    public void glGetActiveAttrib(int var1, int var2, int var3, int[] var4, int var5, int[] var6, int var7, int[] var8, int var9, byte[] var10, int var11) {
        this.gl.glGetActiveAttrib(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11);
    }

    public void glGetActiveAttrib(int var1, int var2, int var3, IntBuffer var4, IntBuffer var5, IntBuffer var6, ByteBuffer var7) {
        this.gl.glGetActiveAttrib(var1, var2, var3, var4, var5, var6, var7);
    }

    public void glGetActiveAttribARB(int var1, int var2, int var3, IntBuffer var4, IntBuffer var5, IntBuffer var6, ByteBuffer var7) {
        this.gl.glGetActiveAttribARB(var1, var2, var3, var4, var5, var6, var7);
    }

    public void glGetActiveAttribARB(int var1, int var2, int var3, int[] var4, int var5, int[] var6, int var7, int[] var8, int var9, byte[] var10, int var11) {
        this.gl.glGetActiveAttribARB(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11);
    }

    public void glGetActiveUniform(int var1, int var2, int var3, int[] var4, int var5, int[] var6, int var7, int[] var8, int var9, byte[] var10, int var11) {
        this.gl.glGetActiveUniform(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11);
    }

    public void glGetActiveUniform(int var1, int var2, int var3, IntBuffer var4, IntBuffer var5, IntBuffer var6, ByteBuffer var7) {
        this.gl.glGetActiveUniform(var1, var2, var3, var4, var5, var6, var7);
    }

    public void glGetActiveUniformARB(int var1, int var2, int var3, IntBuffer var4, IntBuffer var5, IntBuffer var6, ByteBuffer var7) {
        this.gl.glGetActiveUniformARB(var1, var2, var3, var4, var5, var6, var7);
    }

    public void glGetActiveUniformARB(int var1, int var2, int var3, int[] var4, int var5, int[] var6, int var7, int[] var8, int var9, byte[] var10, int var11) {
        this.gl.glGetActiveUniformARB(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11);
    }

    public void glGetActiveVaryingNV(int var1, int var2, int var3, int[] var4, int var5, int[] var6, int var7, int[] var8, int var9, byte[] var10, int var11) {
        this.gl.glGetActiveVaryingNV(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11);
    }

    public void glGetActiveVaryingNV(int var1, int var2, int var3, IntBuffer var4, IntBuffer var5, IntBuffer var6, ByteBuffer var7) {
        this.gl.glGetActiveVaryingNV(var1, var2, var3, var4, var5, var6, var7);
    }

    public void glGetArrayObjectfvATI(int var1, int var2, float[] var3, int var4) {
        this.gl.glGetArrayObjectfvATI(var1, var2, var3, var4);
    }

    public void glGetArrayObjectfvATI(int var1, int var2, FloatBuffer var3) {
        this.gl.glGetArrayObjectfvATI(var1, var2, var3);
    }

    public void glGetArrayObjectivATI(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetArrayObjectivATI(var1, var2, var3, var4);
    }

    public void glGetArrayObjectivATI(int var1, int var2, IntBuffer var3) {
        this.gl.glGetArrayObjectivATI(var1, var2, var3);
    }

    public void glGetAttachedObjectsARB(int var1, int var2, int[] var3, int var4, int[] var5, int var6) {
        this.gl.glGetAttachedObjectsARB(var1, var2, var3, var4, var5, var6);
    }

    public void glGetAttachedObjectsARB(int var1, int var2, IntBuffer var3, IntBuffer var4) {
        this.gl.glGetAttachedObjectsARB(var1, var2, var3, var4);
    }

    public void glGetAttachedShaders(int var1, int var2, int[] var3, int var4, int[] var5, int var6) {
        this.gl.glGetAttachedShaders(var1, var2, var3, var4, var5, var6);
    }

    public void glGetAttachedShaders(int var1, int var2, IntBuffer var3, IntBuffer var4) {
        this.gl.glGetAttachedShaders(var1, var2, var3, var4);
    }

    public int glGetAttribLocation(int var1, String var2) {
        return this.gl.glGetAttribLocation(var1, var2);
    }

    public int glGetAttribLocationARB(int var1, String var2) {
        return this.gl.glGetAttribLocationARB(var1, var2);
    }

    public void glGetBooleanIndexedvEXT(int var1, int var2, byte[] var3, int var4) {
        this.gl.glGetBooleanIndexedvEXT(var1, var2, var3, var4);
    }

    public void glGetBooleanIndexedvEXT(int var1, int var2, ByteBuffer var3) {
        this.gl.glGetBooleanIndexedvEXT(var1, var2, var3);
    }

    public void glGetBooleanv(int var1, byte[] var2, int var3) {
        this.gl.glGetBooleanv(var1, var2, var3);
    }

    public void glGetBooleanv(int var1, ByteBuffer var2) {
        this.gl.glGetBooleanv(var1, var2);
    }

    public void glGetBufferParameteriv(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetBufferParameteriv(var1, var2, var3, var4);
    }

    public void glGetBufferParameteriv(int var1, int var2, IntBuffer var3) {
        this.gl.glGetBufferParameteriv(var1, var2, var3);
    }

    public void glGetBufferParameterivARB(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetBufferParameterivARB(var1, var2, var3, var4);
    }

    public void glGetBufferParameterivARB(int var1, int var2, IntBuffer var3) {
        this.gl.glGetBufferParameterivARB(var1, var2, var3);
    }

    public void glGetBufferSubData(int var1, int var2, int var3, Buffer var4) {
        this.gl.glGetBufferSubData(var1, var2, var3, var4);
    }

    public void glGetBufferSubDataARB(int var1, int var2, int var3, Buffer var4) {
        this.gl.glGetBufferSubDataARB(var1, var2, var3, var4);
    }

    public void glGetClipPlane(int var1, DoubleBuffer var2) {
        this.gl.glGetClipPlane(var1, var2);
    }

    public void glGetClipPlane(int var1, double[] var2, int var3) {
        this.gl.glGetClipPlane(var1, var2, var3);
    }

    public void glGetColorTable(int var1, int var2, int var3, long var4) {
        this.gl.glGetColorTable(var1, var2, var3, var4);
    }

    public void glGetColorTable(int var1, int var2, int var3, Buffer var4) {
        this.gl.glGetColorTable(var1, var2, var3, var4);
    }

    public void glGetColorTableEXT(int var1, int var2, int var3, Buffer var4) {
        this.gl.glGetColorTableEXT(var1, var2, var3, var4);
    }

    public void glGetColorTableParameterfv(int var1, int var2, FloatBuffer var3) {
        this.gl.glGetColorTableParameterfv(var1, var2, var3);
    }

    public void glGetColorTableParameterfv(int var1, int var2, float[] var3, int var4) {
        this.gl.glGetColorTableParameterfv(var1, var2, var3, var4);
    }

    public void glGetColorTableParameterfvEXT(int var1, int var2, FloatBuffer var3) {
        this.gl.glGetColorTableParameterfvEXT(var1, var2, var3);
    }

    public void glGetColorTableParameterfvEXT(int var1, int var2, float[] var3, int var4) {
        this.gl.glGetColorTableParameterfvEXT(var1, var2, var3, var4);
    }

    public void glGetColorTableParameteriv(int var1, int var2, IntBuffer var3) {
        this.gl.glGetColorTableParameteriv(var1, var2, var3);
    }

    public void glGetColorTableParameteriv(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetColorTableParameteriv(var1, var2, var3, var4);
    }

    public void glGetColorTableParameterivEXT(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetColorTableParameterivEXT(var1, var2, var3, var4);
    }

    public void glGetColorTableParameterivEXT(int var1, int var2, IntBuffer var3) {
        this.gl.glGetColorTableParameterivEXT(var1, var2, var3);
    }

    public void glGetCombinerInputParameterfvNV(int var1, int var2, int var3, int var4, float[] var5, int var6) {
        this.gl.glGetCombinerInputParameterfvNV(var1, var2, var3, var4, var5, var6);
    }

    public void glGetCombinerInputParameterfvNV(int var1, int var2, int var3, int var4, FloatBuffer var5) {
        this.gl.glGetCombinerInputParameterfvNV(var1, var2, var3, var4, var5);
    }

    public void glGetCombinerInputParameterivNV(int var1, int var2, int var3, int var4, int[] var5, int var6) {
        this.gl.glGetCombinerInputParameterivNV(var1, var2, var3, var4, var5, var6);
    }

    public void glGetCombinerInputParameterivNV(int var1, int var2, int var3, int var4, IntBuffer var5) {
        this.gl.glGetCombinerInputParameterivNV(var1, var2, var3, var4, var5);
    }

    public void glGetCombinerOutputParameterfvNV(int var1, int var2, int var3, float[] var4, int var5) {
        this.gl.glGetCombinerOutputParameterfvNV(var1, var2, var3, var4, var5);
    }

    public void glGetCombinerOutputParameterfvNV(int var1, int var2, int var3, FloatBuffer var4) {
        this.gl.glGetCombinerOutputParameterfvNV(var1, var2, var3, var4);
    }

    public void glGetCombinerOutputParameterivNV(int var1, int var2, int var3, int[] var4, int var5) {
        this.gl.glGetCombinerOutputParameterivNV(var1, var2, var3, var4, var5);
    }

    public void glGetCombinerOutputParameterivNV(int var1, int var2, int var3, IntBuffer var4) {
        this.gl.glGetCombinerOutputParameterivNV(var1, var2, var3, var4);
    }

    public void glGetCombinerStageParameterfvNV(int var1, int var2, float[] var3, int var4) {
        this.gl.glGetCombinerStageParameterfvNV(var1, var2, var3, var4);
    }

    public void glGetCombinerStageParameterfvNV(int var1, int var2, FloatBuffer var3) {
        this.gl.glGetCombinerStageParameterfvNV(var1, var2, var3);
    }

    public void glGetCompressedTexImage(int var1, int var2, Buffer var3) {
        this.gl.glGetCompressedTexImage(var1, var2, var3);
    }

    public void glGetCompressedTexImage(int var1, int var2, long var3) {
        this.gl.glGetCompressedTexImage(var1, var2, var3);
    }

    public void glGetConvolutionFilter(int var1, int var2, int var3, long var4) {
        this.gl.glGetConvolutionFilter(var1, var2, var3, var4);
    }

    public void glGetConvolutionFilter(int var1, int var2, int var3, Buffer var4) {
        this.gl.glGetConvolutionFilter(var1, var2, var3, var4);
    }

    public void glGetConvolutionParameterfv(int var1, int var2, float[] var3, int var4) {
        this.gl.glGetConvolutionParameterfv(var1, var2, var3, var4);
    }

    public void glGetConvolutionParameterfv(int var1, int var2, FloatBuffer var3) {
        this.gl.glGetConvolutionParameterfv(var1, var2, var3);
    }

    public void glGetConvolutionParameteriv(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetConvolutionParameteriv(var1, var2, var3, var4);
    }

    public void glGetConvolutionParameteriv(int var1, int var2, IntBuffer var3) {
        this.gl.glGetConvolutionParameteriv(var1, var2, var3);
    }

    public void glGetDetailTexFuncSGIS(int var1, float[] var2, int var3) {
        this.gl.glGetDetailTexFuncSGIS(var1, var2, var3);
    }

    public void glGetDetailTexFuncSGIS(int var1, FloatBuffer var2) {
        this.gl.glGetDetailTexFuncSGIS(var1, var2);
    }

    public void glGetDoublev(int var1, DoubleBuffer var2) {
        this.gl.glGetDoublev(var1, var2);
    }

    public void glGetDoublev(int var1, double[] var2, int var3) {
        this.gl.glGetDoublev(var1, var2, var3);
    }

    public int glGetError() {
        return this.gl.glGetError();
    }

    public void glGetFenceivNV(int var1, int var2, IntBuffer var3) {
        this.gl.glGetFenceivNV(var1, var2, var3);
    }

    public void glGetFenceivNV(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetFenceivNV(var1, var2, var3, var4);
    }

    public void glGetFinalCombinerInputParameterfvNV(int var1, int var2, FloatBuffer var3) {
        this.gl.glGetFinalCombinerInputParameterfvNV(var1, var2, var3);
    }

    public void glGetFinalCombinerInputParameterfvNV(int var1, int var2, float[] var3, int var4) {
        this.gl.glGetFinalCombinerInputParameterfvNV(var1, var2, var3, var4);
    }

    public void glGetFinalCombinerInputParameterivNV(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetFinalCombinerInputParameterivNV(var1, var2, var3, var4);
    }

    public void glGetFinalCombinerInputParameterivNV(int var1, int var2, IntBuffer var3) {
        this.gl.glGetFinalCombinerInputParameterivNV(var1, var2, var3);
    }

    public void glGetFloatv(int var1, float[] var2, int var3) {
        this.gl.glGetFloatv(var1, var2, var3);
    }

    public void glGetFloatv(int var1, FloatBuffer var2) {
        this.gl.glGetFloatv(var1, var2);
    }

    public void glGetFogFuncSGIS(float[] var1, int var2) {
        this.gl.glGetFogFuncSGIS(var1, var2);
    }

    public void glGetFogFuncSGIS(FloatBuffer var1) {
        this.gl.glGetFogFuncSGIS(var1);
    }

    public int glGetFragDataLocationEXT(int var1, byte[] var2, int var3) {
        return this.gl.glGetFragDataLocationEXT(var1, var2, var3);
    }

    public int glGetFragDataLocationEXT(int var1, ByteBuffer var2) {
        return this.gl.glGetFragDataLocationEXT(var1, var2);
    }

    public void glGetFragmentLightfvSGIX(int var1, int var2, float[] var3, int var4) {
        this.gl.glGetFragmentLightfvSGIX(var1, var2, var3, var4);
    }

    public void glGetFragmentLightfvSGIX(int var1, int var2, FloatBuffer var3) {
        this.gl.glGetFragmentLightfvSGIX(var1, var2, var3);
    }

    public void glGetFragmentLightivSGIX(int var1, int var2, IntBuffer var3) {
        this.gl.glGetFragmentLightivSGIX(var1, var2, var3);
    }

    public void glGetFragmentLightivSGIX(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetFragmentLightivSGIX(var1, var2, var3, var4);
    }

    public void glGetFragmentMaterialfvSGIX(int var1, int var2, float[] var3, int var4) {
        this.gl.glGetFragmentMaterialfvSGIX(var1, var2, var3, var4);
    }

    public void glGetFragmentMaterialfvSGIX(int var1, int var2, FloatBuffer var3) {
        this.gl.glGetFragmentMaterialfvSGIX(var1, var2, var3);
    }

    public void glGetFragmentMaterialivSGIX(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetFragmentMaterialivSGIX(var1, var2, var3, var4);
    }

    public void glGetFragmentMaterialivSGIX(int var1, int var2, IntBuffer var3) {
        this.gl.glGetFragmentMaterialivSGIX(var1, var2, var3);
    }

    public void glGetFramebufferAttachmentParameterivEXT(int var1, int var2, int var3, int[] var4, int var5) {
        this.gl.glGetFramebufferAttachmentParameterivEXT(var1, var2, var3, var4, var5);
    }

    public void glGetFramebufferAttachmentParameterivEXT(int var1, int var2, int var3, IntBuffer var4) {
        this.gl.glGetFramebufferAttachmentParameterivEXT(var1, var2, var3, var4);
    }

    public int glGetHandleARB(int var1) {
        return this.gl.glGetHandleARB(var1);
    }

    public void glGetHistogram(int var1, boolean var2, int var3, int var4, Buffer var5) {
        this.gl.glGetHistogram(var1, var2, var3, var4, var5);
    }

    public void glGetHistogram(int var1, boolean var2, int var3, int var4, long var5) {
        this.gl.glGetHistogram(var1, var2, var3, var4, var5);
    }

    public void glGetHistogramParameterfv(int var1, int var2, float[] var3, int var4) {
        this.gl.glGetHistogramParameterfv(var1, var2, var3, var4);
    }

    public void glGetHistogramParameterfv(int var1, int var2, FloatBuffer var3) {
        this.gl.glGetHistogramParameterfv(var1, var2, var3);
    }

    public void glGetHistogramParameteriv(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetHistogramParameteriv(var1, var2, var3, var4);
    }

    public void glGetHistogramParameteriv(int var1, int var2, IntBuffer var3) {
        this.gl.glGetHistogramParameteriv(var1, var2, var3);
    }

    public void glGetImageTransformParameterfvHP(int var1, int var2, float[] var3, int var4) {
        this.gl.glGetImageTransformParameterfvHP(var1, var2, var3, var4);
    }

    public void glGetImageTransformParameterfvHP(int var1, int var2, FloatBuffer var3) {
        this.gl.glGetImageTransformParameterfvHP(var1, var2, var3);
    }

    public void glGetImageTransformParameterivHP(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetImageTransformParameterivHP(var1, var2, var3, var4);
    }

    public void glGetImageTransformParameterivHP(int var1, int var2, IntBuffer var3) {
        this.gl.glGetImageTransformParameterivHP(var1, var2, var3);
    }

    public void glGetInfoLogARB(int var1, int var2, int[] var3, int var4, byte[] var5, int var6) {
        this.gl.glGetInfoLogARB(var1, var2, var3, var4, var5, var6);
    }

    public void glGetInfoLogARB(int var1, int var2, IntBuffer var3, ByteBuffer var4) {
        this.gl.glGetInfoLogARB(var1, var2, var3, var4);
    }

    public int glGetInstrumentsSGIX() {
        return this.gl.glGetInstrumentsSGIX();
    }

    public void glGetIntegerIndexedvEXT(int var1, int var2, IntBuffer var3) {
        this.gl.glGetIntegerIndexedvEXT(var1, var2, var3);
    }

    public void glGetIntegerIndexedvEXT(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetIntegerIndexedvEXT(var1, var2, var3, var4);
    }

    public void glGetIntegerv(int var1, int[] var2, int var3) {
        this.gl.glGetIntegerv(var1, var2, var3);
    }

    public void glGetIntegerv(int var1, IntBuffer var2) {
        this.gl.glGetIntegerv(var1, var2);
    }

    public void glGetInvariantBooleanvEXT(int var1, int var2, byte[] var3, int var4) {
        this.gl.glGetInvariantBooleanvEXT(var1, var2, var3, var4);
    }

    public void glGetInvariantBooleanvEXT(int var1, int var2, ByteBuffer var3) {
        this.gl.glGetInvariantBooleanvEXT(var1, var2, var3);
    }

    public void glGetInvariantFloatvEXT(int var1, int var2, FloatBuffer var3) {
        this.gl.glGetInvariantFloatvEXT(var1, var2, var3);
    }

    public void glGetInvariantFloatvEXT(int var1, int var2, float[] var3, int var4) {
        this.gl.glGetInvariantFloatvEXT(var1, var2, var3, var4);
    }

    public void glGetInvariantIntegervEXT(int var1, int var2, IntBuffer var3) {
        this.gl.glGetInvariantIntegervEXT(var1, var2, var3);
    }

    public void glGetInvariantIntegervEXT(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetInvariantIntegervEXT(var1, var2, var3, var4);
    }

    public void glGetLightfv(int var1, int var2, FloatBuffer var3) {
        this.gl.glGetLightfv(var1, var2, var3);
    }

    public void glGetLightfv(int var1, int var2, float[] var3, int var4) {
        this.gl.glGetLightfv(var1, var2, var3, var4);
    }

    public void glGetLightiv(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetLightiv(var1, var2, var3, var4);
    }

    public void glGetLightiv(int var1, int var2, IntBuffer var3) {
        this.gl.glGetLightiv(var1, var2, var3);
    }

    public void glGetListParameterfvSGIX(int var1, int var2, float[] var3, int var4) {
        this.gl.glGetListParameterfvSGIX(var1, var2, var3, var4);
    }

    public void glGetListParameterfvSGIX(int var1, int var2, FloatBuffer var3) {
        this.gl.glGetListParameterfvSGIX(var1, var2, var3);
    }

    public void glGetListParameterivSGIX(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetListParameterivSGIX(var1, var2, var3, var4);
    }

    public void glGetListParameterivSGIX(int var1, int var2, IntBuffer var3) {
        this.gl.glGetListParameterivSGIX(var1, var2, var3);
    }

    public void glGetLocalConstantBooleanvEXT(int var1, int var2, byte[] var3, int var4) {
        this.gl.glGetLocalConstantBooleanvEXT(var1, var2, var3, var4);
    }

    public void glGetLocalConstantBooleanvEXT(int var1, int var2, ByteBuffer var3) {
        this.gl.glGetLocalConstantBooleanvEXT(var1, var2, var3);
    }

    public void glGetLocalConstantFloatvEXT(int var1, int var2, float[] var3, int var4) {
        this.gl.glGetLocalConstantFloatvEXT(var1, var2, var3, var4);
    }

    public void glGetLocalConstantFloatvEXT(int var1, int var2, FloatBuffer var3) {
        this.gl.glGetLocalConstantFloatvEXT(var1, var2, var3);
    }

    public void glGetLocalConstantIntegervEXT(int var1, int var2, IntBuffer var3) {
        this.gl.glGetLocalConstantIntegervEXT(var1, var2, var3);
    }

    public void glGetLocalConstantIntegervEXT(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetLocalConstantIntegervEXT(var1, var2, var3, var4);
    }

    public void glGetMapAttribParameterfvNV(int var1, int var2, int var3, FloatBuffer var4) {
        this.gl.glGetMapAttribParameterfvNV(var1, var2, var3, var4);
    }

    public void glGetMapAttribParameterfvNV(int var1, int var2, int var3, float[] var4, int var5) {
        this.gl.glGetMapAttribParameterfvNV(var1, var2, var3, var4, var5);
    }

    public void glGetMapAttribParameterivNV(int var1, int var2, int var3, int[] var4, int var5) {
        this.gl.glGetMapAttribParameterivNV(var1, var2, var3, var4, var5);
    }

    public void glGetMapAttribParameterivNV(int var1, int var2, int var3, IntBuffer var4) {
        this.gl.glGetMapAttribParameterivNV(var1, var2, var3, var4);
    }

    public void glGetMapControlPointsNV(int var1, int var2, int var3, int var4, int var5, boolean var6, Buffer var7) {
        this.gl.glGetMapControlPointsNV(var1, var2, var3, var4, var5, var6, var7);
    }

    public void glGetMapParameterfvNV(int var1, int var2, FloatBuffer var3) {
        this.gl.glGetMapParameterfvNV(var1, var2, var3);
    }

    public void glGetMapParameterfvNV(int var1, int var2, float[] var3, int var4) {
        this.gl.glGetMapParameterfvNV(var1, var2, var3, var4);
    }

    public void glGetMapParameterivNV(int var1, int var2, IntBuffer var3) {
        this.gl.glGetMapParameterivNV(var1, var2, var3);
    }

    public void glGetMapParameterivNV(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetMapParameterivNV(var1, var2, var3, var4);
    }

    public void glGetMapdv(int var1, int var2, double[] var3, int var4) {
        this.gl.glGetMapdv(var1, var2, var3, var4);
    }

    public void glGetMapdv(int var1, int var2, DoubleBuffer var3) {
        this.gl.glGetMapdv(var1, var2, var3);
    }

    public void glGetMapfv(int var1, int var2, float[] var3, int var4) {
        this.gl.glGetMapfv(var1, var2, var3, var4);
    }

    public void glGetMapfv(int var1, int var2, FloatBuffer var3) {
        this.gl.glGetMapfv(var1, var2, var3);
    }

    public void glGetMapiv(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetMapiv(var1, var2, var3, var4);
    }

    public void glGetMapiv(int var1, int var2, IntBuffer var3) {
        this.gl.glGetMapiv(var1, var2, var3);
    }

    public void glGetMaterialfv(int var1, int var2, FloatBuffer var3) {
        this.gl.glGetMaterialfv(var1, var2, var3);
    }

    public void glGetMaterialfv(int var1, int var2, float[] var3, int var4) {
        this.gl.glGetMaterialfv(var1, var2, var3, var4);
    }

    public void glGetMaterialiv(int var1, int var2, IntBuffer var3) {
        this.gl.glGetMaterialiv(var1, var2, var3);
    }

    public void glGetMaterialiv(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetMaterialiv(var1, var2, var3, var4);
    }

    public void glGetMinmax(int var1, boolean var2, int var3, int var4, long var5) {
        this.gl.glGetMinmax(var1, var2, var3, var4, var5);
    }

    public void glGetMinmax(int var1, boolean var2, int var3, int var4, Buffer var5) {
        this.gl.glGetMinmax(var1, var2, var3, var4, var5);
    }

    public void glGetMinmaxParameterfv(int var1, int var2, float[] var3, int var4) {
        this.gl.glGetMinmaxParameterfv(var1, var2, var3, var4);
    }

    public void glGetMinmaxParameterfv(int var1, int var2, FloatBuffer var3) {
        this.gl.glGetMinmaxParameterfv(var1, var2, var3);
    }

    public void glGetMinmaxParameteriv(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetMinmaxParameteriv(var1, var2, var3, var4);
    }

    public void glGetMinmaxParameteriv(int var1, int var2, IntBuffer var3) {
        this.gl.glGetMinmaxParameteriv(var1, var2, var3);
    }

    public void glGetObjectBufferfvATI(int var1, int var2, FloatBuffer var3) {
        this.gl.glGetObjectBufferfvATI(var1, var2, var3);
    }

    public void glGetObjectBufferfvATI(int var1, int var2, float[] var3, int var4) {
        this.gl.glGetObjectBufferfvATI(var1, var2, var3, var4);
    }

    public void glGetObjectBufferivATI(int var1, int var2, IntBuffer var3) {
        this.gl.glGetObjectBufferivATI(var1, var2, var3);
    }

    public void glGetObjectBufferivATI(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetObjectBufferivATI(var1, var2, var3, var4);
    }

    public void glGetObjectParameterfvARB(int var1, int var2, float[] var3, int var4) {
        this.gl.glGetObjectParameterfvARB(var1, var2, var3, var4);
    }

    public void glGetObjectParameterfvARB(int var1, int var2, FloatBuffer var3) {
        this.gl.glGetObjectParameterfvARB(var1, var2, var3);
    }

    public void glGetObjectParameterivARB(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetObjectParameterivARB(var1, var2, var3, var4);
    }

    public void glGetObjectParameterivARB(int var1, int var2, IntBuffer var3) {
        this.gl.glGetObjectParameterivARB(var1, var2, var3);
    }

    public void glGetOcclusionQueryivNV(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetOcclusionQueryivNV(var1, var2, var3, var4);
    }

    public void glGetOcclusionQueryivNV(int var1, int var2, IntBuffer var3) {
        this.gl.glGetOcclusionQueryivNV(var1, var2, var3);
    }

    public void glGetOcclusionQueryuivNV(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetOcclusionQueryuivNV(var1, var2, var3, var4);
    }

    public void glGetOcclusionQueryuivNV(int var1, int var2, IntBuffer var3) {
        this.gl.glGetOcclusionQueryuivNV(var1, var2, var3);
    }

    public void glGetPixelMapfv(int var1, long var2) {
        this.gl.glGetPixelMapfv(var1, var2);
    }

    public void glGetPixelMapfv(int var1, float[] var2, int var3) {
        this.gl.glGetPixelMapfv(var1, var2, var3);
    }

    public void glGetPixelMapfv(int var1, FloatBuffer var2) {
        this.gl.glGetPixelMapfv(var1, var2);
    }

    public void glGetPixelMapuiv(int var1, IntBuffer var2) {
        this.gl.glGetPixelMapuiv(var1, var2);
    }

    public void glGetPixelMapuiv(int var1, int[] var2, int var3) {
        this.gl.glGetPixelMapuiv(var1, var2, var3);
    }

    public void glGetPixelMapuiv(int var1, long var2) {
        this.gl.glGetPixelMapuiv(var1, var2);
    }

    public void glGetPixelMapusv(int var1, long var2) {
        this.gl.glGetPixelMapusv(var1, var2);
    }

    public void glGetPixelMapusv(int var1, short[] var2, int var3) {
        this.gl.glGetPixelMapusv(var1, var2, var3);
    }

    public void glGetPixelMapusv(int var1, ShortBuffer var2) {
        this.gl.glGetPixelMapusv(var1, var2);
    }

    public void glGetPixelTexGenParameterfvSGIS(int var1, FloatBuffer var2) {
        this.gl.glGetPixelTexGenParameterfvSGIS(var1, var2);
    }

    public void glGetPixelTexGenParameterfvSGIS(int var1, float[] var2, int var3) {
        this.gl.glGetPixelTexGenParameterfvSGIS(var1, var2, var3);
    }

    public void glGetPixelTexGenParameterivSGIS(int var1, IntBuffer var2) {
        this.gl.glGetPixelTexGenParameterivSGIS(var1, var2);
    }

    public void glGetPixelTexGenParameterivSGIS(int var1, int[] var2, int var3) {
        this.gl.glGetPixelTexGenParameterivSGIS(var1, var2, var3);
    }

    public void glGetPolygonStipple(long var1) {
        this.gl.glGetPolygonStipple(var1);
    }

    public void glGetPolygonStipple(byte[] var1, int var2) {
        this.gl.glGetPolygonStipple(var1, var2);
    }

    public void glGetPolygonStipple(ByteBuffer var1) {
        this.gl.glGetPolygonStipple(var1);
    }

    public void glGetProgramEnvParameterIivNV(int var1, int var2, IntBuffer var3) {
        this.gl.glGetProgramEnvParameterIivNV(var1, var2, var3);
    }

    public void glGetProgramEnvParameterIivNV(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetProgramEnvParameterIivNV(var1, var2, var3, var4);
    }

    public void glGetProgramEnvParameterIuivNV(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetProgramEnvParameterIuivNV(var1, var2, var3, var4);
    }

    public void glGetProgramEnvParameterIuivNV(int var1, int var2, IntBuffer var3) {
        this.gl.glGetProgramEnvParameterIuivNV(var1, var2, var3);
    }

    public void glGetProgramEnvParameterdvARB(int var1, int var2, double[] var3, int var4) {
        this.gl.glGetProgramEnvParameterdvARB(var1, var2, var3, var4);
    }

    public void glGetProgramEnvParameterdvARB(int var1, int var2, DoubleBuffer var3) {
        this.gl.glGetProgramEnvParameterdvARB(var1, var2, var3);
    }

    public void glGetProgramEnvParameterfvARB(int var1, int var2, FloatBuffer var3) {
        this.gl.glGetProgramEnvParameterfvARB(var1, var2, var3);
    }

    public void glGetProgramEnvParameterfvARB(int var1, int var2, float[] var3, int var4) {
        this.gl.glGetProgramEnvParameterfvARB(var1, var2, var3, var4);
    }

    public void glGetProgramInfoLog(int var1, int var2, IntBuffer var3, ByteBuffer var4) {
        this.gl.glGetProgramInfoLog(var1, var2, var3, var4);
    }

    public void glGetProgramInfoLog(int var1, int var2, int[] var3, int var4, byte[] var5, int var6) {
        this.gl.glGetProgramInfoLog(var1, var2, var3, var4, var5, var6);
    }

    public void glGetProgramLocalParameterIivNV(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetProgramLocalParameterIivNV(var1, var2, var3, var4);
    }

    public void glGetProgramLocalParameterIivNV(int var1, int var2, IntBuffer var3) {
        this.gl.glGetProgramLocalParameterIivNV(var1, var2, var3);
    }

    public void glGetProgramLocalParameterIuivNV(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetProgramLocalParameterIuivNV(var1, var2, var3, var4);
    }

    public void glGetProgramLocalParameterIuivNV(int var1, int var2, IntBuffer var3) {
        this.gl.glGetProgramLocalParameterIuivNV(var1, var2, var3);
    }

    public void glGetProgramLocalParameterdvARB(int var1, int var2, double[] var3, int var4) {
        this.gl.glGetProgramLocalParameterdvARB(var1, var2, var3, var4);
    }

    public void glGetProgramLocalParameterdvARB(int var1, int var2, DoubleBuffer var3) {
        this.gl.glGetProgramLocalParameterdvARB(var1, var2, var3);
    }

    public void glGetProgramLocalParameterfvARB(int var1, int var2, float[] var3, int var4) {
        this.gl.glGetProgramLocalParameterfvARB(var1, var2, var3, var4);
    }

    public void glGetProgramLocalParameterfvARB(int var1, int var2, FloatBuffer var3) {
        this.gl.glGetProgramLocalParameterfvARB(var1, var2, var3);
    }

    public void glGetProgramNamedParameterdvNV(int var1, int var2, String var3, double[] var4, int var5) {
        this.gl.glGetProgramNamedParameterdvNV(var1, var2, var3, var4, var5);
    }

    public void glGetProgramNamedParameterdvNV(int var1, int var2, String var3, DoubleBuffer var4) {
        this.gl.glGetProgramNamedParameterdvNV(var1, var2, var3, var4);
    }

    public void glGetProgramNamedParameterfvNV(int var1, int var2, String var3, float[] var4, int var5) {
        this.gl.glGetProgramNamedParameterfvNV(var1, var2, var3, var4, var5);
    }

    public void glGetProgramNamedParameterfvNV(int var1, int var2, String var3, FloatBuffer var4) {
        this.gl.glGetProgramNamedParameterfvNV(var1, var2, var3, var4);
    }

    public void glGetProgramParameterdvNV(int var1, int var2, int var3, double[] var4, int var5) {
        this.gl.glGetProgramParameterdvNV(var1, var2, var3, var4, var5);
    }

    public void glGetProgramParameterdvNV(int var1, int var2, int var3, DoubleBuffer var4) {
        this.gl.glGetProgramParameterdvNV(var1, var2, var3, var4);
    }

    public void glGetProgramParameterfvNV(int var1, int var2, int var3, FloatBuffer var4) {
        this.gl.glGetProgramParameterfvNV(var1, var2, var3, var4);
    }

    public void glGetProgramParameterfvNV(int var1, int var2, int var3, float[] var4, int var5) {
        this.gl.glGetProgramParameterfvNV(var1, var2, var3, var4, var5);
    }

    public void glGetProgramStringARB(int var1, int var2, Buffer var3) {
        this.gl.glGetProgramStringARB(var1, var2, var3);
    }

    public void glGetProgramStringNV(int var1, int var2, byte[] var3, int var4) {
        this.gl.glGetProgramStringNV(var1, var2, var3, var4);
    }

    public void glGetProgramStringNV(int var1, int var2, ByteBuffer var3) {
        this.gl.glGetProgramStringNV(var1, var2, var3);
    }

    public void glGetProgramiv(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetProgramiv(var1, var2, var3, var4);
    }

    public void glGetProgramiv(int var1, int var2, IntBuffer var3) {
        this.gl.glGetProgramiv(var1, var2, var3);
    }

    public void glGetProgramivARB(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetProgramivARB(var1, var2, var3, var4);
    }

    public void glGetProgramivARB(int var1, int var2, IntBuffer var3) {
        this.gl.glGetProgramivARB(var1, var2, var3);
    }

    public void glGetProgramivNV(int var1, int var2, IntBuffer var3) {
        this.gl.glGetProgramivNV(var1, var2, var3);
    }

    public void glGetProgramivNV(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetProgramivNV(var1, var2, var3, var4);
    }

    public void glGetQueryObjecti64vEXT(int var1, int var2, LongBuffer var3) {
        this.gl.glGetQueryObjecti64vEXT(var1, var2, var3);
    }

    public void glGetQueryObjecti64vEXT(int var1, int var2, long[] var3, int var4) {
        this.gl.glGetQueryObjecti64vEXT(var1, var2, var3, var4);
    }

    public void glGetQueryObjectiv(int var1, int var2, IntBuffer var3) {
        this.gl.glGetQueryObjectiv(var1, var2, var3);
    }

    public void glGetQueryObjectiv(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetQueryObjectiv(var1, var2, var3, var4);
    }

    public void glGetQueryObjectivARB(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetQueryObjectivARB(var1, var2, var3, var4);
    }

    public void glGetQueryObjectivARB(int var1, int var2, IntBuffer var3) {
        this.gl.glGetQueryObjectivARB(var1, var2, var3);
    }

    public void glGetQueryObjectui64vEXT(int var1, int var2, long[] var3, int var4) {
        this.gl.glGetQueryObjectui64vEXT(var1, var2, var3, var4);
    }

    public void glGetQueryObjectui64vEXT(int var1, int var2, LongBuffer var3) {
        this.gl.glGetQueryObjectui64vEXT(var1, var2, var3);
    }

    public void glGetQueryObjectuiv(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetQueryObjectuiv(var1, var2, var3, var4);
    }

    public void glGetQueryObjectuiv(int var1, int var2, IntBuffer var3) {
        this.gl.glGetQueryObjectuiv(var1, var2, var3);
    }

    public void glGetQueryObjectuivARB(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetQueryObjectuivARB(var1, var2, var3, var4);
    }

    public void glGetQueryObjectuivARB(int var1, int var2, IntBuffer var3) {
        this.gl.glGetQueryObjectuivARB(var1, var2, var3);
    }

    public void glGetQueryiv(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetQueryiv(var1, var2, var3, var4);
    }

    public void glGetQueryiv(int var1, int var2, IntBuffer var3) {
        this.gl.glGetQueryiv(var1, var2, var3);
    }

    public void glGetQueryivARB(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetQueryivARB(var1, var2, var3, var4);
    }

    public void glGetQueryivARB(int var1, int var2, IntBuffer var3) {
        this.gl.glGetQueryivARB(var1, var2, var3);
    }

    public void glGetRenderbufferParameterivEXT(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetRenderbufferParameterivEXT(var1, var2, var3, var4);
    }

    public void glGetRenderbufferParameterivEXT(int var1, int var2, IntBuffer var3) {
        this.gl.glGetRenderbufferParameterivEXT(var1, var2, var3);
    }

    public void glGetSeparableFilter(int var1, int var2, int var3, Buffer var4, Buffer var5, Buffer var6) {
        this.gl.glGetSeparableFilter(var1, var2, var3, var4, var5, var6);
    }

    public void glGetSeparableFilter(int var1, int var2, int var3, long var4, long var6, long var8) {
        this.gl.glGetSeparableFilter(var1, var2, var3, var4, var6, var8);
    }

    public void glGetShaderInfoLog(int var1, int var2, int[] var3, int var4, byte[] var5, int var6) {
        this.gl.glGetShaderInfoLog(var1, var2, var3, var4, var5, var6);
    }

    public void glGetShaderInfoLog(int var1, int var2, IntBuffer var3, ByteBuffer var4) {
        this.gl.glGetShaderInfoLog(var1, var2, var3, var4);
    }

    public void glGetShaderSource(int var1, int var2, IntBuffer var3, ByteBuffer var4) {
        this.gl.glGetShaderSource(var1, var2, var3, var4);
    }

    public void glGetShaderSource(int var1, int var2, int[] var3, int var4, byte[] var5, int var6) {
        this.gl.glGetShaderSource(var1, var2, var3, var4, var5, var6);
    }

    public void glGetShaderSourceARB(int var1, int var2, int[] var3, int var4, byte[] var5, int var6) {
        this.gl.glGetShaderSourceARB(var1, var2, var3, var4, var5, var6);
    }

    public void glGetShaderSourceARB(int var1, int var2, IntBuffer var3, ByteBuffer var4) {
        this.gl.glGetShaderSourceARB(var1, var2, var3, var4);
    }

    public void glGetShaderiv(int var1, int var2, IntBuffer var3) {
        this.gl.glGetShaderiv(var1, var2, var3);
    }

    public void glGetShaderiv(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetShaderiv(var1, var2, var3, var4);
    }

    public void glGetSharpenTexFuncSGIS(int var1, float[] var2, int var3) {
        this.gl.glGetSharpenTexFuncSGIS(var1, var2, var3);
    }

    public void glGetSharpenTexFuncSGIS(int var1, FloatBuffer var2) {
        this.gl.glGetSharpenTexFuncSGIS(var1, var2);
    }

    public String glGetString(int var1) {
        return this.gl.glGetString(var1);
    }

    public void glGetTexBumpParameterfvATI(int var1, float[] var2, int var3) {
        this.gl.glGetTexBumpParameterfvATI(var1, var2, var3);
    }

    public void glGetTexBumpParameterfvATI(int var1, FloatBuffer var2) {
        this.gl.glGetTexBumpParameterfvATI(var1, var2);
    }

    public void glGetTexBumpParameterivATI(int var1, int[] var2, int var3) {
        this.gl.glGetTexBumpParameterivATI(var1, var2, var3);
    }

    public void glGetTexBumpParameterivATI(int var1, IntBuffer var2) {
        this.gl.glGetTexBumpParameterivATI(var1, var2);
    }

    public void glGetTexEnvfv(int var1, int var2, FloatBuffer var3) {
        this.gl.glGetTexEnvfv(var1, var2, var3);
    }

    public void glGetTexEnvfv(int var1, int var2, float[] var3, int var4) {
        this.gl.glGetTexEnvfv(var1, var2, var3, var4);
    }

    public void glGetTexEnviv(int var1, int var2, IntBuffer var3) {
        this.gl.glGetTexEnviv(var1, var2, var3);
    }

    public void glGetTexEnviv(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetTexEnviv(var1, var2, var3, var4);
    }

    public void glGetTexFilterFuncSGIS(int var1, int var2, FloatBuffer var3) {
        this.gl.glGetTexFilterFuncSGIS(var1, var2, var3);
    }

    public void glGetTexFilterFuncSGIS(int var1, int var2, float[] var3, int var4) {
        this.gl.glGetTexFilterFuncSGIS(var1, var2, var3, var4);
    }

    public void glGetTexGendv(int var1, int var2, double[] var3, int var4) {
        this.gl.glGetTexGendv(var1, var2, var3, var4);
    }

    public void glGetTexGendv(int var1, int var2, DoubleBuffer var3) {
        this.gl.glGetTexGendv(var1, var2, var3);
    }

    public void glGetTexGenfv(int var1, int var2, float[] var3, int var4) {
        this.gl.glGetTexGenfv(var1, var2, var3, var4);
    }

    public void glGetTexGenfv(int var1, int var2, FloatBuffer var3) {
        this.gl.glGetTexGenfv(var1, var2, var3);
    }

    public void glGetTexGeniv(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetTexGeniv(var1, var2, var3, var4);
    }

    public void glGetTexGeniv(int var1, int var2, IntBuffer var3) {
        this.gl.glGetTexGeniv(var1, var2, var3);
    }

    public void glGetTexImage(int var1, int var2, int var3, int var4, Buffer var5) {
        this.gl.glGetTexImage(var1, var2, var3, var4, var5);
    }

    public void glGetTexImage(int var1, int var2, int var3, int var4, long var5) {
        this.gl.glGetTexImage(var1, var2, var3, var4, var5);
    }

    public void glGetTexLevelParameterfv(int var1, int var2, int var3, float[] var4, int var5) {
        this.gl.glGetTexLevelParameterfv(var1, var2, var3, var4, var5);
    }

    public void glGetTexLevelParameterfv(int var1, int var2, int var3, FloatBuffer var4) {
        this.gl.glGetTexLevelParameterfv(var1, var2, var3, var4);
    }

    public void glGetTexLevelParameteriv(int var1, int var2, int var3, IntBuffer var4) {
        this.gl.glGetTexLevelParameteriv(var1, var2, var3, var4);
    }

    public void glGetTexLevelParameteriv(int var1, int var2, int var3, int[] var4, int var5) {
        this.gl.glGetTexLevelParameteriv(var1, var2, var3, var4, var5);
    }

    public void glGetTexParameterIivEXT(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetTexParameterIivEXT(var1, var2, var3, var4);
    }

    public void glGetTexParameterIivEXT(int var1, int var2, IntBuffer var3) {
        this.gl.glGetTexParameterIivEXT(var1, var2, var3);
    }

    public void glGetTexParameterIuivEXT(int var1, int var2, IntBuffer var3) {
        this.gl.glGetTexParameterIuivEXT(var1, var2, var3);
    }

    public void glGetTexParameterIuivEXT(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetTexParameterIuivEXT(var1, var2, var3, var4);
    }

    public void glGetTexParameterfv(int var1, int var2, float[] var3, int var4) {
        this.gl.glGetTexParameterfv(var1, var2, var3, var4);
    }

    public void glGetTexParameterfv(int var1, int var2, FloatBuffer var3) {
        this.gl.glGetTexParameterfv(var1, var2, var3);
    }

    public void glGetTexParameteriv(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetTexParameteriv(var1, var2, var3, var4);
    }

    public void glGetTexParameteriv(int var1, int var2, IntBuffer var3) {
        this.gl.glGetTexParameteriv(var1, var2, var3);
    }

    public void glGetTrackMatrixivNV(int var1, int var2, int var3, int[] var4, int var5) {
        this.gl.glGetTrackMatrixivNV(var1, var2, var3, var4, var5);
    }

    public void glGetTrackMatrixivNV(int var1, int var2, int var3, IntBuffer var4) {
        this.gl.glGetTrackMatrixivNV(var1, var2, var3, var4);
    }

    public void glGetTransformFeedbackVaryingNV(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetTransformFeedbackVaryingNV(var1, var2, var3, var4);
    }

    public void glGetTransformFeedbackVaryingNV(int var1, int var2, IntBuffer var3) {
        this.gl.glGetTransformFeedbackVaryingNV(var1, var2, var3);
    }

    public int glGetUniformBufferSizeEXT(int var1, int var2) {
        return this.gl.glGetUniformBufferSizeEXT(var1, var2);
    }

    public int glGetUniformLocation(int var1, String var2) {
        return this.gl.glGetUniformLocation(var1, var2);
    }

    public int glGetUniformLocationARB(int var1, String var2) {
        return this.gl.glGetUniformLocationARB(var1, var2);
    }

    public int glGetUniformOffsetEXT(int var1, int var2) {
        return this.gl.glGetUniformOffsetEXT(var1, var2);
    }

    public void glGetUniformfv(int var1, int var2, float[] var3, int var4) {
        this.gl.glGetUniformfv(var1, var2, var3, var4);
    }

    public void glGetUniformfv(int var1, int var2, FloatBuffer var3) {
        this.gl.glGetUniformfv(var1, var2, var3);
    }

    public void glGetUniformfvARB(int var1, int var2, float[] var3, int var4) {
        this.gl.glGetUniformfvARB(var1, var2, var3, var4);
    }

    public void glGetUniformfvARB(int var1, int var2, FloatBuffer var3) {
        this.gl.glGetUniformfvARB(var1, var2, var3);
    }

    public void glGetUniformiv(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetUniformiv(var1, var2, var3, var4);
    }

    public void glGetUniformiv(int var1, int var2, IntBuffer var3) {
        this.gl.glGetUniformiv(var1, var2, var3);
    }

    public void glGetUniformivARB(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetUniformivARB(var1, var2, var3, var4);
    }

    public void glGetUniformivARB(int var1, int var2, IntBuffer var3) {
        this.gl.glGetUniformivARB(var1, var2, var3);
    }

    public void glGetUniformuivEXT(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetUniformuivEXT(var1, var2, var3, var4);
    }

    public void glGetUniformuivEXT(int var1, int var2, IntBuffer var3) {
        this.gl.glGetUniformuivEXT(var1, var2, var3);
    }

    public void glGetVariantArrayObjectfvATI(int var1, int var2, FloatBuffer var3) {
        this.gl.glGetVariantArrayObjectfvATI(var1, var2, var3);
    }

    public void glGetVariantArrayObjectfvATI(int var1, int var2, float[] var3, int var4) {
        this.gl.glGetVariantArrayObjectfvATI(var1, var2, var3, var4);
    }

    public void glGetVariantArrayObjectivATI(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetVariantArrayObjectivATI(var1, var2, var3, var4);
    }

    public void glGetVariantArrayObjectivATI(int var1, int var2, IntBuffer var3) {
        this.gl.glGetVariantArrayObjectivATI(var1, var2, var3);
    }

    public void glGetVariantBooleanvEXT(int var1, int var2, ByteBuffer var3) {
        this.gl.glGetVariantBooleanvEXT(var1, var2, var3);
    }

    public void glGetVariantBooleanvEXT(int var1, int var2, byte[] var3, int var4) {
        this.gl.glGetVariantBooleanvEXT(var1, var2, var3, var4);
    }

    public void glGetVariantFloatvEXT(int var1, int var2, float[] var3, int var4) {
        this.gl.glGetVariantFloatvEXT(var1, var2, var3, var4);
    }

    public void glGetVariantFloatvEXT(int var1, int var2, FloatBuffer var3) {
        this.gl.glGetVariantFloatvEXT(var1, var2, var3);
    }

    public void glGetVariantIntegervEXT(int var1, int var2, IntBuffer var3) {
        this.gl.glGetVariantIntegervEXT(var1, var2, var3);
    }

    public void glGetVariantIntegervEXT(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetVariantIntegervEXT(var1, var2, var3, var4);
    }

    public int glGetVaryingLocationNV(int var1, byte[] var2, int var3) {
        return this.gl.glGetVaryingLocationNV(var1, var2, var3);
    }

    public int glGetVaryingLocationNV(int var1, ByteBuffer var2) {
        return this.gl.glGetVaryingLocationNV(var1, var2);
    }

    public void glGetVertexAttribArrayObjectfvATI(int var1, int var2, float[] var3, int var4) {
        this.gl.glGetVertexAttribArrayObjectfvATI(var1, var2, var3, var4);
    }

    public void glGetVertexAttribArrayObjectfvATI(int var1, int var2, FloatBuffer var3) {
        this.gl.glGetVertexAttribArrayObjectfvATI(var1, var2, var3);
    }

    public void glGetVertexAttribArrayObjectivATI(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetVertexAttribArrayObjectivATI(var1, var2, var3, var4);
    }

    public void glGetVertexAttribArrayObjectivATI(int var1, int var2, IntBuffer var3) {
        this.gl.glGetVertexAttribArrayObjectivATI(var1, var2, var3);
    }

    public void glGetVertexAttribIivEXT(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetVertexAttribIivEXT(var1, var2, var3, var4);
    }

    public void glGetVertexAttribIivEXT(int var1, int var2, IntBuffer var3) {
        this.gl.glGetVertexAttribIivEXT(var1, var2, var3);
    }

    public void glGetVertexAttribIuivEXT(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetVertexAttribIuivEXT(var1, var2, var3, var4);
    }

    public void glGetVertexAttribIuivEXT(int var1, int var2, IntBuffer var3) {
        this.gl.glGetVertexAttribIuivEXT(var1, var2, var3);
    }

    public void glGetVertexAttribdv(int var1, int var2, DoubleBuffer var3) {
        this.gl.glGetVertexAttribdv(var1, var2, var3);
    }

    public void glGetVertexAttribdv(int var1, int var2, double[] var3, int var4) {
        this.gl.glGetVertexAttribdv(var1, var2, var3, var4);
    }

    public void glGetVertexAttribdvARB(int var1, int var2, DoubleBuffer var3) {
        this.gl.glGetVertexAttribdvARB(var1, var2, var3);
    }

    public void glGetVertexAttribdvARB(int var1, int var2, double[] var3, int var4) {
        this.gl.glGetVertexAttribdvARB(var1, var2, var3, var4);
    }

    public void glGetVertexAttribdvNV(int var1, int var2, double[] var3, int var4) {
        this.gl.glGetVertexAttribdvNV(var1, var2, var3, var4);
    }

    public void glGetVertexAttribdvNV(int var1, int var2, DoubleBuffer var3) {
        this.gl.glGetVertexAttribdvNV(var1, var2, var3);
    }

    public void glGetVertexAttribfv(int var1, int var2, float[] var3, int var4) {
        this.gl.glGetVertexAttribfv(var1, var2, var3, var4);
    }

    public void glGetVertexAttribfv(int var1, int var2, FloatBuffer var3) {
        this.gl.glGetVertexAttribfv(var1, var2, var3);
    }

    public void glGetVertexAttribfvARB(int var1, int var2, float[] var3, int var4) {
        this.gl.glGetVertexAttribfvARB(var1, var2, var3, var4);
    }

    public void glGetVertexAttribfvARB(int var1, int var2, FloatBuffer var3) {
        this.gl.glGetVertexAttribfvARB(var1, var2, var3);
    }

    public void glGetVertexAttribfvNV(int var1, int var2, float[] var3, int var4) {
        this.gl.glGetVertexAttribfvNV(var1, var2, var3, var4);
    }

    public void glGetVertexAttribfvNV(int var1, int var2, FloatBuffer var3) {
        this.gl.glGetVertexAttribfvNV(var1, var2, var3);
    }

    public void glGetVertexAttribiv(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetVertexAttribiv(var1, var2, var3, var4);
    }

    public void glGetVertexAttribiv(int var1, int var2, IntBuffer var3) {
        this.gl.glGetVertexAttribiv(var1, var2, var3);
    }

    public void glGetVertexAttribivARB(int var1, int var2, IntBuffer var3) {
        this.gl.glGetVertexAttribivARB(var1, var2, var3);
    }

    public void glGetVertexAttribivARB(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetVertexAttribivARB(var1, var2, var3, var4);
    }

    public void glGetVertexAttribivNV(int var1, int var2, int[] var3, int var4) {
        this.gl.glGetVertexAttribivNV(var1, var2, var3, var4);
    }

    public void glGetVertexAttribivNV(int var1, int var2, IntBuffer var3) {
        this.gl.glGetVertexAttribivNV(var1, var2, var3);
    }

    public void glGlobalAlphaFactorbSUN(byte var1) {
        this.gl.glGlobalAlphaFactorbSUN(var1);
    }

    public void glGlobalAlphaFactordSUN(double var1) {
        this.gl.glGlobalAlphaFactordSUN(var1);
    }

    public void glGlobalAlphaFactorfSUN(float var1) {
        this.gl.glGlobalAlphaFactorfSUN(var1);
    }

    public void glGlobalAlphaFactoriSUN(int var1) {
        this.gl.glGlobalAlphaFactoriSUN(var1);
    }

    public void glGlobalAlphaFactorsSUN(short var1) {
        this.gl.glGlobalAlphaFactorsSUN(var1);
    }

    public void glGlobalAlphaFactorubSUN(byte var1) {
        this.gl.glGlobalAlphaFactorubSUN(var1);
    }

    public void glGlobalAlphaFactoruiSUN(int var1) {
        this.gl.glGlobalAlphaFactoruiSUN(var1);
    }

    public void glGlobalAlphaFactorusSUN(short var1) {
        this.gl.glGlobalAlphaFactorusSUN(var1);
    }

    public void glHint(int var1, int var2) {
        this.gl.glHint(var1, var2);
    }

    public void glHintPGI(int var1, int var2) {
        this.gl.glHintPGI(var1, var2);
    }

    public void glHistogram(int var1, int var2, int var3, boolean var4) {
        this.gl.glHistogram(var1, var2, var3, var4);
    }

    public void glIglooInterfaceSGIX(int var1, Buffer var2) {
        this.gl.glIglooInterfaceSGIX(var1, var2);
    }

    public void glImageTransformParameterfHP(int var1, int var2, float var3) {
        this.gl.glImageTransformParameterfHP(var1, var2, var3);
    }

    public void glImageTransformParameterfvHP(int var1, int var2, FloatBuffer var3) {
        this.gl.glImageTransformParameterfvHP(var1, var2, var3);
    }

    public void glImageTransformParameterfvHP(int var1, int var2, float[] var3, int var4) {
        this.gl.glImageTransformParameterfvHP(var1, var2, var3, var4);
    }

    public void glImageTransformParameteriHP(int var1, int var2, int var3) {
        this.gl.glImageTransformParameteriHP(var1, var2, var3);
    }

    public void glImageTransformParameterivHP(int var1, int var2, int[] var3, int var4) {
        this.gl.glImageTransformParameterivHP(var1, var2, var3, var4);
    }

    public void glImageTransformParameterivHP(int var1, int var2, IntBuffer var3) {
        this.gl.glImageTransformParameterivHP(var1, var2, var3);
    }

    public void glIndexFuncEXT(int var1, float var2) {
        this.gl.glIndexFuncEXT(var1, var2);
    }

    public void glIndexMask(int var1) {
        this.gl.glIndexMask(var1);
    }

    public void glIndexMaterialEXT(int var1, int var2) {
        this.gl.glIndexMaterialEXT(var1, var2);
    }

    public void glIndexPointer(int var1, int var2, Buffer var3) {
        this.gl.glIndexPointer(var1, var2, var3);
    }

    public void glIndexd(double var1) {
        this.gl.glIndexd(var1);
    }

    public void glIndexdv(DoubleBuffer var1) {
        this.gl.glIndexdv(var1);
    }

    public void glIndexdv(double[] var1, int var2) {
        this.gl.glIndexdv(var1, var2);
    }

    public void glIndexf(float var1) {
        this.gl.glIndexf(var1);
    }

    public void glIndexfv(FloatBuffer var1) {
        this.gl.glIndexfv(var1);
    }

    public void glIndexfv(float[] var1, int var2) {
        this.gl.glIndexfv(var1, var2);
    }

    public void glIndexi(int var1) {
        this.gl.glIndexi(var1);
    }

    public void glIndexiv(IntBuffer var1) {
        this.gl.glIndexiv(var1);
    }

    public void glIndexiv(int[] var1, int var2) {
        this.gl.glIndexiv(var1, var2);
    }

    public void glIndexs(short var1) {
        this.gl.glIndexs(var1);
    }

    public void glIndexsv(short[] var1, int var2) {
        this.gl.glIndexsv(var1, var2);
    }

    public void glIndexsv(ShortBuffer var1) {
        this.gl.glIndexsv(var1);
    }

    public void glIndexub(byte var1) {
        this.gl.glIndexub(var1);
    }

    public void glIndexubv(byte[] var1, int var2) {
        this.gl.glIndexubv(var1, var2);
    }

    public void glIndexubv(ByteBuffer var1) {
        this.gl.glIndexubv(var1);
    }

    public void glInitNames() {
        this.gl.glInitNames();
    }

    public void glInsertComponentEXT(int var1, int var2, int var3) {
        this.gl.glInsertComponentEXT(var1, var2, var3);
    }

    public void glInstrumentsBufferSGIX(int var1, int[] var2, int var3) {
        this.gl.glInstrumentsBufferSGIX(var1, var2, var3);
    }

    public void glInstrumentsBufferSGIX(int var1, IntBuffer var2) {
        this.gl.glInstrumentsBufferSGIX(var1, var2);
    }

    public void glInterleavedArrays(int var1, int var2, Buffer var3) {
        this.gl.glInterleavedArrays(var1, var2, var3);
    }

    public void glInterleavedArrays(int var1, int var2, long var3) {
        this.gl.glInterleavedArrays(var1, var2, var3);
    }

    public boolean glIsAsyncMarkerSGIX(int var1) {
        return this.gl.glIsAsyncMarkerSGIX(var1);
    }

    public boolean glIsBuffer(int var1) {
        return this.gl.glIsBuffer(var1);
    }

    public boolean glIsBufferARB(int var1) {
        return this.gl.glIsBufferARB(var1);
    }

    public boolean glIsEnabled(int var1) {
        return this.gl.glIsEnabled(var1);
    }

    public boolean glIsEnabledIndexedEXT(int var1, int var2) {
        return this.gl.glIsEnabledIndexedEXT(var1, var2);
    }

    public boolean glIsFenceAPPLE(int var1) {
        return this.gl.glIsFenceAPPLE(var1);
    }

    public boolean glIsFenceNV(int var1) {
        return this.gl.glIsFenceNV(var1);
    }

    public boolean glIsFramebufferEXT(int var1) {
        return this.gl.glIsFramebufferEXT(var1);
    }

    public boolean glIsList(int var1) {
        return this.gl.glIsList(var1);
    }

    public boolean glIsObjectBufferATI(int var1) {
        return this.gl.glIsObjectBufferATI(var1);
    }

    public boolean glIsOcclusionQueryNV(int var1) {
        return this.gl.glIsOcclusionQueryNV(var1);
    }

    public boolean glIsProgram(int var1) {
        return this.gl.glIsProgram(var1);
    }

    public boolean glIsProgramARB(int var1) {
        return this.gl.glIsProgramARB(var1);
    }

    public boolean glIsProgramNV(int var1) {
        return this.gl.glIsProgramNV(var1);
    }

    public boolean glIsQuery(int var1) {
        return this.gl.glIsQuery(var1);
    }

    public boolean glIsQueryARB(int var1) {
        return this.gl.glIsQueryARB(var1);
    }

    public boolean glIsRenderbufferEXT(int var1) {
        return this.gl.glIsRenderbufferEXT(var1);
    }

    public boolean glIsShader(int var1) {
        return this.gl.glIsShader(var1);
    }

    public boolean glIsTexture(int var1) {
        return this.gl.glIsTexture(var1);
    }

    public boolean glIsVariantEnabledEXT(int var1, int var2) {
        return this.gl.glIsVariantEnabledEXT(var1, var2);
    }

    public boolean glIsVertexArrayAPPLE(int var1) {
        return this.gl.glIsVertexArrayAPPLE(var1);
    }

    public boolean glIsVertexAttribEnabledAPPLE(int var1, int var2) {
        return this.gl.glIsVertexAttribEnabledAPPLE(var1, var2);
    }

    public void glLightEnviSGIX(int var1, int var2) {
        this.gl.glLightEnviSGIX(var1, var2);
    }

    public void glLightModelf(int var1, float var2) {
        this.gl.glLightModelf(var1, var2);
    }

    public void glLightModelfv(int var1, float[] var2, int var3) {
        this.gl.glLightModelfv(var1, var2, var3);
    }

    public void glLightModelfv(int var1, FloatBuffer var2) {
        this.gl.glLightModelfv(var1, var2);
    }

    public void glLightModeli(int var1, int var2) {
        this.gl.glLightModeli(var1, var2);
    }

    public void glLightModeliv(int var1, IntBuffer var2) {
        this.gl.glLightModeliv(var1, var2);
    }

    public void glLightModeliv(int var1, int[] var2, int var3) {
        this.gl.glLightModeliv(var1, var2, var3);
    }

    public void glLightf(int var1, int var2, float var3) {
        this.gl.glLightf(var1, var2, var3);
    }

    public void glLightfv(int var1, int var2, FloatBuffer var3) {
        this.gl.glLightfv(var1, var2, var3);
    }

    public void glLightfv(int var1, int var2, float[] var3, int var4) {
        this.gl.glLightfv(var1, var2, var3, var4);
    }

    public void glLighti(int var1, int var2, int var3) {
        this.gl.glLighti(var1, var2, var3);
    }

    public void glLightiv(int var1, int var2, int[] var3, int var4) {
        this.gl.glLightiv(var1, var2, var3, var4);
    }

    public void glLightiv(int var1, int var2, IntBuffer var3) {
        this.gl.glLightiv(var1, var2, var3);
    }

    public void glLineStipple(int var1, short var2) {
        this.gl.glLineStipple(var1, var2);
    }

    public void glLineWidth(float var1) {
        this.gl.glLineWidth(var1);
    }

    public void glLinkProgram(int var1) {
        this.gl.glLinkProgram(var1);
    }

    public void glLinkProgramARB(int var1) {
        this.gl.glLinkProgramARB(var1);
    }

    public void glListBase(int var1) {
        this.gl.glListBase(var1);
    }

    public void glListParameterfSGIX(int var1, int var2, float var3) {
        this.gl.glListParameterfSGIX(var1, var2, var3);
    }

    public void glListParameterfvSGIX(int var1, int var2, float[] var3, int var4) {
        this.gl.glListParameterfvSGIX(var1, var2, var3, var4);
    }

    public void glListParameterfvSGIX(int var1, int var2, FloatBuffer var3) {
        this.gl.glListParameterfvSGIX(var1, var2, var3);
    }

    public void glListParameteriSGIX(int var1, int var2, int var3) {
        this.gl.glListParameteriSGIX(var1, var2, var3);
    }

    public void glListParameterivSGIX(int var1, int var2, IntBuffer var3) {
        this.gl.glListParameterivSGIX(var1, var2, var3);
    }

    public void glListParameterivSGIX(int var1, int var2, int[] var3, int var4) {
        this.gl.glListParameterivSGIX(var1, var2, var3, var4);
    }

    public void glLoadIdentity() {
        this.gl.glLoadIdentity();
    }

    public void glLoadIdentityDeformationMapSGIX(int var1) {
        this.gl.glLoadIdentityDeformationMapSGIX(var1);
    }

    public void glLoadMatrixd(double[] var1, int var2) {
        this.gl.glLoadMatrixd(var1, var2);
    }

    public void glLoadMatrixd(DoubleBuffer var1) {
        this.gl.glLoadMatrixd(var1);
    }

    public void glLoadMatrixf(float[] var1, int var2) {
        this.gl.glLoadMatrixf(var1, var2);
    }

    public void glLoadMatrixf(FloatBuffer var1) {
        this.gl.glLoadMatrixf(var1);
    }

    public void glLoadName(int var1) {
        this.gl.glLoadName(var1);
    }

    public void glLoadProgramNV(int var1, int var2, int var3, String var4) {
        this.gl.glLoadProgramNV(var1, var2, var3, var4);
    }

    public void glLoadTransposeMatrixd(double[] var1, int var2) {
        this.gl.glLoadTransposeMatrixd(var1, var2);
    }

    public void glLoadTransposeMatrixd(DoubleBuffer var1) {
        this.gl.glLoadTransposeMatrixd(var1);
    }

    public void glLoadTransposeMatrixf(FloatBuffer var1) {
        this.gl.glLoadTransposeMatrixf(var1);
    }

    public void glLoadTransposeMatrixf(float[] var1, int var2) {
        this.gl.glLoadTransposeMatrixf(var1, var2);
    }

    public void glLockArraysEXT(int var1, int var2) {
        this.gl.glLockArraysEXT(var1, var2);
    }

    public void glLogicOp(int var1) {
        this.gl.glLogicOp(var1);
    }

    public void glMap1d(int var1, double var2, double var4, int var6, int var7, DoubleBuffer var8) {
        this.gl.glMap1d(var1, var2, var4, var6, var7, var8);
    }

    public void glMap1d(int var1, double var2, double var4, int var6, int var7, double[] var8, int var9) {
        this.gl.glMap1d(var1, var2, var4, var6, var7, var8, var9);
    }

    public void glMap1f(int var1, float var2, float var3, int var4, int var5, float[] var6, int var7) {
        this.gl.glMap1f(var1, var2, var3, var4, var5, var6, var7);
    }

    public void glMap1f(int var1, float var2, float var3, int var4, int var5, FloatBuffer var6) {
        this.gl.glMap1f(var1, var2, var3, var4, var5, var6);
    }

    public void glMap2d(int var1, double var2, double var4, int var6, int var7, double var8, double var10, int var12, int var13, double[] var14, int var15) {
        this.gl.glMap2d(var1, var2, var4, var6, var7, var8, var10, var12, var13, var14, var15);
    }

    public void glMap2d(int var1, double var2, double var4, int var6, int var7, double var8, double var10, int var12, int var13, DoubleBuffer var14) {
        this.gl.glMap2d(var1, var2, var4, var6, var7, var8, var10, var12, var13, var14);
    }

    public void glMap2f(int var1, float var2, float var3, int var4, int var5, float var6, float var7, int var8, int var9, FloatBuffer var10) {
        this.gl.glMap2f(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10);
    }

    public void glMap2f(int var1, float var2, float var3, int var4, int var5, float var6, float var7, int var8, int var9, float[] var10, int var11) {
        this.gl.glMap2f(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11);
    }

    public ByteBuffer glMapBuffer(int var1, int var2) {
        return this.gl.glMapBuffer(var1, var2);
    }

    public ByteBuffer glMapBufferARB(int var1, int var2) {
        return this.gl.glMapBufferARB(var1, var2);
    }

    public void glMapControlPointsNV(int var1, int var2, int var3, int var4, int var5, int var6, int var7, boolean var8, Buffer var9) {
        this.gl.glMapControlPointsNV(var1, var2, var3, var4, var5, var6, var7, var8, var9);
    }

    public void glMapGrid1d(int var1, double var2, double var4) {
        this.gl.glMapGrid1d(var1, var2, var4);
    }

    public void glMapGrid1f(int var1, float var2, float var3) {
        this.gl.glMapGrid1f(var1, var2, var3);
    }

    public void glMapGrid2d(int var1, double var2, double var4, int var6, double var7, double var9) {
        this.gl.glMapGrid2d(var1, var2, var4, var6, var7, var9);
    }

    public void glMapGrid2f(int var1, float var2, float var3, int var4, float var5, float var6) {
        this.gl.glMapGrid2f(var1, var2, var3, var4, var5, var6);
    }

    public void glMapParameterfvNV(int var1, int var2, FloatBuffer var3) {
        this.gl.glMapParameterfvNV(var1, var2, var3);
    }

    public void glMapParameterfvNV(int var1, int var2, float[] var3, int var4) {
        this.gl.glMapParameterfvNV(var1, var2, var3, var4);
    }

    public void glMapParameterivNV(int var1, int var2, int[] var3, int var4) {
        this.gl.glMapParameterivNV(var1, var2, var3, var4);
    }

    public void glMapParameterivNV(int var1, int var2, IntBuffer var3) {
        this.gl.glMapParameterivNV(var1, var2, var3);
    }

    public void glMapVertexAttrib1dAPPLE(int var1, int var2, double var3, double var5, int var7, int var8, double[] var9, int var10) {
        this.gl.glMapVertexAttrib1dAPPLE(var1, var2, var3, var5, var7, var8, var9, var10);
    }

    public void glMapVertexAttrib1dAPPLE(int var1, int var2, double var3, double var5, int var7, int var8, DoubleBuffer var9) {
        this.gl.glMapVertexAttrib1dAPPLE(var1, var2, var3, var5, var7, var8, var9);
    }

    public void glMapVertexAttrib1fAPPLE(int var1, int var2, float var3, float var4, int var5, int var6, FloatBuffer var7) {
        this.gl.glMapVertexAttrib1fAPPLE(var1, var2, var3, var4, var5, var6, var7);
    }

    public void glMapVertexAttrib1fAPPLE(int var1, int var2, float var3, float var4, int var5, int var6, float[] var7, int var8) {
        this.gl.glMapVertexAttrib1fAPPLE(var1, var2, var3, var4, var5, var6, var7, var8);
    }

    public void glMapVertexAttrib2dAPPLE(int var1, int var2, double var3, double var5, int var7, int var8, double var9, double var11, int var13, int var14, double[] var15, int var16) {
        this.gl.glMapVertexAttrib2dAPPLE(var1, var2, var3, var5, var7, var8, var9, var11, var13, var14, var15, var16);
    }

    public void glMapVertexAttrib2dAPPLE(int var1, int var2, double var3, double var5, int var7, int var8, double var9, double var11, int var13, int var14, DoubleBuffer var15) {
        this.gl.glMapVertexAttrib2dAPPLE(var1, var2, var3, var5, var7, var8, var9, var11, var13, var14, var15);
    }

    public void glMapVertexAttrib2fAPPLE(int var1, int var2, float var3, float var4, int var5, int var6, float var7, float var8, int var9, int var10, FloatBuffer var11) {
        this.gl.glMapVertexAttrib2fAPPLE(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11);
    }

    public void glMapVertexAttrib2fAPPLE(int var1, int var2, float var3, float var4, int var5, int var6, float var7, float var8, int var9, int var10, float[] var11, int var12) {
        this.gl.glMapVertexAttrib2fAPPLE(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11, var12);
    }

    public void glMaterialf(int var1, int var2, float var3) {
        this.gl.glMaterialf(var1, var2, var3);
    }

    public void glMaterialfv(int var1, int var2, FloatBuffer var3) {
        this.gl.glMaterialfv(var1, var2, var3);
    }

    public void glMaterialfv(int var1, int var2, float[] var3, int var4) {
        this.gl.glMaterialfv(var1, var2, var3, var4);
    }

    public void glMateriali(int var1, int var2, int var3) {
        this.gl.glMateriali(var1, var2, var3);
    }

    public void glMaterialiv(int var1, int var2, int[] var3, int var4) {
        this.gl.glMaterialiv(var1, var2, var3, var4);
    }

    public void glMaterialiv(int var1, int var2, IntBuffer var3) {
        this.gl.glMaterialiv(var1, var2, var3);
    }

    public void glMatrixIndexPointerARB(int var1, int var2, int var3, long var4) {
        this.gl.glMatrixIndexPointerARB(var1, var2, var3, var4);
    }

    public void glMatrixIndexPointerARB(int var1, int var2, int var3, Buffer var4) {
        this.gl.glMatrixIndexPointerARB(var1, var2, var3, var4);
    }

    public void glMatrixIndexubvARB(int var1, byte[] var2, int var3) {
        this.gl.glMatrixIndexubvARB(var1, var2, var3);
    }

    public void glMatrixIndexubvARB(int var1, ByteBuffer var2) {
        this.gl.glMatrixIndexubvARB(var1, var2);
    }

    public void glMatrixIndexuivARB(int var1, int[] var2, int var3) {
        this.gl.glMatrixIndexuivARB(var1, var2, var3);
    }

    public void glMatrixIndexuivARB(int var1, IntBuffer var2) {
        this.gl.glMatrixIndexuivARB(var1, var2);
    }

    public void glMatrixIndexusvARB(int var1, short[] var2, int var3) {
        this.gl.glMatrixIndexusvARB(var1, var2, var3);
    }

    public void glMatrixIndexusvARB(int var1, ShortBuffer var2) {
        this.gl.glMatrixIndexusvARB(var1, var2);
    }

    public void glMatrixMode(int var1) {
        this.gl.glMatrixMode(var1);
    }

    public void glMinmax(int var1, int var2, boolean var3) {
        this.gl.glMinmax(var1, var2, var3);
    }

    public void glMultMatrixd(DoubleBuffer var1) {
        this.gl.glMultMatrixd(var1);
    }

    public void glMultMatrixd(double[] var1, int var2) {
        this.gl.glMultMatrixd(var1, var2);
    }

    public void glMultMatrixf(float[] var1, int var2) {
        this.gl.glMultMatrixf(var1, var2);
    }

    public void glMultMatrixf(FloatBuffer var1) {
        this.gl.glMultMatrixf(var1);
    }

    public void glMultTransposeMatrixd(double[] var1, int var2) {
        this.gl.glMultTransposeMatrixd(var1, var2);
    }

    public void glMultTransposeMatrixd(DoubleBuffer var1) {
        this.gl.glMultTransposeMatrixd(var1);
    }

    public void glMultTransposeMatrixf(float[] var1, int var2) {
        this.gl.glMultTransposeMatrixf(var1, var2);
    }

    public void glMultTransposeMatrixf(FloatBuffer var1) {
        this.gl.glMultTransposeMatrixf(var1);
    }

    public void glMultiDrawArrays(int var1, int[] var2, int var3, int[] var4, int var5, int var6) {
        this.gl.glMultiDrawArrays(var1, var2, var3, var4, var5, var6);
    }

    public void glMultiDrawArrays(int var1, IntBuffer var2, IntBuffer var3, int var4) {
        this.gl.glMultiDrawArrays(var1, var2, var3, var4);
    }

    public void glMultiDrawArraysEXT(int var1, int[] var2, int var3, int[] var4, int var5, int var6) {
        this.gl.glMultiDrawArraysEXT(var1, var2, var3, var4, var5, var6);
    }

    public void glMultiDrawArraysEXT(int var1, IntBuffer var2, IntBuffer var3, int var4) {
        this.gl.glMultiDrawArraysEXT(var1, var2, var3, var4);
    }

    public void glMultiDrawElementArrayAPPLE(int var1, int[] var2, int var3, int[] var4, int var5, int var6) {
        this.gl.glMultiDrawElementArrayAPPLE(var1, var2, var3, var4, var5, var6);
    }

    public void glMultiDrawElementArrayAPPLE(int var1, IntBuffer var2, IntBuffer var3, int var4) {
        this.gl.glMultiDrawElementArrayAPPLE(var1, var2, var3, var4);
    }

    public void glMultiDrawElements(int var1, IntBuffer var2, int var3, Buffer[] var4, int var5) {
        this.gl.glMultiDrawElements(var1, var2, var3, var4, var5);
    }

    public void glMultiDrawElements(int var1, int[] var2, int var3, int var4, Buffer[] var5, int var6) {
        this.gl.glMultiDrawElements(var1, var2, var3, var4, var5, var6);
    }

    public void glMultiDrawElementsEXT(int var1, int[] var2, int var3, int var4, Buffer[] var5, int var6) {
        this.gl.glMultiDrawElementsEXT(var1, var2, var3, var4, var5, var6);
    }

    public void glMultiDrawElementsEXT(int var1, IntBuffer var2, int var3, Buffer[] var4, int var5) {
        this.gl.glMultiDrawElementsEXT(var1, var2, var3, var4, var5);
    }

    public void glMultiDrawRangeElementArrayAPPLE(int var1, int var2, int var3, IntBuffer var4, IntBuffer var5, int var6) {
        this.gl.glMultiDrawRangeElementArrayAPPLE(var1, var2, var3, var4, var5, var6);
    }

    public void glMultiDrawRangeElementArrayAPPLE(int var1, int var2, int var3, int[] var4, int var5, int[] var6, int var7, int var8) {
        this.gl.glMultiDrawRangeElementArrayAPPLE(var1, var2, var3, var4, var5, var6, var7, var8);
    }

    public void glMultiModeDrawArraysIBM(int[] var1, int var2, int[] var3, int var4, int[] var5, int var6, int var7, int var8) {
        this.gl.glMultiModeDrawArraysIBM(var1, var2, var3, var4, var5, var6, var7, var8);
    }

    public void glMultiModeDrawArraysIBM(IntBuffer var1, IntBuffer var2, IntBuffer var3, int var4, int var5) {
        this.gl.glMultiModeDrawArraysIBM(var1, var2, var3, var4, var5);
    }

    public void glMultiModeDrawElementsIBM(IntBuffer var1, IntBuffer var2, int var3, Buffer[] var4, int var5, int var6) {
        this.gl.glMultiModeDrawElementsIBM(var1, var2, var3, var4, var5, var6);
    }

    public void glMultiModeDrawElementsIBM(int[] var1, int var2, int[] var3, int var4, int var5, Buffer[] var6, int var7, int var8) {
        this.gl.glMultiModeDrawElementsIBM(var1, var2, var3, var4, var5, var6, var7, var8);
    }

    public void glMultiTexCoord1d(int var1, double var2) {
        this.gl.glMultiTexCoord1d(var1, var2);
    }

    public void glMultiTexCoord1dv(int var1, DoubleBuffer var2) {
        this.gl.glMultiTexCoord1dv(var1, var2);
    }

    public void glMultiTexCoord1dv(int var1, double[] var2, int var3) {
        this.gl.glMultiTexCoord1dv(var1, var2, var3);
    }

    public void glMultiTexCoord1f(int var1, float var2) {
        this.gl.glMultiTexCoord1f(var1, var2);
    }

    public void glMultiTexCoord1fv(int var1, FloatBuffer var2) {
        this.gl.glMultiTexCoord1fv(var1, var2);
    }

    public void glMultiTexCoord1fv(int var1, float[] var2, int var3) {
        this.gl.glMultiTexCoord1fv(var1, var2, var3);
    }

    public void glMultiTexCoord1hNV(int var1, short var2) {
        this.gl.glMultiTexCoord1hNV(var1, var2);
    }

    public void glMultiTexCoord1hvNV(int var1, ShortBuffer var2) {
        this.gl.glMultiTexCoord1hvNV(var1, var2);
    }

    public void glMultiTexCoord1hvNV(int var1, short[] var2, int var3) {
        this.gl.glMultiTexCoord1hvNV(var1, var2, var3);
    }

    public void glMultiTexCoord1i(int var1, int var2) {
        this.gl.glMultiTexCoord1i(var1, var2);
    }

    public void glMultiTexCoord1iv(int var1, IntBuffer var2) {
        this.gl.glMultiTexCoord1iv(var1, var2);
    }

    public void glMultiTexCoord1iv(int var1, int[] var2, int var3) {
        this.gl.glMultiTexCoord1iv(var1, var2, var3);
    }

    public void glMultiTexCoord1s(int var1, short var2) {
        this.gl.glMultiTexCoord1s(var1, var2);
    }

    public void glMultiTexCoord1sv(int var1, short[] var2, int var3) {
        this.gl.glMultiTexCoord1sv(var1, var2, var3);
    }

    public void glMultiTexCoord1sv(int var1, ShortBuffer var2) {
        this.gl.glMultiTexCoord1sv(var1, var2);
    }

    public void glMultiTexCoord2d(int var1, double var2, double var4) {
        this.gl.glMultiTexCoord2d(var1, var2, var4);
    }

    public void glMultiTexCoord2dv(int var1, DoubleBuffer var2) {
        this.gl.glMultiTexCoord2dv(var1, var2);
    }

    public void glMultiTexCoord2dv(int var1, double[] var2, int var3) {
        this.gl.glMultiTexCoord2dv(var1, var2, var3);
    }

    public void glMultiTexCoord2f(int var1, float var2, float var3) {
        this.gl.glMultiTexCoord2f(var1, var2, var3);
    }

    public void glMultiTexCoord2fv(int var1, FloatBuffer var2) {
        this.gl.glMultiTexCoord2fv(var1, var2);
    }

    public void glMultiTexCoord2fv(int var1, float[] var2, int var3) {
        this.gl.glMultiTexCoord2fv(var1, var2, var3);
    }

    public void glMultiTexCoord2hNV(int var1, short var2, short var3) {
        this.gl.glMultiTexCoord2hNV(var1, var2, var3);
    }

    public void glMultiTexCoord2hvNV(int var1, short[] var2, int var3) {
        this.gl.glMultiTexCoord2hvNV(var1, var2, var3);
    }

    public void glMultiTexCoord2hvNV(int var1, ShortBuffer var2) {
        this.gl.glMultiTexCoord2hvNV(var1, var2);
    }

    public void glMultiTexCoord2i(int var1, int var2, int var3) {
        this.gl.glMultiTexCoord2i(var1, var2, var3);
    }

    public void glMultiTexCoord2iv(int var1, IntBuffer var2) {
        this.gl.glMultiTexCoord2iv(var1, var2);
    }

    public void glMultiTexCoord2iv(int var1, int[] var2, int var3) {
        this.gl.glMultiTexCoord2iv(var1, var2, var3);
    }

    public void glMultiTexCoord2s(int var1, short var2, short var3) {
        this.gl.glMultiTexCoord2s(var1, var2, var3);
    }

    public void glMultiTexCoord2sv(int var1, ShortBuffer var2) {
        this.gl.glMultiTexCoord2sv(var1, var2);
    }

    public void glMultiTexCoord2sv(int var1, short[] var2, int var3) {
        this.gl.glMultiTexCoord2sv(var1, var2, var3);
    }

    public void glMultiTexCoord3d(int var1, double var2, double var4, double var6) {
        this.gl.glMultiTexCoord3d(var1, var2, var4, var6);
    }

    public void glMultiTexCoord3dv(int var1, DoubleBuffer var2) {
        this.gl.glMultiTexCoord3dv(var1, var2);
    }

    public void glMultiTexCoord3dv(int var1, double[] var2, int var3) {
        this.gl.glMultiTexCoord3dv(var1, var2, var3);
    }

    public void glMultiTexCoord3f(int var1, float var2, float var3, float var4) {
        this.gl.glMultiTexCoord3f(var1, var2, var3, var4);
    }

    public void glMultiTexCoord3fv(int var1, FloatBuffer var2) {
        this.gl.glMultiTexCoord3fv(var1, var2);
    }

    public void glMultiTexCoord3fv(int var1, float[] var2, int var3) {
        this.gl.glMultiTexCoord3fv(var1, var2, var3);
    }

    public void glMultiTexCoord3hNV(int var1, short var2, short var3, short var4) {
        this.gl.glMultiTexCoord3hNV(var1, var2, var3, var4);
    }

    public void glMultiTexCoord3hvNV(int var1, ShortBuffer var2) {
        this.gl.glMultiTexCoord3hvNV(var1, var2);
    }

    public void glMultiTexCoord3hvNV(int var1, short[] var2, int var3) {
        this.gl.glMultiTexCoord3hvNV(var1, var2, var3);
    }

    public void glMultiTexCoord3i(int var1, int var2, int var3, int var4) {
        this.gl.glMultiTexCoord3i(var1, var2, var3, var4);
    }

    public void glMultiTexCoord3iv(int var1, int[] var2, int var3) {
        this.gl.glMultiTexCoord3iv(var1, var2, var3);
    }

    public void glMultiTexCoord3iv(int var1, IntBuffer var2) {
        this.gl.glMultiTexCoord3iv(var1, var2);
    }

    public void glMultiTexCoord3s(int var1, short var2, short var3, short var4) {
        this.gl.glMultiTexCoord3s(var1, var2, var3, var4);
    }

    public void glMultiTexCoord3sv(int var1, short[] var2, int var3) {
        this.gl.glMultiTexCoord3sv(var1, var2, var3);
    }

    public void glMultiTexCoord3sv(int var1, ShortBuffer var2) {
        this.gl.glMultiTexCoord3sv(var1, var2);
    }

    public void glMultiTexCoord4d(int var1, double var2, double var4, double var6, double var8) {
        this.gl.glMultiTexCoord4d(var1, var2, var4, var6, var8);
    }

    public void glMultiTexCoord4dv(int var1, DoubleBuffer var2) {
        this.gl.glMultiTexCoord4dv(var1, var2);
    }

    public void glMultiTexCoord4dv(int var1, double[] var2, int var3) {
        this.gl.glMultiTexCoord4dv(var1, var2, var3);
    }

    public void glMultiTexCoord4f(int var1, float var2, float var3, float var4, float var5) {
        this.gl.glMultiTexCoord4f(var1, var2, var3, var4, var5);
    }

    public void glMultiTexCoord4fv(int var1, FloatBuffer var2) {
        this.gl.glMultiTexCoord4fv(var1, var2);
    }

    public void glMultiTexCoord4fv(int var1, float[] var2, int var3) {
        this.gl.glMultiTexCoord4fv(var1, var2, var3);
    }

    public void glMultiTexCoord4hNV(int var1, short var2, short var3, short var4, short var5) {
        this.gl.glMultiTexCoord4hNV(var1, var2, var3, var4, var5);
    }

    public void glMultiTexCoord4hvNV(int var1, ShortBuffer var2) {
        this.gl.glMultiTexCoord4hvNV(var1, var2);
    }

    public void glMultiTexCoord4hvNV(int var1, short[] var2, int var3) {
        this.gl.glMultiTexCoord4hvNV(var1, var2, var3);
    }

    public void glMultiTexCoord4i(int var1, int var2, int var3, int var4, int var5) {
        this.gl.glMultiTexCoord4i(var1, var2, var3, var4, var5);
    }

    public void glMultiTexCoord4iv(int var1, int[] var2, int var3) {
        this.gl.glMultiTexCoord4iv(var1, var2, var3);
    }

    public void glMultiTexCoord4iv(int var1, IntBuffer var2) {
        this.gl.glMultiTexCoord4iv(var1, var2);
    }

    public void glMultiTexCoord4s(int var1, short var2, short var3, short var4, short var5) {
        this.gl.glMultiTexCoord4s(var1, var2, var3, var4, var5);
    }

    public void glMultiTexCoord4sv(int var1, short[] var2, int var3) {
        this.gl.glMultiTexCoord4sv(var1, var2, var3);
    }

    public void glMultiTexCoord4sv(int var1, ShortBuffer var2) {
        this.gl.glMultiTexCoord4sv(var1, var2);
    }

    public int glNewBufferRegion(int var1) {
        return this.gl.glNewBufferRegion(var1);
    }

    public void glNewList(int var1, int var2) {
        this.gl.glNewList(var1, var2);
    }

    public int glNewObjectBufferATI(int var1, Buffer var2, int var3) {
        return this.gl.glNewObjectBufferATI(var1, var2, var3);
    }

    public void glNormal3b(byte var1, byte var2, byte var3) {
        this.gl.glNormal3b(var1, var2, var3);
    }

    public void glNormal3bv(ByteBuffer var1) {
        this.gl.glNormal3bv(var1);
    }

    public void glNormal3bv(byte[] var1, int var2) {
        this.gl.glNormal3bv(var1, var2);
    }

    public void glNormal3d(double var1, double var3, double var5) {
        this.gl.glNormal3d(var1, var3, var5);
    }

    public void glNormal3dv(DoubleBuffer var1) {
        this.gl.glNormal3dv(var1);
    }

    public void glNormal3dv(double[] var1, int var2) {
        this.gl.glNormal3dv(var1, var2);
    }

    public void glNormal3f(float var1, float var2, float var3) {
        this.gl.glNormal3f(var1, var2, var3);
    }

    public void glNormal3fVertex3fSUN(float var1, float var2, float var3, float var4, float var5, float var6) {
        this.gl.glNormal3fVertex3fSUN(var1, var2, var3, var4, var5, var6);
    }

    public void glNormal3fVertex3fvSUN(float[] var1, int var2, float[] var3, int var4) {
        this.gl.glNormal3fVertex3fvSUN(var1, var2, var3, var4);
    }

    public void glNormal3fVertex3fvSUN(FloatBuffer var1, FloatBuffer var2) {
        this.gl.glNormal3fVertex3fvSUN(var1, var2);
    }

    public void glNormal3fv(FloatBuffer var1) {
        this.gl.glNormal3fv(var1);
    }

    public void glNormal3fv(float[] var1, int var2) {
        this.gl.glNormal3fv(var1, var2);
    }

    public void glNormal3hNV(short var1, short var2, short var3) {
        this.gl.glNormal3hNV(var1, var2, var3);
    }

    public void glNormal3hvNV(ShortBuffer var1) {
        this.gl.glNormal3hvNV(var1);
    }

    public void glNormal3hvNV(short[] var1, int var2) {
        this.gl.glNormal3hvNV(var1, var2);
    }

    public void glNormal3i(int var1, int var2, int var3) {
        this.gl.glNormal3i(var1, var2, var3);
    }

    public void glNormal3iv(IntBuffer var1) {
        this.gl.glNormal3iv(var1);
    }

    public void glNormal3iv(int[] var1, int var2) {
        this.gl.glNormal3iv(var1, var2);
    }

    public void glNormal3s(short var1, short var2, short var3) {
        this.gl.glNormal3s(var1, var2, var3);
    }

    public void glNormal3sv(ShortBuffer var1) {
        this.gl.glNormal3sv(var1);
    }

    public void glNormal3sv(short[] var1, int var2) {
        this.gl.glNormal3sv(var1, var2);
    }

    public void glNormalPointer(int var1, int var2, long var3) {
        this.gl.glNormalPointer(var1, var2, var3);
    }

    public void glNormalPointer(int var1, int var2, Buffer var3) {
        this.gl.glNormalPointer(var1, var2, var3);
    }

    public void glNormalStream3bATI(int var1, byte var2, byte var3, byte var4) {
        this.gl.glNormalStream3bATI(var1, var2, var3, var4);
    }

    public void glNormalStream3bvATI(int var1, ByteBuffer var2) {
        this.gl.glNormalStream3bvATI(var1, var2);
    }

    public void glNormalStream3bvATI(int var1, byte[] var2, int var3) {
        this.gl.glNormalStream3bvATI(var1, var2, var3);
    }

    public void glNormalStream3dATI(int var1, double var2, double var4, double var6) {
        this.gl.glNormalStream3dATI(var1, var2, var4, var6);
    }

    public void glNormalStream3dvATI(int var1, double[] var2, int var3) {
        this.gl.glNormalStream3dvATI(var1, var2, var3);
    }

    public void glNormalStream3dvATI(int var1, DoubleBuffer var2) {
        this.gl.glNormalStream3dvATI(var1, var2);
    }

    public void glNormalStream3fATI(int var1, float var2, float var3, float var4) {
        this.gl.glNormalStream3fATI(var1, var2, var3, var4);
    }

    public void glNormalStream3fvATI(int var1, float[] var2, int var3) {
        this.gl.glNormalStream3fvATI(var1, var2, var3);
    }

    public void glNormalStream3fvATI(int var1, FloatBuffer var2) {
        this.gl.glNormalStream3fvATI(var1, var2);
    }

    public void glNormalStream3iATI(int var1, int var2, int var3, int var4) {
        this.gl.glNormalStream3iATI(var1, var2, var3, var4);
    }

    public void glNormalStream3ivATI(int var1, IntBuffer var2) {
        this.gl.glNormalStream3ivATI(var1, var2);
    }

    public void glNormalStream3ivATI(int var1, int[] var2, int var3) {
        this.gl.glNormalStream3ivATI(var1, var2, var3);
    }

    public void glNormalStream3sATI(int var1, short var2, short var3, short var4) {
        this.gl.glNormalStream3sATI(var1, var2, var3, var4);
    }

    public void glNormalStream3svATI(int var1, short[] var2, int var3) {
        this.gl.glNormalStream3svATI(var1, var2, var3);
    }

    public void glNormalStream3svATI(int var1, ShortBuffer var2) {
        this.gl.glNormalStream3svATI(var1, var2);
    }

    public void glOrtho(double var1, double var3, double var5, double var7, double var9, double var11) {
        this.gl.glOrtho(var1, var3, var5, var7, var9, var11);
    }

    public void glPNTrianglesfATI(int var1, float var2) {
        this.gl.glPNTrianglesfATI(var1, var2);
    }

    public void glPNTrianglesiATI(int var1, int var2) {
        this.gl.glPNTrianglesiATI(var1, var2);
    }

    public void glPassTexCoordATI(int var1, int var2, int var3) {
        this.gl.glPassTexCoordATI(var1, var2, var3);
    }

    public void glPassThrough(float var1) {
        this.gl.glPassThrough(var1);
    }

    public void glPixelDataRangeNV(int var1, int var2, Buffer var3) {
        this.gl.glPixelDataRangeNV(var1, var2, var3);
    }

    public void glPixelMapfv(int var1, int var2, long var3) {
        this.gl.glPixelMapfv(var1, var2, var3);
    }

    public void glPixelMapfv(int var1, int var2, float[] var3, int var4) {
        this.gl.glPixelMapfv(var1, var2, var3, var4);
    }

    public void glPixelMapfv(int var1, int var2, FloatBuffer var3) {
        this.gl.glPixelMapfv(var1, var2, var3);
    }

    public void glPixelMapuiv(int var1, int var2, long var3) {
        this.gl.glPixelMapuiv(var1, var2, var3);
    }

    public void glPixelMapuiv(int var1, int var2, int[] var3, int var4) {
        this.gl.glPixelMapuiv(var1, var2, var3, var4);
    }

    public void glPixelMapuiv(int var1, int var2, IntBuffer var3) {
        this.gl.glPixelMapuiv(var1, var2, var3);
    }

    public void glPixelMapusv(int var1, int var2, long var3) {
        this.gl.glPixelMapusv(var1, var2, var3);
    }

    public void glPixelMapusv(int var1, int var2, short[] var3, int var4) {
        this.gl.glPixelMapusv(var1, var2, var3, var4);
    }

    public void glPixelMapusv(int var1, int var2, ShortBuffer var3) {
        this.gl.glPixelMapusv(var1, var2, var3);
    }

    public void glPixelStoref(int var1, float var2) {
        this.gl.glPixelStoref(var1, var2);
    }

    public void glPixelStorei(int var1, int var2) {
        this.gl.glPixelStorei(var1, var2);
    }

    public void glPixelTexGenParameterfSGIS(int var1, float var2) {
        this.gl.glPixelTexGenParameterfSGIS(var1, var2);
    }

    public void glPixelTexGenParameterfvSGIS(int var1, FloatBuffer var2) {
        this.gl.glPixelTexGenParameterfvSGIS(var1, var2);
    }

    public void glPixelTexGenParameterfvSGIS(int var1, float[] var2, int var3) {
        this.gl.glPixelTexGenParameterfvSGIS(var1, var2, var3);
    }

    public void glPixelTexGenParameteriSGIS(int var1, int var2) {
        this.gl.glPixelTexGenParameteriSGIS(var1, var2);
    }

    public void glPixelTexGenParameterivSGIS(int var1, int[] var2, int var3) {
        this.gl.glPixelTexGenParameterivSGIS(var1, var2, var3);
    }

    public void glPixelTexGenParameterivSGIS(int var1, IntBuffer var2) {
        this.gl.glPixelTexGenParameterivSGIS(var1, var2);
    }

    public void glPixelTexGenSGIX(int var1) {
        this.gl.glPixelTexGenSGIX(var1);
    }

    public void glPixelTransferf(int var1, float var2) {
        this.gl.glPixelTransferf(var1, var2);
    }

    public void glPixelTransferi(int var1, int var2) {
        this.gl.glPixelTransferi(var1, var2);
    }

    public void glPixelTransformParameterfEXT(int var1, int var2, float var3) {
        this.gl.glPixelTransformParameterfEXT(var1, var2, var3);
    }

    public void glPixelTransformParameterfvEXT(int var1, int var2, FloatBuffer var3) {
        this.gl.glPixelTransformParameterfvEXT(var1, var2, var3);
    }

    public void glPixelTransformParameterfvEXT(int var1, int var2, float[] var3, int var4) {
        this.gl.glPixelTransformParameterfvEXT(var1, var2, var3, var4);
    }

    public void glPixelTransformParameteriEXT(int var1, int var2, int var3) {
        this.gl.glPixelTransformParameteriEXT(var1, var2, var3);
    }

    public void glPixelTransformParameterivEXT(int var1, int var2, IntBuffer var3) {
        this.gl.glPixelTransformParameterivEXT(var1, var2, var3);
    }

    public void glPixelTransformParameterivEXT(int var1, int var2, int[] var3, int var4) {
        this.gl.glPixelTransformParameterivEXT(var1, var2, var3, var4);
    }

    public void glPixelZoom(float var1, float var2) {
        this.gl.glPixelZoom(var1, var2);
    }

    public void glPointParameterf(int var1, float var2) {
        this.gl.glPointParameterf(var1, var2);
    }

    public void glPointParameterfARB(int var1, float var2) {
        this.gl.glPointParameterfARB(var1, var2);
    }

    public void glPointParameterfEXT(int var1, float var2) {
        this.gl.glPointParameterfEXT(var1, var2);
    }

    public void glPointParameterfSGIS(int var1, float var2) {
        this.gl.glPointParameterfSGIS(var1, var2);
    }

    public void glPointParameterfv(int var1, float[] var2, int var3) {
        this.gl.glPointParameterfv(var1, var2, var3);
    }

    public void glPointParameterfv(int var1, FloatBuffer var2) {
        this.gl.glPointParameterfv(var1, var2);
    }

    public void glPointParameterfvARB(int var1, FloatBuffer var2) {
        this.gl.glPointParameterfvARB(var1, var2);
    }

    public void glPointParameterfvARB(int var1, float[] var2, int var3) {
        this.gl.glPointParameterfvARB(var1, var2, var3);
    }

    public void glPointParameterfvEXT(int var1, float[] var2, int var3) {
        this.gl.glPointParameterfvEXT(var1, var2, var3);
    }

    public void glPointParameterfvEXT(int var1, FloatBuffer var2) {
        this.gl.glPointParameterfvEXT(var1, var2);
    }

    public void glPointParameterfvSGIS(int var1, FloatBuffer var2) {
        this.gl.glPointParameterfvSGIS(var1, var2);
    }

    public void glPointParameterfvSGIS(int var1, float[] var2, int var3) {
        this.gl.glPointParameterfvSGIS(var1, var2, var3);
    }

    public void glPointParameteri(int var1, int var2) {
        this.gl.glPointParameteri(var1, var2);
    }

    public void glPointParameteriNV(int var1, int var2) {
        this.gl.glPointParameteriNV(var1, var2);
    }

    public void glPointParameteriv(int var1, int[] var2, int var3) {
        this.gl.glPointParameteriv(var1, var2, var3);
    }

    public void glPointParameteriv(int var1, IntBuffer var2) {
        this.gl.glPointParameteriv(var1, var2);
    }

    public void glPointParameterivNV(int var1, int[] var2, int var3) {
        this.gl.glPointParameterivNV(var1, var2, var3);
    }

    public void glPointParameterivNV(int var1, IntBuffer var2) {
        this.gl.glPointParameterivNV(var1, var2);
    }

    public void glPointSize(float var1) {
        this.gl.glPointSize(var1);
    }

    public int glPollAsyncSGIX(IntBuffer var1) {
        return this.gl.glPollAsyncSGIX(var1);
    }

    public int glPollAsyncSGIX(int[] var1, int var2) {
        return this.gl.glPollAsyncSGIX(var1, var2);
    }

    public int glPollInstrumentsSGIX(int[] var1, int var2) {
        return this.gl.glPollInstrumentsSGIX(var1, var2);
    }

    public int glPollInstrumentsSGIX(IntBuffer var1) {
        return this.gl.glPollInstrumentsSGIX(var1);
    }

    public void glPolygonMode(int var1, int var2) {
        this.gl.glPolygonMode(var1, var2);
    }

    public void glPolygonOffset(float var1, float var2) {
        this.gl.glPolygonOffset(var1, var2);
    }

    public void glPolygonStipple(byte[] var1, int var2) {
        this.gl.glPolygonStipple(var1, var2);
    }

    public void glPolygonStipple(ByteBuffer var1) {
        this.gl.glPolygonStipple(var1);
    }

    public void glPolygonStipple(long var1) {
        this.gl.glPolygonStipple(var1);
    }

    public void glPopAttrib() {
        this.gl.glPopAttrib();
    }

    public void glPopClientAttrib() {
        this.gl.glPopClientAttrib();
    }

    public void glPopMatrix() {
        this.gl.glPopMatrix();
    }

    public void glPopName() {
        this.gl.glPopName();
    }

    public void glPrimitiveRestartIndexNV(int var1) {
        this.gl.glPrimitiveRestartIndexNV(var1);
    }

    public void glPrimitiveRestartNV() {
        this.gl.glPrimitiveRestartNV();
    }

    public void glPrioritizeTextures(int var1, int[] var2, int var3, float[] var4, int var5) {
        this.gl.glPrioritizeTextures(var1, var2, var3, var4, var5);
    }

    public void glPrioritizeTextures(int var1, IntBuffer var2, FloatBuffer var3) {
        this.gl.glPrioritizeTextures(var1, var2, var3);
    }

    public void glProgramBufferParametersIivNV(int var1, int var2, int var3, int var4, int[] var5, int var6) {
        this.gl.glProgramBufferParametersIivNV(var1, var2, var3, var4, var5, var6);
    }

    public void glProgramBufferParametersIivNV(int var1, int var2, int var3, int var4, IntBuffer var5) {
        this.gl.glProgramBufferParametersIivNV(var1, var2, var3, var4, var5);
    }

    public void glProgramBufferParametersIuivNV(int var1, int var2, int var3, int var4, IntBuffer var5) {
        this.gl.glProgramBufferParametersIuivNV(var1, var2, var3, var4, var5);
    }

    public void glProgramBufferParametersIuivNV(int var1, int var2, int var3, int var4, int[] var5, int var6) {
        this.gl.glProgramBufferParametersIuivNV(var1, var2, var3, var4, var5, var6);
    }

    public void glProgramBufferParametersfvNV(int var1, int var2, int var3, int var4, float[] var5, int var6) {
        this.gl.glProgramBufferParametersfvNV(var1, var2, var3, var4, var5, var6);
    }

    public void glProgramBufferParametersfvNV(int var1, int var2, int var3, int var4, FloatBuffer var5) {
        this.gl.glProgramBufferParametersfvNV(var1, var2, var3, var4, var5);
    }

    public void glProgramEnvParameter4dARB(int var1, int var2, double var3, double var5, double var7, double var9) {
        this.gl.glProgramEnvParameter4dARB(var1, var2, var3, var5, var7, var9);
    }

    public void glProgramEnvParameter4dvARB(int var1, int var2, double[] var3, int var4) {
        this.gl.glProgramEnvParameter4dvARB(var1, var2, var3, var4);
    }

    public void glProgramEnvParameter4dvARB(int var1, int var2, DoubleBuffer var3) {
        this.gl.glProgramEnvParameter4dvARB(var1, var2, var3);
    }

    public void glProgramEnvParameter4fARB(int var1, int var2, float var3, float var4, float var5, float var6) {
        this.gl.glProgramEnvParameter4fARB(var1, var2, var3, var4, var5, var6);
    }

    public void glProgramEnvParameter4fvARB(int var1, int var2, FloatBuffer var3) {
        this.gl.glProgramEnvParameter4fvARB(var1, var2, var3);
    }

    public void glProgramEnvParameter4fvARB(int var1, int var2, float[] var3, int var4) {
        this.gl.glProgramEnvParameter4fvARB(var1, var2, var3, var4);
    }

    public void glProgramEnvParameterI4iNV(int var1, int var2, int var3, int var4, int var5, int var6) {
        this.gl.glProgramEnvParameterI4iNV(var1, var2, var3, var4, var5, var6);
    }

    public void glProgramEnvParameterI4ivNV(int var1, int var2, IntBuffer var3) {
        this.gl.glProgramEnvParameterI4ivNV(var1, var2, var3);
    }

    public void glProgramEnvParameterI4ivNV(int var1, int var2, int[] var3, int var4) {
        this.gl.glProgramEnvParameterI4ivNV(var1, var2, var3, var4);
    }

    public void glProgramEnvParameterI4uiNV(int var1, int var2, int var3, int var4, int var5, int var6) {
        this.gl.glProgramEnvParameterI4uiNV(var1, var2, var3, var4, var5, var6);
    }

    public void glProgramEnvParameterI4uivNV(int var1, int var2, IntBuffer var3) {
        this.gl.glProgramEnvParameterI4uivNV(var1, var2, var3);
    }

    public void glProgramEnvParameterI4uivNV(int var1, int var2, int[] var3, int var4) {
        this.gl.glProgramEnvParameterI4uivNV(var1, var2, var3, var4);
    }

    public void glProgramEnvParameters4fvEXT(int var1, int var2, int var3, float[] var4, int var5) {
        this.gl.glProgramEnvParameters4fvEXT(var1, var2, var3, var4, var5);
    }

    public void glProgramEnvParameters4fvEXT(int var1, int var2, int var3, FloatBuffer var4) {
        this.gl.glProgramEnvParameters4fvEXT(var1, var2, var3, var4);
    }

    public void glProgramEnvParametersI4ivNV(int var1, int var2, int var3, int[] var4, int var5) {
        this.gl.glProgramEnvParametersI4ivNV(var1, var2, var3, var4, var5);
    }

    public void glProgramEnvParametersI4ivNV(int var1, int var2, int var3, IntBuffer var4) {
        this.gl.glProgramEnvParametersI4ivNV(var1, var2, var3, var4);
    }

    public void glProgramEnvParametersI4uivNV(int var1, int var2, int var3, IntBuffer var4) {
        this.gl.glProgramEnvParametersI4uivNV(var1, var2, var3, var4);
    }

    public void glProgramEnvParametersI4uivNV(int var1, int var2, int var3, int[] var4, int var5) {
        this.gl.glProgramEnvParametersI4uivNV(var1, var2, var3, var4, var5);
    }

    public void glProgramLocalParameter4dARB(int var1, int var2, double var3, double var5, double var7, double var9) {
        this.gl.glProgramLocalParameter4dARB(var1, var2, var3, var5, var7, var9);
    }

    public void glProgramLocalParameter4dvARB(int var1, int var2, DoubleBuffer var3) {
        this.gl.glProgramLocalParameter4dvARB(var1, var2, var3);
    }

    public void glProgramLocalParameter4dvARB(int var1, int var2, double[] var3, int var4) {
        this.gl.glProgramLocalParameter4dvARB(var1, var2, var3, var4);
    }

    public void glProgramLocalParameter4fARB(int var1, int var2, float var3, float var4, float var5, float var6) {
        this.gl.glProgramLocalParameter4fARB(var1, var2, var3, var4, var5, var6);
    }

    public void glProgramLocalParameter4fvARB(int var1, int var2, float[] var3, int var4) {
        this.gl.glProgramLocalParameter4fvARB(var1, var2, var3, var4);
    }

    public void glProgramLocalParameter4fvARB(int var1, int var2, FloatBuffer var3) {
        this.gl.glProgramLocalParameter4fvARB(var1, var2, var3);
    }

    public void glProgramLocalParameterI4iNV(int var1, int var2, int var3, int var4, int var5, int var6) {
        this.gl.glProgramLocalParameterI4iNV(var1, var2, var3, var4, var5, var6);
    }

    public void glProgramLocalParameterI4ivNV(int var1, int var2, IntBuffer var3) {
        this.gl.glProgramLocalParameterI4ivNV(var1, var2, var3);
    }

    public void glProgramLocalParameterI4ivNV(int var1, int var2, int[] var3, int var4) {
        this.gl.glProgramLocalParameterI4ivNV(var1, var2, var3, var4);
    }

    public void glProgramLocalParameterI4uiNV(int var1, int var2, int var3, int var4, int var5, int var6) {
        this.gl.glProgramLocalParameterI4uiNV(var1, var2, var3, var4, var5, var6);
    }

    public void glProgramLocalParameterI4uivNV(int var1, int var2, IntBuffer var3) {
        this.gl.glProgramLocalParameterI4uivNV(var1, var2, var3);
    }

    public void glProgramLocalParameterI4uivNV(int var1, int var2, int[] var3, int var4) {
        this.gl.glProgramLocalParameterI4uivNV(var1, var2, var3, var4);
    }

    public void glProgramLocalParameters4fvEXT(int var1, int var2, int var3, float[] var4, int var5) {
        this.gl.glProgramLocalParameters4fvEXT(var1, var2, var3, var4, var5);
    }

    public void glProgramLocalParameters4fvEXT(int var1, int var2, int var3, FloatBuffer var4) {
        this.gl.glProgramLocalParameters4fvEXT(var1, var2, var3, var4);
    }

    public void glProgramLocalParametersI4ivNV(int var1, int var2, int var3, IntBuffer var4) {
        this.gl.glProgramLocalParametersI4ivNV(var1, var2, var3, var4);
    }

    public void glProgramLocalParametersI4ivNV(int var1, int var2, int var3, int[] var4, int var5) {
        this.gl.glProgramLocalParametersI4ivNV(var1, var2, var3, var4, var5);
    }

    public void glProgramLocalParametersI4uivNV(int var1, int var2, int var3, int[] var4, int var5) {
        this.gl.glProgramLocalParametersI4uivNV(var1, var2, var3, var4, var5);
    }

    public void glProgramLocalParametersI4uivNV(int var1, int var2, int var3, IntBuffer var4) {
        this.gl.glProgramLocalParametersI4uivNV(var1, var2, var3, var4);
    }

    public void glProgramNamedParameter4dNV(int var1, int var2, String var3, double var4, double var6, double var8, double var10) {
        this.gl.glProgramNamedParameter4dNV(var1, var2, var3, var4, var6, var8, var10);
    }

    public void glProgramNamedParameter4dvNV(int var1, int var2, String var3, double[] var4, int var5) {
        this.gl.glProgramNamedParameter4dvNV(var1, var2, var3, var4, var5);
    }

    public void glProgramNamedParameter4dvNV(int var1, int var2, String var3, DoubleBuffer var4) {
        this.gl.glProgramNamedParameter4dvNV(var1, var2, var3, var4);
    }

    public void glProgramNamedParameter4fNV(int var1, int var2, String var3, float var4, float var5, float var6, float var7) {
        this.gl.glProgramNamedParameter4fNV(var1, var2, var3, var4, var5, var6, var7);
    }

    public void glProgramNamedParameter4fvNV(int var1, int var2, String var3, float[] var4, int var5) {
        this.gl.glProgramNamedParameter4fvNV(var1, var2, var3, var4, var5);
    }

    public void glProgramNamedParameter4fvNV(int var1, int var2, String var3, FloatBuffer var4) {
        this.gl.glProgramNamedParameter4fvNV(var1, var2, var3, var4);
    }

    public void glProgramParameter4dNV(int var1, int var2, double var3, double var5, double var7, double var9) {
        this.gl.glProgramParameter4dNV(var1, var2, var3, var5, var7, var9);
    }

    public void glProgramParameter4dvNV(int var1, int var2, double[] var3, int var4) {
        this.gl.glProgramParameter4dvNV(var1, var2, var3, var4);
    }

    public void glProgramParameter4dvNV(int var1, int var2, DoubleBuffer var3) {
        this.gl.glProgramParameter4dvNV(var1, var2, var3);
    }

    public void glProgramParameter4fNV(int var1, int var2, float var3, float var4, float var5, float var6) {
        this.gl.glProgramParameter4fNV(var1, var2, var3, var4, var5, var6);
    }

    public void glProgramParameter4fvNV(int var1, int var2, float[] var3, int var4) {
        this.gl.glProgramParameter4fvNV(var1, var2, var3, var4);
    }

    public void glProgramParameter4fvNV(int var1, int var2, FloatBuffer var3) {
        this.gl.glProgramParameter4fvNV(var1, var2, var3);
    }

    public void glProgramParameteriEXT(int var1, int var2, int var3) {
        this.gl.glProgramParameteriEXT(var1, var2, var3);
    }

    public void glProgramParameters4dvNV(int var1, int var2, int var3, DoubleBuffer var4) {
        this.gl.glProgramParameters4dvNV(var1, var2, var3, var4);
    }

    public void glProgramParameters4dvNV(int var1, int var2, int var3, double[] var4, int var5) {
        this.gl.glProgramParameters4dvNV(var1, var2, var3, var4, var5);
    }

    public void glProgramParameters4fvNV(int var1, int var2, int var3, float[] var4, int var5) {
        this.gl.glProgramParameters4fvNV(var1, var2, var3, var4, var5);
    }

    public void glProgramParameters4fvNV(int var1, int var2, int var3, FloatBuffer var4) {
        this.gl.glProgramParameters4fvNV(var1, var2, var3, var4);
    }

    public void glProgramStringARB(int var1, int var2, int var3, String var4) {
        this.gl.glProgramStringARB(var1, var2, var3, var4);
    }

    public void glProgramVertexLimitNV(int var1, int var2) {
        this.gl.glProgramVertexLimitNV(var1, var2);
    }

    public void glPushAttrib(int var1) {
        this.gl.glPushAttrib(var1);
    }

    public void glPushClientAttrib(int var1) {
        this.gl.glPushClientAttrib(var1);
    }

    public void glPushMatrix() {
        this.gl.glPushMatrix();
    }

    public void glPushName(int var1) {
        this.gl.glPushName(var1);
    }

    public void glRasterPos2d(double var1, double var3) {
        this.gl.glRasterPos2d(var1, var3);
    }

    public void glRasterPos2dv(DoubleBuffer var1) {
        this.gl.glRasterPos2dv(var1);
    }

    public void glRasterPos2dv(double[] var1, int var2) {
        this.gl.glRasterPos2dv(var1, var2);
    }

    public void glRasterPos2f(float var1, float var2) {
        this.gl.glRasterPos2f(var1, var2);
    }

    public void glRasterPos2fv(float[] var1, int var2) {
        this.gl.glRasterPos2fv(var1, var2);
    }

    public void glRasterPos2fv(FloatBuffer var1) {
        this.gl.glRasterPos2fv(var1);
    }

    public void glRasterPos2i(int var1, int var2) {
        this.gl.glRasterPos2i(var1, var2);
    }

    public void glRasterPos2iv(IntBuffer var1) {
        this.gl.glRasterPos2iv(var1);
    }

    public void glRasterPos2iv(int[] var1, int var2) {
        this.gl.glRasterPos2iv(var1, var2);
    }

    public void glRasterPos2s(short var1, short var2) {
        this.gl.glRasterPos2s(var1, var2);
    }

    public void glRasterPos2sv(ShortBuffer var1) {
        this.gl.glRasterPos2sv(var1);
    }

    public void glRasterPos2sv(short[] var1, int var2) {
        this.gl.glRasterPos2sv(var1, var2);
    }

    public void glRasterPos3d(double var1, double var3, double var5) {
        this.gl.glRasterPos3d(var1, var3, var5);
    }

    public void glRasterPos3dv(DoubleBuffer var1) {
        this.gl.glRasterPos3dv(var1);
    }

    public void glRasterPos3dv(double[] var1, int var2) {
        this.gl.glRasterPos3dv(var1, var2);
    }

    public void glRasterPos3f(float var1, float var2, float var3) {
        this.gl.glRasterPos3f(var1, var2, var3);
    }

    public void glRasterPos3fv(FloatBuffer var1) {
        this.gl.glRasterPos3fv(var1);
    }

    public void glRasterPos3fv(float[] var1, int var2) {
        this.gl.glRasterPos3fv(var1, var2);
    }

    public void glRasterPos3i(int var1, int var2, int var3) {
        this.gl.glRasterPos3i(var1, var2, var3);
    }

    public void glRasterPos3iv(int[] var1, int var2) {
        this.gl.glRasterPos3iv(var1, var2);
    }

    public void glRasterPos3iv(IntBuffer var1) {
        this.gl.glRasterPos3iv(var1);
    }

    public void glRasterPos3s(short var1, short var2, short var3) {
        this.gl.glRasterPos3s(var1, var2, var3);
    }

    public void glRasterPos3sv(ShortBuffer var1) {
        this.gl.glRasterPos3sv(var1);
    }

    public void glRasterPos3sv(short[] var1, int var2) {
        this.gl.glRasterPos3sv(var1, var2);
    }

    public void glRasterPos4d(double var1, double var3, double var5, double var7) {
        this.gl.glRasterPos4d(var1, var3, var5, var7);
    }

    public void glRasterPos4dv(DoubleBuffer var1) {
        this.gl.glRasterPos4dv(var1);
    }

    public void glRasterPos4dv(double[] var1, int var2) {
        this.gl.glRasterPos4dv(var1, var2);
    }

    public void glRasterPos4f(float var1, float var2, float var3, float var4) {
        this.gl.glRasterPos4f(var1, var2, var3, var4);
    }

    public void glRasterPos4fv(float[] var1, int var2) {
        this.gl.glRasterPos4fv(var1, var2);
    }

    public void glRasterPos4fv(FloatBuffer var1) {
        this.gl.glRasterPos4fv(var1);
    }

    public void glRasterPos4i(int var1, int var2, int var3, int var4) {
        this.gl.glRasterPos4i(var1, var2, var3, var4);
    }

    public void glRasterPos4iv(IntBuffer var1) {
        this.gl.glRasterPos4iv(var1);
    }

    public void glRasterPos4iv(int[] var1, int var2) {
        this.gl.glRasterPos4iv(var1, var2);
    }

    public void glRasterPos4s(short var1, short var2, short var3, short var4) {
        this.gl.glRasterPos4s(var1, var2, var3, var4);
    }

    public void glRasterPos4sv(ShortBuffer var1) {
        this.gl.glRasterPos4sv(var1);
    }

    public void glRasterPos4sv(short[] var1, int var2) {
        this.gl.glRasterPos4sv(var1, var2);
    }

    public void glReadBuffer(int var1) {
        this.gl.glReadBuffer(var1);
    }

    public void glReadBufferRegion(int var1, int var2, int var3, int var4, int var5) {
        this.gl.glReadBufferRegion(var1, var2, var3, var4, var5);
    }

    public void glReadInstrumentsSGIX(int var1) {
        this.gl.glReadInstrumentsSGIX(var1);
    }

    public void glReadPixels(int var1, int var2, int var3, int var4, int var5, int var6, Buffer var7) {
        this.gl.glReadPixels(var1, var2, var3, var4, var5, var6, var7);
    }

    public void glReadPixels(int var1, int var2, int var3, int var4, int var5, int var6, long var7) {
        this.gl.glReadPixels(var1, var2, var3, var4, var5, var6, var7);
    }

    public void glRectd(double var1, double var3, double var5, double var7) {
        this.gl.glRectd(var1, var3, var5, var7);
    }

    public void glRectdv(double[] var1, int var2, double[] var3, int var4) {
        this.gl.glRectdv(var1, var2, var3, var4);
    }

    public void glRectdv(DoubleBuffer var1, DoubleBuffer var2) {
        this.gl.glRectdv(var1, var2);
    }

    public void glRectf(float var1, float var2, float var3, float var4) {
        this.gl.glRectf(var1, var2, var3, var4);
    }

    public void glRectfv(FloatBuffer var1, FloatBuffer var2) {
        this.gl.glRectfv(var1, var2);
    }

    public void glRectfv(float[] var1, int var2, float[] var3, int var4) {
        this.gl.glRectfv(var1, var2, var3, var4);
    }

    public void glRecti(int var1, int var2, int var3, int var4) {
        this.gl.glRecti(var1, var2, var3, var4);
    }

    public void glRectiv(IntBuffer var1, IntBuffer var2) {
        this.gl.glRectiv(var1, var2);
    }

    public void glRectiv(int[] var1, int var2, int[] var3, int var4) {
        this.gl.glRectiv(var1, var2, var3, var4);
    }

    public void glRects(short var1, short var2, short var3, short var4) {
        this.gl.glRects(var1, var2, var3, var4);
    }

    public void glRectsv(short[] var1, int var2, short[] var3, int var4) {
        this.gl.glRectsv(var1, var2, var3, var4);
    }

    public void glRectsv(ShortBuffer var1, ShortBuffer var2) {
        this.gl.glRectsv(var1, var2);
    }

    public void glReferencePlaneSGIX(DoubleBuffer var1) {
        this.gl.glReferencePlaneSGIX(var1);
    }

    public void glReferencePlaneSGIX(double[] var1, int var2) {
        this.gl.glReferencePlaneSGIX(var1, var2);
    }

    public int glRenderMode(int var1) {
        return this.gl.glRenderMode(var1);
    }

    public void glRenderbufferStorageEXT(int var1, int var2, int var3, int var4) {
        this.gl.glRenderbufferStorageEXT(var1, var2, var3, var4);
    }

    public void glRenderbufferStorageMultisampleCoverageNV(int var1, int var2, int var3, int var4, int var5, int var6) {
        this.gl.glRenderbufferStorageMultisampleCoverageNV(var1, var2, var3, var4, var5, var6);
    }

    public void glRenderbufferStorageMultisampleEXT(int var1, int var2, int var3, int var4, int var5) {
        this.gl.glRenderbufferStorageMultisampleEXT(var1, var2, var3, var4, var5);
    }

    public void glReplacementCodeuiColor3fVertex3fSUN(int var1, float var2, float var3, float var4, float var5, float var6, float var7) {
        this.gl.glReplacementCodeuiColor3fVertex3fSUN(var1, var2, var3, var4, var5, var6, var7);
    }

    public void glReplacementCodeuiColor3fVertex3fvSUN(IntBuffer var1, FloatBuffer var2, FloatBuffer var3) {
        this.gl.glReplacementCodeuiColor3fVertex3fvSUN(var1, var2, var3);
    }

    public void glReplacementCodeuiColor3fVertex3fvSUN(int[] var1, int var2, float[] var3, int var4, float[] var5, int var6) {
        this.gl.glReplacementCodeuiColor3fVertex3fvSUN(var1, var2, var3, var4, var5, var6);
    }

    public void glReplacementCodeuiColor4fNormal3fVertex3fSUN(int var1, float var2, float var3, float var4, float var5, float var6, float var7, float var8, float var9, float var10, float var11) {
        this.gl.glReplacementCodeuiColor4fNormal3fVertex3fSUN(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11);
    }

    public void glReplacementCodeuiColor4fNormal3fVertex3fvSUN(IntBuffer var1, FloatBuffer var2, FloatBuffer var3, FloatBuffer var4) {
        this.gl.glReplacementCodeuiColor4fNormal3fVertex3fvSUN(var1, var2, var3, var4);
    }

    public void glReplacementCodeuiColor4fNormal3fVertex3fvSUN(int[] var1, int var2, float[] var3, int var4, float[] var5, int var6, float[] var7, int var8) {
        this.gl.glReplacementCodeuiColor4fNormal3fVertex3fvSUN(var1, var2, var3, var4, var5, var6, var7, var8);
    }

    public void glReplacementCodeuiColor4ubVertex3fSUN(int var1, byte var2, byte var3, byte var4, byte var5, float var6, float var7, float var8) {
        this.gl.glReplacementCodeuiColor4ubVertex3fSUN(var1, var2, var3, var4, var5, var6, var7, var8);
    }

    public void glReplacementCodeuiColor4ubVertex3fvSUN(int[] var1, int var2, byte[] var3, int var4, float[] var5, int var6) {
        this.gl.glReplacementCodeuiColor4ubVertex3fvSUN(var1, var2, var3, var4, var5, var6);
    }

    public void glReplacementCodeuiColor4ubVertex3fvSUN(IntBuffer var1, ByteBuffer var2, FloatBuffer var3) {
        this.gl.glReplacementCodeuiColor4ubVertex3fvSUN(var1, var2, var3);
    }

    public void glReplacementCodeuiNormal3fVertex3fSUN(int var1, float var2, float var3, float var4, float var5, float var6, float var7) {
        this.gl.glReplacementCodeuiNormal3fVertex3fSUN(var1, var2, var3, var4, var5, var6, var7);
    }

    public void glReplacementCodeuiNormal3fVertex3fvSUN(IntBuffer var1, FloatBuffer var2, FloatBuffer var3) {
        this.gl.glReplacementCodeuiNormal3fVertex3fvSUN(var1, var2, var3);
    }

    public void glReplacementCodeuiNormal3fVertex3fvSUN(int[] var1, int var2, float[] var3, int var4, float[] var5, int var6) {
        this.gl.glReplacementCodeuiNormal3fVertex3fvSUN(var1, var2, var3, var4, var5, var6);
    }

    public void glReplacementCodeuiTexCoord2fColor4fNormal3fVertex3fSUN(int var1, float var2, float var3, float var4, float var5, float var6, float var7, float var8, float var9, float var10, float var11, float var12, float var13) {
        this.gl.glReplacementCodeuiTexCoord2fColor4fNormal3fVertex3fSUN(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11, var12, var13);
    }

    public void glReplacementCodeuiTexCoord2fColor4fNormal3fVertex3fvSUN(IntBuffer var1, FloatBuffer var2, FloatBuffer var3, FloatBuffer var4, FloatBuffer var5) {
        this.gl.glReplacementCodeuiTexCoord2fColor4fNormal3fVertex3fvSUN(var1, var2, var3, var4, var5);
    }

    public void glReplacementCodeuiTexCoord2fColor4fNormal3fVertex3fvSUN(int[] var1, int var2, float[] var3, int var4, float[] var5, int var6, float[] var7, int var8, float[] var9, int var10) {
        this.gl.glReplacementCodeuiTexCoord2fColor4fNormal3fVertex3fvSUN(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10);
    }

    public void glReplacementCodeuiTexCoord2fNormal3fVertex3fSUN(int var1, float var2, float var3, float var4, float var5, float var6, float var7, float var8, float var9) {
        this.gl.glReplacementCodeuiTexCoord2fNormal3fVertex3fSUN(var1, var2, var3, var4, var5, var6, var7, var8, var9);
    }

    public void glReplacementCodeuiTexCoord2fNormal3fVertex3fvSUN(IntBuffer var1, FloatBuffer var2, FloatBuffer var3, FloatBuffer var4) {
        this.gl.glReplacementCodeuiTexCoord2fNormal3fVertex3fvSUN(var1, var2, var3, var4);
    }

    public void glReplacementCodeuiTexCoord2fNormal3fVertex3fvSUN(int[] var1, int var2, float[] var3, int var4, float[] var5, int var6, float[] var7, int var8) {
        this.gl.glReplacementCodeuiTexCoord2fNormal3fVertex3fvSUN(var1, var2, var3, var4, var5, var6, var7, var8);
    }

    public void glReplacementCodeuiTexCoord2fVertex3fSUN(int var1, float var2, float var3, float var4, float var5, float var6) {
        this.gl.glReplacementCodeuiTexCoord2fVertex3fSUN(var1, var2, var3, var4, var5, var6);
    }

    public void glReplacementCodeuiTexCoord2fVertex3fvSUN(IntBuffer var1, FloatBuffer var2, FloatBuffer var3) {
        this.gl.glReplacementCodeuiTexCoord2fVertex3fvSUN(var1, var2, var3);
    }

    public void glReplacementCodeuiTexCoord2fVertex3fvSUN(int[] var1, int var2, float[] var3, int var4, float[] var5, int var6) {
        this.gl.glReplacementCodeuiTexCoord2fVertex3fvSUN(var1, var2, var3, var4, var5, var6);
    }

    public void glReplacementCodeuiVertex3fSUN(int var1, float var2, float var3, float var4) {
        this.gl.glReplacementCodeuiVertex3fSUN(var1, var2, var3, var4);
    }

    public void glReplacementCodeuiVertex3fvSUN(IntBuffer var1, FloatBuffer var2) {
        this.gl.glReplacementCodeuiVertex3fvSUN(var1, var2);
    }

    public void glReplacementCodeuiVertex3fvSUN(int[] var1, int var2, float[] var3, int var4) {
        this.gl.glReplacementCodeuiVertex3fvSUN(var1, var2, var3, var4);
    }

    public void glRequestResidentProgramsNV(int var1, int[] var2, int var3) {
        this.gl.glRequestResidentProgramsNV(var1, var2, var3);
    }

    public void glRequestResidentProgramsNV(int var1, IntBuffer var2) {
        this.gl.glRequestResidentProgramsNV(var1, var2);
    }

    public void glResetHistogram(int var1) {
        this.gl.glResetHistogram(var1);
    }

    public void glResetMinmax(int var1) {
        this.gl.glResetMinmax(var1);
    }

    public void glResizeBuffersMESA() {
        this.gl.glResizeBuffersMESA();
    }

    public void glRotated(double var1, double var3, double var5, double var7) {
        this.gl.glRotated(var1, var3, var5, var7);
    }

    public void glRotatef(float var1, float var2, float var3, float var4) {
        this.gl.glRotatef(var1, var2, var3, var4);
    }

    public void glSampleCoverage(float var1, boolean var2) {
        this.gl.glSampleCoverage(var1, var2);
    }

    public void glSampleMapATI(int var1, int var2, int var3) {
        this.gl.glSampleMapATI(var1, var2, var3);
    }

    public void glSampleMaskEXT(float var1, boolean var2) {
        this.gl.glSampleMaskEXT(var1, var2);
    }

    public void glSampleMaskSGIS(float var1, boolean var2) {
        this.gl.glSampleMaskSGIS(var1, var2);
    }

    public void glSamplePatternEXT(int var1) {
        this.gl.glSamplePatternEXT(var1);
    }

    public void glSamplePatternSGIS(int var1) {
        this.gl.glSamplePatternSGIS(var1);
    }

    public void glScaled(double var1, double var3, double var5) {
        this.gl.glScaled(var1, var3, var5);
    }

    public void glScalef(float var1, float var2, float var3) {
        this.gl.glScalef(var1, var2, var3);
    }

    public void glScissor(int var1, int var2, int var3, int var4) {
        this.gl.glScissor(var1, var2, var3, var4);
    }

    public void glSecondaryColor3b(byte var1, byte var2, byte var3) {
        this.gl.glSecondaryColor3b(var1, var2, var3);
    }

    public void glSecondaryColor3bEXT(byte var1, byte var2, byte var3) {
        this.gl.glSecondaryColor3bEXT(var1, var2, var3);
    }

    public void glSecondaryColor3bv(byte[] var1, int var2) {
        this.gl.glSecondaryColor3bv(var1, var2);
    }

    public void glSecondaryColor3bv(ByteBuffer var1) {
        this.gl.glSecondaryColor3bv(var1);
    }

    public void glSecondaryColor3bvEXT(ByteBuffer var1) {
        this.gl.glSecondaryColor3bvEXT(var1);
    }

    public void glSecondaryColor3bvEXT(byte[] var1, int var2) {
        this.gl.glSecondaryColor3bvEXT(var1, var2);
    }

    public void glSecondaryColor3d(double var1, double var3, double var5) {
        this.gl.glSecondaryColor3d(var1, var3, var5);
    }

    public void glSecondaryColor3dEXT(double var1, double var3, double var5) {
        this.gl.glSecondaryColor3dEXT(var1, var3, var5);
    }

    public void glSecondaryColor3dv(double[] var1, int var2) {
        this.gl.glSecondaryColor3dv(var1, var2);
    }

    public void glSecondaryColor3dv(DoubleBuffer var1) {
        this.gl.glSecondaryColor3dv(var1);
    }

    public void glSecondaryColor3dvEXT(double[] var1, int var2) {
        this.gl.glSecondaryColor3dvEXT(var1, var2);
    }

    public void glSecondaryColor3dvEXT(DoubleBuffer var1) {
        this.gl.glSecondaryColor3dvEXT(var1);
    }

    public void glSecondaryColor3f(float var1, float var2, float var3) {
        this.gl.glSecondaryColor3f(var1, var2, var3);
    }

    public void glSecondaryColor3fEXT(float var1, float var2, float var3) {
        this.gl.glSecondaryColor3fEXT(var1, var2, var3);
    }

    public void glSecondaryColor3fv(FloatBuffer var1) {
        this.gl.glSecondaryColor3fv(var1);
    }

    public void glSecondaryColor3fv(float[] var1, int var2) {
        this.gl.glSecondaryColor3fv(var1, var2);
    }

    public void glSecondaryColor3fvEXT(float[] var1, int var2) {
        this.gl.glSecondaryColor3fvEXT(var1, var2);
    }

    public void glSecondaryColor3fvEXT(FloatBuffer var1) {
        this.gl.glSecondaryColor3fvEXT(var1);
    }

    public void glSecondaryColor3hNV(short var1, short var2, short var3) {
        this.gl.glSecondaryColor3hNV(var1, var2, var3);
    }

    public void glSecondaryColor3hvNV(short[] var1, int var2) {
        this.gl.glSecondaryColor3hvNV(var1, var2);
    }

    public void glSecondaryColor3hvNV(ShortBuffer var1) {
        this.gl.glSecondaryColor3hvNV(var1);
    }

    public void glSecondaryColor3i(int var1, int var2, int var3) {
        this.gl.glSecondaryColor3i(var1, var2, var3);
    }

    public void glSecondaryColor3iEXT(int var1, int var2, int var3) {
        this.gl.glSecondaryColor3iEXT(var1, var2, var3);
    }

    public void glSecondaryColor3iv(int[] var1, int var2) {
        this.gl.glSecondaryColor3iv(var1, var2);
    }

    public void glSecondaryColor3iv(IntBuffer var1) {
        this.gl.glSecondaryColor3iv(var1);
    }

    public void glSecondaryColor3ivEXT(IntBuffer var1) {
        this.gl.glSecondaryColor3ivEXT(var1);
    }

    public void glSecondaryColor3ivEXT(int[] var1, int var2) {
        this.gl.glSecondaryColor3ivEXT(var1, var2);
    }

    public void glSecondaryColor3s(short var1, short var2, short var3) {
        this.gl.glSecondaryColor3s(var1, var2, var3);
    }

    public void glSecondaryColor3sEXT(short var1, short var2, short var3) {
        this.gl.glSecondaryColor3sEXT(var1, var2, var3);
    }

    public void glSecondaryColor3sv(ShortBuffer var1) {
        this.gl.glSecondaryColor3sv(var1);
    }

    public void glSecondaryColor3sv(short[] var1, int var2) {
        this.gl.glSecondaryColor3sv(var1, var2);
    }

    public void glSecondaryColor3svEXT(ShortBuffer var1) {
        this.gl.glSecondaryColor3svEXT(var1);
    }

    public void glSecondaryColor3svEXT(short[] var1, int var2) {
        this.gl.glSecondaryColor3svEXT(var1, var2);
    }

    public void glSecondaryColor3ub(byte var1, byte var2, byte var3) {
        this.gl.glSecondaryColor3ub(var1, var2, var3);
    }

    public void glSecondaryColor3ubEXT(byte var1, byte var2, byte var3) {
        this.gl.glSecondaryColor3ubEXT(var1, var2, var3);
    }

    public void glSecondaryColor3ubv(byte[] var1, int var2) {
        this.gl.glSecondaryColor3ubv(var1, var2);
    }

    public void glSecondaryColor3ubv(ByteBuffer var1) {
        this.gl.glSecondaryColor3ubv(var1);
    }

    public void glSecondaryColor3ubvEXT(byte[] var1, int var2) {
        this.gl.glSecondaryColor3ubvEXT(var1, var2);
    }

    public void glSecondaryColor3ubvEXT(ByteBuffer var1) {
        this.gl.glSecondaryColor3ubvEXT(var1);
    }

    public void glSecondaryColor3ui(int var1, int var2, int var3) {
        this.gl.glSecondaryColor3ui(var1, var2, var3);
    }

    public void glSecondaryColor3uiEXT(int var1, int var2, int var3) {
        this.gl.glSecondaryColor3uiEXT(var1, var2, var3);
    }

    public void glSecondaryColor3uiv(IntBuffer var1) {
        this.gl.glSecondaryColor3uiv(var1);
    }

    public void glSecondaryColor3uiv(int[] var1, int var2) {
        this.gl.glSecondaryColor3uiv(var1, var2);
    }

    public void glSecondaryColor3uivEXT(int[] var1, int var2) {
        this.gl.glSecondaryColor3uivEXT(var1, var2);
    }

    public void glSecondaryColor3uivEXT(IntBuffer var1) {
        this.gl.glSecondaryColor3uivEXT(var1);
    }

    public void glSecondaryColor3us(short var1, short var2, short var3) {
        this.gl.glSecondaryColor3us(var1, var2, var3);
    }

    public void glSecondaryColor3usEXT(short var1, short var2, short var3) {
        this.gl.glSecondaryColor3usEXT(var1, var2, var3);
    }

    public void glSecondaryColor3usv(ShortBuffer var1) {
        this.gl.glSecondaryColor3usv(var1);
    }

    public void glSecondaryColor3usv(short[] var1, int var2) {
        this.gl.glSecondaryColor3usv(var1, var2);
    }

    public void glSecondaryColor3usvEXT(short[] var1, int var2) {
        this.gl.glSecondaryColor3usvEXT(var1, var2);
    }

    public void glSecondaryColor3usvEXT(ShortBuffer var1) {
        this.gl.glSecondaryColor3usvEXT(var1);
    }

    public void glSecondaryColorPointer(int var1, int var2, int var3, long var4) {
        this.gl.glSecondaryColorPointer(var1, var2, var3, var4);
    }

    public void glSecondaryColorPointer(int var1, int var2, int var3, Buffer var4) {
        this.gl.glSecondaryColorPointer(var1, var2, var3, var4);
    }

    public void glSecondaryColorPointerEXT(int var1, int var2, int var3, long var4) {
        this.gl.glSecondaryColorPointerEXT(var1, var2, var3, var4);
    }

    public void glSecondaryColorPointerEXT(int var1, int var2, int var3, Buffer var4) {
        this.gl.glSecondaryColorPointerEXT(var1, var2, var3, var4);
    }

    public void glSelectBuffer(int var1, IntBuffer var2) {
        this.gl.glSelectBuffer(var1, var2);
    }

    public void glSeparableFilter2D(int var1, int var2, int var3, int var4, int var5, int var6, long var7, long var9) {
        this.gl.glSeparableFilter2D(var1, var2, var3, var4, var5, var6, var7, var9);
    }

    public void glSeparableFilter2D(int var1, int var2, int var3, int var4, int var5, int var6, Buffer var7, Buffer var8) {
        this.gl.glSeparableFilter2D(var1, var2, var3, var4, var5, var6, var7, var8);
    }

    public void glSetFenceAPPLE(int var1) {
        this.gl.glSetFenceAPPLE(var1);
    }

    public void glSetFenceNV(int var1, int var2) {
        this.gl.glSetFenceNV(var1, var2);
    }

    public void glSetFragmentShaderConstantATI(int var1, float[] var2, int var3) {
        this.gl.glSetFragmentShaderConstantATI(var1, var2, var3);
    }

    public void glSetFragmentShaderConstantATI(int var1, FloatBuffer var2) {
        this.gl.glSetFragmentShaderConstantATI(var1, var2);
    }

    public void glSetInvariantEXT(int var1, int var2, Buffer var3) {
        this.gl.glSetInvariantEXT(var1, var2, var3);
    }

    public void glSetLocalConstantEXT(int var1, int var2, Buffer var3) {
        this.gl.glSetLocalConstantEXT(var1, var2, var3);
    }

    public void glShadeModel(int var1) {
        this.gl.glShadeModel(var1);
    }

    public void glShaderOp1EXT(int var1, int var2, int var3) {
        this.gl.glShaderOp1EXT(var1, var2, var3);
    }

    public void glShaderOp2EXT(int var1, int var2, int var3, int var4) {
        this.gl.glShaderOp2EXT(var1, var2, var3, var4);
    }

    public void glShaderOp3EXT(int var1, int var2, int var3, int var4, int var5) {
        this.gl.glShaderOp3EXT(var1, var2, var3, var4, var5);
    }

    public void glShaderSource(int var1, int var2, String[] var3, int[] var4, int var5) {
        this.gl.glShaderSource(var1, var2, var3, var4, var5);
    }

    public void glShaderSource(int var1, int var2, String[] var3, IntBuffer var4) {
        this.gl.glShaderSource(var1, var2, var3, var4);
    }

    public void glShaderSourceARB(int var1, int var2, String[] var3, IntBuffer var4) {
        this.gl.glShaderSourceARB(var1, var2, var3, var4);
    }

    public void glShaderSourceARB(int var1, int var2, String[] var3, int[] var4, int var5) {
        this.gl.glShaderSourceARB(var1, var2, var3, var4, var5);
    }

    public void glSharpenTexFuncSGIS(int var1, int var2, FloatBuffer var3) {
        this.gl.glSharpenTexFuncSGIS(var1, var2, var3);
    }

    public void glSharpenTexFuncSGIS(int var1, int var2, float[] var3, int var4) {
        this.gl.glSharpenTexFuncSGIS(var1, var2, var3, var4);
    }

    public void glSpriteParameterfSGIX(int var1, float var2) {
        this.gl.glSpriteParameterfSGIX(var1, var2);
    }

    public void glSpriteParameterfvSGIX(int var1, float[] var2, int var3) {
        this.gl.glSpriteParameterfvSGIX(var1, var2, var3);
    }

    public void glSpriteParameterfvSGIX(int var1, FloatBuffer var2) {
        this.gl.glSpriteParameterfvSGIX(var1, var2);
    }

    public void glSpriteParameteriSGIX(int var1, int var2) {
        this.gl.glSpriteParameteriSGIX(var1, var2);
    }

    public void glSpriteParameterivSGIX(int var1, int[] var2, int var3) {
        this.gl.glSpriteParameterivSGIX(var1, var2, var3);
    }

    public void glSpriteParameterivSGIX(int var1, IntBuffer var2) {
        this.gl.glSpriteParameterivSGIX(var1, var2);
    }

    public void glStartInstrumentsSGIX() {
        this.gl.glStartInstrumentsSGIX();
    }

    public void glStencilClearTagEXT(int var1, int var2) {
        this.gl.glStencilClearTagEXT(var1, var2);
    }

    public void glStencilFunc(int var1, int var2, int var3) {
        this.gl.glStencilFunc(var1, var2, var3);
    }

    public void glStencilFuncSeparate(int var1, int var2, int var3, int var4) {
        this.gl.glStencilFuncSeparate(var1, var2, var3, var4);
    }

    public void glStencilFuncSeparateATI(int var1, int var2, int var3, int var4) {
        this.gl.glStencilFuncSeparateATI(var1, var2, var3, var4);
    }

    public void glStencilMask(int var1) {
        this.gl.glStencilMask(var1);
    }

    public void glStencilMaskSeparate(int var1, int var2) {
        this.gl.glStencilMaskSeparate(var1, var2);
    }

    public void glStencilOp(int var1, int var2, int var3) {
        this.gl.glStencilOp(var1, var2, var3);
    }

    public void glStencilOpSeparate(int var1, int var2, int var3, int var4) {
        this.gl.glStencilOpSeparate(var1, var2, var3, var4);
    }

    public void glStencilOpSeparateATI(int var1, int var2, int var3, int var4) {
        this.gl.glStencilOpSeparateATI(var1, var2, var3, var4);
    }

    public void glStopInstrumentsSGIX(int var1) {
        this.gl.glStopInstrumentsSGIX(var1);
    }

    public void glStringMarkerGREMEDY(int var1, Buffer var2) {
        this.gl.glStringMarkerGREMEDY(var1, var2);
    }

    public void glSwapAPPLE() {
        this.gl.glSwapAPPLE();
    }

    public void glSwizzleEXT(int var1, int var2, int var3, int var4, int var5, int var6) {
        this.gl.glSwizzleEXT(var1, var2, var3, var4, var5, var6);
    }

    public void glTagSampleBufferSGIX() {
        this.gl.glTagSampleBufferSGIX();
    }

    public void glTbufferMask3DFX(int var1) {
        this.gl.glTbufferMask3DFX(var1);
    }

    public boolean glTestFenceAPPLE(int var1) {
        return this.gl.glTestFenceAPPLE(var1);
    }

    public boolean glTestFenceNV(int var1) {
        return this.gl.glTestFenceNV(var1);
    }

    public boolean glTestObjectAPPLE(int var1, int var2) {
        return this.gl.glTestObjectAPPLE(var1, var2);
    }

    public void glTexBufferEXT(int var1, int var2, int var3) {
        this.gl.glTexBufferEXT(var1, var2, var3);
    }

    public void glTexBumpParameterfvATI(int var1, float[] var2, int var3) {
        this.gl.glTexBumpParameterfvATI(var1, var2, var3);
    }

    public void glTexBumpParameterfvATI(int var1, FloatBuffer var2) {
        this.gl.glTexBumpParameterfvATI(var1, var2);
    }

    public void glTexBumpParameterivATI(int var1, IntBuffer var2) {
        this.gl.glTexBumpParameterivATI(var1, var2);
    }

    public void glTexBumpParameterivATI(int var1, int[] var2, int var3) {
        this.gl.glTexBumpParameterivATI(var1, var2, var3);
    }

    public void glTexCoord1d(double var1) {
        this.gl.glTexCoord1d(var1);
    }

    public void glTexCoord1dv(DoubleBuffer var1) {
        this.gl.glTexCoord1dv(var1);
    }

    public void glTexCoord1dv(double[] var1, int var2) {
        this.gl.glTexCoord1dv(var1, var2);
    }

    public void glTexCoord1f(float var1) {
        this.gl.glTexCoord1f(var1);
    }

    public void glTexCoord1fv(FloatBuffer var1) {
        this.gl.glTexCoord1fv(var1);
    }

    public void glTexCoord1fv(float[] var1, int var2) {
        this.gl.glTexCoord1fv(var1, var2);
    }

    public void glTexCoord1hNV(short var1) {
        this.gl.glTexCoord1hNV(var1);
    }

    public void glTexCoord1hvNV(ShortBuffer var1) {
        this.gl.glTexCoord1hvNV(var1);
    }

    public void glTexCoord1hvNV(short[] var1, int var2) {
        this.gl.glTexCoord1hvNV(var1, var2);
    }

    public void glTexCoord1i(int var1) {
        this.gl.glTexCoord1i(var1);
    }

    public void glTexCoord1iv(IntBuffer var1) {
        this.gl.glTexCoord1iv(var1);
    }

    public void glTexCoord1iv(int[] var1, int var2) {
        this.gl.glTexCoord1iv(var1, var2);
    }

    public void glTexCoord1s(short var1) {
        this.gl.glTexCoord1s(var1);
    }

    public void glTexCoord1sv(short[] var1, int var2) {
        this.gl.glTexCoord1sv(var1, var2);
    }

    public void glTexCoord1sv(ShortBuffer var1) {
        this.gl.glTexCoord1sv(var1);
    }

    public void glTexCoord2d(double var1, double var3) {
        this.gl.glTexCoord2d(var1, var3);
    }

    public void glTexCoord2dv(double[] var1, int var2) {
        this.gl.glTexCoord2dv(var1, var2);
    }

    public void glTexCoord2dv(DoubleBuffer var1) {
        this.gl.glTexCoord2dv(var1);
    }

    public void glTexCoord2f(float var1, float var2) {
        this.gl.glTexCoord2f(var1, var2);
    }

    public void glTexCoord2fColor3fVertex3fSUN(float var1, float var2, float var3, float var4, float var5, float var6, float var7, float var8) {
        this.gl.glTexCoord2fColor3fVertex3fSUN(var1, var2, var3, var4, var5, var6, var7, var8);
    }

    public void glTexCoord2fColor3fVertex3fvSUN(float[] var1, int var2, float[] var3, int var4, float[] var5, int var6) {
        this.gl.glTexCoord2fColor3fVertex3fvSUN(var1, var2, var3, var4, var5, var6);
    }

    public void glTexCoord2fColor3fVertex3fvSUN(FloatBuffer var1, FloatBuffer var2, FloatBuffer var3) {
        this.gl.glTexCoord2fColor3fVertex3fvSUN(var1, var2, var3);
    }

    public void glTexCoord2fColor4fNormal3fVertex3fSUN(float var1, float var2, float var3, float var4, float var5, float var6, float var7, float var8, float var9, float var10, float var11, float var12) {
        this.gl.glTexCoord2fColor4fNormal3fVertex3fSUN(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11, var12);
    }

    public void glTexCoord2fColor4fNormal3fVertex3fvSUN(float[] var1, int var2, float[] var3, int var4, float[] var5, int var6, float[] var7, int var8) {
        this.gl.glTexCoord2fColor4fNormal3fVertex3fvSUN(var1, var2, var3, var4, var5, var6, var7, var8);
    }

    public void glTexCoord2fColor4fNormal3fVertex3fvSUN(FloatBuffer var1, FloatBuffer var2, FloatBuffer var3, FloatBuffer var4) {
        this.gl.glTexCoord2fColor4fNormal3fVertex3fvSUN(var1, var2, var3, var4);
    }

    public void glTexCoord2fColor4ubVertex3fSUN(float var1, float var2, byte var3, byte var4, byte var5, byte var6, float var7, float var8, float var9) {
        this.gl.glTexCoord2fColor4ubVertex3fSUN(var1, var2, var3, var4, var5, var6, var7, var8, var9);
    }

    public void glTexCoord2fColor4ubVertex3fvSUN(FloatBuffer var1, ByteBuffer var2, FloatBuffer var3) {
        this.gl.glTexCoord2fColor4ubVertex3fvSUN(var1, var2, var3);
    }

    public void glTexCoord2fColor4ubVertex3fvSUN(float[] var1, int var2, byte[] var3, int var4, float[] var5, int var6) {
        this.gl.glTexCoord2fColor4ubVertex3fvSUN(var1, var2, var3, var4, var5, var6);
    }

    public void glTexCoord2fNormal3fVertex3fSUN(float var1, float var2, float var3, float var4, float var5, float var6, float var7, float var8) {
        this.gl.glTexCoord2fNormal3fVertex3fSUN(var1, var2, var3, var4, var5, var6, var7, var8);
    }

    public void glTexCoord2fNormal3fVertex3fvSUN(FloatBuffer var1, FloatBuffer var2, FloatBuffer var3) {
        this.gl.glTexCoord2fNormal3fVertex3fvSUN(var1, var2, var3);
    }

    public void glTexCoord2fNormal3fVertex3fvSUN(float[] var1, int var2, float[] var3, int var4, float[] var5, int var6) {
        this.gl.glTexCoord2fNormal3fVertex3fvSUN(var1, var2, var3, var4, var5, var6);
    }

    public void glTexCoord2fVertex3fSUN(float var1, float var2, float var3, float var4, float var5) {
        this.gl.glTexCoord2fVertex3fSUN(var1, var2, var3, var4, var5);
    }

    public void glTexCoord2fVertex3fvSUN(float[] var1, int var2, float[] var3, int var4) {
        this.gl.glTexCoord2fVertex3fvSUN(var1, var2, var3, var4);
    }

    public void glTexCoord2fVertex3fvSUN(FloatBuffer var1, FloatBuffer var2) {
        this.gl.glTexCoord2fVertex3fvSUN(var1, var2);
    }

    public void glTexCoord2fv(FloatBuffer var1) {
        this.gl.glTexCoord2fv(var1);
    }

    public void glTexCoord2fv(float[] var1, int var2) {
        this.gl.glTexCoord2fv(var1, var2);
    }

    public void glTexCoord2hNV(short var1, short var2) {
        this.gl.glTexCoord2hNV(var1, var2);
    }

    public void glTexCoord2hvNV(ShortBuffer var1) {
        this.gl.glTexCoord2hvNV(var1);
    }

    public void glTexCoord2hvNV(short[] var1, int var2) {
        this.gl.glTexCoord2hvNV(var1, var2);
    }

    public void glTexCoord2i(int var1, int var2) {
        this.gl.glTexCoord2i(var1, var2);
    }

    public void glTexCoord2iv(IntBuffer var1) {
        this.gl.glTexCoord2iv(var1);
    }

    public void glTexCoord2iv(int[] var1, int var2) {
        this.gl.glTexCoord2iv(var1, var2);
    }

    public void glTexCoord2s(short var1, short var2) {
        this.gl.glTexCoord2s(var1, var2);
    }

    public void glTexCoord2sv(short[] var1, int var2) {
        this.gl.glTexCoord2sv(var1, var2);
    }

    public void glTexCoord2sv(ShortBuffer var1) {
        this.gl.glTexCoord2sv(var1);
    }

    public void glTexCoord3d(double var1, double var3, double var5) {
        this.gl.glTexCoord3d(var1, var3, var5);
    }

    public void glTexCoord3dv(DoubleBuffer var1) {
        this.gl.glTexCoord3dv(var1);
    }

    public void glTexCoord3dv(double[] var1, int var2) {
        this.gl.glTexCoord3dv(var1, var2);
    }

    public void glTexCoord3f(float var1, float var2, float var3) {
        this.gl.glTexCoord3f(var1, var2, var3);
    }

    public void glTexCoord3fv(FloatBuffer var1) {
        this.gl.glTexCoord3fv(var1);
    }

    public void glTexCoord3fv(float[] var1, int var2) {
        this.gl.glTexCoord3fv(var1, var2);
    }

    public void glTexCoord3hNV(short var1, short var2, short var3) {
        this.gl.glTexCoord3hNV(var1, var2, var3);
    }

    public void glTexCoord3hvNV(ShortBuffer var1) {
        this.gl.glTexCoord3hvNV(var1);
    }

    public void glTexCoord3hvNV(short[] var1, int var2) {
        this.gl.glTexCoord3hvNV(var1, var2);
    }

    public void glTexCoord3i(int var1, int var2, int var3) {
        this.gl.glTexCoord3i(var1, var2, var3);
    }

    public void glTexCoord3iv(int[] var1, int var2) {
        this.gl.glTexCoord3iv(var1, var2);
    }

    public void glTexCoord3iv(IntBuffer var1) {
        this.gl.glTexCoord3iv(var1);
    }

    public void glTexCoord3s(short var1, short var2, short var3) {
        this.gl.glTexCoord3s(var1, var2, var3);
    }

    public void glTexCoord3sv(short[] var1, int var2) {
        this.gl.glTexCoord3sv(var1, var2);
    }

    public void glTexCoord3sv(ShortBuffer var1) {
        this.gl.glTexCoord3sv(var1);
    }

    public void glTexCoord4d(double var1, double var3, double var5, double var7) {
        this.gl.glTexCoord4d(var1, var3, var5, var7);
    }

    public void glTexCoord4dv(DoubleBuffer var1) {
        this.gl.glTexCoord4dv(var1);
    }

    public void glTexCoord4dv(double[] var1, int var2) {
        this.gl.glTexCoord4dv(var1, var2);
    }

    public void glTexCoord4f(float var1, float var2, float var3, float var4) {
        this.gl.glTexCoord4f(var1, var2, var3, var4);
    }

    public void glTexCoord4fColor4fNormal3fVertex4fSUN(float var1, float var2, float var3, float var4, float var5, float var6, float var7, float var8, float var9, float var10, float var11, float var12, float var13, float var14, float var15) {
        this.gl.glTexCoord4fColor4fNormal3fVertex4fSUN(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11, var12, var13, var14, var15);
    }

    public void glTexCoord4fColor4fNormal3fVertex4fvSUN(FloatBuffer var1, FloatBuffer var2, FloatBuffer var3, FloatBuffer var4) {
        this.gl.glTexCoord4fColor4fNormal3fVertex4fvSUN(var1, var2, var3, var4);
    }

    public void glTexCoord4fColor4fNormal3fVertex4fvSUN(float[] var1, int var2, float[] var3, int var4, float[] var5, int var6, float[] var7, int var8) {
        this.gl.glTexCoord4fColor4fNormal3fVertex4fvSUN(var1, var2, var3, var4, var5, var6, var7, var8);
    }

    public void glTexCoord4fVertex4fSUN(float var1, float var2, float var3, float var4, float var5, float var6, float var7, float var8) {
        this.gl.glTexCoord4fVertex4fSUN(var1, var2, var3, var4, var5, var6, var7, var8);
    }

    public void glTexCoord4fVertex4fvSUN(FloatBuffer var1, FloatBuffer var2) {
        this.gl.glTexCoord4fVertex4fvSUN(var1, var2);
    }

    public void glTexCoord4fVertex4fvSUN(float[] var1, int var2, float[] var3, int var4) {
        this.gl.glTexCoord4fVertex4fvSUN(var1, var2, var3, var4);
    }

    public void glTexCoord4fv(float[] var1, int var2) {
        this.gl.glTexCoord4fv(var1, var2);
    }

    public void glTexCoord4fv(FloatBuffer var1) {
        this.gl.glTexCoord4fv(var1);
    }

    public void glTexCoord4hNV(short var1, short var2, short var3, short var4) {
        this.gl.glTexCoord4hNV(var1, var2, var3, var4);
    }

    public void glTexCoord4hvNV(ShortBuffer var1) {
        this.gl.glTexCoord4hvNV(var1);
    }

    public void glTexCoord4hvNV(short[] var1, int var2) {
        this.gl.glTexCoord4hvNV(var1, var2);
    }

    public void glTexCoord4i(int var1, int var2, int var3, int var4) {
        this.gl.glTexCoord4i(var1, var2, var3, var4);
    }

    public void glTexCoord4iv(int[] var1, int var2) {
        this.gl.glTexCoord4iv(var1, var2);
    }

    public void glTexCoord4iv(IntBuffer var1) {
        this.gl.glTexCoord4iv(var1);
    }

    public void glTexCoord4s(short var1, short var2, short var3, short var4) {
        this.gl.glTexCoord4s(var1, var2, var3, var4);
    }

    public void glTexCoord4sv(short[] var1, int var2) {
        this.gl.glTexCoord4sv(var1, var2);
    }

    public void glTexCoord4sv(ShortBuffer var1) {
        this.gl.glTexCoord4sv(var1);
    }

    public void glTexCoordPointer(int var1, int var2, int var3, long var4) {
        this.gl.glTexCoordPointer(var1, var2, var3, var4);
    }

    public void glTexCoordPointer(int var1, int var2, int var3, Buffer var4) {
        this.gl.glTexCoordPointer(var1, var2, var3, var4);
    }

    public void glTexEnvf(int var1, int var2, float var3) {
        this.gl.glTexEnvf(var1, var2, var3);
    }

    public void glTexEnvfv(int var1, int var2, FloatBuffer var3) {
        this.gl.glTexEnvfv(var1, var2, var3);
    }

    public void glTexEnvfv(int var1, int var2, float[] var3, int var4) {
        this.gl.glTexEnvfv(var1, var2, var3, var4);
    }

    public void glTexEnvi(int var1, int var2, int var3) {
        this.gl.glTexEnvi(var1, var2, var3);
    }

    public void glTexEnviv(int var1, int var2, IntBuffer var3) {
        this.gl.glTexEnviv(var1, var2, var3);
    }

    public void glTexEnviv(int var1, int var2, int[] var3, int var4) {
        this.gl.glTexEnviv(var1, var2, var3, var4);
    }

    public void glTexFilterFuncSGIS(int var1, int var2, int var3, FloatBuffer var4) {
        this.gl.glTexFilterFuncSGIS(var1, var2, var3, var4);
    }

    public void glTexFilterFuncSGIS(int var1, int var2, int var3, float[] var4, int var5) {
        this.gl.glTexFilterFuncSGIS(var1, var2, var3, var4, var5);
    }

    public void glTexGend(int var1, int var2, double var3) {
        this.gl.glTexGend(var1, var2, var3);
    }

    public void glTexGendv(int var1, int var2, double[] var3, int var4) {
        this.gl.glTexGendv(var1, var2, var3, var4);
    }

    public void glTexGendv(int var1, int var2, DoubleBuffer var3) {
        this.gl.glTexGendv(var1, var2, var3);
    }

    public void glTexGenf(int var1, int var2, float var3) {
        this.gl.glTexGenf(var1, var2, var3);
    }

    public void glTexGenfv(int var1, int var2, FloatBuffer var3) {
        this.gl.glTexGenfv(var1, var2, var3);
    }

    public void glTexGenfv(int var1, int var2, float[] var3, int var4) {
        this.gl.glTexGenfv(var1, var2, var3, var4);
    }

    public void glTexGeni(int var1, int var2, int var3) {
        this.gl.glTexGeni(var1, var2, var3);
    }

    public void glTexGeniv(int var1, int var2, int[] var3, int var4) {
        this.gl.glTexGeniv(var1, var2, var3, var4);
    }

    public void glTexGeniv(int var1, int var2, IntBuffer var3) {
        this.gl.glTexGeniv(var1, var2, var3);
    }

    public void glTexImage1D(int var1, int var2, int var3, int var4, int var5, int var6, int var7, long var8) {
        this.gl.glTexImage1D(var1, var2, var3, var4, var5, var6, var7, var8);
    }

    public void glTexImage1D(int var1, int var2, int var3, int var4, int var5, int var6, int var7, Buffer var8) {
        this.gl.glTexImage1D(var1, var2, var3, var4, var5, var6, var7, var8);
    }

    public void glTexImage2D(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, long var9) {
        this.gl.glTexImage2D(var1, var2, var3, var4, var5, var6, var7, var8, var9);
    }

    public void glTexImage2D(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, Buffer var9) {
        this.gl.glTexImage2D(var1, var2, var3, var4, var5, var6, var7, var8, var9);
    }

    public void glTexImage3D(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, long var10) {
        this.gl.glTexImage3D(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10);
    }

    public void glTexImage3D(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, Buffer var10) {
        this.gl.glTexImage3D(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10);
    }

    public void glTexImage4DSGIS(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10, Buffer var11) {
        this.gl.glTexImage4DSGIS(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11);
    }

    public void glTexParameterIivEXT(int var1, int var2, IntBuffer var3) {
        this.gl.glTexParameterIivEXT(var1, var2, var3);
    }

    public void glTexParameterIivEXT(int var1, int var2, int[] var3, int var4) {
        this.gl.glTexParameterIivEXT(var1, var2, var3, var4);
    }

    public void glTexParameterIuivEXT(int var1, int var2, int[] var3, int var4) {
        this.gl.glTexParameterIuivEXT(var1, var2, var3, var4);
    }

    public void glTexParameterIuivEXT(int var1, int var2, IntBuffer var3) {
        this.gl.glTexParameterIuivEXT(var1, var2, var3);
    }

    public void glTexParameterf(int var1, int var2, float var3) {
        this.gl.glTexParameterf(var1, var2, var3);
    }

    public void glTexParameterfv(int var1, int var2, float[] var3, int var4) {
        this.gl.glTexParameterfv(var1, var2, var3, var4);
    }

    public void glTexParameterfv(int var1, int var2, FloatBuffer var3) {
        this.gl.glTexParameterfv(var1, var2, var3);
    }

    public void glTexParameteri(int var1, int var2, int var3) {
        this.gl.glTexParameteri(var1, var2, var3);
    }

    public void glTexParameteriv(int var1, int var2, IntBuffer var3) {
        this.gl.glTexParameteriv(var1, var2, var3);
    }

    public void glTexParameteriv(int var1, int var2, int[] var3, int var4) {
        this.gl.glTexParameteriv(var1, var2, var3, var4);
    }

    public void glTexSubImage1D(int var1, int var2, int var3, int var4, int var5, int var6, long var7) {
        this.gl.glTexSubImage1D(var1, var2, var3, var4, var5, var6, var7);
    }

    public void glTexSubImage1D(int var1, int var2, int var3, int var4, int var5, int var6, Buffer var7) {
        this.gl.glTexSubImage1D(var1, var2, var3, var4, var5, var6, var7);
    }

    public void glTexSubImage2D(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, long var9) {
        this.gl.glTexSubImage2D(var1, var2, var3, var4, var5, var6, var7, var8, var9);
    }

    public void glTexSubImage2D(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, Buffer var9) {
        this.gl.glTexSubImage2D(var1, var2, var3, var4, var5, var6, var7, var8, var9);
    }

    public void glTexSubImage3D(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10, long var11) {
        this.gl.glTexSubImage3D(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11);
    }

    public void glTexSubImage3D(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10, Buffer var11) {
        this.gl.glTexSubImage3D(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11);
    }

    public void glTexSubImage4DSGIS(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10, int var11, int var12, Buffer var13) {
        this.gl.glTexSubImage4DSGIS(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10, var11, var12, var13);
    }

    public void glTextureColorMaskSGIS(boolean var1, boolean var2, boolean var3, boolean var4) {
        this.gl.glTextureColorMaskSGIS(var1, var2, var3, var4);
    }

    public void glTextureLightEXT(int var1) {
        this.gl.glTextureLightEXT(var1);
    }

    public void glTextureMaterialEXT(int var1, int var2) {
        this.gl.glTextureMaterialEXT(var1, var2);
    }

    public void glTextureNormalEXT(int var1) {
        this.gl.glTextureNormalEXT(var1);
    }

    public void glTextureRangeAPPLE(int var1, int var2, Buffer var3) {
        this.gl.glTextureRangeAPPLE(var1, var2, var3);
    }

    public void glTrackMatrixNV(int var1, int var2, int var3, int var4) {
        this.gl.glTrackMatrixNV(var1, var2, var3, var4);
    }

    public void glTransformFeedbackAttribsNV(int var1, int[] var2, int var3, int var4) {
        this.gl.glTransformFeedbackAttribsNV(var1, var2, var3, var4);
    }

    public void glTransformFeedbackAttribsNV(int var1, IntBuffer var2, int var3) {
        this.gl.glTransformFeedbackAttribsNV(var1, var2, var3);
    }

    public void glTransformFeedbackVaryingsNV(int var1, int var2, int[] var3, int var4, int var5) {
        this.gl.glTransformFeedbackVaryingsNV(var1, var2, var3, var4, var5);
    }

    public void glTransformFeedbackVaryingsNV(int var1, int var2, IntBuffer var3, int var4) {
        this.gl.glTransformFeedbackVaryingsNV(var1, var2, var3, var4);
    }

    public void glTranslated(double var1, double var3, double var5) {
        this.gl.glTranslated(var1, var3, var5);
    }

    public void glTranslatef(float var1, float var2, float var3) {
        this.gl.glTranslatef(var1, var2, var3);
    }

    public void glUniform1f(int var1, float var2) {
        this.gl.glUniform1f(var1, var2);
    }

    public void glUniform1fARB(int var1, float var2) {
        this.gl.glUniform1fARB(var1, var2);
    }

    public void glUniform1fv(int var1, int var2, FloatBuffer var3) {
        this.gl.glUniform1fv(var1, var2, var3);
    }

    public void glUniform1fv(int var1, int var2, float[] var3, int var4) {
        this.gl.glUniform1fv(var1, var2, var3, var4);
    }

    public void glUniform1fvARB(int var1, int var2, float[] var3, int var4) {
        this.gl.glUniform1fvARB(var1, var2, var3, var4);
    }

    public void glUniform1fvARB(int var1, int var2, FloatBuffer var3) {
        this.gl.glUniform1fvARB(var1, var2, var3);
    }

    public void glUniform1i(int var1, int var2) {
        this.gl.glUniform1i(var1, var2);
    }

    public void glUniform1iARB(int var1, int var2) {
        this.gl.glUniform1iARB(var1, var2);
    }

    public void glUniform1iv(int var1, int var2, IntBuffer var3) {
        this.gl.glUniform1iv(var1, var2, var3);
    }

    public void glUniform1iv(int var1, int var2, int[] var3, int var4) {
        this.gl.glUniform1iv(var1, var2, var3, var4);
    }

    public void glUniform1ivARB(int var1, int var2, IntBuffer var3) {
        this.gl.glUniform1ivARB(var1, var2, var3);
    }

    public void glUniform1ivARB(int var1, int var2, int[] var3, int var4) {
        this.gl.glUniform1ivARB(var1, var2, var3, var4);
    }

    public void glUniform1uiEXT(int var1, int var2) {
        this.gl.glUniform1uiEXT(var1, var2);
    }

    public void glUniform1uivEXT(int var1, int var2, int[] var3, int var4) {
        this.gl.glUniform1uivEXT(var1, var2, var3, var4);
    }

    public void glUniform1uivEXT(int var1, int var2, IntBuffer var3) {
        this.gl.glUniform1uivEXT(var1, var2, var3);
    }

    public void glUniform2f(int var1, float var2, float var3) {
        this.gl.glUniform2f(var1, var2, var3);
    }

    public void glUniform2fARB(int var1, float var2, float var3) {
        this.gl.glUniform2fARB(var1, var2, var3);
    }

    public void glUniform2fv(int var1, int var2, float[] var3, int var4) {
        this.gl.glUniform2fv(var1, var2, var3, var4);
    }

    public void glUniform2fv(int var1, int var2, FloatBuffer var3) {
        this.gl.glUniform2fv(var1, var2, var3);
    }

    public void glUniform2fvARB(int var1, int var2, FloatBuffer var3) {
        this.gl.glUniform2fvARB(var1, var2, var3);
    }

    public void glUniform2fvARB(int var1, int var2, float[] var3, int var4) {
        this.gl.glUniform2fvARB(var1, var2, var3, var4);
    }

    public void glUniform2i(int var1, int var2, int var3) {
        this.gl.glUniform2i(var1, var2, var3);
    }

    public void glUniform2iARB(int var1, int var2, int var3) {
        this.gl.glUniform2iARB(var1, var2, var3);
    }

    public void glUniform2iv(int var1, int var2, IntBuffer var3) {
        this.gl.glUniform2iv(var1, var2, var3);
    }

    public void glUniform2iv(int var1, int var2, int[] var3, int var4) {
        this.gl.glUniform2iv(var1, var2, var3, var4);
    }

    public void glUniform2ivARB(int var1, int var2, int[] var3, int var4) {
        this.gl.glUniform2ivARB(var1, var2, var3, var4);
    }

    public void glUniform2ivARB(int var1, int var2, IntBuffer var3) {
        this.gl.glUniform2ivARB(var1, var2, var3);
    }

    public void glUniform2uiEXT(int var1, int var2, int var3) {
        this.gl.glUniform2uiEXT(var1, var2, var3);
    }

    public void glUniform2uivEXT(int var1, int var2, int[] var3, int var4) {
        this.gl.glUniform2uivEXT(var1, var2, var3, var4);
    }

    public void glUniform2uivEXT(int var1, int var2, IntBuffer var3) {
        this.gl.glUniform2uivEXT(var1, var2, var3);
    }

    public void glUniform3f(int var1, float var2, float var3, float var4) {
        this.gl.glUniform3f(var1, var2, var3, var4);
    }

    public void glUniform3fARB(int var1, float var2, float var3, float var4) {
        this.gl.glUniform3fARB(var1, var2, var3, var4);
    }

    public void glUniform3fv(int var1, int var2, FloatBuffer var3) {
        this.gl.glUniform3fv(var1, var2, var3);
    }

    public void glUniform3fv(int var1, int var2, float[] var3, int var4) {
        this.gl.glUniform3fv(var1, var2, var3, var4);
    }

    public void glUniform3fvARB(int var1, int var2, float[] var3, int var4) {
        this.gl.glUniform3fvARB(var1, var2, var3, var4);
    }

    public void glUniform3fvARB(int var1, int var2, FloatBuffer var3) {
        this.gl.glUniform3fvARB(var1, var2, var3);
    }

    public void glUniform3i(int var1, int var2, int var3, int var4) {
        this.gl.glUniform3i(var1, var2, var3, var4);
    }

    public void glUniform3iARB(int var1, int var2, int var3, int var4) {
        this.gl.glUniform3iARB(var1, var2, var3, var4);
    }

    public void glUniform3iv(int var1, int var2, IntBuffer var3) {
        this.gl.glUniform3iv(var1, var2, var3);
    }

    public void glUniform3iv(int var1, int var2, int[] var3, int var4) {
        this.gl.glUniform3iv(var1, var2, var3, var4);
    }

    public void glUniform3ivARB(int var1, int var2, IntBuffer var3) {
        this.gl.glUniform3ivARB(var1, var2, var3);
    }

    public void glUniform3ivARB(int var1, int var2, int[] var3, int var4) {
        this.gl.glUniform3ivARB(var1, var2, var3, var4);
    }

    public void glUniform3uiEXT(int var1, int var2, int var3, int var4) {
        this.gl.glUniform3uiEXT(var1, var2, var3, var4);
    }

    public void glUniform3uivEXT(int var1, int var2, IntBuffer var3) {
        this.gl.glUniform3uivEXT(var1, var2, var3);
    }

    public void glUniform3uivEXT(int var1, int var2, int[] var3, int var4) {
        this.gl.glUniform3uivEXT(var1, var2, var3, var4);
    }

    public void glUniform4f(int var1, float var2, float var3, float var4, float var5) {
        this.gl.glUniform4f(var1, var2, var3, var4, var5);
    }

    public void glUniform4fARB(int var1, float var2, float var3, float var4, float var5) {
        this.gl.glUniform4fARB(var1, var2, var3, var4, var5);
    }

    public void glUniform4fv(int var1, int var2, float[] var3, int var4) {
        this.gl.glUniform4fv(var1, var2, var3, var4);
    }

    public void glUniform4fv(int var1, int var2, FloatBuffer var3) {
        this.gl.glUniform4fv(var1, var2, var3);
    }

    public void glUniform4fvARB(int var1, int var2, FloatBuffer var3) {
        this.gl.glUniform4fvARB(var1, var2, var3);
    }

    public void glUniform4fvARB(int var1, int var2, float[] var3, int var4) {
        this.gl.glUniform4fvARB(var1, var2, var3, var4);
    }

    public void glUniform4i(int var1, int var2, int var3, int var4, int var5) {
        this.gl.glUniform4i(var1, var2, var3, var4, var5);
    }

    public void glUniform4iARB(int var1, int var2, int var3, int var4, int var5) {
        this.gl.glUniform4iARB(var1, var2, var3, var4, var5);
    }

    public void glUniform4iv(int var1, int var2, int[] var3, int var4) {
        this.gl.glUniform4iv(var1, var2, var3, var4);
    }

    public void glUniform4iv(int var1, int var2, IntBuffer var3) {
        this.gl.glUniform4iv(var1, var2, var3);
    }

    public void glUniform4ivARB(int var1, int var2, int[] var3, int var4) {
        this.gl.glUniform4ivARB(var1, var2, var3, var4);
    }

    public void glUniform4ivARB(int var1, int var2, IntBuffer var3) {
        this.gl.glUniform4ivARB(var1, var2, var3);
    }

    public void glUniform4uiEXT(int var1, int var2, int var3, int var4, int var5) {
        this.gl.glUniform4uiEXT(var1, var2, var3, var4, var5);
    }

    public void glUniform4uivEXT(int var1, int var2, int[] var3, int var4) {
        this.gl.glUniform4uivEXT(var1, var2, var3, var4);
    }

    public void glUniform4uivEXT(int var1, int var2, IntBuffer var3) {
        this.gl.glUniform4uivEXT(var1, var2, var3);
    }

    public void glUniformBufferEXT(int var1, int var2, int var3) {
        this.gl.glUniformBufferEXT(var1, var2, var3);
    }

    public void glUniformMatrix2fv(int var1, int var2, boolean var3, float[] var4, int var5) {
        this.gl.glUniformMatrix2fv(var1, var2, var3, var4, var5);
    }

    public void glUniformMatrix2fv(int var1, int var2, boolean var3, FloatBuffer var4) {
        this.gl.glUniformMatrix2fv(var1, var2, var3, var4);
    }

    public void glUniformMatrix2fvARB(int var1, int var2, boolean var3, float[] var4, int var5) {
        this.gl.glUniformMatrix2fvARB(var1, var2, var3, var4, var5);
    }

    public void glUniformMatrix2fvARB(int var1, int var2, boolean var3, FloatBuffer var4) {
        this.gl.glUniformMatrix2fvARB(var1, var2, var3, var4);
    }

    public void glUniformMatrix2x3fv(int var1, int var2, boolean var3, float[] var4, int var5) {
        this.gl.glUniformMatrix2x3fv(var1, var2, var3, var4, var5);
    }

    public void glUniformMatrix2x3fv(int var1, int var2, boolean var3, FloatBuffer var4) {
        this.gl.glUniformMatrix2x3fv(var1, var2, var3, var4);
    }

    public void glUniformMatrix2x4fv(int var1, int var2, boolean var3, float[] var4, int var5) {
        this.gl.glUniformMatrix2x4fv(var1, var2, var3, var4, var5);
    }

    public void glUniformMatrix2x4fv(int var1, int var2, boolean var3, FloatBuffer var4) {
        this.gl.glUniformMatrix2x4fv(var1, var2, var3, var4);
    }

    public void glUniformMatrix3fv(int var1, int var2, boolean var3, float[] var4, int var5) {
        this.gl.glUniformMatrix3fv(var1, var2, var3, var4, var5);
    }

    public void glUniformMatrix3fv(int var1, int var2, boolean var3, FloatBuffer var4) {
        this.gl.glUniformMatrix3fv(var1, var2, var3, var4);
    }

    public void glUniformMatrix3fvARB(int var1, int var2, boolean var3, float[] var4, int var5) {
        this.gl.glUniformMatrix3fvARB(var1, var2, var3, var4, var5);
    }

    public void glUniformMatrix3fvARB(int var1, int var2, boolean var3, FloatBuffer var4) {
        this.gl.glUniformMatrix3fvARB(var1, var2, var3, var4);
    }

    public void glUniformMatrix3x2fv(int var1, int var2, boolean var3, FloatBuffer var4) {
        this.gl.glUniformMatrix3x2fv(var1, var2, var3, var4);
    }

    public void glUniformMatrix3x2fv(int var1, int var2, boolean var3, float[] var4, int var5) {
        this.gl.glUniformMatrix3x2fv(var1, var2, var3, var4, var5);
    }

    public void glUniformMatrix3x4fv(int var1, int var2, boolean var3, float[] var4, int var5) {
        this.gl.glUniformMatrix3x4fv(var1, var2, var3, var4, var5);
    }

    public void glUniformMatrix3x4fv(int var1, int var2, boolean var3, FloatBuffer var4) {
        this.gl.glUniformMatrix3x4fv(var1, var2, var3, var4);
    }

    public void glUniformMatrix4fv(int var1, int var2, boolean var3, FloatBuffer var4) {
        this.gl.glUniformMatrix4fv(var1, var2, var3, var4);
    }

    public void glUniformMatrix4fv(int var1, int var2, boolean var3, float[] var4, int var5) {
        this.gl.glUniformMatrix4fv(var1, var2, var3, var4, var5);
    }

    public void glUniformMatrix4fvARB(int var1, int var2, boolean var3, FloatBuffer var4) {
        this.gl.glUniformMatrix4fvARB(var1, var2, var3, var4);
    }

    public void glUniformMatrix4fvARB(int var1, int var2, boolean var3, float[] var4, int var5) {
        this.gl.glUniformMatrix4fvARB(var1, var2, var3, var4, var5);
    }

    public void glUniformMatrix4x2fv(int var1, int var2, boolean var3, float[] var4, int var5) {
        this.gl.glUniformMatrix4x2fv(var1, var2, var3, var4, var5);
    }

    public void glUniformMatrix4x2fv(int var1, int var2, boolean var3, FloatBuffer var4) {
        this.gl.glUniformMatrix4x2fv(var1, var2, var3, var4);
    }

    public void glUniformMatrix4x3fv(int var1, int var2, boolean var3, FloatBuffer var4) {
        this.gl.glUniformMatrix4x3fv(var1, var2, var3, var4);
    }

    public void glUniformMatrix4x3fv(int var1, int var2, boolean var3, float[] var4, int var5) {
        this.gl.glUniformMatrix4x3fv(var1, var2, var3, var4, var5);
    }

    public void glUnlockArraysEXT() {
        this.gl.glUnlockArraysEXT();
    }

    public boolean glUnmapBuffer(int var1) {
        return this.gl.glUnmapBuffer(var1);
    }

    public boolean glUnmapBufferARB(int var1) {
        return this.gl.glUnmapBufferARB(var1);
    }

    public void glUpdateObjectBufferATI(int var1, int var2, int var3, Buffer var4, int var5) {
        this.gl.glUpdateObjectBufferATI(var1, var2, var3, var4, var5);
    }

    public void glUseProgram(int var1) {
        this.gl.glUseProgram(var1);
    }

    public void glUseProgramObjectARB(int var1) {
        this.gl.glUseProgramObjectARB(var1);
    }

    public void glValidateProgram(int var1) {
        this.gl.glValidateProgram(var1);
    }

    public void glValidateProgramARB(int var1) {
        this.gl.glValidateProgramARB(var1);
    }

    public void glVariantArrayObjectATI(int var1, int var2, int var3, int var4, int var5) {
        this.gl.glVariantArrayObjectATI(var1, var2, var3, var4, var5);
    }

    public void glVariantPointerEXT(int var1, int var2, int var3, long var4) {
        this.gl.glVariantPointerEXT(var1, var2, var3, var4);
    }

    public void glVariantPointerEXT(int var1, int var2, int var3, Buffer var4) {
        this.gl.glVariantPointerEXT(var1, var2, var3, var4);
    }

    public void glVariantbvEXT(int var1, byte[] var2, int var3) {
        this.gl.glVariantbvEXT(var1, var2, var3);
    }

    public void glVariantbvEXT(int var1, ByteBuffer var2) {
        this.gl.glVariantbvEXT(var1, var2);
    }

    public void glVariantdvEXT(int var1, double[] var2, int var3) {
        this.gl.glVariantdvEXT(var1, var2, var3);
    }

    public void glVariantdvEXT(int var1, DoubleBuffer var2) {
        this.gl.glVariantdvEXT(var1, var2);
    }

    public void glVariantfvEXT(int var1, FloatBuffer var2) {
        this.gl.glVariantfvEXT(var1, var2);
    }

    public void glVariantfvEXT(int var1, float[] var2, int var3) {
        this.gl.glVariantfvEXT(var1, var2, var3);
    }

    public void glVariantivEXT(int var1, int[] var2, int var3) {
        this.gl.glVariantivEXT(var1, var2, var3);
    }

    public void glVariantivEXT(int var1, IntBuffer var2) {
        this.gl.glVariantivEXT(var1, var2);
    }

    public void glVariantsvEXT(int var1, ShortBuffer var2) {
        this.gl.glVariantsvEXT(var1, var2);
    }

    public void glVariantsvEXT(int var1, short[] var2, int var3) {
        this.gl.glVariantsvEXT(var1, var2, var3);
    }

    public void glVariantubvEXT(int var1, ByteBuffer var2) {
        this.gl.glVariantubvEXT(var1, var2);
    }

    public void glVariantubvEXT(int var1, byte[] var2, int var3) {
        this.gl.glVariantubvEXT(var1, var2, var3);
    }

    public void glVariantuivEXT(int var1, int[] var2, int var3) {
        this.gl.glVariantuivEXT(var1, var2, var3);
    }

    public void glVariantuivEXT(int var1, IntBuffer var2) {
        this.gl.glVariantuivEXT(var1, var2);
    }

    public void glVariantusvEXT(int var1, short[] var2, int var3) {
        this.gl.glVariantusvEXT(var1, var2, var3);
    }

    public void glVariantusvEXT(int var1, ShortBuffer var2) {
        this.gl.glVariantusvEXT(var1, var2);
    }

    public void glVertex2d(double var1, double var3) {
        this.gl.glVertex2d(var1, var3);
    }

    public void glVertex2dv(double[] var1, int var2) {
        this.gl.glVertex2dv(var1, var2);
    }

    public void glVertex2dv(DoubleBuffer var1) {
        this.gl.glVertex2dv(var1);
    }

    public void glVertex2f(float var1, float var2) {
        this.gl.glVertex2f(var1, var2);
    }

    public void glVertex2fv(FloatBuffer var1) {
        this.gl.glVertex2fv(var1);
    }

    public void glVertex2fv(float[] var1, int var2) {
        this.gl.glVertex2fv(var1, var2);
    }

    public void glVertex2hNV(short var1, short var2) {
        this.gl.glVertex2hNV(var1, var2);
    }

    public void glVertex2hvNV(ShortBuffer var1) {
        this.gl.glVertex2hvNV(var1);
    }

    public void glVertex2hvNV(short[] var1, int var2) {
        this.gl.glVertex2hvNV(var1, var2);
    }

    public void glVertex2i(int var1, int var2) {
        this.gl.glVertex2i(var1, var2);
    }

    public void glVertex2iv(int[] var1, int var2) {
        this.gl.glVertex2iv(var1, var2);
    }

    public void glVertex2iv(IntBuffer var1) {
        this.gl.glVertex2iv(var1);
    }

    public void glVertex2s(short var1, short var2) {
        this.gl.glVertex2s(var1, var2);
    }

    public void glVertex2sv(short[] var1, int var2) {
        this.gl.glVertex2sv(var1, var2);
    }

    public void glVertex2sv(ShortBuffer var1) {
        this.gl.glVertex2sv(var1);
    }

    public void glVertex3d(double var1, double var3, double var5) {
        this.gl.glVertex3d(var1, var3, var5);
    }

    public void glVertex3dv(DoubleBuffer var1) {
        this.gl.glVertex3dv(var1);
    }

    public void glVertex3dv(double[] var1, int var2) {
        this.gl.glVertex3dv(var1, var2);
    }

    public void glVertex3f(float var1, float var2, float var3) {
        this.gl.glVertex3f(var1, var2, var3);
    }

    public void glVertex3fv(FloatBuffer var1) {
        this.gl.glVertex3fv(var1);
    }

    public void glVertex3fv(float[] var1, int var2) {
        this.gl.glVertex3fv(var1, var2);
    }

    public void glVertex3hNV(short var1, short var2, short var3) {
        this.gl.glVertex3hNV(var1, var2, var3);
    }

    public void glVertex3hvNV(ShortBuffer var1) {
        this.gl.glVertex3hvNV(var1);
    }

    public void glVertex3hvNV(short[] var1, int var2) {
        this.gl.glVertex3hvNV(var1, var2);
    }

    public void glVertex3i(int var1, int var2, int var3) {
        this.gl.glVertex3i(var1, var2, var3);
    }

    public void glVertex3iv(int[] var1, int var2) {
        this.gl.glVertex3iv(var1, var2);
    }

    public void glVertex3iv(IntBuffer var1) {
        this.gl.glVertex3iv(var1);
    }

    public void glVertex3s(short var1, short var2, short var3) {
        this.gl.glVertex3s(var1, var2, var3);
    }

    public void glVertex3sv(ShortBuffer var1) {
        this.gl.glVertex3sv(var1);
    }

    public void glVertex3sv(short[] var1, int var2) {
        this.gl.glVertex3sv(var1, var2);
    }

    public void glVertex4d(double var1, double var3, double var5, double var7) {
        this.gl.glVertex4d(var1, var3, var5, var7);
    }

    public void glVertex4dv(DoubleBuffer var1) {
        this.gl.glVertex4dv(var1);
    }

    public void glVertex4dv(double[] var1, int var2) {
        this.gl.glVertex4dv(var1, var2);
    }

    public void glVertex4f(float var1, float var2, float var3, float var4) {
        this.gl.glVertex4f(var1, var2, var3, var4);
    }

    public void glVertex4fv(FloatBuffer var1) {
        this.gl.glVertex4fv(var1);
    }

    public void glVertex4fv(float[] var1, int var2) {
        this.gl.glVertex4fv(var1, var2);
    }

    public void glVertex4hNV(short var1, short var2, short var3, short var4) {
        this.gl.glVertex4hNV(var1, var2, var3, var4);
    }

    public void glVertex4hvNV(ShortBuffer var1) {
        this.gl.glVertex4hvNV(var1);
    }

    public void glVertex4hvNV(short[] var1, int var2) {
        this.gl.glVertex4hvNV(var1, var2);
    }

    public void glVertex4i(int var1, int var2, int var3, int var4) {
        this.gl.glVertex4i(var1, var2, var3, var4);
    }

    public void glVertex4iv(IntBuffer var1) {
        this.gl.glVertex4iv(var1);
    }

    public void glVertex4iv(int[] var1, int var2) {
        this.gl.glVertex4iv(var1, var2);
    }

    public void glVertex4s(short var1, short var2, short var3, short var4) {
        this.gl.glVertex4s(var1, var2, var3, var4);
    }

    public void glVertex4sv(ShortBuffer var1) {
        this.gl.glVertex4sv(var1);
    }

    public void glVertex4sv(short[] var1, int var2) {
        this.gl.glVertex4sv(var1, var2);
    }

    public void glVertexArrayParameteriAPPLE(int var1, int var2) {
        this.gl.glVertexArrayParameteriAPPLE(var1, var2);
    }

    public void glVertexArrayRangeAPPLE(int var1, Buffer var2) {
        this.gl.glVertexArrayRangeAPPLE(var1, var2);
    }

    public void glVertexArrayRangeNV(int var1, Buffer var2) {
        this.gl.glVertexArrayRangeNV(var1, var2);
    }

    public void glVertexAttrib1d(int var1, double var2) {
        this.gl.glVertexAttrib1d(var1, var2);
    }

    public void glVertexAttrib1dARB(int var1, double var2) {
        this.gl.glVertexAttrib1dARB(var1, var2);
    }

    public void glVertexAttrib1dNV(int var1, double var2) {
        this.gl.glVertexAttrib1dNV(var1, var2);
    }

    public void glVertexAttrib1dv(int var1, DoubleBuffer var2) {
        this.gl.glVertexAttrib1dv(var1, var2);
    }

    public void glVertexAttrib1dv(int var1, double[] var2, int var3) {
        this.gl.glVertexAttrib1dv(var1, var2, var3);
    }

    public void glVertexAttrib1dvARB(int var1, double[] var2, int var3) {
        this.gl.glVertexAttrib1dvARB(var1, var2, var3);
    }

    public void glVertexAttrib1dvARB(int var1, DoubleBuffer var2) {
        this.gl.glVertexAttrib1dvARB(var1, var2);
    }

    public void glVertexAttrib1dvNV(int var1, double[] var2, int var3) {
        this.gl.glVertexAttrib1dvNV(var1, var2, var3);
    }

    public void glVertexAttrib1dvNV(int var1, DoubleBuffer var2) {
        this.gl.glVertexAttrib1dvNV(var1, var2);
    }

    public void glVertexAttrib1f(int var1, float var2) {
        this.gl.glVertexAttrib1f(var1, var2);
    }

    public void glVertexAttrib1fARB(int var1, float var2) {
        this.gl.glVertexAttrib1fARB(var1, var2);
    }

    public void glVertexAttrib1fNV(int var1, float var2) {
        this.gl.glVertexAttrib1fNV(var1, var2);
    }

    public void glVertexAttrib1fv(int var1, float[] var2, int var3) {
        this.gl.glVertexAttrib1fv(var1, var2, var3);
    }

    public void glVertexAttrib1fv(int var1, FloatBuffer var2) {
        this.gl.glVertexAttrib1fv(var1, var2);
    }

    public void glVertexAttrib1fvARB(int var1, float[] var2, int var3) {
        this.gl.glVertexAttrib1fvARB(var1, var2, var3);
    }

    public void glVertexAttrib1fvARB(int var1, FloatBuffer var2) {
        this.gl.glVertexAttrib1fvARB(var1, var2);
    }

    public void glVertexAttrib1fvNV(int var1, float[] var2, int var3) {
        this.gl.glVertexAttrib1fvNV(var1, var2, var3);
    }

    public void glVertexAttrib1fvNV(int var1, FloatBuffer var2) {
        this.gl.glVertexAttrib1fvNV(var1, var2);
    }

    public void glVertexAttrib1hNV(int var1, short var2) {
        this.gl.glVertexAttrib1hNV(var1, var2);
    }

    public void glVertexAttrib1hvNV(int var1, ShortBuffer var2) {
        this.gl.glVertexAttrib1hvNV(var1, var2);
    }

    public void glVertexAttrib1hvNV(int var1, short[] var2, int var3) {
        this.gl.glVertexAttrib1hvNV(var1, var2, var3);
    }

    public void glVertexAttrib1s(int var1, short var2) {
        this.gl.glVertexAttrib1s(var1, var2);
    }

    public void glVertexAttrib1sARB(int var1, short var2) {
        this.gl.glVertexAttrib1sARB(var1, var2);
    }

    public void glVertexAttrib1sNV(int var1, short var2) {
        this.gl.glVertexAttrib1sNV(var1, var2);
    }

    public void glVertexAttrib1sv(int var1, ShortBuffer var2) {
        this.gl.glVertexAttrib1sv(var1, var2);
    }

    public void glVertexAttrib1sv(int var1, short[] var2, int var3) {
        this.gl.glVertexAttrib1sv(var1, var2, var3);
    }

    public void glVertexAttrib1svARB(int var1, ShortBuffer var2) {
        this.gl.glVertexAttrib1svARB(var1, var2);
    }

    public void glVertexAttrib1svARB(int var1, short[] var2, int var3) {
        this.gl.glVertexAttrib1svARB(var1, var2, var3);
    }

    public void glVertexAttrib1svNV(int var1, short[] var2, int var3) {
        this.gl.glVertexAttrib1svNV(var1, var2, var3);
    }

    public void glVertexAttrib1svNV(int var1, ShortBuffer var2) {
        this.gl.glVertexAttrib1svNV(var1, var2);
    }

    public void glVertexAttrib2d(int var1, double var2, double var4) {
        this.gl.glVertexAttrib2d(var1, var2, var4);
    }

    public void glVertexAttrib2dARB(int var1, double var2, double var4) {
        this.gl.glVertexAttrib2dARB(var1, var2, var4);
    }

    public void glVertexAttrib2dNV(int var1, double var2, double var4) {
        this.gl.glVertexAttrib2dNV(var1, var2, var4);
    }

    public void glVertexAttrib2dv(int var1, double[] var2, int var3) {
        this.gl.glVertexAttrib2dv(var1, var2, var3);
    }

    public void glVertexAttrib2dv(int var1, DoubleBuffer var2) {
        this.gl.glVertexAttrib2dv(var1, var2);
    }

    public void glVertexAttrib2dvARB(int var1, double[] var2, int var3) {
        this.gl.glVertexAttrib2dvARB(var1, var2, var3);
    }

    public void glVertexAttrib2dvARB(int var1, DoubleBuffer var2) {
        this.gl.glVertexAttrib2dvARB(var1, var2);
    }

    public void glVertexAttrib2dvNV(int var1, double[] var2, int var3) {
        this.gl.glVertexAttrib2dvNV(var1, var2, var3);
    }

    public void glVertexAttrib2dvNV(int var1, DoubleBuffer var2) {
        this.gl.glVertexAttrib2dvNV(var1, var2);
    }

    public void glVertexAttrib2f(int var1, float var2, float var3) {
        this.gl.glVertexAttrib2f(var1, var2, var3);
    }

    public void glVertexAttrib2fARB(int var1, float var2, float var3) {
        this.gl.glVertexAttrib2fARB(var1, var2, var3);
    }

    public void glVertexAttrib2fNV(int var1, float var2, float var3) {
        this.gl.glVertexAttrib2fNV(var1, var2, var3);
    }

    public void glVertexAttrib2fv(int var1, float[] var2, int var3) {
        this.gl.glVertexAttrib2fv(var1, var2, var3);
    }

    public void glVertexAttrib2fv(int var1, FloatBuffer var2) {
        this.gl.glVertexAttrib2fv(var1, var2);
    }

    public void glVertexAttrib2fvARB(int var1, float[] var2, int var3) {
        this.gl.glVertexAttrib2fvARB(var1, var2, var3);
    }

    public void glVertexAttrib2fvARB(int var1, FloatBuffer var2) {
        this.gl.glVertexAttrib2fvARB(var1, var2);
    }

    public void glVertexAttrib2fvNV(int var1, float[] var2, int var3) {
        this.gl.glVertexAttrib2fvNV(var1, var2, var3);
    }

    public void glVertexAttrib2fvNV(int var1, FloatBuffer var2) {
        this.gl.glVertexAttrib2fvNV(var1, var2);
    }

    public void glVertexAttrib2hNV(int var1, short var2, short var3) {
        this.gl.glVertexAttrib2hNV(var1, var2, var3);
    }

    public void glVertexAttrib2hvNV(int var1, short[] var2, int var3) {
        this.gl.glVertexAttrib2hvNV(var1, var2, var3);
    }

    public void glVertexAttrib2hvNV(int var1, ShortBuffer var2) {
        this.gl.glVertexAttrib2hvNV(var1, var2);
    }

    public void glVertexAttrib2s(int var1, short var2, short var3) {
        this.gl.glVertexAttrib2s(var1, var2, var3);
    }

    public void glVertexAttrib2sARB(int var1, short var2, short var3) {
        this.gl.glVertexAttrib2sARB(var1, var2, var3);
    }

    public void glVertexAttrib2sNV(int var1, short var2, short var3) {
        this.gl.glVertexAttrib2sNV(var1, var2, var3);
    }

    public void glVertexAttrib2sv(int var1, short[] var2, int var3) {
        this.gl.glVertexAttrib2sv(var1, var2, var3);
    }

    public void glVertexAttrib2sv(int var1, ShortBuffer var2) {
        this.gl.glVertexAttrib2sv(var1, var2);
    }

    public void glVertexAttrib2svARB(int var1, short[] var2, int var3) {
        this.gl.glVertexAttrib2svARB(var1, var2, var3);
    }

    public void glVertexAttrib2svARB(int var1, ShortBuffer var2) {
        this.gl.glVertexAttrib2svARB(var1, var2);
    }

    public void glVertexAttrib2svNV(int var1, short[] var2, int var3) {
        this.gl.glVertexAttrib2svNV(var1, var2, var3);
    }

    public void glVertexAttrib2svNV(int var1, ShortBuffer var2) {
        this.gl.glVertexAttrib2svNV(var1, var2);
    }

    public void glVertexAttrib3d(int var1, double var2, double var4, double var6) {
        this.gl.glVertexAttrib3d(var1, var2, var4, var6);
    }

    public void glVertexAttrib3dARB(int var1, double var2, double var4, double var6) {
        this.gl.glVertexAttrib3dARB(var1, var2, var4, var6);
    }

    public void glVertexAttrib3dNV(int var1, double var2, double var4, double var6) {
        this.gl.glVertexAttrib3dNV(var1, var2, var4, var6);
    }

    public void glVertexAttrib3dv(int var1, double[] var2, int var3) {
        this.gl.glVertexAttrib3dv(var1, var2, var3);
    }

    public void glVertexAttrib3dv(int var1, DoubleBuffer var2) {
        this.gl.glVertexAttrib3dv(var1, var2);
    }

    public void glVertexAttrib3dvARB(int var1, double[] var2, int var3) {
        this.gl.glVertexAttrib3dvARB(var1, var2, var3);
    }

    public void glVertexAttrib3dvARB(int var1, DoubleBuffer var2) {
        this.gl.glVertexAttrib3dvARB(var1, var2);
    }

    public void glVertexAttrib3dvNV(int var1, double[] var2, int var3) {
        this.gl.glVertexAttrib3dvNV(var1, var2, var3);
    }

    public void glVertexAttrib3dvNV(int var1, DoubleBuffer var2) {
        this.gl.glVertexAttrib3dvNV(var1, var2);
    }

    public void glVertexAttrib3f(int var1, float var2, float var3, float var4) {
        this.gl.glVertexAttrib3f(var1, var2, var3, var4);
    }

    public void glVertexAttrib3fARB(int var1, float var2, float var3, float var4) {
        this.gl.glVertexAttrib3fARB(var1, var2, var3, var4);
    }

    public void glVertexAttrib3fNV(int var1, float var2, float var3, float var4) {
        this.gl.glVertexAttrib3fNV(var1, var2, var3, var4);
    }

    public void glVertexAttrib3fv(int var1, FloatBuffer var2) {
        this.gl.glVertexAttrib3fv(var1, var2);
    }

    public void glVertexAttrib3fv(int var1, float[] var2, int var3) {
        this.gl.glVertexAttrib3fv(var1, var2, var3);
    }

    public void glVertexAttrib3fvARB(int var1, float[] var2, int var3) {
        this.gl.glVertexAttrib3fvARB(var1, var2, var3);
    }

    public void glVertexAttrib3fvARB(int var1, FloatBuffer var2) {
        this.gl.glVertexAttrib3fvARB(var1, var2);
    }

    public void glVertexAttrib3fvNV(int var1, float[] var2, int var3) {
        this.gl.glVertexAttrib3fvNV(var1, var2, var3);
    }

    public void glVertexAttrib3fvNV(int var1, FloatBuffer var2) {
        this.gl.glVertexAttrib3fvNV(var1, var2);
    }

    public void glVertexAttrib3hNV(int var1, short var2, short var3, short var4) {
        this.gl.glVertexAttrib3hNV(var1, var2, var3, var4);
    }

    public void glVertexAttrib3hvNV(int var1, ShortBuffer var2) {
        this.gl.glVertexAttrib3hvNV(var1, var2);
    }

    public void glVertexAttrib3hvNV(int var1, short[] var2, int var3) {
        this.gl.glVertexAttrib3hvNV(var1, var2, var3);
    }

    public void glVertexAttrib3s(int var1, short var2, short var3, short var4) {
        this.gl.glVertexAttrib3s(var1, var2, var3, var4);
    }

    public void glVertexAttrib3sARB(int var1, short var2, short var3, short var4) {
        this.gl.glVertexAttrib3sARB(var1, var2, var3, var4);
    }

    public void glVertexAttrib3sNV(int var1, short var2, short var3, short var4) {
        this.gl.glVertexAttrib3sNV(var1, var2, var3, var4);
    }

    public void glVertexAttrib3sv(int var1, short[] var2, int var3) {
        this.gl.glVertexAttrib3sv(var1, var2, var3);
    }

    public void glVertexAttrib3sv(int var1, ShortBuffer var2) {
        this.gl.glVertexAttrib3sv(var1, var2);
    }

    public void glVertexAttrib3svARB(int var1, short[] var2, int var3) {
        this.gl.glVertexAttrib3svARB(var1, var2, var3);
    }

    public void glVertexAttrib3svARB(int var1, ShortBuffer var2) {
        this.gl.glVertexAttrib3svARB(var1, var2);
    }

    public void glVertexAttrib3svNV(int var1, short[] var2, int var3) {
        this.gl.glVertexAttrib3svNV(var1, var2, var3);
    }

    public void glVertexAttrib3svNV(int var1, ShortBuffer var2) {
        this.gl.glVertexAttrib3svNV(var1, var2);
    }

    public void glVertexAttrib4Nbv(int var1, ByteBuffer var2) {
        this.gl.glVertexAttrib4Nbv(var1, var2);
    }

    public void glVertexAttrib4Nbv(int var1, byte[] var2, int var3) {
        this.gl.glVertexAttrib4Nbv(var1, var2, var3);
    }

    public void glVertexAttrib4NbvARB(int var1, ByteBuffer var2) {
        this.gl.glVertexAttrib4NbvARB(var1, var2);
    }

    public void glVertexAttrib4NbvARB(int var1, byte[] var2, int var3) {
        this.gl.glVertexAttrib4NbvARB(var1, var2, var3);
    }

    public void glVertexAttrib4Niv(int var1, int[] var2, int var3) {
        this.gl.glVertexAttrib4Niv(var1, var2, var3);
    }

    public void glVertexAttrib4Niv(int var1, IntBuffer var2) {
        this.gl.glVertexAttrib4Niv(var1, var2);
    }

    public void glVertexAttrib4NivARB(int var1, int[] var2, int var3) {
        this.gl.glVertexAttrib4NivARB(var1, var2, var3);
    }

    public void glVertexAttrib4NivARB(int var1, IntBuffer var2) {
        this.gl.glVertexAttrib4NivARB(var1, var2);
    }

    public void glVertexAttrib4Nsv(int var1, short[] var2, int var3) {
        this.gl.glVertexAttrib4Nsv(var1, var2, var3);
    }

    public void glVertexAttrib4Nsv(int var1, ShortBuffer var2) {
        this.gl.glVertexAttrib4Nsv(var1, var2);
    }

    public void glVertexAttrib4NsvARB(int var1, short[] var2, int var3) {
        this.gl.glVertexAttrib4NsvARB(var1, var2, var3);
    }

    public void glVertexAttrib4NsvARB(int var1, ShortBuffer var2) {
        this.gl.glVertexAttrib4NsvARB(var1, var2);
    }

    public void glVertexAttrib4Nub(int var1, byte var2, byte var3, byte var4, byte var5) {
        this.gl.glVertexAttrib4Nub(var1, var2, var3, var4, var5);
    }

    public void glVertexAttrib4NubARB(int var1, byte var2, byte var3, byte var4, byte var5) {
        this.gl.glVertexAttrib4NubARB(var1, var2, var3, var4, var5);
    }

    public void glVertexAttrib4Nubv(int var1, byte[] var2, int var3) {
        this.gl.glVertexAttrib4Nubv(var1, var2, var3);
    }

    public void glVertexAttrib4Nubv(int var1, ByteBuffer var2) {
        this.gl.glVertexAttrib4Nubv(var1, var2);
    }

    public void glVertexAttrib4NubvARB(int var1, ByteBuffer var2) {
        this.gl.glVertexAttrib4NubvARB(var1, var2);
    }

    public void glVertexAttrib4NubvARB(int var1, byte[] var2, int var3) {
        this.gl.glVertexAttrib4NubvARB(var1, var2, var3);
    }

    public void glVertexAttrib4Nuiv(int var1, int[] var2, int var3) {
        this.gl.glVertexAttrib4Nuiv(var1, var2, var3);
    }

    public void glVertexAttrib4Nuiv(int var1, IntBuffer var2) {
        this.gl.glVertexAttrib4Nuiv(var1, var2);
    }

    public void glVertexAttrib4NuivARB(int var1, IntBuffer var2) {
        this.gl.glVertexAttrib4NuivARB(var1, var2);
    }

    public void glVertexAttrib4NuivARB(int var1, int[] var2, int var3) {
        this.gl.glVertexAttrib4NuivARB(var1, var2, var3);
    }

    public void glVertexAttrib4Nusv(int var1, short[] var2, int var3) {
        this.gl.glVertexAttrib4Nusv(var1, var2, var3);
    }

    public void glVertexAttrib4Nusv(int var1, ShortBuffer var2) {
        this.gl.glVertexAttrib4Nusv(var1, var2);
    }

    public void glVertexAttrib4NusvARB(int var1, ShortBuffer var2) {
        this.gl.glVertexAttrib4NusvARB(var1, var2);
    }

    public void glVertexAttrib4NusvARB(int var1, short[] var2, int var3) {
        this.gl.glVertexAttrib4NusvARB(var1, var2, var3);
    }

    public void glVertexAttrib4bv(int var1, byte[] var2, int var3) {
        this.gl.glVertexAttrib4bv(var1, var2, var3);
    }

    public void glVertexAttrib4bv(int var1, ByteBuffer var2) {
        this.gl.glVertexAttrib4bv(var1, var2);
    }

    public void glVertexAttrib4bvARB(int var1, byte[] var2, int var3) {
        this.gl.glVertexAttrib4bvARB(var1, var2, var3);
    }

    public void glVertexAttrib4bvARB(int var1, ByteBuffer var2) {
        this.gl.glVertexAttrib4bvARB(var1, var2);
    }

    public void glVertexAttrib4d(int var1, double var2, double var4, double var6, double var8) {
        this.gl.glVertexAttrib4d(var1, var2, var4, var6, var8);
    }

    public void glVertexAttrib4dARB(int var1, double var2, double var4, double var6, double var8) {
        this.gl.glVertexAttrib4dARB(var1, var2, var4, var6, var8);
    }

    public void glVertexAttrib4dNV(int var1, double var2, double var4, double var6, double var8) {
        this.gl.glVertexAttrib4dNV(var1, var2, var4, var6, var8);
    }

    public void glVertexAttrib4dv(int var1, DoubleBuffer var2) {
        this.gl.glVertexAttrib4dv(var1, var2);
    }

    public void glVertexAttrib4dv(int var1, double[] var2, int var3) {
        this.gl.glVertexAttrib4dv(var1, var2, var3);
    }

    public void glVertexAttrib4dvARB(int var1, DoubleBuffer var2) {
        this.gl.glVertexAttrib4dvARB(var1, var2);
    }

    public void glVertexAttrib4dvARB(int var1, double[] var2, int var3) {
        this.gl.glVertexAttrib4dvARB(var1, var2, var3);
    }

    public void glVertexAttrib4dvNV(int var1, DoubleBuffer var2) {
        this.gl.glVertexAttrib4dvNV(var1, var2);
    }

    public void glVertexAttrib4dvNV(int var1, double[] var2, int var3) {
        this.gl.glVertexAttrib4dvNV(var1, var2, var3);
    }

    public void glVertexAttrib4f(int var1, float var2, float var3, float var4, float var5) {
        this.gl.glVertexAttrib4f(var1, var2, var3, var4, var5);
    }

    public void glVertexAttrib4fARB(int var1, float var2, float var3, float var4, float var5) {
        this.gl.glVertexAttrib4fARB(var1, var2, var3, var4, var5);
    }

    public void glVertexAttrib4fNV(int var1, float var2, float var3, float var4, float var5) {
        this.gl.glVertexAttrib4fNV(var1, var2, var3, var4, var5);
    }

    public void glVertexAttrib4fv(int var1, float[] var2, int var3) {
        this.gl.glVertexAttrib4fv(var1, var2, var3);
    }

    public void glVertexAttrib4fv(int var1, FloatBuffer var2) {
        this.gl.glVertexAttrib4fv(var1, var2);
    }

    public void glVertexAttrib4fvARB(int var1, FloatBuffer var2) {
        this.gl.glVertexAttrib4fvARB(var1, var2);
    }

    public void glVertexAttrib4fvARB(int var1, float[] var2, int var3) {
        this.gl.glVertexAttrib4fvARB(var1, var2, var3);
    }

    public void glVertexAttrib4fvNV(int var1, float[] var2, int var3) {
        this.gl.glVertexAttrib4fvNV(var1, var2, var3);
    }

    public void glVertexAttrib4fvNV(int var1, FloatBuffer var2) {
        this.gl.glVertexAttrib4fvNV(var1, var2);
    }

    public void glVertexAttrib4hNV(int var1, short var2, short var3, short var4, short var5) {
        this.gl.glVertexAttrib4hNV(var1, var2, var3, var4, var5);
    }

    public void glVertexAttrib4hvNV(int var1, ShortBuffer var2) {
        this.gl.glVertexAttrib4hvNV(var1, var2);
    }

    public void glVertexAttrib4hvNV(int var1, short[] var2, int var3) {
        this.gl.glVertexAttrib4hvNV(var1, var2, var3);
    }

    public void glVertexAttrib4iv(int var1, IntBuffer var2) {
        this.gl.glVertexAttrib4iv(var1, var2);
    }

    public void glVertexAttrib4iv(int var1, int[] var2, int var3) {
        this.gl.glVertexAttrib4iv(var1, var2, var3);
    }

    public void glVertexAttrib4ivARB(int var1, int[] var2, int var3) {
        this.gl.glVertexAttrib4ivARB(var1, var2, var3);
    }

    public void glVertexAttrib4ivARB(int var1, IntBuffer var2) {
        this.gl.glVertexAttrib4ivARB(var1, var2);
    }

    public void glVertexAttrib4s(int var1, short var2, short var3, short var4, short var5) {
        this.gl.glVertexAttrib4s(var1, var2, var3, var4, var5);
    }

    public void glVertexAttrib4sARB(int var1, short var2, short var3, short var4, short var5) {
        this.gl.glVertexAttrib4sARB(var1, var2, var3, var4, var5);
    }

    public void glVertexAttrib4sNV(int var1, short var2, short var3, short var4, short var5) {
        this.gl.glVertexAttrib4sNV(var1, var2, var3, var4, var5);
    }

    public void glVertexAttrib4sv(int var1, ShortBuffer var2) {
        this.gl.glVertexAttrib4sv(var1, var2);
    }

    public void glVertexAttrib4sv(int var1, short[] var2, int var3) {
        this.gl.glVertexAttrib4sv(var1, var2, var3);
    }

    public void glVertexAttrib4svARB(int var1, short[] var2, int var3) {
        this.gl.glVertexAttrib4svARB(var1, var2, var3);
    }

    public void glVertexAttrib4svARB(int var1, ShortBuffer var2) {
        this.gl.glVertexAttrib4svARB(var1, var2);
    }

    public void glVertexAttrib4svNV(int var1, short[] var2, int var3) {
        this.gl.glVertexAttrib4svNV(var1, var2, var3);
    }

    public void glVertexAttrib4svNV(int var1, ShortBuffer var2) {
        this.gl.glVertexAttrib4svNV(var1, var2);
    }

    public void glVertexAttrib4ubNV(int var1, byte var2, byte var3, byte var4, byte var5) {
        this.gl.glVertexAttrib4ubNV(var1, var2, var3, var4, var5);
    }

    public void glVertexAttrib4ubv(int var1, ByteBuffer var2) {
        this.gl.glVertexAttrib4ubv(var1, var2);
    }

    public void glVertexAttrib4ubv(int var1, byte[] var2, int var3) {
        this.gl.glVertexAttrib4ubv(var1, var2, var3);
    }

    public void glVertexAttrib4ubvARB(int var1, byte[] var2, int var3) {
        this.gl.glVertexAttrib4ubvARB(var1, var2, var3);
    }

    public void glVertexAttrib4ubvARB(int var1, ByteBuffer var2) {
        this.gl.glVertexAttrib4ubvARB(var1, var2);
    }

    public void glVertexAttrib4ubvNV(int var1, byte[] var2, int var3) {
        this.gl.glVertexAttrib4ubvNV(var1, var2, var3);
    }

    public void glVertexAttrib4ubvNV(int var1, ByteBuffer var2) {
        this.gl.glVertexAttrib4ubvNV(var1, var2);
    }

    public void glVertexAttrib4uiv(int var1, int[] var2, int var3) {
        this.gl.glVertexAttrib4uiv(var1, var2, var3);
    }

    public void glVertexAttrib4uiv(int var1, IntBuffer var2) {
        this.gl.glVertexAttrib4uiv(var1, var2);
    }

    public void glVertexAttrib4uivARB(int var1, int[] var2, int var3) {
        this.gl.glVertexAttrib4uivARB(var1, var2, var3);
    }

    public void glVertexAttrib4uivARB(int var1, IntBuffer var2) {
        this.gl.glVertexAttrib4uivARB(var1, var2);
    }

    public void glVertexAttrib4usv(int var1, short[] var2, int var3) {
        this.gl.glVertexAttrib4usv(var1, var2, var3);
    }

    public void glVertexAttrib4usv(int var1, ShortBuffer var2) {
        this.gl.glVertexAttrib4usv(var1, var2);
    }

    public void glVertexAttrib4usvARB(int var1, short[] var2, int var3) {
        this.gl.glVertexAttrib4usvARB(var1, var2, var3);
    }

    public void glVertexAttrib4usvARB(int var1, ShortBuffer var2) {
        this.gl.glVertexAttrib4usvARB(var1, var2);
    }

    public void glVertexAttribArrayObjectATI(int var1, int var2, int var3, boolean var4, int var5, int var6, int var7) {
        this.gl.glVertexAttribArrayObjectATI(var1, var2, var3, var4, var5, var6, var7);
    }

    public void glVertexAttribI1iEXT(int var1, int var2) {
        this.gl.glVertexAttribI1iEXT(var1, var2);
    }

    public void glVertexAttribI1ivEXT(int var1, IntBuffer var2) {
        this.gl.glVertexAttribI1ivEXT(var1, var2);
    }

    public void glVertexAttribI1ivEXT(int var1, int[] var2, int var3) {
        this.gl.glVertexAttribI1ivEXT(var1, var2, var3);
    }

    public void glVertexAttribI1uiEXT(int var1, int var2) {
        this.gl.glVertexAttribI1uiEXT(var1, var2);
    }

    public void glVertexAttribI1uivEXT(int var1, int[] var2, int var3) {
        this.gl.glVertexAttribI1uivEXT(var1, var2, var3);
    }

    public void glVertexAttribI1uivEXT(int var1, IntBuffer var2) {
        this.gl.glVertexAttribI1uivEXT(var1, var2);
    }

    public void glVertexAttribI2iEXT(int var1, int var2, int var3) {
        this.gl.glVertexAttribI2iEXT(var1, var2, var3);
    }

    public void glVertexAttribI2ivEXT(int var1, IntBuffer var2) {
        this.gl.glVertexAttribI2ivEXT(var1, var2);
    }

    public void glVertexAttribI2ivEXT(int var1, int[] var2, int var3) {
        this.gl.glVertexAttribI2ivEXT(var1, var2, var3);
    }

    public void glVertexAttribI2uiEXT(int var1, int var2, int var3) {
        this.gl.glVertexAttribI2uiEXT(var1, var2, var3);
    }

    public void glVertexAttribI2uivEXT(int var1, IntBuffer var2) {
        this.gl.glVertexAttribI2uivEXT(var1, var2);
    }

    public void glVertexAttribI2uivEXT(int var1, int[] var2, int var3) {
        this.gl.glVertexAttribI2uivEXT(var1, var2, var3);
    }

    public void glVertexAttribI3iEXT(int var1, int var2, int var3, int var4) {
        this.gl.glVertexAttribI3iEXT(var1, var2, var3, var4);
    }

    public void glVertexAttribI3ivEXT(int var1, IntBuffer var2) {
        this.gl.glVertexAttribI3ivEXT(var1, var2);
    }

    public void glVertexAttribI3ivEXT(int var1, int[] var2, int var3) {
        this.gl.glVertexAttribI3ivEXT(var1, var2, var3);
    }

    public void glVertexAttribI3uiEXT(int var1, int var2, int var3, int var4) {
        this.gl.glVertexAttribI3uiEXT(var1, var2, var3, var4);
    }

    public void glVertexAttribI3uivEXT(int var1, IntBuffer var2) {
        this.gl.glVertexAttribI3uivEXT(var1, var2);
    }

    public void glVertexAttribI3uivEXT(int var1, int[] var2, int var3) {
        this.gl.glVertexAttribI3uivEXT(var1, var2, var3);
    }

    public void glVertexAttribI4bvEXT(int var1, byte[] var2, int var3) {
        this.gl.glVertexAttribI4bvEXT(var1, var2, var3);
    }

    public void glVertexAttribI4bvEXT(int var1, ByteBuffer var2) {
        this.gl.glVertexAttribI4bvEXT(var1, var2);
    }

    public void glVertexAttribI4iEXT(int var1, int var2, int var3, int var4, int var5) {
        this.gl.glVertexAttribI4iEXT(var1, var2, var3, var4, var5);
    }

    public void glVertexAttribI4ivEXT(int var1, int[] var2, int var3) {
        this.gl.glVertexAttribI4ivEXT(var1, var2, var3);
    }

    public void glVertexAttribI4ivEXT(int var1, IntBuffer var2) {
        this.gl.glVertexAttribI4ivEXT(var1, var2);
    }

    public void glVertexAttribI4svEXT(int var1, ShortBuffer var2) {
        this.gl.glVertexAttribI4svEXT(var1, var2);
    }

    public void glVertexAttribI4svEXT(int var1, short[] var2, int var3) {
        this.gl.glVertexAttribI4svEXT(var1, var2, var3);
    }

    public void glVertexAttribI4ubvEXT(int var1, ByteBuffer var2) {
        this.gl.glVertexAttribI4ubvEXT(var1, var2);
    }

    public void glVertexAttribI4ubvEXT(int var1, byte[] var2, int var3) {
        this.gl.glVertexAttribI4ubvEXT(var1, var2, var3);
    }

    public void glVertexAttribI4uiEXT(int var1, int var2, int var3, int var4, int var5) {
        this.gl.glVertexAttribI4uiEXT(var1, var2, var3, var4, var5);
    }

    public void glVertexAttribI4uivEXT(int var1, int[] var2, int var3) {
        this.gl.glVertexAttribI4uivEXT(var1, var2, var3);
    }

    public void glVertexAttribI4uivEXT(int var1, IntBuffer var2) {
        this.gl.glVertexAttribI4uivEXT(var1, var2);
    }

    public void glVertexAttribI4usvEXT(int var1, short[] var2, int var3) {
        this.gl.glVertexAttribI4usvEXT(var1, var2, var3);
    }

    public void glVertexAttribI4usvEXT(int var1, ShortBuffer var2) {
        this.gl.glVertexAttribI4usvEXT(var1, var2);
    }

    public void glVertexAttribIPointerEXT(int var1, int var2, int var3, int var4, Buffer var5) {
        this.gl.glVertexAttribIPointerEXT(var1, var2, var3, var4, var5);
    }

    public void glVertexAttribPointer(int var1, int var2, int var3, boolean var4, int var5, Buffer var6) {
        this.gl.glVertexAttribPointer(var1, var2, var3, var4, var5, var6);
    }

    public void glVertexAttribPointer(int var1, int var2, int var3, boolean var4, int var5, long var6) {
        this.gl.glVertexAttribPointer(var1, var2, var3, var4, var5, var6);
    }

    public void glVertexAttribPointerARB(int var1, int var2, int var3, boolean var4, int var5, long var6) {
        this.gl.glVertexAttribPointerARB(var1, var2, var3, var4, var5, var6);
    }

    public void glVertexAttribPointerARB(int var1, int var2, int var3, boolean var4, int var5, Buffer var6) {
        this.gl.glVertexAttribPointerARB(var1, var2, var3, var4, var5, var6);
    }

    public void glVertexAttribPointerNV(int var1, int var2, int var3, int var4, Buffer var5) {
        this.gl.glVertexAttribPointerNV(var1, var2, var3, var4, var5);
    }

    public void glVertexAttribPointerNV(int var1, int var2, int var3, int var4, long var5) {
        this.gl.glVertexAttribPointerNV(var1, var2, var3, var4, var5);
    }

    public void glVertexAttribs1dvNV(int var1, int var2, double[] var3, int var4) {
        this.gl.glVertexAttribs1dvNV(var1, var2, var3, var4);
    }

    public void glVertexAttribs1dvNV(int var1, int var2, DoubleBuffer var3) {
        this.gl.glVertexAttribs1dvNV(var1, var2, var3);
    }

    public void glVertexAttribs1fvNV(int var1, int var2, float[] var3, int var4) {
        this.gl.glVertexAttribs1fvNV(var1, var2, var3, var4);
    }

    public void glVertexAttribs1fvNV(int var1, int var2, FloatBuffer var3) {
        this.gl.glVertexAttribs1fvNV(var1, var2, var3);
    }

    public void glVertexAttribs1hvNV(int var1, int var2, short[] var3, int var4) {
        this.gl.glVertexAttribs1hvNV(var1, var2, var3, var4);
    }

    public void glVertexAttribs1hvNV(int var1, int var2, ShortBuffer var3) {
        this.gl.glVertexAttribs1hvNV(var1, var2, var3);
    }

    public void glVertexAttribs1svNV(int var1, int var2, short[] var3, int var4) {
        this.gl.glVertexAttribs1svNV(var1, var2, var3, var4);
    }

    public void glVertexAttribs1svNV(int var1, int var2, ShortBuffer var3) {
        this.gl.glVertexAttribs1svNV(var1, var2, var3);
    }

    public void glVertexAttribs2dvNV(int var1, int var2, double[] var3, int var4) {
        this.gl.glVertexAttribs2dvNV(var1, var2, var3, var4);
    }

    public void glVertexAttribs2dvNV(int var1, int var2, DoubleBuffer var3) {
        this.gl.glVertexAttribs2dvNV(var1, var2, var3);
    }

    public void glVertexAttribs2fvNV(int var1, int var2, float[] var3, int var4) {
        this.gl.glVertexAttribs2fvNV(var1, var2, var3, var4);
    }

    public void glVertexAttribs2fvNV(int var1, int var2, FloatBuffer var3) {
        this.gl.glVertexAttribs2fvNV(var1, var2, var3);
    }

    public void glVertexAttribs2hvNV(int var1, int var2, ShortBuffer var3) {
        this.gl.glVertexAttribs2hvNV(var1, var2, var3);
    }

    public void glVertexAttribs2hvNV(int var1, int var2, short[] var3, int var4) {
        this.gl.glVertexAttribs2hvNV(var1, var2, var3, var4);
    }

    public void glVertexAttribs2svNV(int var1, int var2, ShortBuffer var3) {
        this.gl.glVertexAttribs2svNV(var1, var2, var3);
    }

    public void glVertexAttribs2svNV(int var1, int var2, short[] var3, int var4) {
        this.gl.glVertexAttribs2svNV(var1, var2, var3, var4);
    }

    public void glVertexAttribs3dvNV(int var1, int var2, double[] var3, int var4) {
        this.gl.glVertexAttribs3dvNV(var1, var2, var3, var4);
    }

    public void glVertexAttribs3dvNV(int var1, int var2, DoubleBuffer var3) {
        this.gl.glVertexAttribs3dvNV(var1, var2, var3);
    }

    public void glVertexAttribs3fvNV(int var1, int var2, float[] var3, int var4) {
        this.gl.glVertexAttribs3fvNV(var1, var2, var3, var4);
    }

    public void glVertexAttribs3fvNV(int var1, int var2, FloatBuffer var3) {
        this.gl.glVertexAttribs3fvNV(var1, var2, var3);
    }

    public void glVertexAttribs3hvNV(int var1, int var2, short[] var3, int var4) {
        this.gl.glVertexAttribs3hvNV(var1, var2, var3, var4);
    }

    public void glVertexAttribs3hvNV(int var1, int var2, ShortBuffer var3) {
        this.gl.glVertexAttribs3hvNV(var1, var2, var3);
    }

    public void glVertexAttribs3svNV(int var1, int var2, short[] var3, int var4) {
        this.gl.glVertexAttribs3svNV(var1, var2, var3, var4);
    }

    public void glVertexAttribs3svNV(int var1, int var2, ShortBuffer var3) {
        this.gl.glVertexAttribs3svNV(var1, var2, var3);
    }

    public void glVertexAttribs4dvNV(int var1, int var2, double[] var3, int var4) {
        this.gl.glVertexAttribs4dvNV(var1, var2, var3, var4);
    }

    public void glVertexAttribs4dvNV(int var1, int var2, DoubleBuffer var3) {
        this.gl.glVertexAttribs4dvNV(var1, var2, var3);
    }

    public void glVertexAttribs4fvNV(int var1, int var2, float[] var3, int var4) {
        this.gl.glVertexAttribs4fvNV(var1, var2, var3, var4);
    }

    public void glVertexAttribs4fvNV(int var1, int var2, FloatBuffer var3) {
        this.gl.glVertexAttribs4fvNV(var1, var2, var3);
    }

    public void glVertexAttribs4hvNV(int var1, int var2, ShortBuffer var3) {
        this.gl.glVertexAttribs4hvNV(var1, var2, var3);
    }

    public void glVertexAttribs4hvNV(int var1, int var2, short[] var3, int var4) {
        this.gl.glVertexAttribs4hvNV(var1, var2, var3, var4);
    }

    public void glVertexAttribs4svNV(int var1, int var2, short[] var3, int var4) {
        this.gl.glVertexAttribs4svNV(var1, var2, var3, var4);
    }

    public void glVertexAttribs4svNV(int var1, int var2, ShortBuffer var3) {
        this.gl.glVertexAttribs4svNV(var1, var2, var3);
    }

    public void glVertexAttribs4ubvNV(int var1, int var2, ByteBuffer var3) {
        this.gl.glVertexAttribs4ubvNV(var1, var2, var3);
    }

    public void glVertexAttribs4ubvNV(int var1, int var2, byte[] var3, int var4) {
        this.gl.glVertexAttribs4ubvNV(var1, var2, var3, var4);
    }

    public void glVertexBlendARB(int var1) {
        this.gl.glVertexBlendARB(var1);
    }

    public void glVertexBlendEnvfATI(int var1, float var2) {
        this.gl.glVertexBlendEnvfATI(var1, var2);
    }

    public void glVertexBlendEnviATI(int var1, int var2) {
        this.gl.glVertexBlendEnviATI(var1, var2);
    }

    public void glVertexPointer(int var1, int var2, int var3, Buffer var4) {
        this.gl.glVertexPointer(var1, var2, var3, var4);
    }

    public void glVertexPointer(int var1, int var2, int var3, long var4) {
        this.gl.glVertexPointer(var1, var2, var3, var4);
    }

    public void glVertexStream1dATI(int var1, double var2) {
        this.gl.glVertexStream1dATI(var1, var2);
    }

    public void glVertexStream1dvATI(int var1, DoubleBuffer var2) {
        this.gl.glVertexStream1dvATI(var1, var2);
    }

    public void glVertexStream1dvATI(int var1, double[] var2, int var3) {
        this.gl.glVertexStream1dvATI(var1, var2, var3);
    }

    public void glVertexStream1fATI(int var1, float var2) {
        this.gl.glVertexStream1fATI(var1, var2);
    }

    public void glVertexStream1fvATI(int var1, FloatBuffer var2) {
        this.gl.glVertexStream1fvATI(var1, var2);
    }

    public void glVertexStream1fvATI(int var1, float[] var2, int var3) {
        this.gl.glVertexStream1fvATI(var1, var2, var3);
    }

    public void glVertexStream1iATI(int var1, int var2) {
        this.gl.glVertexStream1iATI(var1, var2);
    }

    public void glVertexStream1ivATI(int var1, int[] var2, int var3) {
        this.gl.glVertexStream1ivATI(var1, var2, var3);
    }

    public void glVertexStream1ivATI(int var1, IntBuffer var2) {
        this.gl.glVertexStream1ivATI(var1, var2);
    }

    public void glVertexStream1sATI(int var1, short var2) {
        this.gl.glVertexStream1sATI(var1, var2);
    }

    public void glVertexStream1svATI(int var1, ShortBuffer var2) {
        this.gl.glVertexStream1svATI(var1, var2);
    }

    public void glVertexStream1svATI(int var1, short[] var2, int var3) {
        this.gl.glVertexStream1svATI(var1, var2, var3);
    }

    public void glVertexStream2dATI(int var1, double var2, double var4) {
        this.gl.glVertexStream2dATI(var1, var2, var4);
    }

    public void glVertexStream2dvATI(int var1, DoubleBuffer var2) {
        this.gl.glVertexStream2dvATI(var1, var2);
    }

    public void glVertexStream2dvATI(int var1, double[] var2, int var3) {
        this.gl.glVertexStream2dvATI(var1, var2, var3);
    }

    public void glVertexStream2fATI(int var1, float var2, float var3) {
        this.gl.glVertexStream2fATI(var1, var2, var3);
    }

    public void glVertexStream2fvATI(int var1, FloatBuffer var2) {
        this.gl.glVertexStream2fvATI(var1, var2);
    }

    public void glVertexStream2fvATI(int var1, float[] var2, int var3) {
        this.gl.glVertexStream2fvATI(var1, var2, var3);
    }

    public void glVertexStream2iATI(int var1, int var2, int var3) {
        this.gl.glVertexStream2iATI(var1, var2, var3);
    }

    public void glVertexStream2ivATI(int var1, IntBuffer var2) {
        this.gl.glVertexStream2ivATI(var1, var2);
    }

    public void glVertexStream2ivATI(int var1, int[] var2, int var3) {
        this.gl.glVertexStream2ivATI(var1, var2, var3);
    }

    public void glVertexStream2sATI(int var1, short var2, short var3) {
        this.gl.glVertexStream2sATI(var1, var2, var3);
    }

    public void glVertexStream2svATI(int var1, short[] var2, int var3) {
        this.gl.glVertexStream2svATI(var1, var2, var3);
    }

    public void glVertexStream2svATI(int var1, ShortBuffer var2) {
        this.gl.glVertexStream2svATI(var1, var2);
    }

    public void glVertexStream3dATI(int var1, double var2, double var4, double var6) {
        this.gl.glVertexStream3dATI(var1, var2, var4, var6);
    }

    public void glVertexStream3dvATI(int var1, DoubleBuffer var2) {
        this.gl.glVertexStream3dvATI(var1, var2);
    }

    public void glVertexStream3dvATI(int var1, double[] var2, int var3) {
        this.gl.glVertexStream3dvATI(var1, var2, var3);
    }

    public void glVertexStream3fATI(int var1, float var2, float var3, float var4) {
        this.gl.glVertexStream3fATI(var1, var2, var3, var4);
    }

    public void glVertexStream3fvATI(int var1, FloatBuffer var2) {
        this.gl.glVertexStream3fvATI(var1, var2);
    }

    public void glVertexStream3fvATI(int var1, float[] var2, int var3) {
        this.gl.glVertexStream3fvATI(var1, var2, var3);
    }

    public void glVertexStream3iATI(int var1, int var2, int var3, int var4) {
        this.gl.glVertexStream3iATI(var1, var2, var3, var4);
    }

    public void glVertexStream3ivATI(int var1, IntBuffer var2) {
        this.gl.glVertexStream3ivATI(var1, var2);
    }

    public void glVertexStream3ivATI(int var1, int[] var2, int var3) {
        this.gl.glVertexStream3ivATI(var1, var2, var3);
    }

    public void glVertexStream3sATI(int var1, short var2, short var3, short var4) {
        this.gl.glVertexStream3sATI(var1, var2, var3, var4);
    }

    public void glVertexStream3svATI(int var1, short[] var2, int var3) {
        this.gl.glVertexStream3svATI(var1, var2, var3);
    }

    public void glVertexStream3svATI(int var1, ShortBuffer var2) {
        this.gl.glVertexStream3svATI(var1, var2);
    }

    public void glVertexStream4dATI(int var1, double var2, double var4, double var6, double var8) {
        this.gl.glVertexStream4dATI(var1, var2, var4, var6, var8);
    }

    public void glVertexStream4dvATI(int var1, double[] var2, int var3) {
        this.gl.glVertexStream4dvATI(var1, var2, var3);
    }

    public void glVertexStream4dvATI(int var1, DoubleBuffer var2) {
        this.gl.glVertexStream4dvATI(var1, var2);
    }

    public void glVertexStream4fATI(int var1, float var2, float var3, float var4, float var5) {
        this.gl.glVertexStream4fATI(var1, var2, var3, var4, var5);
    }

    public void glVertexStream4fvATI(int var1, float[] var2, int var3) {
        this.gl.glVertexStream4fvATI(var1, var2, var3);
    }

    public void glVertexStream4fvATI(int var1, FloatBuffer var2) {
        this.gl.glVertexStream4fvATI(var1, var2);
    }

    public void glVertexStream4iATI(int var1, int var2, int var3, int var4, int var5) {
        this.gl.glVertexStream4iATI(var1, var2, var3, var4, var5);
    }

    public void glVertexStream4ivATI(int var1, IntBuffer var2) {
        this.gl.glVertexStream4ivATI(var1, var2);
    }

    public void glVertexStream4ivATI(int var1, int[] var2, int var3) {
        this.gl.glVertexStream4ivATI(var1, var2, var3);
    }

    public void glVertexStream4sATI(int var1, short var2, short var3, short var4, short var5) {
        this.gl.glVertexStream4sATI(var1, var2, var3, var4, var5);
    }

    public void glVertexStream4svATI(int var1, ShortBuffer var2) {
        this.gl.glVertexStream4svATI(var1, var2);
    }

    public void glVertexStream4svATI(int var1, short[] var2, int var3) {
        this.gl.glVertexStream4svATI(var1, var2, var3);
    }

    public void glVertexWeightPointerEXT(int var1, int var2, int var3, long var4) {
        this.gl.glVertexWeightPointerEXT(var1, var2, var3, var4);
    }

    public void glVertexWeightPointerEXT(int var1, int var2, int var3, Buffer var4) {
        this.gl.glVertexWeightPointerEXT(var1, var2, var3, var4);
    }

    public void glVertexWeightfEXT(float var1) {
        this.gl.glVertexWeightfEXT(var1);
    }

    public void glVertexWeightfvEXT(float[] var1, int var2) {
        this.gl.glVertexWeightfvEXT(var1, var2);
    }

    public void glVertexWeightfvEXT(FloatBuffer var1) {
        this.gl.glVertexWeightfvEXT(var1);
    }

    public void glVertexWeighthNV(short var1) {
        this.gl.glVertexWeighthNV(var1);
    }

    public void glVertexWeighthvNV(ShortBuffer var1) {
        this.gl.glVertexWeighthvNV(var1);
    }

    public void glVertexWeighthvNV(short[] var1, int var2) {
        this.gl.glVertexWeighthvNV(var1, var2);
    }

    public void glViewport(int var1, int var2, int var3, int var4) {
        this.gl.glViewport(var1, var2, var3, var4);
    }

    public void glWeightPointerARB(int var1, int var2, int var3, Buffer var4) {
        this.gl.glWeightPointerARB(var1, var2, var3, var4);
    }

    public void glWeightPointerARB(int var1, int var2, int var3, long var4) {
        this.gl.glWeightPointerARB(var1, var2, var3, var4);
    }

    public void glWeightbvARB(int var1, ByteBuffer var2) {
        this.gl.glWeightbvARB(var1, var2);
    }

    public void glWeightbvARB(int var1, byte[] var2, int var3) {
        this.gl.glWeightbvARB(var1, var2, var3);
    }

    public void glWeightdvARB(int var1, DoubleBuffer var2) {
        this.gl.glWeightdvARB(var1, var2);
    }

    public void glWeightdvARB(int var1, double[] var2, int var3) {
        this.gl.glWeightdvARB(var1, var2, var3);
    }

    public void glWeightfvARB(int var1, float[] var2, int var3) {
        this.gl.glWeightfvARB(var1, var2, var3);
    }

    public void glWeightfvARB(int var1, FloatBuffer var2) {
        this.gl.glWeightfvARB(var1, var2);
    }

    public void glWeightivARB(int var1, IntBuffer var2) {
        this.gl.glWeightivARB(var1, var2);
    }

    public void glWeightivARB(int var1, int[] var2, int var3) {
        this.gl.glWeightivARB(var1, var2, var3);
    }

    public void glWeightsvARB(int var1, short[] var2, int var3) {
        this.gl.glWeightsvARB(var1, var2, var3);
    }

    public void glWeightsvARB(int var1, ShortBuffer var2) {
        this.gl.glWeightsvARB(var1, var2);
    }

    public void glWeightubvARB(int var1, ByteBuffer var2) {
        this.gl.glWeightubvARB(var1, var2);
    }

    public void glWeightubvARB(int var1, byte[] var2, int var3) {
        this.gl.glWeightubvARB(var1, var2, var3);
    }

    public void glWeightuivARB(int var1, int[] var2, int var3) {
        this.gl.glWeightuivARB(var1, var2, var3);
    }

    public void glWeightuivARB(int var1, IntBuffer var2) {
        this.gl.glWeightuivARB(var1, var2);
    }

    public void glWeightusvARB(int var1, ShortBuffer var2) {
        this.gl.glWeightusvARB(var1, var2);
    }

    public void glWeightusvARB(int var1, short[] var2, int var3) {
        this.gl.glWeightusvARB(var1, var2, var3);
    }

    public void glWindowPos2d(double var1, double var3) {
        this.gl.glWindowPos2d(var1, var3);
    }

    public void glWindowPos2dARB(double var1, double var3) {
        this.gl.glWindowPos2dARB(var1, var3);
    }

    public void glWindowPos2dMESA(double var1, double var3) {
        this.gl.glWindowPos2dMESA(var1, var3);
    }

    public void glWindowPos2dv(double[] var1, int var2) {
        this.gl.glWindowPos2dv(var1, var2);
    }

    public void glWindowPos2dv(DoubleBuffer var1) {
        this.gl.glWindowPos2dv(var1);
    }

    public void glWindowPos2dvARB(double[] var1, int var2) {
        this.gl.glWindowPos2dvARB(var1, var2);
    }

    public void glWindowPos2dvARB(DoubleBuffer var1) {
        this.gl.glWindowPos2dvARB(var1);
    }

    public void glWindowPos2dvMESA(double[] var1, int var2) {
        this.gl.glWindowPos2dvMESA(var1, var2);
    }

    public void glWindowPos2dvMESA(DoubleBuffer var1) {
        this.gl.glWindowPos2dvMESA(var1);
    }

    public void glWindowPos2f(float var1, float var2) {
        this.gl.glWindowPos2f(var1, var2);
    }

    public void glWindowPos2fARB(float var1, float var2) {
        this.gl.glWindowPos2fARB(var1, var2);
    }

    public void glWindowPos2fMESA(float var1, float var2) {
        this.gl.glWindowPos2fMESA(var1, var2);
    }

    public void glWindowPos2fv(FloatBuffer var1) {
        this.gl.glWindowPos2fv(var1);
    }

    public void glWindowPos2fv(float[] var1, int var2) {
        this.gl.glWindowPos2fv(var1, var2);
    }

    public void glWindowPos2fvARB(FloatBuffer var1) {
        this.gl.glWindowPos2fvARB(var1);
    }

    public void glWindowPos2fvARB(float[] var1, int var2) {
        this.gl.glWindowPos2fvARB(var1, var2);
    }

    public void glWindowPos2fvMESA(FloatBuffer var1) {
        this.gl.glWindowPos2fvMESA(var1);
    }

    public void glWindowPos2fvMESA(float[] var1, int var2) {
        this.gl.glWindowPos2fvMESA(var1, var2);
    }

    public void glWindowPos2i(int var1, int var2) {
        this.gl.glWindowPos2i(var1, var2);
    }

    public void glWindowPos2iARB(int var1, int var2) {
        this.gl.glWindowPos2iARB(var1, var2);
    }

    public void glWindowPos2iMESA(int var1, int var2) {
        this.gl.glWindowPos2iMESA(var1, var2);
    }

    public void glWindowPos2iv(int[] var1, int var2) {
        this.gl.glWindowPos2iv(var1, var2);
    }

    public void glWindowPos2iv(IntBuffer var1) {
        this.gl.glWindowPos2iv(var1);
    }

    public void glWindowPos2ivARB(int[] var1, int var2) {
        this.gl.glWindowPos2ivARB(var1, var2);
    }

    public void glWindowPos2ivARB(IntBuffer var1) {
        this.gl.glWindowPos2ivARB(var1);
    }

    public void glWindowPos2ivMESA(IntBuffer var1) {
        this.gl.glWindowPos2ivMESA(var1);
    }

    public void glWindowPos2ivMESA(int[] var1, int var2) {
        this.gl.glWindowPos2ivMESA(var1, var2);
    }

    public void glWindowPos2s(short var1, short var2) {
        this.gl.glWindowPos2s(var1, var2);
    }

    public void glWindowPos2sARB(short var1, short var2) {
        this.gl.glWindowPos2sARB(var1, var2);
    }

    public void glWindowPos2sMESA(short var1, short var2) {
        this.gl.glWindowPos2sMESA(var1, var2);
    }

    public void glWindowPos2sv(short[] var1, int var2) {
        this.gl.glWindowPos2sv(var1, var2);
    }

    public void glWindowPos2sv(ShortBuffer var1) {
        this.gl.glWindowPos2sv(var1);
    }

    public void glWindowPos2svARB(short[] var1, int var2) {
        this.gl.glWindowPos2svARB(var1, var2);
    }

    public void glWindowPos2svARB(ShortBuffer var1) {
        this.gl.glWindowPos2svARB(var1);
    }

    public void glWindowPos2svMESA(short[] var1, int var2) {
        this.gl.glWindowPos2svMESA(var1, var2);
    }

    public void glWindowPos2svMESA(ShortBuffer var1) {
        this.gl.glWindowPos2svMESA(var1);
    }

    public void glWindowPos3d(double var1, double var3, double var5) {
        this.gl.glWindowPos3d(var1, var3, var5);
    }

    public void glWindowPos3dARB(double var1, double var3, double var5) {
        this.gl.glWindowPos3dARB(var1, var3, var5);
    }

    public void glWindowPos3dMESA(double var1, double var3, double var5) {
        this.gl.glWindowPos3dMESA(var1, var3, var5);
    }

    public void glWindowPos3dv(double[] var1, int var2) {
        this.gl.glWindowPos3dv(var1, var2);
    }

    public void glWindowPos3dv(DoubleBuffer var1) {
        this.gl.glWindowPos3dv(var1);
    }

    public void glWindowPos3dvARB(DoubleBuffer var1) {
        this.gl.glWindowPos3dvARB(var1);
    }

    public void glWindowPos3dvARB(double[] var1, int var2) {
        this.gl.glWindowPos3dvARB(var1, var2);
    }

    public void glWindowPos3dvMESA(DoubleBuffer var1) {
        this.gl.glWindowPos3dvMESA(var1);
    }

    public void glWindowPos3dvMESA(double[] var1, int var2) {
        this.gl.glWindowPos3dvMESA(var1, var2);
    }

    public void glWindowPos3f(float var1, float var2, float var3) {
        this.gl.glWindowPos3f(var1, var2, var3);
    }

    public void glWindowPos3fARB(float var1, float var2, float var3) {
        this.gl.glWindowPos3fARB(var1, var2, var3);
    }

    public void glWindowPos3fMESA(float var1, float var2, float var3) {
        this.gl.glWindowPos3fMESA(var1, var2, var3);
    }

    public void glWindowPos3fv(float[] var1, int var2) {
        this.gl.glWindowPos3fv(var1, var2);
    }

    public void glWindowPos3fv(FloatBuffer var1) {
        this.gl.glWindowPos3fv(var1);
    }

    public void glWindowPos3fvARB(FloatBuffer var1) {
        this.gl.glWindowPos3fvARB(var1);
    }

    public void glWindowPos3fvARB(float[] var1, int var2) {
        this.gl.glWindowPos3fvARB(var1, var2);
    }

    public void glWindowPos3fvMESA(FloatBuffer var1) {
        this.gl.glWindowPos3fvMESA(var1);
    }

    public void glWindowPos3fvMESA(float[] var1, int var2) {
        this.gl.glWindowPos3fvMESA(var1, var2);
    }

    public void glWindowPos3i(int var1, int var2, int var3) {
        this.gl.glWindowPos3i(var1, var2, var3);
    }

    public void glWindowPos3iARB(int var1, int var2, int var3) {
        this.gl.glWindowPos3iARB(var1, var2, var3);
    }

    public void glWindowPos3iMESA(int var1, int var2, int var3) {
        this.gl.glWindowPos3iMESA(var1, var2, var3);
    }

    public void glWindowPos3iv(int[] var1, int var2) {
        this.gl.glWindowPos3iv(var1, var2);
    }

    public void glWindowPos3iv(IntBuffer var1) {
        this.gl.glWindowPos3iv(var1);
    }

    public void glWindowPos3ivARB(int[] var1, int var2) {
        this.gl.glWindowPos3ivARB(var1, var2);
    }

    public void glWindowPos3ivARB(IntBuffer var1) {
        this.gl.glWindowPos3ivARB(var1);
    }

    public void glWindowPos3ivMESA(int[] var1, int var2) {
        this.gl.glWindowPos3ivMESA(var1, var2);
    }

    public void glWindowPos3ivMESA(IntBuffer var1) {
        this.gl.glWindowPos3ivMESA(var1);
    }

    public void glWindowPos3s(short var1, short var2, short var3) {
        this.gl.glWindowPos3s(var1, var2, var3);
    }

    public void glWindowPos3sARB(short var1, short var2, short var3) {
        this.gl.glWindowPos3sARB(var1, var2, var3);
    }

    public void glWindowPos3sMESA(short var1, short var2, short var3) {
        this.gl.glWindowPos3sMESA(var1, var2, var3);
    }

    public void glWindowPos3sv(short[] var1, int var2) {
        this.gl.glWindowPos3sv(var1, var2);
    }

    public void glWindowPos3sv(ShortBuffer var1) {
        this.gl.glWindowPos3sv(var1);
    }

    public void glWindowPos3svARB(short[] var1, int var2) {
        this.gl.glWindowPos3svARB(var1, var2);
    }

    public void glWindowPos3svARB(ShortBuffer var1) {
        this.gl.glWindowPos3svARB(var1);
    }

    public void glWindowPos3svMESA(short[] var1, int var2) {
        this.gl.glWindowPos3svMESA(var1, var2);
    }

    public void glWindowPos3svMESA(ShortBuffer var1) {
        this.gl.glWindowPos3svMESA(var1);
    }

    public void glWindowPos4dMESA(double var1, double var3, double var5, double var7) {
        this.gl.glWindowPos4dMESA(var1, var3, var5, var7);
    }

    public void glWindowPos4dvMESA(double[] var1, int var2) {
        this.gl.glWindowPos4dvMESA(var1, var2);
    }

    public void glWindowPos4dvMESA(DoubleBuffer var1) {
        this.gl.glWindowPos4dvMESA(var1);
    }

    public void glWindowPos4fMESA(float var1, float var2, float var3, float var4) {
        this.gl.glWindowPos4fMESA(var1, var2, var3, var4);
    }

    public void glWindowPos4fvMESA(float[] var1, int var2) {
        this.gl.glWindowPos4fvMESA(var1, var2);
    }

    public void glWindowPos4fvMESA(FloatBuffer var1) {
        this.gl.glWindowPos4fvMESA(var1);
    }

    public void glWindowPos4iMESA(int var1, int var2, int var3, int var4) {
        this.gl.glWindowPos4iMESA(var1, var2, var3, var4);
    }

    public void glWindowPos4ivMESA(IntBuffer var1) {
        this.gl.glWindowPos4ivMESA(var1);
    }

    public void glWindowPos4ivMESA(int[] var1, int var2) {
        this.gl.glWindowPos4ivMESA(var1, var2);
    }

    public void glWindowPos4sMESA(short var1, short var2, short var3, short var4) {
        this.gl.glWindowPos4sMESA(var1, var2, var3, var4);
    }

    public void glWindowPos4svMESA(short[] var1, int var2) {
        this.gl.glWindowPos4svMESA(var1, var2);
    }

    public void glWindowPos4svMESA(ShortBuffer var1) {
        this.gl.glWindowPos4svMESA(var1);
    }

    public void glWriteMaskEXT(int var1, int var2, int var3, int var4, int var5, int var6) {
        this.gl.glWriteMaskEXT(var1, var2, var3, var4, var5, var6);
    }

    public boolean isExtensionAvailable(String var1) {
        return this.gl.isExtensionAvailable(var1);
    }

    public boolean isFunctionAvailable(String var1) {
        return this.gl.isFunctionAvailable(var1);
    }

    public void setSwapInterval(int var1) {
        this.gl.setSwapInterval(var1);
    }

    public Object getExtension(String var1) {
        return this.gl.getExtension(var1);
    }
}
