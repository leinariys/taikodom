package taikodom.render.gl;

public class GLSupportedCaps {
    private static boolean vertexShader;
    private static boolean vertexAttrib;
    private static boolean glsl;
    private static boolean multitexture;
    private static boolean vbo;
    private static boolean dds;
    private static boolean drawRangeElements;
    private static boolean fbo;
    private static boolean pbuffer;
    private static boolean textureRectangle;
    private static boolean textureNPOT;
    private static boolean occlusionQuery;
    private static int maxTexChannels;

    public static boolean isGlsl() {
        return glsl;
    }

    protected static void setGlsl(boolean var0) {
        glsl = var0;
    }

    public static boolean isMultitexture() {
        return multitexture;
    }

    protected static void setMultitexture(boolean var0) {
        multitexture = var0;
    }

    public static boolean isVbo() {
        return vbo;
    }

    protected static void setVbo(boolean var0) {
        vbo = var0;
    }

    public static boolean isDds() {
        return dds;
    }

    protected static void setDds(boolean var0) {
        dds = var0;
    }

    public static boolean isDrawRangeElements() {
        return drawRangeElements;
    }

    protected static void setDrawRangeElements(boolean var0) {
        drawRangeElements = var0;
    }

    public static boolean isFbo() {
        return fbo;
    }

    protected static void setFbo(boolean var0) {
        fbo = var0;
    }

    public static boolean isVertexShader() {
        return vertexShader;
    }

    protected static void setVertexShader(boolean var0) {
        vertexShader = var0;
    }

    public static boolean isPbuffer() {
        return pbuffer;
    }

    protected static void setPbuffer(boolean var0) {
        pbuffer = var0;
    }

    public static boolean isTextureRectangle() {
        return textureRectangle;
    }

    protected static void setTextureRectangle(boolean var0) {
        textureRectangle = var0;
    }

    public static boolean isVertexAttrib() {
        return vertexAttrib;
    }

    protected static void setVertexAttrib(boolean var0) {
        vertexAttrib = var0;
    }

    public static boolean isTextureNPOT() {
        return textureNPOT;
    }

    protected static void setTextureNPOT(boolean var0) {
        textureNPOT = var0;
    }

    public static boolean isOcclusionQuery() {
        return occlusionQuery;
    }

    public static void setOcclusionQuery(boolean var0) {
        occlusionQuery = var0;
    }

    public static int getMaxTexChannels() {
        return maxTexChannels;
    }

    public static void setMaxTexChannels(int var0) {
        maxTexChannels = var0;
    }
}
