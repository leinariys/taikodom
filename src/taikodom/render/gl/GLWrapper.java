package taikodom.render.gl;

import all.LogPrinter;

import javax.media.opengl.GL;
import java.nio.Buffer;

/**
 *
 */
public class GLWrapper extends GLWrapperBase {
    private static LogPrinter logger = LogPrinter.K(GLWrapper.class);
    private static int MAX_USED_GL_CHANNELS = 8;

    public GLWrapper(GL var1) {
        super(var1);
        logger.info("Detecting video cards capabilities:");
        GLSupportedCaps.setVertexShader(var1.isExtensionAvailable("GL_ARB_vertex_shader"));
        GLSupportedCaps.setGlsl(var1.isExtensionAvailable("GL_ARB_shading_language_100"));
        GLSupportedCaps.setMultitexture(var1.isExtensionAvailable("GL_ARB_multitexture"));
        GLSupportedCaps.setVbo(var1.isExtensionAvailable("GL_ARB_vertex_buffer_object"));
        GLSupportedCaps.setTextureRectangle(var1.isExtensionAvailable("GL_ARB_texture_rectangle"));
        GLSupportedCaps.setTextureNPOT(var1.isExtensionAvailable("GL_ARB_texture_non_power_of_two"));
        GLSupportedCaps.setPbuffer(var1.isExtensionAvailable("GL_ARB_pbuffer"));
        GLSupportedCaps.setOcclusionQuery(var1.isExtensionAvailable("GL_ARB_occlusion_query"));
        int[] var2 = new int[1];
        var1.glGetIntegerv(34018, var2, 0);
        GLSupportedCaps.setMaxTexChannels(var2[0] < MAX_USED_GL_CHANNELS ? var2[0] : MAX_USED_GL_CHANNELS);
        GLSupportedCaps.setVertexAttrib(true);

        try {
            var1.glDisableVertexAttribArrayARB(0);
        } catch (Exception var8) {
            GLSupportedCaps.setVertexAttrib(false);
        }

        if (GLSupportedCaps.isOcclusionQuery()) {
            try {
                int[] var3 = new int[1];
                var1.glGenQueries(1, var3, 0);
                var1.glDeleteQueries(1, var3, 0);
            } catch (Exception var7) {
                GLSupportedCaps.setOcclusionQuery(false);
            }
        }

        try {
            var1.glBindBuffer(34962, 0);
        } catch (Exception var6) {
            GLSupportedCaps.setVbo(false);
        }

        GLSupportedCaps.setFbo(true);

        try {
            var1.glBindRenderbufferEXT(36160, 0);
        } catch (Exception var5) {
            GLSupportedCaps.setFbo(false);
        }

        if (GLSupportedCaps.isGlsl()) {
            try {
                var1.glLinkProgram(0);
            } catch (Exception var4) {
                GLSupportedCaps.setGlsl(false);
            }
        }

        GLSupportedCaps.setDds(var1.isExtensionAvailable("GL_ARB_texture_compression"));
        GLSupportedCaps.setDrawRangeElements(var1.isExtensionAvailable("GL_EXT_draw_range_elements"));
        logger.info("Vendor: " + var1.glGetString(7936));
        logger.info("Version: " + var1.glGetString(7938));
        logger.info("Renderer: " + var1.glGetString(7937));
        if (GLSupportedCaps.isGlsl()) {
            logger.info("GLSL supported");
        } else {
            logger.info("GLSL unsupported");
        }

        if (GLSupportedCaps.isMultitexture()) {
            logger.info("Multitexture supported");
        } else {
            logger.info("Multitexture unsupported");
        }

        if (GLSupportedCaps.isVbo()) {
            logger.info("VBO supported");
        } else {
            logger.info("VBO unsupported");
        }

        if (GLSupportedCaps.isDds()) {
            logger.info("Compressed texture supported");
        } else {
            logger.info("Compressed texture unsupported");
        }

        if (GLSupportedCaps.isDrawRangeElements()) {
            logger.info("Draw range elements supported");
        } else {
            logger.info("Draw range elements unsupported");
        }

        if (GLSupportedCaps.isPbuffer()) {
            logger.info("PBuffer supported");
        } else {
            logger.info("PBuffer unsupported");
        }

        if (GLSupportedCaps.isFbo()) {
            logger.info("Frame buffer object supported");
        } else {
            logger.info("Frame buffer object unsupported");
        }

        if (GLSupportedCaps.isTextureRectangle()) {
            logger.info("Texture rectangle supported");
        } else {
            logger.info("Texture rectangle unsupported");
        }

        if (GLSupportedCaps.isTextureNPOT()) {
            logger.info("Texture NPOT supported");
        } else {
            logger.info("Texture NPOT unsupported");
        }

    }

    public static LogPrinter getLogger() {
        return logger;
    }

    public void glEnableVertexAttribArray(int var1) {
        if (GLSupportedCaps.isVertexAttrib() && GLSupportedCaps.isVertexShader()) {
            super.glEnableVertexAttribArray(var1);
        }
    }

    public void glDisableVertexAttribArray(int var1) {
        if (GLSupportedCaps.isVertexAttrib() && GLSupportedCaps.isVertexShader()) {
            super.glDisableVertexAttribArray(var1);
        }
    }

    public void glEnableVertexAttribArrayARB(int var1) {
        if (GLSupportedCaps.isVertexAttrib() && GLSupportedCaps.isVertexShader()) {
            super.glEnableVertexAttribArrayARB(var1);
        }
    }

    public void glDisableVertexAttribArrayARB(int var1) {
        if (GLSupportedCaps.isVertexAttrib() && GLSupportedCaps.isVertexShader()) {
            super.glDisableVertexAttribArrayARB(var1);
        }
    }

    public int glCreateShaderObjectARB(int var1) {
        return GLSupportedCaps.isGlsl() ? super.glCreateShaderObjectARB(var1) : 0;
    }

    public void glBindBuffer(int var1, int var2) {
        if (GLSupportedCaps.isVbo()) {
            super.glBindBuffer(var1, var2);
        }

    }

    public void glBindBufferARB(int var1, int var2) {
        if (GLSupportedCaps.isVbo()) {
            super.glBindBufferARB(var1, var2);
        }

    }

    public void glBufferData(int var1, int var2, Buffer var3, int var4) {
        if (GLSupportedCaps.isVbo()) {
            super.glBufferData(var1, var2, var3, var4);
        }

    }

    public int glCreateProgramObjectARB() {
        return GLSupportedCaps.isGlsl() ? super.glCreateProgramObjectARB() : 0;
    }

    public void glBindFramebufferEXT(int var1, int var2) {
        if (GLSupportedCaps.isFbo()) {
            super.glBindFramebufferEXT(var1, var2);
        }

    }

    public void glUseProgramObjectARB(int var1) {
        if (GLSupportedCaps.isGlsl()) {
            super.glUseProgramObjectARB(var1);
        }

    }

    public void glLinkProgram(int var1) {
        if (GLSupportedCaps.isGlsl()) {
            super.glLinkProgram(var1);
        }

    }
}
