package taikodom.render.graphics2d;

import taikodom.render.DrawContext;
import taikodom.render.data.IntIndexData;

import javax.media.opengl.GL;

public abstract class DrawNode {
    public abstract void draw(DrawContext var1, RGuiScene var2);

    protected void drawElements(GL var1, IntIndexData var2, int var3, int var4, int var5) {
        var2.buffer().position(var4);
        var1.glDrawElements(var3, var5, 5125, var2.buffer());
    }
}
