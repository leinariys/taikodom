package taikodom.render.graphics2d;

import all.ajK;
import all.nu;
import taikodom.render.DrawContext;
import taikodom.render.TexBackedImage;
import taikodom.render.gl.GLSupportedCaps;
import taikodom.render.textures.BaseTexture;
import taikodom.render.textures.RenderTarget;
import taikodom.render.textures.Texture;

import javax.media.opengl.GL;
import javax.swing.*;
import java.awt.*;
import java.awt.RenderingHints.Key;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.awt.geom.*;
import java.awt.geom.Point2D.Double;
import java.awt.geom.Point2D.Float;
import java.awt.image.*;
import java.awt.image.renderable.RenderableImage;
import java.text.AttributedCharacterIterator;
import java.util.Map;

public class RGraphics2 extends Graphics2D {
    public static final int LEFT = 1;
    public static final int TOP = 2;
    public static final int RIGHT = 4;
    public static final int BOTTOM = 8;
    private static Point2D tempP1 = new Float();
    private static Point2D tempP2 = new Float();
    private static Point2D tempP3 = new Float();
    private static Point2D tempP4 = new Float();
    private Color color;
    private Color backgroundColor;
    private Color composedColorMultiplier;
    private Color parentColorMultiplier;
    private Font font;
    private Paint paint;
    private taikodom.render.textures.Font graphicalFont;
    private AffineTransform transform;
    private Rectangle untClip;
    private Stroke stroke;
    private boolean clippingOn;
    private RenderingHints hints;
    private RGraphicsContext context;
    private RGuiScene guiScene;
    private float lineWidth;
    private Composite composite;
    private Color colorMultiplier;
    private RGraphics2 parent;

    protected RGraphics2(RGraphics2 var1) {
        this.init(var1);
    }

    /**
     * @deprecated
     */
    @Deprecated
    public RGraphics2(DrawContext var1) {
        this(new RGraphicsContext(var1));
    }

    public RGraphics2(RGraphicsContext var1) {
        this.context = var1;
        this.guiScene = new RGuiScene(var1);
        this.untClip = new Rectangle(0, 0, 0, 0);
        this.backgroundColor = Color.WHITE;
        this.color = Color.WHITE;
        this.composedColorMultiplier = Color.WHITE;
        this.parentColorMultiplier = Color.WHITE;
        this.transform = new AffineTransform();
        this.lineWidth = 1.0F;
        this.clippingOn = false;
    }

    protected static Shape transformShape(AffineTransform var0, Shape var1) {
        if (var1 == null) {
            return null;
        } else if (var1 instanceof Rectangle2D && (var0.getType() & 48) == 0) {
            Rectangle2D var2 = (Rectangle2D) var1;
            double[] var3 = new double[]{var2.getX(), var2.getY(), var2.getMaxX(), var2.getMaxY()};
            var0.transform(var3, 0, var3, 0, 2);
            java.awt.geom.Rectangle2D.Float var4 = new java.awt.geom.Rectangle2D.Float();
            var4.setFrameFromDiagonal(var3[0], var3[1], var3[2], var3[3]);
            return var4;
        } else {
            return var0.isIdentity() ? cloneShape(var1) : var0.createTransformedShape(var1);
        }
    }

    protected static Shape cloneShape(Shape var0) {
        return new GeneralPath(var0);
    }

    public void reset() {
        this.clippingOn = false;
        this.guiScene.reset();
    }

    public void render() {
        this.guiScene.draw((RenderTarget) null);
    }

    public void init(RGraphics2 var1) {
        this.parent = var1;
        this.context = var1.context;
        this.backgroundColor = var1.backgroundColor;
        this.color = var1.color;
        this.lineWidth = 1.0F;
        this.clippingOn = false;
        if (this.transform == null) {
            this.transform = (AffineTransform) var1.transform.clone();
        } else {
            this.transform.setTransform(var1.transform);
        }

        if (this.untClip == null) {
            this.untClip = new Rectangle(-10000, -10000, 20000, 20000);
        }

        this.composedColorMultiplier = var1.composedColorMultiplier;
        this.parentColorMultiplier = var1.composedColorMultiplier;
        this.untClip.setBounds(var1.untClip);
        this.hints = var1.hints;
        this.clippingOn = var1.clippingOn;
        this.font = var1.font;
        this.paint = var1.paint;
        this.guiScene = var1.guiScene;
    }

    public RGraphicsContext getContext() {
        return this.context;
    }

    public void addRenderingHints(Map var1) {
    }

    public void clip(Shape var1) {
        if (var1 == null) {
            this.clippingOn = false;
        } else {
            Rectangle var2 = var1.getBounds();
            this.clipRect(var2.x, var2.y, var2.width, var2.height);
        }
    }

    public void draw(Shape var1) {
        if (this.color.getAlpha() != 0) {
            float[] var2 = new float[6];
            float var3 = 0.0F;
            float var4 = 0.0F;
            this.guiScene.setLineWidth((float) ((double) this.lineWidth * this.transform.getScaleX()));
            int var5 = this.guiScene.getLastIdx();
            this.guiScene.setBlendFunc(770, 771);
            this.guiScene.setClip(this);
            this.guiScene.setColor(this.color, this.composedColorMultiplier);

            for (FlatteningPathIterator var6 = new FlatteningPathIterator(var1.getPathIterator(this.transform), 1.0D); !var6.isDone(); var6.next()) {
                int var7 = var6.currentSegment(var2);
                switch (var7) {
                    case 0:
                        var3 = var2[0];
                        var4 = var2[1];
                        if (this.guiScene.getLastIdx() - var5 >= 2) {
                            this.guiScene.add(new nu(3, var5, this.guiScene.getLastIdx() - var5));
                        }

                        var5 = this.guiScene.getLastIdx();
                        break;
                    case 1:
                        if (this.guiScene.getLastIdx() == var5) {
                            this.guiScene.addVertex(var3, var4, 0.0F);
                        }

                        var3 = var2[0];
                        var4 = var2[1];
                        this.guiScene.addVertex(var3, var4, 0.0F);
                    case 2:
                    case 3:
                    default:
                        break;
                    case 4:
                        if (this.guiScene.getLastIdx() - var5 >= 2) {
                            this.guiScene.add(new nu(2, var5, this.guiScene.getLastIdx() - var5));
                        }

                        var5 = this.guiScene.getLastIdx();
                }
            }

            if (this.guiScene.getLastIdx() - var5 >= 2) {
                this.guiScene.add(new nu(3, var5, this.guiScene.getLastIdx() - var5));
            }

        }
    }

    public void drawGlyphVector(GlyphVector var1, float var2, float var3) {
    }

    public boolean drawImage(Image var1, AffineTransform var2, ImageObserver var3) {
        this.drawImage(var1, (int) var2.getTranslateX(), (int) var2.getTranslateY(), var3);
        return false;
    }

    public void drawImage(BufferedImage var1, BufferedImageOp var2, int var3, int var4) {
    }

    public void drawRenderableImage(RenderableImage var1, AffineTransform var2) {
    }

    public void drawRenderedImage(RenderedImage var1, AffineTransform var2) {
    }

    public void drawString(String var1, int var2, int var3) {
        this.drawString(var1, (float) var2, (float) var3);
    }

    public void drawString(String var1, float var2, float var3) {
        if (var1 != null && var1.length() != 0) {
            this.guiScene.setClip(this);
            this.guiScene.setColor(this.color, this.composedColorMultiplier);
            this.graphicalFont = this.context.getFont(this.font);
            if (this.transform.getType() != 0 && this.transform.getType() != 1) {
                final double[] var4 = new double[16];
                this.transform.getMatrix(var4);
                var4[15] = 1.0D;
                var4[13] = var4[5];
                var4[12] = var4[4];
                var4[5] = var4[3];
                var4[4] = var4[2];
                var4[2] = 0.0D;
                var4[3] = 0.0D;
                this.guiScene.add(new TextDraw(this.graphicalFont, this.color, var1, var2, var3) {
                    public void draw(DrawContext var1, RGuiScene var2) {
                        /**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
                         * седержит методы для рисования примитивов (линии треугольники)*/
                        GL var3 = var1.getGL();
                        var3.glPushMatrix();
                        var3.glMultMatrixd(var4, 0);
                        super.draw(var1, var2);
                        var3.glPopMatrix();
                    }
                });
            } else {
                this.guiScene.add(new TextDraw(this.graphicalFont, this.color, var1, (float) ((double) var2 + this.transform.getTranslateX()), (float) ((double) var3 + this.transform.getTranslateY())));
            }

        }
    }

    public void drawString(AttributedCharacterIterator var1, int var2, int var3) {
    }

    public void drawString(AttributedCharacterIterator var1, float var2, float var3) {
    }

    private void drawTexPolygon(TexPolygon var1) {
        Image var2 = var1.getImage();
        this.guiScene.setClip(this);
        AffineTransform var3 = new AffineTransform(this.transform);
        float[] var4 = new float[6];
        int var5 = this.guiScene.getLastIdx();
        BaseTexture var6 = this.context.getTexture(var2);
        float var7 = 0.0F;
        float var8 = 0.0F;
        float var9 = 0.0F;
        float var10 = 0.0F;
        boolean var11 = false;

        for (PathIterator var12 = var1.getPathIterator(var3); !var12.isDone(); var12.next()) {
            int var13 = var12.currentSegment(var4);
            switch (var13) {
                case 0:
                    var7 = var4[0];
                    var8 = var4[1];
                    var9 = var4[2];
                    var10 = var4[3];
                    var11 = false;
                    break;
                case 1:
                    if (!var11) {
                        this.guiScene.addVertex(var7, var8, 0.0F);
                        this.guiScene.setCurrentTexCoord(0, var9, var10);
                    }

                    var7 = var4[0];
                    var8 = var4[1];
                    var9 = var4[2];
                    var10 = var4[3];
                    this.guiScene.addVertex(var7, var8, 0.0F);
                    this.guiScene.setCurrentTexCoord(0, var9, var10);
                    var11 = true;
                    break;
                default:
                    var11 = false;
            }
        }

        if (this.guiScene.getLastIdx() - var5 >= 3) {
            this.guiScene.setBlendFunc(770, 771);
            this.guiScene.add(new TexPolyDraw(var6, 6, var5, this.guiScene.getLastIdx() - var5));
        }

        if (!(this.paint instanceof Color)) {
            this.guiScene.setEnableColorArray(false);
        }

        this.guiScene.setPaintContext((PaintContext) null, false);
    }

    public void fill(Shape var1) {
        if (this.color.getAlpha() != 0) {
            if (var1 instanceof TexPolygon) {
                this.drawTexPolygon((TexPolygon) var1);
            } else {
                int var2 = this.guiScene.getLastIdx();
                float[] var3 = new float[6];
                float var4 = 0.0F;
                float var5 = 0.0F;
                Rectangle2D var6 = var1.getBounds2D();
                Double var7 = new Double(var6.getCenterX(), var6.getCenterY());
                AffineTransform var8 = new AffineTransform(this.transform);
                Point2D var12 = var8.transform(var7, var7);
                this.guiScene.setClip(this);
                this.guiScene.setColor(this.color, this.composedColorMultiplier);
                if (this.paint != null && !(this.paint instanceof Color)) {
                    this.guiScene.setEnableColorArray(true);
                    PaintContext var9 = this.paint.createContext(ColorModel.getRGBdefault(), new Rectangle(0, 0, 1000, 1000), new Rectangle(0, 0, 0, 0), var8, (RenderingHints) null);
                    this.guiScene.setPaintContext(var9, this.paint.getTransparency() != 1);
                }

                this.guiScene.addVertex((float) var12.getX(), (float) var12.getY(), 0.0F);
                boolean var13 = false;

                for (FlatteningPathIterator var10 = new FlatteningPathIterator(var1.getPathIterator(var8), 1.0D); !var10.isDone(); var10.next()) {
                    int var11 = var10.currentSegment(var3);
                    switch (var11) {
                        case 0:
                            var4 = var3[0];
                            var5 = var3[1];
                            var13 = false;
                            break;
                        case 1:
                            if (!var13) {
                                this.guiScene.addVertex(var4, var5, 0.0F);
                            }

                            var4 = var3[0];
                            var5 = var3[1];
                            this.guiScene.addVertex(var4, var5, 0.0F);
                            var13 = true;
                            break;
                        default:
                            var13 = false;
                    }
                }

                if (this.guiScene.getLastIdx() - var2 >= 3) {
                    this.guiScene.setBlendFunc(770, 771);
                    this.guiScene.add(new nu(6, var2, this.guiScene.getLastIdx() - var2));
                }

                if (!(this.paint instanceof Color)) {
                    this.guiScene.setEnableColorArray(false);
                }

                this.guiScene.setPaintContext((PaintContext) null, false);
            }
        }
    }

    public Color getBackground() {
        return this.backgroundColor;
    }

    public void setBackground(Color var1) {
        this.backgroundColor = var1;
    }

    public Composite getComposite() {
        return this.composite;
    }

    public void setComposite(Composite var1) {
        this.composite = var1;
    }

    public GraphicsConfiguration getDeviceConfiguration() {
        return null;
    }

    public FontRenderContext getFontRenderContext() {
        return null;
    }

    public Paint getPaint() {
        return this.paint;
    }

    public void setPaint(Paint var1) {
        this.paint = var1;
        if (var1 instanceof Color) {
            this.color = (Color) var1;
        }

    }

    public Object getRenderingHint(Key var1) {
        return this.hints == null ? null : this.hints.get(var1);
    }

    public RenderingHints getRenderingHints() {
        return this.hints;
    }

    public void setRenderingHints(Map var1) {
    }

    public Stroke getStroke() {
        return this.stroke;
    }

    public void setStroke(Stroke var1) {
        if (var1 != null) {
            this.lineWidth = ((BasicStroke) var1).getLineWidth();
        }

    }

    public AffineTransform getTransform() {
        return this.transform;
    }

    public void setTransform(AffineTransform var1) {
        this.transform.setTransform(var1);
    }

    public void setTransform(ajK var1) {
    }

    public AffineTransform internalGetTransform() {
        return this.transform;
    }

    public boolean hit(Rectangle var1, Shape var2, boolean var3) {
        return false;
    }

    public boolean hitClip(int var1, int var2, int var3, int var4) {
        Rectangle var5 = this.untClip;
        if (this.clippingOn && var5 != null) {
            if (this.transform.getType() == 1) {
                double var6 = this.transform.getTranslateX();
                double var8 = this.transform.getTranslateY();
                return var5.intersects((double) var1 + var6, (double) var2 + var8, (double) var3, (double) var4);
            } else {
                return this.transform.getType() == 0 ? var5.intersects((double) var1, (double) var2, (double) var3, (double) var4) : true;
            }
        } else {
            return true;
        }
    }

    public void rotate(double var1) {
        this.transform.rotate(var1);
    }

    public void rotate(double var1, double var3, double var5) {
        this.transform.rotate(var1, var3, var5);
    }

    public void scale(double var1, double var3) {
        this.transform.scale(var1, var3);
    }

    public void setRenderingHint(Key var1, Object var2) {
        if (this.hints == null) {
            this.hints = new RenderingHints(var1, var2);
        } else {
            this.hints.put(var1, var2);
        }

    }

    public void unsetTransform() {
    }

    public void shear(double var1, double var3) {
        this.transform.shear(var1, var3);
    }

    public void transform(AffineTransform var1) {
        this.transform.concatenate(var1);
    }

    public void translate(int var1, int var2) {
        this.translate((double) var1, (double) var2);
    }

    public void translate(double var1, double var3) {
        this.transform.translate(var1, var3);
    }

    public void clearRect(int var1, int var2, int var3, int var4) {
        int var5 = (int) this.transform.getTranslateX();
        int var6 = (int) this.transform.getTranslateY();
        this.fillRect(var5, var6, var3, var4);
    }

    public void clipRect(int var1, int var2, int var3, int var4) {
        if (!this.clippingOn) {
            this.setClip(var1, var2, var3, var4);
        } else {
            Rectangle var5 = this.transform.createTransformedShape(new Rectangle(var1, var2, var3, var4)).getBounds();
            SwingUtilities.computeIntersection(var5.x, var5.y, var5.width, var5.height, this.untClip);
        }
    }

    public void copyArea(int var1, int var2, int var3, int var4, int var5, int var6) {
    }

    public Graphics create() {
        RGraphics2 var1 = this.context.createGraphics();
        var1.init(this);
        return var1;
    }

    public void dispose() {
    }

    public void drawArc(int var1, int var2, int var3, int var4, int var5, int var6) {
        this.draw(new java.awt.geom.Arc2D.Float((float) var1, (float) var2, (float) var3, (float) var4, (float) var5, (float) var6, 0));
    }

    public boolean drawImage(Image var1, int var2, int var3, ImageObserver var4) {
        return this.drawImage(var1, var2, var3, var2 + var1.getWidth(var4), var3 + var1.getHeight(var4), 0, 0, var1.getWidth(var4), var1.getHeight(var4), this.backgroundColor, var4);
    }

    public boolean drawImage(Image var1, int var2, int var3, Color var4, ImageObserver var5) {
        return this.drawImage(var1, var2, var3, var2 + var1.getWidth(var5), var3 + var1.getHeight(var5), 0, 0, var1.getWidth(var5), var1.getHeight(var5), var4, var5);
    }

    public boolean drawImage(Image var1, int var2, int var3, int var4, int var5, ImageObserver var6) {
        return this.drawImage(var1, var2, var3, var2 + var4, var3 + var5, 0, 0, var1.getWidth(var6), var1.getHeight(var6), this.backgroundColor, var6);
    }

    public boolean drawImage(Image var1, int var2, int var3, int var4, int var5, Color var6, ImageObserver var7) {
        return this.drawImage(var1, var2, var3, var2 + var4, var3 + var5, 0, 0, var1.getWidth(var7), var1.getHeight(var7), var6, var7);
    }

    public boolean drawImage(Image var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, ImageObserver var10) {
        return this.drawImage(var1, var2, var3, var4, var5, var6, var7, var8, var9, (Color) null, var10);
    }

    public boolean drawImage(Image var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, Color var10, ImageObserver var11) {
        if (var1 == null) {
            return false;
        } else if (var1.getWidth((ImageObserver) null) != -1 && var1.getHeight((ImageObserver) null) != -1) {
            if (var10 != null && var10.getAlpha() == 0) {
                return false;
            } else {
                BaseTexture var12 = this.context.getTexture(var1);
                if (var12 == null) {
                    return false;
                } else {
                    tempP1.setLocation((double) var4, (double) var3);
                    tempP2.setLocation((double) var2, (double) var3);
                    tempP3.setLocation((double) var2, (double) var5);
                    tempP4.setLocation((double) var4, (double) var5);
                    this.transform.transform(tempP1, tempP1);
                    this.transform.transform(tempP2, tempP2);
                    this.transform.transform(tempP3, tempP3);
                    this.transform.transform(tempP4, tempP4);
                    int var13 = this.guiScene.getLastIdx();
                    float var14 = 1.0F;
                    float var15 = 1.0F;
                    if (var1 instanceof TexBackedImage) {
                        TexBackedImage var16 = (TexBackedImage) var1;
                        var14 = var16.getXmul();
                        var15 = var16.getYmul();
                    } else if (var12.getTarget() == 34037) {
                        var14 = (float) var1.getWidth(var11);
                        var15 = (float) var1.getHeight(var11);
                    } else if ((!Texture.isPowerOf2(var1.getWidth(var11)) || !Texture.isPowerOf2(var1.getHeight(var11))) && !GLSupportedCaps.isTextureNPOT()) {
                        int var18 = Texture.roundToPowerOf2(var1.getWidth(var11));
                        int var17 = Texture.roundToPowerOf2(var1.getHeight(var11));
                        var14 = (float) var1.getWidth(var11) / (float) var18;
                        var15 = (float) var1.getHeight(var11) / (float) var17;
                    }

                    float var19 = (float) var1.getWidth(var11);
                    float var20 = (float) var1.getHeight(var11);
                    this.guiScene.setClip(this);
                    this.guiScene.setColor(var10, this.composedColorMultiplier);
                    this.guiScene.addVertex((float) tempP1.getX(), (float) tempP1.getY(), 0.0F);
                    this.guiScene.setCurrentTexCoord(0, (float) var8 / var19 * var14, (float) var7 / var20 * var15);
                    this.guiScene.addVertex((float) tempP2.getX(), (float) tempP2.getY(), 0.0F);
                    this.guiScene.setCurrentTexCoord(0, (float) var6 / var19 * var14, (float) var7 / var20 * var15);
                    this.guiScene.addVertex((float) tempP3.getX(), (float) tempP3.getY(), 0.0F);
                    this.guiScene.setCurrentTexCoord(0, (float) var6 / var19 * var14, (float) var9 / var20 * var15);
                    this.guiScene.addVertex((float) tempP4.getX(), (float) tempP4.getY(), 0.0F);
                    this.guiScene.setCurrentTexCoord(0, (float) var8 / var19 * var14, (float) var9 / var20 * var15);
                    this.guiScene.setEnableColorArray(false);
                    this.guiScene.setBlendFunc(1, 771);
                    this.guiScene.add(new ImageDraw(var12, var13, 4));
                    return true;
                }
            }
        } else {
            return false;
        }
    }

    int outcode(float var1, float var2) {
        int var3 = 0;
        if (var2 < (float) this.untClip.y) {
            var3 |= 8;
        } else if (var2 > (float) (this.untClip.y + this.untClip.height)) {
            var3 |= 2;
        }

        if (var1 > (float) (this.untClip.x + this.untClip.width)) {
            var3 |= 4;
        } else if (var1 < (float) this.untClip.x) {
            var3 |= 1;
        }

        return var3;
    }

    public void drawLine(int var1, int var2, int var3, int var4) {
        this.draw(new java.awt.geom.Line2D.Float((float) var1, (float) var2, (float) var3, (float) var4));
    }

    public void drawOval(int var1, int var2, int var3, int var4) {
        this.draw(new java.awt.geom.Ellipse2D.Float((float) var1, (float) var2, (float) var3, (float) var4));
    }

    public void drawPolygon(int[] var1, int[] var2, int var3) {
        this.draw(new Polygon(var1, var2, var3));
    }

    public void drawPolyline(int[] var1, int[] var2, int var3) {
        for (int var4 = 0; var4 < var3 - 1; ++var4) {
            this.drawLine(var1[var4], var2[var4], var1[var4 + 1], var2[var4 + 1]);
        }

    }

    public void drawRoundRect(int var1, int var2, int var3, int var4, int var5, int var6) {
        this.draw(new java.awt.geom.RoundRectangle2D.Float((float) var1, (float) var2, (float) var3, (float) var4, (float) var5, (float) var6));
    }

    public void fillArc(int var1, int var2, int var3, int var4, int var5, int var6) {
        this.draw(new java.awt.geom.Arc2D.Float((float) var1, (float) var2, (float) var3, (float) var4, (float) var5, (float) var6, 2));
    }

    public void fillOval(int var1, int var2, int var3, int var4) {
        this.fill(new java.awt.geom.Ellipse2D.Float((float) var1, (float) var2, (float) var3, (float) var4));
    }

    public void fillPolygon(int[] var1, int[] var2, int var3) {
        this.fill(new Polygon(var1, var2, var3));
    }

    public void fillRect(int var1, int var2, int var3, int var4) {
        this.fill(new java.awt.geom.Rectangle2D.Float((float) var1, (float) var2, (float) var3, (float) var4));
    }

    public void fillRoundRect(int var1, int var2, int var3, int var4, int var5, int var6) {
        this.fill(new java.awt.geom.RoundRectangle2D.Float((float) var1, (float) var2, (float) var3, (float) var4, (float) var5, (float) var6));
    }

    public Color getColor() {
        return this.color;
    }

    public void setColor(Color var1) {
        this.color = var1;
    }

    public Font getFont() {
        return this.font;
    }

    public void setFont(Font var1) {
        if (var1 != this.font) {
            this.font = var1;
            this.graphicalFont = this.context.getFont(var1);
        }

    }

    public taikodom.render.textures.Font getGraphicalFont() {
        return this.graphicalFont;
    }

    public FontMetrics getFontMetrics(Font var1) {
        return this.context.getFontMetrics(var1);
    }

    public void setClip(int var1, int var2, int var3, int var4) {
        this.setClip(new Rectangle(var1, var2, var3, var4));
    }

    private Shape untransformShape(Shape var1) {
        if (var1 == null) {
            return null;
        } else {
            try {
                return transformShape(this.transform.createInverse(), var1);
            } catch (NoninvertibleTransformException var3) {
                throw new RuntimeException(var3);
            }
        }
    }

    public Shape getClip() {
        return this.untransformShape(this.untClip);
    }

    public void setClip(Shape var1) {
        if (var1 == null) {
            this.clippingOn = false;
            this.untClip.setBounds(-10000, -10000, 20000, 20000);
        } else {
            this.clippingOn = true;
            this.untClip.setBounds(this.transform.createTransformedShape(var1).getBounds());
        }

    }

    public Rectangle getClipBounds() {
        if (!this.clippingOn) {
            return null;
        } else {
            Rectangle var1 = this.getClip().getBounds();
            return var1;
        }
    }

    public Rectangle getClipBounds(Rectangle var1) {
        if (var1 == null) {
            throw new NullPointerException("null rectangle parameter");
        } else {
            if (this.clippingOn) {
                var1.setBounds(this.getClipBounds());
            }

            return var1;
        }
    }

    public void drawRect(int var1, int var2, int var3, int var4) {
        if (this.color.getAlpha() != 0) {
            if (var3 >= 0 && var4 >= 0) {
                this.draw(new Rectangle(var1, var2, var3, var4));
            }
        }
    }

    public void setPaintMode() {
    }

    public void setXORMode(Color var1) {
    }

    public Color getBackgroundColor() {
        return this.backgroundColor;
    }

    public void setBackgroundColor(Color var1) {
        this.backgroundColor = var1;
    }

    public Graphics create(int var1, int var2, int var3, int var4) {
        Graphics var5 = this.create();
        if (var5 == null) {
            return null;
        } else {
            var5.translate(var1, var2);
            var5.clipRect(0, 0, var3, var4);
            return var5;
        }
    }

    public RGuiScene getGuiScene() {
        return this.guiScene;
    }

    public void untransformedClipBounds(Rectangle var1) {
        var1.setBounds(this.untClip);
    }

    public Color getColorMultiplier() {
        return this.colorMultiplier;
    }

    public void setColorMultiplier(Color var1) {
        this.colorMultiplier = var1;
        if (var1 != null && !Color.WHITE.equals(var1)) {
            this.composedColorMultiplier = new Color(var1.getRed() * this.parentColorMultiplier.getRed() / 255, var1.getGreen() * this.parentColorMultiplier.getGreen() / 255, var1.getBlue() * this.parentColorMultiplier.getBlue() / 255, var1.getAlpha() * this.parentColorMultiplier.getAlpha() / 255);
        } else {
            this.composedColorMultiplier = this.parent.composedColorMultiplier;
        }

    }

    public void blur(int var1, int var2, int var3, int var4) {
        var1 = (int) ((double) var1 + this.transform.getTranslateX());
        var2 = (int) ((double) var2 + this.transform.getTranslateY());
        this.guiScene.setClip(this);
        this.guiScene.addBlur(var1, var2, var3, var4);
    }

    public RGraphics2 getParent() {
        return this.parent;
    }
}
