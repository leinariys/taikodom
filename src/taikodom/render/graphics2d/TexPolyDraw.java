package taikodom.render.graphics2d;

import taikodom.render.DrawContext;
import taikodom.render.textures.BaseTexture;

public class TexPolyDraw extends DrawNode {
    protected int bufferPosition;
    protected int count;
    protected int elementType;
    private BaseTexture texture;

    public TexPolyDraw(BaseTexture var1, int var2, int var3, int var4) {
        this.texture = var1;
        this.bufferPosition = var3;
        this.count = var4;
        this.elementType = var2;
    }

    public void draw(DrawContext var1, RGuiScene var2) {
        this.texture.bind(var1);
        var2.getIndexes().buffer().position(this.bufferPosition);
        var1.getGL().glDrawElements(this.elementType, this.count, 5125, var2.getIndexes().buffer());
        var1.getGL().glDisable(this.texture.getTarget());
    }
}
