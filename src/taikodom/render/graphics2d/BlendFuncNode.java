package taikodom.render.graphics2d;

import taikodom.render.DrawContext;

public class BlendFuncNode extends DrawNode {
    private int sfactor;
    private int dfactor;

    public BlendFuncNode(int var1, int var2) {
        this.sfactor = var1;
        this.dfactor = var2;
    }

    public void draw(DrawContext var1, RGuiScene var2) {
        var1.getGL().glBlendFunc(this.sfactor, this.dfactor);
    }
}
