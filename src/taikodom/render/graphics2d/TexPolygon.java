package taikodom.render.graphics2d;

import taikodom.render.textures.Texture;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;
import java.awt.image.ImageObserver;
import java.util.Arrays;

public class TexPolygon extends Polygon {
    private static final long serialVersionUID = 1L;
    private static final int MIN_LENGTH = 4;
    public float[] upoints;
    public float[] vpoints;
    private Image image;

    public TexPolygon(Image var1) {
        this.image = var1;
        this.upoints = new float[4];
        this.vpoints = new float[4];
    }

    public void addPoint(int var1, int var2) {
        super.addPoint(var1, var2);
        this.addUV(0.0F, 0.0F);
    }

    public void addPoint(int var1, int var2, float var3, float var4) {
        super.addPoint(var1, var2);
        this.addUV(var3, var4);
    }

    private void addUV(float var1, float var2) {
        if (this.npoints >= this.upoints.length || this.npoints >= this.vpoints.length) {
            int var3 = this.npoints * 2;
            if (var3 < 4) {
                var3 = 4;
            } else if ((var3 & var3 - 1) != 0) {
                var3 = Integer.highestOneBit(var3);
            }

            this.upoints = Arrays.copyOf(this.upoints, var3);
            this.vpoints = Arrays.copyOf(this.vpoints, var3);
        }

        this.upoints[this.npoints - 1] = var1;
        this.vpoints[this.npoints - 1] = var2;
    }

    public Image getImage() {
        return this.image;
    }

    public PathIterator getPathIterator(AffineTransform var1) {
        return new TexPolygon.b(this, var1);
    }

    public PathIterator getPathIterator(AffineTransform var1, double var2) {
        return this.getPathIterator(var1);
    }

    public void createOctagon(int var1) {
        int var2 = this.image.getWidth((ImageObserver) null);
        int var3 = this.image.getHeight((ImageObserver) null);
        if (var2 == -1 || var3 == -1) {
            TexPolygon.a var4 = new TexPolygon.a(this, var1);
            var2 = this.image.getWidth(var4);
            var3 = this.image.getHeight(var4);
        }

        if (var2 != -1 && var3 != -1) {
            this.createOctagon(var1, var2, var3);
        }

    }

    private synchronized void createOctagon(int var1, int var2, int var3) {
        if (this.npoints <= 0) {
            float var4;
            float var5;
            if ((!Texture.isPowerOf2(var2) || !Texture.isPowerOf2(var3)) && !Texture.haveNPOT()) {
                if (Texture.haveTexRect()) {
                    var4 = (float) var1;
                    var5 = (float) var1;
                    this.addPoint(var1, 0, var4, 0.0F);
                    this.addPoint(var2 - var1, 0, (float) var2 - var4, 0.0F);
                    this.addPoint(var2, var1, (float) var2, var5);
                    this.addPoint(var2, var3 - var1, (float) var2, (float) var3 - var5);
                    this.addPoint(var2 - var1, var3, (float) var2 - var4, (float) var3);
                    this.addPoint(var1, var3, var4, (float) var3);
                    this.addPoint(0, var3 - var1, 0.0F, (float) var2 - var5);
                    this.addPoint(0, var1, 0.0F, var5);
                } else {
                    var4 = (float) var1 / (float) var2;
                    var5 = (float) var1 / (float) var3;
                    float var6 = (float) var2 / (float) Texture.roundToPowerOf2(var2);
                    float var7 = (float) var3 / (float) Texture.roundToPowerOf2(var3);
                    this.addPoint(var1, 0, var4 * var6, 0.0F);
                    this.addPoint(var2 - var1, 0, (1.0F - var4) * var6, 0.0F);
                    this.addPoint(var2, var1, 1.0F * var6, var5 * var7);
                    this.addPoint(var2, var3 - var1, 1.0F * var6, (1.0F - var5) * var7);
                    this.addPoint(var2 - var1, var3, (1.0F - var4) * var6, 1.0F * var7);
                    this.addPoint(var1, var3, var4 * var6, 1.0F * var7);
                    this.addPoint(0, var3 - var1, 0.0F, (1.0F - var5) * var7);
                    this.addPoint(0, var1, 0.0F, var5);
                }
            } else {
                var4 = (float) var1 / (float) var2;
                var5 = (float) var1 / (float) var3;
                this.addPoint(var1, 0, var4, 0.0F);
                this.addPoint(var2 - var1, 0, 1.0F - var4, 0.0F);
                this.addPoint(var2, var1, 1.0F, var5);
                this.addPoint(var2, var3 - var1, 1.0F, 1.0F - var5);
                this.addPoint(var2 - var1, var3, 1.0F - var4, 1.0F);
                this.addPoint(var1, var3, var4, 1.0F);
                this.addPoint(0, var3 - var1, 0.0F, 1.0F - var5);
                this.addPoint(0, var1, 0.0F, var5);
            }

        }
    }

    private static final class a implements ImageObserver {
        private TexPolygon hdK;
        private int hdL;

        public a(TexPolygon var1, int var2) {
            this.hdK = var1;
            this.hdL = var2;
        }

        public boolean imageUpdate(Image var1, int var2, int var3, int var4, int var5, int var6) {
            this.hdK.createOctagon(this.hdL, var5, var6);
            return false;
        }
    }

    class b implements PathIterator {
        TexPolygon iXG;
        AffineTransform transform;
        int index;

        public b(TexPolygon var2, AffineTransform var3) {
            this.iXG = var2;
            this.transform = var3;
            if (var2.npoints == 0) {
                this.index = 1;
            }

        }

        public int getWindingRule() {
            return 0;
        }

        public boolean isDone() {
            return this.index > this.iXG.npoints;
        }

        public void next() {
            ++this.index;
        }

        public int currentSegment(float[] var1) {
            if (this.index >= this.iXG.npoints) {
                return 4;
            } else {
                var1[0] = (float) this.iXG.xpoints[this.index];
                var1[1] = (float) this.iXG.ypoints[this.index];
                var1[2] = this.iXG.upoints[this.index];
                var1[3] = this.iXG.vpoints[this.index];
                if (this.transform != null) {
                    this.transform.transform(var1, 0, var1, 0, 1);
                }

                return this.index == 0 ? 0 : 1;
            }
        }

        public int currentSegment(double[] var1) {
            if (this.index >= this.iXG.npoints) {
                return 4;
            } else {
                var1[0] = (double) this.iXG.xpoints[this.index];
                var1[1] = (double) this.iXG.ypoints[this.index];
                var1[2] = (double) this.iXG.upoints[this.index];
                var1[3] = (double) this.iXG.vpoints[this.index];
                if (this.transform != null) {
                    this.transform.transform(var1, 0, var1, 0, 1);
                }

                return this.index == 0 ? 0 : 1;
            }
        }
    }
}
