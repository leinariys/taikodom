package taikodom.render.graphics2d;

import all.Ko;
import all.Pq;
import taikodom.render.DrawContext;
import taikodom.render.data.ExpandableIndexData;
import taikodom.render.data.ExpandableVertexData;
import taikodom.render.data.VertexLayout;
import taikodom.render.textures.RenderTarget;

import javax.media.opengl.GL;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Float;
import java.awt.image.Raster;
import java.util.ArrayList;

public class RGuiScene {
    private ArrayList callList;
    private DrawContext dc;
    private ExpandableVertexData vertexes;
    private ExpandableIndexData indexes;
    private int lastIdx;
    private int lastVtx;
    private Rectangle lastClip = new Rectangle(-1073741824, -1073741824, Integer.MAX_VALUE, Integer.MAX_VALUE);
    private Rectangle lastClipGet = new Rectangle(0, 0, 0, 0);
    private AffineTransform lastTransform = new AffineTransform();
    private Color currentColor;
    private boolean colorArrayEnabled;
    private int blendFuncSfactor;
    private int blendFuncDfactor;
    private PaintContext paintContext;
    private float[] fArray;
    private Point2D p1;
    private Point2D p2;
    private boolean transparentPaintContext;
    private float lineWidth;
    private BlurFX blur;
    private RenderTarget renderTarget;

    public RGuiScene(RGraphicsContext var1) {
        this.currentColor = Color.WHITE;
        this.fArray = new float[16];
        this.p1 = new Float(0.0F, 0.0F);
        this.p2 = new Float(0.0F, 0.0F);
        this.lineWidth = -1.0F;
        this.callList = new ArrayList();
        this.indexes = new ExpandableIndexData(128);
        this.dc = var1.dc();
        VertexLayout var2 = new VertexLayout(true, true, false, false, 1);
        this.setVertexes(new ExpandableVertexData(var2, 50));
    }

    public void reset() {
        this.lastVtx = 0;
        this.lastIdx = 0;
        this.getVertexes().data().clear();
        this.getIndexes().buffer().clear();
        this.callList.clear();
        this.currentColor = Color.WHITE;
        this.blendFuncSfactor = 1;
        this.blendFuncDfactor = 771;
        this.colorArrayEnabled = false;
    }

    void add(DrawNode var1) {
        this.callList.add(var1);
    }

    protected void doRender() {
        ArrayList var1 = this.callList;
        int var2 = 0;

        for (int var3 = var1.size(); var2 < var3; ++var2) {
            DrawNode var4 = (DrawNode) var1.get(var2);
            var4.draw(this.dc, this);
        }

    }

    public int currentIndex() {
        return this.lastIdx;
    }

    public void addIndex(int var1) {
    }

    //не дожно быть null
    public void draw() {
        this.draw((RenderTarget) null);
    }

    public void draw(RenderTarget var1) {
        this.renderTarget = var1;
        /**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
         * седержит методы для рисования примитивов (линии треугольники)*/
        GL var2 = this.dc.getGL();
        var2.glPushAttrib(24576);

        try {
            var2.glEnable(3042);
            var2.glBlendFunc(1, 771);
            this.blendFuncSfactor = 1;
            this.blendFuncDfactor = 771;
            var2.glEnableClientState(32884);
            var2.glEnableClientState(32888);
            var2.glEnableClientState(32886);
            this.colorArrayEnabled = false;
            var2.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            this.currentColor = Color.WHITE;
            var2.glBindTexture(3553, 0);
            var2.glDisable(3553);
            var2.glEnable(3089);
            this.getVertexes().data().position(this.getVertexes().layout().positionOffset());
            var2.glVertexPointer(3, 5126, this.getVertexes().layout().stride(), this.getVertexes().data());
            this.getVertexes().data().position(this.getVertexes().getLayout().colorOffset());
            var2.glColorPointer(4, 5126, this.getVertexes().layout().stride(), this.getVertexes().data());
            this.getVertexes().data().position(this.getVertexes().layout().texCoordOffset());
            var2.glTexCoordPointer(2, 5126, this.getVertexes().layout().stride(), this.getVertexes().data());
            var2.glDisableClientState(32886);
            this.doRender();
            var2.glDisableClientState(32886);
            var2.glDisable(3089);
            var2.glDisableClientState(32884);
            var2.glDisableClientState(32888);
        } finally {
            var2.glPopAttrib();
        }
    }

    public void setClip(RGraphics2 var1) {
        var1.untransformedClipBounds(this.lastClipGet);
        if (!this.lastClip.equals(this.lastClipGet)) {
            this.lastClip.setBounds(this.lastClipGet);
            this.callList.add(new Ko(this.lastClip));
        }
    }

    public void addToList(Color var1, int var2, int var3) {
        if (var1 != null && var1.getAlpha() != 0) {
            PolyDraw var4 = new PolyDraw();
            var4.count = var3;
            var4.bufferPosition = var2;
            this.add(var4);
        }
    }

    public ExpandableVertexData getVertexes() {
        return this.vertexes;
    }

    public void setVertexes(ExpandableVertexData var1) {
        this.vertexes = var1;
    }

    public ExpandableIndexData getIndexes() {
        return this.indexes;
    }

    public int getLastVtx() {
        return this.lastVtx;
    }

    public int getLastIdx() {
        return this.lastIdx;
    }

    public void setBlendFunc(int var1, int var2) {
        if (var1 != this.blendFuncSfactor || var2 != this.blendFuncDfactor) {
            this.add(new BlendFuncNode(var1, var2));
            this.blendFuncSfactor = var1;
            this.blendFuncDfactor = var2;
        }
    }

    public void setEnableColorArray(final boolean var1) {
        if (this.colorArrayEnabled != var1) {
            this.colorArrayEnabled = var1;
            this.add(new DrawNode() {
                public void draw(DrawContext var1x, RGuiScene var2) {
                    if (var1) {
                        var1x.getGL().glEnableClientState(32886);
                    } else {
                        var1x.getGL().glDisableClientState(32886);
                    }
                }
            });
        }
    }

    public void setColor(Color var1, Color var2) {
        if (var1 == null) {
            var1 = Color.WHITE;
        }

        if (Color.WHITE.equals(var2)) {
            if (!this.currentColor.equals(var1)) {
                this.currentColor = var1;
                this.callList.add(new Pq(var1));
            }
        } else {
            int var3 = var1.getAlpha() * var2.getAlpha() / 255;
            int var4 = var1.getRed() * var2.getRed() / 255;
            int var5 = var1.getGreen() * var2.getGreen() / 255;
            int var6 = var1.getBlue() * var2.getBlue() / 255;
            if (this.currentColor.getAlpha() != var3 || this.currentColor.getRed() != var4 || this.currentColor.getGreen() != var5 || this.currentColor.getBlue() != var6) {
                this.currentColor = new Color(var4, var5, var6, var3);
                this.callList.add(new Pq(this.currentColor));
            }
        }

    }

    public void addVertex(float var1, float var2, float var3) {
        this.vertexes.setPosition(this.lastVtx, var1, var2, var3);
        if (this.paintContext != null) {
            this.p1.setLocation((double) var1, (double) var2);
            this.lastTransform.transform(this.p1, this.p1);
            int var4 = (int) this.p1.getX();
            int var5 = (int) this.p1.getY();
            Raster var6 = this.paintContext.getRaster(var4, var5, 1, 1);
            var6.getPixel(0, 0, this.fArray);
            if (this.transparentPaintContext) {
                this.vertexes.setColor(this.lastVtx, this.fArray[0] / 255.0F, this.fArray[1] / 255.0F, this.fArray[2] / 255.0F, this.fArray[3] / 255.0F);
            } else {
                this.vertexes.setColor(this.lastVtx, this.fArray[0] / 255.0F, this.fArray[1] / 255.0F, this.fArray[2], 1.0F);
            }
        } else {
            this.vertexes.setColor(this.lastVtx, (float) this.currentColor.getRed() / 255.0F, (float) this.currentColor.getGreen() / 255.0F, (float) this.currentColor.getBlue() / 255.0F, (float) this.currentColor.getAlpha() / 255.0F);
        }

        this.indexes.setIndex(this.lastIdx, this.lastVtx);
        ++this.lastVtx;
        ++this.lastIdx;
    }

    public void setPaintContext(PaintContext var1, boolean var2) {
        this.paintContext = var1;
        this.transparentPaintContext = var2;
    }

    public void setCurrentTexCoord(int var1, float var2, float var3) {
        this.vertexes.setTexCoord(this.lastVtx - 1, var1, var2, var3);
    }

    public void setLineWidth(final float var1) {
        if (this.lineWidth != var1) {
            this.lineWidth = var1;
            this.add(new DrawNode() {
                public void draw(DrawContext var1x, RGuiScene var2) {
                    var1x.getGL().glLineWidth(var1);
                }
            });
        }

    }

    public void addBlur(int var1, int var2, int var3, int var4) {
        this.setColor(Color.WHITE, Color.WHITE);
        this.setEnableColorArray(false);
        final Rectangle var5 = new Rectangle(this.lastClip);
        SwingUtilities.computeIntersection(var1, var2, var3, var4, var5);
        this.add(new DrawNode() {
            public void draw(DrawContext var1, RGuiScene var2) {
                if (RGuiScene.this.blur == null) {
                    RGuiScene.this.blur = new BlurFX();
                }
                var1.getGL();
                RGuiScene.this.blur.blur(var2.renderTarget, var1, var5.x, var5.y, var5.width, var5.height);
            }
        });
    }
}
