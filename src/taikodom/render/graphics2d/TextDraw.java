package taikodom.render.graphics2d;

import taikodom.render.DrawContext;
import taikodom.render.textures.Font;

import javax.media.opengl.GL;
import java.awt.*;

public class TextDraw extends DrawNode {
    private Font font;
    private String text;
    private float x;
    private float y;

    public TextDraw(Font var1, Color var2, String var3, float var4, float var5) {
        this.font = var1;
        this.text = var3;
        this.x = var4;
        this.y = var5;
    }

    public void draw(DrawContext var1, RGuiScene var2) {
        /**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
         * седержит методы для рисования примитивов (линии треугольники)*/
        GL var3 = var1.getGL();
        this.font.drawString(var1, this.text, this.x, this.y);
        var3.glDisable(3553);
    }
}
