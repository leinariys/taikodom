package taikodom.render.graphics2d;

import all.Hm;
import taikodom.render.DrawContext;
import taikodom.render.gl.GLSupportedCaps;
import taikodom.render.shaders.Shader;
import taikodom.render.shaders.ShaderPass;
import taikodom.render.shaders.ShaderProgram;
import taikodom.render.shaders.ShaderProgramObject;
import taikodom.render.textures.ChannelInfo;
import taikodom.render.textures.RenderTarget;

import javax.media.opengl.GL;
import javax.swing.*;
import java.awt.*;

public class BlurFX {
    protected float glowIntensity = 1.0F;
    private boolean initialized = false;
    private boolean isSupported = false;
    private Shader defaultShader;
    private ShaderProgram verProgram;
    private ShaderProgram horProgram;
    private int nFilterPasses = 2;

    public boolean init(DrawContext var1) {
        if (!GLSupportedCaps.isGlsl()) {
            this.initialized = true;
            return false;
        } else {
            this.initialized = true;
            this.defaultShader = new Shader();
            this.defaultShader.setName("Glow shader");
            ShaderPass var2 = new ShaderPass();
            var2.setName("Glow shader pass");
            ChannelInfo var3 = new ChannelInfo();
            var3.setTexChannel(10);
            var2.setChannelTextureSet(0, var3);
            this.defaultShader.addPass(var2);
            this.createShaderPrograms(var1);
            return true;
        }
    }

    private void createShaderPrograms(DrawContext var1) {
        String var2 = "void main(void){gl_Position = ftransform();gl_TexCoord[0] = gl_MultiTexCoord0;}";
        ShaderProgramObject var3 = new ShaderProgramObject(35633, var2);
        String var4 = Hm.c(this.getClass().getResourceAsStream("blur-vertical.frag"), "utf-8");
        String var5 = Hm.c(this.getClass().getResourceAsStream("blur-horizontal.frag"), "utf-8");
        ShaderProgramObject var6 = new ShaderProgramObject(35632, var4);
        ShaderProgramObject var7 = new ShaderProgramObject(35632, var5);
        this.verProgram = new ShaderProgram();
        this.verProgram.addProgramObject(var3);
        this.verProgram.addProgramObject(var6);
        this.horProgram = new ShaderProgram();
        this.horProgram.addProgramObject(var3);
        this.horProgram.addProgramObject(var7);
        if (!this.verProgram.compile((ShaderPass) null, var1)) {
            this.verProgram = null;
            this.horProgram = null;
        } else if (!this.horProgram.compile((ShaderPass) null, var1)) {
            this.verProgram = null;
            this.horProgram = null;
        } else {
            this.isSupported = true;
        }
    }

    public void blur(RenderTarget var1, DrawContext var2, int var3, int var4, int var5, int var6) {
        boolean var7 = true;
        if (this.initialized || this.init(var2)) {
            if (!this.isSupported || var2.getMaxShaderQuality() > 0) {
                var7 = false;
            }

            if (var1 != null && var2.getMaxShaderQuality() <= 1) {/**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
             * седержит методы для рисования примитивов (линии треугольники)*/
                GL var8 = var2.getGL();
                var8.glEnable(3553);
                var1.bindTexture(var2);
                Rectangle var9 = new Rectangle(var3, var4, var5, var6);
                SwingUtilities.computeIntersection(0, 0, var1.getWidth(), var1.getHeight(), var9);
                var3 = var9.x;
                var4 = var9.y;
                var5 = var9.width;
                var6 = var9.height;
                int var10 = var1.getHeight() - var4 - var6;
                var8.glDisable(3042);
                int var11;
                if (var7) {
                    for (var11 = 0; var11 < this.nFilterPasses; ++var11) {
                        var1.copyBufferAreaToTexture(var2, var3, var10, var3, var10, var5, var6);
                        this.verProgram.bind(var2);
                        this.verProgram.updateMaterialProgramAttribs(var2);
                        this.drawQuad(var1, var2, var3, var4, var5, var6);
                        this.verProgram.unbind(var2);
                        var1.copyBufferAreaToTexture(var2, var3, var10, var3, var10, var5, var6);
                        this.horProgram.bind(var2);
                        this.horProgram.updateMaterialProgramAttribs(var2);
                        this.drawQuad(var1, var2, var3, var4, var5, var6);
                        this.horProgram.unbind(var2);
                    }
                } else {
                    var8.glPushAttrib(524288);
                    var11 = var1.getWidth() / 512;
                    var1.copyBufferAreaToTexture(var2, var3, var10, var3, var10, var5, var6);
                    var1.bindTexture(var2);
                    this.drawShrinked(var1, var2, var3, var4, var5, var6, var11);
                    var1.copyBufferAreaToTexture(var2, var3, var10, var3, var10, var5, var6);
                    var1.bindTexture(var2);
                    this.drawQuadStretchedArea(var1, var2, var3, var4, var5, var6, (float) var11, 512);
                    var11 = var1.getWidth() / 256;
                    var1.copyBufferAreaToTexture(var2, var3, var10, var3, var10, var5, var6);
                    var1.bindTexture(var2);
                    this.drawShrinked(var1, var2, var3, var4, var5, var6, var11);
                    var1.copyBufferAreaToTexture(var2, var3, var10, var3, var10, var5, var6);
                    var1.bindTexture(var2);
                    this.drawQuadStretchedArea(var1, var2, var3, var4, var5, var6, (float) var11, 512);
                    var8.glPopAttrib();
                }

                var8.glEnable(3042);
                var8.glBlendFunc(1, 771);
            }
        }
    }

    private void drawQuadStretchedArea(RenderTarget var1, DrawContext var2, int var3, int var4, int var5, int var6, float var7, int var8) {/**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
     * седержит методы для рисования примитивов (линии треугольники)*/
        GL var9 = var2.getGL();
        var9.glEnable(3089);
        var9.glScissor(var3, var1.getHeight() - var4 - var6, var5, var6);
        if ((float) var3 % var7 != 0.0F) {
            var3 = (int) ((float) var3 + (var7 - (float) var3 % var7));
        }

        if ((float) var4 % var7 != 0.0F) {
            var4 = (int) ((float) var4 + (var7 - (float) var4 % var7));
        }

        if ((float) var5 % var7 != 0.0F) {
            var5 = (int) ((float) var5 + (var7 - (float) var5 % var7));
        }

        if ((float) var6 % var7 != 0.0F) {
            var6 = (int) ((float) var6 + (var7 - (float) var6 % var7));
        }

        int var10 = var1.getTexture().getSizeX();
        int var11 = var1.getTexture().getSizeY();
        int var12 = var1.getHeight();
        float var13 = (float) var3 / (float) var10;
        float var14 = (float) (var12 - var4) / (float) var11;
        float var15 = ((float) var3 + (float) var5 / var7) / (float) var10;
        float var16 = ((float) (var12 - var4) - (float) var6 / var7) / (float) var11;
        var9.glBegin(GL.GL_QUADS);//Этот метод запускает процесс рисования. Рассматривает каждую группу из четырех вершин как независимый четырехугольник.
        var9.glTexCoord2f(var13, var14);
        var9.glVertex2f((float) var3, (float) var4);
        var9.glTexCoord2f(var15, var14);
        var9.glVertex2f((float) (var3 + var5), (float) var4);
        var9.glTexCoord2f(var15, var16);
        var9.glVertex2f((float) (var3 + var5), (float) (var4 + var6));
        var9.glTexCoord2f(var13, var16);
        var9.glVertex2f((float) var3, (float) (var4 + var6));
        var9.glEnd();
        var9.glDisable(3089);
    }

    private void drawShrinked(RenderTarget var1, DrawContext var2, int var3, int var4, int var5, int var6, int var7) {/**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
     * седержит методы для рисования примитивов (линии треугольники)*/
        GL var8 = var2.getGL();
        if (var3 % var7 != 0) {
            var3 += var7 - var3 % var7;
        }

        if (var4 % var7 != 0) {
            var4 += var7 - var4 % var7;
        }

        if (var5 % var7 != 0) {
            var5 += var7 - var5 % var7;
        }

        if (var6 % var7 != 0) {
            var6 += var7 - var6 % var7;
        }

        int var9 = var1.getTexture().getSizeX();
        int var10 = var1.getTexture().getSizeY();
        int var11 = var1.getHeight();
        var8.glEnable(3089);
        var8.glScissor(var3, var11 - var4 - var6, var5, var6);
        float var12 = (float) var3 / (float) var9;
        float var13 = (float) (var11 - var4) / (float) var10;
        float var14 = (float) (var3 + var5) / (float) var9;
        float var15 = (float) (var11 - var4 - var6) / (float) var10;
        int var16 = var5 / var7;
        int var17 = var6 / var7;
        var8.glBegin(GL.GL_QUADS);//Этот метод запускает процесс рисования. Рассматривает каждую группу из четырех вершин как независимый четырехугольник.
        var8.glTexCoord2f(var12, var13);
        var8.glVertex2f((float) var3, (float) var4);
        var8.glTexCoord2f(var14, var13);
        var8.glVertex2f((float) (var3 + var16), (float) var4);
        var8.glTexCoord2f(var14, var15);
        var8.glVertex2f((float) (var3 + var16), (float) (var4 + var17));
        var8.glTexCoord2f(var12, var15);
        var8.glVertex2f((float) var3, (float) (var4 + var17));
        var8.glEnd();
        var8.glDisable(3089);
    }

    private void drawQuad(RenderTarget var1, DrawContext var2, int var3, int var4, int var5, int var6) {/**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
     * седержит методы для рисования примитивов (линии треугольники)*/
        GL var7 = var2.getGL();
        int var8 = var1.getTexture().getSizeX();
        int var9 = var1.getTexture().getSizeY();
        int var10 = var1.getHeight();
        float var11 = (float) var3 / (float) var8;
        float var12 = (float) (var10 - var4) / (float) var9;
        float var13 = (float) (var3 + var5 - 1) / (float) var8;
        float var14 = (float) (var10 - var4 - var6 + 1) / (float) var9;
        var7.glBegin(GL.GL_QUADS);//Этот метод запускает процесс рисования. Рассматривает каждую группу из четырех вершин как независимый четырехугольник.
        var7.glTexCoord2f(var11, var12);
        var7.glVertex2f((float) var3, (float) var4);
        var7.glTexCoord2f(var11, var14);
        var7.glVertex2f((float) var3, (float) (var4 + var6 - 1));
        var7.glTexCoord2f(var13, var14);
        var7.glVertex2f((float) (var3 + var5 - 1), (float) (var4 + var6 - 1));
        var7.glTexCoord2f(var13, var12);
        var7.glVertex2f((float) (var3 + var5 - 1), (float) var4);
        var7.glEnd();
    }

    public void releaseReferences() {
        if (this.defaultShader != null) {
            this.defaultShader.releaseReferences();
        }

        if (this.verProgram != null) {
            this.verProgram.releaseReferences();
        }

        if (this.horProgram != null) {
            this.horProgram.releaseReferences();
        }

        this.verProgram = this.horProgram = null;
        this.defaultShader = null;
    }
}
