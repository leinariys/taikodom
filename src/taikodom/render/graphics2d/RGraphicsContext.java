package taikodom.render.graphics2d;

import all.acN;
import gnu.trove.THashMap;
import gnu.trove.TObjectHashingStrategy;
import taikodom.render.DrawContext;
import taikodom.render.TexBackedImage;
import taikodom.render.enums.TexMagFilter;
import taikodom.render.enums.TexMinFilter;
import taikodom.render.textures.BaseTexture;
import taikodom.render.textures.RGraphicsDataSource;
import taikodom.render.textures.Texture;

import java.awt.*;
import java.awt.image.ImageObserver;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;

public class RGraphicsContext {
    StackList graphics = new StackList() {
        @Override
        protected Object create() {
            return new RGraphics2(RGraphicsContext.this);
        }

        @Override
        protected void copy(Object var1, Object var2) {
        }

        protected void a(RGraphics2 var1, RGraphics2 var2) {
        }

        protected RGraphics2 bHp() {
            return new RGraphics2(RGraphicsContext.this);
        }
    };
    private DrawContext dc;
    private Map textureMap;
    private Map fontMap;
    private Map quickFontMap;
    private boolean rendering;

    public RGraphicsContext(DrawContext var1) {
        this.dc = var1;
        this.textureMap = new HashMap();
        this.quickFontMap = new WeakHashMap();
        this.fontMap = new THashMap(new TObjectHashingStrategy() {
            @Override
            public int computeHashCode(Object o) {
                return a((Font) o);
            }

            @Override
            public boolean equals(Object o, Object t1) {
                return a((Font) o, (Font) t1);
            }

            public int a(Font var1) {
                if (var1 == null) {
                    return 0;
                } else {
                    Map var2 = acN.ak(var1);
                    return var1.hashCode() + (var2 != null ? var2.hashCode() + 3000 : 0);
                }
            }

            public boolean a(Font var1, Font var2) {
                if (var1.equals(var2)) {
                    Map var3 = acN.ak(var1);
                    Map var4 = acN.ak(var2);
                    if (var3 == var4) {
                        return true;
                    } else {
                        return var3 != null && var4 != null ? var3.equals(var4) : false;
                    }
                } else {
                    return false;
                }
            }
        });
    }

    public BaseTexture getTexture(Image var1) {
        if (var1 == null) {
            return null;
        } else {
            int var2 = var1.getHeight((ImageObserver) null);
            int var3 = var1.getWidth((ImageObserver) null);
            if (var2 > 0 && var3 > 0) {
                if (var1 instanceof TexBackedImage) {
                    return ((TexBackedImage) var1).getTexture();
                } else {
                    Texture var4 = (Texture) this.textureMap.get(var1);
                    if (var4 != null) {
                        return var4;
                    } else {
                        var4 = new Texture();
                        var4.setMagFilter(TexMagFilter.LINEAR);
                        var4.setMinFilter(TexMinFilter.LINEAR);
                        RGraphicsDataSource var5 = new RGraphicsDataSource();
                        var5.setImage(var1);
                        var4.setDataSource(var5);
                        if ((!Texture.isPowerOf2(var5.getWidth()) || !Texture.isPowerOf2(var5.getHeight())) && !Texture.haveNPOT()) {
                            if (Texture.haveTexRect()) {
                                var4.setTarget(34037);
                            }
                        } else {
                            var4.setTarget(3553);
                        }

                        this.textureMap.put(var1, var4);
                        var4.setOnlyUpdateNeeded(true);
                        return var4;
                    }
                }
            } else {
                return null;
            }
        }
    }

    public taikodom.render.textures.Font getFont(Font var1) {
        if (var1 == null) {
            return null;
        } else {
            taikodom.render.textures.Font var2 = (taikodom.render.textures.Font) this.quickFontMap.get(var1);
            if (var2 == null) {
                var2 = (taikodom.render.textures.Font) this.fontMap.get(var1);
                if (var2 == null) {
                    var2 = new taikodom.render.textures.Font();
                    var2.setSize((float) var1.getSize());
                    var2.setDc(this.dc);
                    var2.setAwtFont(var1);
                    this.fontMap.put(var1, var2);
                }

                this.quickFontMap.put(var1, var2);
            }

            return var2;
        }
    }

    public DrawContext dc() {
        return this.dc;
    }

    public FontMetrics getFontMetrics(Font var1) {
        return this.getFont(var1).getFontMetrics();
    }

    public RGraphics2 createGraphics() {
        return this.rendering ? (RGraphics2) this.graphics.get() : new RGraphics2(this);
    }

    public void startRendering() {
        this.rendering = true;
        this.graphics.push();
    }

    public void stopRendering() {
        this.rendering = false;
        this.graphics.pop();
    }
}
