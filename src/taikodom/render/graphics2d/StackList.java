package taikodom.render.graphics2d;

import java.util.ArrayList;

public abstract class StackList {
    private final ArrayList list = new ArrayList();
    private int[] stack = new int[512];
    private int stackCount = 0;
    private int pos = 0;

    public final void push() {
        this.stack[this.stackCount++] = this.pos;
    }

    public final void pop() {
        this.pos = this.stack[--this.stackCount];
    }

    public Object get() {
        if (this.pos == this.list.size()) {
            this.expand();
        }

        return this.list.get(this.pos++);
    }

    protected abstract Object create();

    protected abstract void copy(Object var1, Object var2);

    private void expand() {
        this.list.add(this.create());
    }
}
