package taikodom.render.graphics2d;

import taikodom.render.DrawContext;
import taikodom.render.textures.BaseTexture;

import javax.media.opengl.GL;

public class ImageDraw extends DrawNode {
    private int bufferPosition;
    private int count;
    private BaseTexture texture;

    public ImageDraw(BaseTexture var1, int var2, int var3) {
        this.bufferPosition = var2;
        this.count = var3;
        this.texture = var1;
    }

    public void draw(DrawContext var1, RGuiScene var2) {
        this.texture.bind(var1);
        /**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
         * седержит методы для рисования примитивов (линии треугольники)*/
        GL var3 = var1.getGL();
        this.drawElements(var3, var2.getIndexes(), 7, this.bufferPosition, this.count);
        var3.glDisable(this.texture.getTarget());
    }
}
