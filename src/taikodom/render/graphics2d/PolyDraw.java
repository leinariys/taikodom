package taikodom.render.graphics2d;

import taikodom.render.DrawContext;

public class PolyDraw extends DrawNode {
    public int bufferPosition;
    public int count;

    public void draw(DrawContext var1, RGuiScene var2) {
        /**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
         * седержит методы для рисования примитивов (линии треугольники)*/
        this.drawElements(var1.getGL(), var2.getIndexes(), 4, this.bufferPosition, this.count);
    }
}
