package taikodom.render.gui;

import gnu.trove.THashSet;
import taikodom.render.GuiContext;
import taikodom.render.StepContext;

import java.util.Iterator;
import java.util.Set;

/**
 * Интерфейс сцены
 */
public class GuiScene {
    /**
     * Список групп линий для отрисовки GBillboard
     */
    private Set objects = new THashSet();

    /**
     *
     */
    private String name = null;

    public GuiScene(String guiScene) {
        this.name = guiScene;
    }

    /**
     * Добавить группу линий в список для отрисовки
     *
     * @param var1
     */
    public void addChild(GuiSceneObject var1) {
        if (var1 == null) {
            throw new NullPointerException("Cannot add all null SceneObject");
        } else {
            synchronized (this.objects) {
                this.objects.add(var1);
            }
        }
    }

    /**
     *
     */
    public void removeAllChildren() {
        synchronized (this.objects) {
            this.objects.clear();
        }
    }

    /**
     * @param var1
     */
    public void removeChild(GuiSceneObject var1) {
        synchronized (this.objects) {
            this.objects.remove(var1);
        }
    }

    /**
     * @param var1
     */
    public void render(GuiContext var1) {
        synchronized (this.objects) {
            Iterator var4 = this.objects.iterator();
            while (var4.hasNext()) {
                GuiSceneObject var3 = (GuiSceneObject) var4.next();
                var3.render(var1);
            }
        }
    }

    public void step(StepContext var1) {
        var1.incObjectSteps((long) this.objects.size());
        synchronized (this.objects) {
            Iterator var4 = this.objects.iterator();
            while (var4.hasNext()) {
                GuiSceneObject var3 = (GuiSceneObject) var4.next();
                var3.step(var1);
            }
        }
    }

    public Set getChildren() {
        return this.objects;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String var1) {
        this.name = var1;
    }

    public String toString() {
        return "Scene: " + this.name;
    }

    public void addStaticChild(GuiSceneObject var1) {
        if (var1 == null) {
            throw new NullPointerException("Cannot add all null SceneObject");
        } else {
            synchronized (this.objects) {
                this.objects.add(var1);
            }
        }
    }
}
