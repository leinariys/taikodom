package taikodom.render.gui;

import all.Vector3dOperations;
import all.aLH;
import all.aQKa;
import all.te_w;
import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import taikodom.render.GuiContext;
import taikodom.render.SceneView;
import taikodom.render.StepContext;
import taikodom.render.camera.Camera;
import taikodom.render.loader.RenderAsset;
import taikodom.render.loader.SceneLoader;
import taikodom.render.primitives.Billboard;
import taikodom.render.scene.SceneObject;
import taikodom.render.textures.Material;

import javax.vecmath.Tuple3d;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Реализация класса указанного в имени тегов из файлов с расширением .pro
 */
public class GHudMark extends GuiSceneObject {
    private static Billboard billBoard = new Billboard();
    private final Vector3dOperations tempVec = new Vector3dOperations();
    private final Vec3f tempVec2 = new Vec3f();
    private final Vec3f screenPosition = new Vec3f();
    private SceneObject referenceObject;
    private ArrayList marks = new ArrayList(4);
    private int maxLockSize;
    private int minLockSize;
    private float areaRadius;
    private Color centerColor;
    private Color outColor;
    private Color satellitesColor;
    private Color selectedColor;
    private float maxVisibleDistance;
    private boolean showWhenOffscreen = true;
    private boolean isOffScreen;
    private boolean showSatellites;
    private float anim = 0.0F;
    private aQKa temp = new aQKa();
    private Material mark;
    private Material markSelected;
    private Material markOut;
    private Material lock;
    private boolean shouldTrack;
    private float screenSize;
    private int markImgWidth;
    private int markImgHeight;
    private int lockImgWidth;
    private int lockImgHeight;
    private aQKa offSet = new aQKa(0.0F, 0.0F);
    private int lastPx = Integer.MAX_VALUE;
    private int lastPy = Integer.MAX_VALUE;
    private boolean smooth = false;

    public GHudMark() {
        this.marks.add(this.temp);
        this.temp = new aQKa();
        this.marks.add(this.temp);
        this.temp = new aQKa();
        this.marks.add(this.temp);
        this.temp = new aQKa();
        this.marks.add(this.temp);
        this.temp = new aQKa();
    }

    public GHudMark(GHudMark var1) {
        super(var1);
        this.marks.add(this.temp);
        this.temp = new aQKa();
        this.marks.add(this.temp);
        this.temp = new aQKa();
        this.marks.add(this.temp);
        this.temp = new aQKa();
        this.marks.add(this.temp);
        this.temp = new aQKa();
        this.setMark(var1.mark);
        this.markSelected = var1.markSelected;
        this.markOut = var1.markOut;
        this.setLock(var1.lock);
        this.centerColor = var1.centerColor;
        this.outColor = var1.outColor;
        this.satellitesColor = var1.satellitesColor;
        this.selectedColor = var1.selectedColor;
        this.maxLockSize = var1.maxLockSize;
        this.minLockSize = var1.minLockSize;
        this.areaRadius = var1.areaRadius;
    }

    float getScreenSize(float var1) {
        Camera var2 = this.sceneView.getCamera();
        SceneObject var3 = this.referenceObject;
        Vector3dOperations var4 = new Vector3dOperations();
        var3.getGlobalTransform().getTranslation(var4);
        aLH var5 = var3.getAabbWorldSpace();
        float var6 = (float) var5.din().f((Tuple3d) var5.dim()).length();
        float var7 = (float) var2.getPosition().f((Tuple3d) var4).length();
        float var8 = var7 * 2.0F * var2.getTanHalfFovY();
        float var9 = var1 / var8;
        float var10 = var6 * var9;
        return var10;
    }

    public int step(StepContext var1) {
        if (this.referenceObject == null) {
            return 0;
        } else {
            double var2 = this.referenceObject.getPosition().ax(this.sceneView.getCamera().getPosition());
            float var4 = (float) (this.sceneView.getViewport().width / 2);
            float var5 = (float) (this.sceneView.getViewport().height / 2);
            if (var2 > (double) this.maxVisibleDistance && this.maxVisibleDistance > 0.0F) {
                this.anim -= var1.getDeltaTime() * 0.001F;
                this.shouldTrack = false;
            } else {
                this.anim += var1.getDeltaTime() * 0.001F;
                this.shouldTrack = true;
            }

            if (this.anim > 1.0F) {
                this.anim = 1.0F;
            }

            if (this.anim < 0.0F) {
                this.anim = 0.0F;
            }

            if (!this.shouldTrack && this.showWhenOffscreen) {
                if (this.anim == 0.0F) {
                    int var12 = Math.round(this.screenPosition.x);
                    int var13 = Math.round(this.screenPosition.y);
                    this.setPosition(var12, var13);
                    return 0;
                }
            } else {
                if (this.sceneView == null) {
                    return 0;
                }

                this.sceneView.getCamera().getTransform().getZ(this.tempVec2);
                this.tempVec.aA(this.sceneView.getCamera().getPosition());
                this.tempVec.g(this.referenceObject.getPosition());
                this.tempVec.normalize();
                double var6 = this.tempVec.b((te_w) this.tempVec2);
                this.sceneView.getWindowCoords(this.referenceObject.getPosition(), this.tempVec);
                this.screenPosition.set(this.tempVec);
                this.screenPosition.E(this.offSet.x, this.offSet.y, 0.0F);
                if (this.tempVec.x >= (double) this.sceneView.getViewport().x && this.tempVec.x <= (double) this.sceneView.getViewport().width && this.tempVec.y >= (double) this.sceneView.getViewport().y && this.tempVec.y <= (double) this.sceneView.getViewport().height && var6 >= 0.0D) {
                    this.isOffScreen = false;
                } else {
                    this.isOffScreen = true;
                    if (!this.showWhenOffscreen) {
                        return 0;
                    }

                    this.tempVec.o((double) var4, (double) var5, 0.0D);
                    this.tempVec.normalize();
                    if (var6 < 0.0D) {
                        this.tempVec.y = -this.tempVec.y;
                        this.tempVec.x = -this.tempVec.x;
                    }

                    this.tempVec.scale((double) this.areaRadius);
                    this.tempVec.z((double) var4, (double) var5, 1.0D);
                    this.screenPosition.set(this.tempVec);
                    this.screenPosition.E(this.offSet.x, this.offSet.y, 0.0F);
                }
            }

            this.screenSize = (float) Math.max(this.markImgWidth, this.markImgHeight);
            float var11 = this.screenSize / 2.0F + (1.0F - this.anim) * 40.0F;
            float var7 = this.anim * 360.0F;
            float var8 = 0.017453292F;
            this.temp.x = (float) (Math.sin((double) ((45.0F + var7) * var8)) * (double) var11);
            this.temp.y = (float) (Math.cos((double) ((45.0F + var7) * var8)) * (double) var11);
            this.temp.add(this.tempVec.x, this.tempVec.y);
            ((aQKa) this.marks.get(0)).set(this.temp);
            this.temp.x = (float) (Math.sin((double) ((135.0F + var7) * var8)) * (double) var11);
            this.temp.y = (float) (Math.cos((double) ((135.0F + var7) * var8)) * (double) var11);
            this.temp.add(this.tempVec.x, this.tempVec.y);
            ((aQKa) this.marks.get(1)).set(this.temp);
            this.temp.x = (float) (Math.sin((double) ((225.0F + var7) * var8)) * (double) var11);
            this.temp.y = (float) (Math.cos((double) ((225.0F + var7) * var8)) * (double) var11);
            this.temp.add(this.tempVec.x, this.tempVec.y);
            ((aQKa) this.marks.get(2)).set(this.temp);
            this.temp.x = (float) (Math.sin((double) ((315.0F + var7) * var8)) * (double) var11);
            this.temp.y = (float) (Math.cos((double) ((315.0F + var7) * var8)) * (double) var11);
            this.temp.add(this.tempVec.x, this.tempVec.y);
            ((aQKa) this.marks.get(3)).set(this.temp);
            int var9 = Math.round(this.screenPosition.x);
            int var10 = Math.round(this.screenPosition.y);
            this.setPosition(var9, var10);
            return super.step(var1);
        }
    }

    public boolean isSmooth() {
        return !this.smooth;
    }

    public void setSmooth(boolean var1) {
        this.smooth = var1;
    }

    public void jump() {
        this.lastPx = Integer.MAX_VALUE;
        this.lastPy = Integer.MAX_VALUE;
    }

    protected void setPosition(int var1, int var2) {
        if (this.smooth && (this.lastPy != var2 || this.lastPx != var1)) {
            if (this.lastPx != Integer.MAX_VALUE && this.lastPy != Integer.MAX_VALUE) {
                int var3 = Math.abs(this.lastPx - var1);
                int var4 = Math.abs(this.lastPy - var2);
                int var5 = (int) Math.ceil((double) var3 / 2.0D);
                if (var3 > 2) {
                    if (this.lastPx > var1) {
                        this.lastPx -= var5;
                    } else {
                        this.lastPx += var5;
                    }
                } else {
                    this.lastPx = var1;
                }

                int var6 = (int) Math.ceil((double) var4 / 2.0D);
                if (var4 > 2) {
                    if (this.lastPy > var2) {
                        this.lastPy -= var6;
                    } else {
                        this.lastPy += var6;
                    }
                } else {
                    this.lastPy = var2;
                }

                this.position.set((float) this.lastPx, (float) this.lastPy);
            } else {
                this.lastPx = var1;
                this.lastPy = var2;
                this.position.set((float) var1, (float) var2);
            }
        } else {
            this.lastPx = var1;
            this.lastPy = var2;
            this.position.set((float) this.lastPx, (float) this.lastPy);
        }
    }

    public void render(GuiContext var1) {
        float var6 = this.anim * 360.0F;
        if (this.render && (!this.isOffScreen || this.showWhenOffscreen)) {
            if (this.mark != null) {
                if (this.mark.getDiffuseTexture() != null) {/**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
                 * седержит методы для рисования примитивов (линии треугольники)*/
                    var1.getDc().getGL();
                    float var2;
                    float var3;
                    float var4;
                    float var5;
                    if (!this.isOffScreen) {
                        var2 = this.centerColor.x;
                        var3 = this.centerColor.y;
                        var4 = this.centerColor.z;
                        var5 = this.centerColor.w;
                    } else {
                        var2 = this.outColor.x;
                        var3 = this.outColor.y;
                        var4 = this.outColor.z;
                        var5 = this.outColor.w;
                    }

                    this.markImgWidth = (int) Math.abs(this.mark.getDiffuseTexture().getTransformedWidth() * this.mark.getXScaling());
                    this.markImgHeight = (int) Math.abs(this.mark.getDiffuseTexture().getTransformedHeight() * this.mark.getYScaling());
                    this.globalTransform.setIdentity();
                    this.globalTransform.setTranslation((double) (this.position.x + 0.5F * (float) (this.markImgWidth % 2)), (double) (this.position.y + 0.5F * (float) (this.markImgHeight % 2)), 0.0D);
                    this.globalTransform.B((float) this.markImgWidth);
                    this.globalTransform.C((float) this.markImgHeight);
                    this.primitiveColor.set(var2, var3, var4, var5);
                    var1.addPrimitive(this.mark.getShader(), this.mark, billBoard, this.globalTransform, this.primitiveColor);
                    var6 = 180.0F - this.anim * 360.0F;
                    if (this.showSatellites) {
                        Iterator var8 = this.marks.iterator();

                        while (var8.hasNext()) {
                            aQKa var7 = (aQKa) var8.next();
                            this.globalTransform.setIdentity();
                            this.globalTransform.setTranslation((double) ((int) var7.x), (double) ((int) var7.y), 0.0D);
                            this.globalTransform.B((float) this.lockImgWidth);
                            this.globalTransform.C((float) this.lockImgHeight);
                            this.transform.setIdentity();
                            this.transform.A(var6);
                            this.globalTransform.e(this.transform);
                            var6 -= 90.0F;
                            var1.addPrimitive(this.lock.getShader(), this.lock, billBoard, this.globalTransform, this.satellitesColor);
                        }
                    }

                }
            }
        }
    }

    public RenderAsset cloneAsset() {
        return new GHudMark(this);
    }

    public Material getMark() {
        return this.mark;
    }

    public void setMark(Material var1) {
        this.mark = var1;
        if (var1 != null) {
            this.markImgWidth = (int) Math.abs(var1.getDiffuseTexture().getTransformedWidth() * var1.getXScaling());
            this.markImgHeight = (int) Math.abs(var1.getDiffuseTexture().getTransformedHeight() * var1.getYScaling());
        }

    }

    public void validate(SceneLoader var1) {
        this.mark.setShader(var1.getDefaultHudMarkShader());
        this.lock.setShader(var1.getDefaultHudMarkShader());
        this.markOut.setShader(var1.getDefaultHudMarkShader());
        super.validate(var1);
    }

    public int getMaxLockSize() {
        return this.maxLockSize;
    }

    public void setMaxLockSize(int var1) {
        this.maxLockSize = var1;
    }

    public int getMinLockSize() {
        return this.minLockSize;
    }

    public void setMinLockSize(int var1) {
        this.minLockSize = var1;
    }

    public float getAreaRadius() {
        return this.areaRadius;
    }

    public void setAreaRadius(float var1) {
        this.areaRadius = var1;
    }

    public Color getCenterColor() {
        return this.centerColor;
    }

    public void setCenterColor(Color var1) {
        this.centerColor = var1;
    }

    public Color getOutColor() {
        return this.outColor;
    }

    public void setOutColor(Color var1) {
        this.outColor = var1;
    }

    public Color getSatellitesColor() {
        return this.satellitesColor;
    }

    public void setSatellitesColor(Color var1) {
        this.satellitesColor = var1;
    }

    public Color getSelectedColor() {
        return this.selectedColor;
    }

    public void setSelectedColor(Color var1) {
        this.selectedColor = var1;
    }

    public boolean isShowWhenOffscreen() {
        return this.showWhenOffscreen;
    }

    public void setShowWhenOffscreen(boolean var1) {
        this.showWhenOffscreen = var1;
    }

    public float getMaxVisibleDistance() {
        return this.maxVisibleDistance;
    }

    public void setMaxVisibleDistance(float var1) {
        this.maxVisibleDistance = var1;
    }

    public Material getMarkSelected() {
        return this.markSelected;
    }

    public void setMarkSelected(Material var1) {
        this.markSelected = var1;
    }

    public Material getMarkOut() {
        return this.markOut;
    }

    public void setMarkOut(Material var1) {
        this.markOut = var1;
    }

    public Material getLock() {
        return this.lock;
    }

    public void setLock(Material var1) {
        this.lock = var1;
        if (var1 != null) {
            this.lockImgWidth = (int) Math.abs(var1.getDiffuseTexture().getTransformedWidth() * var1.getXScaling());
            this.lockImgHeight = (int) Math.abs(var1.getDiffuseTexture().getTransformedHeight() * var1.getYScaling());
        }

    }

    public boolean isOffScreen() {
        return this.isOffScreen;
    }

    public void setOffScreen(boolean var1) {
        this.isOffScreen = var1;
    }

    public boolean isShowSatellites() {
        return this.showSatellites;
    }

    public void setShowSatellites(boolean var1) {
        this.showSatellites = var1;
    }

    public void setTarget(SceneObject var1, SceneView var2) {
        this.setSceneView(var2);
        this.setReferenceObject(var1);
    }

    public SceneObject getReferenceObject() {
        return this.referenceObject;
    }

    public void setReferenceObject(SceneObject var1) {
        this.referenceObject = var1;
    }

    public Vec3f getScreenPosition() {
        return this.screenPosition;
    }

    public void setScreenPosition(Vec3f var1) {
        this.screenPosition.set(var1);
    }

    public float getMarkSize() {
        return this.screenSize;
    }

    public aQKa getCenterBillboardSize() {
        return null;
    }

    public void setCenterBillboardSize(aQKa var1) {
    }

    public void setOffset(float var1, float var2) {
        this.offSet.set(var1, var2);
    }

    public aQKa getOffset() {
        return this.offSet;
    }
}
