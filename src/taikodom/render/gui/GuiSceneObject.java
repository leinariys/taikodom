package taikodom.render.gui;

import all.aQKa;
import all.bc_q;
import com.hoplon.geometry.Color;
import taikodom.render.GuiContext;
import taikodom.render.SceneView;
import taikodom.render.StepContext;
import taikodom.render.loader.RenderAsset;

/**
 *
 */
public abstract class GuiSceneObject extends RenderAsset {
    /**
     * Позиция на экране
     */
    protected aQKa position = new aQKa();
    protected Color primitiveColor = new Color(1.0F, 1.0F, 1.0F, 1.0F);
    protected bc_q globalTransform;
    protected bc_q transform;
    protected SceneView sceneView;
    protected boolean render = true;
    protected boolean disposed = false;

    public GuiSceneObject() {
        this.globalTransform = new bc_q();
        this.transform = new bc_q();
    }

    public GuiSceneObject(GuiSceneObject var1) {
        this.position.set(var1.position);
        this.globalTransform = new bc_q();
        this.globalTransform.b(var1.globalTransform);
        this.transform = new bc_q();
        this.transform.b(var1.globalTransform);
    }

    public abstract void render(GuiContext var1);

    public int step(StepContext var1) {
        return 0;
    }

    public Color getPrimitiveColor() {
        return this.primitiveColor;
    }

    public void setPrimitiveColor(Color var1) {
        this.primitiveColor.set(var1);
    }

    public void setPrimitiveColor(float var1, float var2, float var3, float var4) {
        this.primitiveColor.set(var1, var2, var3, var4);
    }

    public boolean isRender() {
        return this.render;
    }

    public void setRender(boolean var1) {
        this.render = var1;
    }

    public boolean isDisposed() {
        return this.disposed;
    }

    public void dispose() {
        this.disposed = true;
    }

    public aQKa getPosition() {
        return this.position;
    }

    public void setPosition(aQKa var1) {
        this.position.set(var1);
    }

    public SceneView getSceneView() {
        return this.sceneView;
    }

    public void setSceneView(SceneView var1) {
        this.sceneView = var1;
    }
}
