package taikodom.render.gui;

import all.aQKa;
import taikodom.render.GuiContext;
import taikodom.render.loader.RenderAsset;
import taikodom.render.loader.SceneLoader;
import taikodom.render.primitives.Billboard;
import taikodom.render.shaders.Shader;

/**
 *
 */
public class GBillboard extends GuiRenderObject {
    /**
     *
     */
    private static Billboard billboard = new Billboard();
    /**
     *
     */
    private aQKa size;

    /**
     *
     */
    public GBillboard() {
        this.size = new aQKa();
    }

    /**
     * @param var1
     */
    public GBillboard(GBillboard var1) {
        this.size = new aQKa(var1.size);
    }

    /**
     * @param var1
     */
    public void render(GuiContext var1) {
        if (this.render) {
            this.transform.setIdentity();
            this.transform.setTranslation((double) this.position.x, (double) this.position.y, 0.0D);
            this.transform.B(this.size.x);
            this.transform.C(this.size.y);
            var1.addPrimitive(this.material.getShader(), this.material, billboard, this.transform, this.primitiveColor);
        }
    }

    public RenderAsset cloneAsset() {
        return new GBillboard(this);
    }

    public void setSize(aQKa var1) {
        this.size = var1;
    }

    public void validate(SceneLoader var1) {
        this.material.setShader(var1.getDefaultBillboardShader());
        super.validate(var1);
    }

    public void setShader(Shader var1) {
        this.material.setShader(var1);
    }
}
