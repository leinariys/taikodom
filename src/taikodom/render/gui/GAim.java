package taikodom.render.gui;

import all.Vector3dOperations;
import com.hoplon.geometry.Vec3f;
import taikodom.render.GuiContext;
import taikodom.render.SceneView;
import taikodom.render.StepContext;
import taikodom.render.loader.RenderAsset;
import taikodom.render.loader.SceneLoader;
import taikodom.render.primitives.Billboard;
import taikodom.render.scene.SceneObject;
import taikodom.render.textures.Material;

/**
 * Реализация класса указанного в имени тегов из файлов с расширением .pro
 */
public class GAim extends GuiRenderObject {
    private static Billboard billBoard = new Billboard();
    private SceneObject shipObject;
    private SceneObject targetObject;
    private SceneView shipScene;
    private Vector3dOperations tempVect;
    private Vector3dOperations screenPos;
    private Vec3f shipZ;
    private Vec3f zTransform;
    private float projectionDistance = 1.0E12F;
    private int imageHeight;
    private int imageWidth;
    private boolean onScreen = false;

    public GAim() {
        this.screenPos = new Vector3dOperations();
        this.tempVect = new Vector3dOperations();
        this.shipZ = new Vec3f();
        this.zTransform = new Vec3f(0.0F, 0.0F, 1.0F);
    }

    public GAim(GAim var1) {
        super(var1);
        this.material = new Material(var1.material);
        this.screenPos = new Vector3dOperations();
        this.tempVect = new Vector3dOperations();
        this.shipZ = new Vec3f();
        this.zTransform = new Vec3f(0.0F, 0.0F, 1.0F);
    }

    public void setShip(SceneObject var1, SceneView var2) {
        this.shipObject = var1;
        this.shipScene = var2;
    }

    public SceneObject getShip() {
        return this.shipObject;
    }

    public SceneObject getTarget() {
        return this.targetObject;
    }

    public void setTarget(SceneObject var1) {
        this.targetObject = var1;
    }

    public void setProjectionDistance(float var1) {
        this.projectionDistance = var1;
    }

    public int step(StepContext var1) {
        if (this.shipObject != null && this.shipScene != null) {
            this.shipObject.getGlobalTransform().mX.transform(this.zTransform, this.shipZ);
            if (this.shipObject.isDisposed()) {
                this.dispose();
                return 0;
            } else {
                if (this.imageWidth < 1 || this.imageHeight < 1) {
                    this.imageWidth = (int) Math.abs(this.material.getDiffuseTexture().getTransformedWidth() * this.material.getXScaling());
                    this.imageHeight = (int) Math.abs(this.material.getDiffuseTexture().getTransformedWidth() * this.material.getXScaling());
                }

                this.globalTransform.setIdentity();
                if (this.targetObject != null) {
                    double var2 = Vector3dOperations.n(this.targetObject.getPosition(), this.shipObject.getPosition());
                    this.tempVect.x = var2 * (double) this.shipZ.x;
                    this.tempVect.y = var2 * (double) this.shipZ.y;
                    this.tempVect.z = var2 * (double) this.shipZ.z;
                    this.screenPos.x = this.tempVect.x + this.shipObject.getPosition().x;
                    this.screenPos.y = this.tempVect.y + this.shipObject.getPosition().y;
                    this.screenPos.z = this.tempVect.z + this.shipObject.getPosition().z;
                } else {
                    this.shipZ.x *= this.projectionDistance;
                    this.shipZ.y *= this.projectionDistance;
                    this.shipZ.z *= this.projectionDistance;
                    this.screenPos.setVec3f(this.shipZ);
                    this.screenPos.add(this.shipObject.getPosition());
                }

                this.shipScene.getWindowCoords(this.screenPos, this.screenPos);
                if (this.screenPos.z <= 1.0D && this.screenPos.z >= 0.0D) {
                    this.onScreen = true;
                } else {
                    this.onScreen = false;
                }

                this.transform.setIdentity();
                this.transform.setTranslation((double) ((int) Math.round(this.screenPos.x)), (double) ((int) Math.round(this.screenPos.y)), 0.0D);
                this.position.set((float) ((int) Math.round(this.screenPos.x)), (float) ((int) Math.round(this.screenPos.y)));
                this.globalTransform.e(this.transform);
                this.globalTransform.B((float) this.imageHeight);
                this.globalTransform.C((float) this.imageWidth);
                return super.step(var1);
            }
        } else {
            return 0;
        }
    }

    public void render(GuiContext var1) {
        if (this.render && this.onScreen) {
            var1.addPrimitive(this.material.getShader(), this.material, billBoard, this.globalTransform, this.primitiveColor);
        }

    }

    public RenderAsset cloneAsset() {
        return new GAim(this);
    }

    public void validate(SceneLoader var1) {
        this.material.setShader(var1.getDefaultBillboardShader());
        super.validate(var1);
    }

    public void setZTransform(Vec3f var1) {
        this.zTransform.set(var1);
    }
}
