package taikodom.render.gui;

import taikodom.render.GuiContext;
import taikodom.render.loader.RenderAsset;
import taikodom.render.textures.Material;

public class GuiRenderObject extends GuiSceneObject {
    protected Material material;

    public GuiRenderObject() {
        this.material = new Material();
    }

    public GuiRenderObject(GuiRenderObject var1) {
        this.material = (Material) this.smartClone(var1.material);
    }

    protected Object smartClone(Object var1) {
        return var1 instanceof RenderAsset ? ((RenderAsset) var1).cloneAsset() : var1;
    }

    public void render(GuiContext var1) {
    }

    public RenderAsset cloneAsset() {
        return null;
    }

    public Material getMaterial() {
        return this.material;
    }

    public void setMaterial(Material var1) {
        this.material = var1;
    }
}
