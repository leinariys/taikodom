package taikodom.render.gui;

import all.aKa;
import taikodom.render.GuiContext;
import taikodom.render.StepContext;
import taikodom.render.data.ExpandableIndexData;
import taikodom.render.data.ExpandableVertexData;
import taikodom.render.data.VertexLayout;
import taikodom.render.enums.BlendType;
import taikodom.render.enums.GeometryType;
import taikodom.render.loader.RenderAsset;
import taikodom.render.loader.SceneLoader;
import taikodom.render.primitives.Mesh;
import taikodom.render.shaders.Shader;
import taikodom.render.shaders.ShaderPass;
import taikodom.render.textures.ChannelInfo;

import javax.vecmath.Vector2f;

/**
 * Отрисовка линий
 */
public class GLine extends GuiRenderObject {
    private static final aKa p1 = new aKa();
    private static final aKa p2 = new aKa();
    private static final aKa firstPoint = new aKa();
    float xCenter = 0.0F;
    float yCenter = 0.0F;
    private Mesh mesh = new Mesh();
    private ExpandableVertexData vertexes;
    private ExpandableIndexData indexes = new ExpandableIndexData(100);
    private int lastIndex = 0;
    private short lineStipple;

    public GLine() {
        VertexLayout var1 = new VertexLayout(true, false, false, false, 1);
        this.vertexes = new ExpandableVertexData(var1, 100);
        this.mesh.setIndexes(this.indexes);
        this.mesh.setVertexData(this.vertexes);
        this.mesh.setGeometryType(GeometryType.LINES);
    }

    public GLine(GLine var1) {
        VertexLayout var2 = new VertexLayout(true, false, false, false, 1);
        this.vertexes = new ExpandableVertexData(var2, 100);
        this.mesh.setIndexes(this.indexes);
        this.mesh.setVertexData(this.vertexes);
        this.mesh.setGeometryType(GeometryType.LINES);
    }

    public Shader getDefaultLineShader() {
        Shader var1 = new Shader();
        var1.setName("Default Line shader");
        ShaderPass var2 = new ShaderPass();
        var2.setName("Default Line pass");
        ChannelInfo var3 = new ChannelInfo();
        var3.setTexChannel(8);
        var2.setChannelTextureSet(0, var3);
        var2.setBlendEnabled(true);
        var2.setSrcBlend(BlendType.SRC_ALPHA);
        var2.setDstBlend(BlendType.ONE_MINUS_SRC_ALPHA);
        var2.setDepthMask(false);
        var2.setColorArrayEnabled(false);
        var2.getRenderStates().setCullFaceEnabled(true);
        var1.addPass(var2);
        return var1;
    }

    short getLineStipple() {
        return this.lineStipple;
    }

    void setLineStipple(short var1) {
        this.lineStipple = var1;
    }

    GeometryType getGeometryType() {
        return this.mesh.getGeometryType();
    }

    public void setGeometryType(GeometryType var1) {
        this.mesh.setGeometryType(var1);
    }

    /**
     * Нарисовать окружность
     *
     * @param var1 Центр рисования
     * @param var2 Радиус
     */
    public void createCircle(Vector2f var1, float var2) {
        this.createCircle(var1, var2, 0.0F, 360.0F, 5.0F);
    }

    /**
     * Вычисляем все точки для отрисовки и добавляем в список
     *
     * @param var1 Центр рисования
     * @param var2 Радиус
     * @param var3 Шаг отступ начала отрисовки первой точки окружности
     * @param var4 Количество точек окружности
     * @param var5 Шаг между точками на окружности
     */
    void createCircle(Vector2f var1, float var2, float var3, float var4, float var5) {
        float var6 = (float) (Math.PI / 180);
        if (var5 >= 0.0F && var4 >= var3) {//Вычисление первой точки
            p1.x = (float) (Math.cos((double) (var3 * var6)) * (double) var2 + (double) var1.x);
            p1.y = (float) (Math.sin((double) (var3 * var6)) * (double) var2 + (double) var1.y);
            firstPoint.set(p1);//Точка начала отрезка

            for (float var7 = var3; var7 < var4; var7 += var5) {//Вычисление остальных точек окружности
                this.addPoint(p1);
                p2.x = (float) (Math.cos((double) (var7 * var6)) * (double) var2 + (double) var1.x);
                p2.y = (float) (Math.sin((double) (var7 * var6)) * (double) var2 + (double) var1.y);
                this.addPoint(p2);
                p1.set(p2);
            }

            this.addPoint(p2);
            this.addPoint(firstPoint);
        }
    }

    /**
     * Нарисовать Квадрат
     *
     * @param var1 Центр рисования
     * @param var2 стороны квадрата
     */
    public void createSquare(Vector2f var1, float var2) {
        this.addPoint(-var2 + var1.x, -var2 + var1.y);
        this.addPoint(var2 + var1.x, -var2 + var1.y);
        this.addPoint(var2 + var1.x, -var2 + var1.y);
        this.addPoint(var2 + var1.x, var2 + var1.y);
        this.addPoint(var2 + var1.x, var2 + var1.y);
        this.addPoint(-var2 + var1.x, var2 + var1.y);
        this.addPoint(-var2 + var1.x, var2 + var1.y);
        this.addPoint(-var2 + var1.x, -var2 + var1.y);
    }

    /**
     * Нарисовать Прямоугольник
     *
     * @param var1 Центр рисования
     * @param var2 Ширина прямоугольника
     * @param var3 Высота прямоугольника
     */
    public void createRectangle(Vector2f var1, float var2, float var3) {
        this.addPoint(-var2 + var1.x, -var3 + var1.y);
        this.addPoint(var2 + var1.x, -var3 + var1.y);
        this.addPoint(var2 + var1.x, -var3 + var1.y);
        this.addPoint(var2 + var1.x, var3 + var1.y);
        this.addPoint(var2 + var1.x, var3 + var1.y);
        this.addPoint(-var2 + var1.x, var3 + var1.y);
        this.addPoint(-var2 + var1.x, var3 + var1.y);
        this.addPoint(-var2 + var1.x, -var3 + var1.y);
    }

    /**
     * Нарисовать Эллипс
     *
     * @param var1 Центр рисования
     * @param var2 Ширина элипса
     * @param var3 Высота элипса
     * @param var6 Шаг между точками элипса
     */
    public void createElipse(Vector2f var1, float var2, float var3, float var6) {
        float var4 = (float) (Math.PI / 180);
        p1.x = (float) (Math.cos((double) (xCenter * var4)) * (double) var2 + (double) var1.x);
        p1.y = (float) (Math.sin((double) (yCenter * var4)) * (double) var3 + (double) var1.y);
        firstPoint.set(p1);//Первая точка эллипса

        for (float var5 = 0.0F; var5 < 360.0F; var5 += var6) {//Остальные точки эллипса
            this.addPoint(p1);
            p2.x = (float) (Math.cos((double) (var5 * var4)) * (double) var2 + (double) var1.x);
            p2.y = (float) (Math.sin((double) (var5 * var4)) * (double) var3 + (double) var1.y);
            this.addPoint(p2);
            p1.set(p2);
        }

        this.addPoint(p2);
        this.addPoint(firstPoint);
    }

    void forcedDraw(GuiContext var1) {
        this.render(var1);
    }

    void setDrawLine() {
    }

    void setDrawBigSegments() {
    }

    void setDrawMediumSegments() {
    }

    void setDrawSmallSegments() {
    }

    public int step(StepContext var1) {
        return super.step(var1);
    }

    public void render(GuiContext var1) {
        if (this.render) {
            if (this.material.getShader() == null) {
                this.material.setShader(this.getDefaultLineShader());
            }

            this.transform.setTranslation((double) (this.position.x + var1.getViewportWidth() / 2.0F), (double) (this.position.y + var1.getViewportHeight() / 2.0F), 0.0D);
            var1.addPrimitive(this.material.getShader(), this.material, this.mesh, this.transform, this.primitiveColor);
        }
    }

    public RenderAsset cloneAsset() {
        return new GLine(this);
    }

    public void addPoint(float var1, float var2) {
        this.vertexes.setPosition(this.lastIndex, var1, var2, 0.0F);
        this.indexes.setIndex(this.lastIndex, this.lastIndex);
        ++this.lastIndex;
        this.indexes.setSize(this.lastIndex);
    }

    public void addPoint(aKa var1) {
        this.addPoint(var1.x, var1.y);
    }

    public void validate(SceneLoader var1) {
        this.material.setShader(var1.getDefaultSpaceDustShader());
        super.validate(var1);
    }

    public void resetLine() {
        this.lastIndex = 0;
        this.indexes.setSize(0);
    }
}
