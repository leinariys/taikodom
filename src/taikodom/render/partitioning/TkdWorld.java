package taikodom.render.partitioning;

import all.Vector3dOperations;
import taikodom.render.RenderContext;
import taikodom.render.StepContext;
import taikodom.render.helpers.RenderVisitor;
import taikodom.render.scene.SceneObject;

import javax.media.opengl.GL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TkdWorld extends RenderZone {
    private List renderableObjects = new ArrayList();
    private List stepableObjects = new ArrayList();
    RenderVisitor findSteppablesVisitor = new RenderVisitor() {
        public boolean visit(SceneObject var1) {
            if (var1.isNeedToStep()) {
                TkdWorld.this.stepableObjects.add(var1);
            }

            return true;
        }
    };
    RenderVisitor removeSteppablesVisitor = new RenderVisitor() {
        public boolean visit(SceneObject var1) {
            if (var1.isNeedToStep()) {
                TkdWorld.this.stepableObjects.remove(var1);
            }

            return true;
        }
    };
    private List sparseCells = new ArrayList();
    private float cellSize = 20000.0F;
    private int cellDepth = 2;

    private Octree findCell(Vector3dOperations var1) {
        Iterator var3 = this.sparseCells.iterator();

        while (var3.hasNext()) {
            Octree var2 = (Octree) var3.next();
            if (var2.isPointInside(var1)) {
                return var2;
            }
        }

        double var9 = Math.floor(var1.x / (double) this.cellSize + 0.5D) * (double) this.cellSize;
        double var4 = Math.floor(var1.y / (double) this.cellSize + 0.5D) * (double) this.cellSize;
        double var6 = Math.floor(var1.z / (double) this.cellSize + 0.5D) * (double) this.cellSize;
        Octree var8 = new Octree(this, this.cellDepth, new Vector3dOperations(var9, var4, var6), new Vector3dOperations((double) this.cellSize, (double) this.cellSize, (double) this.cellSize));
        var8.setMainNode(true);
        this.sparseCells.add(var8);
        return var8;
    }

    private void destroyCells() {
        Iterator var2 = this.sparseCells.iterator();

        while (var2.hasNext()) {
            Octree var1 = (Octree) var2.next();
            var1.dispose();
        }

        this.sparseCells.clear();
    }

    public void addChild(SceneObject var1) {
        this.renderableObjects.add(var1);
        var1.setRenderZone(this);
        this.findStepableObjects(var1);
    }

    public void addStaticChild(SceneObject var1) {
        Octree var2 = this.findCell(var1.getAabbWorldSpace().zO());
        if (var2 != null) {
            var2.addStaticChild(var1);
        } else {
            this.addChild(var1);
        }

        this.findStepableObjects(var1);
    }

    public void cullZone(RenderContext var1, boolean var2) {
        Iterator var4 = this.sparseCells.iterator();

        while (var4.hasNext()) {
            Octree var3 = (Octree) var4.next();
            var3.isEmpty();
            int var5 = var1.getFrustum().b(var3.getAABB());
            if (var5 == 1) {
                var3.cullZone(var1, true);
            } else if (var5 == 2) {
                var3.cullZone(var1, false);
            }
        }

        for (int var6 = this.renderableObjects.size() - 1; var6 >= 0; --var6) {
            SceneObject var7 = (SceneObject) this.renderableObjects.get(var6);
            if (var7.isDisposed()) {
                var7.setRenderZone((RenderZone) null);
                this.renderableObjects.remove(var6);
            } else {
                var7.fillRenderQueue(var1, true);
            }
        }

    }

    public void findStepableObjects(SceneObject var1) {
        var1.visitPreOrder(this.findSteppablesVisitor);
    }

    public final void onObjectTransform(SceneObject var1) {
    }

    public void rebuildUp() {
    }

    public void removeAll() {
        this.stepableObjects.clear();
        this.renderableObjects.clear();
        this.destroyCells();
    }

    public void removeChild(SceneObject var1) {
        this.renderableObjects.remove(var1);
        this.removeStepableObjects(var1);
    }

    public void stepObjects(StepContext var1) {
        int var2 = this.stepableObjects.size();
        var1.incObjectSteps((long) var2);

        for (int var3 = 0; var3 < var2; ++var3) {
            SceneObject var4 = (SceneObject) this.stepableObjects.get(var3);
            if (var4.isDisposed() || var4.step(var1) != 0) {
                this.stepableObjects.remove(var3);
                --var3;
                --var2;
            }
        }

    }

    public void renderGL(GL var1, Vector3dOperations var2) {
        var1.glEnable(3042);
        var1.glBlendFunc(770, 771);
        var1.glBegin(GL.GL_LINES);//Этот метод запускает процесс рисования
        Iterator var4 = this.sparseCells.iterator();

        while (var4.hasNext()) {
            Octree var3 = (Octree) var4.next();
            var3.renderGL(var1, var2);
        }

        var1.glEnd();
        var1.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        var1.glDisable(3042);
        var1.glBlendFunc(1, 1);
    }

    public void removeStepableObjects(SceneObject var1) {
        var1.visitPreOrder(this.removeSteppablesVisitor);
    }
}
