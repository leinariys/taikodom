package taikodom.render.partitioning;

import all.Vector3dOperations;
import all.aLH;
import taikodom.render.RenderContext;
import taikodom.render.scene.SceneObject;

import javax.media.opengl.GL;
import javax.vecmath.Tuple3d;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Octree extends RenderZone {
    private final aLH aabb = new aLH();
    private final Vector3dOperations center = new Vector3dOperations();
    private final Vector3dOperations defaultHalfSize = new Vector3dOperations();
    private RenderZone parent;
    private Octree[] children = new Octree[8];
    private boolean mainNode = false;
    private boolean leaf;
    private boolean empty;
    private int depth;
    private List objects = new ArrayList();

    public Octree(RenderZone var1, int var2, Vector3dOperations var3, Vector3dOperations var4) {
        this.parent = var1;
        this.center.aA(var3);
        this.defaultHalfSize.aA(var4);
        this.defaultHalfSize.scale(0.5D);
        this.empty = true;
        this.leaf = false;
        this.depth = var2;
        if (var2 == 0) {
            this.leaf = true;
        } else {
            --var2;
            Vector3dOperations var5 = new Vector3dOperations(this.defaultHalfSize);
            double var6 = var4.x / 4.0D;
            double var8 = var4.y / 4.0D;
            double var10 = var4.z / 4.0D;
            this.children[0] = new Octree(this, var2, var3.y(-var6, -var8, -var10), var5);
            this.children[1] = new Octree(this, var2, var3.y(var6, -var8, -var10), var5);
            this.children[2] = new Octree(this, var2, var3.y(var6, -var8, var10), var5);
            this.children[3] = new Octree(this, var2, var3.y(-var6, -var8, var10), var5);
            this.children[4] = new Octree(this, var2, var3.y(-var6, var8, -var10), var5);
            this.children[5] = new Octree(this, var2, var3.y(var6, var8, -var10), var5);
            this.children[6] = new Octree(this, var2, var3.y(var6, var8, var10), var5);
            this.children[7] = new Octree(this, var2, var3.y(-var6, var8, var10), var5);
        }

    }

    public void addChild(SceneObject var1) {
        if (this.leaf) {
            this.empty = false;
            this.objects.add(var1);
            var1.setRenderZone(this);
            this.rebuildUp();
        } else {
            Vector3dOperations var2 = var1.getAabbWorldSpace().zO();
            if (var2.x < this.center.x) {
                if (var2.y < this.center.y) {
                    if (var2.z < this.center.z) {
                        this.children[0].addChild(var1);
                    } else {
                        this.children[3].addChild(var1);
                    }
                } else if (var2.z < this.center.z) {
                    this.children[4].addChild(var1);
                } else {
                    this.children[7].addChild(var1);
                }
            } else if (var2.y < this.center.y) {
                if (var2.z < this.center.z) {
                    this.children[1].addChild(var1);
                } else {
                    this.children[2].addChild(var1);
                }
            } else if (var2.z < this.center.z) {
                this.children[5].addChild(var1);
            } else {
                this.children[6].addChild(var1);
            }
        }

    }

    public void addStaticChild(SceneObject var1) {
        this.addChild(var1);
    }

    public void cullZone(RenderContext var1, boolean var2) {
        var1.getDc().incOctreeNodesVisited();
        boolean var3;
        int var4;
        if (this.leaf) {
            var3 = false;

            for (var4 = this.objects.size() - 1; var4 >= 0; --var4) {
                SceneObject var5 = (SceneObject) this.objects.get(var4);
                if (var5.isDisposed()) {
                    this.removeButNotRebuild(var5);
                    var3 = true;
                } else {
                    var5.fillRenderQueue(var1, var2);
                }
            }

            if (var3) {
                this.rebuildUp();
            }
        } else {
            var3 = false;

            for (var4 = 0; var4 < 8; ++var4) {
                Octree var7 = this.children[var4];
                if (!var7.isEmpty()) {
                    if (var2) {
                        int var6 = var1.getFrustum().b(var7.getAABB());
                        if (var6 == 1) {
                            var7.cullZone(var1, true);
                        } else if (var6 == 2) {
                            var7.cullZone(var1, false);
                        }
                    } else {
                        var7.cullZone(var1, false);
                    }
                }
            }
        }

    }

    private boolean removeButNotRebuild(SceneObject var1) {
        var1.setRenderZone((RenderZone) null);
        if (this.objects.remove(var1)) {
            if (this.objects.size() == 0) {
                this.empty = true;
            }

            return true;
        } else {
            return false;
        }
    }

    private void rebuildNode() {
        this.aabb.reset();
        if (!this.leaf) {
            this.empty = true;

            for (int var1 = 0; var1 < 8; ++var1) {
                if (!this.children[var1].isEmpty()) {
                    this.aabb.h(this.children[var1].getAABB());
                    this.empty = false;
                }
            }
        }

        if (!this.empty) {
            LinkedList var5 = new LinkedList();

            int var2;
            for (var2 = 0; var2 < this.objects.size(); ++var2) {
                var5.add((SceneObject) this.objects.get(var2));
            }

            for (var2 = 0; var2 < var5.size(); ++var2) {
                SceneObject var3 = (SceneObject) var5.get(var2);

                for (int var4 = 0; var4 < var3.childCount(); ++var4) {
                    var5.add(var3.getChild(var4));
                }

                this.aabb.h(((SceneObject) var5.get(var2)).getAabbWorldSpace());
            }

            var5.clear();
            var5 = null;
        }

    }

    public void findStepableObjects(SceneObject var1) {
        if (this.parent != null) {
            this.parent.findStepableObjects(var1);
        }

    }

    public void removeStepableObjects(SceneObject var1) {
        if (this.parent != null) {
            this.parent.removeStepableObjects(var1);
        }

    }

    public void onObjectTransform(SceneObject var1) {
        Vector3dOperations var2 = var1.getAabbWorldSpace().zO().f((Tuple3d) this.center);
        if (Math.abs(var2.x) <= this.defaultHalfSize.x && Math.abs(var2.y) <= this.defaultHalfSize.y && Math.abs(var2.z) <= this.defaultHalfSize.z) {
            if (var1.getRenderZone() == this) {
                this.rebuildUp();
            } else {
                this.addChild(var1);
            }
        } else {
            if (var1.getRenderZone() == this) {
                this.removeButNotRebuild(var1);
            }

            this.rebuildNode();
            if (!this.mainNode && this.parent != null) {
                this.parent.onObjectTransform(var1);
            } else if (this.parent != null) {
                this.parent.addStaticChild(var1);
            } else {
                this.addStaticChild(var1);
            }
        }

    }

    public void rebuildUp() {
        this.rebuildNode();
        if (this.parent != null) {
            this.parent.rebuildUp();
        }

    }

    public void removeAll() {
        int var1;
        for (var1 = 0; var1 < this.objects.size(); ++var1) {
            ((SceneObject) this.objects.get(var1)).setRenderZone((RenderZone) null);
        }

        this.objects.clear();
        this.empty = true;
        if (!this.leaf) {
            for (var1 = 0; var1 < 8; ++var1) {
                if (!this.children[var1].isEmpty()) {
                    this.children[var1].removeAll();
                }
            }
        }

    }

    public void removeChild(SceneObject var1) {
        if (this.removeButNotRebuild(var1)) {
            this.rebuildUp();
        }

    }

    public boolean isPointInside(Vector3dOperations var1) {
        Vector3dOperations var2 = this.center.f((Tuple3d) var1);
        return Math.abs(var2.x) < this.defaultHalfSize.x && Math.abs(var2.y) < this.defaultHalfSize.y && Math.abs(var2.z) < this.defaultHalfSize.z;
    }

    public void setMainNode(boolean var1) {
        this.mainNode = var1;
    }

    public void dispose() {
        this.removeAll();
        if (!this.leaf) {
            for (int var1 = 0; var1 < 8; ++var1) {
                this.children[var1].dispose();
            }
        }

    }

    public boolean isEmpty() {
        return this.empty;
    }

    public aLH getAABB() {
        return this.aabb;
    }

    public void renderGL(GL var1, Vector3dOperations var2) {
        if (!this.empty) {
            var1.glColor3f(1.0F, 0.5F + (float) this.depth * 0.1F, (float) this.depth * 0.2F);
            Vector3dOperations var3 = this.aabb.dim().f((Tuple3d) var2);
            Vector3dOperations var4 = this.aabb.din().f((Tuple3d) var2);
            var1.glVertex3d(var3.x, var3.y, var3.z);
            var1.glVertex3d(var4.x, var3.y, var3.z);
            var1.glVertex3d(var3.x, var3.y, var3.z);
            var1.glVertex3d(var3.x, var4.y, var3.z);
            var1.glVertex3d(var3.x, var3.y, var3.z);
            var1.glVertex3d(var3.x, var3.y, var4.z);
            var1.glVertex3d(var4.x, var4.y, var4.z);
            var1.glVertex3d(var3.x, var4.y, var4.z);
            var1.glVertex3d(var4.x, var4.y, var4.z);
            var1.glVertex3d(var4.x, var3.y, var4.z);
            var1.glVertex3d(var4.x, var4.y, var4.z);
            var1.glVertex3d(var4.x, var4.y, var3.z);
            var1.glVertex3d(var3.x, var4.y, var3.z);
            var1.glVertex3d(var4.x, var4.y, var3.z);
            var1.glVertex3d(var3.x, var4.y, var3.z);
            var1.glVertex3d(var3.x, var4.y, var4.z);
            var1.glVertex3d(var3.x, var4.y, var4.z);
            var1.glVertex3d(var3.x, var3.y, var4.z);
            var1.glVertex3d(var3.x, var3.y, var4.z);
            var1.glVertex3d(var4.x, var3.y, var4.z);
            var1.glVertex3d(var4.x, var3.y, var3.z);
            var1.glVertex3d(var4.x, var3.y, var4.z);
            var1.glVertex3d(var4.x, var3.y, var3.z);
            var1.glVertex3d(var4.x, var4.y, var3.z);
            if (!this.leaf) {
                for (int var5 = 0; var5 < 8; ++var5) {
                    this.children[var5].renderGL(var1, var2);
                }
            }

            if (this.mainNode) {
                var1.glColor3f(0.0F, 1.0F, 1.0F);
                var3 = this.center.f((Tuple3d) this.defaultHalfSize);
                var3.sub(var2);
                var4 = this.center.d((Tuple3d) this.defaultHalfSize);
                var4.sub(var2);
                var1.glVertex3d(var3.x, var3.y, var3.z);
                var1.glVertex3d(var4.x, var3.y, var3.z);
                var1.glVertex3d(var3.x, var3.y, var3.z);
                var1.glVertex3d(var3.x, var4.y, var3.z);
                var1.glVertex3d(var3.x, var3.y, var3.z);
                var1.glVertex3d(var3.x, var3.y, var4.z);
                var1.glVertex3d(var4.x, var4.y, var4.z);
                var1.glVertex3d(var3.x, var4.y, var4.z);
                var1.glVertex3d(var4.x, var4.y, var4.z);
                var1.glVertex3d(var4.x, var3.y, var4.z);
                var1.glVertex3d(var4.x, var4.y, var4.z);
                var1.glVertex3d(var4.x, var4.y, var3.z);
                var1.glVertex3d(var3.x, var4.y, var3.z);
                var1.glVertex3d(var4.x, var4.y, var3.z);
                var1.glVertex3d(var3.x, var4.y, var3.z);
                var1.glVertex3d(var3.x, var4.y, var4.z);
                var1.glVertex3d(var3.x, var4.y, var4.z);
                var1.glVertex3d(var3.x, var3.y, var4.z);
                var1.glVertex3d(var3.x, var3.y, var4.z);
                var1.glVertex3d(var4.x, var3.y, var4.z);
                var1.glVertex3d(var4.x, var3.y, var3.z);
                var1.glVertex3d(var4.x, var3.y, var4.z);
                var1.glVertex3d(var4.x, var3.y, var3.z);
                var1.glVertex3d(var4.x, var4.y, var3.z);
            }

        }
    }
}
