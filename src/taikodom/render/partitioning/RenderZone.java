package taikodom.render.partitioning;

import all.Vector3dOperations;
import taikodom.render.RenderContext;
import taikodom.render.scene.SceneObject;

import javax.media.opengl.GL;

public abstract class RenderZone {
    public abstract void addChild(SceneObject var1);

    public abstract void addStaticChild(SceneObject var1);

    public abstract void removeChild(SceneObject var1);

    public abstract void rebuildUp();

    public abstract void findStepableObjects(SceneObject var1);

    public abstract void removeStepableObjects(SceneObject var1);

    public abstract void cullZone(RenderContext var1, boolean var2);

    public abstract void onObjectTransform(SceneObject var1);

    public abstract void removeAll();

    public abstract void renderGL(GL var1, Vector3dOperations var2);
}
