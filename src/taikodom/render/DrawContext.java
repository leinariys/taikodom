package taikodom.render;

import all.Vector3dOperations;
import all.ajK;
import com.hoplon.geometry.Vec3f;
import com.sun.opengl.util.BufferUtil;
import org.lwjgl.BufferUtils;
import taikodom.render.gl.GLSupportedCaps;
import taikodom.render.primitives.Light;
import taikodom.render.primitives.Primitive;
import taikodom.render.primitives.TreeRecord;
import taikodom.render.scene.LightRecordList;
import taikodom.render.scene.SceneConfig;
import taikodom.render.scene.Viewport;
import taikodom.render.shaders.ShaderPass;
import taikodom.render.textures.Material;
import taikodom.render.textures.PBuffer;

import javax.media.opengl.GL;
import javax.media.opengl.GLContext;
import javax.media.opengl.glu.GLU;
import java.nio.FloatBuffer;
import java.util.LinkedList;
import java.util.List;

/**
 * Инструменты рисования
 * Контекст рисования
 */
public class DrawContext {
    private static PBuffer[] auxPBuffer;
    private static boolean useVbo = false;
    public final ajK camProjection = new ajK();
    public final ajK gpuCamTransform = new ajK();
    public final ajK gpuCamAffine = new ajK();
    public final Vector3dOperations gpuOffset = new Vector3dOperations();
    private final Viewport viewport = new Viewport();
    public RenderView renderView;
    public Material currentMaterial;
    public Primitive currentPrimitive;
    public MaterialDrawingStates materialState = new MaterialDrawingStates();
    public PrimitiveDrawingState primitiveState = new PrimitiveDrawingState();
    public ShaderParametersRenderingContext shaderParamContext = new ShaderParametersRenderingContext();
    public FloatBuffer tempBuffer0 = BufferUtil.newFloatBuffer(32);
    public Vec3f vec3fTemp0 = new Vec3f();
    public Vec3f vec3fTemp1 = new Vec3f();
    public Vec3f vec3fTemp2 = new Vec3f();
    public Vector3dOperations vec3dTemp0 = new Vector3dOperations();
    public Vector3dOperations vec3dTemp1 = new Vector3dOperations();
    public Vector3dOperations vec3dTemp2 = new Vector3dOperations();
    public FloatBuffer floatBuffer60Temp0 = BufferUtils.createFloatBuffer(16);
    public ajK matrix4fTemp0 = new ajK();
    public ajK matrix4fTemp1 = new ajK();
    public ajK matrix4fTemp2 = new ajK();
    public ShaderPass currentPass;
    public SceneConfig sceneConfig = new SceneConfig();
    public List eventList;
    LightRecordList.LightRecord currentLight;
    TreeRecord currentRecord;
    String roll = ".:|:";
    /**
     * GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
     * седержит методы для рисования примитивов (линии треугольники)
     */
    private GL gl;
    /**
     * Контекст сохраняет все состояние, связанное с этим экземпляром OpenGL.
     * Он представляет (потенциально видимый) фреймбуфер по умолчанию, который будут отображать команды рендеринга,
     * если не рисовать на объект framebuffer. Подумайте о контексте как об объекте, который содержит все OpenGL;
     * когда контекст уничтожается, OpenGL уничтожается.
     */
    private GLContext glContext;
    /**
     * OpenGL Utility Library (GLU) — графическая библиотека, надстройка над OpenGL,
     * использующая её функции для рисования более сложных объектов.
     */
    private GLU glu = new GLU();
    private double totalTime;
    private float elapsedTime;
    private long currentFrame;
    private long numTexturesBind;
    private long numShadersBind;
    private long numMaterialsBind;
    private long numPrimitiveBind;
    private long numTriangles;
    private long numMeshesBinds;
    private long textureMemoryBinded;
    private long numLights;
    private long numImpostorsRendered;
    private long numImpostorsUpdated;
    private long meshesBind;
    private long drawCalls;
    private long octreeNodesVisited;
    private long scissorOffsetX;
    private long scissorOffsetY;
    private long scissorWidth;
    private long scissorHeight;
    private int maxShaderQuality;
    private boolean rendering;
    private long numObjectsStepped;
    private long numBillboards;
    private Light mainLight;

    public PBuffer getAuxPBuffer() {
        if (auxPBuffer == null) {
            auxPBuffer = new PBuffer[2];
            auxPBuffer[0] = new PBuffer();
            auxPBuffer[0].create(this, 1024, 1024);
            auxPBuffer[1] = new PBuffer();
            auxPBuffer[1].create(this, 1024, 1024);
        }
        byte var1 = 0;
        if (auxPBuffer[0].isActive()) {
            var1 = 1;
        }
        return auxPBuffer[var1];
    }

    public GLU glu() {
        return this.glu;
    }

    /**
     * GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
     * седержит методы для рисования примитивов (линии треугольники)
     *
     * @return
     */
    public GL getGL() {
        return this.gl;
    }

    /**
     * GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
     * седержит методы для рисования примитивов (линии треугольники)
     *
     * @param var1
     */
    public void setGL(GL var1) {
        this.gl = var1;
    }

    public void incNumMeshesBinds() {
        ++this.meshesBind;
    }

    public void incNumDrawCalls() {
        ++this.drawCalls;
    }

    public long getNumDrawCalls() {
        return this.drawCalls;
    }

    public void incNumTriangles(int var1) {
        this.numTriangles += (long) var1;
    }

    public long getNumTriangles() {
        return this.numTriangles;
    }

    public long getCurrentFrame() {
        return this.currentFrame;
    }

    public void setCurrentFrame(long var1) {
        this.currentFrame = var1;
    }

    public double getTotalTime() {
        return this.totalTime;
    }

    public void setTotalTime(double var1) {
        this.totalTime = var1;
    }

    public float getElapsedTime() {
        return this.elapsedTime;
    }

    public void setElapsedTime(float var1) {
        this.elapsedTime = var1;
    }

    public void reset() {
        this.drawCalls = 0L;
        this.numTexturesBind = 0L;
        this.numPrimitiveBind = 0L;
        this.numMeshesBinds = 0L;
        this.numTriangles = 0L;
        this.numImpostorsRendered = 0L;
        this.numImpostorsUpdated = 0L;
        this.meshesBind = 0L;
        this.numMaterialsBind = 0L;
        this.numShadersBind = 0L;
        this.octreeNodesVisited = 0L;
        this.mainLight = null;
    }

    public void incCurrentFrame() {
        ++this.currentFrame;
    }

    public Light getMainLight() {
        return this.mainLight;
    }

    public void setMainLight(Light var1) {
        this.mainLight = var1;
    }

    public TreeRecord currentRecord() {
        return this.currentRecord;
    }

    public void setCurrentLight(LightRecordList.LightRecord var1) {
        this.currentLight = var1;
    }

    public boolean debugDraw() {
        return this.currentFrame < 3L;
    }

    protected void doLog(String var1) {
        int var2 = 0;
        for (int var3 = (new Exception()).getStackTrace().length; var2 < var3; ++var2) {
            System.out.print(this.roll.charAt(var2 % this.roll.length()));
        }
        System.out.println(var1);
    }

    public void logDraw(Object var1) {
        this.doLog("draw " + var1);
    }

    public void logBind(Object var1) {
        this.doLog("bind " + var1);
    }

    public void logCompile(Object var1) {
        this.doLog("compile " + var1);
    }

    public void logUnbind(Object var1) {
        this.doLog("unbind " + var1);
    }

    public void log(String var1) {
        this.doLog(var1);
    }

    public RenderView getRenderView() {
        return this.renderView;
    }

    public void setRenderView(RenderView var1) {
        this.renderView = var1;
    }

    public void postSceneEvent(String var1, Object var2) {
        if (this.eventList == null) {
            this.eventList = new LinkedList();
        }

        this.eventList.add(new SceneEvent(var1, var2));
    }

    public void setGpuOffset(Vector3dOperations var1) {
        this.gpuOffset.aA(var1);
    }

    public int getScreenOffsetX() {
        return this.viewport.x;
    }

    public int getScreenOffsetY() {
        return this.viewport.y;
    }

    public int getScreenWidth() {
        return this.viewport.width;
    }

    public int getScreenHeight() {
        return this.viewport.height;
    }

    public void setViewport(int var1, int var2, int var3, int var4) {
        this.viewport.x = var1;
        this.viewport.y = var2;
        this.viewport.width = var3;
        this.viewport.height = var4;
    }

    public long getOctreeNodesVisited() {
        return this.octreeNodesVisited;
    }

    public void incOctreeNodesVisited() {
        ++this.octreeNodesVisited;
    }

    public GLContext getGlContext() {
        return this.glContext;
    }

    public void setGlContext(GLContext var1) {
        this.glContext = var1;
    }

    public long getNumShadersBind() {
        return this.numShadersBind;
    }

    public void incNumShadersBind() {
        ++this.numShadersBind;
    }

    public void incNumMaterialBinds() {
        ++this.numMaterialsBind;
    }

    public long getNumMaterarialsBind() {
        return this.numMaterialsBind;
    }

    public void incNumImpostorsRendered() {
        ++this.numImpostorsRendered;
    }

    public long getNumImpostorsRendered() {
        return this.numImpostorsRendered;
    }

    public void incNumImpostorsUpdated() {
        ++this.numImpostorsUpdated;
    }

    public long getNumImpostorsUpdated() {
        return this.numImpostorsUpdated;
    }

    public int getMaxShaderQuality() {
        return this.maxShaderQuality;
    }

    public void setMaxShaderQuality(int var1) {
        this.maxShaderQuality = var1;
    }

    public boolean getRendering() {
        return this.rendering;
    }

    public void setRendering(boolean var1) {
        this.rendering = var1;
    }

    public boolean isUseVbo() {
        return useVbo;
    }

    public void setUseVbo(boolean var1) {
        useVbo = var1 && GLSupportedCaps.isVbo();
    }

    public long getNumPrimitiveBind() {
        return this.numPrimitiveBind;
    }

    public void incNumPrimitiveBind() {
        ++this.numPrimitiveBind;
    }

    public long getNumTextureBind() {
        return this.numTexturesBind;
    }

    public void incNumTextureBind() {
        ++this.numTexturesBind;
    }

    public Viewport getViewport() {
        return this.viewport;
    }

    public void setViewport(Viewport var1) {
        this.viewport.set(var1);
    }
}
