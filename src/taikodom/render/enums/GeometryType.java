package taikodom.render.enums;

public enum GeometryType {
    TRIANGLES(4),
    TRIANGLE_FAN(6),
    TRIANGLE_STRIP(5),
    LINES(1),
    LINE_LOOP(2),
    QUADS(7),
    QUAD_STRIP(8),
    POINTS(0);

    private final int glEquivalent;

    private GeometryType(int var3) {
        this.glEquivalent = var3;
    }

    public int glEquivalent() {
        return this.glEquivalent;
    }
}
