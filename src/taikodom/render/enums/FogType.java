package taikodom.render.enums;

public enum FogType {
    FOG_LINEAR(9729),
    FOG_EXP(2048),
    FOG_EXP2(2049);

    private final int glEquivalent;

    private FogType(int var3) {
        this.glEquivalent = var3;
    }

    public int glEquivalent() {
        return this.glEquivalent;
    }
}
