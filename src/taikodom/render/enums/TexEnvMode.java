package taikodom.render.enums;

public enum TexEnvMode {
    MODULATE(8448),
    DECAL(8449),
    ADD(260),
    BLEND(3042),
    REPLACE(7681);

    private final int glEquivalent;

    private TexEnvMode(int var3) {
        this.glEquivalent = var3;
    }

    public int glEquivalent() {
        return this.glEquivalent;
    }
}
