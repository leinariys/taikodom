package taikodom.render.enums;

public enum BlendType {
    ZERO(0),
    ONE(1),
    SRC_COLOR(768),
    ONE_MINUS_SRC_COLOR(769),
    SRC_ALPHA(770),
    DST_ALPHA(772),
    DST_COLOR(774),
    ONE_MINUS_DST_ALPHA(773),
    ONE_MINUS_SRC_ALPHA(771);

    private final int glEquivalent;

    private BlendType(int var3) {
        this.glEquivalent = var3;
    }

    public int glEquivalent() {
        return this.glEquivalent;
    }
}
