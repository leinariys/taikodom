package taikodom.render.enums;

public enum ShapeTypes {
    SHAPE_DISC,
    SHAPE_SPHERE,
    SHAPE_RECTANGLE,
    SHAPE_CUBE,
    SHAPE_SPIRAL;
}
