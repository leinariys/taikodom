package taikodom.render.enums;

public enum TexWrap {
    CLAMP(10496),
    CLAMP_TO_EDGE(33071),
    CLAMP_TO_BORDER(33069),
    REPEAT(10497),
    MIRRORED_REPEAT(33648);

    private final int glEquivalent;

    private TexWrap(int var3) {
        this.glEquivalent = var3;
    }

    public int glEquivalent() {
        return this.glEquivalent;
    }
}
