package taikodom.render.enums;

public enum LightType {
    NONE,
    POINT_LIGHT,
    DIRECTIONAL_LIGHT,
    SPOT_LIGHT;
}
