package taikodom.render.enums;

public enum BillboardAlignment {
    VIEW_ALIGNED,
    AXIS_ALIGNED,
    SCREEN_ALIGNED,
    USER_ALIGNED;
}
