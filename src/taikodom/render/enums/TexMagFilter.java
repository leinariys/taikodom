package taikodom.render.enums;

public enum TexMagFilter {
    NONE(0),
    NEAREST(9728),
    LINEAR(9729);

    private final int glEquivalent;

    private TexMagFilter(int var3) {
        this.glEquivalent = var3;
    }

    public int glEquivalent() {
        return this.glEquivalent;
    }
}
