package taikodom.render.enums;

public enum CullFace {
    FRONT(1028),
    BACK(1029),
    FRONT_AND_BACK(1032);

    private final int glEquivalent;

    private CullFace(int var3) {
        this.glEquivalent = var3;
    }

    public int glEquivalent() {
        return this.glEquivalent;
    }
}
