package taikodom.render;

import all.bc_q;
import com.hoplon.geometry.Color;
import taikodom.render.primitives.Primitive;
import taikodom.render.primitives.TreeRecord;
import taikodom.render.scene.TreeRecordList;
import taikodom.render.scene.Viewport;
import taikodom.render.shaders.Shader;
import taikodom.render.textures.Material;

import javax.vecmath.Matrix3f;

public class GuiContext {
    protected final Viewport viewPort = new Viewport();
    DrawContext dc;
    private TreeRecordList records = new TreeRecordList();

    public float getViewportWidth() {
        return (float) this.viewPort.width;
    }

    public float getViewportHeight() {
        return (float) this.viewPort.height;
    }

    public DrawContext getDc() {
        return this.dc;
    }

    public void setDc(DrawContext var1) {
        this.dc = var1;
    }

    public void clearRecords() {
        this.records.clear();
    }

    public TreeRecord addPrimitive(Shader var1, Material var2, Primitive var3, bc_q var4, Color var5) {
        if (var2 == null) {
            throw new RuntimeException("Null material!");
        } else if (var1 == null) {
            throw new RuntimeException("Null material!");
        } else if (var3 == null) {
            throw new RuntimeException("Null material!");
        } else {
            TreeRecord var6 = this.records.add();
            var6.shader = var1;
            var6.material = var2;
            var6.primitive = var3;
            var6.transform.b(var4);
            var6.gpuTransform.a((Matrix3f) var4.mX);
            var6.gpuTransform.m03 = (float) var4.position.x;
            var6.gpuTransform.m13 = (float) var4.position.y;
            var6.gpuTransform.m23 = (float) var4.position.z;
            var6.color.set(var5);
            return var6;
        }
    }

    public TreeRecordList getRecords() {
        return this.records;
    }

    public void setRecords(TreeRecordList var1) {
        this.records = var1;
    }

    public void setViewPort(int var1, int var2, int var3, int var4) {
        this.viewPort.x = var1;
        this.viewPort.y = var2;
        this.viewPort.width = var3;
        this.viewPort.height = var4;
    }
}
