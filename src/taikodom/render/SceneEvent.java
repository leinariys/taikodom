package taikodom.render;

public class SceneEvent {
    String message;
    Object object;

    public SceneEvent(String var1, Object var2) {
        this.message = var1;
        this.object = var2;
    }

    public String getMessage() {
        return this.message;
    }

    public Object getObject() {
        return this.object;
    }
}
