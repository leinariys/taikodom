package taikodom.render;

import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLCapabilitiesChooser;
import javax.media.opengl.GLContext;
import javax.swing.*;
import java.awt.*;

/**
 *
 */
public class JGLDesktop extends MyGLCanvas implements TDesktop {

    private JDesktopPane desktopPane = new JDesktopPane();

    public JGLDesktop() {
        this.setContentPane(this.desktopPane);
    }

    public JGLDesktop(GLCapabilities var1, GLCapabilitiesChooser var2, GLContext var3, GraphicsDevice var4) {
        super(var1, var2, var3, var4);
        this.setContentPane(this.desktopPane);
    }

    /**
     * @param var1 Определяет набор возможностей OpenGL.
     */
    public JGLDesktop(GLCapabilities var1) {
        super(var1);
        this.setContentPane(this.desktopPane);
    }

    public JDesktopPane getDesktopPane() {
        return this.desktopPane;
    }

    public void setRootPane(JRootPane var1) {
        super.setRootPane(var1);
    }
}
