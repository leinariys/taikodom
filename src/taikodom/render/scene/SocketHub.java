package taikodom.render.scene;

//import all.NP;
//import all.aVz;

import java.util.List;

public class SocketHub extends SceneObject {
    // private aVz grannyBone;
    private int boneIndex;
/*
   public SocketHub(RModel var1, aVz var2) {
      this.name = var2.getTegName();
      this.grannyBone = var2;
      this.boneIndex = var1.getSkeleton().gb(this.name);
      if (this.boneIndex < 0) {
         throw new IndexOutOfBoundsException("Bone " + this.name + " not found on skeleton " + var1.getSkeleton().getTegName());
      }
   }*/

    public SocketHub(RModel var1, String var2) {
     /* NP var3 = var1.getSkeleton();
      this.boneIndex = var3.ga(var2);
      if (this.boneIndex < 0) {
         throw new IndexOutOfBoundsException("Bone " + var2 + " not found on skeleton " + var3.getTegName());
      } else {
         this.grannyBone = var3.bkH().Bg(this.boneIndex);
         this.name = this.grannyBone.getTegName();
      }*/
    }

    public SocketHub(RModel var1, int var2) {
        // NP var3 = var1.getSkeleton();
        this.boneIndex = var2;
  /*    this.grannyBone = var3.bkH().Bg(var2);
      if (this.grannyBone == null) {
         throw new IndexOutOfBoundsException("Bone [" + var2 + "] not found on skeleton " + var3.getTegName());
      } else {
         this.name = this.grannyBone.getTegName();
      }*/
    }
/*
   public String getTegName() {
      return this.grannyBone.getTegName();
   }*/

    public List getSockets() {
        return this.getChildren();
    }

    public void addSocket(Socket var1) {
        this.addChild(var1);
    }

    public void removeSocket(Socket var1) {
        this.removeChild(var1);
    }
/*
   public String getBoneName() {
      return this.grannyBone.getTegName();
   }*/

    public int getBoneIndex() {
        return this.boneIndex;
    }
}
