package taikodom.render.scene;

public class SceneViewQuality {
    private float lodQuality = 0.0F;
    private int envFxQuality = 0;
    private int shaderQuality = 0;
    private boolean postProcessingFX = true;

    public SceneViewQuality() {
    }

    public SceneViewQuality(SceneViewQuality var1) {
        this.set(var1);
    }

    public void set(SceneViewQuality var1) {
        this.lodQuality = var1.lodQuality;
        this.envFxQuality = var1.envFxQuality;
        this.shaderQuality = var1.shaderQuality;
        this.postProcessingFX = var1.postProcessingFX;
    }

    public float getLodQuality() {
        return this.lodQuality;
    }

    public void setLodQuality(float var1) {
        this.lodQuality = var1;
    }

    public int getEnvFxQuality() {
        return this.envFxQuality;
    }

    public void setEnvFxQuality(int var1) {
        this.envFxQuality = var1;
    }

    public int getShaderQuality() {
        return this.shaderQuality;
    }

    public void setShaderQuality(int var1) {
        this.shaderQuality = var1;
    }

    public boolean isPostProcessingFX() {
        return this.postProcessingFX;
    }

    public void setPostProcessingFX(boolean var1) {
        this.postProcessingFX = var1;
    }
}
