package taikodom.render.scene;

import all.Vector3dOperations;
import all.aQKa;
import taikodom.render.loader.RenderAsset;

/**
 * Реализация класса указанного в имени тегов из файлов с расширением .pro
 */
public class SSourceElement extends RenderAsset {
    private Vector3dOperations position;
    private aQKa distances;

    public SSourceElement() {
        this.position = new Vector3dOperations();
        this.distances = new aQKa();
    }

    public SSourceElement(SSourceElement var1) {
        this();
        this.position.aA(var1.position);
        this.distances.set(var1.distances);
    }

    public Vector3dOperations getPosition() {
        return this.position;
    }

    public void setPosition(Vector3dOperations var1) {
        this.position.aA(var1);
    }

    public aQKa getDistances() {
        return this.distances;
    }

    public void setDistances(aQKa var1) {
        this.distances.set(var1);
    }

    public RenderAsset cloneAsset() {
        return new SSourceElement(this);
    }
}
