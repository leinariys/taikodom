package taikodom.render.scene;

import all.Vector3dOperations;
import all.aQKa;
import all.bc_q;
import com.hoplon.geometry.Vec3f;
import taikodom.render.SoundBuffer;
import taikodom.render.StepContext;
import taikodom.render.enums.ShapeTypes;
import taikodom.render.loader.RenderAsset;
import taikodom.render.shaders.Shader;
import taikodom.render.shaders.ShaderPass;
import taikodom.render.textures.Material;

import java.util.*;
import java.util.Map.Entry;

public class SSoundGroup extends SoundObject {
    private static final bc_q mirrorXTransform = new bc_q(-1.0F, 0.0F, 0.0F, 0.0F, 1.0F, 0.0F, 0.0F, 0.0F, 1.0F, 0.0D, 0.0D, 0.0D);
    protected final List buffers = new ArrayList();
    protected final Map sources = new HashMap();
    final Vec3f velocity = new Vec3f();
    final Vec3f position = new Vec3f();
    private int loopCount;
    private float gain;
    private RShape debugShape;
    private boolean mirrorX = false;

    public SSoundGroup(SSoundGroup var1) {
        this.buffers.addAll(var1.buffers);
        Iterator var3 = var1.sources.keySet().iterator();

        while (var3.hasNext()) {
            SSourceElement var2 = (SSourceElement) var3.next();
            this.sources.put(new SSourceElement(var2), new SSoundSource());
        }

        var3 = this.sources.values().iterator();

        while (var3.hasNext()) {
            SSoundSource var4 = (SSoundSource) var3.next();
            this.addChild(var4);
        }

        this.setGain(var1.getGain());
        this.setLoopCount(var1.getLoopCount());
        if (this.debug()) {
            if (var1.debugShape == null) {
                var1.debugShape = new RShape();
                var1.debugShape.setMaterial(new Material());
                var1.debugShape.getMaterial().setShader(new Shader());
                var1.debugShape.getMaterial().getShader().addPass(new ShaderPass());
                var1.debugShape.setRender(true);
            }

            this.debugShape = new RShape(var1.debugShape);
            this.debugShape.setShapeType(ShapeTypes.SHAPE_SPHERE);
            this.debugShape.setRender(true);
        }

        this.disposeWhenEmpty = true;
    }

    public SSoundGroup() {
        if (this.debug()) {
            this.debugShape = new RShape();
            this.debugShape.setMaterial(new Material());
            this.debugShape.getMaterial().setShader(new Shader());
            this.debugShape.getMaterial().getShader().addPass(new ShaderPass());
            this.debugShape.setRender(true);
        }

        this.gain = 1.0F;
        this.loopCount = 1;
        this.disposeWhenEmpty = true;
    }

    private boolean debug() {
        return false;
    }

    public void removeBuffer(SoundBuffer var1) {
        this.buffers.remove(var1);
    }

    public void addBuffer(SoundBuffer var1) {
        if (var1 == null) {
            System.out.println("Trying to_q add all NULL buffer in object " + this.name);
        }

        this.buffers.add(var1);
        this.firstFrame = true;
    }

    public SoundBuffer getBuffer(int var1) {
        return (SoundBuffer) this.buffers.get(var1);
    }

    public int bufferCount() {
        return this.buffers.size();
    }

    public void setBuffer(int var1, SoundBuffer var2) {
        this.addBuffer(var2);
    }

    public void setSource(int var1, SSourceElement var2) {
        this.addSource(var2);
    }

    public SSourceElement getSource(int var1) {
        int var2 = 0;

        for (Iterator var4 = this.sources.keySet().iterator(); var4.hasNext(); ++var2) {
            SSourceElement var3 = (SSourceElement) var4.next();
            if (var2 == var1) {
                return var3;
            }
        }

        return null;
    }

    public void removeSource(SSourceElement var1) {
        this.sources.remove(var1);
    }

    public void addSource(SSourceElement var1) {
        SSoundSource var2 = new SSoundSource();
        this.sources.put(var1, var2);
        this.addChild(var2);
    }

    public void releaseReferences() {
        super.releaseReferences();
        Iterator var2 = this.sources.values().iterator();

        while (var2.hasNext()) {
            SSoundSource var1 = (SSoundSource) var2.next();
            var1.releaseReferences();
        }

        var2 = this.buffers.iterator();

        while (var2.hasNext()) {
            SoundBuffer var3 = (SoundBuffer) var2.next();
            var3.releaseReferences();
        }

    }

    public int getLoopCount() {
        return this.loopCount;
    }

    public void setLoopCount(int var1) {
        this.loopCount = var1;
        Iterator var3 = this.sources.values().iterator();

        while (var3.hasNext()) {
            SSoundSource var2 = (SSoundSource) var3.next();
            var2.setLoopCount(var1);
        }

    }

    public float getGain() {
        return this.gain;
    }

    public void setGain(float var1) {
        this.gain = var1;
        Iterator var3 = this.sources.values().iterator();

        while (var3.hasNext()) {
            SSoundSource var2 = (SSoundSource) var3.next();
            var2.setGain(var1);
        }

    }

    public RenderAsset cloneAsset() {
        return new SSoundGroup(this);
    }

    public boolean isNeedToStep() {
        return true;
    }

    public int step(StepContext var1) {
        if (this.buffers.size() <= 0) {
            return 0;
        } else {
            if (this.firstFrame) {
                this.play();
                this.firstFrame = false;
            }

            int var2 = 0;
            Iterator var4 = this.sources.values().iterator();

            while (true) {
                SSoundSource var3;
                int var5;
                do {
                    if (!var4.hasNext()) {
                        if (var2 < this.sources.size()) {
                            return 0;
                        }

                        var4 = this.getChildren().iterator();

                        while (var4.hasNext()) {
                            SceneObject var6 = (SceneObject) var4.next();
                            var6.setRender(false);
                            var6.releaseReferences();
                        }

                        this.dispose();
                        return 1;
                    }

                    var3 = (SSoundSource) var4.next();
                    var5 = var3.step(var1);
                } while (var3.isAlive() && var5 != 1);

                var3.releaseReferences();
                var3.dispose();
                ++var2;
            }
        }
    }

    public boolean play() {
        boolean var1 = false;
        if (this.debug()) {
            this.addChild(this.debugShape);
        }

        ArrayList var2 = new ArrayList(this.buffers);
        Iterator var4 = this.sources.entrySet().iterator();

        while (var4.hasNext()) {
            Entry var3 = (Entry) var4.next();
            SSoundSource var5 = (SSoundSource) var3.getValue();
            SSourceElement var6 = (SSourceElement) var3.getKey();
            if (var5 != null && !var5.isPlaying() && !var5.isDisposed()) {
                SoundBuffer var7 = null;
                int var8;
                if (var2.size() > 0) {
                    var8 = (int) Math.floor(Math.random() * (double) var2.size());
                    var7 = (SoundBuffer) var2.get(var8);
                    var2.remove(var7);
                } else if (this.buffers.size() > 0) {
                    var8 = (int) Math.floor(Math.random() * (double) this.buffers.size());
                    var7 = (SoundBuffer) this.buffers.get(var8);
                }

                if (var7 != null) {
                    var5.setBuffer(var7);
                    var5.setIs3d(true);
                    if (this.mirrorX) {
                        Vector3dOperations var10 = new Vector3dOperations(var6.getPosition());
                        mirrorXTransform.a(var10);
                        var5.transform.position.aA(var10);
                    } else {
                        var5.transform.position.aA(var6.getPosition());
                    }

                    if (this.debug()) {
                        RShape var11 = new RShape(this.debugShape);
                        if (this.mirrorX) {
                            Vector3dOperations var9 = new Vector3dOperations(var6.getPosition());
                            mirrorXTransform.a(var9);
                            var11.transform.position.aA(var9);
                        } else {
                            var11.transform.position.aA(var6.getPosition());
                        }

                        var11.setScaling(2.0F);
                        var11.setRender(true);
                        var11.setPrimitiveColor(1.0F, 0.0F, 0.0F, 0.0F);
                        this.addChild(var11);
                    }

                    var5.setLoopCount(this.loopCount);
                    var5.setGain(this.gain);
                    var5.setDistances(var6.getDistances());
                    var1 = true;
                }
            }
        }

        return var1;
    }

    public void reset() {
        this.play();
    }

    public boolean stop() {
        boolean var1 = true;

        SSoundSource var2;
        for (Iterator var3 = this.sources.values().iterator(); var3.hasNext(); var1 &= var2.stop()) {
            var2 = (SSoundSource) var3.next();
        }

        return var1;
    }

    public void onRemove() {
        super.onRemove();
        Iterator var2 = this.sources.values().iterator();

        while (var2.hasNext()) {
            SSoundSource var1 = (SSoundSource) var2.next();
            var1.releaseReferences();
        }

    }

    public void dispose() {
        Iterator var2 = this.sources.values().iterator();

        while (var2.hasNext()) {
            SSoundSource var1 = (SSoundSource) var2.next();
            var1.releaseReferences();
            var1.dispose();
        }

        this.sources.clear();
        this.buffers.clear();
        this.disposed = true;
    }

    public boolean isAlive() {
        return this.isPlaying();
    }

    public boolean isPlaying() {
        Iterator var2 = this.sources.values().iterator();

        SSoundSource var1;
        do {
            if (!var2.hasNext()) {
                return false;
            }

            var1 = (SSoundSource) var2.next();
        } while (var1 == null || !var1.isPlaying());

        return true;
    }

    public boolean isValid() {
        Iterator var2 = this.sources.values().iterator();

        SSoundSource var1;
        do {
            if (!var2.hasNext()) {
                return false;
            }

            var1 = (SSoundSource) var2.next();
        } while (var1 == null || !var1.isValid());

        return true;
    }

    public int getTotalMilesSounds() {
        int var1 = 0;

        SSoundSource var2;
        for (Iterator var3 = this.sources.values().iterator(); var3.hasNext(); var1 += var2.getTotalMilesSounds()) {
            var2 = (SSoundSource) var3.next();
        }

        return var1;
    }

    public void setSquaredSoundSource(SceneObject var1) {
        throw new RuntimeException("Must implement this");
    }

    public boolean fadeOut(float var1) {
        boolean var2 = false;

        SSoundSource var3;
        for (Iterator var4 = this.sources.values().iterator(); var4.hasNext(); var2 |= var3.fadeOut(var1)) {
            var3 = (SSoundSource) var4.next();
        }

        return var2;
    }

    public void setMirrorX(boolean var1) {
        this.mirrorX = var1;
    }

    public void updateInternalGeometry(SceneObject var1) {
        super.updateInternalGeometry(var1);
        Iterator var3 = this.sources.values().iterator();

        while (var3.hasNext()) {
            SSoundSource var2 = (SSoundSource) var3.next();
            var2.updateInternalGeometry(this);
        }

    }

    public void setDistances(aQKa var1) {
        Iterator var3 = this.sources.keySet().iterator();

        while (var3.hasNext()) {
            SSourceElement var2 = (SSourceElement) var3.next();
            var2.setDistances(var1);
        }

    }

    public void setDistances(aQKa var1, SSoundSource var2) {
        Iterator var4 = this.sources.values().iterator();

        while (var4.hasNext()) {
            SSoundSource var3 = (SSoundSource) var4.next();
            if (var3.getName() == var2.getName()) {
                var3.setDistances(var1);
                break;
            }
        }

    }
}
