package taikodom.render.scene;

import all.Vector3dOperations;
import all.aLH;
import all.gu_g;
import taikodom.render.RenderContext;
import taikodom.render.loader.RenderAsset;
import taikodom.render.loader.SceneLoader;
import taikodom.render.primitives.Mesh;
import taikodom.render.raytrace.RayTraceSceneObjectInfoD;
import taikodom.render.textures.BaseTexture;

import javax.vecmath.Vector3d;

/**
 * Реализация класса указанного в имени тегов из файлов с расширением .pro
 */
public class RMesh extends RenderObject {
    protected Mesh mesh;
    protected float minimumScreenSize;

    public RMesh() {
    }

    protected RMesh(RMesh var1) {
        super(var1);
        this.mesh = (Mesh) this.smartClone(var1.mesh);
        this.minimumScreenSize = var1.minimumScreenSize;
    }

    public Mesh getMesh() {
        return this.mesh;
    }

    public void setMesh(Mesh var1) {
        this.mesh = var1;
        this.localAabb.g(var1.getAabb());
        this.computeWorldSpaceAABB();
    }

    public float getMinimumScreenSize() {
        return this.minimumScreenSize;
    }

    public void setMinimumScreenSize(float var1) {
        this.minimumScreenSize = var1;
    }

    public boolean render(RenderContext var1, boolean var2) {
        if (this.render) {
            if (!super.render(var1, var2)) {
                return false;
            } else {
                float var3 = var1.getSizeOnScreen(this.globalTransform.position, this.aabbWSLenght);
                if ((!var2 || var1.getFrustum().a(this.aabbWorldSpace)) && (!var1.isCullOutSmallObjects() || var3 > 5.0F)) {
                    if (this.material == null) {
                        return false;
                    }

                    for (int var4 = 0; var4 < this.material.textureCount(); ++var4) {
                        BaseTexture var5 = this.material.getTexture(var4);
                        if (var5 != null) {
                            var5.addArea(var3);
                        }
                    }

                    var1.addPrimitive(this.material, this.mesh, this.globalTransform, this.primitiveColor, this.occlusionQuery, this.renderPriority, this, this.aabbWorldSpace, this.currentSquaredDistanceToCamera);
                    var1.incMeshesRendered();
                }

                return true;
            }
        } else {
            return false;
        }
    }

    public RenderAsset cloneAsset() {
        return new RMesh(this);
    }

    public aLH getAABB() {
        return this.mesh != null ? this.mesh.getAabb() : this.localAabb;
    }

    public aLH computeWorldSpaceAABB() {
        if (this.mesh != null) {
            this.mesh.getAabb().a(this.globalTransform, this.aabbWorldSpace);
        }

        this.addChildrenWorldSpaceAABB();
        this.aabbWSLenght = this.aabbWorldSpace.din().ax(this.aabbWorldSpace.dim());
        return this.aabbWorldSpace;
    }

    public aLH computeLocalSpaceAABB() {
        super.computeLocalSpaceAABB();
        if (this.mesh != null) {
            this.localAabb.h(this.mesh.getAabb());
        }

        return this.localAabb;
    }

    public RayTraceSceneObjectInfoD rayIntersects(RayTraceSceneObjectInfoD var1, boolean var2, boolean var3) {
        var1.setChanged(false);
        super.rayIntersects(var1, var2, var3);
        if (var1.isIntersected() && var1.isChanged()) {
            return var1;
        } else if (this.mesh == null) {
            return var1;
        } else {
            Vector3dOperations var4 = new Vector3dOperations();
            Vector3dOperations var5 = new Vector3dOperations();
            var4.sub(var1.getStartPos(), this.globalTransform.position);
            var5.sub(var1.getEndPos(), this.globalTransform.position);
            this.globalTransform.mX.a((Vector3d) var4);
            this.globalTransform.mX.a((Vector3d) var5);
            gu_g var6 = this.mesh.rayIntersects(var4.getVec3f(), var5.getVec3f());
            if (var6.isIntersected() && (!var2 || (double) var6.vM() < var1.getParamIntersection())) {
                var1.set(var6);
                var1.setChanged(true);
                var1.setIntersectedObject(this);
            }

            return var1;
        }
    }

    public void validate(SceneLoader var1) {
        super.validate(var1);
        if (this.mesh == null) {
            throw new RuntimeException("No 'mesh' property for " + this.name);
        }
    }
}
