package taikodom.render.scene;

public class VolumeInterpolator {
    float currentTime = 0.0F;
    float totalTime = 1.0F;
    float result = 0.0F;
    float initVal = 0.0F;
    float finalVal = 1.0F;
    SMusic music = null;

    boolean step(float var1) {
        float var2 = this.currentTime / this.totalTime;
        this.result = this.initVal + var2 * (this.finalVal - this.initVal);
        this.currentTime += var1;
        if (this.currentTime > this.totalTime) {
            this.result = this.finalVal;
            return false;
        } else {
            return true;
        }
    }

    void init(float var1, float var2, float var3, SMusic var4) {
        this.music = var4;
        this.initVal = var1;
        this.finalVal = var2;
        this.totalTime = var3;
        this.result = var1;
        this.currentTime = 0.0F;
        if (this.totalTime <= 0.0F) {
            this.totalTime = 1.0E-4F;
        }

    }

    float getValue() {
        return this.result;
    }

    SMusic getMusic() {
        return this.music;
    }
}
