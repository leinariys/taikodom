package taikodom.render.scene;

import all.aLH;
import taikodom.render.RenderContext;
import taikodom.render.helpers.RenderVisitor;
import taikodom.render.loader.RenderAsset;
import taikodom.render.raytrace.RayTraceSceneObjectInfoD;

import java.util.Iterator;

public class RLod extends SceneObject {
    protected SceneObject objectLod0 = null;
    protected SceneObject objectLod1 = null;
    protected SceneObject objectLod2 = null;
    protected SceneObject currentLodObject = null;
    protected float distanceLod0;
    protected float squaredDistanceLod0;
    protected float distanceLod1;
    protected float squaredDistanceLod1;
    protected float distanceLod2;
    protected float squaredDistanceLod2;

    public RLod() {
    }

    protected RLod(RLod var1) {
        super(var1);
        this.objectLod0 = (SceneObject) this.smartClone(var1.objectLod0);
        this.objectLod1 = (SceneObject) this.smartClone(var1.objectLod1);
        this.objectLod2 = (SceneObject) this.smartClone(var1.objectLod2);
        this.setDistanceLod0(var1.distanceLod0);
        this.setDistanceLod1(var1.distanceLod1);
        this.setDistanceLod2(var1.distanceLod2);
    }

    public void setDisposeWhenEmpty(boolean var1) {
        super.setDisposeWhenEmpty(var1);
        if (this.objectLod0 != null) {
            this.objectLod0.setDisposeWhenEmpty(var1);
        }

        if (this.objectLod1 != null) {
            this.objectLod1.setDisposeWhenEmpty(var1);
        }

        if (this.objectLod2 != null) {
            this.objectLod2.setDisposeWhenEmpty(var1);
        }

    }

    public boolean isDisposed() {
        boolean var1 = true;
        if (this.objectLod0 != null && !this.objectLod0.isDisposed()) {
            var1 = false;
        }

        if (this.objectLod1 != null && !this.objectLod1.isDisposed()) {
            var1 = false;
        }

        if (this.objectLod2 != null && !this.objectLod2.isDisposed()) {
            var1 = false;
        }

        return this.disposed || var1;
    }

    public void checkEmpty() {
        boolean var1 = true;
        if (this.objectLod0 != null && !this.objectLod0.isDisposed()) {
            var1 = false;
        }

        if (this.objectLod1 != null && !this.objectLod1.isDisposed()) {
            var1 = false;
        }

        if (this.objectLod2 != null && !this.objectLod2.isDisposed()) {
            var1 = false;
        }

        if (this.objects.size() == 0 && this.disposeWhenEmpty && var1) {
            this.dispose();
        }

    }

    public void onRemove() {
        super.onRemove();
        if (this.objectLod0 != null) {
            this.objectLod0.onRemove();
        }

        if (this.objectLod1 != null) {
            this.objectLod1.onRemove();
        }

        if (this.objectLod2 != null) {
            this.objectLod2.onRemove();
        }

    }

    public boolean render(RenderContext var1, boolean var2) {
        if (this.render) {
            if (!super.render(var1, var2)) {
                return false;
            } else {
                float var3 = var1.getCamera().getTanHalfFovY() / 0.75F;
                var3 /= 1.0F + var1.getSceneViewQuality().getLodQuality() * 0.5F;
                double var4 = this.globalTransform.g(var1.getCamera().getTransform());
                var4 /= (double) var3;
                SceneObject var6 = this.objectLod2;
                if (var4 < (double) this.squaredDistanceLod0) {
                    var6 = this.objectLod0;
                } else if (var4 < (double) this.squaredDistanceLod1) {
                    var6 = this.objectLod1;
                }

                if (var6 == null) {
                    return true;
                } else {
                    if (this.currentLodObject != var6) {
                        this.currentLodObject = var6;
                        this.currentLodObject.updateInternalGeometry(this);
                    }

                    this.currentLodObject.render(var1, var2);
                    return true;
                }
            }
        } else {
            return false;
        }
    }

    public RenderAsset cloneAsset() {
        return new RLod(this);
    }

    public SceneObject getObjectLod0() {
        return this.objectLod0;
    }

    public void setObjectLod0(SceneObject var1) {
        this.objectLod0 = var1;
        this.updateInternalGeometry((SceneObject) null);
    }

    public SceneObject getObjectLod1() {
        return this.objectLod1;
    }

    public void setObjectLod1(SceneObject var1) {
        this.objectLod1 = var1;
        this.updateInternalGeometry((SceneObject) null);
    }

    public SceneObject getObjectLod2() {
        return this.objectLod2;
    }

    public void setObjectLod2(SceneObject var1) {
        this.objectLod2 = var1;
        this.updateInternalGeometry((SceneObject) null);
    }

    public float getDistanceLod0() {
        return this.distanceLod0;
    }

    public void setDistanceLod0(float var1) {
        this.distanceLod0 = var1;
        this.squaredDistanceLod0 = var1 * var1;
    }

    public float getDistanceLod1() {
        return this.distanceLod1;
    }

    public void setDistanceLod1(float var1) {
        this.distanceLod1 = var1;
        this.squaredDistanceLod1 = var1 * var1;
    }

    public float getDistanceLod2() {
        return this.distanceLod2;
    }

    public void setDistanceLod2(float var1) {
        this.distanceLod2 = var1;
        this.squaredDistanceLod2 = var1 * var1;
    }

    public aLH getAABB() {
        return this.localAabb;
    }

    public aLH computeWorldSpaceAABB() {
        this.aabbWorldSpace.reset();
        if (this.currentLodObject != null) {
            this.currentLodObject.updateInternalGeometry(this);
            this.aabbWorldSpace.h(this.objectLod0.getAabbWorldSpace());
        } else {
            if (this.objectLod0 != null) {
                this.objectLod0.updateInternalGeometry(this);
                this.aabbWorldSpace.h(this.objectLod0.getAabbWorldSpace());
            }

            if (this.objectLod1 != null) {
                this.objectLod1.updateInternalGeometry(this);
                this.aabbWorldSpace.h(this.objectLod1.getAabbWorldSpace());
            }

            if (this.objectLod2 != null) {
                this.objectLod2.updateInternalGeometry(this);
                this.aabbWorldSpace.h(this.objectLod2.getAabbWorldSpace());
            }
        }

        this.addChildrenWorldSpaceAABB();
        this.aabbWSLenght = this.aabbWorldSpace.din().ax(this.aabbWorldSpace.dim());
        return this.aabbWorldSpace;
    }

    public aLH computeLocalSpaceAABB() {
        super.computeLocalSpaceAABB();
        if (this.objectLod0 != null) {
            this.localAabb.h(this.objectLod0.computeLocalSpaceAABB());
        }

        this.aabbLSLenght = this.localAabb.din().ax(this.localAabb.dim());
        return this.localAabb;
    }

    public RayTraceSceneObjectInfoD rayIntersects(RayTraceSceneObjectInfoD var1, boolean var2, boolean var3) {
        var1.setChanged(false);
        super.rayIntersects(var1, var2, var3);
        if (var1.isIntersected() && var1.isChanged()) {
            return var1;
        } else {
            SceneObject[] var4 = new SceneObject[]{this.currentLodObject, this.objectLod2, this.objectLod1, this.objectLod0};

            for (int var5 = 0; var5 < var4.length; ++var5) {
                if (var4[var5] != null && (var5 <= 0 || var4[var5] != this.currentLodObject)) {
                    var1.setChanged(false);
                    var4[var5].rayIntersects(var1, var2, var3);
                    if (var1.isIntersected() && var1.isChanged()) {
                        var1.setIntersectedObject(this);
                        return var1;
                    }
                }
            }

            return var1;
        }
    }

    public boolean visitPreOrder(RenderVisitor var1) {
        if (!var1.visit(this)) {
            return false;
        } else if (this.objectLod0 != null && !this.objectLod0.visitPreOrder(var1)) {
            return false;
        } else if (this.objectLod1 != null && !this.objectLod1.visitPreOrder(var1)) {
            return false;
        } else if (this.objectLod2 != null && !this.objectLod2.visitPreOrder(var1)) {
            return false;
        } else {
            Iterator var3 = this.objects.iterator();

            while (var3.hasNext()) {
                SceneObject var2 = (SceneObject) var3.next();
                if (!var2.visitPreOrder(var1)) {
                    return false;
                }
            }

            return true;
        }
    }

    public void releaseReferences() {
        super.releaseReferences();
        if (this.objectLod0 != null) {
            this.objectLod0.releaseReferences();
        }

        if (this.objectLod1 != null) {
            this.objectLod1.releaseReferences();
        }

        if (this.objectLod2 != null) {
            this.objectLod2.releaseReferences();
        }

        this.objectLod0 = null;
        this.objectLod1 = null;
        this.objectLod2 = null;
    }
}
