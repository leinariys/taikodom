package taikodom.render.scene;

import com.hoplon.geometry.Vec3f;
import taikodom.render.SoundBuffer;
import taikodom.render.StepContext;

public abstract class SoundSource {
    public static int numSoundSources;

    public static int getNumSoundSources() {
        return numSoundSources;
    }

    public boolean setBuffer(SoundBuffer var1) {
        return false;
    }

    public SoundBuffer getBuffer() {
        return null;
    }

    public boolean isValid() {
        return false;
    }

    public boolean create() {
        return false;
    }

    public abstract boolean play(int var1);

    public boolean setLoop(int var1) {
        return false;
    }

    public boolean stop() {
        return false;
    }

    public boolean resume() {
        return false;
    }

    public void setPosition(float var1, float var2, float var3) {
    }

    public abstract void setPosition(Vec3f var1);

    public abstract void setVelocity(Vec3f var1);

    public boolean setGain(float var1) {
        return false;
    }

    public boolean setPitch(float var1) {
        return false;
    }

    public boolean isPlaying() {
        return false;
    }

    public boolean fadeOut(float var1) {
        return false;
    }

    public boolean setMinMaxDistance(float var1, float var2) {
        return false;
    }

    public int getTotalMilesSounds() {
        return 0;
    }

    public int getSamplePositionMs() {
        return 0;
    }

    public void setSamplePositionMs(int var1) {
    }

    public boolean releaseSample() {
        return false;
    }

    public void releaseReferences() {
    }

    public int step(StepContext var1) {
        return 0;
    }

    public float getTimeLenght() {
        return 0.0F;
    }
}
