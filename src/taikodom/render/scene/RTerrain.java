package taikodom.render.scene;

import all.LogPrinter;
import all.Vector3dOperations;
import com.hoplon.geometry.Vec3f;
import com.sun.opengl.util.texture.TextureData;
import taikodom.render.RenderContext;
import taikodom.render.data.ExpandableIndexData;
import taikodom.render.data.ExpandableVertexData;
import taikodom.render.data.VertexLayout;
import taikodom.render.enums.GeometryType;
import taikodom.render.loader.RenderAsset;
import taikodom.render.primitives.Mesh;
import taikodom.render.primitives.OcclusionQuery;
import taikodom.render.textures.Texture;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * Реализация класса указанного в имени тегов из файлов с расширением .pro
 */
public class RTerrain extends RenderObject {
    private static final int PATCH_SIZE = 33;
    private static final int PATCH_COUNT = 32;
    private static final LogPrinter logger = LogPrinter.K(RTerrain.class);
    RTerrain.b[][] patches;
    private boolean inited = false;
    private Texture heightmap = null;
    private Vec3f scale = new Vec3f(1.0F, 1.0F, 1.0F);

    public RTerrain() {
    }

    protected RTerrain(RTerrain var1) {
        super(var1);
        this.patches = var1.patches;
        this.inited = var1.inited;
        this.heightmap = var1.heightmap;
        this.scale = var1.scale;
    }

    public boolean render(RenderContext var1, boolean var2) {
        if (!this.render) {
            return false;
        } else if (!super.render(var1, var2)) {
            return false;
        } else {
            if (!this.inited) {
                this.init();
            }

            for (int var3 = 0; var3 < this.patches.length; ++var3) {
                for (int var4 = 0; var4 < this.patches[var3].length; ++var4) {
                    RTerrain.b var5 = this.patches[var3][var4];
                    float var6 = var1.getCamera().getPosition().aC(var5.center);
                    byte var7 = 0;
                    if (var6 > 1000000.0F) {
                        var7 = 5;
                    } else if (var6 > 500000.0F) {
                        var7 = 4;
                    } else if (var6 > 100000.0F) {
                        var7 = 3;
                    } else if (var6 > 200000.0F) {
                        var7 = 2;
                    } else if (var6 > 50000.0F) {
                        var7 = 1;
                    }

                    var1.addPrimitive(this.material, ((RTerrain.a) var5.fMX.get(var7)).mesh, this.transform, this.primitiveColor, (OcclusionQuery) null, this.renderPriority, this, this.aabbWorldSpace, this.currentSquaredDistanceToCamera);
                }
            }

            return true;
        }
    }

    private Vec3f nearestPoint(Vec3f var1, Vec3f var2, Vec3f var3) {
        Vec3f var4 = var3.j(var1);
        Vec3f var5 = var2.j(var1).dfO();
        return var1.i(var5.mS(var4.dot(var5)));
    }

    public void init() {
        this.inited = true;
        if (this.heightmap == null) {
            logger.warn("Missing heightmap for " + this);
        } else {
            TextureData var1 = this.heightmap.getTextureData(0);
            Buffer var2 = var1.getBuffer();
            Class var3 = var2 instanceof FloatBuffer ? Float.class : (var2 instanceof ByteBuffer ? Byte.class : null);
            if (var3 == null) {
                throw new RuntimeException("Unsupported buffer type " + var2.getClass());
            } else {
                int var4 = var1.getWidth() + 1;
                int var5 = var1.getHeight() + 1;
                ExpandableVertexData var6 = new ExpandableVertexData(new VertexLayout(true, false, false, false, 0), var4 * var5);
                int var7 = 0;

                int var10;
                int var12;
                for (int var8 = 0; var8 < var1.getHeight(); ++var8) {
                    float var9 = 0.0F;

                    for (var10 = 0; var10 < var1.getWidth(); ++var10) {
                        if (var3 == Float.class) {
                            var9 = ((FloatBuffer) var2).get();
                        } else if (var3 == Byte.class) {
                            ByteBuffer var11 = (ByteBuffer) var2;
                            var12 = var11.get() & 255;
                            var11.get();
                            var11.get();
                            var9 = (float) var12;
                        }

                        var6.setPosition(var7++, (float) var10 * this.scale.x, var9 * this.scale.y, (float) var8 * this.scale.z);
                    }

                    var6.setPosition(var7++, (float) var1.getWidth() * this.scale.x, var9 * this.scale.y, (float) var8 * this.scale.z);
                }

                float var27 = 0.0F;

                int var28;
                for (var28 = 0; var28 < var1.getWidth(); ++var28) {
                    var6.setPosition(var7++, (float) var28 * this.scale.x, var27 * this.scale.y, (float) var1.getHeight() * this.scale.z);
                }

                var6.setPosition(var7++, (float) var1.getWidth() * this.scale.x, var27 * this.scale.y, (float) var1.getHeight() * this.scale.z);
                var28 = var1.getHeight() / 32;
                var10 = var1.getWidth() / 32;
                this.patches = new RTerrain.b[var28][var10];

                for (int var29 = 0; var29 < var28; ++var29) {
                    for (var12 = 0; var12 < var10; ++var12) {
                        RTerrain.b var13 = new RTerrain.b((RTerrain.b) null);
                        var13.center = new Vector3dOperations((double) ((float) (var12 * 32 + 16) * this.scale.x), 0.0D, (double) ((float) (var29 * 32 + 16) * this.scale.z));
                        int var14 = 1;

                        for (int var15 = 32; var15 > 0; var14 *= 2) {
                            Mesh var16 = new Mesh();
                            var16.setGeometryType(GeometryType.TRIANGLES);
                            var16.setVertexData(var6);
                            ExpandableIndexData var17 = new ExpandableIndexData(var15 * var15 * 2);
                            var16.setIndexes(var17);
                            int var18 = 0;

                            for (int var19 = 0; var19 < 32; var19 += var14) {
                                for (int var20 = 0; var20 < 32; var20 += var14) {
                                    int var21 = var12 * 32 + var20;
                                    int var22 = var29 * 32 + var19;
                                    int var23 = var21 + var22 * var4;
                                    int var24 = var23 + var14;
                                    int var25 = var23 + var4 * var14;
                                    int var26 = var23 + var4 * var14 + var14;
                                    var17.setIndex(var18++, var23);
                                    var17.setIndex(var18++, var25);
                                    var17.setIndex(var18++, var24);
                                    var17.setIndex(var18++, var24);
                                    var17.setIndex(var18++, var25);
                                    var17.setIndex(var18++, var26);
                                }
                            }

                            var17.compact();
                            RTerrain.a var30 = new RTerrain.a((RTerrain.a) null);
                            var30.mesh = var16;
                            var13.fMX.add(var30);
                            var15 /= 2;
                        }

                        this.patches[var29][var12] = var13;
                    }
                }

            }
        }
    }

    public void setScale(Vec3f var1) {
        this.scale = var1;
    }

    public Texture getHeightmap() {
        return this.heightmap;
    }

    public void setHeightmap(Texture var1) {
        this.heightmap = var1;
    }

    public RenderAsset cloneAsset() {
        RTerrain var1 = new RTerrain(this);
        return var1;
    }

    private class a {
        Mesh mesh;
        float awF;

        private a() {
        }

        // $FF: synthetic method
        a(RTerrain.a var2) {
            this();
        }
    }

    private class b {
        List fMX;
        Vector3dOperations center;

        private b() {
            this.fMX = new ArrayList();
        }

        // $FF: synthetic method
        b(RTerrain.b var2) {
            this();
        }
    }
}
