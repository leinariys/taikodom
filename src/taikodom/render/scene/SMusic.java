package taikodom.render.scene;

import taikodom.render.MilesSoundStream;
import taikodom.render.StepContext;
import taikodom.render.loader.RenderAsset;

/**
 * Реализация класса указанного в имени тегов из файлов с расширением .pro
 */
public class SMusic extends SoundObject {
    float loopDelay;
    int loopCount;
    float gain;
    float pitch;
    boolean isPaused;
    boolean shouldPlay;
    MilesSoundStream buffer = null;
    private float accum = 0.0F;
    private boolean onHold = false;

    public SMusic() {
        this.loopCount = 0;
        this.loopDelay = -1.0F;
        this.gain = 1.0F;
        this.pitch = 1.0F;
        this.isPaused = false;
        this.shouldPlay = false;
    }

    protected SMusic(SMusic var1) {
        this.loopCount = var1.loopCount;
        this.gain = var1.gain;
        this.pitch = var1.pitch;
        this.isPaused = false;
        this.setBuffer(var1.buffer);
    }

    public void releaseReferences() {
        super.releaseReferences();
        if (this.buffer != null) {
            this.buffer.releaseReferences();
            this.buffer = null;
        }

    }

    public void dispose() {
        this.stop();
        this.disposed = true;
    }

    public int getLoopCount() {
        return this.loopCount;
    }

    public void setLoopCount(int var1) {
        if (var1 == 0) {
            this.loopDelay = -1.0F;
        }

        this.loopCount = var1;
        if (this.buffer != null) {
            this.buffer.setLoopCount(this.loopCount);
        }

    }

    public float getLoopDelay() {
        return this.loopDelay;
    }

    public void setLoopDelay(float var1) {
        this.setLoopCount(1);
        this.loopDelay = var1;
    }

    public float getGain() {
        return this.gain;
    }

    public void setGain(float var1) {
        this.gain = var1;
        this.buffer.setGain(var1);
    }

    public boolean play() {
        if (this.buffer == null) {
            return false;
        } else {
            this.shouldPlay = true;
            if (this.isPaused) {
                this.isPaused = false;
                return this.buffer.play();
            } else if (!this.buffer.play(this.loopCount)) {
                return false;
            } else {
                this.buffer.setGain(this.gain);
                return true;
            }
        }
    }

    public boolean stop() {
        this.isPaused = false;
        this.shouldPlay = false;
        this.onHold = false;
        this.accum = 0.0F;
        if (this.buffer != null) {
            this.buffer.stop();
            return false;
        } else {
            return false;
        }
    }

    public boolean pause() {
        this.isPaused = true;
        if (this.buffer != null) {
            // this.buffer.pause();
            return true;
        } else {
            return false;
        }
    }

    public boolean isPlaying() {
        return this.buffer != null ? this.buffer.isPlaying() : false;
    }

    public boolean fadeOut(float var1) {
        this.onHold = false;
        this.accum = 0.0F;
        this.shouldPlay = false;
        return this.buffer != null ? this.buffer.fadeOut(var1) : false;
    }

    public boolean fadeIn(float var1) {
        this.play();
        return this.buffer != null ? this.buffer.fadeIn(var1) : false;
    }

    public MilesSoundStream getBuffer() {
        return this.buffer;
    }

    public void setBuffer(MilesSoundStream var1) {
        this.buffer = var1;
    }

    public boolean isAlive() {
        return this.isPlaying() || this.isPaused;
    }

    public int step(StepContext var1) {
        if (this.buffer != null && !this.isPaused && !this.onHold && this.buffer.step(var1) != 0) {
            if (this.loopDelay == -1.0F || !this.shouldPlay) {
                return 1;
            }

            if (this.shouldPlay) {
                this.accum = 0.0F;
                this.onHold = true;
                return 0;
            }
        }

        if (this.onHold && this.shouldPlay) {
            this.accum += var1.getDeltaTime();
            if (this.accum >= this.loopDelay) {
                this.accum = 0.0F;
                this.onHold = false;
                this.play();
            }
        }

        return 0;
    }

    public RenderAsset cloneAsset() {
        return this;
    }

    public int getSamplePositionMs() {
        return this.buffer == null ? 0 : this.buffer.getSamplePositionMs();
    }

    public void setSamplePositionMs(int var1) {
        if (this.buffer != null) {
            this.buffer.setSamplePositionMs(var1);
        }

    }
}
