package taikodom.render.scene;

import all.Vector3dOperations;
import all.aLH;
import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import taikodom.render.RenderContext;
import taikodom.render.StepContext;

public class RayBB extends RenderObject {
    RBillboard billboard;
    float lifeTime;
    float elapsedTime;
    float range;

    public RayBB(RBillboard var1, float var2, float var3) {
        this.billboard = var1;
        this.lifeTime = var2;
        this.range = var3;
    }

    public aLH buildAABB() {
        this.billboard.computeWorldSpaceAABB();
        return this.billboard.getTransformedAABB();
    }

    public void dispose() {
        this.billboard = null;
    }

    public int step(StepContext var1) {
        if (this.billboard != null) {
            this.billboard.step(var1);
        }

        return 1;
    }

    public boolean render(RenderContext var1, boolean var2) {
        if (this.billboard != null) {
            this.billboard.render(var1, var2);
        }

        return true;
    }

    public void setRay(RBillboard var1, float var2) {
        this.billboard = var1;
        this.lifeTime = var2;
    }

    float getLifeTime() {
        return this.lifeTime;
    }

    float getElapsedTime() {
        return this.elapsedTime;
    }

    void decLifeTime(float var1) {
        this.elapsedTime += var1;
        Color var2 = this.billboard.getPrimitiveColor();
        float var3 = 1.0F - this.elapsedTime / this.lifeTime;
        var2.w = var3;
        var2.x = var3;
        var2.y = var3;
        var2.z = var3;
        this.billboard.setPrimitiveColor(var2);
    }

    Color getColor() {
        return this.billboard.getPrimitiveColor();
    }

    void setColor(Color var1) {
        this.billboard.setPrimitiveColor(var1);
    }

    public void trigger(Vector3dOperations var1, Vec3f var2, float var3) {
        throw new RuntimeException("Must implement this");
    }
}
