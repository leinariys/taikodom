package taikodom.render.scene;


import all.Vector3dOperations;
import all.aLH;
import all.gu_g;
import taikodom.render.RenderContext;
import taikodom.render.data.ExpandableIndexData;
import taikodom.render.data.ExpandableVertexData;
import taikodom.render.data.VertexLayout;
import taikodom.render.enums.GeometryType;
import taikodom.render.enums.ShapeTypes;
import taikodom.render.loader.RenderAsset;
import taikodom.render.primitives.Mesh;
import taikodom.render.raytrace.RayTraceSceneObjectInfoD;
import taikodom.render.textures.BaseTexture;

import javax.vecmath.Vector3d;

/**
 * Реализация класса указанного в имени тегов из файлов с расширением .pro
 */
public class RShape extends RenderObject {
    // $FF: synthetic field
    private static int[] $SWITCH_TABLE$taikodom$render$enums$ShapeTypes;
    private Mesh mesh;
    private ExpandableVertexData vertexes;
    private ExpandableIndexData indexes;
    private float innerRadius;
    private float initialAngle;
    private float finalAngle;
    private float numSpiralTurns;
    private float spiralHeight;
    private float spiralWidth;
    private ShapeTypes shapeType;
    private int tesselation;

    public RShape() {
        this.shapeType = ShapeTypes.SHAPE_DISC;
        this.tesselation = 10;
        this.spiralWidth = 0.5F;
        this.numSpiralTurns = 1.0F;
        this.spiralHeight = 0.0F;
        this.innerRadius = 0.0F;
        this.initialAngle = 0.0F;
        this.finalAngle = 360.0F;
        this.tesselation = 1;
        this.initRenderStructs();
    }

    protected RShape(RShape var1) {
        super(var1);
        this.innerRadius = var1.innerRadius;
        this.initialAngle = var1.initialAngle;
        this.finalAngle = var1.finalAngle;
        this.numSpiralTurns = var1.numSpiralTurns;
        this.spiralHeight = var1.spiralHeight;
        this.spiralWidth = var1.spiralWidth;
        this.shapeType = var1.shapeType;
        this.tesselation = var1.tesselation;
        this.initRenderStructs();
        this.build();
    }

    // $FF: synthetic method
    static int[] $SWITCH_TABLE$taikodom$render$enums$ShapeTypes() {
        int[] var10000 = $SWITCH_TABLE$taikodom$render$enums$ShapeTypes;
        if ($SWITCH_TABLE$taikodom$render$enums$ShapeTypes != null) {
            return var10000;
        } else {
            int[] var0 = new int[ShapeTypes.values().length];

            try {
                var0[ShapeTypes.SHAPE_CUBE.ordinal()] = 4;
            } catch (NoSuchFieldError var5) {
                ;
            }

            try {
                var0[ShapeTypes.SHAPE_DISC.ordinal()] = 1;
            } catch (NoSuchFieldError var4) {
                ;
            }

            try {
                var0[ShapeTypes.SHAPE_RECTANGLE.ordinal()] = 3;
            } catch (NoSuchFieldError var3) {
                ;
            }

            try {
                var0[ShapeTypes.SHAPE_SPHERE.ordinal()] = 2;
            } catch (NoSuchFieldError var2) {
                ;
            }

            try {
                var0[ShapeTypes.SHAPE_SPIRAL.ordinal()] = 5;
            } catch (NoSuchFieldError var1) {
                ;
            }

            $SWITCH_TABLE$taikodom$render$enums$ShapeTypes = var0;
            return var0;
        }
    }

    public boolean render(RenderContext var1, boolean var2) {
        if (this.render) {
            if (!super.render(var1, var2)) {
                return false;
            } else {
                if (!var2 || var1.getFrustum().a(this.aabbWorldSpace)) {
                    float var3 = var1.getSizeOnScreen(this.globalTransform.position, this.aabbWSLenght);

                    for (int var4 = 0; var4 < this.material.textureCount(); ++var4) {
                        BaseTexture var5 = this.material.getTexture(var4);
                        if (var5 != null) {
                            var5.addArea(var3);
                        }
                    }

                    var1.addPrimitive(this.material, this.mesh, this.globalTransform, this.primitiveColor, this.occlusionQuery, this.renderPriority, this, this.aabbWorldSpace, this.currentSquaredDistanceToCamera);
                }

                return true;
            }
        } else {
            return false;
        }
    }

    public RenderAsset cloneAsset() {
        return new RShape(this);
    }

    public ShapeTypes getShapeType() {
        return this.shapeType;
    }

    public void setShapeType(ShapeTypes var1) {
        this.shapeType = var1;
        this.build();
    }

    public int getTesselation() {
        return this.tesselation;
    }

    public void setTesselation(int var1) {
        this.tesselation = var1;
        this.build();
    }

    public float getInnerRadius() {
        return this.innerRadius;
    }

    public void setInnerRadius(float var1) {
        this.innerRadius = var1;
        this.build();
    }

    public float getInitialAngle() {
        return this.initialAngle;
    }

    public void setInitialAngle(float var1) {
        this.initialAngle = var1;
        if (this.finalAngle < this.initialAngle) {
            this.finalAngle = this.initialAngle;
        }

        this.build();
    }

    public float getFinalAngle() {
        return this.finalAngle;
    }

    public void setFinalAngle(float var1) {
        this.finalAngle = var1;
        if (this.initialAngle > this.finalAngle) {
            this.initialAngle = this.finalAngle;
        }

        this.build();
    }

    public float getNumSpiralTurns() {
        return this.numSpiralTurns;
    }

    public void setNumSpiralTurns(float var1) {
        this.numSpiralTurns = var1;
        this.build();
    }

    public float getSpiralHeight() {
        return this.spiralHeight;
    }

    public void setSpiralHeight(float var1) {
        this.spiralHeight = var1;
        this.build();
    }

    public float getSpiralWidth() {
        return this.spiralWidth;
    }

    public void setSpiralWidth(float var1) {
        this.spiralWidth = var1;
        this.build();
    }

    private void initRenderStructs() {
        VertexLayout var1 = new VertexLayout(true, true, false, true, 1);
        this.vertexes = new ExpandableVertexData(var1, 100);
        this.indexes = new ExpandableIndexData(100);
        this.mesh = new Mesh();
        this.mesh.setName(this.getName() + "_mesh");
        this.mesh.setGeometryType(GeometryType.TRIANGLES);
        this.mesh.setVertexData(this.vertexes);
        this.mesh.setIndexes(this.indexes);
    }

    private void build() {
        switch ($SWITCH_TABLE$taikodom$render$enums$ShapeTypes()[this.shapeType.ordinal()]) {
            case 1:
                this.buildDisc();
                break;
            case 2:
                this.buildSphere();
                break;
            case 3:
                this.buildRectangle();
                break;
            case 4:
                this.buildCube();
                break;
            case 5:
                this.buildSpiral();
        }

        this.mesh.computeLocalAabb();
        this.computeWorldSpaceAABB();
    }

    private void buildDisc() {
        this.indexes.clear();
        this.mesh.setGeometryType(GeometryType.QUAD_STRIP);
        float var1 = 15.0F;
        float var2 = var1 / (float) this.tesselation;
        int var3 = 0;

        float var4;
        float var5;
        float var6;
        for (var4 = this.initialAngle; var4 < this.finalAngle; var4 += var2) {
            var5 = var4 / 360.0F;
            var6 = (float) (0.5D * Math.cos((double) var5 * 3.141592653589793D * 2.0D));
            float var7 = (float) (0.5D * Math.sin((double) var5 * 3.141592653589793D * 2.0D));
            this.indexes.setIndex(var3, var3);
            this.vertexes.setPosition(var3, var6 * this.innerRadius, var7 * this.innerRadius, 0.0F);
            this.vertexes.setTexCoord(var3, 0, 0.5F + var6 * this.innerRadius, 0.5F + var7 * this.innerRadius);
            this.vertexes.setNormal(var3, 0.0F, 0.0F, 1.0F);
            ++var3;
            this.indexes.setIndex(var3, var3);
            this.vertexes.setPosition(var3, var6, var7, 0.0F);
            this.vertexes.setTexCoord(var3, 0, 0.5F + var6, 0.5F + var7);
            this.vertexes.setNormal(var3, 0.0F, 0.0F, 1.0F);
            ++var3;
        }

        var4 = this.finalAngle / 360.0F;
        var5 = (float) (0.5D * Math.cos((double) var4 * 3.141592653589793D * 2.0D));
        var6 = (float) (0.5D * Math.sin((double) var4 * 3.141592653589793D * 2.0D));
        this.indexes.setIndex(var3, var3);
        this.vertexes.setPosition(var3, var5 * this.innerRadius, var6 * this.innerRadius, 0.0F);
        this.vertexes.setTexCoord(var3, 0, 0.5F + var5 * this.innerRadius, 0.5F + var6 * this.innerRadius);
        this.vertexes.setNormal(var3, 0.0F, 0.0F, 1.0F);
        ++var3;
        this.indexes.setIndex(var3, var3);
        this.vertexes.setPosition(var3, var5, var6, 0.0F);
        this.vertexes.setTexCoord(var3, 0, 0.5F + var5, 0.5F + var6);
        this.vertexes.setNormal(var3, 0.0F, 0.0F, 1.0F);
    }

    private void buildSphere() {
        this.indexes.clear();
        long var1 = 10L;
        long var3 = var1 * (long) this.tesselation;
        this.mesh.setGeometryType(GeometryType.QUADS);
        int var5 = 0;

        for (int var6 = 0; (long) var6 < var3; ++var6) {
            float var7 = (float) var6 / (float) var3;
            float var8 = (float) (var6 + 1) / (float) var3;
            float var9 = (float) (-0.5D * Math.cos((double) var7 * 3.141592653589793D));
            float var10 = (float) (-0.5D * Math.cos((double) var8 * 3.141592653589793D));
            float var11 = (float) Math.sin((double) var7 * 3.141592653589793D);
            float var12 = (float) Math.sin((double) var8 * 3.141592653589793D);

            for (int var13 = 0; (long) var13 < var3; ++var13) {
                float var14 = (float) var13 / (float) var3;
                float var15 = (float) (var13 + 1) / (float) var3;
                float var16 = (float) (0.5D * Math.cos((double) var14 * 3.141592653589793D * 2.0D));
                float var17 = (float) (0.5D * Math.cos((double) var15 * 3.141592653589793D * 2.0D));
                float var18 = (float) (0.5D * Math.sin((double) var14 * 3.141592653589793D * 2.0D));
                float var19 = (float) (0.5D * Math.sin((double) var15 * 3.141592653589793D * 2.0D));
                this.vertexes.setPosition(var5, var16 * var11, var9, var18 * var11);
                this.vertexes.setNormal(var5, 0.0F, 0.0F, 1.0F);
                this.vertexes.setTexCoord(var5, 0, 0.5F + var16, 0.5F + var9);
                this.indexes.setIndex(var5, var5);
                ++var5;
                this.vertexes.setPosition(var5, var16 * var12, var10, var18 * var12);
                this.vertexes.setNormal(var5, 0.0F, 0.0F, 1.0F);
                this.vertexes.setTexCoord(var5, 0, 0.5F + var16, 0.5F + var9);
                this.indexes.setIndex(var5, var5);
                ++var5;
                this.vertexes.setPosition(var5, var17 * var12, var10, var19 * var12);
                this.vertexes.setNormal(var5, 0.0F, 0.0F, 1.0F);
                this.vertexes.setTexCoord(var5, 0, 0.5F + var17, 0.5F + var10);
                this.indexes.setIndex(var5, var5);
                ++var5;
                this.vertexes.setPosition(var5, var17 * var11, var9, var19 * var11);
                this.vertexes.setNormal(var5, 0.0F, 0.0F, 1.0F);
                this.vertexes.setTexCoord(var5, 0, 0.5F + var17, 0.5F + var10);
                this.indexes.setIndex(var5, var5);
                ++var5;
            }
        }

    }

    private void buildRectangle() {
        this.indexes.clear();
        this.mesh.setGeometryType(GeometryType.QUADS);
        byte var1 = 0;
        this.vertexes.setPosition(var1, -0.5F, -0.5F, 0.0F);
        this.vertexes.setNormal(var1, 0.0F, 0.0F, 1.0F);
        this.vertexes.setTexCoord(var1, 0, 0.0F, 0.0F);
        this.indexes.setIndex(var1, var1);
        int var2 = var1 + 1;
        this.vertexes.setPosition(var2, 0.5F, -0.5F, 0.0F);
        this.vertexes.setNormal(var2, 0.0F, 0.0F, 1.0F);
        this.vertexes.setTexCoord(var2, 0, 1.0F, 0.0F);
        this.indexes.setIndex(var2, var2);
        ++var2;
        this.vertexes.setPosition(var2, 0.5F, 0.5F, 0.0F);
        this.vertexes.setNormal(var2, 0.0F, 0.0F, 1.0F);
        this.vertexes.setTexCoord(var2, 0, 1.0F, 1.0F);
        this.indexes.setIndex(var2, var2);
        ++var2;
        this.vertexes.setPosition(var2, -0.5F, 0.5F, 0.0F);
        this.vertexes.setNormal(var2, 0.0F, 0.0F, 1.0F);
        this.vertexes.setTexCoord(var2, 0, 0.0F, 1.0F);
        this.indexes.setIndex(var2, var2);
        ++var2;
    }

    private void buildCube() {
    }

    private void buildSpiral() {
        this.indexes.clear();
        this.mesh.setGeometryType(GeometryType.QUAD_STRIP);
        float var1 = 1.0F / ((float) this.tesselation * 10.0F);
        int var2 = 0;

        for (float var3 = 0.0F; var3 <= 1.0F; var3 += var1) {
            float var4 = var3 * this.numSpiralTurns;
            float var5 = 1.0F - var3;
            float var6 = var5 * this.spiralWidth;
            float var7 = (float) (0.5D * Math.cos((double) var4 * 3.141592653589793D * 2.0D));
            float var8 = (float) (0.5D * Math.sin((double) var4 * 3.141592653589793D * 2.0D));
            this.vertexes.setPosition(var2, var7 * var3, var8 * var3, var3 * this.spiralHeight);
            this.vertexes.setNormal(var2, 0.0F, 0.0F, 1.0F);
            this.vertexes.setTexCoord(var2, 0, 0.5F + var7 * var3, 0.5F + var8 * var3);
            this.indexes.setIndex(var2, var2);
            ++var2;
            this.vertexes.setPosition(var2, var7 * var3 + var7 * var6, var8 * var3 + var8 * var6, var3 * this.spiralHeight);
            this.vertexes.setNormal(var2, 0.0F, 0.0F, 1.0F);
            this.vertexes.setTexCoord(var2, 0, 0.5F + var7, 0.5F + var8);
            this.indexes.setIndex(var2, var2);
            ++var2;
        }

    }

    public aLH getAABB() {
        return this.mesh != null ? this.mesh.getAabb() : this.localAabb;
    }

    public aLH computeLocalSpaceAABB() {
        super.computeLocalSpaceAABB();
        if (this.mesh != null) {
            this.localAabb.h(this.mesh.getAabb());
        }

        return this.localAabb;
    }

    public aLH computeWorldSpaceAABB() {
        if (this.mesh != null) {
            this.mesh.getAabb().a(this.globalTransform, this.aabbWorldSpace);
        } else {
            this.aabbWorldSpace.reset();
        }

        this.addChildrenWorldSpaceAABB();
        this.aabbWSLenght = this.aabbWorldSpace.din().ax(this.aabbWorldSpace.dim());
        return this.aabbWorldSpace;
    }

    public void releaseReferences() {
        super.releaseReferences();
        this.indexes = null;
        this.vertexes = null;
        this.mesh = null;
    }

    public RayTraceSceneObjectInfoD rayIntersects(RayTraceSceneObjectInfoD var1, boolean var2, boolean var3) {
        var1.setChanged(false);
        super.rayIntersects(var1, var2, var3);
        if (var1.isIntersected() && var1.isChanged()) {
            return var1;
        } else if (this.mesh == null) {
            return var1;
        } else {
            Vector3dOperations var4 = new Vector3dOperations();
            Vector3dOperations var5 = new Vector3dOperations();
            var4.sub(var1.getStartPos(), this.globalTransform.position);
            var5.sub(var1.getEndPos(), this.globalTransform.position);
            this.globalTransform.mX.a((Vector3d) var4);
            this.globalTransform.mX.a((Vector3d) var5);
            gu_g var6 = this.mesh.rayIntersects(var4.getVec3f(), var5.getVec3f());
            if (var6.isIntersected() && (!var2 || (double) var6.vM() < var1.getParamIntersection())) {
                var1.set(var6);
                var1.setChanged(true);
                var1.setIntersectedObject(this);
            }

            return var1;
        }
    }
}
