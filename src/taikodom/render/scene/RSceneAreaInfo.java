package taikodom.render.scene;

import all.Vector3dOperations;
import all.aLH;
import com.hoplon.geometry.Color;
import taikodom.render.RenderContext;
import taikodom.render.enums.FogType;
import taikodom.render.loader.RenderAsset;
import taikodom.render.textures.Texture;

/**
 * Реализация класса указанного в имени тегов из файлов с расширением .pro
 */
public class RSceneAreaInfo extends SceneObject {
    protected final Color fogColor;
    protected final Color ambientColor;
    protected final aLH aabb;
    protected FogType fogType;
    protected float fogStart;
    protected float fogEnd;
    protected float fogExp;
    protected Texture diffuseCubemap;
    protected Texture reflectiveCubemap;

    public RSceneAreaInfo() {
        this.fogType = FogType.FOG_LINEAR;
        this.fogColor = new Color(0.0F, 0.0F, 0.0F, 0.0F);
        this.fogStart = 1.0F;
        this.fogEnd = 1000.0F;
        this.fogExp = 1.0F;
        this.ambientColor = new Color(0.0F, 0.0F, 0.0F, 0.0F);
        this.aabb = new aLH();
    }

    public RSceneAreaInfo(RSceneAreaInfo var1) {
        super(var1);
        this.fogType = FogType.FOG_LINEAR;
        this.fogColor = new Color(0.0F, 0.0F, 0.0F, 0.0F);
        this.fogStart = 1.0F;
        this.fogEnd = 1000.0F;
        this.fogExp = 1.0F;
        this.ambientColor = new Color(0.0F, 0.0F, 0.0F, 0.0F);
        this.aabb = new aLH();
        this.fogType = var1.fogType;
        this.fogColor.set(var1.fogColor);
        this.fogStart = var1.fogStart;
        this.fogEnd = var1.fogEnd;
        this.fogExp = var1.fogExp;
        this.ambientColor.set(var1.ambientColor);
        this.aabb.g(var1.aabb);
        this.diffuseCubemap = (Texture) this.smartClone(var1.diffuseCubemap);
        this.reflectiveCubemap = (Texture) this.smartClone(var1.reflectiveCubemap);
    }

    public FogType getFogType() {
        return this.fogType;
    }

    public void setFogType(FogType var1) {
        this.fogType = var1;
    }

    public Vector3dOperations getSize() {
        return this.aabb.din().Z(2.0D);
    }

    public void setSize(Vector3dOperations var1) {
        this.aabb.aD(var1.Z(-0.5D).dfZ());
        this.aabb.aD(var1.Z(0.5D).dfZ());
    }

    public Color getFogColor() {
        return this.fogColor;
    }

    public void setFogColor(Color var1) {
        this.fogColor.set(var1);
    }

    public float getFogStart() {
        return this.fogStart;
    }

    public void setFogStart(float var1) {
        this.fogStart = var1;
    }

    public float getFogEnd() {
        return this.fogEnd;
    }

    public void setFogEnd(float var1) {
        this.fogEnd = var1;
    }

    public float getFogExp() {
        return this.fogExp;
    }

    public void setFogExp(float var1) {
        this.fogExp = var1;
    }

    public Color getAmbientColor() {
        return this.ambientColor;
    }

    public void setAmbientColor(Color var1) {
        this.ambientColor.set(var1);
    }

    public aLH getAabb() {
        return this.aabb;
    }

    public Texture getDiffuseCubemap() {
        return this.diffuseCubemap;
    }

    public void setDiffuseCubemap(Texture var1) {
        this.diffuseCubemap = var1;
    }

    public Texture getReflectiveCubemap() {
        return this.reflectiveCubemap;
    }

    public void setReflectiveCubemap(Texture var1) {
        this.reflectiveCubemap = var1;
    }

    public boolean render(RenderContext var1, boolean var2) {
        if (this.firstFrame) {
            var1.getCurrentScene().addSceneAreaInfo(this);
            this.firstFrame = false;
        }

        return true;
    }

    public RenderAsset cloneAsset() {
        return new RSceneAreaInfo(this);
    }
}
