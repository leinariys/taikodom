package taikodom.render.scene;

import all.aQKa;
import com.hoplon.geometry.Vec3f;
import taikodom.render.PitchEntry;
import taikodom.render.StepContext;

import java.util.Iterator;
import java.util.Vector;

/**
 * Реализация класса указанного в имени тегов из файлов с расширением .pro
 */
public class SPitchedSet extends SoundObject {
    protected final aQKa distances = new aQKa();
    protected final Vec3f position = new Vec3f();
    protected Vector pitchEntries = new Vector(20);
    protected float gain;
    protected boolean isPaused;
    protected float currentControllerValue;

    public SPitchedSet() {
        this.isPaused = false;
        this.gain = 1.0F;
        this.distances.x = 10.0F;
        this.distances.y = 1000.0F;
        this.currentControllerValue = 0.0F;
    }

    protected SPitchedSet(SPitchedSet var1) {
        super(var1);
        this.isPaused = var1.isPaused;
        this.gain = var1.gain;
        this.distances.set(var1.distances);
        this.currentControllerValue = var1.currentControllerValue;

        for (int var2 = 0; var2 < var1.pitchEntryCount(); ++var2) {
            this.pitchEntries.add(var1.getPitchEntry(var2).cloneAsset());
        }

    }

    public void releaseReferences() {
        super.releaseReferences();
        this.stop();
        Iterator var2 = this.pitchEntries.iterator();

        while (var2.hasNext()) {
            PitchEntry var1 = (PitchEntry) var2.next();
            var1.releaseReferences();
        }

        this.pitchEntries.clear();
    }

    public void disposed() {
        this.stop();
        this.disposed = true;
    }

    public float getGain() {
        return this.gain;
    }

    public void setGain(float var1) {
        boolean var2 = false;
        if (this.gain == 0.0F && var1 > 0.0F) {
            var2 = true;
        }

        this.gain = var1;

        for (int var3 = 0; var3 < this.pitchEntries.size(); ++var3) {
            ((PitchEntry) this.pitchEntries.get(var3)).setParentGain(this.gain);
        }

        if (var2) {
            this.play();
        }

    }

    public aQKa getDistances() {
        return this.distances;
    }

    public void setDistances(aQKa var1) {
        this.distances.set(var1);

        for (int var2 = 0; var2 < this.pitchEntries.size(); ++var2) {
            if (((PitchEntry) this.pitchEntries.get(var2)).getSoundSource() != null) {
                ((PitchEntry) this.pitchEntries.get(var2)).getSoundSource().setMinMaxDistance(this.distances.x, this.distances.y);
            }
        }

    }

    public boolean play() {
        this.isPaused = false;
        boolean var1 = true;

        for (int var2 = 0; var2 < this.pitchEntries.size(); ++var2) {
            SoundSource var3 = ((PitchEntry) this.pitchEntries.get(var2)).getSoundSource();
            if (var3 != null) {
                var3.getBuffer().forceSyncLoading();
                var3.create();
                var3.setMinMaxDistance(this.distances.x, this.distances.y);
                this.transform.getTranslation(this.position);
                var3.setPosition(this.position);
                var3.setVelocity(this.velocity);
                var3.setGain(0.0F);
                var3.play(0);
            }
        }

        return var1;
    }

    public boolean stop() {
        this.isPaused = false;
        boolean var1 = true;

        for (int var2 = 0; var2 < this.pitchEntries.size(); ++var2) {
            if (((PitchEntry) this.pitchEntries.get(var2)).getSoundSource() != null) {
                var1 = ((PitchEntry) this.pitchEntries.get(var2)).getSoundSource().releaseSample();
            }
        }

        return var1;
    }

    public boolean isPlaying() {
        for (int var1 = 0; var1 < this.pitchEntries.size(); ++var1) {
            if (((PitchEntry) this.pitchEntries.get(var1)).getSoundSource() != null && ((PitchEntry) this.pitchEntries.get(var1)).getSoundSource().isPlaying()) {
                return true;
            }
        }

        return false;
    }

    public boolean isValid() {
        for (int var1 = 0; var1 < this.pitchEntries.size(); ++var1) {
            if (((PitchEntry) this.pitchEntries.get(var1)).getSoundSource() != null && !((PitchEntry) this.pitchEntries.get(var1)).getSoundSource().isValid()) {
                return false;
            }
        }

        return true;
    }

    public boolean isAlive() {
        return this.isPlaying() || this.isPaused;
    }

    public int step(StepContext var1) {
        float var2 = this.currentControllerValue > 1.0F ? this.getGain() * (this.currentControllerValue * 2.0F - 1.0F) : this.getGain() * (this.currentControllerValue + 1.0F) / 2.0F;

        for (int var3 = 0; var3 < this.pitchEntries.size(); ++var3) {
            ((PitchEntry) this.pitchEntries.get(var3)).setParentGain(var2);
            ((PitchEntry) this.pitchEntries.get(var3)).update(this.currentControllerValue, var1);
            SoundSource var4 = ((PitchEntry) this.pitchEntries.get(var3)).getSoundSource();
            if (var4 != null) {
                this.position.sub(this.globalTransform.position, var1.getCamera().getGlobalTransform().position);
                var4.setPosition(this.position);
                var4.setVelocity(this.velocity);
            }
        }

        return 0;
    }

    public void addPitchEntry(PitchEntry var1) {
        if (var1 != null) {
            if (this.pitchEntries.size() > 1) {
                ((PitchEntry) this.pitchEntries.get(this.pitchEntries.size() - 1)).setLast(false);
            }

            this.pitchEntries.add(var1);
            if (this.pitchEntries.size() == 1) {
                var1.setFirst(true);
            } else {
                var1.setLast(true);
            }

        }
    }

    public int pitchEntryCount() {
        return this.pitchEntries.size();
    }

    public PitchEntry getPitchEntry(int var1) {
        return (PitchEntry) this.pitchEntries.get(var1);
    }

    public void setPitchEntry(int var1, PitchEntry var2) {
        int var3 = this.pitchEntries.size();
        if (var3 > var1) {
            this.pitchEntries.set(var1, var2);
        } else {
            this.pitchEntries.add(var2);
        }

        if (var1 == 0) {
            var2.setFirst(true);
        }

        var2.setLast(true);

        for (int var4 = 0; var4 < this.pitchEntries.size() - 1; ++var4) {
            ((PitchEntry) this.pitchEntries.get(var4)).setLast(false);
        }

    }

    public void removePitchEntry(int var1) {
        this.pitchEntries.remove(this.pitchEntries.get(var1));
    }

    public void removePitchEntry(PitchEntry var1) {
        this.pitchEntries.remove(var1);
    }

    public float getControllerValue() {
        return this.currentControllerValue;
    }

    public void setControllerValue(float var1) {
        this.currentControllerValue = var1;
    }

    public void onRemove() {
        super.onRemove();
        this.stop();
    }

    public SPitchedSet cloneAsset() {
        return new SPitchedSet(this);
    }
}
