package taikodom.render.scene;

import all.Vector3dOperations;
import all.ajK;
import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import gnu.trove.THashMap;
import taikodom.render.DrawContext;
import taikodom.render.primitives.Light;
import taikodom.render.shaders.parameters.GlobalShaderParameter;
import taikodom.render.textures.ChannelInfo;

import javax.vecmath.Vector3f;
import java.nio.FloatBuffer;
import java.util.List;
import java.util.Map;

/**
 * Настройки сцены
 */
public class SceneConfig {
    private Map globalShaderParameters = new THashMap();

    public SceneConfig() {
        this.init();
    }

    public boolean usePostProcessingFXs() {
        return false;
    }

    protected void init() {
        this.addGlobalShaderParameter(new GlobalShaderParameter(0, "elapsedTime") {
            public void bind(int var1, DrawContext var2) {
                var2.getGL().glUniform1f(var1, var2.getElapsedTime());
            }
        });
        this.addGlobalShaderParameter(new GlobalShaderParameter(0, "totalTime") {
            public void bind(int var1, DrawContext var2) {
                var2.getGL().glUniform1f(var1, (float) var2.getTotalTime());
            }
        });
        this.addGlobalShaderParameter(new GlobalShaderParameter(0, "cameraPositionWorldSpace") {
            public void bind(int var1, DrawContext var2) {
                var2.getGL().glUniform3f(var1, 0.0F, 0.0F, 0.0F);
            }
        });
        this.addGlobalShaderParameter(new GlobalShaderParameter(0, "cameraTransform") {
            public void bind(int var1, DrawContext var2) {
                var2.floatBuffer60Temp0.clear();
                var2.getGL().glUniformMatrix4fv(var1, 1, false, var2.gpuCamAffine.b(var2.floatBuffer60Temp0));
            }
        });
        this.addGlobalShaderParameter(new GlobalShaderParameter(0, "cameraAffineTransform") {
            public void bind(int var1, DrawContext var2) {
                var2.floatBuffer60Temp0.clear();
                var2.getGL().glUniformMatrix4fv(var1, 1, false, var2.gpuCamTransform.b(var2.floatBuffer60Temp0));
            }
        });
        this.addGlobalShaderParameter(new GlobalShaderParameter(0, "lightPositionWorldSpace") {
            public void bind(int var1, DrawContext var2) {
                if (var2.shaderParamContext.getLight() != null) {
                    Vector3dOperations var3 = var2.shaderParamContext.getLight().getPosition();
                    Vector3dOperations var4 = var2.gpuOffset;
                    var2.getGL().glUniform3f(var1, (float) (var3.x - var4.x), (float) (var3.y - var4.y), (float) (var3.z - var4.z));
                }

            }
        });
        this.addGlobalShaderParameter(new GlobalShaderParameter(0, "lightDirectionWorldSpace") {
            public void bind(int var1, DrawContext var2) {
                if (var2.shaderParamContext.getLight() != null) {
                    Vec3f var3 = var2.shaderParamContext.getLight().getDirection();
                    var2.getGL().glUniform3f(var1, -var3.x, -var3.y, -var3.z);
                }

            }
        });
        this.addGlobalShaderParameter(new GlobalShaderParameter(0, "lightDiffuseColor") {
            public void bind(int var1, DrawContext var2) {
                if (var2.shaderParamContext.getLight() != null) {
                    Color var3 = var2.shaderParamContext.getLight().getDiffuseColor();
                    var2.getGL().glUniform3f(var1, var3.x, var3.y, var3.z);
                }

            }
        });
        this.addGlobalShaderParameter(new GlobalShaderParameter(0, "lightSpecularColor") {
            public void bind(int var1, DrawContext var2) {
                if (var2.shaderParamContext.getLight() != null) {
                    Color var3 = var2.shaderParamContext.getLight().getSpecularColor();
                    var2.getGL().glUniform3f(var1, var3.x, var3.y, var3.z);
                }

            }
        });
        this.addGlobalShaderParameter(new GlobalShaderParameter(0, "lightInverseSquaredRadius") {
            public void bind(int var1, DrawContext var2) {
                if (var2.shaderParamContext.getLight() != null) {
                    float var3 = var2.shaderParamContext.getLight().getRadius();
                    var2.getGL().glUniform1f(var1, 1.0F / (var3 * var3));
                }

            }
        });
        this.addGlobalShaderParameter(new GlobalShaderParameter(0, "lightRadius") {
            public void bind(int var1, DrawContext var2) {
                if (var2.shaderParamContext.getLight() != null) {
                    var2.getGL().glUniform1f(var1, 1.0F / var2.shaderParamContext.getLight().getRadius());
                }

            }
        });
        this.addGlobalShaderParameter(new GlobalShaderParameter(0, "spotLightCosCutoff") {
            public void bind(int var1, DrawContext var2) {
                if (var2.shaderParamContext.getLight() != null) {
                    var2.getGL().glUniform1f(var1, var2.shaderParamContext.getLight().getCosCutoff());
                }

            }
        });
        this.addGlobalShaderParameter(new GlobalShaderParameter(0, "spotLightExponent") {
            public void bind(int var1, DrawContext var2) {
                if (var2.shaderParamContext.getLight() != null) {
                    var2.getGL().glUniform1f(var1, var2.shaderParamContext.getLight().getSpotExponent());
                }

            }
        });
        this.addGlobalShaderParameter(new GlobalShaderParameter(0, "mainLightDirectionWorldSpace") {
            public void bind(int var1, DrawContext var2) {
                Light var3 = var2.getMainLight();
                if (var3 != null) {
                    Vec3f var4 = var3.getDirection();
                    var2.getGL().glUniform3f(var1, -var4.x, -var4.y, -var4.z);
                } else {
                    var2.getGL().glUniform3f(var1, 0.7F, 0.7F, 0.0F);
                }

            }
        });
        this.addGlobalShaderParameter(new GlobalShaderParameter(0, "mainLightPositionWorldSpace") {
            public void bind(int var1, DrawContext var2) {
                Light var3 = var2.getMainLight();
                if (var3 != null) {
                    Vector3dOperations var4 = var3.getPosition();
                    Vector3dOperations var5 = var2.gpuOffset;
                    var2.getGL().glUniform3f(var1, (float) (var4.x - var5.x), (float) (var4.y - var5.y), (float) (var4.z - var5.z));
                } else {
                    var2.getGL().glUniform3f(var1, 0.0F, 0.0F, 0.0F);
                }

            }
        });
        this.addGlobalShaderParameter(new GlobalShaderParameter(0, "mainLightDiffuseColor") {
            public void bind(int var1, DrawContext var2) {
                Light var3 = var2.getMainLight();
                if (var3 != null) {
                    Color var4 = var3.getDiffuseColor();
                    var2.getGL().glUniform3f(var1, var4.x, var4.y, var4.z);
                } else {
                    var2.getGL().glUniform3f(var1, 1.0F, 1.0F, 1.0F);
                }

            }
        });
        this.addGlobalShaderParameter(new GlobalShaderParameter(0, "mainLightSpecularColor") {
            public void bind(int var1, DrawContext var2) {
                Light var3 = var2.getMainLight();
                if (var3 != null) {
                    Color var4 = var3.getSpecularColor();
                    var2.getGL().glUniform3f(var1, var4.x, var4.y, var4.z);
                } else {
                    var2.getGL().glUniform3f(var1, 1.0F, 1.0F, 1.0F);
                }

            }
        });

        for (int var1 = 0; var1 < 16; ++var1) {
            int finalVar = var1;
            this.addGlobalShaderParameter(new GlobalShaderParameter(1, "tex" + finalVar) {
                public void bind(int var1x, DrawContext var2) {
                    if (var2.materialState.getStates().size() > finalVar) {
                        var2.getGL().glUniform1i(var1x, var2.materialState.getTexInfo(finalVar).getChannelMapping());
                    } else {
                        var2.getGL().glUniform1i(var1x, 0);
                    }

                }
            });
        }

        this.addGlobalShaderParameter(new GlobalShaderParameter(1, "shininess") {
            public void bind(int var1, DrawContext var2) {
                var2.getGL().glUniform1f(var1, var2.currentRecord().material.getShininess());
            }
        });
        this.addGlobalShaderParameter(new GlobalShaderParameter(1, "materialSpecular") {
            public void bind(int var1, DrawContext var2) {
                Color var3 = var2.currentRecord().material.getSpecular();
                var2.getGL().glUniform4f(var1, var3.getRed(), var3.getGreen(), var3.getBlue(), var3.getAlpha());
            }
        });
        this.addGlobalShaderParameter(new GlobalShaderParameter(1, "reflectCubemap") {
            public void bind(int var1, DrawContext var2) {
                List var3 = var2.materialState.getStates();
                int var4 = 0;

                for (int var5 = var3.size(); var4 < var5; ++var4) {
                    if (((ChannelInfo) var3.get(var4)).getTexChannel() == 0) {
                        var2.getGL().glUniform1i(var1, var4);
                        return;
                    }
                }

            }
        });
        this.addGlobalShaderParameter(new GlobalShaderParameter(1, "diffuseCubemap") {
            public void bind(int var1, DrawContext var2) {
                List var3 = var2.materialState.getStates();
                int var4 = 0;

                for (int var5 = var3.size(); var4 < var5; ++var4) {
                    if (((ChannelInfo) var3.get(var4)).getTexChannel() == 1) {
                        var2.getGL().glUniform1i(var1, var4);
                        return;
                    }
                }

            }
        });
        this.addGlobalShaderParameter(new GlobalShaderParameter(2, "cameraPositionObjectSpace") {
            public void bind(int var1, DrawContext var2) {
                var2.gpuCamTransform.getTranslation(var2.vec3fTemp0);
                var2.currentRecord().transform.b(var2.vec3fTemp0, var2.vec3fTemp0);
                var2.getGL().glUniform3f(var1, var2.vec3fTemp0.x, var2.vec3fTemp0.y, var2.vec3fTemp0.z);
            }
        });
        this.addGlobalShaderParameter(new GlobalShaderParameter(2, "lightPositionObjectSpace") {
            public void bind(int var1, DrawContext var2) {
                if (var2.shaderParamContext.getLight() != null) {
                    var2.currentRecord().transform.a(var2.shaderParamContext.getLight().getPosition(), var2.vec3fTemp0);
                    var2.getGL().glUniform3f(var1, var2.vec3fTemp0.x, var2.vec3fTemp0.y, var2.vec3fTemp0.z);
                }

            }
        });
        this.addGlobalShaderParameter(new GlobalShaderParameter(2, "lightDirectionObjectSpace") {
            public void bind(int var1, DrawContext var2) {
                Vec3f var3 = var2.vec3fTemp0;
                var2.currentRecord().transform.mX.a((Vector3f) var2.shaderParamContext.getLight().getDirection(), (Vector3f) var3);
                var3.normalize();
                var2.getGL().glUniform3f(var1, -var3.x, -var3.y, -var3.z);
            }
        });
        this.addGlobalShaderParameter(new GlobalShaderParameter(2, "objectTransform") {
            public void bind(int var1, DrawContext var2) {
                var2.floatBuffer60Temp0.clear();
                var2.getGL().glUniformMatrix4fv(var1, 1, false, var2.currentRecord().gpuTransform.b(var2.floatBuffer60Temp0));
            }
        });
        this.addGlobalShaderParameter(new GlobalShaderParameter(2, "objectAffineTransform") {
            public void bind(int var1, DrawContext var2) {
                ajK var3 = var2.currentRecord().gpuTransform;
                var2.floatBuffer60Temp0.clear();
                var3.a(var2.floatBuffer60Temp0);
                var2.floatBuffer60Temp0.flip();
                var2.getGL().glUniformMatrix4fv(var1, 1, false, var2.floatBuffer60Temp0);
            }
        });
        this.addGlobalShaderParameter(new GlobalShaderParameter(2, "projectTransform") {
            public void bind(int var1, DrawContext var2) {
                var2.floatBuffer60Temp0.clear();
                FloatBuffer var3 = var2.matrix4fTemp0.b(0.5F, 0.0F, 0.0F, 0.5F, 0.0F, 0.5F, 0.0F, 0.5F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 1.0F).h(var2.gpuCamAffine).h(var2.camProjection).h(var2.currentRecord().gpuTransform).b(var2.floatBuffer60Temp0);
                var2.getGL().glUniformMatrix4fv(var1, 1, false, var3);
            }
        });
        this.addGlobalShaderParameter(new GlobalShaderParameter(2, "ambientLightColor") {
            public void bind(int var1, DrawContext var2) {
                Color var3 = var2.currentRecord().areaInfo.ambientColor;
                var2.getGL().glUniform3f(var1, var3.x, var3.y, var3.z);
            }
        });
        this.addGlobalShaderParameter(new GlobalShaderParameter(2, "objectPrimitiveColor") {
            public void bind(int var1, DrawContext var2) {
                Color var3 = var2.currentRecord().color;
                var2.getGL().glUniform4fARB(var1, var3.getRed(), var3.getGreen(), var3.getBlue(), var3.getAlpha());
            }
        });
    }

    private void addGlobalShaderParameter(GlobalShaderParameter var1) {
        this.globalShaderParameters.put(var1.getName(), var1);
    }

    public GlobalShaderParameter getGlobalShaderParameter(String var1) {
        return (GlobalShaderParameter) this.globalShaderParameters.get(var1);
    }
}
