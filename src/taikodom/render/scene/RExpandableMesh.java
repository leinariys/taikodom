package taikodom.render.scene;

import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import taikodom.render.RenderContext;
import taikodom.render.data.ExpandableIndexData;
import taikodom.render.data.ExpandableVertexData;
import taikodom.render.data.VertexData;
import taikodom.render.data.VertexLayout;
import taikodom.render.enums.GeometryType;
import taikodom.render.loader.RenderAsset;
import taikodom.render.primitives.Mesh;

import javax.vecmath.Vector2f;

public class RExpandableMesh extends RenderObject {
    protected Mesh mesh;
    protected float minimumScreenSize;
    protected int currentIdx = 0;

    public RExpandableMesh() {
        this.mesh = new Mesh();
        this.mesh.setGeometryType(GeometryType.TRIANGLES);
        this.mesh.setVertexData(new ExpandableVertexData(VertexLayout.defaultLayout, 10));
        this.mesh.setIndexes(new ExpandableIndexData(10));
    }

    protected RExpandableMesh(RExpandableMesh var1) {
        super(var1);
        this.mesh = (Mesh) this.smartClone(var1.mesh);
        this.minimumScreenSize = var1.minimumScreenSize;
    }

    public void addVertex(float var1, float var2, float var3) {
        int var4 = this.mesh.getVertexData().size();
        this.mesh.getVertexData().setPosition(var4, var1, var2, var3);
    }

    public void setVertex(int var1, float var2, float var3, float var4) {
        this.mesh.getVertexData().setPosition(var1, var2, var3, var4);
    }

    public void setColor(int var1, float var2, float var3, float var4, float var5) {
        this.mesh.getVertexData().setColor(var1, var2, var3, var4, var5);
    }

    public void addVertexWithColor(float var1, float var2, float var3, float var4, float var5, float var6, float var7) {
        int var8 = this.mesh.getVertexData().size();
        this.mesh.getVertexData().setPosition(var8, var1, var2, var3);
        this.mesh.getVertexData().setColor(var8, var4, var5, var6, var7);
    }

    public void setVertexColor(int var1, float var2, float var3, float var4, float var5) {
        this.mesh.getVertexData().setColor(var1, var2, var3, var4, var5);
    }

    public void addIndex(int var1) {
        int var2 = this.mesh.getIndexes().size();
        this.mesh.getIndexes().setIndex(var2, var1);
    }

    public void setIndex(int var1, int var2) {
        this.mesh.getIndexes().setIndex(var1, var2);
    }

    public Mesh getMesh() {
        return this.mesh;
    }

    public void setMesh(Mesh var1) {
        this.mesh = var1;
    }

    public float getMinimumScreenSize() {
        return this.minimumScreenSize;
    }

    public void setMinimumScreenSize(float var1) {
        this.minimumScreenSize = var1;
    }

    public boolean render(RenderContext var1, boolean var2) {
        if (this.render) {
            if (!super.render(var1, var2)) {
                return false;
            } else {
                var1.addPrimitive(this.material, this.mesh, this.transform, this.primitiveColor, this.occlusionQuery, this.renderPriority, this, this.aabbWorldSpace, this.currentSquaredDistanceToCamera);
                return true;
            }
        } else {
            return false;
        }
    }

    public RenderAsset cloneAsset() {
        RExpandableMesh var1 = new RExpandableMesh(this);
        return var1;
    }

    public void addPoint(Vec3f var1, Vector2f var2, Color var3) {
        VertexData var4 = this.mesh.getVertexData();
        var4.setColor(this.currentIdx, var3.x, var3.y, var3.z, var3.w);
        var4.setTexCoord(this.currentIdx, 0, var2.x, var2.y);
        var4.setPosition(this.currentIdx, var1.x, var1.y, var1.z);
        this.mesh.getIndexes().setIndex(this.currentIdx, this.currentIdx);
        ++this.currentIdx;
    }

    public void reset() {
        this.currentIdx = 0;
    }
}
