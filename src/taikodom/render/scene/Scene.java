package taikodom.render.scene;

import all.LogPrinter;
import all.Vector3dOperations;
import com.hoplon.geometry.Color;
import gnu.trove.THashSet;
import taikodom.render.RenderContext;
import taikodom.render.RenderView;
import taikodom.render.StepContext;
import taikodom.render.partitioning.TkdWorld;
import taikodom.render.primitives.RenderAreaInfo;

import javax.media.opengl.GL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Сцена
 */
public class Scene {
    static LogPrinter logger = LogPrinter.K(Scene.class);
    private Set objects = new THashSet();
    private List postPonedRemovalList = new ArrayList();
    /**
     * @deprecated
     */
    @Deprecated
    private List sceneAreaInfos = new ArrayList();
    private SceneConfig sceneConfig = new SceneConfig();
    private boolean enabled = true;
    private String name = null;
    private TkdWorld tkdWorld = null;
    private boolean useTkdWorld = false;
    private float colorIntensity = 1.0F;
    private float currentColorCorrection = 0.0F;
    private float colorIntensityResponse = 0.65F;

    public Scene(String var1) {
        this.name = var1;
    }

    public void createPartitioningStructure() {
        if (this.tkdWorld == null) {
            this.tkdWorld = new TkdWorld();
            Set var1 = this.objects;
            synchronized (this.objects) {
                Iterator var3 = this.objects.iterator();
                while (true) {
                    if (!var3.hasNext()) {
                        break;
                    }
                    SceneObject var2 = (SceneObject) var3.next();
                    this.tkdWorld.addChild(var2);
                }
            }
            this.useTkdWorld = true;
        }
    }

    public boolean addChild(SceneObject var1) {
        if (var1 == null) {
            throw new NullPointerException("Cannot add all null SceneObject");
        } else {
            var1.updateInternalGeometry((SceneObject) null);
            boolean var2 = false;
            synchronized (var1) {
                var2 = this.objects.add(var1);
                if (this.tkdWorld != null) {
                    this.tkdWorld.addChild(var1);
                }
                return var2;
            }
        }
    }

    public void removeAllChildren() {
        Set var1 = this.objects;
        synchronized (this.objects) {
            Iterator var3 = this.objects.iterator();
            while (var3.hasNext()) {
                SceneObject var2 = (SceneObject) var3.next();
                var2.onRemove();
            }
            this.objects.clear();
            this.sceneAreaInfos.clear();
            if (this.tkdWorld != null) {
                this.tkdWorld.removeAll();
            }
        }
    }

    public boolean removeChild(SceneObject var1) {
        if (var1 == null) {
            logger.debug("Trying to_q remove all null child.");
            return false;
        } else {
            boolean var2 = false;
            Set var3 = this.objects;
            synchronized (this.objects) {
                var2 = this.objects.remove(var1);
                var1.onRemove();
                if (this.tkdWorld != null) {
                    this.tkdWorld.removeChild(var1);
                }
                return var2;
            }
        }
    }

    public void render(RenderContext var1) {
        if (this.useTkdWorld && this.tkdWorld != null) {
            this.tkdWorld.cullZone(var1, true);
        } else {
            boolean var2 = var1.getCamera().isOrthogonal();
            Set var3 = this.objects;
            synchronized (this.objects) {
                Iterator var5 = this.objects.iterator();

                while (var5.hasNext()) {
                    SceneObject var4 = (SceneObject) var5.next();
                    var4.fillRenderQueue(var1, !var2);
                }
            }
        }
    }

    private boolean stepObject(SceneObject var1, StepContext var2) {
        if (var1.isDisposed()) {
            return true;
        } else if (var1.step(var2) != 0) {
            return true;
        } else {
            int var3 = var1.childCount();
            var1.setCanRemoveChildren(false);

            for (int var4 = 0; var4 < var3; ++var4) {
                if (this.stepObject(var1.getChild(var4), var2)) {
                    var1.removeChild(var1.getChild(var4));
                }
            }

            var1.setCanRemoveChildren(true);
            if (var1 instanceof RLod) {
                RLod var5 = (RLod) var1;
                if (var5.getObjectLod0() != null) {
                    this.stepObject(var5.getObjectLod0(), var2);
                }

                if (var5.getObjectLod1() != null) {
                    this.stepObject(var5.getObjectLod1(), var2);
                }

                if (var5.getObjectLod2() != null) {
                    this.stepObject(var5.getObjectLod2(), var2);
                }
            }
            return false;
        }
    }

    public void step(StepContext var1) {
        if (this.useTkdWorld && this.tkdWorld != null) {
            this.tkdWorld.stepObjects(var1);
        } else {
            var1.incObjectSteps((long) this.objects.size());
            Set var2 = this.objects;
            synchronized (this.objects) {
                Iterator var4 = this.objects.iterator();
                while (var4.hasNext()) {
                    SceneObject var3 = (SceneObject) var4.next();
                    if (this.stepObject(var3, var1)) {
                        this.postPonedRemovalList.add(var3);
                    }
                }
                if (this.postPonedRemovalList.size() > 0) {
                    this.objects.removeAll(this.postPonedRemovalList);
                    this.postPonedRemovalList.clear();
                }
            }
        }
        this.updateColorIntensity(var1.getDeltaTime());
    }

    public Set getChildren() {
        return this.objects;
    }

    public SceneConfig getSceneConfig() {
        return this.sceneConfig;
    }

    public void setSceneConfig(SceneConfig var1) {
        this.sceneConfig = var1;
    }

    public RenderAreaInfo fillRenderAreaInfo(double var1, double var3, double var5, RenderAreaInfo var7) {
        if (this.sceneAreaInfos.size() > 0) {
            RSceneAreaInfo var8 = (RSceneAreaInfo) this.sceneAreaInfos.get(0);
            if (var8 != null) {
                var7.ambientColor.set(var8.ambientColor);
                var7.diffuseCubemap = var8.diffuseCubemap;
                var7.reflectiveCubemap = var8.reflectiveCubemap;
            }
        }
        return var7;
    }

    /**
     * @deprecated
     */
    @Deprecated
    public void addSceneAreaInfo(RSceneAreaInfo var1) {
        if (var1 != null) {
            this.sceneAreaInfos.add(var1);
        }
    }

    /**
     * @deprecated
     */
    @Deprecated
    public RSceneAreaInfo getSceneAreaInfo(Vector3dOperations var1) {
        return this.sceneAreaInfos.size() > 0 ? (RSceneAreaInfo) this.sceneAreaInfos.get(0) : null;
    }

    public void fillRenderViewAreaInfo(Vector3dOperations var1, RenderView var2) {
        if (this.sceneAreaInfos.size() > 0) {
            RSceneAreaInfo var3 = (RSceneAreaInfo) this.sceneAreaInfos.get(0);
            if (var3 != null) {
                var2.setFogColor(var3.fogColor);
                var2.setFogEnd(var3.fogEnd);
                var2.setFogStart(var3.fogStart);
                var2.setFogExp(var3.fogExp);
                var2.setFogType(var3.fogType.glEquivalent());
            } else {
                var2.setFogColor(new Color(0.0F, 0.0F, 0.0F, 0.0F));
                var2.setFogEnd(100000.0F);
                var2.setFogStart(50000.0F);
            }
        }

    }

    public boolean isEnabled() {
        return this.enabled;
    }

    public void setEnabled(boolean var1) {
        this.enabled = var1;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String var1) {
        this.name = var1;
    }

    public String toString() {
        return "Scene: " + this.name;
    }

    public boolean isUseTkdWorld() {
        return this.useTkdWorld;
    }

    public void setUseTkdWorld(boolean var1) {
        this.useTkdWorld = var1;
    }

    public void addStaticChild(SceneObject var1) {
        if (var1 == null) {
            throw new NullPointerException("Cannot add all null SceneObject");
        } else {
            var1.updateInternalGeometry((SceneObject) null);
            synchronized (var1) {
                this.objects.add(var1);
                if (this.tkdWorld != null) {
                    this.tkdWorld.addStaticChild(var1);
                }
            }
        }
    }

    public void renderAABBs(GL var1, Vector3dOperations var2) {
        if (this.tkdWorld != null) {
            this.tkdWorld.renderGL(var1, var2);
        }
    }

    public boolean hasChild(SceneObject var1) {
        synchronized (this.objects) {
            Iterator var4 = this.objects.iterator();
            while (var4.hasNext()) {
                SceneObject var3 = (SceneObject) var4.next();
                if (var3 == var1) {
                    return true;
                }
            }
            return false;
        }
    }

    public void addColorIntensity(float var1) {
        this.colorIntensity += var1;
    }

    public float getColorIntensity() {
        return this.colorIntensity;
    }

    public float getColorCorrection() {
        return this.currentColorCorrection;
    }

    public void updateColorIntensity(float var1) {
        float var2 = this.colorIntensityResponse * var1;
        float var3 = 1.0F - this.colorIntensity;
        if (var3 < 0.0F) {
            var3 = 0.0F;
        } else if (var3 > 1.0F) {
            var3 = 1.0F;
        }

        float var4 = var3 - this.currentColorCorrection;
        if (var4 > 0.0F) {
            this.currentColorCorrection += var4 * var2;
        } else {
            float var5 = 1.0F;
            if (var4 < 0.0F) {
                var5 = -0.5F * this.colorIntensityResponse / var4;
            }
            if (var5 > 1.0F) {
                var5 = 1.0F;
            }
            this.currentColorCorrection += var5 * var4 * var2;
        }
        this.colorIntensity = 0.0F;
    }
}
