package taikodom.render.scene;

import all.OrientationBase;

public class BoneTransform {
    private int index;
    private OrientationBase orientation;

    public BoneTransform(int var1, OrientationBase var2) {
        this.index = var1;
        this.orientation = new OrientationBase(var2);
    }

    public int getIndex() {
        return this.index;
    }

    public void setIndex(int var1) {
        this.index = var1;
    }

    public OrientationBase getOrientation() {
        return this.orientation;
    }

    public void setOrientation(OrientationBase var1) {
        this.orientation = var1;
    }
}
