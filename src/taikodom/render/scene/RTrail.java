package taikodom.render.scene;

import all.Vector3dOperations;
import all.aBu;
import all.aQKa;
import all.bc_q;
import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import taikodom.render.NotExported;
import taikodom.render.RenderContext;
import taikodom.render.StepContext;
import taikodom.render.camera.Camera;
import taikodom.render.data.ExpandableIndexData;
import taikodom.render.data.ExpandableVertexData;
import taikodom.render.data.VertexLayout;
import taikodom.render.enums.GeometryType;
import taikodom.render.loader.RenderAsset;
import taikodom.render.loader.SceneLoader;
import taikodom.render.primitives.Billboard;
import taikodom.render.primitives.Mesh;
import taikodom.render.textures.BaseTexture;
import taikodom.render.textures.Material;

import java.util.ArrayList;

/**
 * Реализация класса указанного в имени тегов из файлов с расширением .pro
 */
public class RTrail extends RenderObject {
    static final Vec3f curDir2 = new Vec3f();
    static final Vec3f lastDir3 = new Vec3f();
    private static final Vec3f tempVect = new Vec3f();
    private static final Vec3f dir = new Vec3f();
    private static final Vec3f dir2 = new Vec3f();
    private static final Vec3f side = new Vec3f();
    private static final Vec3f m1 = new Vec3f();
    private static final Vec3f m2 = new Vec3f();
    private static final Vec3f m3 = new Vec3f();
    private static final Vector3dOperations norm0 = new Vector3dOperations();
    private static final Vector3dOperations norm1 = new Vector3dOperations();
    private static final Vector3dOperations point = new Vector3dOperations();
    private static final Vector3dOperations spawnVect = new Vector3dOperations();
    private static final Vec3f spawnDirection = new Vec3f();
    private static final Vec3f tempVec = new Vec3f();
    private static final Vec3f lastDir = new Vec3f();
    private static final Vec3f curDir = new Vec3f();
    private static final Vector3dOperations cameraTranslation = new Vector3dOperations();
    private static final Vector3dOperations localTranslation = new Vector3dOperations();
    private static final Vector3dOperations lastPos = new Vector3dOperations();
    final Color whiteAlpha1;
    final Color whiteAlpha0;
    final aQKa texCoord1;
    final aQKa texCoord2;
    final aQKa texCoord3;
    final aQKa billboardSize;
    final Color billboardColor;
    final ArrayList entries2;
    final ExpandableIndexData indexes;
    final ExpandableVertexData vertexes;
    public float xSize;
    public int RenderedTrails;
    public int Trails;
    public int numVertexes;
    long ticksSec = System.currentTimeMillis();
    long lastTick;
    float lifeTimeModifier;
    float minSpeed;
    float spawnDistance;
    float lifeTime;
    float textureTileStart;
    int tempVec3fCall = 0;
    float currentLod;
    float squaredDistance;
    Billboard dotBillboard;
    boolean useLighting;
    float lastAddedTime;
    Vector3dOperations lastAddedPosition;
    Material billboardMaterial;
    int numEntrySlots;
    int actualEntrySlot;
    float SCROLL_FACTOR = 10.0F;
    float FADE_IN_RELATION = 0.25F;
    int MAX_TRAIL_SUBDIVISION = 50;
    Mesh mesh;
    boolean wasVisibleLastFrame;
    int currentPoint;
    float TRAIL_FILTER_VALUE = 0.65F;
    float TRAIL_CUTOFF_COSINE = 0.3F;
    float TRAIL_ANGLE_THRESHOLD = 0.99F;
    float DISTANCE_LOD_1 = 160000.0F;
    float DISTANCE_LOD_2 = 360000.0F;
    float DISTANCE_LOD_3 = 640000.0F;
    float DISTANCE_LOD_4 = 7840000.0F;
    float DISTANCE_OUT_OF_RANGE = 1.0E8F;
    private int actualIndex;
    private Vector3dOperations globalTranslation;
    private bc_q relativeTransform;
    private aBu tempEntry;

    public RTrail() {
        this.initializeLocalVariables();
        VertexLayout var1 = new VertexLayout(true, true, false, false, 1);
        this.mesh = new Mesh();
        this.indexes = new ExpandableIndexData(300);
        this.vertexes = new ExpandableVertexData(var1, 500);
        this.mesh.setIndexes(this.indexes);
        this.mesh.setVertexData(this.vertexes);
        this.squaredDistance = 1.0F;
        this.currentLod = 1.0F;
        this.lifeTimeModifier = 1.0F;
        this.spawnDistance = 1.0F;
        this.lifeTime = 0.5F;
        this.xSize = 1.0F;
        this.minSpeed = 1.0F;
        this.dotBillboard = new Billboard();
        this.billboardSize = new aQKa(1.0F, 1.0F);
        this.billboardColor = new Color(1.0F, 1.0F, 1.0F, 1.0F);
        this.entries2 = new ArrayList(300);
        this.mesh.setName("Mesh for all RTrail");
        this.wasVisibleLastFrame = false;
        this.relativeTransform = new bc_q();
        this.whiteAlpha1 = new Color(1.0F, 1.0F, 1.0F, 1.0F);
        this.whiteAlpha0 = new Color(1.0F, 1.0F, 1.0F, 1.0F);
        this.texCoord1 = new aQKa(0.0F, 0.0F);
        this.texCoord2 = new aQKa(0.5F, 0.0F);
        this.texCoord3 = new aQKa(1.0F, 0.0F);
        this.globalTranslation = new Vector3dOperations();
        this.tempEntry = new aBu();
        this.mesh.setGeometryType(GeometryType.TRIANGLE_STRIP);
        this.textureTileStart = 0.0F;
    }

    RTrail(RTrail var1) {
        super(var1);
        this.initializeLocalVariables();
        VertexLayout var2 = new VertexLayout(true, true, false, false, 1);
        this.mesh = new Mesh();
        this.indexes = new ExpandableIndexData(300);
        this.vertexes = new ExpandableVertexData(var2, 500);
        this.mesh.setIndexes(this.indexes);
        this.mesh.setVertexData(this.vertexes);
        this.squaredDistance = 1.0F;
        this.currentLod = 1.0F;
        this.lifeTimeModifier = 1.0F;
        this.spawnDistance = var1.spawnDistance;
        this.lifeTime = var1.lifeTime;
        this.xSize = var1.xSize;
        this.minSpeed = var1.minSpeed;
        this.dotBillboard = var1.dotBillboard;
        this.billboardSize = new aQKa(var1.billboardSize);
        this.billboardColor = new Color(var1.billboardColor);
        this.entries2 = new ArrayList(300);
        this.mesh.setName("Mesh for all RTrail");
        this.wasVisibleLastFrame = false;
        this.relativeTransform = new bc_q();
        this.whiteAlpha1 = new Color(1.0F, 1.0F, 1.0F, 1.0F);
        this.whiteAlpha0 = new Color(1.0F, 1.0F, 1.0F, 1.0F);
        this.texCoord1 = new aQKa(var1.texCoord1);
        this.texCoord2 = new aQKa(var1.texCoord2);
        this.texCoord3 = new aQKa(var1.texCoord3);
        this.globalTranslation = new Vector3dOperations();
        this.tempEntry = new aBu();
        this.mesh.setGeometryType(GeometryType.TRIANGLE_STRIP);
        this.billboardSize.x = 1.0F;
        this.billboardSize.y = 1.0F;
        this.billboardColor.x = 1.0F;
        this.billboardColor.y = 1.0F;
        this.billboardColor.z = 1.0F;
        this.textureTileStart = 0.0F;
    }

    void setCurrentPoints(int var1) {
        this.currentPoint = var1;
    }

    public int getNumEntries() {
        return this.actualEntrySlot;
    }

    aBu getEntry(int var1) {
        return var1 > this.actualEntrySlot ? null : (aBu) this.entries2.get(var1);
    }

    void addEntry(aBu var1) {
        if (this.actualEntrySlot == this.entries2.size()) {
            this.entries2.add(new aBu(var1));
        } else {
            ((aBu) this.entries2.get(this.actualEntrySlot)).a(var1);
        }
        ++this.actualEntrySlot;
    }

    void removeEntry(int var1) {
        for (int var2 = var1; var2 < this.entries2.size() - 1; ++var2) {
            ((aBu) this.entries2.get(var2)).a((aBu) this.entries2.get(var2 + 1));
        }
        --this.actualEntrySlot;
    }

    aBu getFirstEntry() {
        return (aBu) this.entries2.get(0);
    }

    aBu getLastEntry() {
        return (aBu) this.entries2.get(this.getNumEntries() - 1);
    }

    int getEntriesBegin() {
        return 0;
    }

    int getEntriesEnd() {
        return this.getNumEntries() - 1;
    }

    float invSqrRoot(float var1) {
        float var2 = (float) Math.sqrt((double) var1);
        return 1.0F / var2;
    }

    void fastNormalizeVec3(Vec3f var1) {
        float var2 = var1.x * var1.x + var1.y * var1.y + var1.z * var1.z;
        var2 = this.invSqrRoot(var2);
        var1.x *= var2;
        var1.y *= var2;
        var1.z *= var2;
    }

    float getInvLenght(Vec3f var1) {
        float var2 = var1.x * var1.x + var1.y * var1.y + var1.z * var1.z;
        var2 = this.invSqrRoot(var2);
        return var2;
    }

    void initializeLocalVariables() {
    }

    protected Object clone() {
        return new RTrail(this);
    }

    public void dispose() {
    }

    public void releaseReferences() {
        super.releaseReferences();
    }

    void removeDeadSegments(float var1) {
        if (this.getNumEntries() != 0) {
            for (int var3 = this.getEntriesBegin(); var3 < this.getEntriesEnd(); ++var3) {
                aBu var2 = this.getEntry(var3);
                var2.lm(var1);
                if (var2.getTime() < 0.0F) {
                    this.removeEntry(var3);
                    --var3;
                } else {
                    var2.hhr -= var1;
                    if (var2.hhr < 0.0F) {
                        var2.hhr = 0.0F;
                    }
                }
            }
        }
    }

    void generateMesh(Vector3dOperations var1) {
        this.indexes.clear();
        this.vertexes.data().clear();
        this.mesh.getAabb().reset();
        long var2 = (long) this.getNumEntries();
        if (var2 >= 2L) {
            float var4 = (this.squaredDistance - this.DISTANCE_LOD_3) / (this.DISTANCE_LOD_4 - this.DISTANCE_LOD_3);
            if (var4 < 0.0F) {
                var4 = 0.0F;
            } else if (var4 > 1.0F) {
                var4 = 1.0F;
            }

            var4 = 1.0F - var4;
            if (var4 > 1.0E-4F) {
                this.textureTileStart = (float) ((double) this.textureTileStart + Math.floor((double) Math.abs(this.textureTileStart)));
                float var5 = this.textureTileStart;
                this.globalTransform.getX(tempVect);
                float var6 = this.xSize * 0.5F * tempVect.length();
                this.texCoord1.y = var5;
                this.texCoord2.y = var5;
                this.texCoord3.y = var5;
                int var7 = this.getEntriesEnd();
                int var8 = this.getEntriesBegin();
                norm0.aA(this.getLastEntry().crZ());

                while (var7 != var8) {
                    aBu var9 = this.getEntry(var7);
                    dir.x = (float) (var9.crZ().x - norm0.x);
                    dir.y = (float) (var9.crZ().y - norm0.y);
                    dir.z = (float) (var9.crZ().z - norm0.z);
                    dir2.x = (float) (var1.x - norm0.x);
                    dir2.y = (float) (var1.y - norm0.y);
                    dir2.z = (float) (var1.z - norm0.z);
                    if (dir.lengthSquared() >= 1.0F) {
                        break;
                    }
                    --var7;
                }

                float var20 = 1.0F / this.lifeTime;
                float var10 = 1.0F / (float) var2;
                float var11 = var6;
                dir.scale(5.0F);
                side.set(dir);
                side.c(dir2);
                side.normalize();
                if (!side.anA()) {
                    side.set(0.0F, 0.0F, 0.0F);
                }

                side.mT(var6);
                this.whiteAlpha1.w = this.getEntry(var7).getTime() * var20;
                m2.x = (float) (norm0.x - (double) side.x - this.globalTranslation.x);
                m2.y = (float) (norm0.y - (double) side.y - this.globalTranslation.y);
                m2.z = (float) (norm0.z - (double) side.z - this.globalTranslation.z);
                m3.x = (float) (norm0.x + (double) side.x - this.globalTranslation.x);
                m3.y = (float) (norm0.y + (double) side.y - this.globalTranslation.y);
                m3.z = (float) (norm0.z + (double) side.z - this.globalTranslation.z);
                if (this.currentLod > 1.0F) {
                    this.mesh.getAabb().reset();
                    this.addPointIgnoreAABB(m3, this.texCoord3, this.whiteAlpha1);
                    this.addPointIgnoreAABB(m2, this.texCoord1, this.whiteAlpha1);
                    this.indexes.setIndex(0, 1);
                    this.indexes.setIndex(1, 0);
                    this.indexes.setSize(2);
                } else {
                    this.mesh.getAabb().reset();
                    m1.x = (float) (norm0.x - this.globalTranslation.x);
                    m1.y = (float) (norm0.y - this.globalTranslation.y);
                    m1.z = (float) (norm0.z - this.globalTranslation.z);
                    this.addPointIgnoreAABB(m3, this.texCoord3, this.whiteAlpha1);
                    this.addPointIgnoreAABB(m1, this.texCoord2, this.whiteAlpha1);
                    this.addPointIgnoreAABB(m2, this.texCoord1, this.whiteAlpha1);
                }

                this.mesh.getAabb().aG(norm0);
                int var12 = 1;
                byte var13 = 1;
                if (this.currentLod > 3.0F) {
                    var13 = 8;
                } else if (this.currentLod > 2.0F) {
                    var13 = 5;
                } else if (this.currentLod > 1.0F) {
                    var13 = 3;
                }

                float var14 = 0.0F;
                float var15 = 0.0F;
                float var16 = 1.0F / (this.SCROLL_FACTOR * this.xSize);
                float var17 = 2.0F / (this.FADE_IN_RELATION * this.lifeTime);
                var7 = this.getEntriesEnd();
                this.globalTransform.getZ(tempVect);

                int var18;
                for (var18 = 1; var7 > var8; ++var12) {
                    aBu var19 = this.getEntry(var7);
                    norm1.x = norm0.x * (double) this.TRAIL_FILTER_VALUE;
                    norm1.y = norm0.y * (double) this.TRAIL_FILTER_VALUE;
                    norm1.z = norm0.z * (double) this.TRAIL_FILTER_VALUE;
                    norm1.x += var19.crZ().x * (double) (1.0F - this.TRAIL_FILTER_VALUE);
                    norm1.y += var19.crZ().y * (double) (1.0F - this.TRAIL_FILTER_VALUE);
                    norm1.z += var19.crZ().z * (double) (1.0F - this.TRAIL_FILTER_VALUE);
                    if (var12 % var13 != 0) {
                        norm0.aA(norm1);
                    } else {
                        dir.x = (float) (norm1.x - norm0.x);
                        dir.y = (float) (norm1.y - norm0.y);
                        dir.z = (float) (norm1.z - norm0.z);
                        dir2.x = (float) (var1.x - norm0.x);
                        dir2.y = (float) (var1.y - norm0.y);
                        dir2.z = (float) (var1.z - norm0.z);
                        side.set(dir);
                        side.c(dir2);
                        side.normalize();
                        if (dir.length() >= 0.3F && side.anA()) {
                            var5 += dir.length() * var16;
                            var14 = var19.getTime() * var20;
                            var15 = var6 * (1.0F + 4.0F * (1.0F - var14));
                            var11 = var11 * 0.95F + var15 * 0.05F;
                            side.mT(var11);
                            m2.x = (float) (norm1.x - (double) side.x - this.globalTranslation.x);
                            m2.y = (float) (norm1.y - (double) side.y - this.globalTranslation.y);
                            m2.z = (float) (norm1.z - (double) side.z - this.globalTranslation.z);
                            m3.x = (float) (norm1.x + (double) side.x - this.globalTranslation.x);
                            m3.y = (float) (norm1.y + (double) side.y - this.globalTranslation.y);
                            m3.z = (float) (norm1.z + (double) side.z - this.globalTranslation.z);
                            var14 *= ((float) var2 - (float) var12) * var10;
                            var14 -= var19.hhr * var17;
                            if (var14 > 1.0F) {
                                var14 = 1.0F;
                            } else if (var14 < 0.0F) {
                                var14 = 0.0F;
                            }

                            this.whiteAlpha0.w = this.whiteAlpha1.w;
                            this.whiteAlpha1.w = var14 * var4;
                            this.texCoord1.y = this.texCoord2.y = this.texCoord3.y = var5;
                            if (this.currentLod > 1.0F) {
                                this.addPointIgnoreAABB(m3, this.texCoord3, this.whiteAlpha1);
                                this.addPointIgnoreAABB(m2, this.texCoord1, this.whiteAlpha1);
                                this.indexes.ensure(var18 * 2);
                                this.indexes.setIndex(var18 * 2, var18 * 2 + 1);
                                this.indexes.setIndex(var18 * 2 + 1, var18 * 2);
                                this.indexes.setSize(var18 * 2);
                            } else {
                                m1.x = (float) (norm1.x - this.globalTranslation.x);
                                m1.y = (float) (norm1.y - this.globalTranslation.y);
                                m1.z = (float) (norm1.z - this.globalTranslation.z);
                                this.addPointIgnoreAABB(m3, this.texCoord3, this.whiteAlpha1);
                                this.addPointIgnoreAABB(m1, this.texCoord2, this.whiteAlpha1);
                                this.addPointIgnoreAABB(m2, this.texCoord1, this.whiteAlpha1);
                                this.indexes.ensure(var18 * 8);
                                this.indexes.setIndex(var18 * 8 - 8, var18 * 3 - 3);
                                this.indexes.setIndex(var18 * 8 - 7, var18 * 3);
                                this.indexes.setIndex(var18 * 8 - 6, var18 * 3 - 2);
                                this.indexes.setIndex(var18 * 8 - 5, var18 * 3 + 1);
                                this.indexes.setIndex(var18 * 8 - 4, var18 * 3 - 1);
                                this.indexes.setIndex(var18 * 8 - 3, var18 * 3 + 2);
                                this.indexes.setIndex(var18 * 8 - 2, var18 * 3 + 2);
                                this.indexes.setIndex(var18 * 8 - 1, var18 * 3);
                                this.indexes.setSize(var18 * 8);
                            }

                            this.mesh.getAabb().aG(norm1);
                            norm0.aA(norm1);
                            ++var18;
                        }
                    }
                    --var7;
                }

                if (this.currentLod > 1.0F) {
                    this.setCurrentPoints((var18 - 1) * 2);
                } else {
                    this.setCurrentPoints((var18 - 1) * 8);
                }

                tempVect.x = (float) this.mesh.getAabb().din().x + this.xSize;
                tempVect.y = (float) this.mesh.getAabb().din().y + this.xSize;
                tempVect.z = (float) this.mesh.getAabb().din().z + this.xSize;
                this.mesh.getAabb().addPoint(tempVect);
                tempVect.x -= (float) this.mesh.getAabb().dim().x + this.xSize;
                tempVect.y -= (float) this.mesh.getAabb().dim().y + this.xSize;
                tempVect.z -= (float) this.mesh.getAabb().dim().z + this.xSize;
                this.mesh.getAabb().addPoint(tempVect);
            }
        }
    }

    private void addPointIgnoreAABB(Vec3f var1, aQKa var2, Color var3) {
        this.vertexes.setPosition(this.actualIndex, var1.x, var1.y, var1.z);
        this.vertexes.setColor(this.actualIndex, var3.x, var3.y, var3.z, var3.w);
        this.vertexes.setTexCoord(this.actualIndex, 0, var2.x, var2.y);
        ++this.actualIndex;
    }

    void setActualPoint(int var1) {
        this.actualIndex = var1;
    }

    void generateAABB() {
        this.aabbWorldSpace.reset();
        int var1 = this.getNumEntries();
        if (var1 >= 2) {
            for (int var2 = 0; var2 < var1; ++var2) {
                aBu var3 = this.getEntry(var2);
                this.aabbWorldSpace.aG(var3.crZ());
            }

            float var4 = this.xSize * 0.5F * this.globalTransform.getVectorX().length() * 5.0F;
            point.x = this.aabbWorldSpace.din().x + (double) var4;
            point.y = this.aabbWorldSpace.din().y + (double) var4;
            point.z = this.aabbWorldSpace.din().z + (double) var4;
            this.aabbWorldSpace.aG(point);
            point.x = this.aabbWorldSpace.dim().x - (double) var4;
            point.y = this.aabbWorldSpace.dim().y - (double) var4;
            point.z = this.aabbWorldSpace.dim().z - (double) var4;
            this.aabbWorldSpace.aG(point);
        }
    }

    void checkForNewSegments(Vector3dOperations var1, float var2) {
        if (this.getNumEntries() != 0) {
            spawnDirection.x = (float) (var1.x - this.getLastEntry().crZ().x);
            spawnDirection.y = (float) (var1.y - this.getLastEntry().crZ().y);
            spawnDirection.z = (float) (var1.z - this.getLastEntry().crZ().z);
            this.textureTileStart -= 1.0F / (this.SCROLL_FACTOR * this.xSize * this.getInvLenght(spawnDirection));
            spawnDirection.normalize();
            this.globalTransform.getZ(tempVec);
            float var3 = -spawnDirection.dot(tempVec);
            if (var3 < 0.0F) {
                var3 = 0.0F;
            }

            var3 = this.clamp(var3, 0.0F, 1.0F);
            var3 = 1.0F - var3 * var3;
            var3 *= this.lifeTime * this.FADE_IN_RELATION;
            float var4 = 0.0F;
            tempVec.x = (float) (var1.x - this.lastAddedPosition.x);
            tempVec.y = (float) (var1.y - this.lastAddedPosition.y);
            tempVec.z = (float) (var1.z - this.lastAddedPosition.z);
            float var5 = tempVec.length();
            tempVec.x /= var5;
            tempVec.y /= var5;
            tempVec.z /= var5;
            if (this.getNumEntries() >= 2) {
                lastDir.x = (float) (this.lastAddedPosition.x - this.getEntry(this.getNumEntries() - 2).crZ().x);
                lastDir.y = (float) (this.lastAddedPosition.y - this.getEntry(this.getNumEntries() - 2).crZ().y);
                lastDir.z = (float) (this.lastAddedPosition.z - this.getEntry(this.getNumEntries() - 2).crZ().z);
                lastDir.normalize();
                var4 = lastDir.length() * tempVec.length();
            }

            float var6 = this.spawnDistance * this.currentLod;
            int var7 = (int) Math.floor((double) (var5 / (2.0F * var6)));
            if (var7 > this.MAX_TRAIL_SUBDIVISION) {
                var7 = this.MAX_TRAIL_SUBDIVISION;
            }

            if (var7 > 0) {
                float var8 = var5 / (float) var7;

                for (int var10 = 1; var10 < var7; ++var10) {
                    float var9 = this.lifeTime * this.lifeTimeModifier - var2 * (float) (var7 - var10) / (float) var7;
                    if (var9 > 0.0F) {
                        spawnVect.x = var1.x - (double) (tempVec.x * (float) (var7 - var10) * var8);
                        spawnVect.y = var1.y - (double) (tempVec.y * (float) (var7 - var10) * var8);
                        spawnVect.z = var1.z - (double) (tempVec.z * (float) (var7 - var10) * var8);
                        this.spawn(spawnVect, var3);
                        this.getFirstEntry().setTime(var9 + var2);
                    }

                    var5 -= var8;
                }
            }

            if (var5 > var6) {
                if (var7 == 0) {
                    this.getFirstEntry().X(this.lastAddedPosition);
                }

                this.spawn(var1, var3);
                this.getFirstEntry().ln(var2);
                this.lastAddedPosition.aA(var1);
            } else if (var4 < this.TRAIL_ANGLE_THRESHOLD) {
                if (var7 == 0) {
                    this.getFirstEntry().X(this.lastAddedPosition);
                }

                this.spawn(var1, var3);
                this.getFirstEntry().ln(var2);
                this.lastAddedPosition.aA(var1);
            } else {
                this.getLastEntry().X(var1);
            }
        } else {
            this.textureTileStart = (float) ((double) this.textureTileStart - Vector3dOperations.n(this.lastAddedPosition, var1) / (double) (this.SCROLL_FACTOR * this.xSize));
            this.spawn(var1, 0.0F);
        }

    }

    private float clamp(float var1, float var2, float var3) {
        var1 = var1 < var2 ? var2 : var1;
        var1 = var1 > var3 ? var3 : var1;
        return var1;
    }

    void spawn(Vector3dOperations var1, float var2) {
        if (var1 != this.lastAddedPosition) {
            this.tempEntry.X(var1);
            this.tempEntry.hhr = var2 * this.lifeTimeModifier;
            this.tempEntry.setTime(this.lifeTime * this.lifeTimeModifier);
            this.addEntry(this.tempEntry);
        }
    }

    void checkConsistency() {
        if (this.getNumEntries() >= 2 && this.currentLod <= 2.0F) {
            byte var1 = 0;
            int var2 = 0;
            lastPos.aA(this.getEntry(var1).crZ());
            int var5 = var1 + 1;
            lastDir3.x = (float) (this.getEntry(var5).crZ().x - lastPos.x);
            lastDir3.y = (float) (this.getEntry(var5).crZ().y - lastPos.y);
            lastDir3.z = (float) (this.getEntry(var5).crZ().z - lastPos.z);
            lastDir3.normalize();
            lastPos.aA(this.getEntry(var5).crZ());

            for (int var3 = var5++; var5 != this.getNumEntries() && var2 >= this.getNumEntries() - 2; ++var2) {
                if (this.getEntry(var5).crZ() == lastPos) {
                    boolean var4 = true;
                }

                curDir2.x = (float) (this.getEntry(var5).crZ().x - lastPos.x);
                curDir2.y = (float) (this.getEntry(var5).crZ().y - lastPos.y);
                curDir2.z = (float) (this.getEntry(var5).crZ().z - lastPos.z);
                curDir2.normalize();
                float var6 = curDir2.length() * lastDir.length();
                if (var6 < this.TRAIL_CUTOFF_COSINE) {
                    this.removeEntry(var3);
                    return;
                }

                var3 = var5;
                lastDir.set(curDir2);
                lastPos.aA(this.getEntry(var5).crZ());
                ++var5;
            }

        }
    }

    float getLodQuality() {
        return 1.0F;
    }

    public int step(StepContext var1) {
        Camera var2 = var1.getCamera();
        this.globalTransform.getTranslation(this.globalTranslation);
        var2.getGlobalTransform().getTranslation(cameraTranslation);
        if (this.lastAddedPosition == null) {
            this.lastAddedPosition = new Vector3dOperations(this.globalTranslation);
        }

        this.relativeTransform.setIdentity();
        this.relativeTransform.setTranslation(this.globalTranslation.x, this.globalTranslation.y, this.globalTranslation.z);
        localTranslation.x = this.globalTranslation.x - cameraTranslation.x;
        localTranslation.y = this.globalTranslation.y - cameraTranslation.y;
        localTranslation.z = this.globalTranslation.z - cameraTranslation.z;
        this.squaredDistance = 1.0F + this.getLodQuality() * 0.2F;
        this.squaredDistance = (float) ((double) this.squaredDistance * (localTranslation.lengthSquared() / (double) this.xSize));
        if (this.squaredDistance == 0.0F) {
            this.squaredDistance = 1.0F;
        }

        this.currentLod = 1.0F;
        if (this.squaredDistance > this.DISTANCE_LOD_4) {
            this.currentLod = 10.0F;
        } else if (this.squaredDistance > this.DISTANCE_LOD_3) {
            this.currentLod = 4.0F;
        } else if (this.squaredDistance > this.DISTANCE_LOD_2) {
            this.currentLod = 3.0F;
        } else if (this.squaredDistance > this.DISTANCE_LOD_1) {
            this.currentLod = 2.0F;
        }

        this.removeDeadSegments(var1.getDeltaTime());
        if (this.squaredDistance < this.DISTANCE_OUT_OF_RANGE) {
            this.checkConsistency();
            this.checkForNewSegments(this.globalTranslation, var1.getDeltaTime());
        }

        this.lastTick = System.currentTimeMillis();
        return 0;
    }

    public boolean render(RenderContext var1, boolean var2) {
        if (!this.render) {
            return false;
        } else if (!super.render(var1, var2)) {
            return false;
        } else {
            Camera var3 = var1.getCamera();
            boolean var4 = false;
            if (this.currentLod < 10.0F) {
                if (this.wasVisibleLastFrame) {
                    this.generateAABB();
                    var4 = true;
                } else {
                    this.generateAABB();
                    if (var1.getFrustum().a(this.aabbWorldSpace)) {
                        var4 = true;
                    }
                }
            } else {
                this.mesh.getAabb().reset();
            }

            this.wasVisibleLastFrame = false;
            if (var4) {
                this.generateMesh(var3.getPosition());
                if (var1.getFrustum().a(this.aabbWorldSpace)) {
                    this.wasVisibleLastFrame = true;
                }
            }

            if (var2 && !this.wasVisibleLastFrame && !var4) {
                this.aabbWorldSpace.g(this.mesh.getAabb());
            } else {
                float var5 = var1.getSizeOnScreen(this.globalTransform.position, this.aabbWSLenght);

                for (int var6 = 0; var6 < this.material.textureCount(); ++var6) {
                    BaseTexture var7 = this.material.getTexture(var6);
                    if (var7 != null) {
                        var7.addArea(var5);
                    }
                }

                var1.incTrailsRendered();
                this.mesh.setGeometryType(GeometryType.TRIANGLE_STRIP);
                var1.addPrimitive(this.material, this.mesh, this.relativeTransform, this.getPrimitiveColor(), this.occlusionQuery, this.renderPriority, this, this.aabbWorldSpace, this.currentSquaredDistanceToCamera);
                this.RenderedTrails = this.indexes.size();
                this.Trails = this.getNumEntries();
                this.numVertexes = this.vertexes.size();
                this.actualIndex = 0;
                this.tempVec3fCall = 0;
            }

            return true;
        }
    }

    public void clearEntries() {
        this.lastTick = 0L;
        this.entries2.clear();
    }

    public RenderAsset cloneAsset() {
        return new RTrail(this);
    }

    /**
     * @deprecated
     */
    @NotExported
    @Deprecated
    public Material getTrailMaterial() {
        return this.material;
    }

    /**
     * @deprecated
     */
    @Deprecated
    @NotExported
    public void setTrailMaterial(Material var1) {
        this.material = var1;
    }

    public float getLifeTimeModifier() {
        return this.lifeTimeModifier;
    }

    public void setLifeTimeModifier(float var1) {
        this.lifeTimeModifier = var1;
    }

    public float getMinSpeed() {
        return this.minSpeed;
    }

    public void setMinSpeed(float var1) {
        this.minSpeed = var1;
    }

    public aQKa getBillboardSize() {
        return this.billboardSize;
    }

    public void setBillboardSize(aQKa var1) {
        this.billboardSize.set(var1);
    }

    public Material getBillboardMaterial() {
        return this.billboardMaterial;
    }

    public void setBillboardMaterial(Material var1) {
        this.billboardMaterial = var1;
    }

    public float getTrailWidth() {
        return this.xSize;
    }

    public void setTrailWidth(float var1) {
        this.xSize = var1;
    }

    public float getLifeTime() {
        return this.lifeTime;
    }

    public void setLifeTime(float var1) {
        this.lifeTime = var1;
    }

    public boolean isUseLighting() {
        return this.useLighting;
    }

    public void setUseLighting(boolean var1) {
        this.useLighting = var1;
    }

    public void validate(SceneLoader var1) {
        super.validate(var1);
        if (this.material.getShader() == null) {
            this.material.setShader(var1.getDefaultTrailShader());
        }

    }

    public float getSpawnDistance() {
        return this.spawnDistance;
    }

    public void setSpawnDistance(float var1) {
        this.spawnDistance = var1;
    }

    public boolean isNeedToStep() {
        return true;
    }
}
