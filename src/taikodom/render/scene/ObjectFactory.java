package taikodom.render.scene;

import taikodom.render.data.ExpandableVertexData;
import taikodom.render.data.ShortIndexData;
import taikodom.render.data.VertexLayout;
import taikodom.render.primitives.Mesh;
import taikodom.render.shaders.Shader;
import taikodom.render.shaders.ShaderPass;
import taikodom.render.textures.Material;

public class ObjectFactory {
    private static ObjectFactory instance = new ObjectFactory();

    public static ObjectFactory getDefault() {
        return instance;
    }

    public RMesh createCube(float var1) {
        return this.createBox(var1, var1, var1);
    }

    public Material createDefaultMaterial() {
        Material var1 = new Material();
        Shader var2 = new Shader();
        ShaderPass var3 = new ShaderPass();
        var2.addPass(var3);
        var3.setLineMode(true);
        var3.setColorArrayEnabled(true);
        var3.setCullFaceEnabled(false);
        var1.setShader(var2);
        return var1;
    }

    public Material createDefaultMaterial0() {
        Material var1 = new Material();
        Shader var2 = new Shader();
        ShaderPass var3 = new ShaderPass();
        var2.addPass(var3);
        var3.setLineMode(true);
        var3.setColorArrayEnabled(true);
        var3.setCullFaceEnabled(false);
        var1.setShader(var2);
        return var1;
    }

    private RMesh createBox(float var1, float var2, float var3) {
        ExpandableVertexData var4 = new ExpandableVertexData(new VertexLayout(true, false, false, false, 0), 8);
        ShortIndexData var5 = new ShortIndexData(36);
        float var6 = var1 * 0.5F;
        float var7 = var2 * 0.5F;
        float var8 = var3 * 0.5F;
        var4.setPosition(0, -var6, -var7, -var8);
        var4.setPosition(1, var6, -var7, -var8);
        var4.setPosition(2, var6, var7, -var8);
        var4.setPosition(3, -var6, var7, -var8);
        var4.setPosition(4, -var6, -var7, var8);
        var4.setPosition(5, var6, -var7, var8);
        var4.setPosition(6, var6, var7, var8);
        var4.setPosition(7, -var6, var7, var8);
        int var9 = this.addQuad(var5, 0, 1, 0, 3, 2);
        var9 = this.addQuad(var5, var9, 4, 5, 6, 7);
        var9 = this.addQuad(var5, var9, 0, 1, 5, 4);
        var9 = this.addQuad(var5, var9, 1, 2, 6, 5);
        var9 = this.addQuad(var5, var9, 2, 3, 7, 6);
        this.addQuad(var5, var9, 3, 0, 4, 7);
        Mesh var10 = new Mesh();
        var10.setIndexes(var5);
        var10.setVertexData(var4);
        RMesh var11 = new RMesh();
        var11.setMesh(var10);
        return var11;
    }

    private int addQuad(ShortIndexData var1, int var2, int var3, int var4, int var5, int var6) {
        var1.setIndex(var2++, var3);
        var1.setIndex(var2++, var4);
        var1.setIndex(var2++, var5);
        var1.setIndex(var2++, var3);
        var1.setIndex(var2++, var5);
        var1.setIndex(var2++, var6);
        return var2;
    }
}
