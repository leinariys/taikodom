package taikodom.render.scene;

import all.Pt;
import taikodom.render.enums.LightType;
import taikodom.render.primitives.Light;

import java.util.*;

public class LightRecordList {
    private List data = new ArrayList();
    private LinkedList poolData = new LinkedList();

    public LightRecordList.LightRecord add() {
        LightRecordList.LightRecord var1;
        if (this.poolData.size() > 0) {
            var1 = (LightRecordList.LightRecord) this.poolData.removeLast();
            this.data.add(var1);
            return var1;
        } else {
            var1 = new LightRecordList.LightRecord();
            this.data.add(var1);
            return var1;
        }
    }

    public void clear() {
        this.poolData.addAll(this.data);
        this.data.clear();
    }

    public LightRecordList.LightRecord get(int var1) {
        return (LightRecordList.LightRecord) this.data.get(var1);
    }

    public int size() {
        return this.data.size();
    }

    public void sort() {
        Collections.sort(this.data, new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                return a((LightRecordList.LightRecord) o1, (LightRecordList.LightRecord) o2);
            }

            public int a(LightRecordList.LightRecord var1, LightRecordList.LightRecord var2) {
                Light var3 = var1.light;
                Light var4 = var2.light;
                if (var3.getLightType() == LightType.DIRECTIONAL_LIGHT) {
                    return -1;
                } else if (var4.getLightType() == LightType.DIRECTIONAL_LIGHT) {
                    return 1;
                } else if (var3.getLightType() == LightType.SPOT_LIGHT) {
                    return 1;
                } else if (var4.getLightType() == LightType.SPOT_LIGHT) {
                    return -1;
                } else if (var3.getLightType() == LightType.POINT_LIGHT && var4.getLightType() == LightType.POINT_LIGHT) {
                    return var3.getCurrentDistanceToCamera() - (double) var3.getRadius() > var4.getCurrentDistanceToCamera() - (double) var4.getRadius() ? 1 : -1;
                } else {
                    return 0;
                }
            }
        });
    }

    public class LightRecord {
        public final Pt cameraSpaceBoundingSphere = new Pt();
        public Light light;
    }
}
