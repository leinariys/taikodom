package taikodom.render.scene;

//import all.HO;
//import all.NP;
//import all.Nm;
//import all.QZ;

import all.*;
import org.lwjgl.BufferUtils;
import taikodom.render.RenderContext;
import taikodom.render.StepContext;
import taikodom.render.data.FloatMatrixData;
import taikodom.render.data.VertexData;
import taikodom.render.loader.RenderAsset;
import taikodom.render.primitives.Mesh;
import taikodom.render.raytrace.RayTraceSceneObjectInfoD;
import taikodom.render.scene.custom.RCustomMesh;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.*;

//import all.aMh;
//import all.aUB;
//import all.getLoaderImage;
//import all.arY;
//import all.wP;

/**
 * Реализация класса указанного в имени тегов из файлов с расширением .pro
 */
public class RModel extends SceneObject {
    public static final Vector3dOperations LOCAL_AABB_EXTEND = new Vector3dOperations(5.0D, 5.0D, 5.0D);
    private static final int GRANNY_PNG333_FLOAT_COUNT = 9;
    private static final FloatMatrixData tmp_mat_4x4 = new FloatMatrixData();
    private static final bc_q tmp_transform = new bc_q();
    private boolean hasActiveAnimation = false;
    private boolean needSkinning = false;
    private int maxMutableVertexCount = 0;
    private int numBones = 0;
    private float gameClock = 0.0F;
    private List boneTransforms = new ArrayList();
    private List subMeshes = new ArrayList();
    private List animations = new ArrayList();
    // private getLoaderImage grannyModel = null;
    // private arY grannyModelInstance = null;
    // private wP grannyWorldPose = null;
    // private NP skeleton = null;
    private FloatMatrixData placementMatrix = new FloatMatrixData(4, 4);
    // private Nm lastSeconds = new Nm();
    // private aUB grannyLocalPose = null;
    private FloatBuffer mutableVertexBuffer = null;
    private List socketHubs = new ArrayList();
    private List sockets = new ArrayList();
    private FloatBuffer compositeMatrixes;
    private Set alreadySkinned = new HashSet();
    private Map uniqueMeshes = new HashMap();
    private FloatBuffer tmpMeshBindingMatrices;

    public RModel() {
        /*HO.all(this.lastSeconds);*/
    }

    /*
       public RModel(getLoaderImage var1) {
          this.grannyModel = var1;
          if (var1 != null) {
             this.grannyModelInstance = HO.CreateJComponent(var1);
             this.skeleton = HO.CreateJComponentAndAddInRootPane(this.grannyModelInstance);
          }

          if (this.skeleton != null) {
             this.setNumBones(this.skeleton.bkG());
          }

          HO.all(this.lastSeconds);
       }
    */
    public RModel(RModel var1) {
        super(var1);
        this.setNumBones(var1.getNumBones());
        this.setMaxMutableVertexCount(var1.getMaxMutableVertexCount());
   /*   this.grannyModel = var1.grannyModel;
      if (this.grannyModel != null) {
         this.grannyModelInstance = HO.CreateJComponent(this.grannyModel);
         this.skeleton = HO.CreateJComponentAndAddInRootPane(this.grannyModelInstance);
      }
*/
        Iterator var3 = var1.subMeshes.iterator();

        while (var3.hasNext()) {
            RMesh var2 = (RMesh) var3.next();
            RMesh var4 = (RMesh) this.smartClone(var2);
            var4.setMesh(new Mesh(var4.getMesh()));
            this.addSubMesh(var4);
        }

        var3 = var1.boneTransforms.iterator();

        while (var3.hasNext()) {
            BoneTransform var5 = (BoneTransform) var3.next();
            this.boneTransforms.add(var5);
        }

        var3 = var1.sockets.iterator();

        while (var3.hasNext()) {
            Socket var6 = (Socket) var3.next();
            this.addSocket(new Socket(var6));
        }

        this.findUniqueGrannyMeshes();
        // HO.all(this.lastSeconds);
    }

    public void addBoneRotation(String var1, OrientationBase var2) {
        //  if (this.grannyModelInstance != null)
        {
            IntBuffer var3 = BufferUtils.createIntBuffer(1);
            // if (HO.setGreen((NP)this.skeleton, (String)var1, (Buffer)var3))
            {
                BoneTransform var4 = new BoneTransform(var3.get(0), new OrientationBase(var2));
                this.boneTransforms.add(var4);
            }
        }

    }

    public void addSubMesh(RMesh var1) {
        if (var1 != null) {
            this.subMeshes.add(var1);
            this.localAabb.h(var1.getAABB());
            this.localAabb.a(this.globalTransform.toQuaternion(), this.globalTransform.position, this.aabbWorldSpace);
            this.aabbLSLenght = this.localAabb.din().ax(this.localAabb.dim());
            this.aabbWSLenght = this.aabbWorldSpace.din().ax(this.aabbWorldSpace.dim());
            var1.updateInternalGeometry(this);
            Mesh var2 = var1.getMesh();
            if (var2 != null) {
                //  NP var3 = var2.getGrannySkeleton();
                // if (var3 != null && !var3.equals(this.skeleton))
                {
                    ato var4 = var2.getGrannyBinding();
                    //  var4 = HO.all(var2.getGrannyMesh(), var3, this.skeleton);
                    var2.setGrannyBinding(var4);
                }
            }

            this.findUniqueGrannyMeshes();
        }

    }

    public void removeSubMesh(RMesh var1) {
        if (var1 != null) {
            this.subMeshes.remove(var1);
            this.findUniqueGrannyMeshes();
            this.updateInternalGeometry((SceneObject) null);
        }

    }

    public boolean animate() {
        if (!this.hasActiveAnimation && this.boneTransforms.size() == 0) {
            return false;
        } else {
            // Nm var1 = new Nm();
            // HO.all(var1);
            //  this.lastSeconds = var1;
            //  HO.all(this.grannyModelInstance, this.gameClock);
            if (this.hasActiveAnimation) {
                //    HO.setGreen(this.grannyModelInstance, 0, this.numBones, this.grannyLocalPose);
            }

            Iterator var3 = this.boneTransforms.iterator();

            while (var3.hasNext()) {
                BoneTransform var2 = (BoneTransform) var3.next();
                //    aMh var4 = HO.all(this.grannyLocalPose, var2.getIndex());
                //     HO.all(var4, var2.getOrientation().x, var2.getOrientation().y, var2.getOrientation().z, var2.getOrientation().w);
            }

            // HO.all((NP)this.skeleton, 0, this.numBones, (aUB)this.grannyLocalPose, (Buffer)this.placementMatrix.buffer(), (wP)this.grannyWorldPose);
            this.hasActiveAnimation = false;
            this.needSkinning = true;
            // HO.setGreen((wP)this.grannyWorldPose, this.numBones, (Buffer)this.compositeMatrixes);
            this.localAabb.reset();

            for (int var5 = 0; var5 < this.numBones; ++var5) {
                //  HO.all(var5, (Buffer)this.compositeMatrixes, (Buffer)tmp_mat_4x4.buffer());
                tmp_mat_4x4.getTransform(tmp_transform);
                this.localAabb.aG(tmp_transform.position);
            }

            this.localAabb.G(LOCAL_AABB_EXTEND.x, LOCAL_AABB_EXTEND.y, LOCAL_AABB_EXTEND.z);
            var3 = this.socketHubs.iterator();

            while (var3.hasNext()) {
                SocketHub var6 = (SocketHub) var3.next();
                //   HO.all(var6.getBoneIndex(), (Buffer)this.compositeMatrixes, (Buffer)tmp_mat_4x4.buffer());
                tmp_mat_4x4.getTransform(tmp_transform);
                var6.setTransform(tmp_transform);
            }

            return true;
        }
    }

    public void releaseReferences() {
        super.releaseReferences();
        this.removeAllAnimations();
        this.uniqueMeshes.clear();
        this.subMeshes.clear();
    }

    private boolean deformVerts() {
        //  HO.all((wP)this.grannyWorldPose, this.numBones, (Buffer)this.compositeMatrixes);
        this.needSkinning = false;
        Iterator var2 = this.subMeshes.iterator();

        while (true) {
            while (true) {
                RMesh var1;
                Mesh var3;
                // do {
                do {
                    if (!var2.hasNext()) {
                        this.alreadySkinned.clear();
                        return true;
                    }

                    var1 = (RMesh) var2.next();
                    var3 = var1.getMesh();
                } while (var3 == null);
                // } while(var3.getGrannyMesh() == null);

                // QZ var4 = var3.getGrannyMesh();
                VertexData var5 = var3.getVertexData();
                ato var6 = var3.getGrannyBinding();
                aqE var7 = var3.getGrannyDeformer();
                //  ab_q var8 = HO.endPage(var6);
                //  int var9 = HO.CreateJComponentAndAddInRootPane(var4);
                if (var3.isRidig()) {
                    //     HO.all((ab_q)var8, (Buffer)this.compositeMatrixes, (Buffer)tmp_mat_4x4.buffer());
                    tmp_mat_4x4.getTransform(tmp_transform);
                    var1.setTransform(tmp_transform);
                } else if (var7 != null) {
                    if (this.mutableVertexBuffer == null) {
                        return false;
                    }
/*
               if (!this.alreadySkinned.contains(var4))
               {
                  this.alreadySkinned.add(var4);
                  aLt var10 = HO.endPage(var4);
                  if (!HO.setGreen(var6)) {
                     HO.all(var7, var8, this.compositeMatrixes, var9, var10, this.mutableVertexBuffer);
                  } else {
                     int var11 = HO.CreateJComponent(var6);
                     if (this.tmpMeshBindingMatrices == null || this.tmpMeshBindingMatrices.capacity() < 16 * var11) {
                        this.tmpMeshBindingMatrices = BufferUtils.createFloatBuffer(16 * var11);
                     }

                     HO.all((ato)var6, (wP)this.grannyWorldPose, 0, HO.CreateJComponent(var6), (Buffer)this.tmpMeshBindingMatrices);
                     HO.all(var7, (ab_q)null, this.tmpMeshBindingMatrices, var9, var10, this.mutableVertexBuffer);
                  }

                  this.copyDeformedVertexes(var5, var9);
               }
*/
                    if (var1 instanceof RCustomMesh) {
                        ((RCustomMesh) var1).weldParts(false);
                    }
                }
            }
        }
    }

    private void copyDeformedVertexes(VertexData var1, int var2) {
        for (int var3 = 0; var3 < var2; ++var3) {
            var1.setPosition(var3, this.mutableVertexBuffer.get(var3 * 9), this.mutableVertexBuffer.get(var3 * 9 + 1), this.mutableVertexBuffer.get(var3 * 9 + 2));
            var1.setNormal(var3, this.mutableVertexBuffer.get(var3 * 9 + 3), this.mutableVertexBuffer.get(var3 * 9 + 3 + 1), this.mutableVertexBuffer.get(var3 * 9 + 3 + 2));
            var1.setTangents(var3, this.mutableVertexBuffer.get(var3 * 9 + 6), this.mutableVertexBuffer.get(var3 * 9 + 6 + 1), this.mutableVertexBuffer.get(var3 * 9 + 6 + 2));
        }

    }

    public void checkMaxMutableVertexCount(int var1) {
        if (this.maxMutableVertexCount < var1) {
            this.setMaxMutableVertexCount(var1);
        }

    }

    public void clearBoneRotations() {
        this.boneTransforms.clear();
    }

    public void clearSubMeshes() {
        this.subMeshes.clear();
        this.localAabb.reset();
        this.aabbWorldSpace.reset();
        this.uniqueMeshes.clear();
    }

    public RenderAsset cloneAsset() {
        return new RModel(this);
    }

    public void setSubMesh(int var1, RMesh var2) {
        if (var2 != null) {
            this.subMeshes.set(var1, var2);
            this.localAabb.h(var2.getAABB());
            this.localAabb.a(this.globalTransform.toQuaternion(), this.globalTransform.position, this.aabbWorldSpace);
            this.aabbLSLenght = this.localAabb.din().ax(this.localAabb.dim());
            this.aabbWSLenght = this.aabbWorldSpace.din().ax(this.aabbWorldSpace.dim());
            this.findUniqueGrannyMeshes();
        }

    }

    protected void finalize() {
  /*    super.finalize();
      if (this.grannyModelInstance != null) {
         HO.CreateJComponent(this.grannyModelInstance);
      }

      if (this.grannyWorldPose != null) {
         HO.setGreen(this.grannyWorldPose);
      }

      if (this.grannyLocalPose != null) {
         HO.all(this.grannyLocalPose);
      }
*/
    }

    public float getGameClock() {
        return this.gameClock;
    }

    /*
       public getLoaderImage getGrannyModel() {
          return this.grannyModel;
       }
    *//*
   public arY getGrannyModelInstance() {
      return this.grannyModelInstance;
   }
*/
    public int getMaxMutableVertexCount() {
        return this.maxMutableVertexCount;
    }

    public void setMaxMutableVertexCount(int var1) {
        if (this.mutableVertexBuffer != null) {
            this.mutableVertexBuffer = null;
        }

        this.maxMutableVertexCount = var1;
        if (var1 != 0) {
            this.mutableVertexBuffer = BufferUtils.createFloatBuffer(this.maxMutableVertexCount * 9);
        }

    }

    public int getNumBones() {
        return this.numBones;
    }

    public void setNumBones(int var1) {
        if (var1 != 0) {
            this.numBones = var1;
      /*   if (this.grannyWorldPose != null) {
            HO.setGreen(this.grannyWorldPose);
         }
*/
      /*   if (this.grannyLocalPose != null) {
            HO.all(this.grannyLocalPose);
         }

         this.grannyWorldPose = HO.hx(this.numBones);
         this.grannyLocalPose = HO.hu(this.numBones);*/
            this.compositeMatrixes = BufferUtils.createFloatBuffer(this.numBones * 16);
        }

    }

    /*
       public NP getSkeleton() {
          return this.skeleton;
       }
    */
    public List getSubMeshes() {
        return this.subMeshes;
    }

    public RayTraceSceneObjectInfoD rayIntersects(RayTraceSceneObjectInfoD var1, boolean var2, boolean var3) {
        var1.setChanged(false);
        super.rayIntersects(var1, var2, var3);
        if (var1.isIntersected() && var1.isChanged()) {
            return var1;
        } else {
            Vector3dOperations var4 = new Vector3dOperations();
            Vector3dOperations var5 = new Vector3dOperations();
            bc_q var6 = new bc_q();
            this.globalTransform.a(var6);
            var6.multiply3x4(var1.getStartPos(), var4);
            var6.multiply3x4(var1.getEndPos(), var5);
            gu_g var7 = new gu_g(var4.getVec3f(), var5.getVec3f());
            Iterator var9 = this.subMeshes.iterator();

            while (true) {
                do {
                    do {
                        Mesh var10;
                        do {
                            if (!var9.hasNext()) {
                                return var1;
                            }

                            RMesh var8 = (RMesh) var9.next();
                            var10 = var8.getMesh();
                        } while (var10 == null);

                        var10.rayIntersects(var7);
                    } while (!var7.isIntersected());
                } while (var2 && (double) var7.vM() >= var1.getParamIntersection());

                var1.set(var7);
                var1.setChanged(true);
                var1.setIntersectedObject(this);
            }
        }
    }

    public void updateInternalGeometry(SceneObject var1) {
        if (var1 != null) {
            this.parentTransform.b(var1.getGlobalTransform());
            this.globalTransform.a(this.parentTransform, this.transform);
        } else {
            this.globalTransform.a(this.parentTransform, this.transform);
        }

        this.globalTransform.B(this.scaling.x);
        this.globalTransform.C(this.scaling.y);
        this.globalTransform.D(this.scaling.z);
        Iterator var3 = this.subMeshes.iterator();

        while (var3.hasNext()) {
            RMesh var2 = (RMesh) var3.next();
            var2.updateInternalGeometry(this);
        }

        float var5 = this.scaling.x;
        float var6 = this.scaling.y;
        float var4 = this.scaling.z;
        this.scaling.set(1.0F, 1.0F, 1.0F);
        super.updateInternalGeometry(var1);
        this.scaling.set(var5, var6, var4);
    }
/*
   public void setGrannyModel(getLoaderImage var1) {
      this.grannyModel = var1;
      if (this.grannyModelInstance != null) {
         HO.CreateJComponent(this.grannyModelInstance);
      }

      this.grannyModelInstance = HO.CreateJComponent(var1);
   }*/

    protected void addChildrenWorldSpaceAABB() {
        super.addChildrenWorldSpaceAABB();
        Iterator var2 = this.subMeshes.iterator();

        while (var2.hasNext()) {
            RMesh var1 = (RMesh) var2.next();
            this.aabbWorldSpace.h(var1.getAabbWorldSpace());
        }

    }

    public boolean render(RenderContext var1, boolean var2) {
        if (this.render) {
            if (!super.render(var1, var2)) {
                return false;
            } else {
                this.localAabb.a(this.globalTransform, this.aabbWorldSpace);
                if (!var2 || var1.getFrustum().a(this.aabbWorldSpace)) {
                    if (this.needSkinning) {
                        this.deformVerts();
                    }

                    Iterator var4 = this.subMeshes.iterator();

                    while (var4.hasNext()) {
                        RMesh var3 = (RMesh) var4.next();
                        var3.render(var1, false);
                    }
                }

                this.aabbLSLenght = this.localAabb.din().ax(this.localAabb.dim());
                this.aabbWSLenght = this.aabbWorldSpace.din().ax(this.aabbWorldSpace.dim());
                return true;
            }
        } else {
            return false;
        }
    }

    public void setHasActiveAnimation(boolean var1) {
        this.hasActiveAnimation = var1;
    }

    public int step(StepContext var1) {
        this.gameClock += var1.getDeltaTime();
        this.stepAnimations(var1);
        int var2;
        RMesh var3;
        if (this.animate()) {
            for (var2 = 0; var2 < this.subMeshes.size(); ++var2) {
                var3 = (RMesh) this.subMeshes.get(var2);
                if (!var3.getMesh().isRidig()) {
                    var3.getAABB().g(this.localAabb);
                    var3.computeLocalSpaceAABB();
                    var3.computeWorldSpaceAABB();
                }

                this.localAabb.h(var3.getAABB());
            }
        } else {
            this.localAabb.reset();

            for (var2 = 0; var2 < this.subMeshes.size(); ++var2) {
                var3 = (RMesh) this.subMeshes.get(var2);
                this.localAabb.h(var3.getAABB());
            }
        }

        this.localAabb.a(this.globalTransform, this.aabbWorldSpace);
        this.aabbLSLenght = this.localAabb.din().ax(this.localAabb.dim());
        this.aabbWSLenght = this.aabbWorldSpace.din().ax(this.aabbWorldSpace.dim());
        return 0;
    }

    private void stepAnimations(StepContext var1) {
        for (int var2 = this.animations.size() - 1; var2 >= 0; --var2) {
            RAnimation var3 = (RAnimation) this.animations.get(var2);
            var3.step(var1);
            if (var3.isDisposed()) {
                this.animations.remove(var2);
            }
        }

    }

    public boolean isNeedToStep() {
        return true;
    }

    public SocketHub getHub(String var1) {
        Iterator var3 = this.socketHubs.iterator();

        SocketHub var2;
        while (var3.hasNext()) {
            var2 = (SocketHub) var3.next();
            if (var2.getName().equals(var1)) {
                return var2;
            }
        }

        var2 = new SocketHub(this, var1);
        this.addChild(var2);
        this.socketHubs.add(var2);
        return var2;
    }

    public SocketHub getHub(int var1) {
        Iterator var3 = this.socketHubs.iterator();

        SocketHub var2;
        while (var3.hasNext()) {
            var2 = (SocketHub) var3.next();
            if (var2.getBoneIndex() == var1) {
                return var2;
            }
        }

        var2 = new SocketHub(this, var1);
        this.addChild(var2);
        this.socketHubs.add(var2);
        return var2;
    }

    public void addSocket(Socket var1) {
        this.sockets.add(var1);
        var1.setRModel(this);
    }

    public Socket getSocket(int var1) {
        return (Socket) this.sockets.get(var1);
    }

    public void setSocket(int var1, Socket var2) {
        this.sockets.add(var2);
        var2.setRModel(this);
    }

    public void removeSocket(Socket var1) {
        if (this.sockets.remove(var1)) {
            var1.unlink();
        }

    }

    public int socketCount() {
        return this.sockets.size();
    }

    public void addAnimation(RAnimation var1) {
        this.animations.add(var1);
        var1.setModel(this);
    }

    public RAnimation getAnimation(String var1) {
        Iterator var3 = this.animations.iterator();

        while (var3.hasNext()) {
            RAnimation var2 = (RAnimation) var3.next();
            if (var2.getName().equals(var1)) {
                return var2;
            }
        }

        return null;
    }

    public void removeAnimation(RAnimation var1) {
        this.animations.remove(var1);
    }

    public void removeAnimation(String var1) {
        RAnimation var2 = this.getAnimation(var1);
        if (var2 != null) {
            this.removeAnimation(var2);
        }

    }

    public void removeAllAnimations() {
        Iterator var2 = this.animations.iterator();

        while (var2.hasNext()) {
            RAnimation var1 = (RAnimation) var2.next();
            var1.setFadeTime(0.0F);
            var1.stop();
        }

        this.animations.clear();
    }

    private void findUniqueGrannyMeshes() {
        HashSet var1 = new HashSet();
        Iterator var3 = this.subMeshes.iterator();

        while (var3.hasNext()) {
            RMesh var2 = (RMesh) var3.next();
            Mesh var4 = var2.getMesh();
            // var1.add(var4.getGrannyMesh());
            VertexData var5;
            // if (!this.uniqueMeshes.containsKey(var4.getGrannyMesh()))
            {
                if (!var4.isRidig()) {
                    var5 = var4.getVertexData().duplicate();
                    //  this.uniqueMeshes.put(var4.getGrannyMesh(), var5);
                    var4.setVertexData(var5);
                    var4.invalidateHandle();
                } else {
                    //  this.uniqueMeshes.put(var4.getGrannyMesh(), var4.getVertexData());
                }
            } //else
            if (!var4.isRidig()) {
                // var5 = (VertexData)this.uniqueMeshes.get(var4.getGrannyMesh());
                //  var4.setVertexData(var5);
            }
        }

        HashSet var6 = new HashSet();
        Iterator var8 = this.uniqueMeshes.keySet().iterator();

        // QZ var7;
        while (var8.hasNext()) {
       /*  var7 = (QZ)var8.next();
         if (!var1.contains(var7)) {
            var6.add(var7);
         }*/
        }

        var8 = var6.iterator();

        while (var8.hasNext()) {
      /*  var7 = (QZ)var8.next();
         this.uniqueMeshes.remove(var7);*/
        }

    }
}
