package taikodom.render.scene;

/**
 * Реализация класса указанного в имени тегов из файлов с расширением .pro
 */
public class Socket extends SceneObject {
    private SocketHub hub;
    private RModel rmodel;
    private String hubName = "";

    public Socket(Socket var1) {
        super(var1);
        this.rmodel = var1.rmodel;
        this.hub = var1.hub;
        this.hubName = var1.hubName;
    }

    public Socket() {
    }

    public Socket(String var1) {
        this.hubName = var1;
    }

    public Socket(RModel var1, String var2) {
        this.hubName = var2;
        var1.addSocket(this);
    }

    public SocketHub getHub() {
        return this.hub;
    }

    private void setHub(SocketHub var1) {
        if (this.hub != var1) {
            if (this.hub != null) {
                this.hub.removeSocket(this);
            }

            this.hub = var1;
            if (this.hub != null) {
                this.hub.addChild(this);
            }
        }

    }

    public String getHubName() {
        return this.hubName;
    }

    public void setHubName(String var1) {
        if (!var1.isEmpty()) {
            this.hubName = var1;
            if (this.rmodel != null) {
                this.setHub(this.rmodel.getHub(var1));
            }

        }
    }

    public void setRModel(RModel var1) {
        if (this.rmodel != var1) {
            if (this.rmodel != null) {
                this.rmodel.removeSocket(this);
            }

            this.rmodel = var1;
            if (this.rmodel != null) {
                this.setHubName(this.hubName);
            } else {
                this.setHub((SocketHub) null);
            }
        }

    }

    public RModel gerRModel() {
        return this.rmodel;
    }

    public void unlink() {
        this.rmodel = null;
        this.setHub((SocketHub) null);
    }
}
