package taikodom.render.scene;

import all.Vector3dOperations;
import all.aLH;
import all.ajD;
import com.hoplon.geometry.Vec3f;
import taikodom.render.RenderContext;
import taikodom.render.loader.RenderAsset;
import taikodom.render.primitives.Mesh;
import taikodom.render.textures.BaseTexture;

/**
 * Реализация класса указанного в имени тегов из файлов с расширением .pro
 */
public class RFlowerShot extends RenderObject {
    protected static ajD tempMat3 = new ajD();
    protected final Vec3f shotSize = new Vec3f(1.0F, 1.0F, 1.0F);
    protected Mesh shotMesh;

    public RFlowerShot() {
    }

    public RFlowerShot(RFlowerShot var1) {
        super(var1);
        this.setShotMesh(var1.shotMesh);
        this.setShotSize(var1.shotSize);
    }

    public RenderAsset cloneAsset() {
        return new RFlowerShot(this);
    }

    public Mesh getShotMesh() {
        return this.shotMesh;
    }

    public void setShotMesh(Mesh var1) {
        this.shotMesh = var1;
        if (this.shotMesh != null) {
            this.localAabb.g(this.shotMesh.getAabb());
        }

    }

    public Vec3f getShotSize() {
        return this.shotSize;
    }

    public void setShotSize(Vec3f var1) {
        this.shotSize.set(var1);
        this.updateInternalGeometry((SceneObject) null);
    }

    public void updateInternalGeometry(SceneObject var1) {
        this.globalTransform.setIdentity();
        tempMat3.m00 = this.shotSize.x;
        tempMat3.m11 = this.shotSize.y;
        tempMat3.m22 = this.shotSize.z;
        this.globalTransform.mX.mul(this.transform.mX, tempMat3);
        this.globalTransform.setTranslation(this.transform.position);
        this.localAabb.a(this.globalTransform, this.aabbWorldSpace);
    }

    public void setPosition(double var1, double var3, double var5) {
        this.aabbWorldSpace.D(var1 - this.globalTransform.position.x, var3 - this.globalTransform.position.y, var5 - this.globalTransform.position.z);
        this.transform.position.set(var1, var3, var5);
        this.globalTransform.position.set(var1, var3, var5);
    }

    public void setPosition(Vector3dOperations var1) {
        this.setPosition(var1.x, var1.y, var1.z);
    }

    public boolean render(RenderContext var1, boolean var2) {
        if (this.render) {
            this.currentSquaredDistanceToCamera = this.globalTransform.g(var1.getCamera().globalTransform);
            if (this.maxVisibleDistance > 0.0D && this.currentSquaredDistanceToCamera > this.maxSquaredVisibleDistance) {
                return false;
            } else {
                if (!var2 || var1.getFrustum().a(this.aabbWorldSpace)) {
                    BaseTexture var3 = null;
                    if (this.material != null) {
                        var3 = this.material.getDiffuseTexture();
                    }

                    if (var3 != null) {
                        var3.addArea(1000.0F);
                    }

                    var1.addPrimitive(this.material, this.shotMesh, this.globalTransform, this.primitiveColor, this.occlusionQuery, this.renderPriority, this, this.aabbWorldSpace, this.currentSquaredDistanceToCamera);
                    var1.incMeshesRendered();
                }

                return true;
            }
        } else {
            return false;
        }
    }

    public aLH getAABB() {
        return this.shotMesh != null ? this.shotMesh.getAabb() : this.localAabb;
    }
}
