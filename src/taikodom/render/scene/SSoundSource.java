package taikodom.render.scene;

import all.aQKa;
import com.hoplon.geometry.Vec3f;
import taikodom.render.MilesSoundSource;
import taikodom.render.SoundBuffer;
import taikodom.render.StepContext;
import taikodom.render.loader.RenderAsset;

/**
 * Реализация класса указанного в имени тегов из файлов с расширением .pro
 */
public class SSoundSource extends SoundObject {
    public static float MIN_AUDIBLE_VOLUME = -35.0F;
    final aQKa distances = new aQKa();
    final Vec3f velocity = new Vec3f();
    final Vec3f position = new Vec3f();
    float gain;
    boolean is3d;
    float pitch;
    float audibleRadius;
    boolean disposeAfterPlaying = true;
    private SoundSource source = null;
    private int loopCount;

    private SSoundSource(SSoundSource var1) {
        super(var1);
        this.loopCount = var1.loopCount;
        this.gain = var1.gain;
        this.distances.set(var1.distances);
        this.velocity.set(var1.velocity);
        this.pitch = var1.pitch;
        this.audibleRadius = var1.audibleRadius;
        this.is3d = var1.is3d;
        this.source = new MilesSoundSource();
        this.source.setBuffer(var1.getBuffer());
        this.setBuffer(var1.getBuffer());
        this.disposeAfterPlaying = var1.disposeAfterPlaying;
    }

    public SSoundSource() {
        this.loopCount = 1;
        this.gain = 1.0F;
        this.distances.x = 50.0F;
        this.distances.y = 1000.0F;
        this.pitch = 1.0F;
        this.audibleRadius = 1000.0F;
        this.is3d = false;
        this.source = new MilesSoundSource();
    }

    public void setDisposeAfterPlaying(boolean var1) {
        this.disposeAfterPlaying = var1;
    }

    public void releaseReferences() {
        super.releaseReferences();
        this.stop();
        if (this.source != null) {
            this.source.releaseReferences();
            this.source = null;
        }

    }

    public void dispose() {
        this.stop();
        this.disposed = true;
    }

    public int getLoopCount() {
        return this.loopCount;
    }

    public void setLoopCount(int var1) {
        this.loopCount = var1;
        if (this.source != null) {
            this.source.setLoop(this.loopCount);
        }

    }

    public float getGain() {
        return this.gain;
    }

    public void setGain(float var1) {
        this.gain = var1;
        if (this.source != null) {
            this.source.setGain(var1);
        }

    }

    public void setPitch(float var1) {
        this.pitch = var1;
        if (this.source != null) {
            this.source.setPitch(var1);
        }

    }

    public boolean play() {
        if (this.source != null) {
            if (this.source.getBuffer() == null) {
                return false;
            } else {
                this.firstFrame = false;
                if (this.source.isPlaying()) {
                    return this.source.play(this.loopCount);
                } else if (!this.source.create()) {
                    return false;
                } else {
                    this.source.setGain(this.gain);
                    this.source.setPitch(this.pitch);
                    this.source.setMinMaxDistance(this.distances.x, this.distances.y);
                    this.source.setLoop(this.loopCount);
                    if (this.is3d) {
                        this.transform.getTranslation(this.position);
                        this.source.setPosition(this.position);
                        this.source.setVelocity(this.velocity);
                    }

                    this.source.play(this.loopCount);
                    return true;
                }
            }
        } else {
            return false;
        }
    }

    public boolean fadeOut(float var1) {
        return this.source != null ? this.source.fadeOut(var1) : false;
    }

    public boolean stop() {
        return this.source != null ? this.source.stop() : false;
    }

    public boolean isPlaying() {
        return this.source != null ? this.source.isPlaying() : false;
    }

    public boolean isValid() {
        return this.source != null ? this.source.isValid() : false;
    }

    public SoundSource getSource() {
        return this.source;
    }

    public void setSource(SoundSource var1) {
        this.source = var1;
    }

    public SoundBuffer getBuffer() {
        return this.source.getBuffer();
    }

    public void setBuffer(SoundBuffer var1) {
        this.source.setBuffer(var1);
    }

    public boolean isAlive() {
        return this.isPlaying();
    }

    public float getAudibleRadius() {
        return this.audibleRadius;
    }

    public void setAudibleRadius(float var1) {
        this.audibleRadius = var1;
    }

    public int step(StepContext var1) {
        if (this.firstFrame) {
            if (this.is3d && this.loopCount != 0) {
                double var2 = var1.getCamera().getGlobalTransform().g(this.globalTransform);
                float var4 = this.getMaxListenDistance();
                if (var2 > (double) (var4 * var4)) {
                    if (this.disposeAfterPlaying) {
                        this.dispose();
                        this.releaseReferences();
                    }

                    return 1;
                }
            }

            this.play();
            this.firstFrame = false;
        }

        if (this.source != null) {
            if (this.is3d) {
                this.position.x = (float) (this.globalTransform.position.x - var1.getCamera().getGlobalTransform().position.x);
                this.position.y = (float) (this.globalTransform.position.y - var1.getCamera().getGlobalTransform().position.y);
                this.position.z = (float) (this.globalTransform.position.z - var1.getCamera().getGlobalTransform().position.z);
                this.source.setPosition(this.position);
                this.source.setVelocity(this.velocity);
            }

            if (this.source.step(var1) != 0) {
                if (this.disposeAfterPlaying) {
                    this.dispose();
                    this.releaseReferences();
                }

                return 1;
            } else {
                return 0;
            }
        } else {
            return 1;
        }
    }

    public void reset() {
        this.play();
    }

    public aQKa getDistances() {
        return this.distances;
    }

    public void setDistances(aQKa var1) {
        this.distances.set(var1);
        if (this.distances.x <= 0.0F) {
            this.distances.x = 0.01F;
        }

        if (this.source != null) {
            this.source.setMinMaxDistance(this.distances.x, this.distances.y);
        }

    }

    public boolean getIs3d() {
        return this.is3d;
    }

    public void setIs3d(boolean var1) {
        this.is3d = var1;
    }

    public void onRemove() {
        super.onRemove();
        this.stop();
    }

    public int getTotalMilesSounds() {
        return this.source != null ? this.source.getTotalMilesSounds() : 0;
    }

    public RenderAsset cloneAsset() {
        return new SSoundSource(this);
    }

    public void setSquaredSoundSource(SceneObject var1) {
        throw new RuntimeException("Must implement this");
    }

    public float getMaxListenDistance() {
        if (this.is3d) {
            float var1 = (float) Math.pow(10.0D, (double) (MIN_AUDIBLE_VOLUME / 20.0F));
            float var2 = this.distances.x / var1;
            return var2;
        } else {
            return -1.0F;
        }
    }

    public float getTimeLenght() {
        return this.source != null ? this.source.getTimeLenght() : 0.0F;
    }

    public float getSamplePosition() {
        return this.source != null ? (float) this.source.getSamplePositionMs() : 0.0F;
    }
}
