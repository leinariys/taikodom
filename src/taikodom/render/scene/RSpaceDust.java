package taikodom.render.scene;

import all.*;
import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import taikodom.render.RenderContext;
import taikodom.render.StepContext;
import taikodom.render.camera.Camera;
import taikodom.render.data.ExpandableIndexData;
import taikodom.render.data.ExpandableVertexData;
import taikodom.render.data.VertexLayout;
import taikodom.render.enums.GeometryType;
import taikodom.render.loader.RenderAsset;
import taikodom.render.loader.SceneLoader;
import taikodom.render.primitives.Mesh;
import taikodom.render.textures.BaseTexture;

import javax.vecmath.Tuple3f;

/**
 * Реализация класса указанного в имени тегов из файлов с расширением .pro
 */
public class RSpaceDust extends RenderObject {
    static final aQKa tex1 = new aQKa(0.0F, 0.0F);
    static final aQKa tex2 = new aQKa(1.0F, 0.0F);
    static final aQKa tex3 = new aQKa(1.0F, 1.0F);
    static final aQKa tex4 = new aQKa(0.0F, 1.0F);
    static final Vec3f d1 = new Vec3f();
    static final Vec3f d2 = new Vec3f();
    static final Vec3f d3 = new Vec3f();
    static final Vec3f d4 = new Vec3f();
    static final bc_q ticamera = new bc_q();
    static final Vec3f v1 = new Vec3f();
    static final Vec3f v2 = new Vec3f();
    static final Vec3f camPointDist = new Vec3f();
    static final Vec3f Z = new Vec3f();
    static final Vec3f Y = new Vec3f();
    static final Color white = new Color(1.0F, 1.0F, 1.0F, 1.0F);
    static final Vec3f csp0 = new Vec3f();
    private static final Vec3f tempVect = new Vec3f();
    private static double speedModificator = 4.340000152587891D;
    final Vector3dOperations lastCamPos = new Vector3dOperations();
    final Vector3dOperations dislocation = new Vector3dOperations();
    private final aQKa billboardSize;
    uv random;
    private float fadeIn;
    private float fadeOut;
    private float numBillboards;
    private float regionSize;
    private float halfRegionSize;
    private SceneObject referenceObject;
    private Mesh expandableMesh;
    private ExpandableVertexData vertexes;
    private ExpandableIndexData indexes;
    private int lastVertex;
    private double speedCap = 20.0D;

    public RSpaceDust() {
        this.fadeIn = 0.2F;
        this.fadeOut = 0.2F;
        this.numBillboards = 50.0F;
        this.regionSize = 200.0F;
        this.halfRegionSize = this.regionSize / 2.0F;
        this.expandableMesh = null;
        this.billboardSize = new aQKa(10.0F, 10.0F);
        this.random = new uv(10);
        VertexLayout var1 = new VertexLayout(true, true, false, false, 1);
        this.vertexes = new ExpandableVertexData(var1, 100);
        this.indexes = new ExpandableIndexData(400);
        this.expandableMesh = new Mesh();
        this.expandableMesh.setName("Expandable mesh for all RSpacedust");
        this.expandableMesh.setGeometryType(GeometryType.QUADS);
        this.expandableMesh.setVertexData(this.vertexes);
        this.expandableMesh.setIndexes(this.indexes);
    }

    RSpaceDust(RSpaceDust var1) {
        this.fadeIn = var1.fadeIn;
        this.fadeOut = var1.fadeOut;
        this.numBillboards = var1.numBillboards;
        this.regionSize = var1.regionSize;
        this.halfRegionSize = this.regionSize / 2.0F;
        this.expandableMesh = null;
        this.billboardSize = new aQKa(var1.billboardSize.x, var1.billboardSize.y);
        this.random = new uv(10);
        this.material = var1.getMaterial();
        this.primitiveColor.set(var1.primitiveColor);
        VertexLayout var2 = new VertexLayout(true, true, false, false, 1);
        this.vertexes = new ExpandableVertexData(var2, 100);
        this.indexes = new ExpandableIndexData(400);
        this.expandableMesh = new Mesh();
        this.expandableMesh.setName("Expandable mesh for all RSpacedust");
        this.expandableMesh.setGeometryType(GeometryType.QUADS);
        this.expandableMesh.setVertexData(this.vertexes);
        this.expandableMesh.setIndexes(this.indexes);
    }

    static double dpmodd(double var0, double var2) {
        return (var0 % var2 + var2) % var2;
    }

    public void dispose() {
    }

    public void releaseReferences() {
        super.releaseReferences();
        this.indexes.clear();
        this.vertexes.data().clear();
    }

    public boolean render(RenderContext var1, boolean var2) {
        if (this.render) {
            this.indexes.clear();
            this.indexes.setSize(0);
            this.vertexes.data().clear();
            this.lastVertex = 0;

            for (int var3 = 0; var3 < this.material.textureCount(); ++var3) {
                BaseTexture var4 = this.material.getTexture(var3);
                if (var4 != null) {
                    var4.addArea(1024.0F);
                }
            }

            if (!super.render(var1, var2)) {
                return false;
            } else {
                if (this.referenceObject == null) {
                    this.processSpaceDust(var1);
                    if (var1.getSceneViewQuality().getEnvFxQuality() > 0) {
                        return true;
                    }

                    var1.addPrimitive(this.material, this.expandableMesh, this.transform, this.primitiveColor, this.occlusionQuery, this.renderPriority, this, this.aabbWorldSpace, this.currentSquaredDistanceToCamera);
                } else {
                    this.processSpaceDustForReference(var1);
                    var1.addPrimitive(this.material, this.expandableMesh, this.transform, this.primitiveColor, this.occlusionQuery, this.renderPriority, this, this.aabbWorldSpace, this.currentSquaredDistanceToCamera);
                }

                return true;
            }
        } else {
            return false;
        }
    }

    public int step(StepContext var1) {
        super.step(var1);
        return 0;
    }

    public void setReferenceObject(SceneObject var1) {
        this.referenceObject = var1;
    }

    void processSpaceDust(RenderContext var1) {
        Camera var2 = var1.getCamera();
        bc_q var3 = var2.getTransform();
        Vector3dOperations var4 = var1.vec3dTemp0;
        var4.aA(var2.getPosition());
        var4.sub(this.lastCamPos);
        var4.scale(speedModificator);
        double var5 = var4.length();
        if (var5 > this.speedCap) {
            var4.scale(this.speedCap / var5);
        }

        this.dislocation.add(var4);
        this.lastCamPos.aA(var2.getPosition());
        this.transform.setIdentity();
        this.transform.setTranslation(var2.getPosition());
        var3.a(ticamera);
        float var7 = this.regionSize;
        aQKa var8 = this.billboardSize;
        float var9 = this.fadeOut * this.halfRegionSize * 0.5F + var2.getNearPlane();
        float var10 = (1.0F - this.fadeIn) * this.halfRegionSize * 0.5F + var2.getNearPlane();
        float var11 = this.getPrimitiveColor().w;
        float var12 = 1.0F / (this.halfRegionSize - (var10 - var2.getNearPlane()));
        float var13 = 1.0F / var9;
        this.random.eP(this.hashCode());
        Vec3f var14 = var3.getVectorX();
        Vec3f var15 = var3.getVectorY();
        d1.x = -(var14.x * var8.x) - var15.x * var8.y;
        d1.y = -(var14.y * var8.x) - var15.y * var8.y;
        d1.z = -(var14.z * var8.x) - var15.z * var8.y;
        d2.x = var14.x * var8.x - var15.x * var8.y;
        d2.y = var14.y * var8.x - var15.y * var8.y;
        d2.z = var14.z * var8.x - var15.z * var8.y;
        d3.x = var14.x * var8.x + var15.x * var8.y;
        d3.y = var14.y * var8.x + var15.y * var8.y;
        d3.z = var14.z * var8.x + var15.z * var8.y;
        d4.x = -(var14.x * var8.x) + var15.x * var8.y;
        d4.y = -(var14.y * var8.x) + var15.y * var8.y;
        d4.z = -(var14.z * var8.x) + var15.z * var8.y;

        for (int var16 = 0; (float) var16 < this.numBillboards; ++var16) {
            csp0.x = (float) (dpmodd((double) this.random.j(0.0F, var7) - this.dislocation.x + (double) this.halfRegionSize, (double) var7) - (double) this.halfRegionSize);
            csp0.y = (float) (dpmodd((double) this.random.j(0.0F, var7) - this.dislocation.y + (double) this.halfRegionSize, (double) var7) - (double) this.halfRegionSize);
            csp0.z = (float) (dpmodd((double) this.random.j(0.0F, var7) - this.dislocation.z + (double) this.halfRegionSize, (double) var7) - (double) this.halfRegionSize);
            float var17 = -(csp0.x * ticamera.mX.m20 + csp0.y * ticamera.mX.m21 + csp0.z * ticamera.mX.m22);
            if (var17 >= 1.0F) {
                float var18;
                if (var17 < var9) {
                    var18 = (var17 - var2.getNearPlane()) * var13;
                } else if (var17 > var10) {
                    var18 = 1.0F - (var17 - var10) * var12;
                    if (var18 > 1.0F) {
                        var18 = 1.0F;
                    }
                } else {
                    var18 = 1.0F;
                }

                if (var18 <= 1.0F && var18 > 0.0F) {
                    white.x = this.primitiveColor.x * var18;
                    white.y = this.primitiveColor.y * var18;
                    white.z = this.primitiveColor.z * var18;
                    white.w = var18 * var11;
                    tempVect.set(csp0);
                    tempVect.p((Tuple3f) d3);
                    this.addPoint(tempVect, tex3, white);
                    tempVect.set(csp0);
                    tempVect.p((Tuple3f) d4);
                    this.addPoint(tempVect, tex4, white);
                    tempVect.set(csp0);
                    tempVect.p((Tuple3f) d1);
                    this.addPoint(tempVect, tex1, white);
                    tempVect.set(csp0);
                    tempVect.p((Tuple3f) d2);
                    this.addPoint(tempVect, tex2, white);
                }
            }
        }
    }

    private void addPoint(Vec3f var1, aQKa var2, aJF var3) {
        this.vertexes.setPosition(this.lastVertex, var1.x, var1.y, var1.z);
        this.vertexes.setColor(this.lastVertex, var3.x, var3.y, var3.z, var3.w);
        this.vertexes.setTexCoord(this.lastVertex, 0, var2.x, var2.y);
        this.indexes.setIndex(this.lastVertex, this.lastVertex);
        this.indexes.setSize(this.lastVertex);
        ++this.lastVertex;
    }

    void processSpaceDustForReference(RenderContext var1) {
        Camera var2 = var1.getCamera();
        bc_q var3 = var2.getTransform();
        Vector3dOperations var4 = var1.vec3dTemp0;
        var4.aA(var2.getPosition());
        var4.sub(this.lastCamPos);
        var4.scale(speedModificator);
        double var5 = var4.length();
        if (var5 > this.speedCap) {
            var4.scale(this.speedCap / var5);
        }

        this.dislocation.add(var4);
        this.lastCamPos.aA(var2.getPosition());
        this.transform.setIdentity();
        this.transform.setTranslation(var2.getPosition());
        var3.a(ticamera);
        float var7 = this.regionSize;
        float var8 = var7 / 2.0F;
        this.random.eP(this.hashCode());
        white.set(this.primitiveColor);
        this.referenceObject.getGlobalTransform().multiply3x3(this.referenceObject.getVelocity(), Z);
        float var9 = Z.length();
        if (var9 == 0.0F) {
            this.referenceObject.getGlobalTransform().getZ(Z);
            Z.normalize();
        } else {
            Z.normalize();
        }

        float var10 = this.referenceObject.getVelocity().length() / 100.0F;
        if (var10 > 0.001F) {
            var1.getCamera().getTransform().a(ticamera);
            int var12 = (int) (this.numBillboards + this.numBillboards * var10 * 0.1F);
            if (var12 > 300) {
                var12 = 300;
            }

            for (int var13 = 0; var13 < var12; ++var13) {
                csp0.x = (float) (dpmodd((double) this.random.j(0.0F, var7) - this.dislocation.x + (double) var8, (double) var7) - (double) var8);
                csp0.y = (float) (dpmodd((double) this.random.j(0.0F, var7) - this.dislocation.y + (double) var8, (double) var7) - (double) var8);
                csp0.z = (float) (dpmodd((double) this.random.j(0.0F, var7) - this.dislocation.z + (double) var8, (double) var7) - (double) var8);
                float var14 = -(csp0.x * ticamera.mX.m20 + csp0.y * ticamera.mX.m21 + csp0.z * ticamera.mX.m22);
                if (var14 >= 50.0F) {
                    camPointDist.set(var2.getPosition());
                    camPointDist.sub(csp0);
                    Y.set(camPointDist);
                    Y.c(Z);
                    Y.normalize();
                    float var15 = (Math.abs(var14) - 50.0F) / (var7 - 50.0F);
                    float var11 = (float) ((1.0D - Math.cos((double) (var15 * 2.0F) * 3.141592653589793D)) * 0.5D);
                    white.w = var11 * this.primitiveColor.w;
                    white.x = var11 * this.primitiveColor.x;
                    white.y = var11 * this.primitiveColor.y;
                    white.z = var11 * this.primitiveColor.z;
                    v1.x = Y.x * this.billboardSize.y;
                    v1.y = Y.y * this.billboardSize.y;
                    v1.z = Y.z * this.billboardSize.y;
                    v2.x = Z.x * this.billboardSize.x;
                    v2.y = Z.y * this.billboardSize.x;
                    v2.z = Z.z * this.billboardSize.x;
                    d1.x = v2.x * -var10 - v1.x;
                    d1.y = v2.y * -var10 - v1.y;
                    d1.z = v2.z * -var10 - v1.z;
                    d2.x = var10 * v2.x - v1.x;
                    d2.y = var10 * v2.y - v1.y;
                    d2.z = var10 * v2.z - v1.z;
                    if (d1.isValid() && d2.isValid()) {
                        tempVect.set(csp0);
                        tempVect.p((Tuple3f) d1);
                        this.addPoint(tempVect, tex1, white);
                        tempVect.set(csp0);
                        tempVect.p((Tuple3f) d2);
                        this.addPoint(tempVect, tex2, white);
                        tempVect.set(csp0);
                        tempVect.o(d1);
                        this.addPoint(tempVect, tex3, white);
                        tempVect.set(csp0);
                        tempVect.o(d2);
                        this.addPoint(tempVect, tex4, white);
                    }
                }
            }

        }
    }

    public RenderAsset cloneAsset() {
        return new RSpaceDust(this);
    }

    public aQKa getBillboardSize() {
        return this.billboardSize;
    }

    public void setBillboardSize(aQKa var1) {
        this.billboardSize.set(var1);
    }

    public float getFadeOut() {
        return this.fadeOut;
    }

    public void setFadeOut(float var1) {
        this.fadeOut = var1;
    }

    public float getFadeIn() {
        return this.fadeIn;
    }

    public void setFadeIn(float var1) {
        this.fadeIn = var1;
    }

    public float getRegionSize() {
        return this.regionSize;
    }

    public void setRegionSize(float var1) {
        this.regionSize = var1;
    }

    public void validate(SceneLoader var1) {
        super.validate(var1);
        this.material.setShader(var1.getDefaultSpaceDustShader());
    }
}
