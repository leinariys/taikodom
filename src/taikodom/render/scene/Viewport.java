package taikodom.render.scene;

/**
 *
 */
public class Viewport {
    public int x = 0;
    public int y = 0;
    public int width = 720;
    public int height = 640;

    public Viewport() {
    }

    public Viewport(Viewport var1) {
        this.set(var1);
    }

    public Viewport(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public void set(Viewport var1) {
        this.x = var1.x;
        this.y = var1.y;
        this.width = var1.width;
        this.height = var1.height;
    }
}
