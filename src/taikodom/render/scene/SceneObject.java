package taikodom.render.scene;

import all.*;
import com.hoplon.geometry.Vec3f;
import gnu.trove.THashMap;
import taikodom.render.NotExported;
import taikodom.render.RenderContext;
import taikodom.render.StepContext;
import taikodom.render.helpers.RenderVisitor;
import taikodom.render.impostor.Impostor;
import taikodom.render.loader.RenderAsset;
import taikodom.render.partitioning.RenderZone;
import taikodom.render.raytrace.RayTraceSceneObjectInfoD;

import java.util.*;


/**
 * Объект сцены
 * Реализация класса указанного в имени тегов из файлов с расширением .pro
 */
public class SceneObject extends RenderAsset {
    protected static final int MIN_AABB_WS_LENGHT = 5;
    protected final List objects = new ArrayList();
    protected final bc_q transform = new bc_q();
    protected final bc_q globalTransform = new bc_q();
    protected final bc_q parentTransform = new bc_q();
    protected final Vec3f velocity = new Vec3f(0.0F, 0.0F, 0.0F);
    protected final Vec3f angularVelocity = new Vec3f(0.0F, 0.0F, 0.0F);
    protected final Vec3f scaling = new Vec3f(1.0F, 1.0F, 1.0F);
    protected final Vec3f desiredSize = new Vec3f(0.0F, 0.0F, 0.0F);
    protected final aLH aabbWorldSpace = new aLH();
    protected final aLH localAabb = new aLH();
    protected Map clientProperties;
    protected double currentSquaredDistanceToCamera = 0.0D;
    protected boolean disposed = false;
    protected boolean firstFrame = true;
    protected boolean render = true;
    protected boolean allowSelection = true;
    protected int renderPriority = 0;
    protected boolean rayTraceable;
    protected boolean canRemoveChildren = true;
    protected double maxVisibleDistance;
    protected double maxSquaredVisibleDistance;
    protected boolean disposeWhenEmpty = false;
    protected double aabbWSLenght;
    protected double aabbLSLenght;
    RenderZone renderZone;
    private List postPonedRemovalList = new ArrayList();
    private boolean impostorEnabled = false;
    private double impostorDistance;
    private float impostorBlendingDistance;
    private Impostor impostorData;

    public SceneObject() {
        this.localAabb.B(-0.5D, -0.5D, -0.5D);
        this.localAabb.A(0.5D, 0.5D, 0.5D);
        this.setMaxVisibleDistance(-1.0D);
    }

    protected SceneObject(SceneObject var1) {
        super(var1);
        this.localAabb.g(var1.localAabb);
        this.aabbLSLenght = var1.aabbLSLenght;
        this.aabbWorldSpace.g(var1.aabbWorldSpace);
        this.aabbWSLenght = var1.aabbWSLenght;
        this.disposeWhenEmpty = var1.disposeWhenEmpty;
        this.impostorDistance = var1.impostorDistance;
        this.setImpostorEnabled(var1.impostorEnabled);
        this.impostorBlendingDistance = var1.impostorBlendingDistance;
        Iterator var3 = var1.objects.iterator();

        while (var3.hasNext()) {//перечисление из раздела objects
            SceneObject var2 = (SceneObject) var3.next();
            this.objects.add((SceneObject) this.smartClone(var2));
        }

        this.name = var1.name;//fig_spa_bullfrog
        this.transform.b(var1.transform);
        this.velocity.set(var1.velocity);
        this.disposed = var1.disposed;
        this.desiredSize.set(var1.desiredSize);
        this.render = var1.render;
        this.allowSelection = var1.allowSelection;
        this.renderPriority = var1.renderPriority;
        this.rayTraceable = var1.rayTraceable;
        this.setMaxVisibleDistance(var1.maxVisibleDistance);
        this.scaling.set(var1.scaling);
    }

    protected Object smartClone(Object var1) {
        return var1 instanceof RenderAsset ? ((RenderAsset) var1).cloneAsset() : var1;
    }

    public RenderAsset cloneAsset() {
        return new SceneObject(this);
    }

    public bc_q getTransform() {
        return this.transform;
    }

    public void setTransform(bc_q var1) {
        this.transform.b(var1);
        this.updateInternalGeometry((SceneObject) null);
    }

    @NotExported
    public void setTransform(ajK var1) {
        this.transform.set(var1);
        this.updateInternalGeometry((SceneObject) null);
    }

    public bc_q getGlobalTransform() {
        return this.globalTransform;
    }

    public Vec3f getVelocity() {
        return this.velocity;
    }

    public void setVelocity(Vec3f var1) {
        this.velocity.set(var1);
    }

    public void setVelocity(float var1, float var2, float var3) {
        this.velocity.set(var1, var2, var3);
    }

    public boolean isDisposed() {
        return this.disposed;
    }

    public Vec3f getDesiredSize() {
        return this.desiredSize;
    }

    public void setDesiredSize(Vec3f var1) {
        this.desiredSize.set(var1);
    }

    public boolean isRender() {
        return this.render;
    }

    public void setRender(boolean var1) {
        this.render = var1;
    }

    public boolean isVisible() {
        return this.render;
    }

    public void setVisible(boolean var1) {
        this.render = var1;
    }

    public boolean isAllowSelection() {
        return this.allowSelection;
    }

    public void setAllowSelection(boolean var1) {
        this.allowSelection = var1;
    }

    public int getRenderPriority() {
        return this.renderPriority;
    }

    public void setRenderPriority(int var1) {
        this.renderPriority = var1;
    }

    public boolean isRayTraceable() {
        return this.rayTraceable;
    }

    public void setRayTraceable(boolean var1) {
        this.rayTraceable = var1;
    }

    @NotExported
    public void setPosition(double var1, double var3, double var5) {
        if (!Double.isInfinite(var1) && !Double.isNaN(var1) && !Double.isInfinite(var3) && !Double.isNaN(var3) && !Double.isInfinite(var5) && !Double.isNaN(var5)) {
            this.transform.setTranslation(var1, var3, var5);
            this.updateInternalGeometry((SceneObject) null);
        } else {
            throw new RuntimeException("Not all valid position for " + this.getName());
        }
    }

    public void setPosition(float var1, float var2, float var3) {
        if (!Float.isInfinite(var1) && !Float.isNaN(var1) && !Float.isInfinite(var2) && !Float.isNaN(var2) && !Float.isInfinite(var3) && !Float.isNaN(var3)) {
            this.transform.setTranslation((double) var1, (double) var2, (double) var3);
            this.updateInternalGeometry((SceneObject) null);
        } else {
            throw new RuntimeException("Not all valid position for " + this.getName());
        }
    }

    public void setTransform(Vec3f var1, OrientationBase var2) {
        this.transform.position.setVec3f(var1);
        this.transform.mX.set(var2);
        this.updateInternalGeometry((SceneObject) null);
    }

    public void setTransform(Vector3dOperations var1, OrientationBase var2) {
        if (!Double.isInfinite(var1.x) && !Double.isNaN(var1.x) && !Double.isInfinite(var1.y) && !Double.isNaN(var1.y) && !Double.isInfinite(var1.z) && !Double.isNaN(var1.z)) {
            this.transform.position.aA(var1);
            this.transform.mX.set(var2);
            this.updateInternalGeometry((SceneObject) null);
        } else {
            throw new RuntimeException("Not all valid position for " + this.getName());
        }
    }

    void setTransform(Vec3f var1, Vec3f var2, Vec3f var3, Vec3f var4) {
        this.transform.setX(var1);
        this.transform.setY(var2);
        this.transform.setZ(var3);
        this.transform.position.setVec3f(var4);
        this.updateInternalGeometry((SceneObject) null);
    }

    public void setOrientation(OrientationBase var1) {
        this.transform.setOrientation(var1);
        this.updateInternalGeometry((SceneObject) null);
    }

    public void reset() {
    }

    public void show() {
        this.render = true;
    }

    public void hide() {
        this.render = false;
    }

    public int step(StepContext var1) {
        return 0;
    }

    public boolean needStep() {
        return true;
    }

    public void fillRenderQueue(RenderContext var1, boolean var2) {
        this.currentSquaredDistanceToCamera = this.globalTransform.g(var1.getCamera().globalTransform);
        if (this.isImpostorEnabled() && var1.isAllowImpostors()) {
            if (this.currentSquaredDistanceToCamera > this.impostorData.getImpostorDistance() * this.impostorData.getImpostorDistance()) {
                if (!var2 || var1.getFrustum().a(this.aabbWorldSpace)) {
                    float var3;
                    if (this.impostorData.isValid()) {
                        this.impostorData.updateImpostor(var1, this);
                        var3 = (float) (Math.sqrt(this.currentSquaredDistanceToCamera) - this.impostorData.getImpostorDistance()) / this.impostorBlendingDistance;
                        if (var3 > 1.0F) {
                            var3 = 1.0F;
                        }

                        this.impostorData.setCurrentColor(var3);
                        if (var3 < 1.0F) {
                            this.render(var1, false);
                            this.impostorData.setTransition(true);
                        }
                    } else {
                        var1.addImpostor(this);
                        var3 = (float) (Math.sqrt(this.currentSquaredDistanceToCamera) - this.impostorData.getImpostorDistance()) / this.impostorBlendingDistance;
                        if (var3 > 1.0F) {
                            var3 = 1.0F;
                        }

                        this.impostorData.setCurrentColor(var3);
                        if (var3 < 1.0F) {
                            this.render(var1, false);
                        }
                    }
                }

                return;
            }

            if (this.impostorData.isValid()) {
                this.impostorData.clearData();
            }
        }

        this.render(var1, var2);
    }

    public void processRender(RenderContext var1, boolean var2) {
        int var3 = 0;
        this.setCanRemoveChildren(false);
        List var4 = this.objects;
        synchronized (this.objects) {
            for (Iterator var6 = this.objects.iterator(); var6.hasNext(); ++var3) {
                SceneObject var5 = (SceneObject) var6.next();
                if (var5 == null) {
                    this.setCanRemoveChildren(true);
                    throw new RuntimeException("Null child " + var3 + " in " + this.name);
                }

                if (var5.isDisposed()) {
                    this.removeChild(var5);
                } else {
                    var5.fillRenderQueue(var1, var2);
                }
            }

            this.setCanRemoveChildren(true);
        }
    }

    public boolean render(RenderContext var1, boolean var2) {
        if (this.render) {
            if (this.maxVisibleDistance > 0.0D && this.currentSquaredDistanceToCamera > this.maxSquaredVisibleDistance) {
                return false;
            } else {
                if (this.objects.size() > 0) {
                    this.processRender(var1, var2);
                }

                return true;
            }
        } else {
            return false;
        }
    }

    public Vec3f getScaling() {
        return this.scaling;
    }

    public void setScaling(Vec3f var1) {
        this.scaling.set(var1);
        this.updateInternalGeometry((SceneObject) null);
    }

    public void setScaling(float var1) {
        this.setScaling(new Vec3f(var1, var1, var1));
    }

    public Vector3dOperations getPosition() {
        return this.transform.position;
    }

    public void setPosition(Vector3dOperations var1) {
        if (!Double.isInfinite(var1.x) && !Double.isNaN(var1.x) && !Double.isInfinite(var1.y) && !Double.isNaN(var1.y) && !Double.isInfinite(var1.z) && !Double.isNaN(var1.z)) {
            this.transform.setTranslation(var1);
            this.updateInternalGeometry((SceneObject) null);
        } else {
            throw new RuntimeException("Not all valid position for " + this.getName());
        }
    }

    @NotExported
    public void setPosition(Vec3f var1) {
        if (!Float.isInfinite(var1.x) && !Float.isNaN(var1.x) && !Float.isInfinite(var1.y) && !Float.isNaN(var1.y) && !Float.isInfinite(var1.z) && !Float.isNaN(var1.z)) {
            this.transform.setTranslation(var1);
            this.updateInternalGeometry((SceneObject) null);
        } else {
            throw new RuntimeException("Not all valid position for " + this.getName());
        }
    }

    public Vec3f getAngularVelocity() {
        return this.angularVelocity;
    }

    public void setAngularVelocity(Vec3f var1) {
        this.angularVelocity.set(var1);
    }

    public void updateInternalGeometry(SceneObject var1) {
        if (var1 != null) {
            this.parentTransform.b(var1.getGlobalTransform());
            this.globalTransform.a(this.parentTransform, this.transform);
        } else {
            this.globalTransform.a(this.parentTransform, this.transform);
        }

        if (this.scaling.x != 1.0F) {
            this.scaling.x = this.scaling.x;
        }

        this.globalTransform.B(this.scaling.x);
        this.globalTransform.C(this.scaling.y);
        this.globalTransform.D(this.scaling.z);
        int var2 = this.childCount();
        if (var2 > 0) {
            this.setCanRemoveChildren(false);

            for (int var3 = 0; var3 < var2; ++var3) {
                SceneObject var4 = this.getChild(var3);
                if (var4 != null && !var4.isDisposed()) {
                    var4.updateInternalGeometry(this);
                } else {
                    this.removeChild(var4);
                }
            }

            this.setCanRemoveChildren(true);
        }

        this.computeWorldSpaceAABB();
        if (this.renderZone != null) {
            this.renderZone.onObjectTransform(this);
        }

    }

    public aLH computeWorldSpaceAABB() {
        this.aabbWorldSpace.reset();
        this.addChildrenWorldSpaceAABB();
        this.aabbWSLenght = this.aabbWorldSpace.din().ax(this.aabbWorldSpace.dim());
        return this.aabbWorldSpace;
    }

    public aLH computeLocalSpaceAABB() {
        this.localAabb.reset();
        int var1 = this.childCount();
        boolean var2 = true;
        if (var1 > 0) {
            aLH var3 = new aLH();
            Iterator var5 = this.objects.iterator();

            label46:
            while (true) {
                SceneObject var4;
                do {
                    do {
                        if (!var5.hasNext()) {
                            break label46;
                        }

                        var4 = (SceneObject) var5.next();
                    } while (!var4.isRender());
                } while (!(var4 instanceof RGroup) && !(var4 instanceof RLod) && !(var4 instanceof RMesh) && !(var4 instanceof RModel) && !(var4 instanceof RShape) && !(var4 instanceof RParticleSystem) && !(var4 instanceof RBillboard));

                aLH var6 = var4.computeLocalSpaceAABB();
                if (var6.dim().x <= var6.din().x) {
                    var6.a(var4.getTransform(), var3);
                    this.localAabb.aG(var3.dim());
                    this.localAabb.aG(var3.din());
                    var2 = false;
                }
            }
        }

        if (var2) {
            this.localAabb.C(0.0D, 0.0D, 0.0D);
        }

        this.aabbLSLenght = this.localAabb.din().ax(this.localAabb.dim());
        return this.localAabb;
    }

    public aLH getAABB() {
        return this.localAabb;
    }

    protected void addChildrenWorldSpaceAABB() {
        int var1 = this.childCount();

        for (int var2 = 0; var2 < var1; ++var2) {
            SceneObject var3 = this.getChild(var2);
            if (var3 instanceof RGroup || var3 instanceof RLod || var3 instanceof RMesh || var3 instanceof RModel || var3 instanceof RShape || var3 instanceof RBillboard) {
                this.aabbWorldSpace.h(var3.getAabbWorldSpace());
            }
        }

    }

    public void removeChild(SceneObject var1) {
        if (!this.canRemoveChildren) {
            this.postPonedRemovalList.add(var1);
        } else {
            if (this.objects.remove(var1) && var1 != null) {
                var1.onRemove();
            }

            this.checkEmpty();
        }
    }

    public void removeAllChildren() {
        if (!this.canRemoveChildren) {
            this.postPonedRemovalList.addAll(this.objects);
        } else {
            Iterator var2 = this.objects.iterator();

            while (var2.hasNext()) {
                SceneObject var1 = (SceneObject) var2.next();
                var1.onRemove();
            }

            this.objects.clear();
            this.checkEmpty();
        }
    }

    public void addChild(SceneObject var1) {
        if (var1 == null) {
            System.out.println("Trying to_q add all NULL child in object " + this.name);
        }

        this.objects.add(var1);
        this.updateInternalGeometry((SceneObject) null);
        this.computeLocalSpaceAABB();
        if (this.renderZone != null && var1 != null) {
            var1.setRenderZone(this.renderZone);
            this.renderZone.findStepableObjects(var1);
        }

    }

    public SceneObject getChild(int var1) {
        return (SceneObject) this.objects.get(var1);
    }

    public void setChild(int var1, SceneObject var2) {
        this.addChild(var2);
        this.updateInternalGeometry((SceneObject) null);
        if (this.renderZone != null && var2 != null) {
            var2.setRenderZone(this.renderZone);
            this.renderZone.findStepableObjects(var2);
        }

    }

    public List getChildren() {
        return this.objects;
    }

    public int childCount() {
        return this.objects.size();
    }

    public aLH getAabbWorldSpace() {
        return this.aabbWorldSpace;
    }

    public boolean isNeedToStep() {
        return false;
    }

    public RenderZone getRenderZone() {
        return this.renderZone;
    }

    public void setRenderZone(RenderZone var1) {
        this.renderZone = var1;
    }

    public void dispose() {
        this.disposed = true;
        if (this.impostorData != null) {
            this.impostorData.clearData();
            this.impostorData = null;
        }

    }

    public void releaseReferences() {
        super.releaseReferences();
        if (this.impostorData != null) {
            this.impostorData.clearData();
            this.impostorData = null;
        }

    }

    public void onRemove() {
        if (this.renderZone != null) {
            this.renderZone.removeChild(this);
            this.renderZone = null;
        }

        if (this.impostorData != null) {
            this.impostorData.clearData();
            this.impostorData = null;
        }

        Iterator var2 = this.objects.iterator();

        while (var2.hasNext()) {
            SceneObject var1 = (SceneObject) var2.next();
            var1.onRemove();
        }

    }

    public RayTraceSceneObjectInfoD rayIntersects(RayTraceSceneObjectInfoD var1, boolean var2, boolean var3) {
        var1.setChanged(false);
        if (var3) {
            List var4 = this.objects;
            synchronized (this.objects) {
                Iterator var6 = this.objects.iterator();

                while (var6.hasNext()) {
                    SceneObject var5 = (SceneObject) var6.next();
                    var5.rayIntersects(var1, var2, var3);
                    if (var1.isIntersected() && var1.isChanged()) {
                        return var1;
                    }
                }
            }
        }

        return var1;
    }

    public boolean isCanRemoveChildren() {
        return this.canRemoveChildren;
    }

    public void setCanRemoveChildren(boolean var1) {
        if (!var1) {
            this.canRemoveChildren = false;
        } else {
            this.canRemoveChildren = true;
            if (this.postPonedRemovalList.size() > 0) {
                Iterator var3 = this.postPonedRemovalList.iterator();

                while (var3.hasNext()) {
                    SceneObject var2 = (SceneObject) var3.next();
                    this.removeChild(var2);
                }

                this.postPonedRemovalList.clear();
            }
        }

    }

    public SceneObject findChildByName(String var1) {
        Iterator var3 = this.objects.iterator();

        while (var3.hasNext()) {
            SceneObject var2 = (SceneObject) var3.next();
            if (var2.name.equals(var1)) {
                return var2;
            }
        }

        return null;
    }

    public boolean hasChild(SceneObject var1) {
        Iterator var3 = this.objects.iterator();

        while (var3.hasNext()) {
            SceneObject var2 = (SceneObject) var3.next();
            if (var2 == var1) {
                return true;
            }
        }

        return false;
    }

    public void resetRotation() {
        this.transform.mX.setIdentity();
        this.updateInternalGeometry((SceneObject) null);
    }

    public void resetPosition() {
        this.transform.position.set(0.0D, 0.0D, 0.0D);
        this.updateInternalGeometry((SceneObject) null);
    }

    public boolean isImpostorEnabled() {
        return this.impostorEnabled;
    }

    public void setImpostorEnabled(boolean var1) {
        this.impostorEnabled = var1;
        if (var1 && this.impostorData == null) {
            this.impostorData = new Impostor();
            this.impostorData.setImpostorDistance(this.impostorDistance);
        }

    }

    public double getImpostorDistance() {
        return this.impostorDistance;
    }

    public void setImpostorDistance(double var1) {
        this.impostorDistance = var1;
        if (this.impostorData != null) {
            this.impostorData.setImpostorDistance(var1);
        }

    }

    public float getImpostorBlendingDistance() {
        return this.impostorBlendingDistance;
    }

    public void setImpostorBlendingDistance(float var1) {
        this.impostorBlendingDistance = var1;
    }

    public void getTransformWithNoScale(bc_q var1) {
        var1.b(this.transform);
    }

    public double getMaxVisibleDistance() {
        return this.maxVisibleDistance;
    }

    public void setMaxVisibleDistance(double var1) {
        this.maxVisibleDistance = var1;
        this.maxSquaredVisibleDistance = var1 * var1;
        if (var1 < 0.0D) {
            this.maxSquaredVisibleDistance = var1;
        }

    }

    public boolean isDisposeWhenEmpty() {
        return this.disposeWhenEmpty;
    }

    public void setDisposeWhenEmpty(boolean var1) {
        this.disposeWhenEmpty = var1;
    }

    public void checkEmpty() {
        if (this.objects.size() == 0 && this.disposeWhenEmpty) {
            this.dispose();
        }

    }

    public boolean visitPreOrder(RenderVisitor var1) {
        if (!var1.visit(this)) {
            return false;
        } else {
            Iterator var3 = this.objects.iterator();

            while (var3.hasNext()) {
                SceneObject var2 = (SceneObject) var3.next();
                var2.setRenderZone(this.renderZone);
                if (!var2.visitPreOrder(var1)) {
                    return false;
                }
            }

            return true;
        }
    }

    public Impostor getImpostor() {
        return this.impostorData;
    }

    public double getAabbWSLenght() {
        return this.aabbWSLenght;
    }

    public double getAabbLSLenght() {
        return this.aabbLSLenght;
    }

    public double getCurrentSquaredDistanceToCamera() {
        return this.currentSquaredDistanceToCamera;
    }

    public Object putClientProperty(Object var1, Object var2) {
        if (this.clientProperties == null) {
            if (var2 == null) {
                return null;
            }

            this.clientProperties = Collections.synchronizedMap(new THashMap());
        }

        return var2 == null ? this.clientProperties.remove(var1) : this.clientProperties.put(var1, var2);
    }

    public Object getClientProperty(Object var1) {
        return this.clientProperties == null ? null : this.clientProperties.get(var1);
    }
}
