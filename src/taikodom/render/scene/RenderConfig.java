package taikodom.render.scene;

import all.aip;

public class RenderConfig {
    private final SceneViewQuality sceneViewQuality = new SceneViewQuality();
    private int screenWidth = 1024;
    private int screenHeight = 768;
    private int refreshRate = 60;
    /**
     * Глубина цвета 32-bit
     */
    private int bpp = 32;
    private boolean fullscreen = false;
    private boolean useVbo = true;
    private int textureQuality = 1024;
    private int textureFilter = 0;
    private int antiAliasing = 0;
    private float anisotropicFilter = 0.0F;
    private int maxMipMapResolution = 11;
    private int minMipMapLevel = 2;
    private float guiContrastLevel;

    public int getScreenWidth() {
        return this.screenWidth;
    }

    public void setScreenWidth(int var1) {
        this.screenWidth = var1;
    }

    public int getScreenHeight() {
        return this.screenHeight;
    }

    public void setScreenHeight(int var1) {
        this.screenHeight = var1;
    }

    /**
     * Глубина цвета 32-bit
     *
     * @return
     */
    public int getBpp() {
        return this.bpp;
    }

    /**
     * Глубина цвета 32-bit
     *
     * @param var1
     */
    public void setBpp(int var1) {
        this.bpp = var1;
    }

    public int getTextureQuality() {
        return this.textureQuality;
    }

    public void setTextureQuality(int var1) {
        this.textureQuality = aip.ra(var1);
        if (var1 < 256) {
            boolean var2 = true;
        }
    }

    public int getTextureFilter() {
        return this.textureFilter;
    }

    public void setTextureFilter(int var1) {
        this.textureFilter = var1;
    }

    public int getAntiAliasing() {
        return this.antiAliasing;
    }

    public void setAntiAliasing(int var1) {
        this.antiAliasing = var1;
    }

    public float getLodQuality() {
        return this.sceneViewQuality.getLodQuality();
    }

    public void setLodQuality(float var1) {
        this.sceneViewQuality.setLodQuality(var1);
    }

    public int getEnvFxQuality() {
        return this.sceneViewQuality.getEnvFxQuality();
    }

    public void setEnvFxQuality(int var1) {
        this.sceneViewQuality.setEnvFxQuality(var1);
    }

    public int getShaderQuality() {
        return this.sceneViewQuality.getShaderQuality();
    }

    public void setShaderQuality(int var1) {
        this.sceneViewQuality.setShaderQuality(var1);
    }

    public boolean isPostProcessingFX() {
        return this.sceneViewQuality.isPostProcessingFX();
    }

    public void setPostProcessingFX(boolean var1) {
        this.sceneViewQuality.setPostProcessingFX(var1);
    }

    public boolean isFullscreen() {
        return this.fullscreen;
    }

    public void setFullscreen(boolean var1) {
        this.fullscreen = var1;
    }

    public boolean isUseVbo() {
        return this.useVbo;
    }

    public void setUseVbo(boolean var1) {
        this.useVbo = var1;
    }

    public Float getAnisotropicFilter() {
        return this.anisotropicFilter;
    }

    public void setAnisotropicFilter(Float var1) {
        this.anisotropicFilter = var1.floatValue();
    }

    public void setAnisotropicFilter(float var1) {
        this.anisotropicFilter = var1;
    }

    public SceneViewQuality getSceneViewQuality() {
        return this.sceneViewQuality;
    }

    public int getMaxMipMapResolution() {
        return this.maxMipMapResolution;
    }

    public void setMaxMipMapResolution(int var1) {
        this.maxMipMapResolution = var1;
    }

    public int getMinMipMapLevel() {
        return this.minMipMapLevel;
    }

    public void setMinMipMapLevel(int var1) {
        this.minMipMapLevel = var1;
    }

    public float getGuiContrastLevel() {
        return this.guiContrastLevel;
    }

    public void setGuiContrastLevel(float var1) {
        this.guiContrastLevel = var1;
    }

    public int getRefreshRate() {
        return this.refreshRate;
    }

    public void setRefreshRate(int var1) {
        this.refreshRate = var1;
    }
}
