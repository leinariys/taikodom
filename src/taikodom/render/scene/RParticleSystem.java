package taikodom.render.scene;

import all.Vector3dOperations;
import all.aLH;
import all.aip;
import all.bc_q;
import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import taikodom.render.RenderContext;
import taikodom.render.StepContext;
import taikodom.render.camera.Camera;
import taikodom.render.data.ExpandableIndexData;
import taikodom.render.data.ExpandableVertexData;
import taikodom.render.data.VertexLayout;
import taikodom.render.enums.BillboardAlignment;
import taikodom.render.enums.GeometryType;
import taikodom.render.loader.RenderAsset;
import taikodom.render.loader.SceneLoader;
import taikodom.render.primitives.Mesh;
import taikodom.render.textures.BaseTexture;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Реализация класса указанного в имени тегов из файлов с расширением .pro
 */
public class RParticleSystem extends RenderObject {
    private static final int MAX_ALIVE_PARTICLES = 5000;
    private static int globalAliveParticles = 0;
    // $FF: synthetic field
    private static int[] $SWITCH_TABLE$taikodom$render$enums$BillboardAlignment;
    protected final Vec3f alignmentDirection = new Vec3f(0.0F, 1.0F, 0.0F);
    protected final Vector3dOperations lastPosition = new Vector3dOperations(0.0D, 0.0D, 0.0D);
    protected final Vector3dOperations transformedParticlesVelocity = new Vector3dOperations(0.0D, 0.0D, 0.0D);
    protected final Vector3dOperations particlesVelocity = new Vector3dOperations(0.0D, 0.0D, 0.0D);
    protected final Vector3dOperations particlesAcceleration = new Vector3dOperations(0.0D, 0.0D, 0.0D);
    protected final Vector3dOperations particlesVelocityVariation = new Vector3dOperations(0.0D, 0.0D, 0.0D);
    protected final Color particlesInitialColor = new Color(1.0F, 1.0F, 1.0F, 1.0F);
    protected final Color particlesFinalColor = new Color(1.0F, 1.0F, 1.0F, 0.0F);
    protected final Color particlesColorVariation = new Color(0.1F, 0.1F, 0.1F, 0.1F);
    private final bc_q cameraSpaceTransform = new bc_q();
    protected BillboardAlignment alignment;
    protected int currentAliveParticles;
    protected float newPartsSecond;
    protected int particlesLimit;
    protected int maxParticles;
    protected int initialBurst;
    protected float particlesMaxLifeTime;
    protected float particlesLifeTimeVariation;
    protected float particlesInitialSize;
    protected float particlesFinalSize;
    protected float particlesInitialSizeVariation;
    protected float particlesFinalSizeVariation;
    protected boolean active = true;
    protected float systemLifeTime;
    protected float currentLifeTime;
    protected boolean isDead;
    protected float initialDelay;
    protected float currentDelay;
    protected boolean followEmitter;
    protected boolean scaleable;
    protected float currentParticlesToSpawn;
    protected boolean alignToMovement;
    protected boolean alignToHorizon;
    protected float initialMaxAngle;
    protected float initialMinAngle;
    private Mesh mesh;
    private ExpandableVertexData vertexes;
    private ExpandableIndexData indexes;
    private ArrayList particlesPool = new ArrayList();
    private float minRotationSpeed;
    private float maxRotationSpeed;
    private double spawnLimitStart = Double.MAX_VALUE;
    private double spawnLimitEnd = Double.MAX_VALUE;
    private double maxSpawnDistance = 5500000.0D;

    public RParticleSystem() {
        this.alignment = BillboardAlignment.VIEW_ALIGNED;
        this.alignmentDirection.z = 1.0F;
        this.newPartsSecond = 60.0F;
        this.currentParticlesToSpawn = 0.0F;
        this.particlesMaxLifeTime = 1.0F;
        this.particlesInitialSize = 1.0F;
        this.particlesFinalSize = 1.0F;
        this.particlesInitialColor.set(1.0F, 1.0F, 1.0F, 1.0F);
        this.particlesLifeTimeVariation = 0.0F;
        this.particlesInitialSizeVariation = 0.0F;
        this.particlesFinalSizeVariation = 0.0F;
        this.minRotationSpeed = 0.0F;
        this.maxRotationSpeed = 0.0F;
        this.initialBurst = 0;
        this.currentLifeTime = 0.0F;
        this.systemLifeTime = -1.0F;
        this.isDead = false;
        this.initialMaxAngle = 0.0F;
        this.initialMinAngle = 0.0F;
        this.followEmitter = false;
        this.currentAliveParticles = 0;
        this.initialDelay = 0.0F;
        this.currentDelay = 0.0F;
        this.scaleable = true;
        this.alignToMovement = false;
        this.alignToHorizon = false;
        this.initRenderStructs();
        this.setMaxParticles(100);
    }

    public RParticleSystem(RParticleSystem var1) {
        super(var1);
        this.followEmitter = var1.followEmitter;
        this.alignment = var1.alignment;
        this.alignmentDirection.set(var1.alignmentDirection);
        this.newPartsSecond = var1.newPartsSecond;
        this.currentParticlesToSpawn = 0.0F;
        this.particlesMaxLifeTime = var1.particlesMaxLifeTime;
        this.particlesInitialSize = var1.particlesInitialSize;
        this.particlesFinalSize = var1.particlesFinalSize;
        this.particlesFinalColor.set(var1.particlesFinalColor);
        this.particlesColorVariation.set(var1.particlesColorVariation);
        this.particlesInitialColor.set(var1.particlesInitialColor);
        this.particlesLifeTimeVariation = var1.particlesLifeTimeVariation;
        this.particlesInitialSizeVariation = var1.particlesInitialSizeVariation;
        this.particlesFinalSizeVariation = var1.particlesFinalSizeVariation;
        this.particlesVelocity.aA(var1.particlesVelocity);
        this.particlesAcceleration.aA(var1.particlesAcceleration);
        this.particlesVelocityVariation.aA(var1.particlesVelocityVariation);
        this.setInitialBurst(var1.initialBurst);
        this.currentLifeTime = 0.0F;
        this.systemLifeTime = var1.systemLifeTime;
        this.isDead = false;
        this.alignToMovement = var1.alignToMovement;
        this.alignToHorizon = var1.alignToHorizon;
        this.initialMaxAngle = var1.initialMaxAngle;
        this.initialMinAngle = var1.initialMinAngle;
        this.initialDelay = var1.initialDelay;
        this.currentDelay = 0.0F;
        this.currentAliveParticles = 0;
        this.lastPosition.aA(this.getPosition());
        this.localAabb.g(-this.particlesFinalSize, -this.particlesFinalSize, -this.particlesFinalSize, this.particlesFinalSize, this.particlesFinalSize, this.particlesFinalSize);
        this.scaleable = var1.scaleable;
        this.minRotationSpeed = var1.minRotationSpeed;
        this.maxRotationSpeed = var1.maxRotationSpeed;
        this.initRenderStructs();
        this.setMaxParticles(var1.getMaxParticles());
    }

    public static int getGlobalAliveParticles() {
        return globalAliveParticles;
    }

    // $FF: synthetic method
    static int[] $SWITCH_TABLE$taikodom$render$enums$BillboardAlignment() {
        int[] var10000 = $SWITCH_TABLE$taikodom$render$enums$BillboardAlignment;
        if ($SWITCH_TABLE$taikodom$render$enums$BillboardAlignment != null) {
            return var10000;
        } else {
            int[] var0 = new int[BillboardAlignment.values().length];

            try {
                var0[BillboardAlignment.AXIS_ALIGNED.ordinal()] = 2;
            } catch (NoSuchFieldError var4) {
                ;
            }

            try {
                var0[BillboardAlignment.SCREEN_ALIGNED.ordinal()] = 3;
            } catch (NoSuchFieldError var3) {
                ;
            }

            try {
                var0[BillboardAlignment.USER_ALIGNED.ordinal()] = 4;
            } catch (NoSuchFieldError var2) {
                ;
            }

            try {
                var0[BillboardAlignment.VIEW_ALIGNED.ordinal()] = 1;
            } catch (NoSuchFieldError var1) {
                ;
            }

            $SWITCH_TABLE$taikodom$render$enums$BillboardAlignment = var0;
            return var0;
        }
    }

    public aLH computeWorldSpaceAABB() {
        this.addChildrenWorldSpaceAABB();
        this.aabbWSLenght = this.aabbWorldSpace.din().ax(this.aabbWorldSpace.dim());
        return this.aabbWorldSpace;
    }

    public boolean render(RenderContext var1, boolean var2) {
        if (this.render) {
            if (!super.render(var1, var2)) {
                return false;
            } else {
                this.cameraSpaceTransform.setIdentity();
                this.cameraSpaceTransform.setTranslation(var1.getCamera().getPosition());
                if (!var2 || var1.getFrustum().a(this.aabbWorldSpace)) {
                    this.generateMesh(var1);
                    if (this.mesh.getIndexes().size() > 3) {
                        float var3 = var1.getSizeOnScreen(this.globalTransform.position, this.aabbWSLenght);

                        for (int var4 = 0; var4 < this.material.textureCount(); ++var4) {
                            BaseTexture var5 = this.material.getTexture(var4);
                            if (var5 != null) {
                                var5.addArea(var3);
                            }
                        }
                        var1.addPrimitive(this.material, this.mesh, this.cameraSpaceTransform, this.primitiveColor, this.occlusionQuery, this.renderPriority, this, this.aabbWorldSpace, this.currentSquaredDistanceToCamera);
                        var1.incParticleSystemsRendered();
                    }
                }
                return true;
            }
        } else {
            return false;
        }
    }

    public RenderAsset cloneAsset() {
        return new RParticleSystem(this);
    }

    public float getNewPartsSecond() {
        return this.newPartsSecond;
    }

    public void setNewPartsSecond(float var1) {
        this.newPartsSecond = var1;
    }

    public int getMaxParticles() {
        return this.maxParticles;
    }

    public void setMaxParticles(int var1) {
        if (var1 == 0) {
            var1 = 2;
        }

        this.maxParticles = var1;
        this.particlesPool.clear();
        this.particlesPool.ensureCapacity(var1);

        int var2;
        for (var2 = 0; var2 < var1; ++var2) {
            this.particlesPool.add(new RParticleSystem.a());
        }

        this.indexes.ensure(var1 * 4);
        this.vertexes.ensure(var1 * 4);
        this.indexes.clear();
        var2 = 0;

        for (int var3 = 0; var3 < var1; ++var3) {
            this.indexes.setIndex(var2, var2);
            this.vertexes.setTexCoord(var2++, 0, 0.0F, 0.0F);
            this.indexes.setIndex(var2, var2);
            this.vertexes.setTexCoord(var2++, 0, 1.0F, 0.0F);
            this.indexes.setIndex(var2, var2);
            this.vertexes.setTexCoord(var2++, 0, 1.0F, 1.0F);
            this.indexes.setIndex(var2, var2);
            this.vertexes.setTexCoord(var2++, 0, 0.0F, 1.0F);
        }

    }

    public float getParticlesMaxLifeTime() {
        return this.particlesMaxLifeTime;
    }

    public void setParticlesMaxLifeTime(float var1) {
        this.particlesMaxLifeTime = var1;
    }

    public float getParticlesLifeTimeVariation() {
        return this.particlesLifeTimeVariation;
    }

    public void setParticlesLifeTimeVariation(float var1) {
        this.particlesLifeTimeVariation = var1;
    }

    public float getParticlesInitialSize() {
        return this.particlesInitialSize;
    }

    public void setParticlesInitialSize(float var1) {
        this.particlesInitialSize = var1;
    }

    public float getParticlesFinalSize() {
        return this.particlesFinalSize;
    }

    public void setParticlesFinalSize(float var1) {
        this.particlesFinalSize = var1;
    }

    public float getParticlesInitialSizeVariation() {
        return this.particlesInitialSizeVariation;
    }

    public void setParticlesInitialSizeVariation(float var1) {
        this.particlesInitialSizeVariation = var1;
    }

    public float getParticlesFinalSizeVariation() {
        return this.particlesFinalSizeVariation;
    }

    public void setParticlesFinalSizeVariation(float var1) {
        this.particlesFinalSizeVariation = var1;
    }

    public boolean isFollowEmitter() {
        return this.followEmitter;
    }

    public void setFollowEmitter(boolean var1) {
        this.followEmitter = var1;
    }

    public Vector3dOperations getParticlesVelocity() {
        return this.particlesVelocity;
    }

    public void setParticlesVelocity(Vector3dOperations var1) {
        this.particlesVelocity.aA(var1);
    }

    public Color getParticlesInitialColor() {
        return this.particlesInitialColor;
    }

    public void setParticlesInitialColor(Color var1) {
        this.particlesInitialColor.set(var1);
    }

    public Color getParticlesFinalColor() {
        return this.particlesFinalColor;
    }

    public void setParticlesFinalColor(Color var1) {
        this.particlesFinalColor.set(var1);
    }

    public float getInitialMaxAngle() {
        return this.initialMaxAngle;
    }

    public void setInitialMaxAngle(float var1) {
        this.initialMaxAngle = var1;
    }

    public Vector3dOperations getParticlesVelocityVariation() {
        return this.particlesVelocityVariation;
    }

    public void setParticlesVelocityVariation(Vector3dOperations var1) {
        this.particlesVelocityVariation.aA(var1);
    }

    private void initRenderStructs() {
        VertexLayout var1 = new VertexLayout(true, true, false, false, 1);
        this.vertexes = new ExpandableVertexData(var1, 100);
        this.indexes = new ExpandableIndexData(100);
        this.mesh = new Mesh();
        this.mesh.setName("RParticleSystem_mesh");
        this.mesh.setGeometryType(GeometryType.QUADS);
        this.mesh.setVertexData(this.vertexes);
        this.mesh.setIndexes(this.indexes);
    }

    public float getMinRotationSpeed() {
        return this.minRotationSpeed;
    }

    public void setMinRotationSpeed(float var1) {
        this.minRotationSpeed = var1;
    }

    public float getMaxRotationSpeed() {
        return this.maxRotationSpeed;
    }

    public void setMaxRotationSpeed(float var1) {
        this.maxRotationSpeed = var1;
    }

    public BillboardAlignment getAlignment() {
        return this.alignment;
    }

    public void setAlignment(BillboardAlignment var1) {
        this.alignment = var1;
    }

    public Vec3f getAlignmentDirection() {
        return this.alignmentDirection;
    }

    public void setAlignmentDirection(Vec3f var1) {
        this.alignmentDirection.set(var1);
    }

    public int getInitialBurst() {
        return this.initialBurst;
    }

    public void setInitialBurst(int var1) {
        this.initialBurst = var1;
        this.currentParticlesToSpawn += (float) var1;
    }

    public float getSystemLifeTime() {
        return this.systemLifeTime;
    }

    public void setSystemLifeTime(float var1) {
        this.systemLifeTime = var1;
    }

    public float getInitialDelay() {
        return this.initialDelay;
    }

    public void setInitialDelay(float var1) {
        this.initialDelay = var1;
    }

    public boolean isAlignToMovement() {
        return this.alignToMovement;
    }

    public void setAlignToMovement(boolean var1) {
        this.alignToMovement = var1;
    }

    public boolean isAlignToHorizon() {
        return this.alignToHorizon;
    }

    public void setAlignToHorizon(boolean var1) {
        this.alignToHorizon = var1;
    }

    public float getInitialMinAngle() {
        return this.initialMinAngle;
    }

    public void setInitialMinAngle(float var1) {
        this.initialMinAngle = var1;
    }

    private void updateParticlesLifeTime(StepContext var1) {
        this.indexes.clear();
        this.mesh.getAabb().reset();
        float var2 = var1.getDeltaTime();
        this.currentAliveParticles = 0;
        this.currentDelay += var2;
        if (this.currentDelay >= this.initialDelay) {
            this.currentParticlesToSpawn += var2 * this.newPartsSecond;
            int var3 = aip.jv(this.currentParticlesToSpawn);
            this.currentParticlesToSpawn -= (float) var3;
            this.particlesLimit = this.maxParticles;
            if (var3 > this.particlesLimit) {
                var3 = this.particlesLimit;
            }

            if (this.currentSquaredDistanceToCamera > this.spawnLimitStart) {
                double var4 = 1.0D - (this.currentSquaredDistanceToCamera - this.spawnLimitStart) / (this.spawnLimitEnd * 0.8D);
                if (var4 < 0.0D) {
                    this.particlesLimit = 3;
                } else {
                    this.particlesLimit = (int) (var4 * (double) this.particlesLimit);
                }
            }

            if (this.particlesLimit < 0 || this.currentSquaredDistanceToCamera > this.spawnLimitEnd) {
                this.particlesLimit = 0;
            }

            if (this.systemLifeTime > 0.0F) {
                this.currentLifeTime += var2;
                if (this.currentLifeTime > this.systemLifeTime) {
                    this.currentParticlesToSpawn = 0.0F;
                    var3 = 0;
                    this.isDead = true;
                }
            }

            float var8 = 1.0F / (float) var3;
            int var5 = this.particlesLimit;
            Iterator var7 = this.particlesPool.iterator();

            while (var7.hasNext()) {
                RParticleSystem.a var6 = (RParticleSystem.a) var7.next();
                --var5;
                if (var5 <= 0) {
                    break;
                }

                if (var6.imi) {
                    if ((var6.lifeTime -= var2) <= 0.0F) {
                        var6.imi = false;
                        --globalAliveParticles;
                    } else {
                        var6.position.x += var6.imd.x * (double) var2;
                        var6.position.y += var6.imd.y * (double) var2;
                        var6.position.z += var6.imd.z * (double) var2;
                        var6.imd.x += var6.ime.x * (double) var2;
                        var6.imd.y += var6.ime.y * (double) var2;
                        var6.imd.z += var6.ime.z * (double) var2;
                        var6.size += var6.imh * var2;
                        var6.color.x += var6.imf.x * var2;
                        var6.color.y += var6.imf.y * var2;
                        var6.color.z += var6.imf.z * var2;
                        var6.color.w += var6.imf.w * var2;
                        var6.deX += var6.img * var2;
                        if (var6.color.x < 0.0F) {
                            var6.color.x = 0.0F;
                        }

                        if (var6.color.y < 0.0F) {
                            var6.color.y = 0.0F;
                        }

                        if (var6.color.z < 0.0F) {
                            var6.color.z = 0.0F;
                        }

                        if (var6.color.w < 0.0F) {
                            var6.color.w = 0.0F;
                        }

                        ++this.currentAliveParticles;
                        this.mesh.getAabb().aG(var6.position);
                    }
                } else if (var3 > 0 && this.active && globalAliveParticles < 5000 && this.currentSquaredDistanceToCamera < this.maxSpawnDistance) {
                    this.spawnParticle(var6, var8 * (float) var3);
                    --var3;
                    ++globalAliveParticles;
                }
            }

            if (this.isDead && this.currentAliveParticles == 0 && !var1.getEditMode()) {
                this.dispose();
            } else if (this.currentAliveParticles > 0) {
                float var9 = this.particlesFinalSize > this.particlesInitialSize ? this.particlesFinalSize : this.particlesInitialSize;
                this.mesh.getAabb().C(this.mesh.getAabb().din().x + (double) var9, this.mesh.getAabb().din().y + (double) var9, this.mesh.getAabb().din().z + (double) var9);
                this.mesh.getAabb().C(this.mesh.getAabb().dim().x - (double) var9, this.mesh.getAabb().dim().y - (double) var9, this.mesh.getAabb().dim().z - (double) var9);
            }

        }
    }

    private void spawnParticle(RParticleSystem.a var1, float var2) {
        var1.imi = true;
        var1.lifeTime = this.particlesMaxLifeTime + aip.cbI() * this.particlesLifeTimeVariation;
        var1.imd.x = this.transformedParticlesVelocity.x + this.particlesVelocityVariation.x * (double) aip.cbI();
        var1.imd.y = this.transformedParticlesVelocity.y + this.particlesVelocityVariation.y * (double) aip.cbI();
        var1.imd.z = this.transformedParticlesVelocity.z + this.particlesVelocityVariation.z * (double) aip.cbI();
        var1.ime.aA(this.particlesAcceleration);
        var1.color.x = this.particlesInitialColor.x * (1.0F + aip.cbI() * this.particlesColorVariation.x);
        var1.color.y = this.particlesInitialColor.y * (1.0F + aip.cbI() * this.particlesColorVariation.y);
        var1.color.z = this.particlesInitialColor.z * (1.0F + aip.cbI() * this.particlesColorVariation.z);
        var1.color.w = this.particlesInitialColor.w * (1.0F + aip.cbI() * this.particlesColorVariation.w);
        var1.imf.x = (this.particlesFinalColor.x - var1.color.x) / var1.lifeTime;
        var1.imf.y = (this.particlesFinalColor.y - var1.color.y) / var1.lifeTime;
        var1.imf.z = (this.particlesFinalColor.z - var1.color.z) / var1.lifeTime;
        var1.imf.w = (this.particlesFinalColor.w - var1.color.w) / var1.lifeTime;
        var1.size = this.particlesInitialSize + aip.cbI() * this.particlesInitialSizeVariation;
        var1.imh = (this.particlesFinalSize + aip.cbI() * this.particlesFinalSizeVariation - var1.size) / var1.lifeTime;
        var1.deX = this.initialMinAngle + (this.initialMaxAngle - this.initialMinAngle) * aip.cbJ();
        var1.img = this.minRotationSpeed + (this.maxRotationSpeed - this.minRotationSpeed) * aip.cbJ();
        if (this.followEmitter) {
            float var3 = var1.lifeTime * (1.0F - var2);
            var1.lifeTime *= var2;
            var1.position.x = var1.imd.x * (double) var3;
            var1.position.y = var1.imd.y * (double) var3;
            var1.position.z = var1.imd.z * (double) var3;
            var1.color.x += var1.imf.x * var3;
            var1.color.y += var1.imf.y * var3;
            var1.color.z += var1.imf.z * var3;
            var1.color.w += var1.imf.w * var3;
            var1.size += var1.imh * var3;
        } else {
            var1.position.x = (this.globalTransform.position.x - this.lastPosition.x) * (double) var2 + this.lastPosition.x;
            var1.position.y = (this.globalTransform.position.y - this.lastPosition.y) * (double) var2 + this.lastPosition.y;
            var1.position.z = (this.globalTransform.position.z - this.lastPosition.z) * (double) var2 + this.lastPosition.z;
        }

    }

    public int step(StepContext var1) {
        if (this.firstFrame) {
            this.firstFrame = false;
            this.lastPosition.aA(this.globalTransform.position);
        }

        if (var1.getCamera() != null) {
            this.currentSquaredDistanceToCamera = this.globalTransform.g(var1.getCamera().globalTransform);
        }

        this.updateParticlesLifeTime(var1);
        this.globalTransform.multiply3x3(this.particlesVelocity, this.transformedParticlesVelocity);
        if (this.currentAliveParticles > 0) {
            if (this.followEmitter) {
                this.mesh.getAabb().a(this.globalTransform, this.aabbWorldSpace);
                this.aabbWorldSpace.g(this.mesh.getAabb());
                this.aabbWorldSpace.din().add(this.globalTransform.position);
                this.aabbWorldSpace.dim().add(this.globalTransform.position);
            } else {
                this.aabbWorldSpace.g(this.mesh.getAabb());
            }
        }

        this.lastPosition.aA(this.globalTransform.position);
        var1.incParticleSystemStepped();
        return this.disposed ? 1 : 0;
    }

    void generateMeshDefaultAligned(Vec3f var1, Vec3f var2, Vector3dOperations var3) {
        this.mesh.getAabb().reset();
        double var22 = 0.0D;
        double var24 = 0.0D;
        double var26 = 0.0D;
        if (this.followEmitter) {
            var22 = this.globalTransform.position.x;
            var24 = this.globalTransform.position.y;
            var26 = this.globalTransform.position.z;
        }

        var22 -= var3.x;
        var24 -= var3.y;
        var26 -= var3.z;
        int var31 = 0;
        int var32 = 0;
        int var33 = this.particlesLimit;
        this.indexes.clear();
        Iterator var35 = this.particlesPool.iterator();

        while (var35.hasNext()) {
            RParticleSystem.a var34 = (RParticleSystem.a) var35.next();
            if (var34.imi) {
                --var33;
                if (var33 <= 0) {
                    break;
                }

                ++var31;
                double var16 = var34.position.x + var22;
                double var18 = var34.position.y + var24;
                double var20 = var34.position.z + var26;
                this.mesh.getAabb().C(var16, var18, var20);
                double var4 = (double) (var1.x * var34.size);
                double var6 = (double) (var1.y * var34.size);
                double var8 = (double) (var1.z * var34.size);
                double var10 = (double) (var2.x * var34.size);
                double var12 = (double) (var2.y * var34.size);
                double var14 = (double) (var2.z * var34.size);
                float var28 = 1.4142137F * (float) Math.cos((double) ((var34.deX + 45.0F) * 0.017453292F));
                float var29 = 1.4142137F * (float) Math.sin((double) ((var34.deX + 45.0F) * 0.017453292F));
                this.vertexes.setPosition(var32, (float) (var16 + var4 * (double) var28 + var10 * (double) var29), (float) (var18 + var6 * (double) var28 + var12 * (double) var29), (float) (var20 + var8 * (double) var28 + var14 * (double) var29));
                this.vertexes.setColor(var32, var34.color.x, var34.color.y, var34.color.z, var34.color.w);
                ++var32;
                float var30 = -var29;
                var29 = var28;
                var28 = var30;
                this.vertexes.setPosition(var32, (float) (var16 + var4 * (double) var30 + var10 * (double) var29), (float) (var18 + var6 * (double) var30 + var12 * (double) var29), (float) (var20 + var8 * (double) var30 + var14 * (double) var29));
                this.vertexes.setColor(var32, var34.color.x, var34.color.y, var34.color.z, var34.color.w);
                ++var32;
                var30 = -var29;
                var29 = var28;
                var28 = var30;
                this.vertexes.setPosition(var32, (float) (var16 + var4 * (double) var30 + var10 * (double) var29), (float) (var18 + var6 * (double) var30 + var12 * (double) var29), (float) (var20 + var8 * (double) var30 + var14 * (double) var29));
                this.vertexes.setColor(var32, var34.color.x, var34.color.y, var34.color.z, var34.color.w);
                ++var32;
                var30 = -var29;
                this.vertexes.setPosition(var32, (float) (var16 + var4 * (double) var30 + var10 * (double) var28), (float) (var18 + var6 * (double) var30 + var12 * (double) var28), (float) (var20 + var8 * (double) var30 + var14 * (double) var28));
                this.vertexes.setColor(var32, var34.color.x, var34.color.y, var34.color.z, var34.color.w);
                ++var32;
            }
        }

        this.indexes.setSize(var31 * 4);
    }

    void generateMesh(RenderContext var1) {
        Camera var2 = var1.getCamera();
        var2.getTransform().getX(var1.vec3fTemp0);
        var2.getTransform().getY(var1.vec3fTemp1);
        var2.getTransform().getZ(var1.vec3fTemp2);
        var2.getTransform().getTranslation(var1.vec3dTemp0);
        if (this.alignToHorizon) {
            this.generateMeshHorizonAligned(var1.vec3fTemp0, var1.vec3fTemp2, var1.vec3dTemp0);
        } else if (this.alignToMovement) {
            this.generateMeshMovementAligned(var1.vec3fTemp2, var1.vec3dTemp0);
        } else {
            switch ($SWITCH_TABLE$taikodom$render$enums$BillboardAlignment()[this.alignment.ordinal()]) {
                case 1:
                case 3:
                    if (this.material != null && this.material.getShader() != null && this.material.getShader().getBestShader(var1.getDc()).getPass(0).getProgram() != null) {
                        this.generateMeshForGPUAlignment(var1.vec3dTemp0);
                        break;
                    }

                    this.generateMeshDefaultAligned(var1.vec3fTemp0, var1.vec3fTemp1, var1.vec3dTemp0);
                    break;
                case 2:
                    this.generateMeshAxisAligned(var1.vec3fTemp2, var1.vec3dTemp0);
                    break;
                case 4:
                    this.globalTransform.getX(var1.vec3fTemp0);
                    this.globalTransform.getY(var1.vec3fTemp1);
                    this.generateMeshHorizonAligned(var1.vec3fTemp0, var1.vec3fTemp1, var1.vec3dTemp0);
                    break;
                default:
                    this.generateMeshDefaultAligned(var1.vec3fTemp0, var1.vec3fTemp1, var1.vec3dTemp0);
            }

            if (this.mesh != null) {
                this.localAabb.reset();
                if (!this.mesh.getAabb().dim().anA() || !this.mesh.getAabb().din().anA()) {
                    this.mesh.getAabb().C(0.0D, 0.0D, 0.0D);
                }

                this.localAabb.h(this.mesh.getAabb());
            }
        }

    }

    private void generateMeshMovementAligned(Vec3f var1, Vector3dOperations var2) {
        double var21 = 0.0D;
        double var23 = 0.0D;
        double var25 = 0.0D;
        if (this.followEmitter) {
            var21 = this.globalTransform.position.x;
            var23 = this.globalTransform.position.y;
            var25 = this.globalTransform.position.z;
        }

        var21 -= var2.x;
        var23 -= var2.y;
        var25 -= var2.z;
        int var27 = 0;
        int var28 = 0;
        Vec3f var29 = new Vec3f();
        Vec3f var30 = new Vec3f();
        this.indexes.clear();
        Iterator var32 = this.particlesPool.iterator();

        while (var32.hasNext()) {
            RParticleSystem.a var31 = (RParticleSystem.a) var32.next();
            if (var31.imi) {
                ++var27;
                double var15 = var31.position.x + var21;
                double var17 = var31.position.y + var23;
                double var19 = var31.position.z + var25;
                var29.x = (float) var31.imd.x;
                var29.y = (float) var31.imd.y;
                var29.z = (float) var31.imd.z;
                var29.normalize();
                var30.cross(var29, var1);
                var30.normalize();
                double var3 = (double) (var30.x * var31.size);
                double var5 = (double) (var30.y * var31.size);
                double var7 = (double) (var30.z * var31.size);
                double var9 = (double) (var29.x * var31.size);
                double var11 = (double) (var29.y * var31.size);
                double var13 = (double) (var29.z * var31.size);
                this.vertexes.setPosition(var28, (float) (var15 - (var3 + var9)), (float) (var17 - (var5 + var11)), (float) (var19 - (var7 + var13)));
                this.vertexes.setColor(var28, var31.color.x, var31.color.y, var31.color.z, var31.color.w);
                ++var28;
                this.vertexes.setPosition(var28, (float) (var15 + (var3 - var9)), (float) (var17 + (var5 - var11)), (float) (var19 + (var7 - var13)));
                this.vertexes.setColor(var28, var31.color.x, var31.color.y, var31.color.z, var31.color.w);
                ++var28;
                this.vertexes.setPosition(var28, (float) (var15 + var3 + var9), (float) (var17 + var5 + var11), (float) (var19 + var7 + var13));
                this.vertexes.setColor(var28, var31.color.x, var31.color.y, var31.color.z, var31.color.w);
                ++var28;
                this.vertexes.setPosition(var28, (float) (var15 + -var3 + var9), (float) (var17 + -var5 + var11), (float) (var19 + -var7 + var13));
                this.vertexes.setColor(var28, var31.color.x, var31.color.y, var31.color.z, var31.color.w);
                ++var28;
            }
        }

        this.indexes.setSize(var27 * 4);
    }

    private void generateMeshHorizonAligned(Vec3f var1, Vec3f var2, Vector3dOperations var3) {
        double var22 = 0.0D;
        double var24 = 0.0D;
        double var26 = 0.0D;
        if (this.followEmitter) {
            var22 = this.globalTransform.position.x;
            var24 = this.globalTransform.position.y;
            var26 = this.globalTransform.position.z;
        }

        var22 -= var3.x;
        var24 -= var3.y;
        var26 -= var3.z;
        int var28 = 0;
        int var29 = 0;
        int var30 = this.particlesLimit;
        Iterator var32 = this.particlesPool.iterator();

        while (var32.hasNext()) {
            RParticleSystem.a var31 = (RParticleSystem.a) var32.next();
            if (var31.imi) {
                --var30;
                if (var30 <= 0) {
                    break;
                }

                ++var28;
                double var16 = var31.position.x + var22;
                double var18 = var31.position.y + var24;
                double var20 = var31.position.z + var26;
                double var4 = (double) (var1.x * var31.size);
                double var6 = (double) (var1.y * var31.size);
                double var8 = (double) (var1.z * var31.size);
                double var10 = (double) (var2.x * var31.size);
                double var12 = (double) (var2.y * var31.size);
                double var14 = (double) (var2.z * var31.size);
                this.vertexes.setPosition(var29, (float) (var16 - var4 - var10), (float) (var18 - var6 - var12), (float) (var20 - var8 - var14));
                this.vertexes.setColor(var29, var31.color.x, var31.color.y, var31.color.z, var31.color.w);
                ++var29;
                this.vertexes.setPosition(var29, (float) (var16 + var4 - var10), (float) (var18 + var6 - var12), (float) (var20 + var8 - var14));
                this.vertexes.setColor(var29, var31.color.x, var31.color.y, var31.color.z, var31.color.w);
                ++var29;
                this.vertexes.setPosition(var29, (float) (var16 + var4 + var10), (float) (var18 + var6 + var12), (float) (var20 + var8 + var14));
                this.vertexes.setColor(var29, var31.color.x, var31.color.y, var31.color.z, var31.color.w);
                ++var29;
                this.vertexes.setPosition(var29, (float) (var16 - var4 + var10), (float) (var18 - var6 + var12), (float) (var20 - var8 + var14));
                this.vertexes.setColor(var29, var31.color.x, var31.color.y, var31.color.z, var31.color.w);
                ++var29;
            }
        }

        this.indexes.setSize(var28 * 4);
    }

    private void generateMeshAxisAligned(Vec3f var1, Vector3dOperations var2) {
        this.mesh.getAabb().reset();
    }

    private void generateMeshForGPUAlignment(Vector3dOperations var1) {
        double var8 = 0.0D;
        double var10 = 0.0D;
        double var12 = 0.0D;
        if (this.followEmitter) {
            var8 = this.globalTransform.position.x;
            var10 = this.globalTransform.position.y;
            var12 = this.globalTransform.position.z;
        }

        var8 -= var1.x;
        var10 -= var1.y;
        var12 -= var1.z;
        int var14 = 0;
        float var15 = 0.0F;
        float var16 = 0.0F;
        float var17 = 0.255F;
        this.indexes.clear();
        int var18 = 0;
        Iterator var20 = this.particlesPool.iterator();

        while (var20.hasNext()) {
            RParticleSystem.a var19 = (RParticleSystem.a) var20.next();
            if (var19.imi) {
                ++var14;
                double var2 = var19.position.x + var8;
                double var4 = var19.position.y + var10;
                double var6 = var19.position.z + var12;
                this.vertexes.setPosition(var18, (float) var2, (float) var4, (float) var6);
                this.vertexes.setPosition(var18 + 1, (float) var2, (float) var4, (float) var6);
                this.vertexes.setPosition(var18 + 2, (float) var2, (float) var4, (float) var6);
                this.vertexes.setPosition(var18 + 3, (float) var2, (float) var4, (float) var6);
                var15 = aip.ju(var19.color.x * 255.0F);
                var15 += var17 * var19.color.y;
                var16 = aip.ju(var19.color.z * 255.0F);
                var16 += var17 * var19.color.w;
                this.vertexes.setColor(var18, var15, var16, var19.size, var19.deX);
                this.vertexes.setColor(var18 + 1, var15, var16, var19.size, var19.deX);
                this.vertexes.setColor(var18 + 2, var15, var16, var19.size, var19.deX);
                this.vertexes.setColor(var18 + 3, var15, var16, var19.size, var19.deX);
                var18 += 4;
            }
        }

        this.indexes.setSize(var14 * 4);
    }

    public void releaseReferences() {
        super.releaseReferences();
        this.mesh = null;
        this.indexes = null;
        this.vertexes = null;
        this.releaseParticles();
    }

    public boolean isNeedToStep() {
        return true;
    }

    public void dispose() {
        super.dispose();
        this.releaseParticles();
    }

    private void releaseParticles() {
        Iterator var2 = this.particlesPool.iterator();

        while (var2.hasNext()) {
            RParticleSystem.a var1 = (RParticleSystem.a) var2.next();
            if (var1.imi) {
                --globalAliveParticles;
                var1.imi = false;
            }
        }

        this.particlesPool.clear();
    }

    protected void finalize() {
        // super.finalize();
        this.releaseParticles();
    }

    public void validate(SceneLoader var1) {
        super.validate(var1);
        if (this.material.getShader() == null) {
            this.material.setShader(var1.getDefaultTrailShader());
        }

    }

    public Vector3dOperations getParticlesAcceleration() {
        return this.particlesAcceleration;
    }

    public void setParticlesAcceleration(Vector3dOperations var1) {
        this.particlesAcceleration.aA(var1);
    }

    public Color getParticlesColorVariation() {
        return this.particlesColorVariation;
    }

    public void setParticlesColorVariation(Color var1) {
        this.particlesColorVariation.set(var1);
    }

    public aLH computeLocalSpaceAABB() {
        return this.localAabb;
    }

    public double getSpawnLimitStart() {
        return this.spawnLimitStart;
    }

    public void setSpawnLimitStart(double var1) {
        this.spawnLimitStart = var1;
    }

    public double getSpawnLimitEnd() {
        return this.spawnLimitEnd;
    }

    public void setSpawnLimitEnd(double var1) {
        this.spawnLimitEnd = var1;
    }

    public void onRemove() {
        super.onRemove();
        this.releaseParticles();
    }

    class a {
        final Vector3dOperations position = new Vector3dOperations();
        final Vector3dOperations imc = new Vector3dOperations();
        final Vector3dOperations imd = new Vector3dOperations();
        final Vector3dOperations ime = new Vector3dOperations();
        final Color color = new Color(1.0F, 1.0F, 1.0F, 1.0F);
        final Color imf = new Color(0.0F, 0.0F, 0.0F, 0.0F);
        float img = 0.0F;
        float deX = 0.0F;
        float lifeTime = 0.0F;
        float size = 1.0F;
        float imh = 0.0F;
        boolean imi = false;
    }
}
