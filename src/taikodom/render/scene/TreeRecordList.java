package taikodom.render.scene;

import taikodom.render.enums.BlendType;
import taikodom.render.primitives.TreeRecord;
import taikodom.render.shaders.RenderStates;
import taikodom.render.shaders.ShaderPass;

import java.util.*;

public class TreeRecordList {
    private List data = new ArrayList();
    private LinkedList poolData = new LinkedList();

    public TreeRecord add() {
        TreeRecord var1;
        if (this.poolData.size() > 0) {
            var1 = (TreeRecord) this.poolData.removeLast();
            this.data.add(var1);
            return var1;
        } else {
            var1 = new TreeRecord();
            this.data.add(var1);
            return var1;
        }
    }

    public void clear() {
        this.poolData.addAll(this.data);
        this.data.clear();
    }

    public TreeRecord get(int var1) {
        return (TreeRecord) this.data.get(var1);
    }

    public int size() {
        return this.data.size();
    }

    public void sort() {
        Collections.sort(this.data, new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                return a((TreeRecord) o1, (TreeRecord) o2);
            }

            public int a(TreeRecord var1, TreeRecord var2) {
                if ((var1.entryPriority > 0 || var2.entryPriority > 0) && var1.entryPriority != var2.entryPriority) {
                    return var2.entryPriority - var1.entryPriority;
                } else {
                    ShaderPass var3 = var1.shader.getPass(0);
                    ShaderPass var4 = var2.shader.getPass(0);
                    RenderStates var5 = var3.getRenderStates();
                    RenderStates var6 = var4.getRenderStates();
                    if (var1.shader.getHandle() == var2.shader.getHandle()) {
                        if (var5.isBlendEnabled()) {
                            return (int) (var2.squaredDistanceToCamera - var1.squaredDistanceToCamera);
                        } else if (var1.entryPriority != var2.entryPriority) {
                            return var2.entryPriority - var1.entryPriority;
                        } else if (var1.material.getHandle() == var2.material.getHandle()) {
                            if (var1.primitive.getHandle() == var2.primitive.getHandle()) {
                                return (int) (var1.squaredDistanceToCamera - var2.squaredDistanceToCamera);
                            } else {
                                return var1.primitive.getHandle() > var2.primitive.getHandle() ? 1 : -1;
                            }
                        } else {
                            return var1.material.getHandle() > var2.material.getHandle() ? 1 : -1;
                        }
                    } else if (var5.isBlendEnabled() && var6.isBlendEnabled()) {
                        if (var1.entryPriority != var2.entryPriority) {
                            return var2.entryPriority - var1.entryPriority;
                        } else if (var5.isDepthMask() && !var6.isDepthMask()) {
                            return -1;
                        } else if (var6.isDepthMask() && !var5.isDepthMask()) {
                            return 1;
                        } else if (var5.getDepthRangeMin() != var6.getDepthRangeMin()) {
                            return (int) (1000.0F * var6.getDepthRangeMin() - 1000.0F * var5.getDepthRangeMin());
                        } else {
                            if (var5.getDstBlend() != var6.getDstBlend()) {
                                if (var5.getDstBlend() == BlendType.ONE_MINUS_SRC_ALPHA) {
                                    return -1;
                                }

                                if (var6.getDstBlend() == BlendType.ONE_MINUS_SRC_ALPHA) {
                                    return 1;
                                }
                            }

                            return (int) (var2.squaredDistanceToCamera - var1.squaredDistanceToCamera);
                        }
                    } else if (var5.isBlendEnabled()) {
                        return 1;
                    } else if (var6.isBlendEnabled()) {
                        return -1;
                    } else if (var1.shader.getPriority() != var2.shader.getPriority()) {
                        return var2.shader.getPriority() - var1.shader.getPriority();
                    } else if (var1.entryPriority != var2.entryPriority) {
                        return var2.entryPriority - var1.entryPriority;
                    } else {
                        return var1.shader.getHandle() > var2.shader.getHandle() ? 1 : -1;
                    }
                }
            }
        });
    }
}
