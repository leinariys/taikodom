package taikodom.render.scene;

import all.aLH;
import taikodom.render.loader.RenderAsset;

/**
 * Реализация класса указанного в имени тегов из файлов с расширением .pro
 */
public class RGroup extends SceneObject {
    public RGroup() {
    }

    protected RGroup(RGroup var1) {
        super(var1);
    }

    public RenderAsset cloneAsset() {
        return new RGroup(this);
    }

    public aLH computeWorldSpaceAABB() {
        super.computeWorldSpaceAABB();
        this.aabbWorldSpace.aG(this.globalTransform.position);
        this.aabbWSLenght = this.aabbWorldSpace.din().ax(this.aabbWorldSpace.dim());
        return this.aabbWorldSpace;
    }
}
