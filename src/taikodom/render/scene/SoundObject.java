package taikodom.render.scene;

public abstract class SoundObject extends SceneObject {
    public SoundObject() {
    }

    public SoundObject(SoundObject var1) {
        super(var1);
    }

    public void releaseReferences() {
    }

    public boolean isNeedToStep() {
        return true;
    }

    public int getLoopCount() {
        return 1;
    }

    public void setLoopCount(int var1) {
    }

    public float getGain() {
        return 1.0F;
    }

    public void setGain(float var1) {
    }

    public void setPitch(float var1) {
    }

    public boolean play() {
        return false;
    }

    public boolean stop() {
        return false;
    }

    public boolean isPlaying() {
        return false;
    }

    public boolean fadeOut(float var1) {
        return false;
    }

    public boolean fadeIn(float var1) {
        return false;
    }

    public boolean isValid() {
        return false;
    }

    public SoundSource getSource() {
        return null;
    }

    public void setControllerValue(float var1) {
    }

    public boolean isAlive() {
        return false;
    }

    float getAudibleRadius() {
        return 0.0F;
    }

    void setAudibleRadius(float var1) {
    }

    public float getSamplePosition() {
        return 0.0F;
    }

    public float getTimeLenght() {
        return 0.0F;
    }
}
