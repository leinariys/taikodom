package taikodom.render.scene;

import all.ajD;
import com.hoplon.geometry.Vec3f;
import taikodom.render.StepContext;
import taikodom.render.loader.RenderAsset;

/**
 * Реализация класса указанного в имени тегов из файлов с расширением .pro
 */
public class RRotor extends SceneObject {
    private final Vec3f pos = new Vec3f();
    private final ajD rotationMatrix = new ajD();
    private final ajD tempMatrix = new ajD();
    float rotX;
    float rotY;
    float rotZ;

    public RRotor() {
    }

    public RRotor(RRotor var1) {
        this.rotationMatrix.set(var1.rotationMatrix);
        this.pos.set(var1.pos);
        this.rotX = var1.rotX;
        this.rotY = var1.rotY;
        this.rotZ = var1.rotZ;
    }

    public int step(StepContext var1) {
        this.rotX += var1.getDeltaTime() * this.angularVelocity.x;
        this.rotY += var1.getDeltaTime() * this.angularVelocity.y;
        this.rotZ += var1.getDeltaTime() * this.angularVelocity.z;
        this.rotationMatrix.setIdentity();
        this.tempMatrix.setIdentity();
        this.tempMatrix.rotateY(this.rotY);
        this.rotationMatrix.mul(this.tempMatrix);
        this.tempMatrix.setIdentity();
        this.tempMatrix.rotateX(this.rotX);
        this.rotationMatrix.mul(this.tempMatrix);
        this.tempMatrix.setIdentity();
        this.tempMatrix.rotateZ(this.rotZ);
        this.rotationMatrix.mul(this.tempMatrix);
        this.transform.getTranslation(this.pos);
        this.transform.mX.set(this.rotationMatrix);
        this.transform.setTranslation(this.pos);
        this.updateInternalGeometry((SceneObject) null);
        return 0;
    }

    public RenderAsset cloneAsset() {
        return new RRotor(this);
    }

    public boolean isNeedToStep() {
        return true;
    }
}
