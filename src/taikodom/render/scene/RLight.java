package taikodom.render.scene;

import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import taikodom.render.RenderContext;
import taikodom.render.enums.LightType;
import taikodom.render.loader.RenderAsset;
import taikodom.render.primitives.Light;

/**
 * Реализация класса указанного в имени тегов из файлов с расширением .pro
 */
public class RLight extends SceneObject {
    protected final Vec3f direction = new Vec3f(0.0F, 1.0F, 0.0F);
    protected Light light;
    protected Light renderLight;

    public RLight() {
        this.light = new Light();
    }

    public RLight(RLight var1) {
        super(var1);
        this.light = (Light) var1.light.cloneAsset();
        this.direction.set(var1.direction);
    }

    public boolean render(RenderContext var1, boolean var2) {
        if (this.render) {
            if (!super.render(var1, var2)) {
                return false;
            } else {
                this.light.setPosition(this.getGlobalTransform().position);
                this.globalTransform.mX.transform(this.direction, this.light.getDirection());
                if (!var2 || this.light.getType() == LightType.DIRECTIONAL_LIGHT || var1.getFrustum().a(this.light.getBoundingSphere()) && (var1.getSizeOnScreen(this.transform.position, (double) this.light.getRadius()) > 4.0F || !var1.isCullOutSmallObjects())) {
                    var1.addLight(this.light);
                    if (this.isMainLight()) {
                        var1.setMainLight(this.light);
                    }
                }

                return true;
            }
        } else {
            return false;
        }
    }

    public RenderAsset cloneAsset() {
        return new RLight(this);
    }

    public LightType getType() {
        return this.light.getLightType();
    }

    public void setType(LightType var1) {
        this.light.setType(var1);
    }

    public Light getLight() {
        return this.light;
    }

    public void setLight(Light var1) {
        this.light = var1;
    }

    public Light getRenderLight() {
        return this.renderLight;
    }

    public void setRenderLight(Light var1) {
        this.renderLight = var1;
    }

    public Color getDiffuseColor() {
        return this.light.getDiffuseColor();
    }

    public void setDiffuseColor(Color var1) {
        this.light.setDiffuseColor(var1);
    }

    public Color getSpecularColor() {
        return this.light.getSpecularColor();
    }

    public void setSpecularColor(Color var1) {
        this.light.setSpecularColor(var1);
    }

    public Vec3f getDirection() {
        return this.direction;
    }

    public void setDirection(Vec3f var1) {
        this.direction.set(var1);
    }

    public float getExponent() {
        return this.light.getExponent();
    }

    public void setExponent(float var1) {
        this.light.setExponent(var1);
    }

    public float getCutoff() {
        return this.light.getCutoff();
    }

    public void setCutoff(float var1) {
        this.light.setCutoff(var1);
    }

    public boolean isMainLight() {
        return this.light.isMainLight();
    }

    public void setMainLight(boolean var1) {
        this.light.setMainLight(var1);
    }

    public float getRadius() {
        return this.light.getRadius();
    }

    public void setRadius(float var1) {
        this.light.setRadius(var1);
    }
}
