package taikodom.render.scene;

import all.*;
import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;
import taikodom.render.RenderContext;
import taikodom.render.enums.BillboardAlignment;
import taikodom.render.enums.CullFace;
import taikodom.render.loader.RenderAsset;
import taikodom.render.loader.SceneLoader;
import taikodom.render.primitives.Billboard;
import taikodom.render.primitives.OcclusionQuery;
import taikodom.render.raytrace.RayTraceSceneObjectInfoD;
import taikodom.render.shaders.ShaderPass;

import javax.vecmath.Vector3d;

/**
 * Реализация класса указанного в имени тегов из файлов с расширением .pro
 */
public class RBillboard extends RenderObject {
    protected static final Billboard billboard = new Billboard();
    private static final int MIN_SCREEN_SIZE = 3;
    protected final Vec3f alignmentDirection;
    protected aQKa size = new aQKa();
    protected BillboardAlignment alignment;
    protected double maxSizeGlobal;
    private bc_q localTransform;
    private boolean disableDepthWrite;

    public RBillboard() {
        this.alignment = BillboardAlignment.VIEW_ALIGNED;
        this.alignmentDirection = new Vec3f(0.0F, 1.0F, 0.0F);
        this.maxSizeGlobal = 0.0D;
        this.localTransform = new bc_q();
        this.disableDepthWrite = false;
        this.size.set(128.0F, 128.0F);
    }

    public RBillboard(RBillboard var1) {
        super(var1);
        this.alignment = BillboardAlignment.VIEW_ALIGNED;
        this.alignmentDirection = new Vec3f(0.0F, 1.0F, 0.0F);
        this.maxSizeGlobal = 0.0D;
        this.localTransform = new bc_q();
        this.disableDepthWrite = false;
        this.size.set(var1.size);
        this.alignment = var1.alignment;
    }

    public aQKa getSize() {
        return this.size;
    }

    public void setSize(aQKa var1) {
        this.size.set(var1);
    }

    public void setSize(float var1, float var2) {
        this.size.set(var1, var2);
    }

    public Billboard getBillboard() {
        return billboard;
    }

    public boolean render(RenderContext var1, boolean var2) {
        if (this.render) {
            if (!super.render(var1, var2)) {
                return false;
            } else {
                this.doAlignment(var1);
                float var3 = var1.getSizeOnScreen(this.globalTransform.position, this.maxSizeGlobal);
                if (var3 >= 3.0F || !var1.isCullOutSmallObjects() || var1.getCamera().isOrthogonal()) {
                    ShaderPass var4 = this.material.getShader().getPass(0);
                    if (var4.isCullFaceEnabled() && !var1.getCamera().isOrthogonal() && this.alignment == BillboardAlignment.USER_ALIGNED) {
                        var1.vec3fTemp0.sub(var1.getCamera().getGlobalTransform().position, this.globalTransform.position);
                        this.globalTransform.getZ(var1.vec3fTemp1);
                        double var5 = (double) var1.vec3fTemp1.dot(var1.vec3fTemp0);
                        CullFace var7 = var4.getCullFace();
                        if (var7.equals(CullFace.BACK) && var5 < 0.0D) {
                            return true;
                        }

                        if (var7.equals(CullFace.FRONT) && var5 > 0.0D) {
                            return true;
                        }
                    }

                    if (!var2 || var1.getFrustum().a(this.aabbWorldSpace)) {
                        for (int var8 = 0; var8 < this.material.textureCount(); ++var8) {
                            if (this.material.getTexture(var8) != null) {
                                this.material.getTexture(var8).addArea(var3);
                            }
                        }

                        var1.addPrimitive(this.material, billboard, this.localTransform, this.primitiveColor, this.occlusionQuery, this.renderPriority, this, this.aabbWorldSpace, this.currentSquaredDistanceToCamera);
                        var1.incBillboardsRendered();
                    }
                }

                return true;
            }
        } else {
            return false;
        }
    }

    private void doAlignment(RenderContext var1) {
        if (this.alignment == BillboardAlignment.SCREEN_ALIGNED) {
            var1.getCamera().getTransform().getX(var1.vec3fTemp0);
            var1.vec3fTemp0.scale(this.size.x);
            var1.getCamera().getTransform().getY(var1.vec3fTemp1);
            var1.vec3fTemp1.scale(this.size.y);
            var1.getCamera().getTransform().getZ(var1.vec3fTemp2);
            this.localTransform.setX(var1.vec3fTemp0);
            this.localTransform.setY(var1.vec3fTemp1);
            this.localTransform.setZ(var1.vec3fTemp2);
            this.transform.getTranslation(var1.vec3dTemp0);
            this.localTransform.setTranslation(var1.vec3dTemp0);
        } else if (this.alignment == BillboardAlignment.AXIS_ALIGNED) {
            this.globalTransform.multiply3x3(this.alignmentDirection, var1.vec3fTemp0);
            var1.getCamera().getTransform().getTranslation(var1.vec3dTemp1);
            this.globalTransform.getTranslation(var1.vec3dTemp2);
            var1.vec3dTemp1.sub(var1.vec3dTemp2);
            var1.vec3dTemp1.dfX();
            var1.vec3fTemp1.set(var1.vec3dTemp1);
            var1.vec3fTemp2.cross(var1.vec3fTemp1, var1.vec3fTemp0);
            var1.vec3fTemp2.dfT();
            var1.vec3fTemp2.dfP();
            var1.vec3fTemp1.cross(var1.vec3fTemp0, var1.vec3fTemp2);
            var1.vec3fTemp1.negate();
            var1.vec3fTemp2.scale(this.size.x);
            var1.vec3fTemp0.scale(this.size.y);
            this.localTransform.setX(var1.vec3fTemp2);
            this.localTransform.setY(var1.vec3fTemp0);
            this.localTransform.setZ(var1.vec3fTemp1);
            this.globalTransform.getTranslation(var1.vec3dTemp0);
            this.localTransform.setTranslation(var1.vec3dTemp0);
        } else if (this.alignment == BillboardAlignment.VIEW_ALIGNED) {
            var1.getCamera().getTransform().getTranslation(var1.vec3dTemp1);
            this.globalTransform.getTranslation(var1.vec3dTemp2);
            var1.vec3dTemp1.sub(var1.vec3dTemp2);
            var1.vec3dTemp1.dfX();
            var1.vec3fTemp1.set(var1.vec3dTemp1);
            var1.getCamera().getTransform().getY(var1.vec3fTemp0);
            var1.vec3fTemp2.cross(var1.vec3fTemp1, var1.vec3fTemp0);
            var1.vec3fTemp2.dfP();
            var1.vec3fTemp0.scale(this.size.y);
            var1.vec3fTemp2.scale(-this.size.x);
            this.localTransform.setX(var1.vec3fTemp2);
            this.localTransform.setY(var1.vec3fTemp0);
            this.localTransform.setZ(var1.vec3fTemp1);
            this.localTransform.setTranslation(this.globalTransform.position);
        }

    }

    public RenderAsset cloneAsset() {
        return new RBillboard(this);
    }

    public BillboardAlignment getAlignment() {
        return this.alignment;
    }

    public void setAlignment(BillboardAlignment var1) {
        this.alignment = var1;
    }

    public Vec3f getAlignmentDirection() {
        return this.alignmentDirection;
    }

    public void setAlignmentDirection(Vec3f var1) {
        this.alignmentDirection.set(var1);
    }

    public OcclusionQuery getOcclusionQuery() {
        return this.occlusionQuery;
    }

    public aLH computeWorldSpaceAABB() {
        float var1 = this.size.x;
        if (this.size.y > var1) {
            var1 = this.size.y;
        }

        this.aabbWorldSpace.g(-var1, -var1, -var1, var1, var1, var1);
        this.aabbWorldSpace.a(this.globalTransform, this.aabbWorldSpace);
        this.addChildrenWorldSpaceAABB();
        this.aabbWSLenght = this.aabbWorldSpace.din().ax(this.aabbWorldSpace.dim());
        return this.aabbWorldSpace;
    }

    public aLH computeLocalSpaceAABB() {
        super.computeLocalSpaceAABB();
        this.localAabb.C((double) (-this.size.x), (double) (-this.size.y), 0.0D);
        this.localAabb.C((double) this.size.x, (double) this.size.y, 0.0D);
        return this.localAabb;
    }

    public RayTraceSceneObjectInfoD rayIntersects(RayTraceSceneObjectInfoD var1, boolean var2, boolean var3) {
        var1.setChanged(false);
        super.rayIntersects(var1, var2, var3);
        if (var1.isIntersected() && var1.isChanged()) {
            return var1;
        } else if (billboard == null) {
            return var1;
        } else {
            Vector3dOperations var4 = new Vector3dOperations();
            Vector3dOperations var5 = new Vector3dOperations();
            var4.sub(var1.getStartPos(), this.globalTransform.position);
            var5.sub(var1.getEndPos(), this.globalTransform.position);
            this.globalTransform.mX.a((Vector3d) var4);
            this.globalTransform.mX.a((Vector3d) var5);
            gu_g var6 = billboard.rayIntersects(var4.getVec3f(), var5.getVec3f());
            if (var6.isIntersected() && (!var2 || (double) var6.vM() < var1.getParamIntersection())) {
                var1.set(var6);
                var1.setChanged(true);
                var1.setIntersectedObject(this);
            }

            return var1;
        }
    }

    public void updateInternalGeometry(SceneObject var1) {
        super.updateInternalGeometry(var1);
        double var2 = this.globalTransform.gk() * (double) this.size.x;
        double var4 = this.globalTransform.gm() * (double) this.size.y;
        this.maxSizeGlobal = var2;
        if (var4 > var2) {
            this.maxSizeGlobal = var4;
        }

        if (this.alignment == BillboardAlignment.USER_ALIGNED) {
            this.localTransform.b(this.globalTransform);
            this.localTransform.B(this.size.x);
            this.localTransform.C(this.size.y);
        }

    }

    public boolean getDisableDepthWrite() {
        return this.disableDepthWrite;
    }

    public void setDisableDepthWrite(boolean var1) {
        this.disableDepthWrite = var1;
    }

    public void validate(SceneLoader var1) {
        super.validate(var1);
        if (this.material.getShader() == null) {
            if (this.disableDepthWrite) {
                this.material.setShader(var1.getDefaultBillboardShaderNoDepth());
            } else {
                this.material.setShader(var1.getDefaultBillboardShader());
            }
        }

    }

    public aLH getTransformedAABB() {
        float var1 = this.size.x;
        if (this.size.y > var1) {
            var1 = this.size.y;
        }

        var1 *= 0.5F;
        BoundingBox var2 = new BoundingBox(-var1, -var1, -var1, var1, var1, var1);
        var2.a(this.getGlobalTransform(), this.aabbWorldSpace);
        return this.aabbWorldSpace;
    }
}
