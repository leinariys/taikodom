package taikodom.render.scene;

//import all.HO;

import all.aah;
import taikodom.render.StepContext;
import taikodom.render.loader.RenderAsset;

//import all.pY;

public class RAnimation extends RenderAsset {
    private boolean initOnce = true;
    private boolean removeOnEnd = false;
    private boolean stopping = false;
    private float animationSpeed = 1.0F;
    private float fadeTime = 0.3F;
    private RModel model = null;
    //  private pY grannyAnimation = null;
    private aah grannyControl = null;
    private boolean disposed = false;

    public RAnimation() {
    }

    /*
       public RAnimation(pY var1) {
          this.grannyAnimation = var1;
       }
    */
    public RAnimation(RAnimation var1) {
        super(var1);
        this.removeOnEnd = var1.removeOnEnd;
        //  this.grannyAnimation = var1.grannyAnimation;
        this.fadeTime = var1.fadeTime;
        this.animationSpeed = var1.animationSpeed;
    }

    public RenderAsset cloneAsset() {
        return new RAnimation(this);
    }

    public float getAnimationSpeed() {
        return this.animationSpeed;
    }

    public void setAnimationSpeed(float var1) {
        this.animationSpeed = var1;
        if (this.grannyControl != null) {
            // HO.endPage(this.grannyControl, this.animationSpeed);
        }

    }

    public aah getControl() {
        return this.grannyControl;
    }

    public float getFadeTime() {
        return this.fadeTime;
    }

    public void setFadeTime(float var1) {
        this.fadeTime = var1;
    }

    /*
       public pY getGrannyAnimation() {
          return this.grannyAnimation;
       }
    */
    public RModel getModel() {
        return this.model;
    }

    public void setModel(RModel var1) {
        this.model = var1;
    }

    /*
       public float getRemainingTimeSeconds() {
          return this.grannyControl != null ? HO.o(this.grannyControl) : 0.0F;
       }
    */
    public boolean isInitOnce() {
        return this.initOnce;
    }

    /*
       public void setGrannyAnimation(pY var1) {
          this.grannyAnimation = var1;
       }
    */
    public void setInitOnce(boolean var1) {
        this.initOnce = var1;
    }

    public boolean isPlaying() {
        return this.grannyControl != null;
    }

    public boolean isRemoveOnEnd() {
        return this.removeOnEnd;
    }

    public void setRemoveOnEnd(boolean var1) {
        this.removeOnEnd = var1;
    }

    public void play() {
        this.play(0);
    }

    public void play(int var1) {
        //  if (this.model != null && this.model.getGrannyModelInstance() != null && this.grannyAnimation != null)
        {
            if (this.grannyControl != null) {
                //  HO.CreateJComponent(this.grannyControl);
            }

            this.initOnce = true;
            this.stopping = false;
            // this.grannyControl = HO.all(this.model.getGameClock(), this.grannyAnimation, this.model.getGrannyModelInstance());
            if (this.fadeTime > 0.0F) {
                //  HO.setGreen(this.grannyControl, this.fadeTime, false);
            }

            // HO.all(this.grannyControl, var1);
            // HO.endPage(this.grannyControl, this.animationSpeed);
        }

    }

    public void reset() {
        this.play();
    }

    public void step(StepContext var1) {
        if (this.grannyControl != null) {
            //  float var2 = HO.o(this.grannyControl);
            //  float var3 = HO.t(this.grannyControl);
            //  if (var2 > 0.0F && (var3 > 0.0F || !this.stopping))
            {
                this.model.setHasActiveAnimation(true);
            } //else
            {
                //  HO.CreateJComponent(this.grannyControl);
                this.grannyControl = null;
                if (this.removeOnEnd && !var1.getEditMode()) {
                    this.dispose();
                }
            }
        } else if (this.removeOnEnd && !var1.getEditMode()) {
            this.dispose();
        }

    }

    private void dispose() {
        this.disposed = true;
    }

    public void stop() {
        if (this.grannyControl != null) {
            this.stopping = true;
            if (this.fadeTime > 0.0F) {
                // HO.setGreen(this.grannyControl, HO.parseStyleSheet(this.grannyControl, this.fadeTime));
            } else {
                //  HO.CreateJComponent(this.grannyControl);
                this.grannyControl = null;
            }
        }

    }

    public boolean isDisposed() {
        return this.disposed;
    }
}
