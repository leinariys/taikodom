package taikodom.render.scene;

import taikodom.render.RenderContext;
import taikodom.render.loader.RenderAsset;

/**
 * Реализация класса указанного в имени тегов из файлов с расширением .pro
 */
public class RPanorama extends SceneObject {
    public RPanorama() {
    }

    public RPanorama(RPanorama var1) {
        super(var1);
    }

    public boolean render(RenderContext var1, boolean var2) {
        if (this.render) {
            var1.getCamera().getTransform().getTranslation(this.transform.position);
            this.updateInternalGeometry((SceneObject) null);
            return super.render(var1, var2);
        } else {
            return false;
        }
    }

    public RenderAsset cloneAsset() {
        return new RPanorama(this);
    }
}
