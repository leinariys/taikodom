package taikodom.render.scene;

import com.hoplon.geometry.Color;
import taikodom.render.NotExported;
import taikodom.render.loader.RenderAsset;
import taikodom.render.loader.SceneLoader;
import taikodom.render.primitives.OcclusionQuery;
import taikodom.render.primitives.RenderAreaInfo;
import taikodom.render.shaders.Shader;
import taikodom.render.textures.Material;

public class RenderObject extends SceneObject {
    protected final Color primitiveColor = new Color(1.0F, 1.0F, 1.0F, 1.0F);
    protected final Color emissiveColor = new Color(1.0F, 1.0F, 1.0F, 1.0F);
    protected final RenderAreaInfo areaInfo = new RenderAreaInfo();
    protected Material material;
    protected boolean selected;
    protected OcclusionQuery occlusionQuery;
    protected long lastFrameRendered;
    protected boolean selectedRecursive;

    public RenderObject() {
    }

    protected RenderObject(RenderObject var1) {
        super(var1);
        this.material = (Material) this.smartClone(var1.material);
        this.primitiveColor.set(var1.primitiveColor);
        this.selected = var1.selected;
        this.rayTraceable = var1.rayTraceable;
        this.lastFrameRendered = var1.lastFrameRendered;
        this.selectedRecursive = var1.selectedRecursive;
    }

    public RenderAsset cloneAsset() {
        return new RenderObject(this);
    }

    public Material getMaterial() {
        return this.material;
    }

    public void setMaterial(Material var1) {
        this.material = var1;
    }

    public Color getPrimitiveColor() {
        return this.primitiveColor;
    }

    public void setPrimitiveColor(Color var1) {
        this.primitiveColor.set(var1);
    }

    public void setPrimitiveColor(float var1, float var2, float var3, float var4) {
        this.primitiveColor.set(var1, var2, var3, var4);
    }

    public boolean isSelected() {
        return this.selected;
    }

    public void setSelected(boolean var1) {
        this.selected = var1;
    }

    public boolean isOcclusionQueryEnabled() {
        return this.occlusionQuery != null;
    }

    public void setOcclusionQueryEnabled(boolean var1) {
        if (var1 && this.occlusionQuery == null) {
            this.occlusionQuery = new OcclusionQuery();
        }

    }

    @NotExported
    public long getLastFrameRendered() {
        return this.lastFrameRendered;
    }

    @NotExported
    public void setLastFrameRendered(long var1) {
        this.lastFrameRendered = var1;
    }

    public boolean isSelectedRecursive() {
        return this.selectedRecursive;
    }

    public void setSelectedRecursive(boolean var1) {
        this.selectedRecursive = var1;
    }

    /**
     * @deprecated
     */
    @Deprecated
    public Shader getShader() {
        return this.material.getShader();
    }

    /**
     * @deprecated
     */
    @Deprecated
    public void setShader(Shader var1) {
        if (this.material != null) {
            this.material.setShader(var1);
        }

    }

    public RenderAreaInfo getRenderAreaInfo() {
        return this.areaInfo;
    }

    public void validate(SceneLoader var1) {
        super.validate(var1);
        if (this.material == null) {
            this.material = var1.getDefaultMaterial();
        }

    }

    public Color getEmissiveColor() {
        return this.emissiveColor;
    }

    public void setEmissiveColor(Color var1) {
        this.emissiveColor.set(this.primitiveColor);
    }
}
