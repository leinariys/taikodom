package taikodom.render.scene;

import taikodom.render.StepContext;
import taikodom.render.loader.RenderAsset;

import java.util.Iterator;
import java.util.Vector;

/**
 * Реализация класса указанного в имени тегов из файлов с расширением .pro
 */
public class SMultiLayerMusic extends SoundObject {
    Vector volumeInterpolators;
    Vector musics;
    int currentLayer;
    float gain;

    public SMultiLayerMusic() {
        this.currentLayer = 0;
        this.gain = 1.0F;
        this.musics = new Vector();
        this.volumeInterpolators = new Vector();
    }

    public SMultiLayerMusic(SMultiLayerMusic var1) {
        this.currentLayer = var1.currentLayer;
        this.gain = var1.gain;
        int var2 = var1.musicCount();

        for (int var3 = 0; var3 < var2; ++var3) {
            this.addMusic((SMusic) var1.musics.get(var3));
        }

    }

    public void releaseReferences() {
        super.releaseReferences();
        this.musics.clear();
    }

    public void dispose() {
        this.stop();
        this.disposed = true;
    }

    public int step(StepContext var1) {
        Iterator var2 = this.volumeInterpolators.iterator();

        while (var2.hasNext()) {
            VolumeInterpolator var3 = (VolumeInterpolator) var2.next();
            boolean var4 = !var3.step(var1.getDeltaTime());
            SMusic var5 = var3.getMusic();
            if (var5 != null) {
                var5.setGain(var3.getValue() * this.gain);
            }

            if (var4) {
                if (var5 != null && var3.getValue() <= 0.0F) {
                    var5.stop();
                }

                var2.remove();
            }
        }

        int var6 = 1;

        for (int var7 = 0; var7 < this.musics.size(); ++var7) {
            var6 *= ((SMusic) this.musics.get(var7)).step(var1);
        }

        return var6;
    }

    public boolean play() {
        if (this.currentLayer < this.musics.size()) {
            ((SMusic) this.musics.get(this.currentLayer)).play();
            ((SMusic) this.musics.get(this.currentLayer)).setGain(this.gain);
            return true;
        } else {
            return false;
        }
    }

    public boolean stop() {
        for (int var1 = 0; var1 < this.musics.size(); ++var1) {
            ((SMusic) this.musics.get(var1)).stop();
        }

        return true;
    }

    public boolean pause() {
        if (this.currentLayer < this.musics.size()) {
            ((SMusic) this.musics.get(this.currentLayer)).pause();
            return true;
        } else {
            return false;
        }
    }

    public boolean fadeOut(float var1) {
        for (int var2 = 0; var2 < this.musics.size(); ++var2) {
            ((SMusic) this.musics.get(var2)).fadeOut(var1);
        }

        return true;
    }

    public boolean fadeIn(float var1) {
        for (int var2 = 0; var2 < this.musics.size(); ++var2) {
            ((SMusic) this.musics.get(var2)).fadeIn(var1);
        }

        return true;
    }

    public void changeLayer(int var1, float var2) {
        if (this.currentLayer != var1 && var1 < this.musics.size()) {
            VolumeInterpolator var3 = new VolumeInterpolator();
            float var4 = this.gain;
            int var5 = 0;
            SMusic var6 = (SMusic) this.musics.get(this.currentLayer);
            if (var6 != null) {
                var4 = var6.getGain();
                var5 = var6.getSamplePositionMs();
            }

            var3.init(var4, 0.0F, var2, var6);
            this.volumeInterpolators.add(var3);
            VolumeInterpolator var7 = new VolumeInterpolator();
            var4 = 0.0F;
            var6 = (SMusic) this.musics.get(var1);
            if (var6 != null) {
                var4 = var6.getGain();
                var6.play();
                var6.setGain(0.0F);
                var6.setSamplePositionMs(var5);
            }

            var7.init(var4, this.gain, var2, var6);
            this.volumeInterpolators.add(var7);
            this.currentLayer = var1;
        }
    }

    public void addMusic(SMusic var1) {
        this.musics.add(var1);
    }

    public int musicCount() {
        return this.musics.size();
    }

    public SMusic getMusic(int var1) {
        return (SMusic) this.musics.get(var1);
    }

    public void setMusic(int var1, SMusic var2) {
        int var3 = this.musics.size();
        if (var3 > var1 && var1 >= 0) {
            this.musics.set(var1, var2);
        } else {
            this.musics.add(var2);
        }

    }

    public float getGain() {
        return this.gain;
    }

    public void setGain(float var1) {
        this.gain = var1;

        for (int var2 = 0; var2 < this.musicCount(); ++var2) {
            ((SMusic) this.musics.get(var2)).setGain(var1);
        }

    }

    public RenderAsset cloneAsset() {
        return this;
    }
}
