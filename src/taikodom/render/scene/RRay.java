package taikodom.render.scene;

import all.Vector3dOperations;
import all.aLH;
import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import taikodom.render.RenderContext;
import taikodom.render.StepContext;
import taikodom.render.enums.BillboardAlignment;

import java.util.ArrayList;

public class RRay extends SceneObject {
    RBillboard billboard;
    float lifeTime;
    float elapsedTime;
    float range;
    private ArrayList billboardList;

    public RRay() {
        this.billboardList = new ArrayList();
    }

    public RRay(RRay var1) {
        super(var1);
        this.billboard = new RBillboard(var1.billboard);
        this.billboardList = new ArrayList();
        this.lifeTime = var1.lifeTime;
        this.range = var1.range;
    }

    public boolean isNeedToStep() {
        return true;
    }

    public aLH buildAABB() {
        this.billboard.computeWorldSpaceAABB();
        return this.billboard.getTransformedAABB();
    }

    public void dispose() {
        this.billboard = null;
    }

    public int step(StepContext var1) {
        for (int var2 = 0; var2 < this.billboardList.size(); ++var2) {
            RayBB var3 = (RayBB) this.billboardList.get(var2);
            if (var3.getElapsedTime() > var3.getLifeTime()) {
                var3.dispose();
                this.billboardList.remove(var2);
            } else {
                var3.decLifeTime(var1.getDeltaTime());
            }
        }

        return 0;
    }

    public void releaseReferences() {
        super.releaseReferences();

        for (int var1 = 0; var1 < this.billboardList.size(); ++var1) {
            RayBB var2 = (RayBB) this.billboardList.get(var1);
            var2.dispose();
        }

        this.billboardList.clear();
        super.releaseReferences();
    }

    public boolean render(RenderContext var1, boolean var2) {
        if (!this.render) {
            return false;
        } else if (!super.render(var1, var2)) {
            return false;
        } else {
            for (int var3 = 0; var3 < this.billboardList.size(); ++var3) {
                RayBB var4 = (RayBB) this.billboardList.get(var3);
                var4.render(var1, var2);
            }

            return true;
        }
    }

    public void setRay(RBillboard var1, float var2) {
        this.billboard = var1;
        this.lifeTime = var2;
    }

    float getLifeTime() {
        return this.lifeTime;
    }

    float getElapsedTime() {
        return this.elapsedTime;
    }

    void decLifeTime(float var1) {
        this.elapsedTime += var1;
        Color var2 = this.billboard.getPrimitiveColor();
        float var3 = 1.0F - this.elapsedTime / this.lifeTime;
        var2.w = var3;
        var2.x = var3;
        var2.y = var3;
        var2.z = var3;
        this.billboard.setPrimitiveColor(var2);
    }

    Color getColor() {
        return this.billboard.getPrimitiveColor();
    }

    void setColor(Color var1) {
        this.billboard.setPrimitiveColor(var1);
    }

    public void trigger(Vector3dOperations var1, Vec3f var2, float var3) {
        if (this.billboard != null) {
            RBillboard var4 = (RBillboard) this.billboard.cloneAsset();
            var4.setPosition(var1);
            var4.setAlignment(BillboardAlignment.AXIS_ALIGNED);
            var4.setAlignmentDirection(var2);
            var4.setSize(this.billboard.getSize().x, var3);
            RayBB var5 = new RayBB(var4, this.lifeTime, var3);
            this.billboardList.add(var5);
            this.aabbWorldSpace.h(var5.buildAABB());
        }

    }

    public RRay cloneAsset() {
        return new RRay(this);
    }
}
