package taikodom.render.scene.custom;

import java.io.Serializable;

public class WeldingInfo implements Serializable {

    private int[] info;
    private boolean[] weldTangents;
    private int maxDuplicates;
    private int uniqueCount;

    public WeldingInfo(int var1, int var2, int[] var3, boolean[] var4) {
        this.uniqueCount = var1;
        this.maxDuplicates = var2;
        this.info = var3;
        this.weldTangents = var4;
    }

    public int[] getInfo() {
        return this.info;
    }

    public int getMaxDuplicates() {
        return this.maxDuplicates;
    }

    public int getUniqueCount() {
        return this.uniqueCount;
    }

    public boolean[] getWeldTangents() {
        return this.weldTangents;
    }
}
