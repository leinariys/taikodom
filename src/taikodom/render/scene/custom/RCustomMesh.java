package taikodom.render.scene.custom;

//import all.CS;

import all.LogPrinter;
import all.aJF;
import all.aQKa;
import com.hoplon.geometry.Vec3f;
import taikodom.render.data.ExpandableIndexData;
import taikodom.render.data.IndexData;
import taikodom.render.data.VertexData;
import taikodom.render.primitives.Mesh;
import taikodom.render.scene.RMesh;

import java.io.*;
import java.util.*;
import java.util.Map.Entry;

public class RCustomMesh extends RMesh {
    private static final float TANGENT_THRESHOLD = 0.8F;
    private static final float NORMALS_THRESHOLD = 0.8F;
    private static final boolean ORTHONORMALIZE_TANGENT_SPACE = true;
    private static final LogPrinter logger = LogPrinter.setClass(RCustomMesh.class);
    private static final float VERTEX_SQUARED_DISTANCE_THRESHOLD = 1.0E-7F;
    private static final boolean CHECK_NON_EDGE_VERTS = false;
    private final aQKa v2tmp1 = new aQKa();
    private final aQKa v2tmp2 = new aQKa();
    private final Vec3f v3tmp1 = new Vec3f();
    private final Vec3f v3tmp2 = new Vec3f();
    private final Vec3f v3tmp3 = new Vec3f();
    private final aJF v4tmp1 = new aJF();
    private final aJF v4tmp2 = new aJF();
    private final Comparator vertexComparator = new Comparator() {
        @Override
        public int compare(Object o1, Object o2) {
            return d((Vec3f) o1, (Vec3f) o2);
        }

        public int d(Vec3f var1, Vec3f var2) {
            if (var1.x + 1.0E-7F < var2.x) {
                return -1;
            } else if (var1.x - 1.0E-7F > var2.x) {
                return 1;
            } else if (var1.y + 1.0E-7F < var2.y) {
                return -1;
            } else if (var1.y - 1.0E-7F > var2.y) {
                return 1;
            } else if (var1.z + 1.0E-7F < var2.z) {
                return -1;
            } else {
                return var1.z - 1.0E-7F > var2.z ? 1 : 0;
            }
        }
    };
    private final List parts = new ArrayList();
    private int indexCount = 0;
    private WeldingInfo weldingInfo;
    private boolean[] usedVertexes;

    public void addPart(Mesh var1, boolean var2) {
        //    CS var3 = var1.getGrannyMesh().bqM().wa(var1.getGrannyMaterialIndex()).cDE();
        String var4 = "unnamed";
 /*     if (var3 != null) {
         var4 = var3.getTegName();
      }
*/
        RCustomMesh.a var5 = new RCustomMesh.a(var4, var1, var2);
        this.parts.add(var5);
        this.indexCount += var1.getIndexes().size();
        if (this.mesh == null) {
            this.setMesh(new Mesh(var1));
        }

    }

    public void assembleMesh() {
        this.buildIndexData();
        this.weldParts(true);
    }

    public void buildIndexData() {
        ExpandableIndexData var1 = null;
        if (this.mesh == null) {
            logger.error("trying to_q build an index data for all RCustomMesh without any mesh!!");
        } else {
            if (!(this.mesh.getIndexes() instanceof ExpandableIndexData)) {
                var1 = new ExpandableIndexData(this.indexCount);
                var1.setUseVbo(false);
            } else {
                var1 = (ExpandableIndexData) this.mesh.getIndexes();
            }

            int var2 = 0;
            var1.clear();
            Iterator var4 = this.parts.iterator();

            while (true) {
                RCustomMesh.a var3;
                do {
                    if (!var4.hasNext()) {
                        var1.compact();
                        this.mesh.setIndexes(var1);
                        this.mesh.computeLocalAabb();
                        return;
                    }

                    var3 = (RCustomMesh.a) var4.next();
                } while (!var3.isVisible());

                IndexData var5 = var3.getMesh().getIndexes();

                for (int var6 = 0; var6 < var5.size(); ++var6) {
                    var1.setIndex(var2, var5.getIndex(var6));
                    ++var2;
                }
            }
        }
    }

    public void generateWeldingInfo() {
        System.nanoTime();
        TreeMap var1 = new TreeMap(this.vertexComparator);
        VertexData var2 = this.mesh.getVertexData();
        int var3 = var2.size();
        int var4 = 0;

        for (int var5 = 0; var5 < var3; ++var5) {
            var2.getPosition(var5, this.v3tmp1);
            Object var6 = this.getDuplicate(var1, this.v3tmp1);
            if (var6 == null) {
                var6 = new ArrayList();
                var1.put(new Vec3f(this.v3tmp1), var6);
            }

            if (!((List) var6).contains(var5)) {
                ((List) var6).add(var5);
                var4 = Math.max(var4, ((List) var6).size());
            }
        }

        TreeMap var26 = new TreeMap(this.vertexComparator);
        int[] var27 = new int[var1.size()];

        int var7;
        Vec3f var12;
        for (var7 = 0; var7 < this.parts.size(); ++var7) {
            IndexData var8 = ((RCustomMesh.a) this.parts.get(var7)).getMesh().getIndexes();
            TreeSet var9 = new TreeSet(this.vertexComparator);

            for (int var10 = 0; var10 < var8.size(); ++var10) {
                int var11 = var8.getIndex(var10);
                var2.getPosition(var11, this.v3tmp1);
                if (!var9.contains(this.v3tmp1)) {
                    var12 = new Vec3f(this.v3tmp1);
                    var9.add(var12);
                    Integer var13 = (Integer) var26.get(var12);
                    if (var13 == null) {
                        var13 = Integer.valueOf(0);
                    }

                    var26.put(var12, var13.intValue() + 1);
                }
            }
        }

        var7 = 0;

        Integer var32;
        for (Iterator var30 = var1.keySet().iterator(); var30.hasNext(); var27[var7++] = var32.intValue()) {
            Vec3f var28 = (Vec3f) var30.next();
            var32 = (Integer) var26.get(var28);
            if (var32 == null) {
                var32 = Integer.valueOf(0);
            }
        }

        var7 = 0;
        int var29 = 0;
        int var31 = 0;
        ArrayList var34 = new ArrayList();
        ArrayList var33 = new ArrayList();
        var12 = new Vec3f();
        Vec3f var35 = new Vec3f();
        Vec3f var14 = new Vec3f();
        Vec3f var15 = new Vec3f();
        aJF var16 = new aJF();
        aJF var17 = new aJF();

        int var24;
        for (Iterator var19 = var1.entrySet().iterator(); var19.hasNext(); ++var7) {
            Entry var18 = (Entry) var19.next();
            List var20 = (List) var18.getValue();
            int var21 = var20.size();
            if (var21 > 1) {
                boolean var22 = false;
                if (var27[var7] > 1) {
                    var2.getNormal(((Integer) var20.get(0)).intValue(), var12);
                    var2.getNormal(((Integer) var20.get(1)).intValue(), var35);
                    if (var12.dot(var35) > 0.8F) {
                        var34.add(var20);
                        var29 = Math.max(var29, var21);
                        var31 += 1 + var21;
                        var22 = true;
                    }
                }

                if (var22) {
                    boolean[] var23 = new boolean[var21];
                    var23[0] = true;
                    var33.add(var23);

                    for (var24 = 0; var24 < var21 - 1; ++var24) {
                        var2.getTangents(((Integer) var20.get(var24)).intValue(), var16);
                        var14.set(var16.x, var16.y, var16.z);

                        for (int var25 = var24 + 1; var25 < var21; ++var25) {
                            if (!var23[var25]) {
                                var2.getTangents(((Integer) var20.get(var25)).intValue(), var17);
                                var15.set(var17.x, var17.y, var17.z);
                                if (var14.dot(var15) > 0.8F) {
                                    var23[var25] = true;
                                    var25 = var21;
                                }
                            }
                        }
                    }
                }
            }
        }

        int[] var36 = new int[var31];
        boolean[] var37 = new boolean[var31];
        var7 = 0;

        for (int var38 = 0; var38 < var34.size(); ++var38) {
            List var39 = (List) var34.get(var38);
            boolean[] var40 = (boolean[]) var33.get(var38);
            int var41 = var39.size();
            var36[var7] = var41;
            var37[var7] = false;
            ++var7;

            for (var24 = 0; var24 < var41; ++var24) {
                var36[var7] = ((Integer) var39.get(var24)).intValue();
                var37[var7] = var40[var24];
                ++var7;
            }
        }

        this.weldingInfo = new WeldingInfo(var34.size(), var29, var36, var37);
    }

    public void generateWeldingInfo(File var1, File var2) {
        String var3 = var2.getParent() + File.separator + var2.getName().replace('.', '_') + ".weld";
        File var4 = new File(var3);
        if (var4.exists()) {
            if (var4.lastModified() == var1.lastModified()) {
                try {
                    FileInputStream var5 = new FileInputStream(var4);
                    ObjectInputStream var6 = new ObjectInputStream(var5);
                    Object var7 = var6.readObject();
                    if (var7 instanceof WeldingInfo) {
                        this.weldingInfo = (WeldingInfo) var7;
                        var5.close();
                        return;
                    }

                    var5.close();
                } catch (IOException var9) {
                    ;
                } catch (ClassNotFoundException var10) {
                    System.err.println("Possible serialization error in RCustomMesh.");
                }
            }

            var4.delete();
        }

        this.generateWeldingInfo();

        try {
            var4.createNewFile();
            FileOutputStream var11 = new FileOutputStream(var4);
            ObjectOutputStream var12 = new ObjectOutputStream(var11);
            var12.writeObject(this.weldingInfo);
            var11.close();
            var4.setLastModified(var1.lastModified());
        } catch (IOException var8) {
            System.err.println("Could not cache welding information in RCustomMesh.");
        }

    }

    private List getDuplicate(Map var1, Vec3f var2) {
        return (List) var1.get(var2);
    }

    public int getPartCount() {
        return this.parts.size();
    }

    public WeldingInfo getWeldingInfo() {
        return this.weldingInfo;
    }

    public void setWeldingInfo(WeldingInfo var1) {
        this.weldingInfo = var1;
    }

    public void hidePart(int var1) {
        ((RCustomMesh.a) this.parts.get(var1)).setVisible(false);
    }

    public void hidePart(String var1) {
        Iterator var3 = this.parts.iterator();

        while (var3.hasNext()) {
            RCustomMesh.a var2 = (RCustomMesh.a) var3.next();
            if (var2.getName().equals(var1)) {
                var2.setVisible(false);
                return;
            }
        }

        System.err.println("Part \"" + var1 + "\" not found!");
    }

    public void showPart(int var1) {
        ((RCustomMesh.a) this.parts.get(var1)).setVisible(true);
    }

    public void showPart(String var1) {
        Iterator var3 = this.parts.iterator();

        while (var3.hasNext()) {
            RCustomMesh.a var2 = (RCustomMesh.a) var3.next();
            if (var2.getName().equals(var1)) {
                var2.setVisible(true);
                return;
            }
        }

        System.err.println("Part \"" + var1 + "\" not found!");
    }

    public void weldParts(boolean var1) {
        if (this.mesh == null) {
            logger.error("trying do weld all RCustomMesh without any mesh!!");
        } else {
            if (this.weldingInfo == null) {
                this.generateWeldingInfo();
            }

            VertexData var2 = this.mesh.getVertexData();
            VertexData var3 = ((RCustomMesh.a) this.parts.get(0)).getMesh().getVertexData();
            IndexData var4 = this.mesh.getIndexes();
            int var5;
            if (this.usedVertexes == null || this.usedVertexes.length != var2.size()) {
                this.usedVertexes = new boolean[var2.size()];

                for (var5 = 0; var5 < var4.size(); ++var5) {
                    this.usedVertexes[var4.getIndex(var5)] = true;
                }
            }

            var5 = 0;
            int[] var6 = this.weldingInfo.getInfo();
            boolean[] var7 = this.weldingInfo.getWeldTangents();
            Vec3f var10 = new Vec3f();
            Vec3f var11 = new Vec3f();
            Vec3f var12 = new Vec3f();
            Vec3f var13 = new Vec3f();
            aJF var14 = new aJF();

            for (int var15 = 0; var15 < this.weldingInfo.getUniqueCount(); ++var15) {
                boolean var8 = false;
                int var9 = var6[var5];
                ++var5;

                for (int var16 = 0; var16 < var9; ++var16) {
                    if (this.usedVertexes[var6[var5 + var16]]) {
                        var8 = true;
                        break;
                    }
                }

                if (!var8) {
                    var5 += var9;
                } else {
                    VertexData var18;
                    if (var1) {
                        var18 = var3;
                    } else {
                        var18 = var2;
                    }

                    var18.getNormal(var6[var5], this.v3tmp1);
                    var18.getTangents(var6[var5], var14);
                    this.v4tmp1.set(var14);

                    int var17;
                    for (var17 = 1; var17 < var9; ++var17) {
                        var18.getNormal(var6[var5 + var17], this.v3tmp2);
                        this.v3tmp1.add(this.v3tmp2);
                        if (var7[var5 + var17]) {
                            var18.getTangents(var6[var5 + var17], this.v4tmp2);
                            this.v4tmp2.w = 0.0F;
                            this.v4tmp1.add(this.v4tmp2);
                        }
                    }

                    this.v3tmp1.cox();
                    this.v3tmp2.set(this.v4tmp1.x, this.v4tmp1.y, this.v4tmp1.z);
                    this.v3tmp2.cox();
                    this.v4tmp1.x = this.v3tmp2.x;
                    this.v4tmp1.y = this.v3tmp2.y;
                    this.v4tmp1.z = this.v3tmp2.z;
                    var10.set(this.v3tmp2);
                    var11.set(this.v3tmp1);

                    for (var17 = 0; var17 < var9; ++var17) {
                        var2.setNormal(var6[var5], var11.x, var11.y, var11.z);
                        var18.getTangents(var6[var5], var14);
                        if (var7[var5]) {
                            var12.cross(var11, var10);
                            var12.normalize();
                            var13.cross(var12, var11);
                            var2.setTangents(var6[var5], var13.x, var13.y, var13.z, var14.w);
                        } else {
                            var13.set(var14.x, var14.y, var14.z);
                            var12.cross(var11, var13);
                            var12.normalize();
                            var13.cross(var12, var11);
                            var2.setTangents(var6[var5], var13.x, var13.y, var13.z, var14.w);
                        }

                        ++var5;
                    }
                }
            }

        }
    }

    public void releaseReferences() {
        this.parts.clear();
        this.indexCount = 0;
        this.usedVertexes = null;
        this.weldingInfo = null;
        this.mesh = null;
        super.releaseReferences();
    }

    private class a {
        private Mesh mesh;
        private String name;
        private boolean visible;

        public a(String var2, Mesh var3, boolean var4) {
            this.name = var2;
            this.mesh = var3;
            this.visible = var4;
        }

        public Mesh getMesh() {
            return this.mesh;
        }

        public String getName() {
            return this.name;
        }

        public boolean isVisible() {
            return this.visible;
        }

        public void setVisible(boolean var1) {
            this.visible = var1;
        }
    }
}
