package taikodom.render.scene;

import all.aLH;
import taikodom.render.RenderContext;
import taikodom.render.loader.RenderAsset;
import taikodom.render.primitives.OcclusionQuery;

/**
 * Реализация класса указанного в имени тегов из файлов с расширением .pro
 */
public class RNuclearLight extends RenderObject {
    protected RenderObject defaultObject;
    protected RBillboard nuclearBillboard;
    protected RBillboard objectBillboard;
    protected float occlusionRatio;

    public RNuclearLight() {
        this.occlusionRatio = 1.0F;
    }

    protected RNuclearLight(RNuclearLight var1) {
        super(var1);
        this.occlusionRatio = var1.occlusionRatio;
        if (var1.defaultObject != null) {
            this.defaultObject = (RenderObject) this.smartClone(var1.defaultObject);
        }

        if (var1.nuclearBillboard != null) {
            this.nuclearBillboard = (RBillboard) this.smartClone(var1.nuclearBillboard);
        }

        if (var1.objectBillboard != null) {
            this.objectBillboard = (RBillboard) this.smartClone(var1.objectBillboard);
        }

    }

    public RenderObject getDefaultObject() {
        return this.defaultObject;
    }

    public void setDefaultObject(RenderObject var1) {
        this.defaultObject = var1;
    }

    public RBillboard getNuclearBillboard() {
        return this.nuclearBillboard;
    }

    public void setNuclearBillboard(RBillboard var1) {
        this.nuclearBillboard = var1;
    }

    public RBillboard getObjectBillboard() {
        return this.objectBillboard;
    }

    public void setObjectBillboard(RBillboard var1) {
        this.objectBillboard = var1;
    }

    public boolean render(RenderContext var1, boolean var2) {
        if (this.render) {
            if (!super.render(var1, var2)) {
                return false;
            } else if (this.objectBillboard == null) {
                return true;
            } else {
                this.objectBillboard.render(var1, var2);
                if (this.defaultObject != null) {
                    this.defaultObject.render(var1, var2);
                }

                float var3 = 0.0F;
                if (this.nuclearBillboard != null && var1.getSceneViewQuality().getEnvFxQuality() <= 1) {
                    OcclusionQuery var4 = this.objectBillboard.getOcclusionQuery();
                    if (var4 == null) {
                        this.objectBillboard.setOcclusionQueryEnabled(true);
                    } else {
                        float var5 = this.objectBillboard.getSize().length();
                        var1.vec3dTemp0.sub(var1.getCamera().getPosition(), this.objectBillboard.getGlobalTransform().position);
                        float var6 = (float) var1.vec3dTemp0.length();
                        float var7 = var6 * 2.0F * var1.getCamera().getTanHalfFovY();
                        float var8 = (float) var1.getDc().getScreenHeight() / var7;
                        float var9 = var5 * var8 / 1.414F;
                        var3 = var4.getResult(var9 * var9 * this.occlusionRatio, var1.getDc().getGL());
                        var1.vec3dTemp0.normalize();
                        var1.vec3fTemp0.set(var1.vec3dTemp0);
                        var1.getCamera().getTransform().getZ(var1.vec3fTemp1);
                        var1.vec3dTemp1.negate();
                        float var10 = var1.vec3fTemp0.dot(var1.vec3fTemp1);
                        float var11 = (float) Math.sin((double) (var1.getCamera().getFovY() / 2.0F) * 3.141592653589793D / 180.0D);
                        float var12 = var10 - var11;
                        var12 /= 1.0F - var11;
                        var3 *= var12;
                        if (var3 < 0.0F) {
                            var3 = 0.0F;
                        } else if (var3 > 1.0F) {
                            var3 = 1.0F;
                        }

                        this.nuclearBillboard.setPrimitiveColor(var3, var3, var3, 1.0F);
                        this.nuclearBillboard.render(var1, var2);
                    }
                }

                var1.getCurrentScene().addColorIntensity(var3);
                return true;
            }
        } else {
            return false;
        }
    }

    public RenderAsset cloneAsset() {
        return new RNuclearLight(this);
    }

    public float getOcclusionRatio() {
        return this.occlusionRatio;
    }

    public void setOcclusionRatio(float var1) {
        this.occlusionRatio = var1;
    }

    public aLH computeWorldSpaceAABB() {
        this.aabbWorldSpace.reset();
        if (this.defaultObject != null) {
            this.defaultObject.updateInternalGeometry(this);
            this.aabbWorldSpace.h(this.defaultObject.getAabbWorldSpace());
        }

        if (this.objectBillboard != null) {
            this.objectBillboard.updateInternalGeometry(this);
            this.aabbWorldSpace.h(this.objectBillboard.getAabbWorldSpace());
        }

        if (this.nuclearBillboard != null) {
            this.nuclearBillboard.updateInternalGeometry(this);
            this.aabbWorldSpace.h(this.nuclearBillboard.getAabbWorldSpace());
        }

        this.addChildrenWorldSpaceAABB();
        this.aabbWSLenght = this.aabbWorldSpace.din().ax(this.aabbWorldSpace.dim());
        return this.aabbWorldSpace;
    }
}
