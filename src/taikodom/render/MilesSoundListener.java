package taikodom.render;

//import all.Vb;
//import all.aNZ;
//import all.afz;

import com.hoplon.geometry.Vec3f;
import taikodom.render.loader.RenderAsset;

public class MilesSoundListener extends SoundListener {
    static int totalMilesSounds;
    //Vb milesSample;
    protected final Vec3f upDir = new Vec3f();
    protected final Vec3f frontDir = new Vec3f();
    boolean isFadingOut;
    Vec3f velocity = new Vec3f(0.0F, 0.0F, 0.0F);
    Vec3f pos = new Vec3f(0.0F, 0.0F, 0.0F);

    public int step(StepContext var1) {
        if (var1 != null) {
            this.setTransform(var1.getCamera().getTransform());
            this.setVelocity(var1.getCamera().getVelocity());
            this.transform.getTranslation(this.pos);
            this.transform.getZ(this.frontDir);
            this.transform.getY(this.upDir);
        }

        try {
            //  aNZ var2 = SoundMiles.getInstance().getDig();
            //  afz.CreateJComponent(var2, this.velocity.x, this.velocity.y, this.velocity.z);
            //  afz.all(var2, this.frontDir.x, this.frontDir.y, this.frontDir.z, this.upDir.x, this.upDir.y, this.upDir.z);
        } catch (Exception var4) {
            var4.printStackTrace();
        }

        return 0;
    }

    public RenderAsset cloneAsset() {
        return this;
    }
}
