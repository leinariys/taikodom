package taikodom.render;

import taikodom.render.textures.ChannelInfo;

import java.util.ArrayList;
import java.util.List;

public class PrimitiveDrawingState {
    public static final int MAX_USED_GL_CHANNELS = 8;
    public static final int MAX_TEX_COORD_SETS = 8;
    public static final int MAX_MATERIAL_TEX = 16;
    public final List states = new ArrayList();
    public boolean useNormalArray = false;
    public boolean useColorArray = false;
    public boolean useTangentArray = false;
    public int tangentChannel = 1;

    public void setStates(List var1) {
        this.states.clear();
        this.states.addAll(var1);
    }

    public boolean isUseNormalArray() {
        return this.useNormalArray;
    }

    public void setUseNormalArray(boolean var1) {
        this.useNormalArray = var1;
    }

    public boolean isUseColorArray() {
        return this.useColorArray;
    }

    public void setUseColorArray(boolean var1) {
        this.useColorArray = var1;
    }

    public boolean isUseTangentArray() {
        return this.useTangentArray;
    }

    public void setUseTangentArray(boolean var1) {
        this.useTangentArray = var1;
    }

    public int getTangentChannel() {
        return this.tangentChannel;
    }

    public void setTangentChannel(int var1) {
        this.tangentChannel = var1;
    }

    public int getNumTextures() {
        return this.states.size();
    }

    public int channelMapping(int var1) {
        return ((ChannelInfo) this.states.get(var1)).getChannelMapping();
    }

    public int texCoordsSets(int var1) {
        return ((ChannelInfo) this.states.get(var1)).getTexCoord();
    }

    public void clear() {
        this.states.clear();
        this.useNormalArray = false;
        this.useColorArray = false;
        this.useTangentArray = false;
    }
}
