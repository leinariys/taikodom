package taikodom.render;

import taikodom.render.loader.RenderAsset;

import java.nio.Buffer;

public abstract class SoundBuffer extends RenderAsset {
    protected int length = 0;
    protected Buffer data = null;
    protected int sampleRate = 0;
    protected int bitDepth = 0;
    protected int channels = 0;
    protected long dataLenght = 0L;
    protected float timeLenght = 1000000.0F;

    public void releaseReferences() {
    }

    public void forceSyncLoading() {
    }

    boolean isValid() {
        return false;
    }

    float getTimeLength() {
        return 0.0F;
    }

    long getSampleCount() {
        return 0L;
    }

    int getSampleRate() {
        return 0;
    }

    int getChannels() {
        return 0;
    }

    int getBitDepth() {
        return 0;
    }

    int getBufferId() {
        return 0;
    }

    public abstract Buffer getData();

    public abstract boolean setData(Buffer var1, int var2);

    public abstract String getFileName();

    public abstract SoundBuffer cloneAsset();
}
