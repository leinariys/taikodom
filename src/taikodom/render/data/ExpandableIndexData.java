package taikodom.render.data;

import com.sun.opengl.util.BufferUtil;

import java.nio.IntBuffer;

public class ExpandableIndexData extends IntIndexData {
    private int used;

    public ExpandableIndexData(int var1) {
        super(var1);
    }

    public ExpandableIndexData(ExpandableIndexData var1) {
        super(var1);
        this.used = var1.used;
    }

    public void setIndex(int var1, int var2) {
        this.ensure(var1);
        this.data.put(var1, var2);
    }

    public int size() {
        return this.used;
    }

    public void clear() {
        this.data.clear();
        this.used = 0;
    }

    public void ensure(int var1) {
        if (this.size <= var1) {
            int var2 = Math.max(this.size * 3 / 2, var1) + 1;
            IntBuffer var3 = BufferUtil.newIntBuffer(var2);
            this.data.position(0);
            this.data.limit(this.used);
            var3.put(this.data);
            var3.flip();
            var3.position(0);
            var3.limit(var2);
            this.data = var3;
            this.size = var2;
            this.used = var1 + 1;
        } else if (this.used <= var1) {
            this.used = var1 + 1;
        }

    }

    public void compact() {
        IntBuffer var1 = BufferUtil.newIntBuffer(this.used);
        this.data.position(0);
        this.data.limit(this.used);
        var1.put(this.data);
        var1.position(0);
        var1.limit(this.used);
        this.data = var1;
        this.size = this.used;
    }

    public void setSize(int var1) {
        this.used = var1;
    }

    public IndexData duplicate() {
        return new ExpandableIndexData(this);
    }
}
