package taikodom.render.data;

import all.aJF;
import com.hoplon.geometry.Vec3f;

public class Vec4fData extends Data {
    public Vec4fData(int var1) {
        super(var1 * 4);
    }

    public void set(int var1, float var2, float var3, float var4, float var5) {
        int var6 = var1 * 4;
        this.data.put(var6, var2);
        this.data.put(var6 + 1, var3);
        this.data.put(var6 + 2, var4);
        this.data.put(var6 + 3, var5);
    }

    public void set(int var1, aJF var2) {
        int var3 = var1 * 4;
        this.data.put(var3, var2.x);
        this.data.put(var3 + 1, var2.y);
        this.data.put(var3 + 2, var2.z);
        this.data.put(var3 + 3, var2.w);
    }

    public void set(int var1, float var2, float var3, float var4) {
        int var5 = var1 * 4;
        this.data.put(var5, var2);
        this.data.put(var5 + 1, var3);
        this.data.put(var5 + 2, var4);
    }

    public void setW(int var1, float var2) {
        this.data.put(var1 * 4 + 3, var2);
    }

    public void get(int var1, aJF var2) {
        int var3 = var1 * 4;
        var2.x = this.data.get(var3);
        var2.y = this.data.get(var3 + 1);
        var2.z = this.data.get(var3 + 2);
        var2.w = this.data.get(var3 + 3);
    }

    public void getVec3(int var1, Vec3f var2) {
        int var3 = var1 * 4;
        var2.x = this.data.get(var3);
        var2.y = this.data.get(var3 + 1);
        var2.z = this.data.get(var3 + 2);
    }

    public String toString() {
        StringBuilder var1 = new StringBuilder();

        for (int var2 = 0; var2 < this.data.capacity(); var2 += 4) {
            var1.append("(");
            var1.append(this.data.get(var2)).append(", ");
            var1.append(this.data.get(var2 + 1)).append(", ");
            var1.append(this.data.get(var2 + 2)).append(", ");
            var1.append(this.data.get(var2 + 3));
            var1.append("), ");
        }

        return var1.toString();
    }

    public Vec4fData duplicate() {
        this.data.clear();
        Vec4fData var1 = new Vec4fData(this.data.capacity() / 4);
        var1.buffer().put(this.data);
        var1.buffer().clear();
        this.data.clear();
        return var1;
    }
}
