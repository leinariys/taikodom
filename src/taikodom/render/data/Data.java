package taikodom.render.data;

import com.sun.opengl.util.BufferUtil;

import java.nio.FloatBuffer;

public class Data {
    protected final FloatBuffer data;

    public Data(int var1) {
        this.data = BufferUtil.newFloatBuffer(var1);
    }

    public Data(FloatBuffer var1) {
        this.data = var1;
    }

    public final FloatBuffer buffer() {
        return this.data;
    }

    public int byteSize() {
        return this.data.capacity() * 4;
    }

    public Data duplicate() {
        this.data.clear();
        Data var1 = new Data(this.data.capacity());
        var1.buffer().put(this.data);
        var1.buffer().clear();
        this.data.clear();
        return var1;
    }
}
