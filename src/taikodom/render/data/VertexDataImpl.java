package taikodom.render.data;

import all.aJF;
import all.aQKa;
import com.hoplon.geometry.Vec3f;
import taikodom.render.DrawContext;
import taikodom.render.PrimitiveDrawingState;

import javax.media.opengl.GL;
import java.nio.Buffer;

/**
 *
 */
public class VertexDataImpl extends VertexData {
    private final VertexLayout layout;
    protected Vec3fData vertices;
    protected Vec3fData normals;
    protected Vec4fData tangents;
    protected Vec4fData colors;
    protected Vec2fData[] texCoords;
    protected int vertexCount;
    protected int verticesOffset;
    protected int normalsOffset;
    protected int tangentsOffset;
    protected int colorsOffset;
    protected int texCoordsOffset;
    private int[] texCoordsOffsets;
    private boolean vboCreated;
    private int VBO;
    private boolean useVBO = false;
    private int totalAllocatedBytes;

    public VertexDataImpl(VertexLayout var1, int var2) {
        this.layout = var1;
        this.vertexCount = var2;
        int var3 = 0;
        if (var1.isHasPosition()) {
            this.vertices = new Vec3fData(var2);
            var3 += this.vertices.buffer().capacity() * 4;
        }

        if (var1.isHasTangents()) {
            this.tangents = new Vec4fData(var2);
            var3 += this.tangents.buffer().capacity() * 4;
        }

        if (var1.hasNormals()) {
            this.normals = new Vec3fData(var2);
            var3 += this.normals.buffer().capacity() * 4;
        }

        if (var1.isHasColor()) {
            this.colors = new Vec4fData(var2);
            var3 += this.colors.buffer().capacity() * 4;
        }

        this.texCoords = new Vec2fData[var1.texCoordCount()];

        for (int var4 = 0; var4 < var1.texCoordCount(); ++var4) {
            this.texCoords[var4] = new Vec2fData(var2);
            var3 += this.texCoords[var4].buffer().capacity() * 4;
        }

        this.totalAllocatedBytes = var3;
    }

    public VertexDataImpl(VertexDataImpl var1) {
        this.layout = var1.getLayout();
        this.vertexCount = var1.size();
        int var2 = 0;
        if (this.layout.isHasPosition()) {
            this.vertices = var1.getVertices().duplicate();
            var2 += this.vertices.buffer().capacity() * 4;
        }

        if (this.layout.isHasTangents()) {
            this.tangents = var1.getTangents().duplicate();
            var2 += this.tangents.buffer().capacity() * 4;
        }

        if (this.layout.hasNormals()) {
            this.normals = var1.getNormals().duplicate();
            var2 += this.normals.buffer().capacity() * 4;
        }

        if (this.layout.isHasColor()) {
            this.colors = var1.getColors().duplicate();
            var2 += this.colors.buffer().capacity() * 4;
        }

        this.texCoords = new Vec2fData[this.layout.texCoordCount()];

        for (int var3 = 0; var3 < this.layout.texCoordCount(); ++var3) {
            this.texCoords[var3] = var1.getTexCoords()[var3].duplicate();
            var2 += this.texCoords[var3].buffer().capacity() * 4;
        }

        this.totalAllocatedBytes = var2;
    }

    public void bind(DrawContext var1) {/**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
     * седержит методы для рисования примитивов (линии треугольники)*/
        GL var2 = var1.getGL();
        PrimitiveDrawingState var3 = var1.primitiveState;
        if (!var1.isUseVbo()) {
            this.useVBO = false;
        }

        int var4;
        int var5;
        int var6;
        if (this.useVBO) {
            if (!this.vboCreated) {
                this.buildVBO(var1);
            }

            var2.glBindBufferARB(34962, this.VBO);
            if (this.layout.positionOffset() >= 0) {
                if (this.verticesOffset == -1) {
                    throw new RuntimeException("This mesh doesnt have position");
                }

                var2.glVertexPointer(3, 5126, 0, (long) this.verticesOffset);
            }

            if (var3.isUseNormalArray()) {
                if (this.normalsOffset == -1) {
                    throw new RuntimeException("This mesh doesnt have normals");
                }

                var2.glNormalPointer(5126, 0, (long) this.normalsOffset);
            }

            if (var3.isUseColorArray()) {
                if (this.colorsOffset == -1) {
                    throw new RuntimeException("This mesh doesnt have colors");
                }

                var2.glColorPointer(this.layout.colorSize(), 5126, 0, (long) this.colorsOffset);
            }

            if (var3.isUseTangentArray()) {
                if (this.tangentsOffset == -1) {
                    throw new RuntimeException("This mesh doesnt have tangents");
                }

                var2.glVertexAttribPointer(var3.getTangentChannel(), 4, 5126, false, 0, (long) this.tangentsOffset);
            }

            if (this.layout.texCoordCount() > 0) {
                for (var4 = 0; var4 < var3.getNumTextures(); ++var4) {
                    var5 = var3.channelMapping(var4);
                    var6 = var3.texCoordsSets(var5);
                    var2.glClientActiveTexture('蓀' + var5);
                    if (this.layout.texCoordCount() > var6 && this.texCoordsOffsets[var6] != -1) {
                        var2.glTexCoordPointer(2, 5126, 0, (long) this.texCoordsOffsets[var6]);
                    } else if (this.texCoordsOffsets[0] != -1) {
                        var2.glTexCoordPointer(2, 5126, 0, (long) this.texCoordsOffsets[0]);
                    }
                }

                var2.glClientActiveTexture(33984);
            }
        } else {
            if (this.vertices == null) {
                throw new RuntimeException("This mesh doesnt have position");
            }

            var2.glVertexPointer(3, 5126, 0, this.vertices.buffer());
            if (var3.isUseNormalArray()) {
                if (this.normals == null) {
                    throw new RuntimeException("This mesh doesnt have normals");
                }

                var2.glNormalPointer(5126, 0, this.normals.buffer());
            }

            if (var3.isUseColorArray()) {
                if (this.colors == null) {
                    throw new RuntimeException("This mesh doesnt have colors");
                }

                var2.glColorPointer(4, 5126, 0, this.colors.buffer());
            }

            if (var3.isUseTangentArray()) {
                if (this.tangents == null) {
                    throw new RuntimeException("This mesh doesnt have tangents");
                }

                var2.glVertexAttribPointer(var3.getTangentChannel(), 4, 5126, false, 0, this.tangents.buffer());
            }

            if (this.texCoords != null && this.texCoords.length > 0) {
                for (var4 = 0; var4 < var3.getNumTextures(); ++var4) {
                    var5 = var3.channelMapping(var4);
                    var6 = var3.texCoordsSets(var5);
                    var2.glClientActiveTexture('蓀' + var5);
                    if (this.layout.texCoordCount() > var6 && var4 < this.texCoords.length && this.texCoords[var4] != null) {
                        var2.glTexCoordPointer(2, 5126, 0, this.texCoords[var4].buffer());
                    } else {
                        var2.glTexCoordPointer(2, 5126, 0, this.texCoords[0].buffer());
                    }
                }

                var2.glClientActiveTexture(33984);
            }
        }

    }

    public void setColor(int var1, float var2, float var3, float var4, float var5) {
        this.colors.set(var1, var2, var3, var4, var5);
    }

    public void setNormal(int var1, float var2, float var3, float var4) {
        this.normals.set(var1, var2, var3, var4);
    }

    public void setPosition(int var1, float var2, float var3, float var4) {
        this.vertices.set(var1, var2, var3, var4);
    }

    public void setTangents(int var1, float var2, float var3, float var4, float var5) {
        this.tangents.set(var1, var2, var3, var4, var5);
    }

    public void setTangents(int var1, float var2, float var3, float var4) {
        this.tangents.set(var1, var2, var3, var4);
    }

    public void setTexCoord(int var1, int var2, float var3, float var4) {
        this.texCoords[var2].set(var1, var3, var4);
    }

    public int size() {
        return this.vertexCount;
    }

    public void getPosition(int var1, Vec3f var2) {
        this.vertices.get(var1, var2);
    }

    public Vec3fData getVertices() {
        return this.vertices;
    }

    public Vec3fData getNormals() {
        return this.normals;
    }

    public void setNormals(Vec3fData var1) {
        this.totalAllocatedBytes -= var1.buffer().capacity() * 4;
        this.normals = var1;
        this.totalAllocatedBytes += var1.buffer().capacity() * 4;
    }

    public Vec4fData getTangents() {
        return this.tangents;
    }

    public void setTangents(Vec4fData var1) {
        this.totalAllocatedBytes -= var1.buffer().capacity() * 4;
        this.tangents = var1;
        this.totalAllocatedBytes += var1.buffer().capacity() * 4;
    }

    public Vec4fData getColors() {
        return this.colors;
    }

    public Vec2fData[] getTexCoords() {
        return this.texCoords;
    }

    private void buildVBO(DrawContext var1) {
        GL var2 = var1.getGL();
        int[] var3 = new int[1];
        var2.glGenBuffersARB(1, var3, 0);
        this.VBO = var3[0];
        this.vboCreated = true;
        var2.glBindBufferARB(34962, var3[0]);
        int var4 = 0;
        var2.glBufferDataARB(34962, this.totalAllocatedBytes, (Buffer) null, 35044);
        if (this.vertices != null) {
            var2.glBufferSubDataARB(34962, var4, this.vertices.byteSize(), this.vertices.buffer());
            this.verticesOffset = var4;
            var4 += this.vertices.byteSize();
        }

        if (this.normals != null) {
            var2.glBufferSubDataARB(34962, var4, this.normals.byteSize(), this.normals.buffer());
            this.normalsOffset = var4;
            var4 += this.normals.byteSize();
        }

        if (this.tangents != null) {
            var2.glBufferSubDataARB(34962, var4, this.tangents.byteSize(), this.tangents.buffer());
            this.tangentsOffset = var4;
            var4 += this.tangents.byteSize();
        }

        if (this.colors != null) {
            var2.glBufferSubDataARB(34962, var4, this.colors.byteSize(), this.colors.buffer());
            this.colorsOffset = var4;
            var4 += this.colors.byteSize();
        }

        if (this.texCoords != null && this.texCoords.length > 0) {
            this.texCoordsOffsets = new int[this.texCoords.length];

            for (int var5 = 0; var5 < this.texCoordsOffsets.length; ++var5) {
                if (this.texCoords[var5] != null) {
                    var2.glBufferSubDataARB(34962, var4, this.texCoords[var5].byteSize(), this.texCoords[var5].buffer());
                    this.texCoordsOffsets[var5] = var4;
                    var4 += this.texCoords[var5].byteSize();
                } else {
                    this.texCoordsOffsets[var5] = -1;
                }
            }
        }

        var2.glBindBufferARB(34962, 0);
    }

    public void disposeVBO(DrawContext var1) {
        if (this.vboCreated) {
            var1.getGL().glDeleteBuffersARB(1, new int[]{this.VBO}, 0);
            this.vboCreated = false;
            this.VBO = 0;
        }

    }

    public void setUseVbo(boolean var1) {
        this.useVBO = var1;
    }

    public boolean isUseVBO() {
        return this.useVBO;
    }

    public void getNormal(int var1, Vec3f var2) {
        this.normals.get(var1, var2);
    }

    public void getTexCoord(int var1, int var2, aQKa var3) {
        this.texCoords[var2].get(var1, var3);
    }

    public void getTangents(int var1, aJF var2) {
        this.tangents.get(var1, var2);
    }

    public VertexLayout getLayout() {
        return this.layout;
    }

    public VertexData duplicate() {
        return new VertexDataImpl(this);
    }
}
