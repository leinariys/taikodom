package taikodom.render.data;

import all.aJF;
import all.aQKa;
import com.hoplon.geometry.Vec3f;
import com.sun.opengl.util.BufferUtil;
import taikodom.render.DrawContext;
import taikodom.render.PrimitiveDrawingState;

import javax.media.opengl.GL;
import java.nio.ByteBuffer;

public class InterleavedVertexData extends VertexData {
    protected ByteBuffer data;
    protected boolean vboCreated;
    protected int size;
    private VertexLayout layout;
    private boolean useVBO = false;
    private int VBO;

    public InterleavedVertexData(VertexLayout var1, int var2) {
        this.layout = var1;
        this.size = var2;
        int var3 = var2 * var1.stride();
        this.data = BufferUtil.newByteBuffer(var3);
    }

    public InterleavedVertexData(InterleavedVertexData var1) {
        this.layout = var1.layout;
        this.size = var1.size;
        int var2 = this.size * this.layout.stride();
        this.data = BufferUtil.newByteBuffer(var2);
        var1.data.clear();
        this.data.clear();
        this.data.put(var1.data);
        var1.data.clear();
        this.data.clear();
    }

    public void setPosition(int var1, float var2, float var3, float var4) {
        int var5 = var1 * this.layout.stride() + this.layout.positionOffset();
        this.data.putFloat(var5, var2);
        this.data.putFloat(var5 + 4, var3);
        this.data.putFloat(var5 + 8, var4);
    }

    public Vec3f getPosition(int var1) {
        int var2 = var1 * this.layout.stride() + this.layout.positionOffset();
        return new Vec3f(this.data.getFloat(var2), this.data.getFloat(var2 + 4), this.data.getFloat(var2 + 8));
    }

    public aQKa getTexCoord(int var1, int var2) {
        int var3 = var1 * this.layout.stride() + this.layout.texCoordOffset(var2);
        return new aQKa(this.data.getFloat(var3), this.data.getFloat(var3 + 4));
    }

    public void setColor(int var1, float var2, float var3, float var4, float var5) {
        int var6 = var1 * this.layout.stride() + this.layout.colorOffset();
        this.data.putFloat(var6, var2);
        this.data.putFloat(var6 + 4, var3);
        this.data.putFloat(var6 + 8, var4);
        this.data.putFloat(var6 + 12, var5);
    }

    public void setNormal(int var1, float var2, float var3, float var4) {
        int var5 = var1 * this.layout.stride() + this.layout.normalsOffset();
        this.data.putFloat(var5, var2);
        this.data.putFloat(var5 + 4, var3);
        this.data.putFloat(var5 + 8, var4);
    }

    public void setTangents(int var1, float var2, float var3, float var4, float var5) {
        int var6 = var1 * this.layout.stride() + this.layout.tangentsOffset();
        this.data.putFloat(var6, var2);
        this.data.putFloat(var6 + 4, var3);
        this.data.putFloat(var6 + 8, var4);
        this.data.putFloat(var6 + 12, var5);
    }

    public void setTangents(int var1, float var2, float var3, float var4) {
        int var5 = var1 * this.layout.stride() + this.layout.tangentsOffset();
        this.data.putFloat(var5, var2);
        this.data.putFloat(var5 + 4, var3);
        this.data.putFloat(var5 + 8, var4);
    }

    public void setTexCoord(int var1, int var2, float var3, float var4) {
        int var5 = var1 * this.layout.stride() + this.layout.texCoordOffset(var2);
        this.data.putFloat(var5, var3);
        this.data.putFloat(var5 + 4, var4);
    }

    public int size() {
        return this.size;
    }

    public ByteBuffer data() {
        return this.data;
    }

    public VertexLayout layout() {
        return this.layout;
    }

    public boolean isUseVBO() {
        return this.useVBO;
    }

    public void setUseVbo(boolean var1) {
        this.useVBO = var1;
    }

    public void bind(DrawContext var1) {
        /**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
         * седержит методы для рисования примитивов (линии треугольники)*/
        GL var2 = var1.getGL();
        PrimitiveDrawingState var3 = var1.primitiveState;
        if (!var1.isUseVbo()) {
            this.useVBO = false;
        }

        int var4;
        int var5;
        int var6;
        if (this.useVBO) {
            if (!this.vboCreated) {
                this.buildVBO(var1);
            }

            var2.glBindBufferARB(34962, this.VBO);
            if (this.layout.positionOffset() >= 0) {
                var2.glVertexPointer(3, 5126, this.layout.stride(), (long) this.layout.positionOffset());
            }

            if (var3.isUseNormalArray()) {
                if (this.layout.normalsOffset() < 0) {
                    throw new RuntimeException("This mesh doesnt have normals");
                }

                var2.glNormalPointer(5126, this.layout.stride(), (long) this.layout.normalsOffset());
            }

            if (var3.isUseColorArray()) {
                if (this.layout.colorOffset() < 0) {
                    throw new RuntimeException("This mesh doesnt have colors");
                }

                var2.glColorPointer(this.layout.colorSize(), 5126, this.layout.stride(), (long) this.layout.colorOffset());
            }

            if (var3.isUseTangentArray()) {
                if (this.layout.tangentsOffset() < 0) {
                    throw new RuntimeException("This mesh doesnt have tangents");
                }

                var2.glVertexAttribPointer(var3.getTangentChannel(), 4, 5126, false, 0, (long) this.layout.tangentsOffset());
            }

            if (this.layout.texCoordCount() > 0) {
                for (var4 = 0; var4 < var3.getNumTextures(); ++var4) {
                    var5 = var3.channelMapping(var4);
                    var6 = var3.texCoordsSets(var5);
                    var2.glClientActiveTexture('蓀' + var5);
                    if (this.layout.texCoordCount() > var6) {
                        var2.glTexCoordPointer(2, 5126, this.layout.stride(), (long) this.layout.texCoordOffset(var6));
                    } else {
                        var2.glTexCoordPointer(2, 5126, this.layout.stride(), (long) this.layout.texCoordOffset(0));
                    }
                }

                var2.glClientActiveTexture(33984);
            }
        } else {
            if (this.layout.positionOffset() >= 0) {
                this.data.position(this.layout.positionOffset());
                var2.glVertexPointer(3, 5126, this.layout.stride(), this.data);
            }

            if (var3.isUseNormalArray()) {
                if (this.layout.normalsOffset() < 0) {
                    throw new RuntimeException("This mesh doesnt have normals");
                }

                this.data.position(this.layout.normalsOffset());
                var2.glNormalPointer(5126, this.layout.stride(), this.data);
            }

            if (var3.isUseColorArray()) {
                if (this.layout.colorOffset() < 0) {
                    throw new RuntimeException("This mesh doesnt have colors");
                }

                this.data.position(this.layout.colorOffset());
                var2.glColorPointer(this.layout.colorSize(), 5126, this.layout.stride(), this.data);
            }

            if (var3.isUseTangentArray()) {
                if (this.layout.tangentsOffset() < 0) {
                    throw new RuntimeException("This mesh doesnt have tangents");
                }

                this.data.position(this.layout.tangentsOffset());
                var2.glVertexAttribPointer(var3.getTangentChannel(), 4, 5126, false, 0, this.data);
            }

            if (this.layout.texCoordCount() > 0) {
                for (var4 = 0; var4 < var3.getNumTextures(); ++var4) {
                    var5 = var3.channelMapping(var4);
                    var6 = var3.texCoordsSets(var5);
                    var2.glClientActiveTexture('蓀' + var5);
                    if (this.layout.texCoordCount() > var6) {
                        this.data.position(this.layout.texCoordOffset(var6));
                    } else {
                        this.data.position(this.layout.texCoordOffset(0));
                    }

                    var2.glTexCoordPointer(2, 5126, this.layout.stride(), this.data);
                }

                var2.glClientActiveTexture(33984);
            }
        }

    }

    private void buildVBO(DrawContext var1) {
        /**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
         * седержит методы для рисования примитивов (линии треугольники)*/
        GL var2 = var1.getGL();
        int[] var3 = new int[1];
        var2.glGenBuffersARB(1, var3, 0);
        var2.glBindBufferARB(34962, var3[0]);
        var2.glBufferDataARB(34962, this.size() * this.layout.stride(), this.data, 35044);
        this.VBO = var3[0];
        this.vboCreated = true;
    }

    public void disposeVBO(DrawContext var1) {
        if (this.vboCreated) {
            var1.getGL().glDeleteBuffersARB(1, new int[]{this.VBO}, 0);
            this.vboCreated = false;
            this.VBO = 0;
        }

    }

    public void getPosition(int var1, Vec3f var2) {
        int var3 = var1 * this.layout.stride() + this.layout.positionOffset();
        var2.x = this.data.getFloat(var3);
        var2.y = this.data.getFloat(var3 + 4);
        var2.z = this.data.getFloat(var3 + 8);
    }

    public void getNormal(int var1, Vec3f var2) {
        int var3 = var1 * this.layout.stride() + this.layout.normalsOffset();
        var2.set(this.data.getFloat(var3), this.data.getFloat(var3 + 4), this.data.getFloat(var3 + 8));
    }

    public void getTexCoord(int var1, int var2, aQKa var3) {
        int var4 = var1 * this.layout.stride() + this.layout.texCoordOffset(var2);
        var3.set(this.data.getFloat(var4), this.data.getFloat(var4 + 4));
    }

    public void getTangents(int var1, aJF var2) {
        int var3 = var1 * this.layout.stride() + this.layout.tangentsOffset();
        var2.set(this.data.getFloat(var3), this.data.getFloat(var3 + 4), this.data.getFloat(var3 + 8), this.data.getFloat(var3 + 12));
    }

    public VertexLayout getLayout() {
        return this.layout;
    }

    public VertexData duplicate() {
        return new InterleavedVertexData(this);
    }
}
