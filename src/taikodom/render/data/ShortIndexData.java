package taikodom.render.data;

import com.sun.opengl.util.BufferUtil;
import taikodom.render.DrawContext;

import javax.media.opengl.GL;
import java.nio.Buffer;
import java.nio.ShortBuffer;

/**
 *
 */
public class ShortIndexData extends IndexData {
    private int glType = 5123;
    private ShortBuffer data;
    private int VBO;
    private boolean vboCreated = false;

    public ShortIndexData(int var1) {
        this.data = BufferUtil.newShortBuffer(var1);
        this.size = var1;
    }

    public ShortIndexData(ShortIndexData var1) {
        this.size = var1.size;
        this.data = BufferUtil.newShortBuffer(this.size);
        var1.data.clear();
        this.data.clear();
        this.data.put(var1.data);
        var1.data.clear();
        this.data.clear();
    }

    public void setIndex(int var1, int var2) {
        this.data.put(var1, (short) var2);
    }

    public int getIndex(int var1) {
        return this.data.get(var1);
    }

    public int getGLType() {
        return this.glType;
    }

    public Buffer buffer() {
        return this.useVbo && this.vboCreated ? null : this.data;
    }

    public IndexData subSet(int var1, int var2) {
        ShortIndexData var3 = new ShortIndexData(var2);
        this.data.position(var1);
        this.data.limit(var1 + var2);
        var3.data.put(this.data);
        var3.data.clear();
        this.data.clear();
        return var3;
    }

    public String toString() {
        StringBuilder var1 = new StringBuilder();

        for (int var2 = 0; var2 < this.data.capacity(); ++var2) {
            var1.append(this.data.get(var2)).append(", ");
        }

        return var1.toString();
    }

    public void resize(int var1) {
        if (var1 > this.data.capacity()) {
            ShortBuffer var2 = BufferUtil.newShortBuffer(var1);
            this.size = var1;
            var2.put(this.data);
            var2.clear();
            this.data = var2;
        }

    }

    public void clear() {
        this.data.clear();
        this.size = 0;
    }

    public void bind(DrawContext var1) {/**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
     * седержит методы для рисования примитивов (линии треугольники)*/
        GL var2 = var1.getGL();
        if (!var1.isUseVbo()) {
            this.useVbo = false;
        }
        if (this.useVbo) {
            if (!this.vboCreated) {
                this.buildVBO(var1);
            }
            var2.glBindBufferARB(34963, this.VBO);
        } else {
            var2.glBindBufferARB(34963, 0);
        }
    }

    private void buildVBO(DrawContext var1) {/**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
     * седержит методы для рисования примитивов (линии треугольники)*/
        GL var2 = var1.getGL();
        if (this.data != null) {
            int[] var3 = new int[1];
            var2.glGenBuffersARB(1, var3, 0);
            this.VBO = var3[0];
            this.vboCreated = true;
            var2.glBindBufferARB(34963, var3[0]);
            var2.glBufferDataARB(34963, this.size * 2, this.data, 35044);
            var2.glBindBufferARB(34963, 0);
            this.vboCreated = true;
        } else {
            this.useVbo = false;
        }

    }

    public IndexData duplicate() {
        return new ShortIndexData(this);
    }

    public boolean isVBO() {
        return this.VBO != 0;
    }
}
