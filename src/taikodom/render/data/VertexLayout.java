package taikodom.render.data;

public class VertexLayout {
    public static final VertexLayout defaultLayout = new VertexLayout(true, true, true, true, 1);
    private final int stride;
    private final int positionOffset;
    private final int tangentsOffset;
    private final int normalsOffset;
    private final int colorOffset;
    private final int texCoordOffset;
    private final int binormalsOffset;
    private final boolean hasPosition;
    private final boolean hasColor;
    private final boolean hasTangents;
    private final boolean hasNormals;
    private final int texCoordCount;

    public VertexLayout(boolean var1, boolean var2, boolean var3, boolean var4, int var5) {
        this.hasPosition = var1;
        this.hasColor = var2;
        this.hasTangents = var3;
        this.hasNormals = var4;
        this.texCoordCount = var5;
        int var6 = 0;
        if (var1) {
            this.positionOffset = 0;
            var6 += 12;
        } else {
            this.positionOffset = -1;
        }

        if (var2) {
            this.colorOffset = var6;
            var6 += 16;
        } else {
            this.colorOffset = -1;
        }

        if (var3) {
            this.tangentsOffset = var6;
            var6 += 16;
        } else {
            this.tangentsOffset = -1;
        }

        if (var4) {
            this.normalsOffset = var6;
            var6 += 12;
        } else {
            this.normalsOffset = -1;
        }

        if (var5 > 0) {
            this.texCoordOffset = var6;
            var6 += 8 * var5;
        } else {
            this.texCoordOffset = -1;
        }

        this.binormalsOffset = -1;
        this.stride = var6;
    }

    public int stride() {
        return this.stride;
    }

    public int positionOffset() {
        return this.positionOffset;
    }

    public int colorOffset() {
        return this.colorOffset;
    }

    public int tangentsOffset() {
        return this.tangentsOffset;
    }

    public int normalsOffset() {
        return this.normalsOffset;
    }

    public int texCoordOffset() {
        return this.texCoordOffset;
    }

    public int texCoordCount() {
        return this.texCoordCount;
    }

    public int colorSize() {
        return 4;
    }

    public int texCoordOffset(int var1) {
        return var1 * 4 * 2 + this.texCoordOffset;
    }

    public boolean isHasPosition() {
        return this.hasPosition;
    }

    public boolean isHasColor() {
        return this.hasColor;
    }

    public boolean isHasTangents() {
        return this.hasTangents;
    }

    public boolean hasNormals() {
        return this.hasNormals;
    }
}
