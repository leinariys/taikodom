package taikodom.render.data;

import all.aJF;
import all.aQKa;
import com.hoplon.geometry.Vec3f;
import taikodom.render.DrawContext;

public abstract class VertexData {
    public abstract void setPosition(int var1, float var2, float var3, float var4);

    public abstract void setColor(int var1, float var2, float var3, float var4, float var5);

    public abstract void setNormal(int var1, float var2, float var3, float var4);

    public abstract void setTangents(int var1, float var2, float var3, float var4, float var5);

    public abstract void setTangents(int var1, float var2, float var3, float var4);

    public abstract void setTexCoord(int var1, int var2, float var3, float var4);

    public abstract void getPosition(int var1, Vec3f var2);

    public abstract int size();

    public abstract void bind(DrawContext var1);

    public abstract boolean isUseVBO();

    public abstract void setUseVbo(boolean var1);

    public abstract void disposeVBO(DrawContext var1);

    public abstract void getNormal(int var1, Vec3f var2);

    public abstract void getTexCoord(int var1, int var2, aQKa var3);

    public abstract void getTangents(int var1, aJF var2);

    public abstract VertexLayout getLayout();

    public abstract VertexData duplicate();

    public void setPosition(int var1, Vec3f var2) {
        this.setPosition(var1, var2.x, var2.y, var2.z);
    }

    public void setTangents(int var1, aJF var2) {
        this.setTangents(var1, var2.x, var2.y, var2.z, var2.w);
    }

    public void setNormal(int var1, Vec3f var2) {
        this.setNormal(var1, var2.x, var2.y, var2.z);
    }
}
