package taikodom.render.data;

import all.bc_q;

import java.nio.FloatBuffer;

public class FloatMatrixData extends Data {
    private int columnCount;
    private int rowCount;

    public FloatMatrixData() {
        this(4, 4);
    }

    public FloatMatrixData(int var1) {
        this((int) Math.sqrt((double) var1), (int) Math.sqrt((double) var1));
    }

    public FloatMatrixData(FloatBuffer var1) {
        this((int) Math.sqrt((double) var1.capacity()), (int) Math.sqrt((double) var1.capacity()), var1);
    }

    public FloatMatrixData(int var1, int var2) {
        super(var1 * var2);
        this.rowCount = var1;
        this.columnCount = var2;
        int var3 = Math.max(var1, var2);

        for (int var4 = 0; var4 < var3; ++var4) {
            this.set(var4, var4, 1.0F);
        }

    }

    public FloatMatrixData(int var1, int var2, FloatBuffer var3) {
        super(var3.capacity());
        this.rowCount = var1;
        this.columnCount = var2;
        this.data.put(var3);
    }

    public float get(int var1, int var2) {
        return this.data.get(this.index(var1, var2));
    }

    private int index(int var1, int var2) {
        return Math.min(var1, this.rowCount - 1) * this.columnCount + Math.min(var2, this.columnCount - 1);
    }

    public void set(int var1, int var2, float var3) {
        this.data.put(this.index(var1, var2), var3);
    }

    public int getColumnCount() {
        return this.columnCount;
    }

    public int getRowCount() {
        return this.rowCount;
    }

    public bc_q getTransform() {
        if (this.rowCount >= 4 && this.columnCount >= 4) {
            bc_q var1 = new bc_q();
            var1.a(this.get(0, 0), this.get(0, 1), this.get(0, 2), this.get(1, 0), this.get(1, 1), this.get(1, 2), this.get(2, 0), this.get(2, 1), this.get(2, 2));
            var1.setTranslation((double) this.get(3, 0), (double) this.get(3, 1), (double) this.get(3, 2));
            return var1;
        } else {
            throw new IllegalStateException();
        }
    }

    public void setTransform(bc_q var1) {
        this.set(0, 0, var1.mX.m00);
        this.set(1, 0, var1.mX.m10);
        this.set(2, 0, var1.mX.m20);
        this.set(0, 1, var1.mX.m01);
        this.set(1, 1, var1.mX.m11);
        this.set(2, 1, var1.mX.m21);
        this.set(0, 2, var1.mX.m02);
        this.set(1, 2, var1.mX.m12);
        this.set(2, 2, var1.mX.m22);
        this.set(0, 3, (float) var1.position.x);
        this.set(1, 3, (float) var1.position.y);
        this.set(2, 3, (float) var1.position.z);
    }

    public void getTransform(bc_q var1) {
        if (this.rowCount >= 4 && this.columnCount >= 4) {
            var1.a(this.get(0, 0), this.get(0, 1), this.get(0, 2), this.get(1, 0), this.get(1, 1), this.get(1, 2), this.get(2, 0), this.get(2, 1), this.get(2, 2));
            var1.setTranslation((double) this.get(3, 0), (double) this.get(3, 1), (double) this.get(3, 2));
        } else {
            throw new IllegalStateException();
        }
    }

    public FloatMatrixData duplicate() {
        this.data.clear();
        FloatMatrixData var1 = new FloatMatrixData(this.rowCount, this.columnCount);
        var1.buffer().put(this.data);
        var1.buffer().clear();
        this.data.clear();
        return var1;
    }
}
