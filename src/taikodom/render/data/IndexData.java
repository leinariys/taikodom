package taikodom.render.data;

import taikodom.render.DrawContext;

import java.nio.Buffer;

public abstract class IndexData {
    protected int glType;
    protected int size;
    protected boolean useVbo = false;

    public abstract void bind(DrawContext var1);

    public abstract Buffer buffer();

    public abstract void clear();

    public abstract int getGLType();

    public abstract int getIndex(int var1);

    public abstract void setIndex(int var1, int var2);

    public abstract IndexData subSet(int var1, int var2);

    public abstract IndexData duplicate();

    public boolean isUseVBO() {
        return this.useVbo;
    }

    public void setUseVbo(boolean var1) {
        this.useVbo = var1;
    }

    public boolean isVBO() {
        return false;
    }

    public int size() {
        return this.size;
    }
}
