package taikodom.render.data;

import com.sun.opengl.util.BufferUtil;
import taikodom.render.DrawContext;

import java.nio.ByteBuffer;

public class ExpandableVertexData extends InterleavedVertexData {
    protected int used;
    private boolean shouldDisposeVBO;

    public ExpandableVertexData(VertexLayout var1, int var2) {
        super(var1, var2);
        this.used = 0;
    }

    public ExpandableVertexData(ExpandableVertexData var1) {
        super(var1);
        this.used = var1.used;
    }

    public int size() {
        return this.used;
    }

    public void setColor(int var1, float var2, float var3, float var4, float var5) {
        this.ensure(var1);
        super.setColor(var1, var2, var3, var4, var5);
    }

    public void setNormal(int var1, float var2, float var3, float var4) {
        this.ensure(var1);
        super.setNormal(var1, var2, var3, var4);
    }

    public void setPosition(int var1, float var2, float var3, float var4) {
        this.ensure(var1);
        super.setPosition(var1, var2, var3, var4);
    }

    public void setTangents(int var1, float var2, float var3, float var4, float var5) {
        this.ensure(var1);
        super.setTangents(var1, var2, var3, var4, var5);
    }

    public void setTexCoord(int var1, int var2, float var3, float var4) {
        this.ensure(var1);
        super.setTexCoord(var1, var2, var3, var4);
    }

    public void bind(DrawContext var1) {
        if (this.shouldDisposeVBO) {
            this.disposeVBO(var1);
            this.shouldDisposeVBO = false;
        }

        super.bind(var1);
    }

    public void ensure(int var1) {
        if (this.size <= var1) {
            if (this.vboCreated && !this.shouldDisposeVBO) {
                this.shouldDisposeVBO = true;
            }

            int var2 = Math.max(this.size * 3 / 2, var1) + 1;
            int var3 = var2 * this.layout().stride();
            ByteBuffer var4 = BufferUtil.newByteBuffer(var3);
            var4.put(this.data);
            var4.clear();
            this.data = var4;
            this.size = var2;
            this.used = var1 + 1;
        } else if (this.used <= var1) {
            this.used = var1 + 1;
        }

    }

    public VertexData duplicate() {
        return new ExpandableVertexData(this);
    }
}
