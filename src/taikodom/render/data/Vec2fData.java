package taikodom.render.data;

import all.aQKa;

public class Vec2fData extends Data {
    public Vec2fData(int var1) {
        super(var1 * 2);
    }

    public void set(int var1, float var2, float var3) {
        int var4 = var1 * 2;
        this.data.put(var4, var2);
        this.data.put(var4 + 1, var3);
    }

    public void set(int var1, aQKa var2) {
        int var3 = var1 * 2;
        this.data.put(var3, var2.x);
        this.data.put(var3 + 1, var2.y);
    }

    public void get(int var1, aQKa var2) {
        int var3 = var1 * 2;
        var2.x = this.data.get(var3);
        var2.y = this.data.get(var3 + 1);
    }

    public String toString() {
        StringBuilder var1 = new StringBuilder();

        for (int var2 = 0; var2 < this.data.capacity(); var2 += 2) {
            var1.append("(");
            var1.append(this.data.get(var2)).append(", ");
            var1.append(this.data.get(var2 + 1));
            var1.append("), ");
        }

        return var1.toString();
    }

    public Vec2fData duplicate() {
        this.data.clear();
        Vec2fData var1 = new Vec2fData(this.data.capacity() / 2);
        var1.buffer().put(this.data);
        var1.buffer().clear();
        this.data.clear();
        return var1;
    }
}
