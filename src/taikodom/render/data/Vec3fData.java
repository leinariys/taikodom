package taikodom.render.data;

import com.hoplon.geometry.Vec3f;

import java.nio.FloatBuffer;

public class Vec3fData extends Data {
    public Vec3fData(int var1) {
        super(var1 * 3);
    }

    public Vec3fData(FloatBuffer var1) {
        super(var1);
    }

    public void set(int var1, float var2, float var3, float var4) {
        int var5 = var1 * 3;
        this.data.put(var5, var2);
        this.data.put(var5 + 1, var3);
        this.data.put(var5 + 2, var4);
    }

    public void set(int var1, Vec3f var2) {
        int var3 = var1 * 3;
        this.data.put(var3, var2.x);
        this.data.put(var3 + 1, var2.y);
        this.data.put(var3 + 2, var2.z);
    }

    public void get(int var1, Vec3f var2) {
        int var3 = var1 * 3;
        var2.x = this.data.get(var3);
        var2.y = this.data.get(var3 + 1);
        var2.z = this.data.get(var3 + 2);
    }

    public String toString() {
        StringBuilder var1 = new StringBuilder();

        for (int var2 = 0; var2 < this.data.capacity(); var2 += 3) {
            var1.append("(");
            var1.append(this.data.get(var2)).append(", ");
            var1.append(this.data.get(var2 + 1)).append(", ");
            var1.append(this.data.get(var2 + 2));
            var1.append("), ");
        }

        return var1.toString();
    }

    public Vec3fData duplicate() {
        this.data.clear();
        Vec3fData var1 = new Vec3fData(this.data.capacity() / 3);
        var1.buffer().put(this.data);
        var1.buffer().clear();
        this.data.clear();
        return var1;
    }
}
