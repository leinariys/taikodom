package taikodom.render;

import com.hoplon.geometry.Color;

/**
 *
 */
public class RenderInfo {
    private final Color clearColor = new Color(0.0F, 0.0F, 0.0F, 0.0F);
    private final Color fogColor = new Color(0.0F, 0.0F, 0.0F, 0.0F);
    private boolean clearColorBuffer = true;
    private boolean clearDepthBuffer = true;
    private boolean clearStencilBuffer = true;
    private int fogType = 9729;
    private float fogStart = 1.0F;
    private float fogEnd = 1000.0F;
    private float fogExp = 1.0F;
    private boolean wireframeOnly = false;
    /**
     * Прямоугольник вокруб объекта
     * для обнаружения столкновений или пересечений различных объектов между собой.
     */
    private boolean drawAABBs = false;
    private boolean noShading = false;
    private boolean drawLights = false;

    public void set(RenderInfo var1) {
        this.clearColor.set(var1.clearColor);
        this.clearColorBuffer = var1.clearColorBuffer;
        this.clearDepthBuffer = var1.clearDepthBuffer;
        this.clearStencilBuffer = var1.clearStencilBuffer;
        this.fogType = var1.fogType;
        this.fogStart = var1.fogStart;
        this.fogEnd = var1.fogEnd;
        this.fogExp = var1.fogExp;
        this.fogColor.set(var1.fogColor);
        this.wireframeOnly = var1.wireframeOnly;
        this.noShading = var1.noShading;
        this.drawAABBs = var1.drawAABBs;
        this.drawLights = var1.drawLights;
    }

    public boolean isClearColorBuffer() {
        return this.clearColorBuffer;
    }

    public void setClearColorBuffer(boolean var1) {
        this.clearColorBuffer = var1;
    }

    public boolean isClearDepthBuffer() {
        return this.clearDepthBuffer;
    }

    public void setClearDepthBuffer(boolean var1) {
        this.clearDepthBuffer = var1;
    }

    public boolean isClearStencilBuffer() {
        return this.clearStencilBuffer;
    }

    public void setClearStencilBuffer(boolean var1) {
        this.clearStencilBuffer = var1;
    }

    public int getFogType() {
        return this.fogType;
    }

    public void setFogType(int var1) {
        this.fogType = var1;
    }

    public float getFogStart() {
        return this.fogStart;
    }

    public void setFogStart(float var1) {
        this.fogStart = var1;
    }

    public float getFogEnd() {
        return this.fogEnd;
    }

    public void setFogEnd(float var1) {
        this.fogEnd = var1;
    }

    public float getFogExp() {
        return this.fogExp;
    }

    public void setFogExp(float var1) {
        this.fogExp = var1;
    }

    public Color getFogColor() {
        return this.fogColor;
    }

    public void setFogColor(Color var1) {
        this.fogColor.set(var1);
    }

    public boolean isWireframeOnly() {
        return this.wireframeOnly;
    }

    public void setWireframeOnly(boolean var1) {
        this.wireframeOnly = var1;
    }

    public Color getClearColor() {
        return this.clearColor;
    }

    public void setClearColor(float var1, float var2, float var3, float var4) {
        this.clearColor.set(var1, var2, var3, var4);
    }

    /**
     * Прямоугольник вокруб объекта
     * для обнаружения столкновений или пересечений различных объектов между собой.
     *
     * @return
     */
    public boolean isDrawAABBs() {
        return this.drawAABBs;
    }

    /**
     * Прямоугольник вокруб объекта
     * для обнаружения столкновений или пересечений различных объектов между собой.
     *
     * @param var1
     */
    public void setDrawAABBs(boolean var1) {
        this.drawAABBs = var1;
    }

    public boolean isNoShading() {
        return this.noShading;
    }

    public void setNoShading(boolean var1) {
        this.noShading = var1;
    }

    public boolean isDrawLights() {
        return this.drawLights;
    }

    public void setDrawLights(boolean var1) {
        this.drawLights = var1;
    }
}
