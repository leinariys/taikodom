package taikodom.render.postProcessing;

import taikodom.render.RenderContext;
import taikodom.render.SceneView;
import taikodom.render.loader.RenderAsset;

public abstract class PostProcessingFX extends RenderAsset {
    protected boolean enabled = true;

    public abstract boolean init(RenderContext var1);

    public abstract void preRender(RenderContext var1, SceneView var2);

    public abstract void render(RenderContext var1, SceneView var2);

    public void setSceneIntensity(float var1) {
    }

    public boolean isEnabled() {
        return this.enabled;
    }

    public void setEnabled(boolean var1) {
        this.enabled = var1;
    }
}
