package taikodom.render.postProcessing;

import taikodom.render.DrawContext;
import taikodom.render.RenderContext;
import taikodom.render.SceneView;
import taikodom.render.enums.BlendType;
import taikodom.render.gl.GLSupportedCaps;
import taikodom.render.loader.RenderAsset;
import taikodom.render.primitives.Primitive;
import taikodom.render.primitives.TreeRecord;
import taikodom.render.scene.TreeRecordList;
import taikodom.render.shaders.*;
import taikodom.render.textures.ChannelInfo;
import taikodom.render.textures.Material;
import taikodom.render.textures.RenderTarget;

import javax.media.opengl.GL;
import javax.vecmath.Matrix3f;
import java.util.Collections;

public class GlowFX extends PostProcessingFX {
    private static final int RT_RESOLUTION = 256;
    protected float glowIntensity = 1.0F;
    private RenderTarget renderTarget;
    private RenderTarget tempTarget;
    private boolean initialized = false;
    private boolean isSupported = false;
    private Shader defaultShader;
    private ShaderProgram verProgram;
    private ShaderProgram horProgram;
    private float feedback = 0.75F;
    private int nFilterPasses = 4;
    private float texCoord = 1.0F;
    private boolean useFBO = false;
    private RenderStates defaultStates = new RenderStates();

    public void setSceneIntensity(float var1) {
        this.glowIntensity = var1;
    }

    public boolean init(RenderContext var1) {
        if (!GLSupportedCaps.isGlsl()) {
            this.initialized = true;
            return false;
        } else {
            this.useFBO = GLSupportedCaps.isFbo();
            if (this.useFBO) {
                this.initAsFBO(var1);
            } else {
                this.initAsPbuffer(var1);
            }

            this.defaultShader = new Shader();
            this.defaultShader.setName("Glow shader");
            ShaderPass var2 = new ShaderPass();
            var2.setName("Glow shader pass");
            ChannelInfo var3 = new ChannelInfo();
            var3.setTexChannel(10);
            var2.setChannelTextureSet(0, var3);
            this.defaultShader.addPass(var2);
            this.createShaderPrograms(var1);
            this.initialized = true;
            return true;
        }
    }

    private void initAsFBO(RenderContext var1) {
        this.renderTarget = new RenderTarget();
        this.renderTarget.create(256, 256, true, var1.getDc(), false);
        this.tempTarget = new RenderTarget();
        this.tempTarget.create(256, 256, true, var1.getDc(), false);
        this.texCoord = 256.0F;
    }

    private void initAsPbuffer(RenderContext var1) {
        this.renderTarget = new RenderTarget();
        this.renderTarget.create(256, 256, true, var1.getDc(), true);
    }

    private void createShaderPrograms(RenderContext var1) {
        String var2 = "void main(void){gl_Position = ftransform();gl_TexCoord[0] = gl_MultiTexCoord0;}";
        ShaderProgramObject var3 = new ShaderProgramObject(35633, var2);
        float var4 = 0.005859375F;
        if (this.useFBO) {
            var4 = 1.0F;
        }

        String var5;
        String var6;
        if (this.useFBO) {
            var5 = "uniform sampler2DRect tex0; float texScaler = " + var4 + "; " + "float weight = 1.0/7.0; vec4 gaussFilter[7];" + "void main(void) " + "{ vec4 color = vec4(0.0,0.0,0.0,0.0);  gaussFilter[0] = vec4(0.0, -3.9, 0.0,  weight);gaussFilter[1] = vec4(0.0, -2.6, 0.0,  weight); gaussFilter[2] = vec4(0.0, -1.3, 0.0, weight); gaussFilter[3] = vec4( 0.0, 0.0, 0.0, weight); gaussFilter[4] = vec4(0.0,  1.3, 0.0, weight); gaussFilter[5] = vec4(0.0, 2.6, 0.0, weight); gaussFilter[6] = vec4(0.0, 3.9, 0.0,  weight);  int parseStyleDeclaration; " + "for (parseStyleDeclaration=0;parseStyleDeclaration<7;parseStyleDeclaration++) " + "{ " + "color += texture2DRect(tex0,vec2(gl_TexCoord[0].x + gaussFilter[parseStyleDeclaration].x * texScaler , " + "gl_TexCoord[0].y + gaussFilter[parseStyleDeclaration].y * texScaler )) *  gaussFilter[parseStyleDeclaration].w; " + "}  gl_FragColor = color; }";
            var6 = "uniform sampler2DRect tex0; float texScaler = " + var4 + "; " + "float weight = 1.0/7.0; vec4 gaussFilter[7];" + "void main(void) " + "{ vec4 color = vec4(0.0,0.0,0.0,0.0);  gaussFilter[0] = vec4(-3.9, 0.0, 0.0,  weight);gaussFilter[1] = vec4(-2.6, 0.0, 0.0,  weight); gaussFilter[2] = vec4(-1.3, 0.0, 0.0, weight); gaussFilter[3] = vec4( 0.0, 0.0, 0.0, weight); gaussFilter[4] = vec4( 1.3, 0.0, 0.0, weight); gaussFilter[5] = vec4(2.6, 0.0, 0.0, weight); gaussFilter[6] = vec4(3.9, 0.0, 0.0,  weight);  int parseStyleDeclaration; " + "for (parseStyleDeclaration=0;parseStyleDeclaration<7;parseStyleDeclaration++) " + "{ " + "color += texture2DRect(tex0,vec2(gl_TexCoord[0].x + gaussFilter[parseStyleDeclaration].x * texScaler , " + "gl_TexCoord[0].y + gaussFilter[parseStyleDeclaration].y * texScaler )) *  gaussFilter[parseStyleDeclaration].w; " + "}  gl_FragColor = color; }";
        } else {
            var5 = "uniform sampler2D tex0; float texScaler = " + var4 + "; " + "float weight = 1.0/7.0; vec4 gaussFilter[7];void main(void) { vec4 color = vec4(0.0,0.0,0.0,0.0);  gaussFilter[0] = vec4(0.0, -3.9, 0.0,  weight);gaussFilter[1] = vec4(0.0, -2.6, 0.0,  weight); gaussFilter[2] = vec4(0.0, -1.3, 0.0, weight); gaussFilter[3] = vec4( 0.0, 0.0, 0.0, weight); gaussFilter[4] = vec4(0.0,  1.3, 0.0, weight); gaussFilter[5] = vec4(0.0, 2.6, 0.0, weight); gaussFilter[6] = vec4(0.0, 3.9, 0.0,  weight);  int parseStyleDeclaration; for (parseStyleDeclaration=0;parseStyleDeclaration<7;parseStyleDeclaration++) { color += texture2D(tex0,vec2(gl_TexCoord[0].x + gaussFilter[parseStyleDeclaration].x * texScaler , gl_TexCoord[0].y + gaussFilter[parseStyleDeclaration].y * texScaler )) *  gaussFilter[parseStyleDeclaration].w; }  gl_FragColor = color; }";
            var6 = "uniform sampler2D tex0; float texScaler = " + var4 + "; " + "float weight = 1.0/7.0; vec4 gaussFilter[7];void main(void) { vec4 color = vec4(0.0,0.0,0.0,0.0);  gaussFilter[0] = vec4(-3.9, 0.0, 0.0,  weight);gaussFilter[1] = vec4(-2.6, 0.0, 0.0,  weight); gaussFilter[2] = vec4(-1.3, 0.0, 0.0, weight); gaussFilter[3] = vec4( 0.0, 0.0, 0.0, weight); gaussFilter[4] = vec4( 1.3, 0.0, 0.0, weight); gaussFilter[5] = vec4(2.6, 0.0, 0.0, weight); gaussFilter[6] = vec4(3.9, 0.0, 0.0,  weight);  int parseStyleDeclaration; for (parseStyleDeclaration=0;parseStyleDeclaration<7;parseStyleDeclaration++) { color += texture2D(tex0,vec2(gl_TexCoord[0].x + gaussFilter[parseStyleDeclaration].x * texScaler , gl_TexCoord[0].y + gaussFilter[parseStyleDeclaration].y * texScaler )) *  gaussFilter[parseStyleDeclaration].w; }  gl_FragColor = color; }";
        }

        ShaderProgramObject var7 = new ShaderProgramObject(35632, var5);
        ShaderProgramObject var8 = new ShaderProgramObject(35632, var6);
        this.verProgram = new ShaderProgram();
        this.verProgram.addProgramObject(var3);
        this.verProgram.addProgramObject(var7);
        this.horProgram = new ShaderProgram();
        this.horProgram.addProgramObject(var3);
        this.horProgram.addProgramObject(var8);
        if (!this.verProgram.compile((ShaderPass) null, var1.getDc())) {
            this.verProgram = null;
            this.horProgram = null;
        } else if (!this.horProgram.compile((ShaderPass) null, var1.getDc())) {
            this.verProgram = null;
            this.horProgram = null;
        } else {
            this.isSupported = true;
        }
    }

    public void preRender(RenderContext var1, SceneView var2) {
        if (this.initialized || this.init(var1)) {
            if (!this.isSupported) {
                this.enabled = false;
            } else {
                DrawContext var3 = var1.getDc();
                /**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
                 * седержит методы для рисования примитивов (линии треугольники)*/
                GL var4 = var3.getGL();
                var4.glMatrixMode(5889);
                var4.glPushMatrix();
                var4.glMatrixMode(5888);
                var4.glPushMatrix();
                this.renderTarget.bind(var1.getDc());
                this.renderEmissiveMaterial(var1);
                var4.glMatrixMode(5889);
                var4.glLoadIdentity();
                var4.glOrtho(0.0D, 256.0D, 0.0D, 256.0D, -1.0D, 1.0D);
                var4.glMatrixMode(5888);
                var4.glLoadIdentity();
                var4.glEnable(3042);
                var4.glBlendFunc(1, 1);
                var4.glDisable(2929);
                int var5;
                if (this.useFBO) {
                    this.tempTarget.bindTexture(var3);
                    this.renderFeedback(var3, var4);
                    this.renderTarget.unbind(var3);
                    var4.glDisable(3042);

                    for (var5 = 0; var5 < this.nFilterPasses; ++var5) {
                        this.renderTarget.bindTexture(var3);
                        this.tempTarget.bind(var3);
                        this.verticalBlur(var1);
                        this.tempTarget.unbind(var3);
                        this.tempTarget.bindTexture(var3);
                        this.renderTarget.bind(var3);
                        this.horizontalBlur(var1);
                        this.renderTarget.unbind(var3);
                    }
                } else {
                    this.renderTarget.bindTexture(var3);
                    this.renderFeedback(var3, var4);
                    this.renderTarget.copyBufferToTexture(var3);
                    var4.glDisable(3042);

                    for (var5 = 0; var5 < this.nFilterPasses; ++var5) {
                        this.verticalBlur(var1);
                        this.renderTarget.copyBufferToTexture(var3);
                        this.horizontalBlur(var1);
                        this.renderTarget.copyBufferToTexture(var3);
                    }

                    this.renderTarget.unbind(var3);
                }

                var4.glEnable(2929);
                var4.glDisable(this.renderTarget.getTexture().getTarget());
                var4.glDisable(3042);
                var4.glMatrixMode(5889);
                var4.glPopMatrix();
                var4.glMatrixMode(5888);
                var4.glPopMatrix();
            }
        }
    }

    private void renderFeedback(DrawContext var1, GL var2) {
        var2.glColor3f(this.feedback, this.feedback, this.feedback);
        this.drawRTQuad(var1);
    }

    private void renderEmissiveMaterial(RenderContext var1) {
        DrawContext var2 = var1.getDc();
        /**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
         * седержит методы для рисования примитивов (линии треугольники)*/
        GL var3 = var2.getGL();
        var3.glViewport(0, 0, 256, 256);
        var3.glEnable(3089);
        var3.glScissor(0, 0, 256, 256);
        var3.glClearColor(0.0F, 0.0F, 0.0F, 0.0F);
        var3.glClear(17664);
        var3.glDisable(3089);
        var3.glColor4f(this.glowIntensity, this.glowIntensity, this.glowIntensity, this.glowIntensity);
        var1.getCamera().updateGLProjection(var2);
        var1.getDc().gpuCamTransform.a((Matrix3f) var1.getCamera().getGlobalTransform().mX);
        var2.gpuCamTransform.p(0.0F, 0.0F, 0.0F);
        var1.getDc().gpuCamAffine.k(var2.gpuCamTransform);
        var3.glMatrixMode(5888);
        var3.glLoadIdentity();
        var2.tempBuffer0.clear();
        var1.getDc().gpuCamAffine.asColumnMajorBuffer(var2.tempBuffer0);
        var2.tempBuffer0.flip();
        var3.glMultMatrixf(var2.tempBuffer0);
        var3.glEnableClientState(32884);
        var2.primitiveState.setStates(Collections.EMPTY_LIST);
        TreeRecordList var4 = var1.getRecords();
        var2.primitiveState.setUseColorArray(false);
        var2.primitiveState.setUseNormalArray(false);
        var2.primitiveState.setUseTangentArray(false);
        var2.currentPass = this.defaultShader.getPass(0);
        RenderStates var5 = this.defaultShader.getPass(0).getRenderStates();
        var5.fillPrimitiveStates(var2.primitiveState);
        var5.fillMaterialStates(var2.materialState);
        var5.blindlyUpdateGLStates(var2);
        Primitive var6 = null;
        Material var7 = null;

        for (int var8 = 0; var8 < var4.size(); ++var8) {
            TreeRecord var9 = var4.get(var8);
            RenderStates var10 = var9.shader.getPass(0).getRenderStates();
            if (!var10.isBlendEnabled() || var10.getDstBlend() == BlendType.ONE || var10.getDstBlend() == BlendType.ZERO) {
                if (var10.isAlphaTestEnabled() != var5.isAlphaTestEnabled()) {
                    var5 = var10;
                    if (var10.isAlphaTestEnabled()) {
                        var3.glEnable(3008);
                        var3.glAlphaFunc(var10.getAlphaFunc().glEquivalent(), var10.getAlphaRef());
                    } else {
                        var3.glDisable(3008);
                    }
                }

                if (var10.isBlendEnabled()) {
                    var3.glEnable(3042);
                    var3.glBlendFunc(var10.getSrcBlend().glEquivalent(), var10.getDstBlend().glEquivalent());
                } else {
                    var3.glDisable(3042);
                }

                if (var6 == null || var6.getHandle() != var9.primitive.getHandle()) {
                    if (var6 != null && var6.getUseVBO() && !var9.primitive.getUseVBO()) {
                        this.unbindPrimitive(var2);
                    }

                    var6 = var9.primitive;
                    var6.bind(var2);
                }

                if (var7 == null || var7.getHandle() != var9.material.getHandle()) {
                    var3.glMatrixMode(5890);
                    var9.material.bind(var2);
                    var3.glMatrixMode(5888);
                    var7 = var9.material;
                }

                var3.glPushMatrix();
                var10.updateDepthStates(var2);
                var2.tempBuffer0.clear();
                var9.gpuTransform.asColumnMajorBuffer(var2.tempBuffer0);
                var2.tempBuffer0.flip();
                var3.glMultMatrixf(var2.tempBuffer0);
                var6.draw(var2);
                var3.glPopMatrix();
            }
        }

        var3.glDisableClientState(32884);
        this.unbindPrimitive(var2);
        this.defaultStates.blindlyUpdateGLStates(var2);
    }

    private void unbindPrimitive(DrawContext var1) {
        var1.getGL().glBindBufferARB(34962, 0);
        var1.getGL().glBindBufferARB(34963, 0);
    }

    public void render(RenderContext var1, SceneView var2) {
        if (this.isSupported) {
            DrawContext var3 = var1.getDc();
            /**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
             * седержит методы для рисования примитивов (линии треугольники)*/
            GL var4 = var3.getGL();
            var4.glColor3f(1.0F, 1.0F, 1.0F);
            var4.glPushMatrix();
            var4.glLoadIdentity();
            var4.glMatrixMode(5889);
            var4.glPushMatrix();
            var4.glLoadIdentity();
            var4.glOrtho(0.0D, (double) var3.getScreenWidth(), 0.0D, (double) var3.getScreenHeight(), -1.0D, 1.0D);
            this.renderTarget.bindTexture(var3);
            var4.glBlendFunc(1, 1);
            var4.glDisable(2929);
            var4.glDepthMask(false);
            var4.glEnable(3042);
            var4.glBegin(GL.GL_QUADS);//Этот метод запускает процесс рисования. Рассматривает каждую группу из четырех вершин как независимый четырехугольник.
            var4.glTexCoord2f(0.0F, 0.0F);
            var4.glVertex2f((float) var3.getScreenOffsetX(), (float) var3.getScreenOffsetY());
            var4.glTexCoord2f(this.texCoord, 0.0F);
            var4.glVertex2f((float) (var3.getScreenOffsetX() + var3.getScreenWidth()), (float) var3.getScreenOffsetY());
            var4.glTexCoord2f(this.texCoord, this.texCoord);
            var4.glVertex2f((float) (var3.getScreenOffsetX() + var3.getScreenWidth()), (float) (var3.getScreenOffsetY() + var3.getScreenHeight()));
            var4.glTexCoord2f(0.0F, this.texCoord);
            var4.glVertex2f((float) var3.getScreenOffsetX(), (float) (var3.getScreenOffsetY() + var3.getScreenHeight()));
            var4.glEnd();
            var4.glDisable(this.renderTarget.getTexture().getTarget());
            var4.glDisable(3042);
            var4.glEnable(2929);
            var4.glDepthMask(true);
            var4.glPopMatrix();
            var4.glMatrixMode(5888);
            var4.glPopMatrix();
            var4.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            if (this.useFBO) {
                RenderTarget var5 = this.tempTarget;
                this.tempTarget = this.renderTarget;
                this.renderTarget = var5;
            }

        }
    }

    private void drawRTQuad(DrawContext var1) {
        /**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
         * седержит методы для рисования примитивов (линии треугольники)*/
        GL var2 = var1.getGL();
        var2.glBegin(GL.GL_QUADS);//Этот метод запускает процесс рисования. Рассматривает каждую группу из четырех вершин как независимый четырехугольник.
        var2.glTexCoord2f(0.0F, 0.0F);
        var2.glVertex2f(0.0F, 0.0F);
        var2.glTexCoord2f(this.texCoord, 0.0F);
        var2.glVertex2f(256.0F, 0.0F);
        var2.glTexCoord2f(this.texCoord, this.texCoord);
        var2.glVertex2f(256.0F, 256.0F);
        var2.glTexCoord2f(0.0F, this.texCoord);
        var2.glVertex2f(0.0F, 256.0F);
        var2.glEnd();
    }

    private void verticalBlur(RenderContext var1) {
        DrawContext var2 = var1.getDc();
        this.verProgram.bind(var2);
        this.verProgram.updateMaterialProgramAttribs(var2);
        this.drawRTQuad(var2);
        this.verProgram.unbind(var2);
    }

    private void horizontalBlur(RenderContext var1) {
        DrawContext var2 = var1.getDc();
        this.horProgram.bind(var2);
        this.horProgram.updateMaterialProgramAttribs(var2);
        this.drawRTQuad(var2);
        this.horProgram.unbind(var2);
    }

    public void releaseReferences() {
        if (this.renderTarget != null) {
            this.renderTarget.releaseReferences();
        }

        if (this.defaultShader != null) {
            this.defaultShader.releaseReferences();
        }

        if (this.verProgram != null) {
            this.verProgram.releaseReferences();
        }

        if (this.horProgram != null) {
            this.horProgram.releaseReferences();
        }

        this.renderTarget = null;
        this.verProgram = this.horProgram = null;
        this.defaultShader = null;
    }

    public RenderAsset cloneAsset() {
        return this;
    }
}
