package taikodom.render;

import taikodom.render.camera.Camera;
import taikodom.render.primitives.Light;
import taikodom.render.primitives.TreeRecord;

import javax.media.opengl.GL;

public class ShaderParametersRenderingContext {
    private TreeRecord treeRecord;
    private Light light;
    private DrawContext renderContext;
    private Camera camera;
    private GL gl;

    public GL gl() {
        return this.gl;
    }

    public TreeRecord getTreeRecord() {
        return this.treeRecord;
    }

    public void setTreeRecord(TreeRecord var1) {
        this.treeRecord = var1;
    }

    public Light getLight() {
        return this.light;
    }

    public void setLight(Light var1) {
        this.light = var1;
    }

    public DrawContext getRenderContext() {
        return this.renderContext;
    }

    public void setRenderContext(DrawContext var1) {
        this.renderContext = var1;
    }

    public Camera getCamera() {
        return this.camera;
    }

    public void setCamera(Camera var1) {
        this.camera = var1;
    }
}
