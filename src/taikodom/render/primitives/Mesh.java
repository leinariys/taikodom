package taikodom.render.primitives;

//import all.HO;
//import all.NP;
//import all.QZ;

import all.*;
import com.hoplon.geometry.Vec3f;
import taikodom.render.DrawContext;
import taikodom.render.data.IndexData;
import taikodom.render.data.VertexData;
import taikodom.render.enums.GeometryType;
import taikodom.render.loader.RenderAsset;

import java.nio.Buffer;

public class Mesh extends Primitive implements PrimitiveBase {
    private final aLH aabb = new aLH();
    protected VertexData data;
    protected IndexData indexes;
    protected GeometryType geometryType;
    // private QZ grannyMesh;
    private ato grannyBinding;
    private aqE grannyDeformer;
    private int grannyMaterialIndex;
    //  private NP grannySkeleton;
    private boolean rigid = true;

    public Mesh() {
        this.geometryType = GeometryType.TRIANGLES;
    }

    public Mesh(Mesh var1) {
        super(var1);
        this.geometryType = GeometryType.TRIANGLES;
        this.data = var1.data;
        this.indexes = var1.indexes.duplicate();
        this.rigid = var1.rigid;
        //    this.grannyMesh = var1.grannyMesh;
        this.grannyBinding = var1.grannyBinding;
        this.grannyDeformer = var1.grannyDeformer;
        //     this.grannySkeleton = var1.grannySkeleton;
    }

    public boolean isRidig() {
        return this.rigid;
    }

    public VertexData getVertexData() {
        return this.data;
    }

    public void setVertexData(VertexData var1) {
        this.data = var1;
    }

    public IndexData getIndexes() {
        return this.indexes;
    }

    public void setIndexes(IndexData var1) {
        this.indexes = var1;
    }

    public GeometryType getGeometryType() {
        return this.geometryType;
    }

    public void setGeometryType(GeometryType var1) {
        this.geometryType = var1;
    }

    public void bind(DrawContext var1) {
        if (var1.debugDraw()) {
            var1.logBind(this);
        }

        try {
            this.data.bind(var1);
            this.indexes.bind(var1);
            var1.incNumPrimitiveBind();
        } catch (Exception var4) {
            String var3 = var1.currentPass != null ? var1.currentPass.getName() : "unknown";
            System.err.println("Problem with mesh " + this.getName() + ". " + var4.getMessage() + ". Its using all shader that uses missing mesh properties: shader " + var3);
            throw new RuntimeException("Missing mesh properties. Need to_q export it again");
        }
    }

    public void draw(DrawContext var1) {
        if (var1.debugDraw()) {
            var1.logDraw(this);
        }

        if (this.indexes.size() > 0) {
            Buffer var2 = this.indexes.buffer();
            if (var2 != null) {
                var1.getGL().glDrawElements(this.geometryType.glEquivalent(), this.indexes.size(), this.indexes.getGLType(), var2);
            } else {
                var1.getGL().glDrawElements(this.geometryType.glEquivalent(), this.indexes.size(), this.indexes.getGLType(), 0L);
            }

            var1.incNumDrawCalls();
            var1.incNumTriangles(this.indexes.size() / 3);
        }

    }

    public RenderAsset cloneAsset() {
        return this;
    }

    /*
       public void setGrannyMesh(QZ var1) {
          this.grannyMesh = var1;
          this.rigid = HO.parseSelectors(var1);
       }

       public QZ getGrannyMesh() {
          return this.grannyMesh;
       }
    */
    public boolean getUseVBO() {
        return this.data.isUseVBO();
    }

    public void computeLocalAabb() {
        this.aabb.reset();
        Vec3f var1 = new Vec3f();
        if (this.indexes != null && this.data != null) {
            for (int var2 = 0; var2 < this.indexes.size(); ++var2) {
                this.data.getPosition(this.indexes.getIndex(var2), var1);
                this.aabb.addPoint(var1);
            }
        }

        if (!this.aabb.din().isValid()) {
            this.aabb.addPoint(var1);
        }

    }

    public aLH getAabb() {
        return this.aabb;
    }

    public gu_g rayIntersects(gu_g var1) {
        if (this.indexes != null && this.data != null) {
            Vec3f[] var2 = new Vec3f[]{new Vec3f(), new Vec3f(), new Vec3f()};
            aSH var3 = new aSH(var2[0], var2[1], var2[2]);

            for (int var4 = 0; var4 < this.indexes.size(); var4 += 3) {
                this.data.getPosition(this.indexes.getIndex(var4), var2[0]);
                this.data.getPosition(this.indexes.getIndex(var4 + 1), var2[1]);
                this.data.getPosition(this.indexes.getIndex(var4 + 2), var2[2]);
                var3.q(var2[0], var2[1], var2[2]);
                var3.rayIntersects(var1);
                if (var1.isIntersected()) {
                    return var1;
                }
            }
        }

        return var1;
    }

    public ato getGrannyBinding() {
        return this.grannyBinding;
    }

    public void setGrannyBinding(ato var1) {
        this.grannyBinding = var1;
    }

    public aqE getGrannyDeformer() {
        return this.grannyDeformer;
    }

    public void setGrannyDeformer(aqE var1) {
        this.grannyDeformer = var1;
    }

    public int getGrannyMaterialIndex() {
        return this.grannyMaterialIndex;
    }

    public void setGrannyMaterialIndex(int var1) {
        this.grannyMaterialIndex = var1;
    }
/*
   public NP getGrannySkeleton() {
      return this.grannySkeleton;
   }

   public void setGrannySkeleton(NP var1) {
      this.grannySkeleton = var1;
   }*/
}
