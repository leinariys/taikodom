package taikodom.render.primitives;

import all.gu_g;
import com.hoplon.geometry.Vec3f;
import taikodom.render.DrawContext;
import taikodom.render.loader.RenderAsset;

public abstract class Primitive extends RenderAsset {
    public Primitive() {
    }

    public Primitive(Primitive var1) {
        super(var1);
    }

    public abstract void bind(DrawContext var1);

    public abstract void draw(DrawContext var1);

    public abstract boolean getUseVBO();

    public gu_g rayIntersects(Vec3f var1, Vec3f var2) {
        gu_g var3 = new gu_g(var1, var2);
        return this.rayIntersects(var3);
    }

    public abstract gu_g rayIntersects(gu_g var1);
}
