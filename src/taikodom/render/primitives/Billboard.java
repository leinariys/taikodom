package taikodom.render.primitives;

import all.aSH;
import all.gu_g;
import com.hoplon.geometry.Vec3f;
import taikodom.render.DrawContext;
import taikodom.render.data.InterleavedVertexData;
import taikodom.render.data.VertexData;
import taikodom.render.data.VertexLayout;
import taikodom.render.loader.RenderAsset;

/**
 * рекламный щит
 */
public class Billboard extends Primitive implements PrimitiveBase {
    protected static final Vec3f p0 = new Vec3f(-0.5F, -0.5F, 0.0F);
    protected static final Vec3f p1 = new Vec3f(0.5F, -0.5F, 0.0F);
    protected static final Vec3f p2 = new Vec3f(0.5F, 0.5F, 0.0F);
    protected static final Vec3f p3 = new Vec3f(-0.5F, 0.5F, 0.0F);
    protected VertexData data;

    public Billboard() {
        this.data = new InterleavedVertexData(VertexLayout.defaultLayout, 4);
        this.data.setPosition(0, p0.x, p0.y, p0.z);
        this.data.setPosition(1, p1.x, p1.y, p1.z);
        this.data.setPosition(2, p2.x, p2.y, p2.z);
        this.data.setPosition(3, p3.x, p3.y, p3.z);
        this.data.setTexCoord(0, 0, 0.0F, 0.0F);
        this.data.setTexCoord(1, 0, 1.0F, 0.0F);
        this.data.setTexCoord(2, 0, 1.0F, 1.0F);
        this.data.setTexCoord(3, 0, 0.0F, 1.0F);
        this.data.setNormal(0, 0.0F, 0.0F, 1.0F);
        this.data.setNormal(1, 0.0F, 0.0F, 1.0F);
        this.data.setNormal(2, 0.0F, 0.0F, 1.0F);
        this.data.setNormal(3, 0.0F, 0.0F, 1.0F);
        this.data.setTangents(0, 1.0F, 0.0F, 0.0F, 1.0F);
        this.data.setTangents(1, 1.0F, 0.0F, 0.0F, 1.0F);
        this.data.setTangents(2, 1.0F, 0.0F, 0.0F, 1.0F);
        this.data.setTangents(3, 1.0F, 0.0F, 0.0F, 1.0F);
        this.data.setColor(0, 1.0F, 1.0F, 1.0F, 1.0F);
        this.data.setColor(1, 1.0F, 1.0F, 1.0F, 1.0F);
        this.data.setColor(2, 1.0F, 1.0F, 1.0F, 1.0F);
        this.data.setColor(3, 1.0F, 1.0F, 1.0F, 1.0F);
    }

    public void bind(DrawContext var1) {
        if (var1.debugDraw()) {
            var1.logBind(this);
        }

        this.data.bind(var1);
        var1.incNumPrimitiveBind();
    }

    public void draw(DrawContext var1) {
        if (var1.debugDraw()) {
            var1.logDraw(this);
        }

        var1.getGL().glDrawArrays(7, 0, 4);
        var1.incNumDrawCalls();
        var1.incNumTriangles(2);
    }

    public boolean getUseVBO() {
        return this.data.isUseVBO();
    }

    public RenderAsset cloneAsset() {
        return this;
    }

    public gu_g rayIntersects(gu_g var1) {
        aSH var2 = new aSH();
        var2.bS(p0);
        var2.bT(p1);
        var2.bU(p3);
        var2.rayIntersects(var1);
        if (var1.isIntersected()) {
            return var1;
        } else {
            var2.bS(p0);
            var2.bT(p3);
            var2.bU(p1);
            var2.rayIntersects(var1);
            if (var1.isIntersected()) {
                return var1;
            } else {
                var2.bS(p1);
                var2.bT(p2);
                var2.bU(p3);
                var2.rayIntersects(var1);
                if (var1.isIntersected()) {
                    return var1;
                } else {
                    var2.bS(p1);
                    var2.bT(p3);
                    var2.bU(p2);
                    var2.rayIntersects(var1);
                    return var1;
                }
            }
        }
    }
}
