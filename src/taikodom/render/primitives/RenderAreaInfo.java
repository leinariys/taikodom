package taikodom.render.primitives;

import com.hoplon.geometry.Color;
import taikodom.render.textures.Texture;

public class RenderAreaInfo {
    public final Color ambientColor = new Color(0.0F, 0.0F, 0.0F, 0.0F);
    public Texture diffuseCubemap;
    public Texture reflectiveCubemap;
}
