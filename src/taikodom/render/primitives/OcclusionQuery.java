package taikodom.render.primitives;

import taikodom.render.gl.GLSupportedCaps;

import javax.media.opengl.GL;

public class OcclusionQuery {
    private int queryId = -1;
    private boolean initialized = false;

    public void begin(GL var1) {
        if (this.initialized && this.queryId != -1) {
            var1.glBeginQuery(35092, this.queryId);
        }

    }

    public void end(GL var1) {
        if (this.initialized && this.queryId != -1) {
            var1.glEndQuery(35092);
        }

    }

    public float getResult(float var1, GL var2) {
        if ((!this.initialized || this.queryId == -1 || !var2.glIsQuery(this.queryId)) && !this.create(var2)) {
            return 0.0F;
        } else {
            int[] var3 = new int[1];
            var2.glGetQueryObjectiv(this.queryId, 34918, var3, 0);
            return (float) var3[0] / var1;
        }
    }

    private boolean create(GL var1) {
        if (this.initialized) {
            return this.queryId != -1;
        } else {
            this.initialized = true;
            if (!GLSupportedCaps.isOcclusionQuery()) {
                return false;
            } else {
                int[] var2 = new int[1];
                var1.glGenQueries(1, var2, 0);
                this.queryId = var2[0];
                var1.glBeginQuery(35092, this.queryId);
                var1.glEndQuery(35092);
                if (!var1.glIsQuery(this.queryId)) {
                    this.queryId = -1;
                }

                return this.queryId != -1;
            }
        }
    }

    public void dispose(GL var1) {
        int[] var2 = new int[]{this.queryId};
        var1.glDeleteQueries(1, var2, 0);
    }
}
