package taikodom.render.primitives;

public interface PrimitiveBase {
    boolean getUseVBO();
}
