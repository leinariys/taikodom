package taikodom.render.primitives;

import all.Vector3dOperations;
import all.aVS;
import all.aip;
import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import taikodom.render.DrawContext;
import taikodom.render.RenderContext;
import taikodom.render.camera.Camera;
import taikodom.render.enums.LightType;
import taikodom.render.loader.RenderAsset;

import javax.media.opengl.GL;
import javax.vecmath.Tuple3d;

public class Light extends RenderAsset {
    protected final Color diffuseColor;
    protected final Color specularColor;
    protected final Vec3f direction;
    protected final Vec3f globalDirection;
    protected final Color primitiveColor;
    private final aVS boundingSphere;
    protected LightType type;
    protected float exponent;
    protected float cutoff;
    protected boolean isMainLight;
    int scRight;
    int scLeft;
    int scBottom;
    int scTop;
    private Vector3dOperations position;
    private float radius;
    private float spotExponent;
    private float cosCutoff;
    private double currentDistanceToCamera;

    public Light() {
        this.type = LightType.POINT_LIGHT;
        this.diffuseColor = new Color(1.0F, 1.0F, 1.0F, 1.0F);
        this.specularColor = new Color(1.0F, 1.0F, 1.0F, 1.0F);
        this.direction = new Vec3f(0.0F, 0.0F, 1.0F);
        this.globalDirection = new Vec3f(1.0F, 1.0F, 1.0F);
        this.primitiveColor = new Color(1.0F, 1.0F, 1.0F, 1.0F);
        this.position = new Vector3dOperations(0.0D, 0.0D, 0.0D);
        this.boundingSphere = new aVS();
    }

    public Light(Light var1) {
        this.type = LightType.POINT_LIGHT;
        this.diffuseColor = new Color(1.0F, 1.0F, 1.0F, 1.0F);
        this.specularColor = new Color(1.0F, 1.0F, 1.0F, 1.0F);
        this.direction = new Vec3f(0.0F, 0.0F, 1.0F);
        this.globalDirection = new Vec3f(1.0F, 1.0F, 1.0F);
        this.primitiveColor = new Color(1.0F, 1.0F, 1.0F, 1.0F);
        this.position = new Vector3dOperations(0.0D, 0.0D, 0.0D);
        this.boundingSphere = new aVS();
        this.setRadius(var1.radius);
        this.type = var1.type;
        this.exponent = var1.exponent;
        this.cutoff = var1.cutoff;
        this.isMainLight = var1.isMainLight;
        this.primitiveColor.set(var1.primitiveColor);
        this.diffuseColor.set(var1.diffuseColor);
        this.specularColor.set(var1.specularColor);
        this.direction.set(var1.direction);
        this.globalDirection.set(var1.globalDirection);
        this.boundingSphere.ag((double) this.radius);
    }

    public Vector3dOperations getPosition() {
        return this.position;
    }

    public void setPosition(Vector3dOperations var1) {
        this.position.aA(var1);
        this.boundingSphere.h(var1);
    }

    public float getRadius() {
        return this.radius;
    }

    public void setRadius(float var1) {
        this.radius = var1;
        this.boundingSphere.ag((double) var1);
    }

    public float getCosCutoff() {
        return this.cosCutoff;
    }

    public float getSpotExponent() {
        return this.spotExponent;
    }

    public boolean setupScissorRectangle(RenderContext var1) {
        DrawContext var2 = var1.getDc();
        /**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
         * седержит методы для рисования примитивов (линии треугольники)*/
        GL var3 = var2.getGL();
        double var4 = -1.0D;
        double var6 = 1.0D;
        double var8 = -1.0D;
        double var10 = 1.0D;
        if (this.type == LightType.DIRECTIONAL_LIGHT) {
            var3.glDisable(3089);
            return true;
        } else {
            var3.glEnable(3089);
            if (this.radius == 0.0F) {
                return false;
            } else {
                Vector3dOperations var12 = var1.vec3dTemp0;
                var12.sub(this.position, var1.getGpuOffset());
                var1.cameraAffineTransform.b((Tuple3d) var12, (Tuple3d) var12);
                Camera var13 = var1.getCamera();
                double var14 = (double) var13.getNearPlane() * aip.O((double) var13.getFovY() * 0.5D);
                double var16 = var14 * (double) var13.getAspect();
                Vector3dOperations var18 = var1.vec3dTemp1;
                double var19 = var12.z * var12.z * (var12.x * var12.x + var12.z * var12.z - (double) (this.radius * this.radius));
                double var21;
                int var23;
                Vector3dOperations var24;
                double var25;
                if (var19 > 0.0D) {
                    var21 = Math.sqrt(var19);

                    for (var23 = 0; var23 < 2; ++var23) {
                        if (var23 == 0) {
                            var18.x = (double) this.radius * var12.x + var21;
                        } else {
                            var18.x = (double) this.radius * var12.x - var21;
                        }

                        var18.x /= var12.x * var12.x + var12.z * var12.z;
                        var18.z = (double) this.radius - var18.x * var12.x;
                        var18.z /= var12.z;
                        if (var18.x != 0.0D) {
                            var24 = var1.vec3dTemp2;
                            var24.z = var12.x * var12.x + var12.z * var12.z - (double) (this.radius * this.radius);
                            var24.z /= var12.z - var18.z / var18.x * var12.x;
                            if (var24.z < 0.0D) {
                                var24.x = -var24.z * var18.z / var18.x;
                                var25 = var18.z * (double) var13.getNearPlane() / (var18.x * var16);
                                if (var24.x < var12.x && var25 > var4) {
                                    var4 = var25;
                                }

                                if (var24.x > var12.x && var25 < var6) {
                                    var6 = var25;
                                }
                            }
                        }
                    }
                }

                var18.x = 0.0D;
                var19 = var12.z * var12.z * (var12.y * var12.y + var12.z * var12.z - (double) (this.radius * this.radius));
                if (var19 > 0.0D) {
                    var21 = Math.sqrt(var19);

                    for (var23 = 0; var23 < 2; ++var23) {
                        if (var23 == 0) {
                            var18.y = (double) this.radius * var12.y + var21;
                        } else {
                            var18.y = (double) this.radius * var12.y - var21;
                        }

                        var18.y /= var12.y * var12.y + var12.z * var12.z;
                        var18.z = (double) this.radius - var18.y * var12.y;
                        var18.z /= var12.z;
                        if (var18.y != 0.0D) {
                            var24 = var1.vec3dTemp2;
                            var24.z = var12.y * var12.y + var12.z * var12.z - (double) (this.radius * this.radius);
                            var24.z /= var12.z - var18.z / var18.y * var12.y;
                            if (var24.z < 0.0D) {
                                var24.y = -var24.z * var18.z / var18.y;
                                var25 = var18.z * (double) var13.getNearPlane() / (var18.y * var14);
                                if (var24.y < var12.y && var25 > var8) {
                                    var8 = var25;
                                }

                                if (var24.y > var12.y && var25 < var10) {
                                    var10 = var25;
                                }
                            }
                        }
                    }
                }

                int var27 = (int) ((double) var2.getScreenWidth() * (var4 + 1.0D) / 2.0D);
                int var22 = (int) ((double) var2.getScreenHeight() * (var8 + 1.0D) / 2.0D);
                var23 = (int) ((double) var2.getScreenWidth() * (var6 - var4) / 2.0D);
                int var28 = (int) ((double) var2.getScreenHeight() * (var10 - var8) / 2.0D);
                if (var27 < 0) {
                    var27 = 0;
                } else if (var27 > var2.getScreenWidth() - 1) {
                    return false;
                }

                if (var22 < 0) {
                    var22 = 0;
                } else if (var22 > var2.getScreenHeight() - 1) {
                    return false;
                }

                if (var23 < 0) {
                    var23 = 0;
                } else if (var27 + var23 > var2.getScreenWidth() - 1) {
                    var23 = var2.getScreenWidth() - 1 - var27;
                }

                if (var28 < 0) {
                    var28 = 0;
                } else if (var22 + var28 > var2.getScreenHeight() - 1) {
                    var28 = var2.getScreenHeight() - 1 - var22;
                }

                this.scLeft = var27 + var2.getScreenOffsetX();
                this.scRight = var27 + var23;
                this.scBottom = var22 + var2.getScreenOffsetY();
                this.scTop = var22 + var28;
                var3.glScissor(var27 + var2.getScreenOffsetX(), var22 + var2.getScreenOffsetY(), var23, var28);
                return true;
            }
        }
    }

    public LightType getLightType() {
        return this.type;
    }

    public aVS getBoundingSphere() {
        return this.boundingSphere;
    }

    public RenderAsset cloneAsset() {
        return new Light(this);
    }

    public LightType getType() {
        return this.type;
    }

    public void setType(LightType var1) {
        this.type = var1;
    }

    public Color getDiffuseColor() {
        return this.diffuseColor;
    }

    public void setDiffuseColor(Color var1) {
        this.diffuseColor.set(var1);
    }

    public Color getSpecularColor() {
        return this.specularColor;
    }

    public void setSpecularColor(Color var1) {
        this.specularColor.set(var1);
    }

    public Vec3f getDirection() {
        return this.direction;
    }

    public void setDirection(Vec3f var1) {
        this.direction.set(var1);
    }

    public Vec3f getGlobalDirection() {
        return this.globalDirection;
    }

    public void setGlobalDirection(Vec3f var1) {
        this.globalDirection.set(var1);
    }

    public float getExponent() {
        return this.exponent;
    }

    public void setExponent(float var1) {
        this.exponent = var1;
    }

    public float getCutoff() {
        return this.cutoff;
    }

    public void setCutoff(float var1) {
        this.cutoff = var1;
    }

    public Color getPrimitiveColor() {
        return this.primitiveColor;
    }

    public void setPrimitiveColor(Color var1) {
        this.primitiveColor.set(var1);
    }

    public boolean isMainLight() {
        return this.isMainLight;
    }

    public void setMainLight(boolean var1) {
        this.isMainLight = var1;
    }

    public double getCurrentDistanceToCamera() {
        return this.currentDistanceToCamera;
    }

    public void setCurrentDistanceToCamera(double var1) {
        this.currentDistanceToCamera = var1;
    }
}
