package taikodom.render.primitives;

import all.aLH;
import all.ajK;
import all.bc_q;
import com.hoplon.geometry.Color;
import taikodom.render.scene.RenderObject;
import taikodom.render.shaders.Shader;
import taikodom.render.textures.Material;

public class TreeRecord {
    public final ajK gpuTransform = new ajK();
    public final bc_q transform = new bc_q();
    public final aLH cameraSpaceBoundingBox = new aLH();
    public final Color color = new Color(1.0F, 1.0F, 1.0F, 1.0F);
    public Shader shader;
    public Material material;
    public Primitive primitive;
    public OcclusionQuery occlusionQuery;
    public int entryPriority;
    public double squaredDistanceToCamera;
    public RenderObject renderObject;
    public boolean isGuiItem;
    public RenderAreaInfo areaInfo;
}
