package taikodom.render;

public interface Video {
    void update(DrawContext var1);

    void render(DrawContext var1);

    void play();

    void stop();

    boolean isEnded();
}
