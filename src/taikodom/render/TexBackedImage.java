package taikodom.render;

import taikodom.render.textures.BaseTexture;

import java.awt.*;
import java.awt.image.ImageObserver;
import java.awt.image.ImageProducer;

public class TexBackedImage extends Image {
    private int height;
    private int width;
    private BaseTexture texture;
    private float xmul;
    private float ymul;

    public TexBackedImage() {
        this.width = 0;
        this.height = 0;
        this.xmul = 1.0F;
        this.ymul = 1.0F;
    }

    public TexBackedImage(int var1, int var2, BaseTexture var3) {
        this.height = var1;
        this.width = var2;
        this.texture = var3;
        this.xmul = 1.0F;
        this.ymul = 1.0F;
    }

    public TexBackedImage(int var1, int var2, float var3, float var4, BaseTexture var5) {
        this.height = var1;
        this.width = var2;
        this.xmul = var3;
        this.ymul = var4;
        this.texture = var5;
    }

    public Graphics getGraphics() {
        return null;
    }

    public int getHeight(ImageObserver var1) {
        return this.height;
    }

    public Object getProperty(String var1, ImageObserver var2) {
        return null;
    }

    public ImageProducer getSource() {
        return null;
    }

    public int getWidth(ImageObserver var1) {
        return this.width;
    }

    public BaseTexture getTexture() {
        return this.texture;
    }

    public void setTexture(BaseTexture var1) {
        this.texture = var1;
    }

    public void flush() {
    }

    public float getXmul() {
        return this.xmul;
    }

    public float getYmul() {
        return this.ymul;
    }

    public int getHeight() {
        return this.height;
    }

    public void setHeight(int var1) {
        this.height = var1;
    }

    public int getWidth() {
        return this.width;
    }

    public void setWidth(int var1) {
        this.width = var1;
    }
}
