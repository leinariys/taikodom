package taikodom.render.impostor;

import all.aQKa;
import all.aip;
import com.hoplon.geometry.Color;
import taikodom.render.DrawContext;
import taikodom.render.textures.Material;
import taikodom.render.textures.SimpleTexture;

import java.util.ArrayList;

public class DynamicTextureAtlas {
    SimpleTexture atlasTexture;
    Material atlasMaterial;
    ArrayList[] freeSlots;
    ArrayList usedSlots = new ArrayList();
    private int atlasSize;
    private int minTextureSize;
    private int maxTextureSize;

    public DynamicTextureAtlas(int var1, int var2, int var3, DrawContext var4) {
        this.atlasSize = var1;
        this.minTextureSize = var3;
        this.maxTextureSize = var2;
        this.init(var4);
    }

    private void init(DrawContext var1) {
        this.atlasTexture = new SimpleTexture();
        this.atlasTexture.createEmptyRGBA(this.atlasSize, this.atlasSize, 32);
        this.atlasTexture.fillWithColor(new Color(0.0F, 0.0F, 0.0F, 0.0F));
        int var2 = aip.rb(this.maxTextureSize) - aip.rb(this.minTextureSize) + 1;
        this.freeSlots = new ArrayList[var2];
        int var3 = 0;
        int var4 = this.atlasSize / 2;
        int var5 = this.maxTextureSize;

        for (int var6 = 0; var6 < var2; ++var6) {
            this.freeSlots[var6] = new ArrayList();
            int var7 = this.atlasSize / var5;
            int var8 = var4 / var5;

            for (int var9 = 0; var9 < var7; ++var9) {
                for (int var10 = 0; var10 < var8; ++var10) {
                    int var11 = var3 + var10 * var5;
                    int var12 = var9 * var5;
                    aQKa var13 = new aQKa((float) var11 / (float) this.atlasSize, (float) var12 / (float) this.atlasSize);
                    aQKa var14 = var13.g((double) ((float) var5 / (float) this.atlasSize), (double) ((float) var5 / (float) this.atlasSize));
                    DynamicTextureAtlas.MaterialSlot var15 = new DynamicTextureAtlas.MaterialSlot(var11, var12, var13, var14, var5);
                    this.freeSlots[var6].add(var15);
                }
            }
            var3 += var4;
            var5 /= 2;
            var4 /= 2;
        }
        this.atlasMaterial = new Material();
        this.atlasMaterial.setDiffuseTexture(this.atlasTexture);
        this.atlasMaterial.setName("Atlas material " + this.atlasSize);
    }

    private int listForResolution(int var1) {
        int var2 = this.maxTextureSize / var1;
        return aip.rb(var2);
    }

    public boolean hasFreeSlot(int var1) {
        int var2 = this.listForResolution(var1);
        return this.freeSlots[var2].size() > 0;
    }

    public DynamicTextureAtlas.MaterialSlot getFreeSlot(int var1) {
        int var2 = this.listForResolution(var1);
        if (this.freeSlots[var2].size() > 0) {
            DynamicTextureAtlas.MaterialSlot var3 = (DynamicTextureAtlas.MaterialSlot) this.freeSlots[var2].remove(this.freeSlots[var2].size() - 1);
            this.usedSlots.add(var3);
            return var3;
        } else {
            return null;
        }
    }

    public void pushBackSlot(DynamicTextureAtlas.MaterialSlot var1) {
        if (!this.usedSlots.remove(var1)) {
            throw new RuntimeException("Releasing all material to_q an wrong atlas!");
        } else {
            int var2 = this.listForResolution(var1.textureSize);
            this.freeSlots[var2].add(var1);
        }
    }

    public int getAtlasSize() {
        return this.atlasSize;
    }

    public SimpleTexture getAtlasTexture() {
        return this.atlasTexture;
    }

    public Material getAtlasMaterial() {
        return this.atlasMaterial;
    }

    public void releaseReferences() {
        if (this.freeSlots != null) {
            ArrayList[] var4 = this.freeSlots;
            int var3 = this.freeSlots.length;

            for (int var2 = 0; var2 < var3; ++var2) {
                ArrayList var1 = var4[var2];
                var1.clear();
            }

            this.freeSlots = null;
        }

        if (this.usedSlots != null) {
            this.usedSlots.clear();
            this.usedSlots = null;
        }

        if (this.atlasTexture != null) {
            this.atlasTexture.releaseReferences();
            this.atlasTexture = null;
        }

        if (this.atlasMaterial != null) {
            this.atlasMaterial.releaseReferences();
            this.atlasMaterial = null;
        }

    }

    public class MaterialSlot {
        protected final aQKa minCoordinates = new aQKa();
        protected final aQKa maxCoordinates = new aQKa();
        protected int x;
        protected int y;
        protected int textureSize;

        public MaterialSlot(int var2, int var3, aQKa var4, aQKa var5, int var6) {
            this.x = var2;
            this.y = var3;
            this.minCoordinates.set(var4);
            this.maxCoordinates.set(var5);
            this.textureSize = var6;
        }
    }
}
