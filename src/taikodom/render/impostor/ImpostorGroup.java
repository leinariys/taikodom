package taikodom.render.impostor;

import all.Vector3dOperations;
import taikodom.render.RenderContext;
import taikodom.render.data.ExpandableIndexData;
import taikodom.render.data.ExpandableVertexData;
import taikodom.render.data.VertexLayout;
import taikodom.render.enums.GeometryType;
import taikodom.render.primitives.Mesh;
import taikodom.render.scene.RenderObject;
import taikodom.render.textures.Material;

import java.util.*;

public class ImpostorGroup extends RenderObject {
    static final ArrayList toBeRemoved = new ArrayList();
    private DynamicTextureAtlas texAtlas;
    private Mesh mesh;
    private ExpandableVertexData vertexes;
    private ExpandableIndexData indexes;
    private List impostors = new ArrayList();
    private boolean dirty = false;
    private boolean recalculateCenter = false;

    public ImpostorGroup() {
        this.initRenderStructs();
    }

    public void releaseReferences() {
        super.releaseReferences();
        this.removeAllImpostors();
        this.texAtlas = null;
    }

    private void initRenderStructs() {
        VertexLayout var1 = new VertexLayout(true, true, false, false, 1);
        this.vertexes = new ExpandableVertexData(var1, 300);
        this.indexes = new ExpandableIndexData(300);
        this.mesh = new Mesh();
        this.mesh.setName("ImpostorGroup_mesh");
        this.mesh.setGeometryType(GeometryType.QUADS);
        this.mesh.setVertexData(this.vertexes);
        this.mesh.setIndexes(this.indexes);
    }

    public void removeImpostor(Impostor var1) {
        if (this.impostors.remove(var1)) {
            this.texAtlas.pushBackSlot(var1.slot);
        } else {
            System.out.println("Impostor removed from an wrong group!!");
        }

        this.dirty = true;
    }

    public void addImpostor(Impostor var1) {
        if (this.impostors.size() == 0) {
            this.recalculateCenter = true;
        }

        this.impostors.add(var1);
        this.dirty = true;
    }

    private void recalculateCenter() {
        this.recalculateCenter = false;
        float var1 = 1.0F / (float) this.impostors.size();
        Vector3dOperations var2 = new Vector3dOperations();
        Iterator var4 = this.impostors.iterator();

        while (var4.hasNext()) {
            Impostor var3 = (Impostor) var4.next();
            var2.add(var3.position.Z((double) var1));
        }

        this.setPosition(var2);
    }

    public void regenerateMesh() {
        Collections.sort(this.impostors, new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                return a((Impostor) o1, (Impostor) o2);
            }

            public int a(Impostor var1, Impostor var2) {
                if (var1.getCurrentDistanceToCameraPlane() > var2.getCurrentDistanceToCameraPlane()) {
                    return -1;
                } else {
                    return var1.getCurrentDistanceToCameraPlane() < var2.getCurrentDistanceToCameraPlane() ? 1 : 0;
                }
            }
        });
        this.indexes.ensure(this.impostors.size() * 4);
        this.vertexes.ensure(this.impostors.size() * 4);
        this.indexes.clear();
        if (this.recalculateCenter) {
            this.recalculateCenter();
        }

        int var4 = 0;

        for (Iterator var6 = this.impostors.iterator(); var6.hasNext(); ++var4) {
            Impostor var5 = (Impostor) var6.next();
            DynamicTextureAtlas.MaterialSlot var7 = var5.getSlot();
            var5.indexStart = var4;
            this.indexes.setIndex(var4, var4);
            float var1 = (float) (var5.getVert0().x - this.globalTransform.position.x);
            float var2 = (float) (var5.getVert0().y - this.globalTransform.position.y);
            float var3 = (float) (var5.getVert0().z - this.globalTransform.position.z);
            this.vertexes.setPosition(var4, var1, var2, var3);
            this.vertexes.setTexCoord(var4, 0, var7.minCoordinates.x, var7.minCoordinates.y);
            this.vertexes.setColor(var4, var5.currentColor, var5.currentColor, var5.currentColor, var5.currentColor);
            ++var4;
            this.indexes.setIndex(var4, var4);
            var1 = (float) (var5.getVert1().x - this.globalTransform.position.x);
            var2 = (float) (var5.getVert1().y - this.globalTransform.position.y);
            var3 = (float) (var5.getVert1().z - this.globalTransform.position.z);
            this.vertexes.setPosition(var4, var1, var2, var3);
            this.vertexes.setTexCoord(var4, 0, var7.maxCoordinates.x, var7.minCoordinates.y);
            this.vertexes.setColor(var4, var5.currentColor, var5.currentColor, var5.currentColor, var5.currentColor);
            ++var4;
            this.indexes.setIndex(var4, var4);
            var1 = (float) (var5.getVert2().x - this.globalTransform.position.x);
            var2 = (float) (var5.getVert2().y - this.globalTransform.position.y);
            var3 = (float) (var5.getVert2().z - this.globalTransform.position.z);
            this.vertexes.setPosition(var4, var1, var2, var3);
            this.vertexes.setTexCoord(var4, 0, var7.maxCoordinates.x, var7.maxCoordinates.y);
            this.vertexes.setColor(var4, var5.currentColor, var5.currentColor, var5.currentColor, var5.currentColor);
            ++var4;
            this.indexes.setIndex(var4, var4);
            var1 = (float) (var5.getVert3().x - this.globalTransform.position.x);
            var2 = (float) (var5.getVert3().y - this.globalTransform.position.y);
            var3 = (float) (var5.getVert3().z - this.globalTransform.position.z);
            this.vertexes.setPosition(var4, var1, var2, var3);
            this.vertexes.setTexCoord(var4, 0, var7.minCoordinates.x, var7.maxCoordinates.y);
            this.vertexes.setColor(var4, var5.currentColor, var5.currentColor, var5.currentColor, var5.currentColor);
        }

        this.indexes.setSize(var4);
        this.dirty = false;
        this.mesh.computeLocalAabb();
        this.localAabb.g(this.mesh.getAabb());
        this.localAabb.a(this.globalTransform, this.aabbWorldSpace);
    }

    public boolean render(RenderContext var1, boolean var2) {
        if (this.render) {
            if (this.dirty) {
                this.regenerateMesh();
            }

            if (!super.render(var1, var2)) {
                return false;
            } else if (this.mesh.getIndexes().size() == 0) {
                return true;
            } else {
                if (!var2 || var1.getFrustum().a(this.aabbWorldSpace)) {
                    var1.getDc().incNumImpostorsRendered();
                    var1.addPrimitive(this.texAtlas.getAtlasMaterial(), this.mesh, this.globalTransform, this.primitiveColor, this.occlusionQuery, this.renderPriority, this, this.aabbWorldSpace, this.currentSquaredDistanceToCamera);
                    var1.incMeshesRendered();
                }

                return true;
            }
        } else {
            return false;
        }
    }

    public DynamicTextureAtlas.MaterialSlot getMaterialSlot(int var1) {
        return this.texAtlas.getFreeSlot(var1);
    }

    public DynamicTextureAtlas getTexAtlas() {
        return this.texAtlas;
    }

    public void setTexAtlas(DynamicTextureAtlas var1) {
        this.texAtlas = var1;
    }

    public Material getMaterial() {
        return this.texAtlas.atlasMaterial;
    }

    public void setDirty(boolean var1) {
        this.dirty = var1;
    }

    public void removeInvalidImpostors(RenderContext var1) {
        Iterator var3 = this.impostors.iterator();

        Impostor var2;
        while (var3.hasNext()) {
            var2 = (Impostor) var3.next();
            double var4 = var2.position.ax(var1.getCamera().getGlobalTransform().position);
            if (var4 / var2.getCurrentDistanceToCameraPlane() > 2.0D) {
                toBeRemoved.add(var2);
            }
        }

        var3 = toBeRemoved.iterator();

        while (var3.hasNext()) {
            var2 = (Impostor) var3.next();
            var2.clearData();
        }

        if (toBeRemoved.size() != 0) {
            toBeRemoved.clear();
            this.dirty = true;
        }

    }

    public void regenerateMeshColor(Impostor var1) {
        this.vertexes.setColor(var1.indexStart, var1.currentColor, var1.currentColor, var1.currentColor, var1.currentColor);
        this.vertexes.setColor(var1.indexStart + 1, var1.currentColor, var1.currentColor, var1.currentColor, var1.currentColor);
        this.vertexes.setColor(var1.indexStart + 2, var1.currentColor, var1.currentColor, var1.currentColor, var1.currentColor);
        this.vertexes.setColor(var1.indexStart + 3, var1.currentColor, var1.currentColor, var1.currentColor, var1.currentColor);
    }

    public void setRecalculateCenter(boolean var1) {
        this.recalculateCenter = var1;
    }

    public int getImpostorsCount() {
        return this.impostors.size();
    }

    private void removeAllImpostors() {
        toBeRemoved.addAll(this.impostors);
        Iterator var2 = toBeRemoved.iterator();

        while (var2.hasNext()) {
            Impostor var1 = (Impostor) var2.next();
            var1.clearData();
        }

        toBeRemoved.clear();
        this.impostors.clear();
    }
}
