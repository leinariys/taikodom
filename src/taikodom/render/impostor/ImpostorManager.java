package taikodom.render.impostor;

import all.Vector3dOperations;
import all.bc_q;
import com.hoplon.geometry.Vec3f;
import taikodom.render.DrawContext;
import taikodom.render.RenderContext;
import taikodom.render.RenderView;
import taikodom.render.SceneView;
import taikodom.render.camera.Camera;
import taikodom.render.enums.BlendType;
import taikodom.render.enums.DepthFuncType;
import taikodom.render.scene.LightRecordList;
import taikodom.render.scene.SceneObject;
import taikodom.render.shaders.Shader;
import taikodom.render.shaders.ShaderPass;
import taikodom.render.textures.ChannelInfo;
import taikodom.render.textures.PBuffer;
import taikodom.render.textures.SimpleTexture;

import javax.media.opengl.GL;
import javax.media.opengl.GLContext;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Дискуссионный менеджер
 */
public class ImpostorManager {
    protected static final int MAX_IMPOSTOR_ATLAS_SIZE = 512;
    protected static final int MAX_IMPOSTOR_RESOLUTION = 64;
    protected static final int MIN_IMPOSTOR_RESOLUTION = 2;
    protected static final int MAX_IMPOSTORS_GENERATED_FRAME = 10;
    private static final float INTERVAL_TO_CHECK_SEC = 30.0F;
    private List impostorGroups = new ArrayList();
    private List impostorsToUpdate = new ArrayList();
    private List releasedAtlas = new ArrayList();
    private PBuffer pbuffer;
    private Shader impostorShader = new Shader();
    private RenderView impostorRenderView;
    private SceneView impostorSceneView;
    private Camera impostorCamera;
    private float currentElapsedTimeSec = 0.0F;

    public ImpostorManager(GLContext var1) {
        this.impostorShader.setName("Impostor shader");
        ShaderPass var2 = new ShaderPass();
        var2.setName("Default Impostor pass1");
        ChannelInfo var3 = new ChannelInfo();
        var3.setTexChannel(8);
        var2.setChannelTextureSet(0, var3);
        var2.setColorArrayEnabled(true);
        var2.setAlphaTestEnabled(true);
        var2.setCullFaceEnabled(true);
        var2.setAlphaRef(0.95F);
        var2.setDstBlend(BlendType.ONE_MINUS_SRC_ALPHA);
        var2.setSrcBlend(BlendType.ONE);
        var2.setBlendEnabled(true);
        ShaderPass var4 = new ShaderPass();
        var4.setName("Default Impostor pass2");
        var4.setChannelTextureSet(0, var3);
        var4.setBlendEnabled(true);
        var4.setColorArrayEnabled(true);
        var4.setAlphaTestEnabled(true);
        var4.setDepthFunc(DepthFuncType.LESS);
        var4.setDstBlend(BlendType.ONE_MINUS_SRC_ALPHA);
        var4.setSrcBlend(BlendType.ONE);
        var4.setCullFaceEnabled(true);
        this.impostorShader.addPass(var2);
        this.impostorShader.addPass(var4);
        this.impostorRenderView = new RenderView(var1);
        this.impostorRenderView.getDrawContext().setCurrentFrame(1000L);
        this.impostorRenderView.getRenderContext().setCullOutSmallObjects(false);
        this.impostorRenderView.getRenderInfo().setClearColor(0.0F, 0.0F, 0.0F, 0.0F);
        this.impostorSceneView = new SceneView();
        this.impostorSceneView.setClearColorBuffer(true);
        this.impostorCamera = this.impostorSceneView.getCamera();
    }

    public ImpostorGroup getImpostorGroupForSize(int var1, RenderContext var2) {
        Iterator var4 = this.impostorGroups.iterator();
        ImpostorGroup var3;
        while (var4.hasNext()) {
            var3 = (ImpostorGroup) var4.next();
            if (var3.getTexAtlas().hasFreeSlot(var1)) {
                return var3;
            }
        }

        var3 = new ImpostorGroup();
        DynamicTextureAtlas var5 = new DynamicTextureAtlas(MAX_IMPOSTOR_ATLAS_SIZE, MAX_IMPOSTOR_RESOLUTION, MIN_IMPOSTOR_RESOLUTION, var2.getDc());
        var5.getAtlasMaterial().setShader(this.impostorShader);
        var3.setTexAtlas(var5);
        this.impostorGroups.add(var3);
        return var3;
    }

    public void addImpostor(SceneObject var1) {
        this.impostorsToUpdate.add(var1);
    }

    public void updateImpostors(RenderContext var1, SceneView var2) {
        this.currentElapsedTimeSec += var1.getDeltaTime();
        if (this.currentElapsedTimeSec > INTERVAL_TO_CHECK_SEC) {
            this.currentElapsedTimeSec = 0.0F;
            Iterator var4 = this.impostorGroups.iterator();
            while (var4.hasNext()) {
                ImpostorGroup var3 = (ImpostorGroup) var4.next();
                var3.removeInvalidImpostors(var1);
                if (var3.getImpostorsCount() == 0) {
                    var3.setRecalculateCenter(true);
                }
            }

            var4 = this.releasedAtlas.iterator();
            while (var4.hasNext()) {
                DynamicTextureAtlas var21 = (DynamicTextureAtlas) var4.next();
                var21.releaseReferences();
            }
            this.releasedAtlas.clear();
        }

        this.impostorSceneView.setSceneViewQuality(var2.getSceneViewQuality());
        this.impostorRenderView.getRenderContext().setSceneViewQuality(var2.getSceneViewQuality());
        this.impostorRenderView.getDrawContext().setMaxShaderQuality(var2.getSceneViewQuality().getShaderQuality());
        int var22 = this.impostorsToUpdate.size();
        if (var22 > 0) {
            DrawContext var23 = var1.getDc();
            if (this.pbuffer == null) {
                this.pbuffer = var23.getAuxPBuffer();
                this.pbuffer.activate(var23);
                /**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
                 * седержит методы для рисования примитивов (линии треугольники)*/
                GL var5 = var23.getGL();
                var5.glClear(16384);
                this.pbuffer.deactivate(var23);
                var23.getGlContext().makeCurrent();
            }

            Camera var25 = var1.getCamera();
            this.impostorCamera.setFarPlane(var25.getFarPlane());
            this.impostorCamera.setAspect(var25.getAspect());
            this.impostorCamera.setNearPlane(var25.getNearPlane());
            this.impostorCamera.setFovY(var25.getFovY());
            this.impostorCamera.setTransform(var25.getTransform());
            this.impostorCamera.calculateProjectionMatrix();
            bc_q var6 = this.impostorCamera.getTransform();
            var2.setCamera(this.impostorCamera);
            var1.setCamera(this.impostorCamera);
            Vec3f var7 = new Vec3f();
            Vec3f var8 = new Vec3f();
            Vec3f var9 = new Vec3f();
            Vector3dOperations var10 = new Vector3dOperations();
            Vector3dOperations var11 = new Vector3dOperations();
            this.pbuffer.activate(var23);
            /**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
             * седержит методы для рисования примитивов (линии треугольники)*/
            GL var12 = var23.getGL();
            RenderContext var13 = this.impostorRenderView.getRenderContext();
            var13.getLights().clear();
            var13.getDc().setMainLight(var23.getMainLight());
            var13.setGpuOffset(var1.getGpuOffset());
            var13.setMainLight(var23.getMainLight());

            int var14;
            for (var14 = 0; var14 < var1.getLights().size(); ++var14) {
                LightRecordList.LightRecord var15 = var1.getLights().get(var14);
                var13.addLight(var15.light);
            }

            for (var14 = 0; var14 < var22; ++var14) {
                SceneObject var27 = (SceneObject) this.impostorsToUpdate.get(var14);
                Impostor var16 = var27.getImpostor();
                if (var14 >= MAX_IMPOSTORS_GENERATED_FRAME) {
                    if (!var16.isTransition()) {
                        var27.render(var1, false);
                    }

                    var16.clearData();
                } else {
                    var10.add(var27.getAabbWorldSpace().din(), var27.getAabbWorldSpace().dim());
                    var10.scale(0.5D);
                    var7.sub(var25.getPosition(), var10);
                    var7.normalize();
                    var8.set(0.0F, 1.0F, 0.0F);
                    var9.cross(var8, var7);
                    var9.normalize();
                    var8.cross(var7, var9);
                    var6.setX(var9);
                    var6.setY(var8);
                    var6.setZ(var7);
                    this.impostorCamera.setTransform(var6);
                    if (var16.createImpostor(var1, var27, this)) {
                        var23.incNumImpostorsUpdated();
                        DynamicTextureAtlas.MaterialSlot var17 = var16.slot;
                        int var18 = -(var2.getViewport().width / 2 - var17.textureSize / 2);
                        int var19 = -(var2.getViewport().height / 2 - var17.textureSize / 2);
                        this.impostorRenderView.setViewport(var18, var19, var2.getViewport().width, var2.getViewport().height);
                        var13.getRecords().clear();
                        var13.setCamera(this.impostorCamera);
                        this.impostorRenderView.setupCamera(var2.getScene(), this.impostorCamera);
                        var27.render(var13, false);
                        this.impostorRenderView.renderEntries(this.impostorCamera);
                        SimpleTexture var20 = var16.getImpostorGroup().getTexAtlas().getAtlasTexture();
                        var20.copyFromCurrentBuffer(var12, var17.x, var17.y, 0, 0, var17.textureSize, var17.textureSize);
                        var2.getWindowCoords(var10, var11);
                    }
                }
            }

            this.pbuffer.deactivate(var23);
            var23.getGlContext().makeCurrent();
            var2.setCamera(var25);
            var1.setCamera(var25);
            this.impostorsToUpdate.clear();
        }

        Iterator var26 = this.impostorGroups.iterator();

        while (var26.hasNext()) {
            ImpostorGroup var24 = (ImpostorGroup) var26.next();
            var24.render(var1, true);
        }
    }

    public List getImpostorGroups() {
        return this.impostorGroups;
    }

    public void clear() {
        Iterator var2 = this.impostorGroups.iterator();
        while (var2.hasNext()) {
            ImpostorGroup var1 = (ImpostorGroup) var2.next();
            this.releasedAtlas.add(var1.getTexAtlas());
            var1.releaseReferences();
        }
        this.impostorGroups.clear();
    }
}
