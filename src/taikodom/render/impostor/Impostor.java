package taikodom.render.impostor;

import all.Vector3dOperations;
import all.aip;
import all.bc_q;
import all.te_w;
import com.hoplon.geometry.Vec3f;
import taikodom.render.RenderContext;
import taikodom.render.camera.Camera;
import taikodom.render.scene.SceneObject;

import javax.vecmath.Vector3f;

public class Impostor {
    private static final float IMPOSTOR_TOLERANCE = 0.96F;
    private static final float RESOLUTION_FACTOR = 0.15F;
    protected final Vector3dOperations vert0 = new Vector3dOperations();
    protected final Vector3dOperations vert1 = new Vector3dOperations();
    protected final Vector3dOperations vert2 = new Vector3dOperations();
    protected final Vector3dOperations vert3 = new Vector3dOperations();
    protected final Vector3dOperations position = new Vector3dOperations();
    private final Vec3f impostorVector = new Vec3f();
    protected DynamicTextureAtlas.MaterialSlot slot;
    protected ImpostorGroup impostorGroup;
    protected int indexStart;
    protected int impostorRes;
    protected float currentColor;
    protected int requestRes;
    protected boolean valid;
    protected double impostorDistance;
    protected float objectSize;
    private float impostorSizeX;
    private float impostorSizeY;
    private double impostorDistanceRelation;
    private double currentDistanceToCamera;
    private boolean transition;

    public double getImpostorDistance() {
        return this.impostorDistance;
    }

    public void setImpostorDistance(double var1) {
        this.impostorDistance = var1;
    }

    public DynamicTextureAtlas.MaterialSlot getSlot() {
        return this.slot;
    }

    public void setSlot(DynamicTextureAtlas.MaterialSlot var1) {
        this.slot = var1;
    }

    public int getIndexStart() {
        return this.indexStart;
    }

    public void setIndexStart(int var1) {
        this.indexStart = var1;
    }

    public float getCurrentColor() {
        return this.currentColor;
    }

    public void setCurrentColor(float var1) {
        if (this.currentColor != var1) {
            this.currentColor = var1;
            if (this.impostorGroup != null) {
                this.impostorGroup.regenerateMeshColor(this);
            }
        }

    }

    public int getRequestRes() {
        return this.requestRes;
    }

    public void setRequestRes(int var1) {
        this.requestRes = var1;
    }

    public boolean isValid() {
        return this.valid;
    }

    public void setValid(boolean var1) {
        this.valid = var1;
    }

    public boolean updateImpostor(RenderContext var1, SceneObject var2) {
        var1.incNumImpostorsUpdated();
        Vector3dOperations var3 = var1.vec3dTemp0;
        Vec3f var4 = var1.vec3fTemp0;
        Vec3f var5 = var1.vec3fTemp1;
        Vec3f var6 = var1.vec3fTemp2;
        Vec3f var7 = var1.vec3fTemp3;
        var2.getAabbWorldSpace().aH(var3);
        bc_q var8 = var1.transformTemp0;
        Camera var9 = var1.getCamera();
        var8.b(var9.getTransform());
        var4.sub(var9.getGlobalTransform().position, var3);
        var4.normalize();
        var2.getGlobalTransform().mX.a((Vector3f) var4, (Vector3f) var7);
        float var10 = this.impostorVector.dot(var7);
        float var11 = (9.0F + (float) this.impostorRes / 64.0F) * 0.1F;
        float var12 = (float) Math.sqrt(var2.getCurrentSquaredDistanceToCamera());
        float var13 = (float) (this.impostorDistance / (double) var12);
        var11 = (0.85F + var11 * 0.15F) * 0.96F + 0.04000002F * var13;
        if (var10 < var11) {
            var1.addImpostor(var2);
            return true;
        } else {
            var5.set(0.0F, 1.0F, 0.0F);
            var6.cross(var5, var4);
            var6.normalize();
            var5.cross(var4, var6);
            var9.getTransform().setX(var6);
            var9.getTransform().setY(var5);
            var9.getTransform().setZ(var4);
            var9.updateInternalGeometry((SceneObject) null);
            int var14 = (int) Math.ceil((double) var1.getSizeOnScreen(var3, var2.getAabbWSLenght()));
            int var15;
            var14 = var15 = aip.ra(var14);
            var9.setTransform(var8);
            if (var14 > 2 && var15 > 2) {
                int var16 = Math.max(var14, var15);
                if (var16 != this.impostorRes) {
                    this.requestRes = var16;
                    var1.addImpostor(var2);
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    public boolean createImpostor(RenderContext var1, SceneObject var2, ImpostorManager var3) {
        this.valid = false;
        Camera var4 = var1.getCamera();
        if (this.requestRes == 0) {
            var2.getAabbWorldSpace().aH(var1.vec3dTemp0);
            int var5 = (int) Math.ceil((double) var1.getSizeOnScreen(var1.vec3dTemp0, var2.getAabbWSLenght()));
            int var6;
            var5 = var6 = aip.ra(var5);
            if (var6 <= 2 || var5 <= 2) {
                return false;
            }

            this.requestRes = Math.max(var6, var5);
        }

        if (this.requestRes > 64) {
            this.impostorDistance = var1.getDistanceForSize(var2.getAabbWSLenght(), 53.333332F);
            this.requestRes = 0;
            if (this.impostorGroup != null) {
                this.impostorGroup.removeImpostor(this);
                this.impostorGroup = null;
                this.slot = null;
            }

            return false;
        } else if (this.requestRes <= 2) {
            return false;
        } else {
            this.impostorRes = this.requestRes;
            this.requestRes = 0;
            float var10 = (float) this.impostorRes;
            float var11 = (float) this.impostorRes;
            if (this.slot != null) {
                if (this.slot.textureSize != this.impostorRes) {
                    this.impostorGroup.removeImpostor(this);
                    this.impostorGroup = var3.getImpostorGroupForSize(this.impostorRes, var1);
                    this.slot = this.impostorGroup.getMaterialSlot(this.impostorRes);
                    if (this.slot == null) {
                        this.impostorGroup = null;
                        return false;
                    }

                    this.impostorGroup.addImpostor(this);
                }
            } else {
                this.impostorGroup = var3.getImpostorGroupForSize(this.impostorRes, var1);
                this.slot = this.impostorGroup.getMaterialSlot(this.impostorRes);
                if (this.slot == null) {
                    this.impostorGroup = null;
                    return false;
                }

                this.impostorGroup.addImpostor(this);
            }

            var2.getAabbWorldSpace().aH(this.position);
            this.currentDistanceToCamera = this.position.ax(var4.getGlobalTransform().position);
            float var7 = var4.getTanHalfFovY();
            float var8 = var7 * var4.getAspect();
            this.impostorSizeX = (float) (this.currentDistanceToCamera * (double) var8 * (double) var10) / (float) var1.getDc().getViewport().width;
            this.impostorSizeY = (float) (this.currentDistanceToCamera * (double) var7 * (double) var11) / (float) var1.getDc().getViewport().height;
            Vec3f var9 = var1.vec3fTemp0;
            var4.getGlobalTransform().getZ(var9);
            var2.getGlobalTransform().mX.a((Vector3f) var9, (Vector3f) this.impostorVector);
            this.objectSize = (float) var2.getAabbWSLenght();
            this.position.sub(var9.mT(-this.objectSize * 0.5F));
            this.impostorDistanceRelation = (this.currentDistanceToCamera - (double) (this.objectSize * 0.5F)) / this.currentDistanceToCamera;
            var4.getGlobalTransform().getZ(var1.vec3fTemp2);
            var1.vec3dTemp2.sub(var4.getGlobalTransform().position, this.position);
            this.currentDistanceToCamera = var1.vec3dTemp2.b((te_w) var1.vec3fTemp2);
            this.impostorSizeX = (float) ((double) this.impostorSizeX * this.impostorDistanceRelation);
            this.impostorSizeY = (float) ((double) this.impostorSizeY * this.impostorDistanceRelation);
            this.vert0.set((double) (-this.impostorSizeX), (double) (-this.impostorSizeY), 0.0D);
            this.vert1.set((double) this.impostorSizeX, (double) (-this.impostorSizeY), 0.0D);
            this.vert2.set((double) this.impostorSizeX, (double) this.impostorSizeY, 0.0D);
            this.vert3.set((double) (-this.impostorSizeX), (double) this.impostorSizeY, 0.0D);
            var4.getGlobalTransform().mX.transform(this.vert0);
            var4.getGlobalTransform().mX.transform(this.vert1);
            var4.getGlobalTransform().mX.transform(this.vert2);
            var4.getGlobalTransform().mX.transform(this.vert3);
            this.vert0.add(this.position);
            this.vert1.add(this.position);
            this.vert2.add(this.position);
            this.vert3.add(this.position);
            this.impostorGroup.setDirty(true);
            this.valid = true;
            this.transition = false;
            return true;
        }
    }

    public Vector3dOperations getVert0() {
        return this.vert0;
    }

    public Vector3dOperations getVert1() {
        return this.vert1;
    }

    public Vector3dOperations getVert2() {
        return this.vert2;
    }

    public Vector3dOperations getVert3() {
        return this.vert3;
    }

    public ImpostorGroup getImpostorGroup() {
        return this.impostorGroup;
    }

    public void clearData() {
        if (this.impostorGroup != null) {
            this.impostorGroup.removeImpostor(this);
            this.impostorGroup = null;
        }

        this.slot = null;
        this.valid = false;
        this.transition = false;
    }

    public double getCurrentDistanceToCameraPlane() {
        return this.currentDistanceToCamera;
    }

    public boolean isTransition() {
        return this.transition;
    }

    public void setTransition(boolean var1) {
        this.transition = var1;
    }
}
