package taikodom.render;

import all.*;
import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import taikodom.render.camera.Camera;
import taikodom.render.postProcessing.PostProcessingFX;
import taikodom.render.raytrace.RayTraceSceneObjectInfoD;
import taikodom.render.scene.Scene;
import taikodom.render.scene.SceneObject;
import taikodom.render.scene.SceneViewQuality;
import taikodom.render.scene.Viewport;
import taikodom.render.textures.RenderTarget;

import javax.vecmath.Tuple3d;
import javax.vecmath.Vector2f;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

/**
 * Отрисоввщик сцены
 */
public class SceneView {
    public static aJD_q tempVec4d = new aJD_q();
    public static aJD_q tempVec4d2 = new aJD_q();
    public static bc_q tempTransform = new bc_q();
    private final SceneViewQuality sceneViewQuality;
    protected List postProcessingFX;
    private String handle;
    private Scene scene;
    private Camera camera;
    private Viewport viewport;
    private RenderInfo renderInfo;
    private int priority;
    private boolean enabled;
    private RenderTarget renderTarget;
    private boolean allowImpostors;

    /**
     * Отрисоввщик сцены
     * создаём сцену и камеру
     */
    public SceneView() {
        this(new Scene("SceneView"), new Camera());
    }

    /**
     * Отрисоввщик сцены
     *
     * @param var1 Сцена
     * @param var2 Камера
     */
    public SceneView(Scene var1, Camera var2) {
        this.viewport = new Viewport(0, 0, 1024, 768);
        this.renderInfo = new RenderInfo();
        this.sceneViewQuality = new SceneViewQuality();
        this.postProcessingFX = new ArrayList();
        this.enabled = true;
        this.scene = var1;
        this.camera = var2;
    }

    /**
     * Отрисоввщик сцены
     *
     * @param var1
     */
    public SceneView(SceneView var1) {
        this.viewport = new Viewport(0, 0, 1024, 768);
        this.renderInfo = new RenderInfo();
        this.sceneViewQuality = new SceneViewQuality();
        this.postProcessingFX = new ArrayList();
        this.enabled = true;
        this.set(var1);
    }

    /**
     * @param var1
     */
    public void set(SceneView var1) {
        this.scene = var1.scene;
        this.camera = var1.camera;
        this.viewport.set(var1.viewport);
        this.renderInfo.set(var1.renderInfo);
        this.sceneViewQuality.set(var1.sceneViewQuality);
        this.postProcessingFX.clear();
        this.postProcessingFX.addAll(var1.postProcessingFX);
        this.priority = var1.priority;
        this.enabled = var1.enabled;
        this.renderTarget = var1.renderTarget;
        this.allowImpostors = var1.allowImpostors;
    }

    public Viewport getViewport() {
        return this.viewport;
    }

    public void setViewport(Viewport var1) {
        this.viewport = var1;
    }

    public void setViewport(int x, int y, int width, int height) {
        this.viewport.x = x;
        this.viewport.y = y;
        this.viewport.width = width;
        this.viewport.height = height;
    }

    public RenderInfo getRenderInfo() {
        return this.renderInfo;
    }

    public void setRenderInfo(RenderInfo var1) {
        this.renderInfo = var1;
    }

    public Scene getScene() {
        return this.scene;
    }

    public void setScene(Scene var1) {
        this.scene = var1;
    }

    public Camera getCamera() {
        return this.camera;
    }

    public void setCamera(Camera var1) {
        this.camera = var1;
    }

    public Color getClearColor() {
        return this.renderInfo.getClearColor();
    }

    public Color getFogColor() {
        return this.renderInfo.getFogColor();
    }

    public void setFogColor(Color var1) {
        this.renderInfo.setFogColor(var1);
    }

    public float getFogEnd() {
        return this.renderInfo.getFogEnd();
    }

    public void setFogEnd(float var1) {
        this.renderInfo.setFogEnd(var1);
    }

    public float getFogExp() {
        return this.renderInfo.getFogExp();
    }

    public void setFogExp(float var1) {
        this.renderInfo.setFogExp(var1);
    }

    public float getFogStart() {
        return this.renderInfo.getFogStart();
    }

    public void setFogStart(float var1) {
        this.renderInfo.setFogStart(var1);
    }

    public int getFogType() {
        return this.renderInfo.getFogType();
    }

    public void setFogType(int var1) {
        this.renderInfo.setFogType(var1);
    }

    public boolean isClearColorBuffer() {
        return this.renderInfo.isClearColorBuffer();
    }

    public void setClearColorBuffer(boolean var1) {
        this.renderInfo.setClearColorBuffer(var1);
    }

    public boolean isClearDepthBuffer() {
        return this.renderInfo.isClearDepthBuffer();
    }

    public void setClearDepthBuffer(boolean var1) {
        this.renderInfo.setClearDepthBuffer(var1);
    }

    public boolean isClearStencilBuffer() {
        return this.renderInfo.isClearStencilBuffer();
    }

    public void setClearStencilBuffer(boolean var1) {
        this.renderInfo.setClearStencilBuffer(var1);
    }

    public boolean isWireframeOnly() {
        return this.renderInfo.isWireframeOnly();
    }

    public void setWireframeOnly(boolean var1) {
        this.renderInfo.setWireframeOnly(var1);
    }

    public RayTraceSceneObjectInfoD rayTraceMouseOverScene(Vector2f var1) {
        return this.rayTraceMouseOverScene(var1, true);
    }

    public RayTraceSceneObjectInfoD rayTraceMouseOverScene(Vector2f var1, boolean var2) {
        Vec3f var3 = new Vec3f();
        var3.x = var1.x / (float) this.getViewport().width;
        var3.y = var1.y / (float) this.getViewport().height;
        var3.z = 0.0F;
        Vector3dOperations var4 = this.getNormScreenPosToWorldPos(var3);
        var3.z = 1.0F;
        Vector3dOperations var5 = this.getNormScreenPosToWorldPos(var3);
        if (this.camera.isOrthogonal()) {
            var4.z *= -1.0D;
            var5.z *= -1.0D;
        }

        RayTraceSceneObjectInfoD var6 = new RayTraceSceneObjectInfoD(var4, var5);
        Stack var7 = new Stack();
        synchronized (this.scene.getChildren()) {
            Iterator var10 = this.scene.getChildren().iterator();

            while (true) {
                if (!var10.hasNext()) {
                    break;
                }

                SceneObject var9 = (SceneObject) var10.next();
                var7.add(var9);
            }
        }

        while (true) {
            SceneObject var8;
            do {
                if (var7.empty()) {
                    return var6;
                }

                var8 = (SceneObject) var7.pop();
            } while (!var8.isAllowSelection());

            if (!var2) {
                synchronized (var8.getChildren()) {
                    Iterator var11 = var8.getChildren().iterator();

                    while (var11.hasNext()) {
                        SceneObject var15 = (SceneObject) var11.next();
                        var7.add(var15);
                    }
                }
            }

            aDM var14 = var8.getAabbWorldSpace().a(var4, var5, true);
            if (var14.isIntersected()) {
                var6.setChanged(false);
                var8.rayIntersects(var6, true, var2);
                if (var6.isIntersected() && var6.isChanged()) {
                    var6.setIntersectedPoint(var4.d((Tuple3d) var6.getRayDelta().Z(var6.getParamIntersection())));
                    var6.getPlane().setNormal(var14.getNormal());
                    var6.getPlane().ac(-var6.getNormal().az(var6.getIntersectedPoint()));
                    var6.setIntersectedObject(var8);
                }
            }
        }
    }

    public Vector3dOperations getScreenPosToWorldPos(Vec3f var1) {
        Vec3f var2 = new Vec3f(var1);
        var2.x = var1.x / (float) this.getViewport().width;
        var2.y = var1.y / (float) this.getViewport().height;
        return this.getNormScreenPosToWorldPos(var2);
    }

    public double getDistanceForWidthSize(double var1, float var3) {
        double var4 = (double) (this.camera.getAspect() * var3) / var1;
        double var6 = (double) this.viewport.width / var4;
        double var8 = var6 / (double) (2.0F * this.camera.getTanHalfFovY());
        return var8;
    }

    public double getDistanceForHeightSize(double var1, float var3) {
        double var4 = (double) var3 / var1;
        double var6 = (double) this.viewport.height / var4;
        double var8 = var6 / (double) (2.0F * this.camera.getTanHalfFovY());
        return var8;
    }

    public float getSizeOnScreen(Vector3dOperations var1, double var2) {
        float var4 = (float) var1.ax(this.camera.getPosition());
        if (var4 == 0.0F) {
            return (float) this.viewport.width;
        } else {
            float var5 = var4 * 2.0F * this.camera.getTanHalfFovY();
            float var6 = (float) this.viewport.width / var5;
            float var7 = (float) (var2 * (double) var6);
            return var7;
        }
    }

    public Vector3dOperations getNormScreenPosToWorldPos(Vec3f var1) {
        aJF var2 = new aJF();
        var2.x = (float) ((double) (var1.x * 2.0F) - 1.0D);
        var2.y = (float) ((double) (var1.y * 2.0F) - 1.0D);
        var2.z = (float) ((double) (var1.z * 2.0F) - 1.0D);
        var2.w = 1.0F;
        ajK var3 = new ajK();
        var3.invert(this.camera.getProjection());
        var3.transform(var2);
        if (var2.w == 0.0F) {
            return var1.dfR();
        } else {
            Vector3dOperations var4 = new Vector3dOperations((double) var2.x, (double) var2.y, (double) var2.z);
            var4.aa((double) (1.0F / var2.w));
            this.camera.getTransform().a(var4);
            return var4;
        }
    }

    public boolean getWindowCoords(Vector3dOperations var1, Vector3dOperations var2) {
        Camera var3 = this.camera;
        aJD_q var4 = tempVec4d;
        aJD_q var5 = tempVec4d2;
        var4.x = var1.x - var3.getTransform().position.x;
        var4.y = var1.y - var3.getTransform().position.y;
        var4.z = var1.z - var3.getTransform().position.z;
        var4.w = 1.0D;
        tempTransform.a(var3.getTransform().mX);
        tempTransform.b(var4, var5);
        var3.getProjection().c(var5, var4);
        if (var4.w == 0.0D) {
            return false;
        } else {
            var4.x /= var4.w;
            var4.y /= var4.w;
            var4.z /= var4.w;
            var2.x = (double) this.viewport.x + (1.0D + var4.x) * (double) this.viewport.width / 2.0D;
            var2.y = (double) this.viewport.y + (1.0D + var4.y) * (double) this.viewport.height / 2.0D;
            var2.z = (1.0D + var4.z) / 2.0D;
            return true;
        }
    }

    public void setNoShading(boolean var1) {
        this.renderInfo.setNoShading(var1);
    }

    public void setDrawAABBs(boolean var1) {
        this.renderInfo.setDrawAABBs(var1);
    }

    public void setDrawLights(boolean var1) {
        this.renderInfo.setDrawLights(var1);
    }

    public int getPriority() {
        return this.priority;
    }

    public void setPriority(int var1) {
        this.priority = var1;
    }

    public boolean isEnabled() {
        return this.enabled;
    }

    public void setEnabled(boolean var1) {
        this.enabled = var1;
    }

    public void enable() {
        this.setEnabled(true);
    }

    public void disable() {
        this.setEnabled(false);
    }

    public SceneViewQuality getSceneViewQuality() {
        return this.sceneViewQuality;
    }

    public void setSceneViewQuality(SceneViewQuality var1) {
        this.sceneViewQuality.set(var1);
    }

    public RenderTarget getRenderTarget() {
        return this.renderTarget;
    }

    public void setRenderTarget(RenderTarget var1) {
        this.renderTarget = var1;
    }

    public void addPostProcessingFX(PostProcessingFX var1) {
        this.postProcessingFX.add(var1);
    }

    public void removePostProcessingFX(PostProcessingFX var1) {
        this.postProcessingFX.remove(var1);
    }

    public List getPostProcessingFXs() {
        return this.postProcessingFX;
    }

    public boolean isAllowImpostors() {
        return this.allowImpostors;
    }

    public void setAllowImpostors(boolean var1) {
        this.allowImpostors = var1;
    }

    public String getHandle() {
        return this.handle;
    }

    public void setHandle(String var1) {
        this.handle = var1;
    }
}
