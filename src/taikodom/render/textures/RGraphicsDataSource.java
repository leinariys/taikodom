package taikodom.render.textures;

import com.sun.opengl.util.texture.TextureData;
import com.sun.opengl.util.texture.TextureIO;
import taikodom.render.gl.GLSupportedCaps;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;

public class RGraphicsDataSource extends TextureDataSource {
    static MediaTracker tracker = new MediaTracker(new Label());
    Image image;
    TextureData[] tdata = new TextureData[1];
    boolean isFlushed = false;

    public TextureData[] getData() {
        if (this.isFlushed) {
            tracker.addImage(this.image, 0);

            try {
                tracker.waitForID(0, 10000L);
                tracker.removeImage(this.image);
            } catch (InterruptedException var6) {
                throw new RuntimeException(var6);
            }
        }

        int var2 = this.image.getWidth((ImageObserver) null);
        int var3 = this.image.getHeight((ImageObserver) null);
        boolean var4 = !Texture.isPowerOf2(var2);
        boolean var5 = !Texture.isPowerOf2(var3);
        BufferedImage var1;
        if ((var4 || var5) && !GLSupportedCaps.isTextureNPOT() && !GLSupportedCaps.isTextureRectangle()) {
            var1 = new BufferedImage(var4 ? var2 + 1 : var2, var5 ? var3 + 1 : var3, 2);
        } else {
            var4 = false;
            var5 = false;
            var1 = new BufferedImage(var2, var3, 2);
        }

        var1.getGraphics().drawImage(this.image, 0, 0, (ImageObserver) null);
        if (var4) {
            var1.getGraphics().copyArea(var2 - 1, 0, 1, var3, 1, 0);
        }

        if (var5) {
            var1.getGraphics().copyArea(0, var3 - 1, var2, 1, 0, 1);
        }

        this.tdata[0] = TextureIO.newTextureData(var1, false);
        var1.flush();
        this.isFlushed = true;
        this.image.flush();
        return this.tdata;
    }

    public Image getImage() {
        return this.image;
    }

    public void setImage(Image var1) {
        this.image = var1;
        this.width = var1.getWidth((ImageObserver) null);
        this.height = var1.getHeight((ImageObserver) null);
    }

    public void clearData() {
        this.tdata[0] = null;
    }
}
