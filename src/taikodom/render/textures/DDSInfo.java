package taikodom.render.textures;

import java.nio.ByteBuffer;

public class DDSInfo {
    private ByteBuffer[] data;
    private int width;
    private int height;
    private boolean isCompressed;
    private int compressionFormat;

    public DDSInfo(ByteBuffer[] var1, int var2, int var3, boolean var4, int var5) {
        this.data = var1;
        this.width = var2;
        this.height = var3;
        this.isCompressed = var4;
        this.compressionFormat = var5;
    }

    public int getWidth() {
        return this.width;
    }

    public int getHeight() {
        return this.height;
    }

    public ByteBuffer getData() {
        return this.data[0];
    }

    public ByteBuffer getData(int var1) {
        return this.data[var1];
    }

    public ByteBuffer[] getDataList() {
        return this.data;
    }

    public boolean isCompressed() {
        return this.isCompressed;
    }

    public int getCompressionFormat() {
        return this.compressionFormat;
    }

    public void setCompressionFormat(int var1) {
        this.compressionFormat = var1;
    }
}
