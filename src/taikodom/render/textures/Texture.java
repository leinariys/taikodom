package taikodom.render.textures;

import all.aQKa;
import com.sun.opengl.impl.Debug;
import com.sun.opengl.util.texture.TextureData;
import com.sun.opengl.util.texture.TextureIO;
import gnu.trove.TIntObjectHashMap;
import gnu.trove.TIntObjectIterator;
import taikodom.render.DrawContext;
import taikodom.render.enums.TexMagFilter;
import taikodom.render.enums.TexMinFilter;
import taikodom.render.enums.TexWrap;
import taikodom.render.gl.GLSupportedCaps;
import taikodom.render.loader.RenderAsset;
import taikodom.render.loader.provider.ImageLoader;
import taikodom.render.loader.provider.ResourceCleaner;

import javax.media.opengl.GL;

public class Texture extends BaseTexture {
    private static final boolean disableNPOT = Debug.isPropertyDefined("jogl.texture.nonpot");
    /**
     * Данные текстуры
     */
    protected TextureDataSource dataSource;
    protected boolean isDead = false;
    protected boolean initialized = false;
    protected boolean firstBindTextureRequested = false;
    protected boolean waitingForUpdate = false;
    protected boolean onlyUpdateNeeded = false;
    protected TexWrap wrapS;
    protected TexWrap wrapT;
    protected TexMagFilter magFilter;
    protected TexMinFilter minFilter;
    protected int internalFormat;
    protected boolean transformed;
    aQKa ScreenSize;
    private float persistencePriority;
    private boolean updatingData = false;
    private long lastBindTime;
    private long lastAdaptationTime;
    private boolean hasMipmap;
    private TIntObjectHashMap textureData;
    private com.sun.opengl.util.texture.Texture tex;
    private boolean firstBind;

    public Texture() {
        this.wrapS = TexWrap.REPEAT;
        this.wrapT = TexWrap.REPEAT;
        this.magFilter = TexMagFilter.NONE;
        this.minFilter = TexMinFilter.NONE;
        this.internalFormat = 6408;
        this.transformed = false;
        this.hasMipmap = false;
        this.textureData = new TIntObjectHashMap(1);
        this.tex = null;
        this.firstBind = true;
    }

    public static boolean haveNPOT() {
        return !disableNPOT && GLSupportedCaps.isTextureNPOT();
    }

    public static boolean haveTexRect() {
        return TextureIO.isTexRectEnabled() && GLSupportedCaps.isTextureRectangle();
    }

    public static boolean isPowerOf2(int var0) {
        return (var0 & var0 - 1) == 0;
    }

    public static int roundToPowerOf2(int var0) {
        return (int) Math.pow(2.0D, Math.ceil(Math.log10((double) var0) / Math.log10(2.0D)));
    }

    public void setTextureData(int var1, TextureData var2) {
        if (var2 == null) {
            this.textureData.remove(var1);
        } else {
            this.hasMipmap = var2.getMipmap();
            this.textureData.put(var1, var2);
            if (var1 == 0) {
                this.width = var2.getWidth();
                this.height = var2.getHeight();
            }
        }

        char var3;
        if (this.textureData.size() > 1) {
            var3 = '蔓';
        }

        if (this.textureData.size() == 1) {
            if ((!isPowerOf2(((TextureData) this.textureData.get(0)).getWidth()) || !isPowerOf2(((TextureData) this.textureData.get(0)).getHeight())) && !haveNPOT()) {
                if (haveTexRect()) {
                    var3 = '蓵';
                }
            } else {
                boolean var4 = true;
            }
        }

    }

    public RenderAsset cloneAsset() {
        return this;
    }

    public void bind(DrawContext var1) {
        this.lastBindTime = System.currentTimeMillis();
        if (this.persistencePriority < 65000.0F) {
            this.persistencePriority += var1.getElapsedTime();
        }
/**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
 * седержит методы для рисования примитивов (линии треугольники)*/
        GL var2 = var1.getGL();
        var1.incNumTextureBind();
        if (!this.waitingForUpdate && this.dataSource instanceof DDSDataSource) {
            DDSDataSource var3 = (DDSDataSource) this.dataSource;
            if (this.dataSource.getDegradationLevel() != 0 && var3.getMipMapResolution() < ImageLoader.getMaxMipMapSize() && this.getScreenArea() > (float) var3.getMipMapResolution()) {
                this.waitingForUpdate = ImageLoader.addTexture(this, (int) Math.ceil((double) this.getScreenArea()), false);
            }
        }

        if (this.tex == null) {
            this.updatingData = true;
            if (this.dataSource != null) {
                TextureData[] var7 = this.dataSource.getData();
                if (var7.length > 1) {
                    this.tex = TextureIO.newTexture(34067);
                    if (this.dataSource instanceof DDSDataSource) {
                        this.width = ((DDSDataSource) this.dataSource).getRealWidth();
                        this.height = ((DDSDataSource) this.dataSource).getRealHeight();
                    } else {
                        this.width = this.dataSource.getWidth();
                        this.height = this.dataSource.getHeight();
                    }

                    for (int var9 = 0; var9 < var7.length; ++var9) {
                        this.tex.updateImage(var7[var9], ImageLoader.cubeMapTargets[var9]);
                    }
                } else {
                    this.tex = TextureIO.newTexture(var7[0]);
                    if (this.dataSource instanceof DDSDataSource) {
                        this.width = ((DDSDataSource) this.dataSource).getRealWidth();
                        this.height = ((DDSDataSource) this.dataSource).getRealHeight();
                    } else {
                        this.width = this.dataSource.getWidth();
                        this.height = this.dataSource.getHeight();
                    }
                }

                this.dataSource.clearData();
            } else {
                if (this.textureData.get(0) == null) {
                    return;
                }

                if (this.textureData.size() > 1) {
                    this.tex = TextureIO.newTexture(34067);
                    this.width = this.tex.getWidth();
                    this.height = this.tex.getWidth();
                    TIntObjectIterator var8 = this.textureData.iterator();

                    while (var8.hasNext()) {
                        var8.advance();
                        this.tex.updateImage((TextureData) var8.value(), var8.key());
                    }
                } else {
                    this.tex = TextureIO.newTexture((TextureData) this.textureData.get(0));
                    if (((TextureData) this.textureData.get(0)).getMipmap()) {
                        var2.glTexParameteri(this.getTarget(), 10241, 9987);
                    }
                }

                this.textureData = null;
            }

            this.target = this.tex.getTarget();
            if (this.target != 34037) {
                var2.glTexParameteri(this.target, 10242, this.wrapS.glEquivalent());
                var2.glTexParameteri(this.target, 10243, this.wrapT.glEquivalent());
            }

            if (this.target == 34067) {
                var2.glTexParameteri(this.target, 10242, 33071);
                var2.glTexParameteri(this.target, 10243, 33071);
                var2.glTexParameteri(this.target, 32882, 33071);
            }

            if (this.minFilter != TexMinFilter.NONE) {
                var2.glTexParameteri(this.target, 10241, this.minFilter.glEquivalent());
            }

            if (this.magFilter != TexMagFilter.NONE) {
                var2.glTexParameteri(this.target, 10240, this.magFilter.glEquivalent());
            }

            this.updatingData = false;
            var2.glEnable(this.target);
            this.tex.bind();
        } else {
            if (!this.initialized) {
                ResourceCleaner.addTexture(this);
                addTextureMemoryUsed(this.tex.getEstimatedMemorySize());
                this.initialized = true;
            }

            if (this.onlyUpdateNeeded) {
                if (this.dataSource != null && !this.dataSource.isReady()) {
                    ImageLoader.reAddTexture(this);
                }

                this.waitingForUpdate = false;
                this.onlyUpdateNeeded = false;
                this.updatingData = true;
                addTextureMemoryUsed(-this.tex.getEstimatedMemorySize());
                boolean var6 = false;
                TextureData[] var4 = this.dataSource.getData();
                if (var4.length > 1) {
                    for (int var5 = 0; var5 < var4.length; ++var5) {
                        this.tex.updateImage(var4[var5], ImageLoader.cubeMapTargets[var5]);
                    }
                } else {
                    this.tex.updateImage(var4[0], 0);
                    if (this.dataSource instanceof DDSDataSource && this.dataSource.getData(0).getMipmap()) {
                        this.minFilter = TexMinFilter.LINEAR_MIPMAP_LINEAR;
                    }
                }

                if (!var6) {
                    if (this.target == 34037) {
                        if (this.wrapS == TexWrap.REPEAT || this.wrapS == TexWrap.MIRRORED_REPEAT) {
                            this.wrapS = TexWrap.CLAMP_TO_EDGE;
                        }

                        if (this.wrapT == TexWrap.REPEAT || this.wrapT == TexWrap.MIRRORED_REPEAT) {
                            this.wrapT = TexWrap.CLAMP_TO_EDGE;
                        }
                    }

                    if (this.minFilter != TexMinFilter.NONE) {
                        var2.glTexParameteri(this.target, 10241, this.minFilter.glEquivalent());
                    }

                    if (this.magFilter != TexMagFilter.NONE) {
                        var2.glTexParameteri(this.target, 10240, this.magFilter.glEquivalent());
                    }

                    if (this.target == 34067) {
                        var2.glTexParameteri(this.target, 10242, 33071);
                        var2.glTexParameteri(this.target, 10243, 33071);
                        var2.glTexParameteri(this.target, 32882, 33071);
                    } else {
                        var2.glTexParameteri(this.target, 10242, this.wrapS.glEquivalent());
                        var2.glTexParameteri(this.target, 10243, this.wrapT.glEquivalent());
                    }

                    if (this.minFilter != TexMinFilter.NONE) {
                        var2.glTexParameteri(this.target, 10241, this.minFilter.glEquivalent());
                    }

                    if (this.magFilter != TexMagFilter.NONE) {
                        var2.glTexParameteri(this.target, 10240, this.magFilter.glEquivalent());
                    }
                }

                this.dataSource.clearData();
                this.updatingData = false;
                addTextureMemoryUsed(this.tex.getEstimatedMemorySize());
            }

            var2.glEnable(this.target);
            this.tex.bind();
        }
    }

    public void bind(int var1, DrawContext var2) {
        var2.getGL().glActiveTexture(var1);
        this.bind(var2);
    }

    public com.sun.opengl.util.texture.Texture getInternalTexture() {
        return this.tex;
    }

    public TextureData getTextureData(int var1) {
        return (TextureData) this.textureData.get(var1);
    }

    public TexWrap getWrapS() {
        return this.wrapS;
    }

    public void setWrapS(TexWrap var1) {
        this.wrapS = var1;
    }

    public TexWrap getWrapT() {
        return this.wrapT;
    }

    public void setWrapT(TexWrap var1) {
        this.wrapT = var1;
    }

    /**
     * @deprecated
     */
    @Deprecated
    public TexWrap getWrap() {
        return this.wrapS;
    }

    /**
     * @deprecated
     */
    @Deprecated
    public void setWrap(TexWrap var1) {
        this.setWrapS(var1);
        this.setWrapT(var1);
    }

    public TexMagFilter getMagFilter() {
        return this.magFilter;
    }

    public void setMagFilter(TexMagFilter var1) {
        this.magFilter = var1;
    }

    public TexMinFilter getMinFilter() {
        return this.minFilter;
    }

    public void setMinFilter(TexMinFilter var1) {
        this.minFilter = var1;
    }

    public int getTexId() {
        return this.tex != null ? this.tex.getTextureObject() : 0;
    }

    public boolean isHasMipmap() {
        return this.hasMipmap;
    }

    public void releaseReferences() {
        if (this.tex != null) {
            addTextureMemoryUsed(-this.tex.getEstimatedMemorySize());
            this.tex.dispose();
            this.tex = null;
        }

    }

    public float getTransformedWidth() {
        return Math.abs((float) this.width * this.transform.m00);
    }

    public float getTransformedHeight() {
        return Math.abs((float) this.height * this.transform.m11);
    }

    /**
     * Данные текстуры
     *
     * @return
     */
    public TextureDataSource getDataSource() {
        return this.dataSource;
    }

    /**
     * Данные текстуры
     *
     * @param var1
     */
    public void setDataSource(TextureDataSource var1) {
        this.dataSource = var1;
    }

    public long getLastBindTime() {
        return this.lastBindTime;
    }

    public void setLastBindTime(long var1) {
        this.lastBindTime = var1;
    }

    public boolean isInitialized() {
        return this.initialized;
    }

    public void setInitialized(boolean var1) {
        this.initialized = var1;
    }

    public boolean isOnlyUpdateNeeded() {
        return this.onlyUpdateNeeded;
    }

    public void setOnlyUpdateNeeded(boolean var1) {
        this.onlyUpdateNeeded = var1;
    }

    public boolean isUpdatingData() {
        return this.updatingData;
    }

    public boolean isFirstBind() {
        return this.firstBind;
    }

    public void setFirstBind(boolean var1) {
        this.firstBind = var1;
    }

    public float getPersistencePriority() {
        return this.persistencePriority;
    }

    public void setPersistencePriority(float var1) {
        this.persistencePriority = var1;
    }

    public boolean isFirstBindTextureRequested() {
        return this.firstBindTextureRequested;
    }

    public void setFirstBindTextureRequested(boolean var1) {
        this.firstBindTextureRequested = var1;
    }

    public void destroyInternalTexture() {
        if (this.tex != null) {
            ImageLoader.destroyTexture(this);
        }

    }

    public long getLastDegradationTime() {
        return this.lastAdaptationTime;
    }

    public void setLastDegradationTime(long var1) {
        this.lastAdaptationTime = var1;
    }

    public void updateImage(TextureData var1, int var2) {
        if (this.tex != null) {
            this.tex.updateImage(var1, var2);
        }

    }

    public void updateImage(DrawContext var1, TextureData var2) {
        if (this.tex != null) {
            this.tex.bind();
            var1.getGL().glTexImage2D(this.tex.getTarget(), 0, 0, 8, 8, 0, 6408, 5121, var2.getBuffer());
        }

    }

    public int getMemorySize() {
        return this.tex.getEstimatedMemorySize();
    }

    public void setTarget(int var1) {
        this.target = var1;
    }

    public boolean isWaitingForUpdate() {
        return this.waitingForUpdate;
    }

    public void setWaitingForUpdate(boolean var1) {
        this.waitingForUpdate = var1;
    }
}
