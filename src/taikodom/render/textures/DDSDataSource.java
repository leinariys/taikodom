package taikodom.render.textures;

import com.sun.opengl.util.texture.TextureData;

public class DDSDataSource extends TextureDataSource {
    DDSLoader loader;
    TextureData[] data;
    TextureData[] sampleData;
    int sampleWidth;
    int sampleHeight;
    private int mipMapResolution;

    public TextureData getData(int var1) {
        return this.data != null ? this.data[var1] : this.sampleData[var1];
    }

    public int getMipMapResolution() {
        return this.mipMapResolution;
    }

    public void setMipMapResolution(int var1) {
        this.mipMapResolution = var1;
        if (var1 != 0) {
            ;
        }
    }

    public TextureData[] getData() {
        return this.data != null ? this.data : this.sampleData;
    }

    public void setData(TextureData[] var1) {
        this.data = var1;
    }

    public void setSampleData(TextureData[] var1) {
        this.sampleData = var1;
    }

    public void setSampleData(TextureData var1) {
        this.sampleData = new TextureData[1];
        this.sampleData[0] = var1;
    }

    public int getSampleWidth() {
        return this.sampleWidth;
    }

    public void setSampleWidth(int var1) {
        this.sampleWidth = var1;
    }

    public int getSampleHeight() {
        return this.sampleHeight;
    }

    public void setSampleHeight(int var1) {
        this.sampleHeight = var1;
    }

    public int getMipMapIDByResolution(int var1) {
        return this.loader.getMipMapIDByResolution(var1);
    }

    public boolean isCubemap() {
        return this.loader.isCubemap();
    }

    public int getNumMipMaps() {
        return this.loader.getNumMipMaps();
    }

    public DDSLoader getLoader() {
        return this.loader;
    }

    /**
     * Ссылка на загрузчик текстуры
     *
     * @param var1
     */
    public void setLoader(DDSLoader var1) {
        if (var1 != null) {
            this.loader = var1;
        }
    }

    public int getRealWidth() {
        return this.loader.getRealWidth();
    }

    public int getRealHeight() {
        return this.loader.getRealHeight();
    }

    public void clearData() {
        this.data = null;
    }
}
