package taikodom.render.textures;

import all.ajK;
import all.ju;
import all.ol;
import taikodom.render.DrawContext;
import taikodom.render.loader.RenderAsset;

import javax.media.opengl.GL;
import java.util.ArrayList;
import java.util.List;

public class FlashTexture extends BaseTexture {
    private static float maxCurveError = 20.0F;
    private static int maxHeight = 1024;
    private static int maxWidth = 1024;
    private static float minCurveError = 0.05F;
    private static int minHeight = 64;
    private static int minWidth = 64;
    private SimpleTexture texture;
    private List lodLevels = new ArrayList();
    private PBuffer pbuffer;
    private ol player;
    private float quality = 0.5F;
    private float curveError = 0.1F;
    private float scaleX = 1.0F;
    private float scaleY = 1.0F;

    FlashTexture() {
    }

    public FlashTexture(ol var1) {
        this.player = var1;
        ju var2 = var1.SC();
        this.width = Texture.roundToPowerOf2(var2.getWidth());
        this.height = Texture.roundToPowerOf2(var2.getHeight());
        this.scaleX = (float) var2.getWidth() / (float) this.width;
        this.scaleY = (float) var2.getHeight() / (float) this.height;
        this.setupLOD();
        ol.cA(this.curveError);
    }

    public static void setQualityLimits(int var0, int var1, int var2, int var3, float var4, float var5) {
        minCurveError = var4;
        maxCurveError = var5;
        minWidth = var0;
        minHeight = var1;
        maxWidth = var2;
        maxHeight = var3;
    }

    private void setupLOD() {
        if (minWidth != maxWidth) {
            int var1 = this.player.SC().getWidth();
            int var2 = this.player.SC().getHeight();
            int var3 = 0;
            int var4 = 0;
            this.lodLevels.add(new FlashTexture.a(var1, var2));
            int var5 = var1 * 2;

            int var6;
            FlashTexture.a var7;
            for (var6 = var2 * 2; var1 >= var2 && var5 <= maxWidth || var1 < var2 && var6 <= maxHeight; var6 *= 2) {
                var7 = new FlashTexture.a(var5, var6);
                this.lodLevels.add(var7);
                ++var4;
                var5 *= 2;
            }

            var5 = var1 / 2;

            for (var6 = var2 / 2; var1 >= var2 && var5 >= minWidth || var1 < var2 && var6 >= minHeight; var6 /= 2) {
                var7 = new FlashTexture.a(var5, var6);
                this.lodLevels.add(0, var7);
                ++var3;
                var5 /= 2;
            }

            this.quality = (1.0F + (float) var3) / (1.0F + (float) var3 + (float) var4);
        }
    }

    public void releaseReferences() {
        this.pbuffer.destroy();
        this.texture.releaseReferences();
        this.player.destroy();
        this.player = null;
    }

    private void createPBuffer(DrawContext var1) {
        /**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
         * седержит методы для рисования примитивов (линии треугольники)*/
        GL var2 = var1.getGL();
        if (this.pbuffer != null) {
            this.pbuffer.destroy();
            this.pbuffer = null;
            this.texture.releaseReferences();
        }

        this.pbuffer = new PBuffer();
        this.pbuffer.create(var1, this.width, this.height);
        this.texture = new SimpleTexture();
        this.texture.createEmptyRGBA(this.width, this.height, 32);
        ajK var3 = new ajK(this.scaleX, 0.0F, 0.0F, 0.0F, 0.0F, this.scaleY, 0.0F, 0.0F, 0.0F, 0.0F, 1.0F, 0.0F, 0.0F, 1.0F - this.scaleY, 0.0F, 1.0F);
        this.setTransform(var3);
        this.pbuffer.activate(var1);
        var2.glClear(16384);
        this.pbuffer.deactivate(var1);
        this.texture.bind(var1);
    }

    public void bind(DrawContext var1) {
        /**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
         * седержит методы для рисования примитивов (линии треугольники)*/
        GL var2 = var1.getGL();
        var1.incNumTextureBind();
        if (this.pbuffer == null) {
            this.createPBuffer(var1);
        }

        if (this.player.SG()) {
            this.pbuffer.activate(var1);

            try {
                this.player.draw();
            } finally {
                this.texture.copyFromCurrentBuffer(var2);
                this.pbuffer.deactivate(var1);
            }
        } else if (this.player.isFinished()) {
            var1.postSceneEvent("endVideo", this);
        }

        this.texture.bind(var1);
    }

    public ol getPlayer() {
        return this.player;
    }

    public float getQuality() {
        return this.quality;
    }

    public void setTarget(int var1) {
        this.target = var1;
    }

    public void setQuality(DrawContext var1, float var2) {
        if (minWidth != maxWidth) {
            this.quality = Math.min(Math.max(0.0F, var2), 1.0F);
            this.curveError = (1.0F - this.quality) * (maxCurveError - minCurveError) + minCurveError;
            int var3 = (int) Math.ceil((double) (this.quality * ((float) this.lodLevels.size() - 1.0F)));
            int var4 = ((FlashTexture.a) this.lodLevels.get(var3)).width;
            int var5 = ((FlashTexture.a) this.lodLevels.get(var3)).height;
            if (this.player.SC().getWidth() != var4) {
                this.player.setSize(var4, var5);
                this.width = Texture.roundToPowerOf2(var4);
                this.height = Texture.roundToPowerOf2(var5);
                this.createPBuffer(var1);
            }
        }
    }

    public void bind(int var1, DrawContext var2) {
        var2.getGL().glActiveTexture(var1);
        this.bind(var1, var2);
    }

    public RenderAsset cloneAsset() {
        return this;
    }

    public float getScaleX() {
        return this.scaleX;
    }

    public float getScaleY() {
        return this.scaleY;
    }

    private class a {
        public int height;
        public int width;

        public a(int var2, int var3) {
            this.width = var2;
            this.height = var3;
        }
    }
}
