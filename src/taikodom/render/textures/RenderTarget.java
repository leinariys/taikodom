package taikodom.render.textures;

import com.hoplon.geometry.Color;
import taikodom.render.DrawContext;
import taikodom.render.enums.FBOAttachTarget;
import taikodom.render.gl.GLSupportedCaps;
import taikodom.render.loader.RenderAsset;

import javax.media.opengl.GL;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLException;

public class RenderTarget extends RenderAsset {
    /**
     * GLCapabilities указывает неизменный набор возможностей OpenGL.
     */
    private static final GLCapabilities PBUFFER_CAPS = new GLCapabilities();

    static {
        if (!GLSupportedCaps.isPbuffer()) {
            throw new GLException("PBuffers not supported with this graphics card");
        } else {
            PBUFFER_CAPS.setDoubleBuffered(false);
            PBUFFER_CAPS.setHardwareAccelerated(true);
            PBUFFER_CAPS.setPbufferRenderToTexture(false);
            PBUFFER_CAPS.setAlphaBits(8);
            PBUFFER_CAPS.setGreenBits(8);
            PBUFFER_CAPS.setRedBits(8);
            PBUFFER_CAPS.setBlueBits(8);
            PBUFFER_CAPS.setDepthBits(24);
            PBUFFER_CAPS.setStencilBits(8);
        }
    }

    private SimpleTexture texture;
    private int width;
    private int height;
    private PBuffer pbuffer;
    private FrameBufferObject fbo;
    private boolean isOffscreen = false;
    private boolean isBound = false;
    private GL gl;
    private int target = 3553;

    public void bind(DrawContext var1) {
        if (this.isBound) {
            throw new RuntimeException("RenderTarget already bound");
        } else {
            this.isBound = true;
            if (this.isOffscreen) {
                this.pbuffer = var1.getAuxPBuffer();
                this.pbuffer.activate(var1);
                GL var2 = var1.getGL();
                var2.glClearColor(0.0F, 0.0F, 0.0F, 0.0F);
                var2.glClear(16384);
            } else if (this.fbo != null) {
                this.fbo.bind(var1);
            }

        }
    }

    public void releaseReferences() {
        if (this.texture != null) {
            this.texture.releaseReferences();
            this.texture = null;
        }

        if (this.fbo != null) {
            this.fbo.releaseReferences();
            this.fbo = null;
        }

    }

    public void releaseReferences(GL var1) {
        if (this.texture != null) {
            this.texture.releaseReferences(var1);
            this.texture = null;
        }

    }

    public void createAsFBO(int var1, int var2, DrawContext var3) {
        this.releaseReferences(var3.getGL());
        if (!GLSupportedCaps.isFbo()) {
            throw new RuntimeException("FBO extension not supported");
        } else {
            this.fbo = new FrameBufferObject();
            this.fbo.create(var3, var1, var2, true);
            this.texture = new SimpleTexture();
            this.texture.setName("Simple texture for RT " + this.getName());
            this.texture.createRectangleEmpty(var1, var2, 32);
            this.texture.fillWithColor(new Color(0.0F, 0.0F, 0.0F, 0.0F));
            this.fbo.addColorAttachTexture(var3, this.texture, FBOAttachTarget.TEXTURE_RECTANGLE);
        }
    }

    public void createAsPBO(int var1, int var2, DrawContext var3) {
        this.releaseReferences(var3.getGL());
        this.texture = new SimpleTexture();
        this.texture.setName("Simple texture for RT " + this.getName());
        this.texture.createEmptyRGBA(var1, var2, 32);
        this.texture.fillWithColor(new Color(0.0F, 0.0F, 0.0F, 0.0F));
        this.isOffscreen = true;
    }

    public void create(int var1, int var2, boolean var3, DrawContext var4, boolean var5) {
        this.releaseReferences();
        this.gl = var4.getGL();
        if (var3) {
            if (GLSupportedCaps.isFbo() && !var5) {
                this.createAsFBO(var1, var2, var4);
            } else {
                if (!GLSupportedCaps.isPbuffer()) {
                    throw new GLException("pBuffers not supported with this graphics card");
                }

                this.createAsPBO(var1, var2, var4);
            }
        } else {
            this.texture = new SimpleTexture();
            this.texture.setName("Simple texture for RT " + this.getName());
            this.texture.createEmptyRGBA(var1, var2, 32);
            this.texture.fillWithColor(new Color(0.0F, 0.0F, 0.0F, 0.0F));
        }

    }

    public void unbind(DrawContext var1) {
        if (!this.isBound) {
            throw new RuntimeException("RenderTarget not bound");
        } else {
            this.isBound = false;
            if (this.pbuffer != null) {
                this.pbuffer.deactivate(var1);
            } else if (this.fbo != null) {
                FrameBufferObject.unbind(var1);
            } else {
                this.texture.copyFromCurrentBuffer(var1.getGL());
            }

        }
    }

    public void bindTexture(DrawContext var1) {
        this.texture.bind(var1);
    }

    public int getWidth() {
        return this.width;
    }

    public void setWidth(int var1) {
        this.width = var1;
    }

    public int getHeight() {
        return this.height;
    }

    public void setHeight(int var1) {
        this.height = var1;
    }

    public RenderAsset cloneAsset() {
        return this;
    }

    public BaseTexture getTexture() {
        return this.texture;
    }

    public void setTexture(SimpleTexture var1) {
        this.texture = var1;
    }

    public void copyBufferToTexture(DrawContext var1) {
        if (this.fbo == null) {
            this.texture.copyFromCurrentBuffer(var1.getGL());
        }

    }

    public void copyBufferAreaToTexture(DrawContext var1, int var2, int var3, int var4, int var5, int var6, int var7) {
        if (this.fbo == null) {
            this.texture.copyFromCurrentBuffer(this.gl, var2, var3, var4, var5, var6, var7);
        }

    }
}
