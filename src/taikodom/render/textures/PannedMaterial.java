package taikodom.render.textures;

import all.ajK;
import com.hoplon.geometry.Vec3f;

/**
 * Реализация класса указанного в имени тегов из файлов с расширением .pro
 */
public class PannedMaterial extends Material {
    protected final ajK panMatrix = new ajK();
    protected Material material;

    public Material getMaterial() {
        return this.material;
    }

    public void setMaterial(Material var1) {
        this.material = var1;
    }

    public Vec3f getPanSpeed() {
        return new Vec3f(this.panMatrix.m03, this.panMatrix.m13, this.panMatrix.m23);
    }

    public void setPanSpeed(Vec3f var1) {
        this.panMatrix.m03 = var1.x;
        this.panMatrix.m13 = var1.y;
        this.panMatrix.m23 = var1.z;
    }

    public Vec3f getPanSpeed(Vec3f var1) {
        var1.x = this.panMatrix.m03;
        var1.y = this.panMatrix.m13;
        var1.z = this.panMatrix.m23;
        return var1;
    }
}
