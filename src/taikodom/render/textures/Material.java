package taikodom.render.textures;

import all.aQKa;
import all.ajK;
import com.hoplon.geometry.Color;
import taikodom.render.DrawContext;
import taikodom.render.MaterialDrawingStates;
import taikodom.render.loader.Property;
import taikodom.render.loader.RenderAsset;
import taikodom.render.loader.SceneLoader;
import taikodom.render.shaders.Shader;
import taikodom.render.shaders.ShaderPass;
import taikodom.render.shaders.ShaderProgram;
import taikodom.render.shaders.parameters.ShaderParameter;

import javax.media.opengl.GL;
import java.util.ArrayList;
import java.util.List;

/**
 * Реализация класса указанного в имени тегов из файлов с расширением .pro
 */
public class Material extends RenderAsset {
    public static final int MAX_MATERIAL_TEX = 16;
    public static final int LIGHTMAP_CHANNEL = 7;
    public static final int DIFFUSE_CHANNEL = 8;
    public static final int DIFFUSE_DETAIL_CHANNEL = 9;
    public static final int SELFILUM_CHANNEL = 10;
    public static final int NORMAL_CHANNEL = 12;
    public static final int FRAMEBUFFER_CHANNEL = 14;
    protected final List textures = new ArrayList();
    protected final Color specular = new Color(1.0F, 1.0F, 1.0F, 1.0F);
    protected final ajK transform = new ajK();
    protected float shininess = 32.0F;
    protected ShaderParameter[][] internalShaderParameters;
    protected int shaderPassCount;
    protected List shaderParameters = new ArrayList();
    protected boolean shaderParametersUpdated;
    private Shader shader;
    private boolean useFramebufferRGB;

    public Material() {//TODO изменил на 16
        for (int var1 = 0; var1 < 16; ++var1) {
            this.textures.add((Object) null);
        }

    }

    public Material(Material var1) {
        super(var1);
        this.shader = null;
        this.shaderPassCount = 0;
        this.shaderParametersUpdated = false;
        this.internalShaderParameters = null;

        int var2;
        for (var2 = 0; var2 < var1.textures.size(); ++var2) {
            BaseTexture var3 = (BaseTexture) var1.textures.get(var2);
            if (var3 != null) {
                this.textures.add((BaseTexture) var3.cloneAsset());
            } else {
                this.textures.add((Object) null);
            }
        }

        this.shininess = var1.shininess;
        this.specular.set(var1.specular);
        if (var1.shader != null) {
            this.setShader((Shader) var1.shader.cloneAsset());
        }

        this.transform.set(var1.transform);
        this.useFramebufferRGB = var1.useFramebufferRGB;

        for (var2 = 0; var2 < var1.shaderParameters.size(); ++var2) {
            ShaderParameter var4 = (ShaderParameter) var1.shaderParameters.get(var2);
            if (var4 != null) {
                this.shaderParameters.add((ShaderParameter) var4.cloneAsset());
            } else {
                this.shaderParameters.add((Object) null);
            }
        }

    }

    public float getShininess() {
        return this.shininess;
    }

    public void setShininess(float var1) {
        this.shininess = var1;
    }

    public Color getSpecular() {
        return this.specular;
    }

    public void setSpecular(Color var1) {
        this.specular.set(var1);
    }

    public void setTexture(int var1, BaseTexture var2) {
        if (var1 == -1) {
            this.textures.add(var2);
        } else {
            while (this.textures.size() <= var1) {
                this.textures.add((Object) null);
            }

            this.textures.set(var1, var2);
        }
    }

    public void bind(DrawContext var1) {
        if (var1.debugDraw()) {
            var1.logBind(this);
        }

        if (!this.shaderParametersUpdated && this.shader != null) {
            this.shader.compile(var1);
            this.populateShaderParameters();
        }

        var1.incNumMaterialBinds();
        /**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
         * седержит методы для рисования примитивов (линии треугольники)*/
        GL var2 = var1.getGL();
        var2.glMaterialf(1028, 5633, this.shininess);
        MaterialDrawingStates var3 = var1.materialState;
        if (var1.currentMaterial != null) {
            var1.currentMaterial.unbind(var1);
        }

        for (int var4 = 0; var4 < var3.getStates().size(); ++var4) {
            ChannelInfo var5 = var3.getTexInfo(var4);
            int var6 = var5.channelMapping;
            var2.glActiveTexture('蓀' + var6);
            var2.glLoadIdentity();
            this.bindTexture(var6, var5.texChannel, var1, (BaseTexture) null);
        }

    }

    private void bindTexture(int var1, int var2, DrawContext var3, BaseTexture var4) {
        this.updateTextureMatrix(var1, var2, var3);
        BaseTexture var5 = (BaseTexture) this.textures.get(var2);
        if (var5 != null) {
            var5.bind(var3);
        }

    }

    private void updateTextureMatrix(int var1, int var2, DrawContext var3) {
        var3.getGL().glLoadMatrixf(this.transform.b(var3.floatBuffer60Temp0));
        BaseTexture var4 = this.getTexture(var2);
        if (var4 != null) {
            var3.getGL().glMultMatrixf(var4.getTransform().b(var3.floatBuffer60Temp0));
        }

    }

    private void populateShaderParameters() {
        if (this.shader != null) {
            int var1 = this.shader.passCount();
            this.internalShaderParameters = new ShaderParameter[var1][];
            this.shaderPassCount = var1;

            int var2;
            ShaderProgram var3;
            int var4;
            for (var2 = 0; var2 < var1; ++var2) {
                var3 = this.shader.getPass(var2).getProgram();
                if (var3 != null) {
                    if (var3.getManualParamsCount() > 0) {
                        this.internalShaderParameters[var2] = new ShaderParameter[var3.getManualParamsCount()];

                        for (var4 = 0; var4 < var3.getManualParamsCount(); ++var4) {
                            this.internalShaderParameters[var2][var4] = (ShaderParameter) var3.getManualParameter(var4).cloneAsset();
                        }
                    }
                } else {
                    this.internalShaderParameters[var2] = null;
                }
            }

            for (var2 = 0; var2 < var1; ++var2) {
                var3 = this.shader.getPass(var2).getProgram();
                if (var3 != null) {
                    for (var4 = 0; var4 < var3.getManualParamsCount(); ++var4) {
                        for (int var5 = 0; var5 < this.shaderParameters.size(); ++var5) {
                            ShaderParameter var6 = (ShaderParameter) this.shaderParameters.get(var5);
                            ShaderParameter var7 = this.internalShaderParameters[var2][var4];
                            if (var6.getParameterName().equals(var7.getParameterName())) {
                                var7.assignValue(var6);
                                var6.setLocationId(var7.getLocationId());
                                this.internalShaderParameters[var2][var4] = var6;
                            }
                        }
                    }
                }
            }

            this.shaderParametersUpdated = true;
        }
    }

    public Shader getShader() {
        return this.shader;
    }

    public void setShader(Shader var1) {
        this.shader = var1;
    }

    public BaseTexture getTexture(int var1) {
        return (BaseTexture) this.textures.get(var1);
    }

    public RenderAsset cloneAsset() {
        return new Material(this);
    }

    public BaseTexture getNormalTexture() {
        return this.getTexture(12);
    }

    public void setNormalTexture(BaseTexture var1) {
        this.setTexture(12, var1);
    }

    public BaseTexture getSelfIluminationTexture() {
        return this.getTexture(10);
    }

    public void setSelfIluminationTexture(BaseTexture var1) {
        this.setTexture(10, var1);
    }

    public BaseTexture getDiffuseTexture() {
        return this.getTexture(8);
    }

    public void setDiffuseTexture(BaseTexture var1) {
        this.setTexture(8, var1);
    }

    @Property(
            persisted = false,
            name = "diffuseDetailTexture"
    )
    public BaseTexture getDiffuseDetailTexture() {
        return this.getTexture(9);
    }

    @Property(
            persisted = false,
            name = "diffuseDetailTexture"
    )
    public void setDiffuseDetailTexture(BaseTexture var1) {
        this.setTexture(9, var1);
    }

    public void setNormalTexture(int var1, BaseTexture var2) {
        this.setTexture(12 + var1, var2);
    }

    public void setDiffuseDetailTexture(int var1, BaseTexture var2) {
        this.setTexture(9 + var1, var2);
    }

    public void setSelfIluminationTexture(int var1, BaseTexture var2) {
        this.setTexture(10 + var1, var2);
    }

    public void setDiffuseTexture(int var1, BaseTexture var2) {
        this.setTexture(8 + var1, var2);
    }

    public void setShaderParameter(int var1, ShaderParameter var2) {
        this.addShaderParameter(var2);
    }

    public void addShaderParameter(ShaderParameter var1) {
        this.shaderParameters.add(var1);
    }

    public ajK getTransform() {
        return this.transform;
    }

    public void setTransform(ajK var1) {
        this.transform.set(var1);
    }

    public void updateShaderParameters(int var1, DrawContext var2) {
        if (this.shader != null && this.shader == var2.currentRecord().shader && this.internalShaderParameters != null && this.internalShaderParameters[var1] != null) {
            if (var1 >= this.shaderPassCount) {
                this.shaderParametersUpdated = false;
                this.importShaderParametersFromShader();
            }

            ShaderPass var3 = this.shader.getPass(var1);
            if (var3 == null) {
                return;
            }

            for (int var4 = 0; var4 < var3.getProgram().getManualParamsCount(); ++var4) {
                this.internalShaderParameters[var1][var4].bind(var2);
            }
        }

    }

    public ShaderParameter getShaderParameter(int var1) {
        return (ShaderParameter) this.shaderParameters.get(var1);
    }

    public void importShaderParametersFromShader() {
    }

    public void validate(SceneLoader var1) {
        super.validate(var1);
        if (this.getDiffuseTexture() == null) {
            this.setDiffuseTexture(var1.getDefaultDiffuseTexture());
        }

        if (this.getNormalTexture() == null) {
            this.setNormalTexture(var1.getDefaultNormalTexture());
        }

        if (this.getSelfIluminationTexture() == null) {
            this.setSelfIluminationTexture(var1.getDefaultSelfIluminationTexture());
        }

    }

    public int shaderParameterCount() {
        return this.shaderParameters.size();
    }

    public int textureCount() {
        return this.textures.size();
    }

    public aQKa getScaling() {
        aQKa var1 = new aQKa();
        var1.x = (float) Math.sqrt((double) (this.transform.m00 * this.transform.m00 + this.transform.m10 * this.transform.m10 + this.transform.m20 * this.transform.m20));
        var1.y = (float) Math.sqrt((double) (this.transform.m01 * this.transform.m01 + this.transform.m11 * this.transform.m11 + this.transform.m21 * this.transform.m21));
        return var1;
    }

    public float getXScaling() {
        return (float) Math.sqrt((double) (this.transform.m00 * this.transform.m00 + this.transform.m10 * this.transform.m10 + this.transform.m20 * this.transform.m20));
    }

    public float getYScaling() {
        return (float) Math.sqrt((double) (this.transform.m01 * this.transform.m01 + this.transform.m11 * this.transform.m11 + this.transform.m21 * this.transform.m21));
    }

    public float getTransformedHeight() {
        return Math.abs(this.getDiffuseTexture().getTransformedHeight() * this.transform.m11);
    }

    public void releaseReferences() {
        super.releaseReferences();
        this.textures.clear();
        this.shader = null;
    }

    public void unbind(DrawContext var1) {
        /**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
         * седержит методы для рисования примитивов (линии треугольники)*/
        GL var2 = var1.getGL();
        MaterialDrawingStates var3 = var1.materialState;

        for (int var4 = 0; var4 < var3.getStates().size(); ++var4) {
            ChannelInfo var5 = var3.getTexInfo(var4);
            int var6 = var5.channelMapping;
            var2.glActiveTexture('蓀' + var6);
            BaseTexture var7 = this.getTexture(var5.texChannel);
            if (var7 != null) {
                var2.glDisable(var7.getTarget());
            }
        }

    }

    public boolean isUseFramebufferRGB() {
        return this.useFramebufferRGB;
    }

    public void setUseFramebufferRGB(boolean var1) {
        this.useFramebufferRGB = var1;
    }
}
