package taikodom.render.textures;

import all.ajK;
import com.hoplon.geometry.Vec3f;
import taikodom.render.DrawContext;

/**
 * Реализация класса указанного в имени тегов из файлов с расширением .pro
 */
public class TiledTexture extends Texture {
    protected final ajK tileMatrix = new ajK();
    protected Texture texture = null;

    public void addArea(float var1) {
        if (this.texture != null) {
            this.texture.addArea(var1);
        }
    }

    public Texture getTexture() {
        return this.texture;
    }

    public void setTexture(Texture var1) {
        this.texture = var1;
    }

    public Vec3f getTile() {
        return new Vec3f(this.tileMatrix.m00, this.tileMatrix.m11, this.tileMatrix.m22);
    }

    public void setTile(Vec3f var1) {
        this.tileMatrix.m00 = var1.x;
        this.tileMatrix.m11 = var1.y;
        this.tileMatrix.m22 = var1.z;
    }

    public void bind(DrawContext var1) {
        if (var1.debugDraw()) {
            var1.logBind(this);
        }

        if (this.texture != null) {
            this.texture.bind(var1);
            var1.getGL().glTexParameteri(this.texture.target, 10242, this.wrapS.glEquivalent());
            var1.getGL().glTexParameteri(this.texture.target, 10243, this.wrapT.glEquivalent());
        }

        var1.getGL().glMultMatrixf(this.tileMatrix.b(var1.tempBuffer0));
    }

    public void bind(int var1, DrawContext var2) {
        if (this.texture != null) {
            this.texture.bind(var1, var2);
            var2.getGL().glTexParameteri(this.texture.target, 10242, this.wrapS.glEquivalent());
            var2.getGL().glTexParameteri(this.texture.target, 10243, this.wrapT.glEquivalent());
        }

        var2.getGL().glMultMatrixf(this.tileMatrix.b(var2.tempBuffer0));
    }
}
