package taikodom.render.textures;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;

/**
 * Чтение заголовка Файла DDS
 */
public class DDSHeader {
    private static final int DDS_MAGIC = 542327876;// = 0x20534444;
    int size;                     //размер структуры DDSURFACEDESC
    int flags;                    //определяет, какие поля действительны
    int height;                   //высота создаваемой поверхности
    int width;                    //ширина создаваемой поверхности
    int pitchOrLinearSize;
    int backBufferCountOrDepth;
    int mipMapCountOrAux;         //количество запрошенных уровней мип-карты (в этом контексте)
    int alphaBitDepth;            //глубина запроса альфа-буфера
    int reserved1;                //зарезервированный
    int surface;                  //указатель на связанную память поверхности
    //ПРИМЕЧАНИЕ: после двух записей из структуры данных DDCOLORKEY
    //Накладываются цветом для пустых граней куба (не используются в этом ридере)
    int colorSpaceLowValue;
    int colorSpaceHighValue;
    int destBltColorSpaceLowValue;
    int destBltColorSpaceHighValue;
    int srcOverlayColorSpaceLowValue;
    int srcOverlayColorSpaceHighValue;
    int srcBltColorSpaceLowValue;
    int srcBltColorSpaceHighValue;
    //ПРИМЕЧАНИЕ. После ввода данных из структуры данных DDPIXELFORMAT
    //Наложены с помощью гибкого формата вершинного формата вершины
    //буферы (неиспользуемые в этом читателе)
    int pfSize;                //размер структуры DDPIXELFORMAT
    int pfFlags;               //флаги формата пикселей
    int pfFourCC;              //Код FOURCC
    // После пяти записей есть несколько интерпретаций, а не только
    // RGBA (но это все, что мы поддерживаем прямо сейчас)
    int pfRGBBitCount;         //Сколько бит на пиксель
    int pfRBitMask;            //маска для красных бит
    int pfGBitMask;            //маска для зеленых бит
    int pfBBitMask;            //маска для синих бит
    int pfABitMask;            //маска для альфа-канала
    int ddsCaps1;              //Флаги текстур и мип-карт
    int ddsCaps2;              //Расширенные возможности, включая поддержку cubemap
    int ddsCapsReserved1;      //
    int ddsCapsReserved2;      //
    int textureStage;          //этап в каскаде мультитекстурирования

    private static int size() {
        return 124;
    }

    private static int pfSize() {
        return 32;
    }

    public static int writtenSize() {
        return 128;
    }

    public final int readInt(InputStream var1) throws IOException {
        int var2 = var1.read();//Чтение по байтам
        int var3 = var1.read();
        int var4 = var1.read();
        int var5 = var1.read();
        if ((var2 | var3 | var4 | var5) < 0) {
            throw new EOFException();
        } else {
            return (var5 << 24) + (var4 << 16) + (var3 << 8) + (var2 << 0);
        }
    }

    void read(InputStream var1) throws IOException {
        int var2 = this.readInt(var1);
        if (var2 != DDS_MAGIC) {
            throw new IOException("Incorrect magic number 0x" + Integer.toHexString(var2) + " (expected " + DDS_MAGIC + ")");
        } else {
            this.size = this.readInt(var1);
            this.flags = this.readInt(var1);
            this.height = this.readInt(var1);
            this.width = this.readInt(var1);
            this.pitchOrLinearSize = this.readInt(var1);
            this.backBufferCountOrDepth = this.readInt(var1);
            this.mipMapCountOrAux = this.readInt(var1);
            this.alphaBitDepth = this.readInt(var1);
            this.reserved1 = this.readInt(var1);
            this.surface = this.readInt(var1);
            this.colorSpaceLowValue = this.readInt(var1);
            this.colorSpaceHighValue = this.readInt(var1);
            this.destBltColorSpaceLowValue = this.readInt(var1);
            this.destBltColorSpaceHighValue = this.readInt(var1);
            this.srcOverlayColorSpaceLowValue = this.readInt(var1);
            this.srcOverlayColorSpaceHighValue = this.readInt(var1);
            this.srcBltColorSpaceLowValue = this.readInt(var1);
            this.srcBltColorSpaceHighValue = this.readInt(var1);
            this.pfSize = this.readInt(var1);
            this.pfFlags = this.readInt(var1);
            this.pfFourCC = this.readInt(var1);
            this.pfRGBBitCount = this.readInt(var1);
            this.pfRBitMask = this.readInt(var1);
            this.pfGBitMask = this.readInt(var1);
            this.pfBBitMask = this.readInt(var1);
            this.pfABitMask = this.readInt(var1);
            this.ddsCaps1 = this.readInt(var1);
            this.ddsCaps2 = this.readInt(var1);
            this.ddsCapsReserved1 = this.readInt(var1);
            this.ddsCapsReserved2 = this.readInt(var1);
            this.textureStage = this.readInt(var1);
        }
    }

    public int getPfFourCC() {
        return this.pfFourCC;
    }

    public void setPfFourCC(int var1) {
        this.pfFourCC = var1;
    }

    public int getHeight() {
        return this.height;
    }

    public void setHeight(int var1) {
        this.height = var1;
    }

    public int getWidth() {
        return this.width;
    }

    public void setWidth(int var1) {
        this.width = var1;
    }
}
