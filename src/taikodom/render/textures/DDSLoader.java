package taikodom.render.textures;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class DDSLoader {
    public static final int DDSD_CAPS = 1;
    public static final int DDSD_HEIGHT = 2;
    public static final int DDSD_WIDTH = 4;
    public static final int DDSD_PITCH = 8;
    public static final int DDSD_BACKBUFFERCOUNT = 32;
    public static final int DDSD_ZBUFFERBITDEPTH = 64;
    public static final int DDSD_ALPHABITDEPTH = 128;
    public static final int DDSD_LPSURFACE = 2048;
    public static final int DDSD_PIXELFORMAT = 4096;
    public static final int DDSD_MIPMAPCOUNT = 131072;
    public static final int DDSD_LINEARSIZE = 524288;
    public static final int DDSD_DEPTH = 8388608;
    public static final int DDPF_ALPHA = 2;
    public static final int DDPF_FOURCC = 4;
    public static final int DDPF_PALETTEINDEXED4 = 8;
    public static final int DDPF_PALETTEINDEXEDTO8 = 16;
    public static final int DDPF_RGBTOYUV = 256;
    public static final int DDSCAPS_TEXTURE = 4096;
    public static final int DDSCAPS_MIPMAP = 4194304;
    public static final int DDSCAPS_COMPLEX = 8;
    public static final int DDSCAPS2_CUBEMAP = 512;
    public static final int DDSCAPS2_CUBEMAP_POSITIVEX = 1024;
    public static final int DDSCAPS2_CUBEMAP_NEGATIVEX = 2048;
    public static final int DDSCAPS2_CUBEMAP_POSITIVEY = 4096;
    public static final int DDSCAPS2_CUBEMAP_NEGATIVEY = 8192;
    public static final int DDSCAPS2_CUBEMAP_POSITIVEZ = 16384;
    public static final int DDSCAPS2_CUBEMAP_NEGATIVEZ = 32768;
    public static final int D3DFMT_DXT1 = 827611204;
    public static final int D3DFMT_DXT2 = 844388420;
    public static final int D3DFMT_DXT3 = 861165636;
    public static final int D3DFMT_DXT4 = 877942852;
    public static final int D3DFMT_DXT5 = 894720068;
    public static final int D3DFMT_UNKNOWN = 0;
    public static final int D3DFMT_R8G8B8 = 20;
    public static final int D3DFMT_A8R8G8B8 = 21;
    public static final int D3DFMT_X8R8G8B8 = 22;
    public static final int DDPF_PALETTEINDEXED8 = 32;
    public static final int DDPF_RGB = 64;
    public static final int DDPF_COMPRESSED = 128;
    public static final int DDPF_ALPHAPIXELS = 1;
    boolean compressed;
    private DDSHeader header;

    public static String getCompressionFormatName(int var0) {
        StringBuffer var1 = new StringBuffer();

        for (int var2 = 0; var2 < 4; ++var2) {
            char var3 = (char) (var0 & 255);
            var1.append(var3);
            var0 >>= 8;
        }

        return var1.toString();
    }

    private static int computeCompressedBlockSize(int var0, int var1, int var2, int var3) {
        int var4 = (var0 + 3) / 4 * ((var1 + 3) / 4) * ((var2 + 3) / 4);
        switch (var3) {
            case 827611204:
                var4 *= 8;
                break;
            default:
                var4 *= 16;
        }

        return var4;
    }

    private boolean isCompressed() {
        return this.isPixelFormatFlagSet(4);
    }

    public boolean isSurfaceDescFlagSet(int var1) {
        return (this.header.flags & var1) != 0;
    }

    private int getCompressionFormat() {
        return this.header.getPfFourCC();
    }

    public int getRealWidth() {
        return this.header.getWidth();
    }

    public int getRealHeight() {
        return this.header.getHeight();
    }

    private void fixupHeader() {
        if (this.isCompressed() && !this.isSurfaceDescFlagSet(524288)) {
            int var1 = this.header.backBufferCountOrDepth;
            if (var1 == 0) {
                var1 = 1;
            }

            this.header.pitchOrLinearSize = computeCompressedBlockSize(this.getRealWidth(), this.getRealHeight(), var1, this.getCompressionFormat());
            this.header.flags |= 524288;
        }

    }

    /**
     * Чтение заголовка файла
     *
     * @param var1 Поток для чтения
     */
    public void loadFromFile(InputStream var1) {
        this.header = new DDSHeader();

        try {
            this.header.read(var1);
        } catch (IOException var3) {
            var3.printStackTrace();
        }

    }

    public boolean isPixelFormatFlagSet(int var1) {
        return (this.header.pfFlags & var1) != 0;
    }

    public int getPixelFormat() {
        if (this.isCompressed()) {
            return this.getCompressionFormat();
        } else {
            if (this.isPixelFormatFlagSet(64)) {
                if (this.isPixelFormatFlagSet(1)) {
                    if (this.getDepth() == 32 && this.header.pfRBitMask == 16711680 && this.header.pfGBitMask == 65280 && this.header.pfBBitMask == 255 && this.header.pfABitMask == -16777216) {
                        return 21;
                    }
                } else {
                    if (this.getDepth() == 24 && this.header.pfRBitMask == 16711680 && this.header.pfGBitMask == 65280 && this.header.pfBBitMask == 255) {
                        return 20;
                    }

                    if (this.getDepth() == 32 && this.header.pfRBitMask == 16711680 && this.header.pfGBitMask == 65280 && this.header.pfBBitMask == 255) {
                        return 22;
                    }
                }
            }

            return 0;
        }
    }

    public boolean isCubemap() {
        return (this.header.ddsCaps1 & 8) != 0 && (this.header.ddsCaps2 & 512) != 0;
    }

    public boolean isCubemapSidePresent(int var1) {
        return this.isCubemap() && (this.header.ddsCaps2 & var1) != 0;
    }

    public int getNumMipMaps() {
        return !this.isSurfaceDescFlagSet(131072) ? 0 : this.header.mipMapCountOrAux;
    }

    public int mipMapWidth(int var1) {
        int var2 = this.header.width;

        for (int var3 = 0; var3 < var1; ++var3) {
            var2 >>= 1;
        }

        return Math.max(var2, 1);
    }

    public int getBpp() {
        return this.header.pfSize;
    }

    public int getDepth() {
        return this.header.pfRGBBitCount;
    }

    public int mipMapHeight(int var1) {
        int var2 = this.header.height;

        for (int var3 = 0; var3 < var1; ++var3) {
            var2 >>= 1;
        }

        return Math.max(var2, 1);
    }

    private int mipMapSizeInBytes(int var1) {
        int var2 = this.mipMapWidth(var1);
        int var3 = this.mipMapHeight(var1);
        if (this.isCompressed()) {
            int var4 = this.getCompressionFormat() == 827611204 ? 8 : 16;
            return (var2 + 3) / 4 * ((var3 + 3) / 4) * var4;
        } else {
            return var2 * var3 * (this.getDepth() / 8);
        }
    }

    private int sideSizeInBytes() {
        int var1 = this.getNumMipMaps();
        if (var1 == 0) {
            var1 = 1;
        }

        int var2 = 0;

        for (int var3 = 0; var3 < var1; ++var3) {
            var2 += this.mipMapSizeInBytes(var3);
        }

        return var2;
    }

    private int sideShiftInBytes(int var1) {
        int[] var2 = new int[]{1024, 2048, 4096, 8192, 16384, 32768};
        int var3 = 0;
        int var4 = this.sideSizeInBytes();

        for (int var5 = 0; var5 < var2.length; ++var5) {
            int var6 = var2[var5];
            if ((var6 & var1) != 0) {
                return var3;
            }

            var3 += var4;
        }

        throw new RuntimeException("Illegal side: " + var1);
    }

    public DDSInfo getMipMaps(int var1, int var2, int var3, InputStream var4) {
        if (var2 == var3) {
            return this.getMipMap(var1, var2, var4);
        } else if (!this.isCubemap() && var1 != 0) {
            throw new RuntimeException("Illegal side for 2D texture: " + var1);
        } else if (this.isCubemap() && !this.isCubemapSidePresent(var1)) {
            throw new RuntimeException("Illegal side, side not present: " + var1);
        } else if (this.getNumMipMaps() > 0 && (var2 < 0 || var2 >= this.getNumMipMaps())) {
            throw new RuntimeException("Illegal mipmap number " + var2 + " (0.." + (this.getNumMipMaps() - 1) + ")");
        } else {
            int var9 = DDSHeader.writtenSize();
            if (this.isCubemap()) {
                var9 += this.sideShiftInBytes(var1);
            }

            int var10;
            for (var10 = 0; var10 < var2; ++var10) {
                var9 += this.mipMapSizeInBytes(var10);
            }

            long var5;
            for (var5 = (long) var9; var10 <= var3; ++var10) {
                var9 += this.mipMapSizeInBytes(var10);
            }

            long var7 = (long) var9;
            FileChannel var11 = ((FileInputStream) var4).getChannel();
            ByteBuffer var12 = ByteBuffer.allocate((int) (var7 - var5));

            try {
                var11.position(var5);
                var11.read(var12);
            } catch (IOException var17) {
                var17.printStackTrace();
            }

            if (var3 < var2) {
                return null;
            } else {
                ByteBuffer[] var13 = new ByteBuffer[var3 - var2];
                int var14 = 0;

                for (int var15 = 0; var15 < var3 - var2; ++var15) {
                    int var16 = this.mipMapSizeInBytes(var15 + var2);
                    var12.position(var14);
                    var12.limit(var14 + var16);
                    var13[var15] = var12.slice();
                    var13[var15].position(0);
                    var14 += var16;
                }

                DDSInfo var18 = new DDSInfo(var13, this.mipMapWidth(var2), this.mipMapHeight(var2), this.isCompressed(), this.getCompressionFormat());
                var12.clear();
                return var18;
            }
        }
    }

    public DDSInfo getMipMap(int var1, int var2, InputStream var3) {
        if (!this.isCubemap() && var1 != 0) {
            throw new RuntimeException("Illegal side for 2D texture: " + var1);
        } else if (this.isCubemap() && !this.isCubemapSidePresent(var1)) {
            throw new RuntimeException("Illegal side, side not present: " + var1);
        } else if (this.getNumMipMaps() > 0 && (var2 < 0 || var2 >= this.getNumMipMaps())) {
            throw new RuntimeException("Illegal mipmap number " + var2 + " (0.." + (this.getNumMipMaps() - 1) + ")");
        } else {
            int var4 = DDSHeader.writtenSize();
            if (this.isCubemap()) {
                var4 += this.sideShiftInBytes(var1);
            }

            int var5;
            for (var5 = 0; var5 < var2; ++var5) {
                var4 += this.mipMapSizeInBytes(var5);
            }

            var5 = this.mipMapSizeInBytes(var2);
            FileChannel var6 = ((FileInputStream) var3).getChannel();
            ByteBuffer[] var7 = new ByteBuffer[]{ByteBuffer.allocateDirect(var5)};

            try {
                var6.position((long) var4);
                var6.read(var7);
            } catch (IOException var9) {
                var9.printStackTrace();
            }

            var7[0].position(0);
            DDSInfo var8 = new DDSInfo(var7, this.mipMapWidth(var2), this.mipMapHeight(var2), this.isCompressed(), this.getCompressionFormat());
            return var8;
        }
    }

    public int getMipMapIDByResolution(int var1) {
        if (var1 >= this.header.width) {
            return 0;
        } else {
            int var2 = this.header.width;

            int var3;
            for (var3 = 0; var2 > var1; ++var3) {
                var2 >>= 1;
            }

            if (var2 > var1) {
                --var3;
            }

            var3 = var3 < this.getNumMipMaps() ? var3 : this.getNumMipMaps() - 1;
            return var3 < 0 ? 0 : var3;
        }
    }
}
