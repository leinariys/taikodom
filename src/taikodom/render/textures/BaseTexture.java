package taikodom.render.textures;

import all.ajK;
import taikodom.render.DrawContext;
import taikodom.render.loader.RenderAsset;

public abstract class BaseTexture extends RenderAsset {
    protected static final ajK FLIP_Y_MATRIX = new ajK(1.0F, 0.0F, 0.0F, 0.0F, 0.0F, -1.0F, 0.0F, 0.0F, 0.0F, 0.0F, 1.0F, 0.0F, 0.0F, 1.0F, 0.0F, 1.0F);
    protected static int textureMemoryUsed;
    protected final ajK transform;
    protected int target = 3553;
    protected int width;
    protected int height;
    protected int bpp;
    private float screenArea;
    private float tempScreenArea;

    public BaseTexture() {
        this.transform = new ajK(FLIP_Y_MATRIX);
    }

    public static int getTextureMemoryUsed() {
        return textureMemoryUsed;
    }

    public static void addTextureMemoryUsed(int var0) {
        textureMemoryUsed += var0;
    }

    public abstract void bind(DrawContext var1);

    public abstract void bind(int var1, DrawContext var2);

    public ajK getTransform() {
        return this.transform;
    }

    public void setTransform(ajK var1) {
        this.transform.set(FLIP_Y_MATRIX);
        ajK.c(FLIP_Y_MATRIX, var1, this.transform);
    }

    public int getSizeX() {
        return this.width;
    }

    public void setSizeX(int var1) {
        this.width = var1;
    }

    public int getSizeY() {
        return this.height;
    }

    public void setSizeY(int var1) {
        this.height = var1;
    }

    public int getBpp() {
        return this.bpp;
    }

    public void setBpp(int var1) {
        this.bpp = var1;
    }

    public float getTransformedWidth() {
        return Math.abs((float) this.width * this.transform.m00);
    }

    public float getTransformedHeight() {
        return Math.abs((float) this.height * this.transform.m11);
    }

    public void addArea(float var1) {
        if (var1 > this.tempScreenArea) {
            this.tempScreenArea = var1;
        }

    }

    public void resetTextureArea() {
        this.screenArea = this.tempScreenArea;
        this.tempScreenArea = 0.0F;
    }

    public float getScreenArea() {
        return this.screenArea > this.tempScreenArea ? this.screenArea : this.tempScreenArea;
    }

    public int getTexId() {
        return 0;
    }

    public int getTarget() {
        return this.target;
    }

    public float getXScaling() {
        return this.transform.m00;
    }

    public float getYScaling() {
        return this.transform.m11;
    }

    public int getMemorySize() {
        return 0;
    }
}
