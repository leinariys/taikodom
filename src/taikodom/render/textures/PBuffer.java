package taikodom.render.textures;

import all.LogPrinter;
import com.sun.opengl.impl.windows.WGL;
import com.sun.opengl.impl.windows.WGLExt;
import com.sun.opengl.impl.windows.WGLExtImpl;
import com.sun.opengl.impl.windows.WindowsGLContext;
import taikodom.render.DrawContext;
import taikodom.render.gl.GLSupportedCaps;

import javax.media.opengl.GL;
import javax.media.opengl.GLException;

public class PBuffer {
    private static LogPrinter logger = LogPrinter.K(PBuffer.class);
    private int height;
    private int width;
    private WGLExt glExt;
    private long hdc;
    private long lastHdc;
    private long buffer;
    private long glRc;
    private long lastRc;
    private boolean active = false;

    public boolean create(DrawContext var1, int var2, int var3) {
        this.glExt = new WGLExtImpl((WindowsGLContext) var1.getGlContext());
        if (!GLSupportedCaps.isPbuffer()) {
            return false;
        } else {
            int[] var4 = new int[30];
            float[] var5 = new float[1];
            int[] var10000 = new int[256];
            byte var6 = 0;
            int var15 = var6 + 1;
            var4[var6] = 8209;
            var4[var15++] = 0;
            var4[var15++] = 8208;
            var4[var15++] = 1;
            var4[var15++] = 8237;
            var4[var15++] = 1;
            var4[var15++] = 8215;
            var4[var15++] = 8;
            var4[var15++] = 8213;
            var4[var15++] = 8;
            var4[var15++] = 8217;
            var4[var15++] = 8;
            var4[var15++] = 8219;
            var4[var15++] = 8;
            var4[var15++] = 8226;
            var4[var15++] = 24;
            var4[var15++] = 8227;
            var4[var15++] = 8;
            var4[var15++] = 8211;
            var4[var15++] = 8235;
            var4[var15++] = 8195;
            var4[var15++] = 8231;
            var4[var15++] = 0;
            long var7 = WGL.wglGetCurrentDC();
            long var9 = WGL.wglGetCurrentContext();
            if (var7 == 0L) {
                logger.error("Problem getting the Current Device Context");
                return false;
            } else {
                int var11 = WGL.GetPixelFormat(var7);
                if (var11 != 0) {
                    this.buffer = this.glExt.wglCreatePbufferARB(var7, var11, var2, var3, var4, var15);
                } else {
                    this.buffer = 0L;
                    logger.warn("Problem trying to_q get game getGL pixel format. Trying to_q find all suitable format.");
                }

                int[] var13;
                if (this.buffer == 0L) {
                    int[] var12 = new int[256];
                    var13 = new int[1];
                    if (!this.glExt.wglChoosePixelFormatARB(var7, var4, 0, var5, 0, 255, var12, 0, var13, 0)) {
                        throw new GLException("pbuffer creation error: wglChoosePixelFormatARB() failed");
                    }

                    for (int var14 = 0; var14 < var13[0]; ++var14) {
                        this.buffer = this.glExt.wglCreatePbufferARB(var7, var12[var14], var2, var3, var4, var15);
                        if (this.buffer != 0L) {
                            break;
                        }
                    }
                }

                if (this.buffer == 0L) {
                    int var16 = var1.getGL().glGetError();
                    String var17 = Integer.toString(var16);
                    switch (var16) {
                        case 13:
                            var17 = "Invalid data";
                            break;
                        case 1450:
                            var17 = "No resources avaliable";
                            break;
                        case 2000:
                            var17 = "Invalid pixel format";
                    }

                    throw new GLException("Pbuffer creation error: test_GLCanvas suitable format to_q generate buffer found. (" + var17 + ").");
                } else {
                    this.hdc = this.glExt.wglGetPbufferDCARB(this.buffer);
                    if (this.hdc == 0L) {
                        throw new GLException("pbuffer creation error: wglGetPbufferDCARB() failed.");
                    } else {
                        this.width = var2;
                        this.height = var3;
                        this.glRc = WGL.wglCreateContext(this.hdc);
                        WGL.wglShareLists(var9, this.glRc);
                        var13 = new int[2];
                        this.glExt.wglQueryPbufferARB(this.buffer, 8244, var13, 0);
                        this.glExt.wglQueryPbufferARB(this.buffer, 8245, var13, 1);
                        return true;
                    }
                }
            }
        }
    }

    public void releaseReferences() {
        this.destroy();
    }

    public int getHeight() {
        return this.height;
    }

    public int getWidth() {
        return this.width;
    }

    public void activate(DrawContext var1) {
        if (this.active) {
            logger.warn("Trying to_q activate all pbuffer that's already active.");
        } else {
            this.active = true;
            /**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
             * седержит методы для рисования примитивов (линии треугольники)*/
            GL var2 = var1.getGL();
            this.lastHdc = WGL.wglGetCurrentDC();
            this.lastRc = WGL.wglGetCurrentContext();
            if (this.lastHdc == 0L || this.lastRc == 0L) {
                logger.warn("Activating all pbuffer in all thread without active glContext.");
            }

            WGL.wglMakeCurrent(this.hdc, this.glRc);
            var2.glClear(16640);
        }
    }

    public void deactivate(DrawContext var1) {
        if (this.active) {
            this.active = false;
            WGL.wglMakeCurrent(this.lastHdc, this.lastRc);
        }
    }

    public void destroy() {
        if (this.glRc != 0L) {
            WGL.wglDeleteContext(this.glRc);
            this.glExt.wglReleasePbufferDCARB(this.buffer, this.hdc);
            this.glExt.wglDestroyPbufferARB(this.buffer);
        }

    }

    public boolean isActive() {
        return this.active;
    }

    public void setActive(boolean var1) {
        this.active = var1;
    }
}
