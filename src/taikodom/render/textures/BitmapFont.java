package taikodom.render.textures;

import all.aQKa;
import com.sun.opengl.util.texture.TextureIO;
import gnu.trove.TIntShortHashMap;
import gnu.trove.TShortIntHashMap;
import taikodom.render.DrawContext;
import taikodom.render.data.Vec2fData;
import taikodom.render.loader.provider.BitmapFontFactory;

import javax.media.opengl.GL;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Vector;

public class BitmapFont {
    protected Texture texture;
    protected boolean textureInitialized;
    int numOfValidChars;
    float textureResolution;
    java.awt.Font font;
    boolean discartedChars;
    TIntShortHashMap mapIndex = null;
    TShortIntHashMap mapDisplayList = null;
    Vec2fData vertexes;
    Vec2fData texCoords;
    DrawContext dc;
    Vector charWidth = new Vector();
    private FontMetrics fontMetrics;
    private boolean ready;

    public int getDisplayList(DrawContext var1, int var2) {
        short var3 = this.mapIndex.get(var2);
        if (this.mapDisplayList.containsKey(var3)) {
            return this.mapDisplayList.get(var3);
        } else {
            GL var4 = var1.getGL();
            int var5 = var4.glGenLists(1);
            var4.glNewList(var5, 4864);
            var4.glBegin(GL.GL_QUADS);//Этот метод запускает процесс рисования. Рассматривает каждую группу из четырех вершин как независимый четырехугольник.
            aQKa var6 = new aQKa();
            aQKa var7 = new aQKa();
            int var8 = var3 * 4;
            this.vertexes.get(var8, var6);
            this.texCoords.get(var8, var7);
            var4.glTexCoord2f(var7.x, var7.y);
            var4.glVertex2f(var6.x, var6.y);
            this.vertexes.get(var8 + 1, var6);
            this.texCoords.get(var8 + 1, var7);
            var4.glTexCoord2f(var7.x, var7.y);
            var4.glVertex2f(var6.x, var6.y);
            this.vertexes.get(var8 + 2, var6);
            this.texCoords.get(var8 + 2, var7);
            var4.glTexCoord2f(var7.x, var7.y);
            var4.glVertex2f(var6.x, var6.y);
            float var9 = var6.x;
            this.vertexes.get(var8 + 3, var6);
            this.texCoords.get(var8 + 3, var7);
            var4.glTexCoord2f(var7.x, var7.y);
            var4.glVertex2f(var6.x, var6.y);
            var4.glEnd();
            var4.glTranslatef(var9, 0.0F, 0.0F);
            var4.glEndList();
            this.mapDisplayList.put(var3, var5);
            return var5;
        }
    }

    public void initialize(java.awt.Font var1) {
        System.nanoTime();
        boolean var2 = false;
        this.font = var1;
        float var4 = 50.0F;
        boolean var5 = false;
        BufferedImage var3 = new BufferedImage(1, 1, 2);
        Graphics2D var6 = (Graphics2D) var3.getGraphics();
        var6.setColor(Color.WHITE);
        this.textureResolution = 256.0F;
        int var8 = 0;
        var6.setFont(this.font.deriveFont(Math.min((float) this.font.getSize(), var4)));
        var3 = BitmapFontFactory.readSnapshot(this);
        if (var3 != null) {
            var2 = true;
        }

        short var9 = 512;
        FontMetrics var10 = var6.getFontMetrics();
        int var11;
        if (!var2) {
            for (var11 = 0; var11 < 65536; ++var11) {
                if (this.font.canDisplay(var11)) {
                    var8 += (var10.charWidth(var11) + 5) * (var10.getHeight() + 5);
                    if ((float) var8 > this.textureResolution * this.textureResolution) {
                        if (this.textureResolution < (float) var9) {
                            this.textureResolution += this.textureResolution;
                        } else {
                            this.discartedChars = true;
                        }
                    }

                    ++this.numOfValidChars;
                }
            }
        }

        if (!var2) {
            var3 = new BufferedImage((int) this.textureResolution, (int) this.textureResolution, 2);
        }

        var6 = (Graphics2D) var3.getGraphics();
        var6.setFont(this.font);
        Graphics2D var7 = (Graphics2D) var6.create();
        if ((float) this.font.getSize() > var4) {
            var7.setFont(this.font.deriveFont(var4));
            var5 = true;
        }

        var7.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        var6.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
        var6.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        this.vertexes = new Vec2fData(this.numOfValidChars * 4);
        this.texCoords = new Vec2fData(this.numOfValidChars * 4);
        var11 = 0;
        float var12 = 0.0F;
        float var13 = (float) var10.getHeight();
        float var14 = (float) (var10.getAscent() + var10.getDescent() + var10.getLeading());
        FontMetrics var16 = var7.getFontMetrics();
        float var15;
        if (var5) {
            var15 = (float) (var16.getAscent() + var16.getDescent() + var16.getLeading());
        } else {
            var15 = var14;
        }

        this.fontMetrics = var10;
        Color var17 = new Color(255, 255, 255, 0);
        Color var18 = new Color(255, 255, 255, 160);
        Color var19 = new Color(255, 255, 255, 255);
        if (!var2) {
            var6.setColor(var17);
            var6.fillRect(0, 0, (int) this.textureResolution, (int) this.textureResolution);
            var7.setColor(var19);
        }

        this.mapIndex = new TIntShortHashMap(this.numOfValidChars);
        this.mapDisplayList = new TShortIntHashMap(this.numOfValidChars / 4);
        int[] var20 = new int[1];
        short var21 = 0;

        for (int var22 = 0; var22 < 65536; ++var22) {
            if (this.font.canDisplay(var22)) {
                if (var13 > this.textureResolution) {
                    break;
                }

                this.mapIndex.put(var22, var21++);
                int var23;
                int var24;
                if (!var2) {
                    var23 = var10.charWidth(var22);
                    if (var5) {
                        var24 = var16.charWidth(var22);
                    } else {
                        var24 = var23;
                    }

                    this.charWidth.add(new WidthData(var23, var24));
                } else {
                    WidthData var25 = (WidthData) this.charWidth.get(var21 - 1);
                    var23 = var25.width;
                    var24 = var25.realWidth;
                }

                if (var12 + var15 > this.textureResolution) {
                    var13 += var15 + 5.0F;
                    var12 = 0.0F;
                }

                this.vertexes.set(var11, 0.0F, 0.0F);
                this.texCoords.set(var11, var12 / this.textureResolution, var13 / this.textureResolution);
                this.vertexes.set(var11 + 1, (float) var23, 0.0F);
                this.texCoords.set(var11 + 1, var12 / this.textureResolution + (float) var24 / this.textureResolution, var13 / this.textureResolution);
                this.vertexes.set(var11 + 2, (float) var23, -var14);
                this.texCoords.set(var11 + 2, var12 / this.textureResolution + (float) var24 / this.textureResolution, var13 / this.textureResolution - var15 / this.textureResolution);
                this.vertexes.set(var11 + 3, 0.0F, -var14);
                this.texCoords.set(var11 + 3, var12 / this.textureResolution, var13 / this.textureResolution - var15 / this.textureResolution);
                var20[0] = var22;
                var7.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                if (!var2) {
                    var7.setColor(var19);
                    var7.drawString(new String(var20, 0, 1), (int) var12, (int) var13 - var16.getDescent());
                    var7.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
                    var7.setColor(var18);
                    var7.drawString(new String(var20, 0, 1), (int) var12, (int) var13 - var16.getDescent());
                }

                var12 += (float) (var24 + 5);
                var11 += 4;
            }
        }

        if (!var2) {
            BitmapFontFactory.writeSnapshot(var3, var11, this);
        }

        this.texture = new Texture();
        this.texture.setTextureData(0, TextureIO.newTextureData(var3, false));
        var3.flush();
        this.textureInitialized = true;
        this.ready = true;
    }

    public void destroyDisplayList() {
        if (this.mapDisplayList != null) {
            int[] var4;
            int var3 = (var4 = this.mapDisplayList.getValues()).length;

            for (int var2 = 0; var2 < var3; ++var2) {
                int var1 = var4[var2];
                this.dc.getGL().glDeleteLists(var1, 1);
            }

            this.mapDisplayList.clear();
            this.mapDisplayList = null;
        }
    }

    public java.awt.Font getFont() {
        return this.font;
    }

    public FontMetrics getFontMetrics() {
        return this.fontMetrics;
    }

    public void setFontMetrics(FontMetrics var1) {
        this.fontMetrics = var1;
    }

    public float getTextureResolution() {
        return this.textureResolution;
    }

    public void setTextureResolution(int var1) {
        this.textureResolution = (float) var1;
    }

    public int getNumOfValidChars() {
        return this.numOfValidChars;
    }

    public void setNumOfValidChars(int var1) {
        this.numOfValidChars = var1;
    }

    public Vector getCharWidths() {
        return this.charWidth;
    }

    public void setCharWidths(Vector var1) {
        this.charWidth = var1;
    }

    public boolean isReady() {
        return this.ready;
    }
}
