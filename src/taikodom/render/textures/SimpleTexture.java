package taikodom.render.textures;

import all.aip;
import all.ajK;
import com.hoplon.geometry.Color;
import org.lwjgl.BufferUtils;
import taikodom.render.DrawContext;
import taikodom.render.loader.RenderAsset;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;

public class SimpleTexture extends BaseTexture {
    protected SimpleTexture.a type;
    protected int texID;
    protected int internalFormat;
    protected int internalWidth;
    protected int internalHeight;

    public SimpleTexture() {
        this.type = SimpleTexture.a.dIN;
        this.internalFormat = 6408;
    }

    public void releaseReferences() {
        GL var1 = GLU.getCurrentGL();
        if (var1.glIsTexture(this.texID)) {
            var1.glDeleteTextures(1, new int[]{this.texID}, 0);
            addTextureMemoryUsed(-(this.width * this.height * this.bpp / 8));
            this.texID = 0;
            this.width = this.height = 0;
        }

    }

    public void releaseReferences(GL var1) {
        if (var1.glIsTexture(this.texID)) {
            var1.glDeleteTextures(1, new int[]{this.texID}, 0);
            addTextureMemoryUsed(-(this.width * this.height * this.bpp / 8));
            this.texID = 0;
            this.width = this.height = 0;
        }

    }

    public void createRectangleEmpty(int var1, int var2, int var3) {
        if (this.width != var1 || this.height != var2 || this.type != SimpleTexture.a.dIN) {
            this.releaseReferences();
            this.type = SimpleTexture.a.dIN;
            if (var1 != 0 && var2 != 0) {
                this.internalWidth = var1;
                this.internalHeight = var2;
                GL var4 = GLU.getCurrentGL();
                IntBuffer var5 = BufferUtils.createIntBuffer(1);
                this.target = 34037;
                var4.glEnable(this.target);
                var4.glGenTextures(1, var5);
                this.texID = var5.get(0);
                this.width = var1;
                this.height = var2;
                addTextureMemoryUsed(this.width * this.height * var3 / 8);
                if (var3 == 32) {
                    this.internalFormat = 6408;
                } else if (var3 == 24) {
                    this.internalFormat = 6407;
                } else {
                    this.internalFormat = 6406;
                }

                this.bpp = var3;
                var4.glActiveTexture(33984);
                var4.glEnable(this.target);
                var4.glBindTexture(this.target, this.texID);
                var4.glTexImage2D(this.target, 0, 6408, this.width, this.height, 0, 6408, 5121, (Buffer) null);
                var4.glTexParameteri(this.target, 10241, 9729);
                var4.glTexParameteri(this.target, 10240, 9729);
                var4.glTexParameteri(this.target, 10242, 33071);
                var4.glTexParameteri(this.target, 10243, 33071);
                var4.glDisable(this.target);
                float var6 = (float) this.width;
                float var7 = (float) this.height;
                ajK var8 = new ajK(var6, 0.0F, 0.0F, 0.0F, 0.0F, -var7, 0.0F, 0.0F, 0.0F, 0.0F, 1.0F, 0.0F, 0.0F, 1.0F, 0.0F, 1.0F);
                this.setTransform(var8);
            }
        }

    }

    public void createEmptyDepth(int var1, int var2, int var3) {
        if (this.width != var1 || this.height != var2 || this.type != SimpleTexture.a.dIO) {
            this.releaseReferences();
            this.type = SimpleTexture.a.dIO;
            this.target = 3553;
            if (var1 != 0 && var2 != 0) {
                this.internalWidth = var1;
                this.internalHeight = var2;
                GL var4 = GLU.getCurrentGL();
                IntBuffer var5 = BufferUtils.createIntBuffer(1);
                var4.glEnable(this.target);
                var4.glGenTextures(1, var5);
                this.texID = var5.get(0);
                this.width = aip.ra(var1);
                this.height = aip.ra(var2);
                this.bpp = var3;
                addTextureMemoryUsed(this.width * this.height * var3 / 8);
                var4.glActiveTexture(33984);
                var4.glEnable(this.target);
                var4.glBindTexture(this.target, this.texID);
                var4.glTexImage2D(3553, 0, 6402, this.width, this.height, 0, 6402, 5126, (Buffer) null);
                var4.glTexParameteri(this.target, 10241, 9729);
                var4.glTexParameteri(this.target, 10240, 9729);
                var4.glTexParameteri(this.target, 10242, 33071);
                var4.glTexParameteri(this.target, 10243, 33071);
                var4.glDisable(this.target);
                ajK var6 = new ajK();
                this.setTransform(var6);
            }
        }

    }

    public void createEmptyRGBA(int var1, int var2, int var3) {
        if (this.width != var1 || this.height != var2 || this.type != SimpleTexture.a.dIN) {
            this.releaseReferences();
            this.type = SimpleTexture.a.dIN;
            this.target = 3553;
            if (var1 != 0 && var2 != 0) {
                this.internalWidth = var1;
                this.internalHeight = var2;
                GL var4 = GLU.getCurrentGL();
                IntBuffer var5 = BufferUtils.createIntBuffer(1);
                var4.glEnable(this.target);
                var4.glGenTextures(1, var5);
                this.texID = var5.get(0);
                this.width = aip.ra(var1);
                this.height = aip.ra(var2);
                addTextureMemoryUsed(this.width * this.height * var3 / 8);
                if (var3 == 32) {
                    this.internalFormat = 6408;
                } else if (var3 == 24) {
                    this.internalFormat = 6407;
                } else {
                    this.internalFormat = 6406;
                }

                this.bpp = var3;
                var4.glActiveTexture(33984);
                var4.glEnable(this.target);
                var4.glBindTexture(this.target, this.texID);
                var4.glTexImage2D(this.target, 0, 6408, this.width, this.height, 0, 6408, 5121, (Buffer) null);
                var4.glTexParameteri(this.target, 10241, 9729);
                var4.glTexParameteri(this.target, 10240, 9729);
                var4.glTexParameteri(this.target, 10242, 33071);
                var4.glTexParameteri(this.target, 10243, 33071);
                var4.glDisable(this.target);
                float var6 = (float) this.internalWidth / (float) this.width;
                float var7 = (float) this.internalHeight / (float) this.height;
                ajK var8 = new ajK(var6, 0.0F, 0.0F, 0.0F, 0.0F, -var7, 0.0F, 0.0F, 0.0F, 0.0F, 1.0F, 0.0F, 0.0F, 1.0F, 0.0F, 1.0F);
                this.setTransform(var8);
            }
        }

    }

    public RenderAsset cloneAsset() {
        return this;
    }

    public void bind(DrawContext var1) {
        if (this.width != 0 && this.height != 0) {
            var1.incNumTextureBind();
            var1.getGL().glEnable(this.target);
            var1.getGL().glBindTexture(this.target, this.texID);
        }
    }

    public void bind(int var1, DrawContext var2) {
        var2.getGL().glActiveTexture(var1);
        this.bind(var2);
    }

    public void setSubData(int var1, int var2, int var3, int var4, Buffer var5) {
        if (var1 + var3 <= this.width && var2 + var4 <= this.height) {
            GL var6 = GLU.getCurrentGL();
            var6.glEnable(this.target);
            var6.glBindTexture(this.target, this.texID);
            var6.glTexSubImage2D(this.target, 0, var1, var2, var3, var4, this.internalFormat, 5121, var5);
            var6.glDisable(this.target);
        }

    }

    public void copyFromCurrentBuffer(GL var1) {
        var1.glActiveTexture(33984);
        var1.glEnable(this.target);
        var1.glBindTexture(this.target, this.texID);
        var1.glCopyTexSubImage2D(this.target, 0, 0, 0, 0, 0, this.internalWidth, this.internalHeight);
        var1.glDisable(this.target);
    }

    public void copyFromCurrentBuffer(GL var1, int var2, int var3, int var4, int var5) {
        var1.glActiveTexture(33984);
        var1.glEnable(this.target);
        var1.glBindTexture(this.target, this.texID);
        var1.glCopyTexSubImage2D(this.target, 0, var2, var3, var4, var5, this.internalWidth, this.internalHeight);
        var1.glDisable(this.target);
    }

    public void copyFromCurrentBuffer(GL var1, int var2, int var3, int var4, int var5, int var6, int var7) {
        var1.glActiveTexture(33984);
        var1.glEnable(this.target);
        var1.glBindTexture(this.target, this.texID);
        var1.glCopyTexSubImage2D(this.target, 0, var2, var3, var4, var5, var6, var7);
        var1.glDisable(this.target);
    }

    public void fillWithColor(Color var1) {
        ByteBuffer var2 = ByteBuffer.allocate(this.width * this.height * this.bpp / 8);
        int var3;
        if (this.bpp == 32) {
            for (var3 = 0; var3 < this.width * this.height * this.bpp / 8; var3 += 4) {
                var2.put((byte) ((int) (var1.x * 255.0F)));
                var2.put((byte) ((int) (var1.y * 255.0F)));
                var2.put((byte) ((int) (var1.z * 255.0F)));
                var2.put((byte) ((int) (var1.w * 255.0F)));
            }
        } else {
            if (this.bpp != 24) {
                throw new RuntimeException("Dont know how to_q fill an texture with bpp=" + this.bpp);
            }

            for (var3 = 0; var3 < this.width * this.height * this.bpp / 8; var3 += 3) {
                var2.put((byte) ((int) (var1.x * 255.0F)));
                var2.put((byte) ((int) (var1.y * 255.0F)));
                var2.put((byte) ((int) (var1.z * 255.0F)));
            }
        }

        var2.position(0);
        this.setSubData(0, 0, this.width, this.height, var2);
    }

    public int getTexId() {
        return this.texID;
    }

    static enum a {
        dIN,
        dIO;
    }
}
