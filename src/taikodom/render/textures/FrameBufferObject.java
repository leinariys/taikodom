package taikodom.render.textures;

import all.LogPrinter;
import taikodom.render.DrawContext;
import taikodom.render.enums.FBOAttachTarget;
import taikodom.render.gl.GLSupportedCaps;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;
import java.util.ArrayList;
import java.util.List;

public class FrameBufferObject {
    private static LogPrinter logger = LogPrinter.K(FrameBufferObject.class);
    private static int MAX_ATTACH_TEXTURES = 4;
    private int height;
    private int width;
    private int fboId;
    private int depthId;
    private boolean hasDepthBuffer = false;
    private List textures = new ArrayList();

    public static void unbind(DrawContext var0) {
        var0.getGL().glBindFramebufferEXT(36160, 0);
    }

    public boolean create(DrawContext var1, int var2, int var3, boolean var4) {
        if (!GLSupportedCaps.isFbo()) {
            return false;
        } else {
            this.releaseReferences();
            this.width = var2;
            this.height = var3;
            this.hasDepthBuffer = var4;
            GL var5 = var1.getGL();
            int[] var6 = new int[1];
            var5.glGenFramebuffersEXT(1, var6, 0);
            this.fboId = var6[0];
            if (this.hasDepthBuffer) {
                var5.glBindFramebufferEXT(36160, this.fboId);
                var5.glGenRenderbuffersEXT(1, var6, 0);
                this.depthId = var6[0];
                var5.glBindRenderbufferEXT(36161, this.depthId);
                var5.glRenderbufferStorageEXT(36161, 33190, this.width, this.height);
                var5.glFramebufferRenderbufferEXT(36160, 36096, 36161, this.depthId);
            }

            unbind(var1);
            return true;
        }
    }

    public void releaseReferences() {
        GL var1;
        int[] var2;
        if (this.fboId != 0) {
            var1 = GLU.getCurrentGL();
            var2 = new int[]{this.fboId};
            var1.glDeleteFramebuffersEXT(1, var2, 0);
            this.fboId = 0;
        }

        if (this.depthId != 0) {
            var1 = GLU.getCurrentGL();
            var2 = new int[]{this.depthId};
            var1.glDeleteRenderbuffersEXT(1, var2, 0);
            this.depthId = 0;
        }

        this.textures.clear();
    }

    public int getHeight() {
        return this.height;
    }

    public int getWidth() {
        return this.width;
    }

    public boolean isHasDepthBuffer() {
        return this.hasDepthBuffer;
    }

    public void bind(DrawContext var1) {
        var1.getGL().glBindFramebufferEXT(36160, this.fboId);
    }

    public boolean addDepthAttachTexture(DrawContext var1, BaseTexture var2, FBOAttachTarget var3) {
        var1.getGL();
        if (var2.getSizeX() == this.width && var2.getSizeY() == this.height) {
            this.bind(var1);
            var1.getGL().glFramebufferTexture2DEXT(36160, 36096, var3.glEquivalent(), var2.getTexId(), 0);
            unbind(var1);
            return true;
        } else {
            logger.error("Texture size is different from FBO size");
            return false;
        }
    }

    public boolean addColorAttachTexture(DrawContext var1, BaseTexture var2, FBOAttachTarget var3) {
        var1.getGL();
        if (var2.getSizeX() == this.width && var2.getSizeY() == this.height) {
            if (this.textures.size() == MAX_ATTACH_TEXTURES) {
                logger.error("Max attach textures reached in FBO");
                return false;
            } else {
                this.bind(var1);
                var1.getGL().glFramebufferTexture2DEXT(36160, '賠' + this.textures.size(), var3.glEquivalent(), var2.getTexId(), 0);
                unbind(var1);
                this.textures.add(var2);
                return true;
            }
        } else {
            logger.error("Texture size is different from FBO size");
            return false;
        }
    }

    public boolean checkStatus(DrawContext var1) {
        /**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
         * седержит методы для рисования примитивов (линии треугольники)*/
        GL var2 = var1.getGL();
        this.bind(var1);
        int var3 = var2.glCheckFramebufferStatusEXT(36160);
        unbind(var1);
        if (var3 != 36053) {
            if (var3 == 36061) {
                logger.error("GL_FRAMEBUFFER_UNSUPPORTED_EXT");
            } else {
                logger.error("Problem creating FBO");
            }

            this.releaseReferences();
            return false;
        } else {
            return true;
        }
    }
}
