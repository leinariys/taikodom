package taikodom.render.textures;

import all.ajK;
import com.hoplon.geometry.Vec3f;
import taikodom.render.DrawContext;

/**
 * Реализация класса указанного в имени тегов из файлов с расширением .pro
 */
public class PannedTexture extends Texture {
    protected final ajK panMatrix = new ajK();
    protected Texture texture;

    public void addArea(float var1) {
        if (this.texture != null) {
            this.texture.addArea(var1);
        }
    }

    public Texture getTexture() {
        return this.texture;
    }

    public void setTexture(Texture var1) {
        this.texture = var1;
    }

    public Vec3f getPanSpeed() {
        return new Vec3f(this.panMatrix.m03, this.panMatrix.m13, this.panMatrix.m23);
    }

    public void setPanSpeed(Vec3f var1) {
        this.panMatrix.m03 = var1.x;
        this.panMatrix.m13 = var1.y;
        this.panMatrix.m23 = var1.z;
    }

    public Vec3f getPanSpeed(Vec3f var1) {
        var1.x = this.panMatrix.m03;
        var1.y = this.panMatrix.m13;
        var1.z = this.panMatrix.m23;
        return var1;
    }

    public void bind(DrawContext var1) {
        if (this.texture != null) {
            this.texture.bind(var1);
        }

        var1.matrix4fTemp0.set(this.panMatrix);
        var1.matrix4fTemp0.m03 *= (float) var1.getTotalTime();
        var1.matrix4fTemp0.m13 *= (float) var1.getTotalTime();
        var1.matrix4fTemp0.m23 *= (float) var1.getTotalTime();
        var1.getGL().glMultMatrixf(var1.matrix4fTemp0.b(var1.tempBuffer0));
    }

    public void bind(int var1, DrawContext var2) {
        if (this.texture != null) {
            this.texture.bind(var1, var2);
        }

        var2.matrix4fTemp0.set(this.panMatrix);
        var2.matrix4fTemp0.m03 *= (float) var2.getTotalTime();
        var2.matrix4fTemp0.m13 *= (float) var2.getTotalTime();
        var2.matrix4fTemp0.m23 *= (float) var2.getTotalTime();
        var2.getGL().glMultMatrixf(var2.matrix4fTemp0.b(var2.tempBuffer0));
    }
}
