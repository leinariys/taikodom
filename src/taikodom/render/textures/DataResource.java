package taikodom.render.textures;

public class DataResource {
    boolean isReady = false;
    int degradationLevel = 0;
    int lifeTime;
    /**
     * Путь к файлу
     * Пример /data/gui/imageset/Splash_Screen.bik
     */
    String fileName;

    public int getDegradationLevel() {
        return this.degradationLevel;
    }

    public void setDegradationLevel(int var1) {
        this.degradationLevel = var1;
    }

    public void incDegradationLevel() {
        ++this.degradationLevel;
    }

    /**
     * Получить путь к файлу
     *
     * @return Пример /data/gui/imageset/Splash_Screen.bik
     */
    public String getFileName() {
        return this.fileName;
    }

    /**
     * Путь к файлу
     *
     * @param var1 Пример /data/gui/imageset/Splash_Screen.bik
     */
    public void setFileName(String var1) {
        this.fileName = var1;
    }

    public boolean isReady() {
        return this.isReady;
    }

    public void setReady(boolean var1) {
        this.isReady = var1;
    }

    public int getLifeTime() {
        return this.lifeTime;
    }

    public void setLifeTime(int var1) {
        this.lifeTime = var1;
    }

    public void incLifeTime(int var1) {
        this.lifeTime += var1;
    }
}
