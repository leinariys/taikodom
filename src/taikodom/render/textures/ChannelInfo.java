package taikodom.render.textures;

import all.aJF;
import taikodom.render.enums.TexEnvMode;

/**
 * Реализация класса указанного в имени тегов из файлов с расширением .pro
 */
public class ChannelInfo {
    protected int channelMapping;
    protected int texCoord;
    protected TexEnvMode texEnvMode;
    protected int texChannel;
    private boolean sTexCoordGeneration;
    private boolean tTexCoordGeneration;
    private boolean rTexCoordGeneration;
    private boolean qTexCoordGeneration;
    private aJF sEquation;
    private aJF tEquation;
    private aJF rEquation;
    private aJF qEquation;

    public ChannelInfo() {
        this.texEnvMode = TexEnvMode.MODULATE;
        this.sTexCoordGeneration = false;
        this.tTexCoordGeneration = false;
        this.rTexCoordGeneration = false;
        this.qTexCoordGeneration = false;
        this.sEquation = new aJF(1.0F, 0.0F, 0.0F, 1.0F);
        this.tEquation = new aJF(0.0F, 1.0F, 0.0F, 1.0F);
        this.rEquation = new aJF(0.0F, 0.0F, 1.0F, 1.0F);
        this.qEquation = new aJF(0.0F, 0.0F, 0.0F, 1.0F);
    }

    public boolean isAutoCoord() {
        return this.sTexCoordGeneration || this.tTexCoordGeneration || this.rTexCoordGeneration || this.qTexCoordGeneration;
    }

    public int getChannelMapping() {
        return this.channelMapping;
    }

    public void setChannelMapping(int var1) {
        this.channelMapping = var1;
    }

    /**
     * @deprecated
     */
    @Deprecated
    public int getGlChannel() {
        return this.channelMapping;
    }

    /**
     * @deprecated
     */
    @Deprecated
    public void setGlChannel(int var1) {
        this.channelMapping = var1;
    }

    public int getTexCoord() {
        return this.texCoord;
    }

    public void setTexCoord(int var1) {
        this.texCoord = var1;
    }

    /**
     * @deprecated
     */
    @Deprecated
    public int getTexCoordSet() {
        return this.texCoord;
    }

    /**
     * @deprecated
     */
    @Deprecated
    public void setTexCoordSet(int var1) {
        this.texCoord = var1;
    }

    public TexEnvMode getTexEnvMode() {
        return this.texEnvMode;
    }

    public void setTexEnvMode(TexEnvMode var1) {
        this.texEnvMode = var1;
    }

    /**
     * @deprecated
     */
    @Deprecated
    public TexEnvMode getEnvMode() {
        return this.texEnvMode;
    }

    /**
     * @deprecated
     */
    @Deprecated
    public void setEnvMode(TexEnvMode var1) {
        this.texEnvMode = var1;
    }

    public int getTexChannel() {
        return this.texChannel;
    }

    public void setTexChannel(int var1) {
        this.texChannel = var1;
    }

    public void assign(ChannelInfo var1) {
        this.channelMapping = var1.channelMapping;
        this.texCoord = var1.texCoord;
        this.texEnvMode = var1.texEnvMode;
        this.texChannel = var1.texChannel;
        this.sTexCoordGeneration = var1.sTexCoordGeneration;
        this.tTexCoordGeneration = var1.tTexCoordGeneration;
        this.rTexCoordGeneration = var1.rTexCoordGeneration;
        this.sEquation.set(var1.sEquation);
        this.tEquation.set(var1.tEquation);
        this.rEquation.set(var1.rEquation);
        this.qEquation.set(var1.rEquation);
    }

    public boolean isSTexCoordGeneration() {
        return this.sTexCoordGeneration;
    }

    public void setSTexCoordGeneration(boolean var1) {
        this.sTexCoordGeneration = var1;
    }

    public boolean isTTexCoordGeneration() {
        return this.tTexCoordGeneration;
    }

    public void setTTexCoordGeneration(boolean var1) {
        this.tTexCoordGeneration = var1;
    }

    public aJF getSEquation() {
        return this.sEquation;
    }

    public void setSEquation(aJF var1) {
        this.sEquation.set(var1);
    }

    public aJF getTEquation() {
        return this.tEquation;
    }

    public void setTEquation(aJF var1) {
        this.tEquation.set(var1);
    }

    public boolean isRTexCoordGeneration() {
        return this.rTexCoordGeneration;
    }

    public void setRTexCoordGeneration(boolean var1) {
        this.rTexCoordGeneration = var1;
    }

    public aJF getREquation() {
        return this.rEquation;
    }

    public void setREquation(aJF var1) {
        this.rEquation.set(var1);
    }

    public boolean isQTexCoordGeneration() {
        return this.qTexCoordGeneration;
    }

    public void setQTexCoordGeneration(boolean var1) {
        this.qTexCoordGeneration = var1;
    }

    public aJF getQEquation() {
        return this.qEquation;
    }

    public void setQEquation(aJF var1) {
        this.qEquation.set(var1);
    }
}
