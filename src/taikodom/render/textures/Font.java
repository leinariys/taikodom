package taikodom.render.textures;

import com.sun.opengl.util.BufferUtil;
import taikodom.render.DrawContext;
import taikodom.render.loader.RenderAsset;
import taikodom.render.loader.provider.BitmapFontFactory;

import javax.media.opengl.GL;
import java.awt.*;
import java.nio.IntBuffer;
import java.util.Vector;

public class Font extends RenderAsset {
    public static final String CACHE_DIRECTORY = "data/fontCache/";
    protected java.awt.Font awtFont;
    protected float size = -1.0F;
    protected int numOfValidChars = 0;
    BitmapFont bitmapFont;
    private DrawContext dc;
    private boolean discartedChars;
    private IntBuffer glyphs;

    public Font() {
        this.glyphs = IntBuffer.allocate(this.numOfValidChars / 4);
    }

    public boolean haveDiscartedChars() {
        return this.discartedChars;
    }

    public java.awt.Font getAwtFont() {
        return this.awtFont;
    }

    public void setAwtFont(java.awt.Font var1) {
        this.awtFont = var1;
        this.size = (float) var1.getSize();
        this.bitmapFont = BitmapFontFactory.getBitmapFont(var1);
    }

    private void destroyTexture() {
    }

    public Texture getTexture() {
        return this.bitmapFont.texture;
    }

    public RenderAsset cloneAsset() {
        return this;
    }

    public void bind(DrawContext var1) {
        if (this.bitmapFont.isReady()) {
            this.bitmapFont.texture.bind(var1);
        }
    }

    public void drawString(DrawContext var1, String var2, float var3, float var4) {
        if (this.bitmapFont.isReady()) {
            this.bind(var1);
            this.glyphs.clear();
            if (this.glyphs.capacity() < var2.length()) {
                this.glyphs = BufferUtil.newIntBuffer(var2.length());
            }

            for (int var5 = 0; var5 < var2.length(); ++var5) {
                this.glyphs.put(this.bitmapFont.getDisplayList(var1, var2.codePointAt(var5)));
            }

            this.glyphs.flip();
            /**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
             * седержит методы для рисования примитивов (линии треугольники)*/
            GL var6 = var1.getGL();
            var6.glPushMatrix();
            var6.glTranslatef(var3, var4 + (float) this.bitmapFont.getFontMetrics().getDescent(), 0.0F);
            var6.glCallLists(var2.length(), 5124, this.glyphs);
            var6.glPopMatrix();
        }
    }

    public float getSize() {
        return this.size;
    }

    public void setSize(float var1) {
        this.size = var1;
    }

    public DrawContext getDc() {
        return this.dc;
    }

    public void setDc(DrawContext var1) {
        this.dc = var1;
    }

    public FontMetrics getFontMetrics() {
        return this.bitmapFont.getFontMetrics();
    }

    public void setFontMetrics(FontMetrics var1) {
        this.bitmapFont.setFontMetrics(var1);
    }

    public int getNumOfValidChars() {
        return this.numOfValidChars;
    }

    public void setNumOfValidChars(int var1) {
        this.numOfValidChars = var1;
    }

    public Vector getCharWidths() {
        return this.bitmapFont.charWidth;
    }
}
