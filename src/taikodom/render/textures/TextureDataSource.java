package taikodom.render.textures;

import com.sun.opengl.util.texture.TextureData;

public class TextureDataSource extends DataResource {
    protected int width;
    protected int height;
    private float actualAreaSize;
    private long lastRequestTime;

    public long getLastRequestTime() {
        return this.lastRequestTime;
    }

    public void setLastRequestTime(long var1) {
        this.lastRequestTime = var1;
    }

    public int getWidth() {
        return this.width;
    }

    public void setWidth(int var1) {
        this.width = var1;
    }

    public int getHeight() {
        return this.height;
    }

    public void setHeight(int var1) {
        this.height = var1;
    }

    public int getBpp() {
        return 0;
    }

    public float getActualAreaSize() {
        return this.actualAreaSize;
    }

    public void setActualAreaSize(float var1) {
        this.actualAreaSize = var1;
    }

    public TextureData[] getData() {
        return null;
    }

    public void setData(TextureData var1) {
    }

    public TextureData getData(int var1) {
        return null;
    }

    public void clearData() {
    }
}
