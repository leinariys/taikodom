package taikodom.render.textures;

import com.sun.opengl.util.texture.TextureData;

public class Texture2DDataSource extends TextureDataSource {
    TextureData[] data = new TextureData[1];

    public TextureData[] getData() {
        return this.data;
    }

    public void setData(TextureData var1) {
        this.data[0] = var1;
    }

    public void clearData() {
        this.data[0] = null;
    }
}
