package taikodom.render.textures;

import taikodom.render.DrawContext;

/**
 * Реализация класса указанного в имени тегов из файлов с расширением .pro
 */
public class AnimatedTexture extends Texture {
    protected Texture texture;
    protected boolean looped = true;
    protected double startTime = -1.0D;
    protected float fps = 1.0F;
    protected int horizontalFrames = 1;
    protected int verticalFrames = 1;
    protected int totalFrames = 1;

    public void addArea(float var1) {
        if (this.texture != null) {
            this.texture.addArea(var1);
        }

    }

    public Texture getTexture() {
        return this.texture;
    }

    public void setTexture(Texture var1) {
        this.texture = var1;
    }

    public boolean getIsLooped() {
        return this.looped;
    }

    public void setIsLooped(boolean var1) {
        this.looped = var1;
    }

    public float getFps() {
        return this.fps;
    }

    public void setFps(float var1) {
        this.fps = var1;
    }

    public int getHorizontalFrames() {
        return this.horizontalFrames;
    }

    public void setHorizontalFrames(int var1) {
        this.horizontalFrames = var1;
        this.totalFrames = var1 * this.verticalFrames;
    }

    public int getVerticalFrames() {
        return this.verticalFrames;
    }

    public void setVerticalFrames(int var1) {
        this.verticalFrames = var1;
        this.totalFrames = this.horizontalFrames * var1;
    }

    public void bind(DrawContext var1) {
        if (this.texture != null) {
            this.texture.bind(var1);
        }

        this.applyMatrixToRender(var1);
    }

    public void bind(int var1, DrawContext var2) {
        if (this.texture != null) {
            this.texture.bind(var1, var2);
        }

        this.applyMatrixToRender(var2);
    }

    private void applyMatrixToRender(DrawContext var1) {
        if (this.startTime < 0.0D) {
            this.startTime = var1.getTotalTime();
        }

        int var2 = (int) ((var1.getTotalTime() - this.startTime) * (double) this.fps % (double) this.totalFrames);
        if (!this.looped && var2 >= this.totalFrames) {
            var2 = this.totalFrames - 1;
        }

        int var3 = var2 % this.verticalFrames;
        int var4 = (int) Math.floor((double) (var2 / this.horizontalFrames));
        var1.matrix4fTemp0.ceM();
        var1.matrix4fTemp0.m00 = 1.0F / (float) this.verticalFrames;
        var1.matrix4fTemp0.m11 = 1.0F / (float) this.horizontalFrames;
        var1.matrix4fTemp0.m03 = var1.matrix4fTemp0.m00 * (float) var3;
        var1.matrix4fTemp0.m13 = var1.matrix4fTemp0.m11 * (float) (this.horizontalFrames - var4 - 1);
        var1.getGL().glMultMatrixf(var1.matrix4fTemp0.b(var1.tempBuffer0));
    }
}
