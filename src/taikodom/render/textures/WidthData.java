package taikodom.render.textures;

public class WidthData {
    public int width;
    public int realWidth;

    public WidthData(int var1, int var2) {
        this.width = var1;
        this.realWidth = var2;
    }
}
