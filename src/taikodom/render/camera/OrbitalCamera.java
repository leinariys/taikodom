package taikodom.render.camera;

import all.Vector3dOperations;
import all.ajD;
import taikodom.render.StepContext;
import taikodom.render.scene.SceneObject;

public class OrbitalCamera extends Camera {
    protected final Vector3dOperations distance = new Vector3dOperations();
    protected float yaw;
    protected float pitch;
    protected float roll;
    protected float maxPitch;
    double maxZoomOut;
    double zoomFactor;
    double zoom;
    private SceneObject target;

    public OrbitalCamera() {
        this.distance.set(0.0D, 30.0D, 60.0D);
        this.maxPitch = 60.0F;
        this.maxZoomOut = 10.0D;
        this.zoomFactor = 1.0D;
    }

    public boolean isNeedToStep() {
        return true;
    }

    public Vector3dOperations getDistance() {
        return this.distance;
    }

    public void setDistance(Vector3dOperations var1) {
        if (var1.lengthSquared() < 0.01D) {
            this.distance.set(0.0D, 0.0D, 1.0D);
        }

        this.distance.aA(var1);
    }

    public double getZoomFactor() {
        return this.zoomFactor;
    }

    public void setZoomFactor(double var1) {
        this.zoomFactor = var1;
    }

    public float getRoll() {
        return this.roll;
    }

    public void setRoll(float var1) {
        this.roll = var1;
    }

    public float getMaxPitch() {
        return this.maxPitch;
    }

    public void setMaxPitch(float var1) {
        this.maxPitch = var1;
    }

    public double getZoom() {
        return this.zoom;
    }

    public void setZoom(double var1) {
        this.zoom = var1;
    }

    public float getYaw() {
        return this.yaw;
    }

    public void setYaw(float var1) {
        this.yaw = var1;
    }

    public float getPitch() {
        return this.pitch;
    }

    public void setPitch(float var1) {
        this.pitch = var1;
    }

    public SceneObject getTarget() {
        return this.target;
    }

    public void setTarget(SceneObject var1) {
        this.target = var1;
    }

    public double getMaxZoomOut() {
        return this.maxZoomOut;
    }

    public void setMaxZoomOut(double var1) {
        this.maxZoomOut = var1;
    }

    public int step(StepContext var1) {
        if (this.target == null) {
            return 0;
        } else {
            this.computeTransformation(var1.vec3dTemp0, var1.matrix3fTemp0, var1.matrix3fTemp1);
            return 0;
        }
    }

    public void computeTransformation() {
        this.computeTransformation((Vector3dOperations) null, (ajD) null, (ajD) null);
    }

    public void computeTransformation(Vector3dOperations var1, ajD var2, ajD var3) {
        if (var1 == null) {
            var1 = new Vector3dOperations();
        }

        if (var2 == null) {
            var2 = new ajD();
        }

        if (var3 == null) {
            var3 = new ajD();
        }

        var1.aA(this.distance);
        var1.normalize();
        var1.scale(this.zoom * this.zoomFactor);
        var1.add(this.distance);
        var2.setIdentity();
        var3.setIdentity();
        var3.rotateY(this.yaw * 0.017453292F);
        var2.rotateX(this.pitch * 0.017453292F);
        var3.mul(var2);
        var3.transform(var1);
        this.transform.mX.mul(this.target.getGlobalTransform().mX, var3);
        this.target.getGlobalTransform().b(var1, this.transform.position);
        this.updateInternalGeometry((SceneObject) null);
    }
}
