package taikodom.render.camera;

import all.Vector3dOperations;
import all.bc_q;
import com.hoplon.geometry.Vec3f;

import javax.vecmath.Tuple3d;
import javax.vecmath.Tuple3f;

public class ViewCameraControl {
    public static final int PERSPECTIVE_VIEW = 0;
    public static final int TOP_VIEW = 1;
    public static final int FRONT_VIEW = 2;
    public static final int SIDE_VIEW = 3;
    public static final int LIGHT_SHADER = 0;
    public static final int FLAT_SHADER = 1;
    public static final int WIREFRAME_SHADER = 2;
    protected Camera camera = new Camera();
    protected float orthoZoom = 1.0F;
    private int viewType = 0;
    private int shaderType = 1;
    private int windowWidth;
    private int windowHeigth;
    private float speed;
    private float distanceToPivot;
    private float camAngX = 0.0F;
    private float camAngY = 0.0F;
    private float camAngZ = 0.0F;

    public ViewCameraControl() {
        this.windowWidth = this.windowHeigth = 1;
        this.speed = 1.0F;
    }

    public Camera getCamera() {
        return this.camera;
    }

    public void setCamera(Camera var1) {
        this.camera = var1;
    }

    public void reset() {
        if (this.getViewType() == 0) {
            bc_q var1 = new bc_q();
            this.camAngY = 45.0F;
            var1.z(45.0F);
            this.camAngX = -15.0F;
            this.camAngZ = 0.0F;
            var1.y(-15.0F);
            this.setCameraTransform(var1);
            this.camera.setPosition(40.0F, 20.0F, 40.0F);
        } else {
            this.camera.setPosition(0.0F, 0.0F, 0.0F);
            this.setOrthoMode(this.viewType);
        }

        this.orthoZoom = 1.0F;
        this.updateCameraAttribs();
    }

    float getOrthoZoom() {
        return this.orthoZoom;
    }

    int getViewType() {
        return this.viewType;
    }

    public void setPosition(Vector3dOperations var1) {
        this.camera.setPosition(var1);
    }

    public void setWindowSize(int var1, int var2) {
        this.windowWidth = var1;
        this.windowHeigth = var2;
    }

    public void setOrthoMode(int var1) {
        this.viewType = var1;
        this.camera.setOrthogonal(true);
        Vector3dOperations var2 = this.camera.getPosition();
        bc_q var3 = new bc_q();
        if (this.viewType == 1) {
            var3.y(90.0F);
        } else if (this.viewType == 3) {
            var3.z(90.0F);
        }

        var3.setTranslation(var2);
        this.setCameraTransform(var3);
        this.updateCameraAttribs();
    }

    public void updateCameraAttribs() {
        float var1 = (float) this.windowWidth * 1.0F / ((float) this.windowHeigth * 1.0F);
        this.camera.setOrthoWindow(-this.orthoZoom * var1, this.orthoZoom * var1, -this.orthoZoom, this.orthoZoom);
        this.camera.setAspect(var1);
    }

    public void setPerspectiveMode() {
        this.viewType = 0;
        this.camera.setOrthogonal(false);
    }

    public void zoomIn(float var1) {
        if (var1 > 0.0F) {
            Vec3f var2;
            Vector3dOperations var3;
            if (this.viewType == 0) {
                var2 = this.camera.getTransform().getVectorZ();
                var3 = this.camera.getPosition();
                var2 = var2.mS(var1 * this.speed);
                var3 = var3.s((Tuple3f) var2.mS(var1));
                this.camera.setPosition(var3);
                this.distanceToPivot -= var1 * this.speed;
                if (this.distanceToPivot < 1.0F) {
                    this.distanceToPivot = 1.0F;
                }
            } else {
                this.orthoZoom /= 1.0F + var1 * this.speed;
                var2 = this.camera.getTransform().getVectorZ();
                var3 = this.camera.getPosition();
                var2 = var2.mS(var1);
                var3 = var3.s((Tuple3f) var2.mS(var1));
                this.camera.setPosition(var3);
                this.updateCameraAttribs();
            }

        }
    }

    public void zoomOut(float var1) {
        if (var1 > 0.0F) {
            Vec3f var2;
            Vector3dOperations var3;
            if (this.viewType == 0) {
                var2 = this.camera.getTransform().getVectorZ();
                var3 = this.camera.getPosition();
                var2 = var2.mS(var1 * this.speed);
                var3 = var3.q(var2.mS(var1));
                this.camera.setPosition(var3);
            } else {
                this.orthoZoom *= 1.0F + var1 * this.speed;
                var2 = this.camera.getTransform().getVectorZ();
                var3 = this.camera.getPosition();
                var2 = var2.mS(var1);
                var3 = var3.q(var2.mS(var1));
                this.camera.setPosition(var3);
                this.updateCameraAttribs();
            }

        }
    }

    public void pan(int var1, int var2) {
        Vec3f var3;
        Vec3f var4;
        Vector3dOperations var5;
        if (this.viewType == 0) {
            var3 = this.camera.getTransform().getVectorX();
            var4 = this.camera.getTransform().getVectorY();
            var3 = var3.mS((float) var1 * this.speed * 0.1F);
            var4 = var4.mS((float) var2 * this.speed * 0.1F);
            var5 = this.camera.getPosition();
            var5 = var5.q(var4.j(var3));
            this.camera.setPosition(var5);
        } else {
            var3 = this.camera.getTransform().getVectorX();
            var4 = this.camera.getTransform().getVectorY();
            var3 = var3.mS((float) var1 * this.orthoZoom * 0.01F * this.speed);
            var4 = var4.mS((float) var2 * this.orthoZoom * 0.01F * this.speed);
            var5 = this.camera.getPosition();
            var5 = var5.q(var4.j(var3));
            this.camera.setPosition(var5);
        }

    }

    public void rotate(int var1, int var2) {
        if (this.viewType == 0) {
            this.camAngX -= (float) var2 * 0.2F;
            this.camAngY -= (float) var1 * 0.2F;
            bc_q var3 = new bc_q();
            var3.y(this.camAngX);
            var3.z(this.camAngY);
            var3.A(this.camAngZ);
            var3.setTranslation(this.camera.getPosition());
            this.setCameraTransform(var3);
        }

    }

    private void setCameraTransform(bc_q var1) {
        double var2 = var1.position.lengthSquared();
        float var4 = var1.determinant3x3();
        if (var4 == var4 && var2 == var2 && !Double.isInfinite(var2)) {
            this.camera.setTransform(var1);
        } else {
            System.out.println("Invalid camera transform: " + var1 + " angles " + this.camAngX + ", " + this.camAngY + ", " + this.camAngZ);
            if (var4 != var4) {
                var1.mX.setIdentity();
            }

            if (var2 != var2 || Double.isInfinite(var2)) {
                var1.setTranslation(this.camera.getPosition());
            }

            if (Float.isNaN(this.camAngX) || Float.isInfinite(this.camAngX)) {
                this.camAngX = 0.0F;
            }

            if (Float.isNaN(this.camAngY) || Float.isInfinite(this.camAngY)) {
                this.camAngY = 0.0F;
            }

            if (Float.isNaN(this.camAngZ) || Float.isInfinite(this.camAngZ)) {
                this.camAngZ = 0.0F;
            }

            System.out.println("Using " + var1);
            this.camera.setTransform(var1);
        }

    }

    public float getSpeed() {
        return this.speed;
    }

    public void setSpeed(float var1) {
        if (var1 > 0.0F) {
            this.speed = var1;
        }

    }

    public int getWindowHeigth() {
        return this.windowHeigth;
    }

    public void setWindowHeigth(int var1) {
        this.windowHeigth = var1;
    }

    public int getWindowWidth() {
        return this.windowWidth;
    }

    public void setWindowWidth(int var1) {
        this.windowWidth = var1;
    }

    public Vec3f getWorldSpacePos(int var1, int var2) {
        Vec3f var3 = new Vec3f();
        float var4 = (float) this.windowWidth / (float) this.windowHeigth;
        if (this.viewType == 0) {
            return var3;
        } else {
            var3.x = this.orthoZoom * var4 * (float) (2 * var1 - this.windowWidth) / (float) this.windowWidth;
            var3.y = -this.orthoZoom * (float) (2 * var2 - this.windowHeigth) / (float) this.windowHeigth;
            this.camera.getTransform().multiply3x3(var3, var3);
            if (this.viewType == 1) {
                var3.y = 0.0F;
            } else if (this.viewType == 2) {
                var3.z = 0.0F;
            } else if (this.viewType == 3) {
                var3.x = 0.0F;
            }

            return var3;
        }
    }

    public int getShaderType() {
        return this.shaderType;
    }

    public void setShaderType(int var1) {
        this.shaderType = var1;
    }

    public void orbit(float var1, float var2, Vec3f var3) {
        if (this.viewType == 0) {
            this.camAngX -= var2;
            this.camAngY -= var1;
            Vec3f var4 = new Vec3f(0.0F, 0.0F, 0.0F);
            var4.x = this.distanceToPivot * (float) Math.sin((double) this.camAngY * 3.141592653589793D / 180.0D) * (float) Math.cos((double) this.camAngX * 3.141592653589793D / 180.0D);
            var4.z = this.distanceToPivot * (float) Math.cos((double) this.camAngY * 3.141592653589793D / 180.0D) * (float) Math.cos((double) this.camAngX * 3.141592653589793D / 180.0D);
            var4.y = -this.distanceToPivot * (float) Math.sin((double) this.camAngX * 3.141592653589793D / 180.0D);
            var4 = var4.i(var3);
            bc_q var5 = new bc_q();
            var5.z(this.camAngY);
            var5.y(this.camAngX);
            var5.A(this.camAngZ);
            var5.setTranslation(var4);
            this.setCameraTransform(var5);
        }

    }

    public void lookTo(Vector3dOperations var1) {
        if (this.viewType == 0) {
            Vector3dOperations var2 = var1.f((Tuple3d) this.camera.getPosition());
            this.distanceToPivot = (float) var2.length();
            var2.normalize();
            this.camAngX = 180.0F * (float) Math.asin(var2.y) / 3.1415927F;
            this.camAngY = 180.0F + 180.0F * (float) Math.atan2(var2.x, var2.z) / 3.1415927F;
            bc_q var3 = new bc_q();
            var3.z(this.camAngY);
            var3.y(this.camAngX);
            var3.A(this.camAngZ);
            var3.setTranslation(this.camera.getPosition());

            try {
                this.setCameraTransform(var3);
            } catch (Exception var4) {
                System.out.println("Invalid camera transform: " + var3 + " angles " + this.camAngX + ", " + this.camAngY + ", " + this.camAngZ + "  position: " + var1 + " camera position " + this.camera.getPosition());
            }
        }

    }

    public void orbitZoomOut(float var1, Vec3f var2) {
        if (var1 > 0.0F) {
            this.distanceToPivot += var1 * this.speed * 0.25F;
            this.orbit(0.0F, 0.0F, var2);
        }
    }

    public void orbitZoomIn(float var1, Vec3f var2) {
        if (var1 > 0.0F) {
            this.distanceToPivot -= var1 * this.speed * 0.25F;
            this.orbit(0.0F, 0.0F, var2);
        }
    }

    public float getDistanceToPivot() {
        return this.distanceToPivot;
    }

    public void setDistanceToPivot(float var1) {
        this.distanceToPivot = var1;
    }
}
