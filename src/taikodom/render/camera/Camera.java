package taikodom.render.camera;

import all.Vector3dOperations;
import all.ajK;
import com.hoplon.geometry.Vec3f;
import taikodom.render.DrawContext;
import taikodom.render.scene.SceneObject;

import javax.media.opengl.GL;
import javax.vecmath.Tuple3d;
import java.nio.FloatBuffer;

public class Camera extends SceneObject {
    private final ajK projection = new ajK();
    protected float fovY = 75.0F;
    /**
     * Коофециент ширина на высоту
     */
    private float aspect = 1.3333334F;
    private float farPlane = 400000.0F;
    private float nearPlane = 1.0F;
    private boolean isOrthogonal = false;
    private float left = -1.0F;
    private float right = 1.0F;
    private float bottom = -1.0F;
    private float top = 1.0F;
    private float tanHalfFovY;

    public Camera() {
        this.tanHalfFovY = (float) Math.tan((double) this.fovY * Math.PI / 360.0D);
    }

    public Camera cloneAsset() {
        return this;
    }

    public void setOrthoWindow(float var1, float var2, float var3, float var4) {
        this.left = var1;
        this.right = var2;
        this.bottom = var3;
        this.top = var4;
    }

    public ajK getProjection() {
        return this.projection;
    }

    /**
     * Самая дальняя точка относительно камеры, до которой идёт отрисовка.
     *
     * @return
     */
    public float getFarPlane() {
        return this.farPlane;
    }

    /**
     * Самая дальняя точка относительно камеры, до которой идёт отрисовка.
     *
     * @param var1
     */
    public void setFarPlane(float var1) {
        this.farPlane = var1;
    }

    /**
     * Ближайшая точка относительно камеры, от которой идёт отрисовка.
     *
     * @return
     */
    public float getNearPlane() {
        return this.nearPlane;
    }

    /**
     * Ближайшая точка относительно камеры, от которой идёт отрисовка.
     *
     * @param var1
     */
    public void setNearPlane(float var1) {
        this.nearPlane = var1;
    }

    /**
     * Коофециент ширина на высоту
     *
     * @return
     */
    public float getAspect() {
        return this.aspect;
    }

    /**
     * Коофециент ширина на высоту
     *
     * @param var1
     */
    public void setAspect(float var1) {
        this.aspect = var1;
    }

    public float getFovY() {
        return this.fovY;
    }

    public void setFovY(float var1) {
        this.fovY = var1;
        this.tanHalfFovY = (float) Math.tan((double) var1 * Math.PI / 360.0D);
    }

    public FloatBuffer getRawData() {
        return null;
    }

    public FloatBuffer getRawOrientation() {
        return null;
    }

    public void calculateProjectionMatrix() {
        if (this.isOrthogonal) {
            this.projection.a(2.0F / (this.right - this.left), 0.0F, 0.0F, 0.0F, 0.0F, 2.0F / (this.top - this.bottom), 0.0F, 0.0F, 0.0F, 0.0F, 1.0F / (this.farPlane - this.nearPlane), 0.0F, -(this.right + this.left) / (this.right - this.left), -(this.top + this.bottom) / (this.top - this.bottom), -this.nearPlane / (this.farPlane + this.nearPlane), 1.0F);
        } else {
            if ((double) this.getFovY() == 0.0D) {
                return;
            }

            float var4 = this.nearPlane * this.tanHalfFovY;
            float var3 = -var4;
            float var1 = var3 * this.aspect;
            float var2 = var4 * this.aspect;
            this.projection.a(2.0F * this.nearPlane / (var2 - var1), 0.0F, 0.0F, 0.0F, 0.0F, 2.0F * this.nearPlane / (var4 - var3), 0.0F, 0.0F, (var2 + var1) / (var2 - var1), (var4 + var3) / (var4 - var3), -((this.farPlane + this.nearPlane) / (this.farPlane - this.nearPlane)), -1.0F, 0.0F, 0.0F, -(2.0F * this.farPlane * this.nearPlane / (this.farPlane - this.nearPlane)), 0.0F);
        }
    }

    public void updateGLProjection(DrawContext var1) {/**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
     * седержит методы для рисования примитивов (линии треугольники)*/
        GL var2 = var1.getGL();
        var2.glMatrixMode(5889);
        var2.glLoadIdentity();
        if (!this.isOrthogonal) {
            var1.glu().gluPerspective((double) this.getFovY(), (double) this.getAspect(), (double) this.getNearPlane(), (double) this.getFarPlane());
        } else {
            var1.getGL().glOrtho((double) this.left, (double) this.right, (double) this.bottom, (double) this.top, (double) (-this.nearPlane), (double) this.farPlane);
        }
        var2.glMatrixMode(5888);
    }

    public boolean isOrthogonal() {
        return this.isOrthogonal;
    }

    public void setOrthogonal(boolean var1) {
        this.isOrthogonal = var1;
    }

    public float getTanHalfFovY() {
        return this.tanHalfFovY;
    }

    public void lookAt(Vector3dOperations var1) {
        Vec3f var2 = var1.f((Tuple3d) this.getPosition()).dfX().getVec3f();
        Vec3f var3 = new Vec3f(0.0F, 1.0F, 0.0F);
        Vec3f var4 = var2.b(var3);
        var4.dfP();
        var3.set(var4.b(var2));
        this.transform.setX(var4);
        this.transform.setY(var3);
        this.transform.setZ(var2.dfT());
    }
}
