package taikodom.render;

import javax.media.opengl.GL;

/**
 * Эксклюзивный видеопроигрыватель
 * Для заставки
 */
public class ExclusiveVideoPlayer {
    /**
     * Заставка для проигрывания
     */
    Video video;
    boolean playing = false;

    /**
     * Установить видео для проигрывания
     *
     * @param var1
     */
    public void playVideo(Video var1) {
        this.video = var1;
        this.playing = true;
    }

    public void renderFrame(DrawContext var1) {
        GL var2 = var1.getGL();
        var2.glMatrixMode(5888);
        var2.glPushMatrix();
        var2.glLoadIdentity();
        var2.glMatrixMode(5889);
        var2.glPushMatrix();
        var2.glLoadIdentity();
        var2.glViewport(0, 0, var1.getScreenWidth(), var1.getScreenHeight());
        var2.glDisable(2929);
        var2.glOrtho(0.0D, (double) var1.getScreenWidth(), (double) var1.getScreenHeight(), 0.0D, -1.0D, 1.0D);
        var2.glMatrixMode(5888);
        var2.glDisable(2884);
        this.video.update(var1);
        this.video.render(var1);
        if (this.video.isEnded()) {
            this.playing = false;
            var1.postSceneEvent("endVideo", this);
        }

        var2.glPopMatrix();
        var2.glMatrixMode(5889);
        var2.glPopMatrix();
        var2.glMatrixMode(5888);
    }

    public boolean isPlaying() {
        return this.playing;
    }

    public void stopVideo() {
        this.playing = false;
        this.video.stop();
    }
}
