package taikodom.render.raytrace;

import all.Vector3dOperations;
import all.aDM;
import taikodom.render.scene.SceneObject;

public class RayTraceSceneObjectInfoD extends aDM {
    private Vector3dOperations intersectedPoint = new Vector3dOperations();
    private SceneObject intersectedObject = null;
    private boolean changed = false;

    public RayTraceSceneObjectInfoD(Vector3dOperations var1, Vector3dOperations var2) {
        super(var1, var2);
    }

    public Vector3dOperations getIntersectedPoint() {
        return this.intersectedPoint;
    }

    public void setIntersectedPoint(Vector3dOperations var1) {
        this.intersectedPoint.aA(var1);
    }

    public SceneObject getIntersectedObject() {
        return this.intersectedObject;
    }

    public void setIntersectedObject(SceneObject var1) {
        this.intersectedObject = var1;
    }

    public boolean isChanged() {
        return this.changed;
    }

    public void setChanged(boolean var1) {
        this.changed = var1;
    }
}
