package taikodom.render;

//import all.OJ;

import all.ajK;
import org.lwjgl.BufferUtils;
import taikodom.render.enums.TexMagFilter;
import taikodom.render.enums.TexMinFilter;
import taikodom.render.loader.provider.ImageLoader;
import taikodom.render.loader.provider.ResourceCleaner;
import taikodom.render.textures.BinkDataSource;
import taikodom.render.textures.Texture;

import javax.media.opengl.GL;
import java.nio.Buffer;
import java.util.concurrent.Semaphore;

//import all.fN;

public class BinkTexture extends Texture {
    public static final int BINKSURFACE32RA = 6;
    public static final int BINKSURFACE24R = 2;
    public static final long BINKCOPYALL = 2147483648L;
    public static final long BINKNOSKIP = 524288L;
    public static final long BINKFROMMEMORY = 67108864L;
    public static final long BINKPRELOADALL = 8192L;
    public static final long BINKALPHA = 1048576L;
    Semaphore binkLock = new Semaphore(1);
    int numFrames = 0;
    private short pixelSize;
    private short pitch;
    private short videoX;
    private short videoY;
    private int pixelFormat;
    private Buffer pixelBuffer;
    private boolean isPlaying;
    private boolean isEnded;
    private boolean copyBinkTextureToBuffer;
    private int texId;

    public BinkTexture() {
    }

    public BinkTexture(BinkDataSource var1) {
        this.dataSource = var1;
        this.processData();
        this.copyBinkTextureToBuffer = true;
    }

    public boolean isPlaying() {
        return this.isPlaying;
    }

    private void updateTexture(DrawContext var1) {   /**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
     * седержит методы для рисования примитивов (линии треугольники)*/
        GL var2 = var1.getGL();
        var2.glPixelStorei(3314, this.pitch / this.pixelSize);
        var2.glPixelStorei(3317, 8);
        var2.glTexSubImage2D(this.target, 0, 0, 0, this.videoX, this.videoY, this.pixelFormat, 5121, this.pixelBuffer);
    }

    void processData() {
        this.videoX = (short) ((BinkDataSource) this.dataSource).getWidth();
        this.videoY = (short) ((BinkDataSource) this.dataSource).getHeight();
        this.target = 3553;
        this.width = this.videoX;
        this.height = this.videoY;
        if (!isPowerOf2(this.videoX) || !isPowerOf2(this.videoY)) {
            this.width = (short) Texture.roundToPowerOf2(this.videoX);
            this.height = (short) Texture.roundToPowerOf2(this.videoY);
        }

        int var1 = 0;//(int)(((BinkDataSource)this.dataSource).getBink().sV() & 1048576L);
        this.pixelFormat = var1 != 0 ? 6408 : 6407;
        this.pixelSize = (short) (var1 != 0 ? 4 : 3);
        this.pitch = (short) (this.videoX * this.pixelSize);
        this.pixelBuffer = BufferUtils.createByteBuffer(this.pitch * this.height);
        this.bpp = this.pixelSize * 8;
    }

    /**
     * @param var1
     */
    private void createTexture(DrawContext var1) {
        /**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
         * седержит методы для рисования примитивов (линии треугольники)*/
        GL var2 = var1.getGL();
        int[] var3 = new int[1];
        var2.glEnable(this.target);
        var2.glGenTextures(1, var3, 0);
        this.texId = var3[0];
        var2.glBindTexture(this.target, this.texId);
        var2.glTexParameteri(this.target, 10242, this.wrapS.glEquivalent());
        var2.glTexParameteri(this.target, 10243, this.wrapT.glEquivalent());
        if (this.minFilter != TexMinFilter.NONE) {
            var2.glTexParameteri(this.target, 10241, this.minFilter.glEquivalent());
        } else {
            var2.glTexParameteri(this.target, 10241, 9729);
        }

        if (this.magFilter != TexMagFilter.NONE) {
            var2.glTexParameteri(this.target, 10240, this.magFilter.glEquivalent());
        } else {
            var2.glTexParameteri(this.target, 10240, 9729);
        }

        var2.glTexImage2D(this.target, 0, this.pixelFormat, this.width, this.height, 0, this.pixelFormat, 5121, (Buffer) null);
        float var4 = (float) this.videoX / (float) this.width;
        float var5 = (float) this.videoY / (float) this.height;
        ajK var6 = new ajK(var4, 0.0F, 0.0F, 0.0F, 0.0F, var5, 0.0F, 0.0F, 0.0F, 0.0F, 1.0F, 0.0F, 0.0F, 1.0F - var5, 0.0F, 1.0F);
        this.setTransform(var6);
    }

    public void play() {
        // OJ.all(((BinkDataSource)this.dataSource).getBink(), 0);
        this.isPlaying = true;
    }

    public void bind(DrawContext var1) {
        try {
            this.binkLock.acquire();
        } catch (InterruptedException var4) {
            var4.printStackTrace();
        }

        if (!this.initialized) {
            ResourceCleaner.addTexture(this);
            this.initialized = true;
        }

        this.setLastBindTime(System.currentTimeMillis());
        if (this.texId == 0) {
            this.createTexture(var1);
            this.isPlaying = true;
        }

        if (!this.initialized) {
            ResourceCleaner.addTexture(this);
            this.initialized = true;
        }

        /**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
         * седержит методы для рисования примитивов (линии треугольники)*/
        GL var2 = var1.getGL();
        var2.glEnable(this.target);
        var2.glBindTexture(this.target, this.texId);
      /*
      if (((BinkDataSource)this.dataSource).getBink() == null)
      {
         this.binkLock.release();
      } else {
         if (this.isPlaying || this.copyBinkTextureToBuffer)
         {
            if (OJ.CreateJComponent(((BinkDataSource)this.dataSource).getBink()) == 0 || this.copyBinkTextureToBuffer)
            {
               OJ.setGreen(((BinkDataSource)this.dataSource).getBink());
               if (OJ.parseSelectors(((BinkDataSource)this.dataSource).getBink()) == 0 || this.copyBinkTextureToBuffer)
               {
                  int var3 = OJ.all(((BinkDataSource)this.dataSource).getBink(), this.pixelBuffer, this.pitch, (long)this.videoY, 0L, 0L, (long)(this.pixelFormat == 6408 ? 6 : 2) | 2147483648L | 524288L);
                  if (var3 == 0)
                  {
                     this.updateTexture(var1);
                     this.copyBinkTextureToBuffer = false;
                  }
               }

               if (this.isPlaying) {
                  OJ.CreateJComponent(((BinkDataSource)this.dataSource).getBink());
               }
            }

            if (((BinkDataSource)this.dataSource).getBink().sQ() == ((BinkDataSource)this.dataSource).getBink().sP()) {
               this.isPlaying = false;
               this.isEnded = true;
               var1.postSceneEvent("endVideo", this);
            }
         }

         this.binkLock.release();
      }
      */
    }

    public void finalize() {
    }

    public void releaseReferences() {
        try {
            this.binkLock.acquire();
        } catch (InterruptedException var2) {
            var2.printStackTrace();
        }
/*
      if (((BinkDataSource)this.dataSource).getBink() == null) {
         this.binkLock.release();
      } else {
         ((BinkDataSource)this.dataSource).flush();
         this.binkLock.release();
      }*/
    }

    public int getVideoSizeX() {
        return this.videoX;
    }

    public int getVideoSizeY() {
        return this.videoY;
    }

    public int getTexId() {
        return this.texId;
    }

    public int getType() {
        return this.target;
    }

    /*
       public fN getBink() {
          return ((BinkDataSource)this.dataSource).getBink();
       }
    */
/*
   public void rewind() {
      OJ.all(((BinkDataSource)this.dataSource).getBink(), 1L, 0);
      this.copyBinkTextureToBuffer = true;
   }
*/
/*
   public void gotoFrame(long var1) {
      OJ.all(((BinkDataSource)this.dataSource).getBink(), var1, 0);
      this.copyBinkTextureToBuffer = true;
   }
*/
/*
   public void setBink(fN var1) {
      ((BinkDataSource)this.dataSource).setBink(var1);
      this.processData();
      this.copyBinkTextureToBuffer = true;
   }
*/
/*
   public void pause() {
      OJ.all(((BinkDataSource)this.dataSource).getBink(), 1);
      this.isPlaying = false;
   }
*/
    public void destroyInternalTexture() {
        this.releaseReferences();
        this.initialized = false;
        ImageLoader.destroyTexture(this);
    }

    public int getMemorySize() {
        return this.width * this.height * this.pixelSize;
    }

    public boolean isEnded() {
        return this.isEnded;
    }
}
