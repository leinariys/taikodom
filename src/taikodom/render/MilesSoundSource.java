package taikodom.render;

import all.LogPrinter;
import com.hoplon.geometry.Vec3f;
import org.lwjgl.BufferUtils;
import taikodom.render.scene.SoundSource;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

//import all.Vb;
//import all.aDx;
//import all.afz;

public class MilesSoundSource extends SoundSource {
    public static final long SMP_PLAYING = 4L;
    public static final long SMP_DONE = 2L;
    public static final long SMP_STOPPED = 8L;
    static LogPrinter logger = LogPrinter.K(MilesSoundSource.class);
    protected FloatBuffer tempFloat = BufferUtils.createFloatBuffer(1);
    protected IntBuffer tempInt = BufferUtils.createIntBuffer(1);
    //   protected Vb milesSample;
    boolean isFadingOut;
    MilesSoundBuffer soundBuffer;
    int xOffset;
    int yOffset;
    int loop;
    boolean outRanged;

    public void finalize() {
        this.releaseReferences();
    }

    public void releaseReferences() {
        this.releaseSample();
    }

    public boolean setBuffer(SoundBuffer var1) {
        this.soundBuffer = (MilesSoundBuffer) var1;
        return true;
    }

    public SoundBuffer getBuffer() {
        return this.soundBuffer;
    }

    private boolean shouldPlay() {
        if ((float) numSoundSources > (float) SoundMiles.getInstance().getMaxSourceNumber() * 0.85F) {
            return this.soundBuffer != null && this.soundBuffer.getTimeLength() < 1.0F;
        } else if ((float) numSoundSources > (float) SoundMiles.getInstance().getMaxSourceNumber() * 0.7F) {
            return this.soundBuffer != null && this.soundBuffer.getTimeLength() < 2.5F;
        } else {
            return true;
        }
    }

    /*
       public boolean create() {
          if (this.soundBuffer == null) {
             return false;
          } else if (numSoundSources >= SoundMiles.getInstance().getMaxSourceNumber()) {
             return false;
          } else if (!this.shouldPlay()) {
             return false;
          } else {
             if (this.milesSample == null) {
                try {
                   this.milesSample = afz.parseStyleSheet(SoundMiles.getInstance().getDig());
                } catch (Exception var2) {
                   var2.printStackTrace();
                }
             }

             if (this.milesSample == null) {
                return false;
             } else {
                ++numSoundSources;
                if (afz.all(this.milesSample, this.soundBuffer.getExtension(), this.soundBuffer.getData(), (long)this.soundBuffer.getLength(), 0) == 0) {
                   afz.all(this.milesSample, 0, 0L);
                   afz.all(this.milesSample, this.soundBuffer.getData(), (long)this.soundBuffer.getLength());
                   afz.all((Vb)this.milesSample, 44100);
                }

                return true;
             }
          }
       }
    */
    public boolean play(int var1) {
    /*  if (this.milesSample == null) {
         logger.debug("trying to_q play all sound without create it before.");
         return false;
      } else {
         afz.setGreen(this.milesSample, var1);
         this.loop = var1;
         if (!this.isOutRange()) {
            afz.endPage(this.milesSample);
         }

         return true;
      }*/
        return true;
    }

    /*
       public boolean setLoop(int var1) {
          if (this.milesSample == null) {
             return false;
          } else {
             afz.setGreen(this.milesSample, var1);
             return true;
          }
       }
    *//*
   public boolean stop() {
      if (this.milesSample == null) {
         return false;
      } else {
         afz.parseStyleDeclaration(this.milesSample);
         return true;
      }
   }
*//*
   public boolean fadeOut(float var1) {
      if (this.milesSample == null) {
         return false;
      } else if (afz.AIL_find_filter("Volume Ramp Filter", this.tempInt) != 0) {
         this.isFadingOut = true;
         int var2 = this.tempInt.get(0);
         afz.all(this.milesSample, aDx.hBQ, (long)var2);
         float var3 = 0.0F;
         afz.all(this.milesSample, aDx.hBQ, (long)var2);
         afz.all((Vb)this.milesSample, (Buffer)this.tempFloat, (Buffer)null);
         float var4 = this.tempFloat.get(0);
         afz.all(this.milesSample, aDx.hBQ, var4);
         var3 = var1 * 1000.0F;
         afz.setGreen(this.milesSample, aDx.hBQ, var3);
         var4 = 0.0F;
         afz.all(this.milesSample, aDx.hBQ, var4);
         return true;
      } else {
         return false;
      }
   }
*//*
   public boolean resume() {
      if (this.milesSample == null) {
         return false;
      } else {
         afz.parseStyleSheet(this.milesSample);
         return true;
      }
   }
*/
    boolean isOutRange() {
        return false;
    }

    public void ValidadePosition() {
    }

    /*
       public void setPosition(float var1, float var2, float var3) {
          afz.setGreen(this.milesSample, var1, var2, var3);
       }
    */
    public void setPosition(Vec3f var1) {
        this.setPosition(var1.x, var1.y, var1.z);
    }

    public void setVelocity(Vec3f var1) {
     /* if (this.milesSample != null) {
         afz.CreateJComponent(this.milesSample, var1.x, var1.y, var1.z);
      }*/
    }

    /*
       public boolean setGain(float var1) {
          if (this.milesSample == null) {
             return false;
          } else {
             afz.setGreen(this.milesSample, var1, var1);
             return true;
          }
       }
    *//*
   public boolean setMinMaxDistance(float var1, float var2) {
      if (this.milesSample == null) {
         return false;
      } else {
         afz.all(this.milesSample, var2, var1, 1);
         return true;
      }
   }
*//*
   public boolean setPitch(float var1) {
      if (this.milesSample != null && this.soundBuffer != null) {
         afz.all(this.milesSample, (int)(var1 * (float)this.soundBuffer.getSampleRate()));
         return true;
      } else {
         return false;
      }
   }
*//*
   public boolean isPlaying() {
      if (this.milesSample == null) {
         return false;
      } else {
         return afz.parseRule(this.milesSample) == 4L;
      }
   }
*//*
   public int step(StepContext var1) {
      if (this.milesSample != null) {
         int var2 = (int)afz.parseRule(this.milesSample);
         if ((long)var2 == 2L) {
            this.releaseSample();
            return 1;
         } else {
            if (this.isFadingOut) {
               this.tempFloat.put(0, 1.0F);
               afz.all(this.milesSample, (aDx)aDx.hBQ, (String)"Ramp At", (Buffer)this.tempFloat, (Buffer)null, (Buffer)null);
               if (this.tempFloat.get(0) <= 0.0F) {
                  this.releaseSample();
                  this.isFadingOut = false;
                  return 1;
               }
            }

            return 0;
         }
      } else {
         return 1;
      }
   }
*//*
   public int getSamplePositionMs() {
      if (this.milesSample == null) {
         return 0;
      } else {
         afz.setGreen((Vb)this.milesSample, (Buffer)null, (Buffer)this.tempInt);
         return this.tempInt.get(0);
      }
   }
*//*
   public void setSamplePositionMs(int var1) {
      if (this.milesSample != null) {
         afz.CreateJComponent(this.milesSample, var1);
      }
   }
*//*
   public boolean releaseSample() {
      if (this.milesSample != null) {
         afz.parseStyleDeclaration(this.milesSample);
         afz.CreateJComponentAndAddInRootPane(this.milesSample);
         this.milesSample = null;
         --numSoundSources;
         return true;
      } else {
         return false;
      }
   }
*//*
   public void setMilesSample(Vb var1) {
      this.milesSample = var1;
   }
*/
    public float getTimeLenght() {
        return this.soundBuffer != null ? this.soundBuffer.getTimeLength() : 0.0F;
    }
}
