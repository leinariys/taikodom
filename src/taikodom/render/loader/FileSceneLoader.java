package taikodom.render.loader;

import all.FileControl;
import all.ain;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * Загрузчик ресурсов файлов с расширением .pro
 */
public class FileSceneLoader extends SceneLoader {
    protected ain baseDir;

    /**
     * Создание загрузчика файлов счены
     *
     * @param var1 set baseDir
     */
    public FileSceneLoader(String var1) {
        this((ain) (new FileControl(var1)));
    }

    /**
     * Создание загрузчика файлов счены
     *
     * @param var1 set baseDir
     */
    public FileSceneLoader(ain var1) {
        this.setBaseDir(var1.getAbsoluteFile());
    }

    /**
     * @return
     */
    public ain getBaseDir() {
        return this.baseDir;
    }

    public void setBaseDir(File var1) {
        this.setBaseDir((ain) (new FileControl(var1)));
    }

    /**
     * @param var1
     */
    public void setBaseDir(ain var1) {
        this.baseDir = var1;
        this.imageLoader.setBasePath(var1);
    }

    /**
     * Поставить файл в очередь на загрузку
     *
     * @param var1 Локальный адрес ресурса
     * @throws IOException
     */
    public void loadFile(String var1) throws IOException {
        ain var2 = this.baseDir.concat(var1);//соединение адреса с базовой директорией
        this.loadFile(var2);
    }

    /**
     * Поставить файл в очередь на загрузку
     *
     * @param var1
     * @throws IOException
     */
    public void loadFile(ain var1) throws IOException {
        String var2 = var1.getPath();//полный путь к ресурсу
        String var3 = var1.getPath().substring(this.baseDir.getPath().length() + 1).replace('\\', '/');//окончание пути
        InputStream var4 = var1.getInputStream();
        this.loadFile(var4, var3, var2);//перейти к загрузке файла
        var4.close();
    }

    /**
     * Приступить к загрузке файлов из Список файлов для загрузки из раздела  includes
     *
     * @throws IOException
     */
    public void loadMissing() throws IOException {
        while (this.getMissingIncludes().size() > 0) {
            String var1 = (String) this.getMissingIncludes().iterator().next();
            this.loadFile(var1);
        }
    }

    public void removeEntry(StockEntry var1) {
        this.stockEntryCache.remove(var1);
    }
}
