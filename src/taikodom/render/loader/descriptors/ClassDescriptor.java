package taikodom.render.loader.descriptors;

import java.util.Collection;

/**
 * Базовый класс обёртки
 * class ReflectiveClassDescriptor
 */
public abstract class ClassDescriptor {
    /**
     * создать экземпляр класса
     *
     * @return
     */
    public abstract Object createInstance();

    public abstract PropertyDescriptor getProperty(String var1);

    public abstract String getName();

    public abstract Collection getProperties();
}
