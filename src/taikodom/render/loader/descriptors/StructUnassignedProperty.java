package taikodom.render.loader.descriptors;

import taikodom.render.loader.SceneLoader;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class StructUnassignedProperty extends UnassignedPropertyBase {
    private final Object object;
    public List subProperties = new ArrayList();

    public StructUnassignedProperty(SceneLoader var1, PropertyDescriptor var2, int var3, Object var4) {
        super(var2, var3);
        this.object = var4;
    }

    public boolean doAssign(SceneLoader var1, Object var2) {
        Iterator var4 = this.subProperties.iterator();

        while (var4.hasNext()) {
            UnassignedPropertyBase var3 = (UnassignedPropertyBase) var4.next();
            var3.doAssign(var1, this.object);
        }

        return this.descriptor.set(var2, this.index, this.object);
    }
}
