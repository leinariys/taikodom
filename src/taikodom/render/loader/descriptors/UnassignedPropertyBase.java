package taikodom.render.loader.descriptors;

import taikodom.render.loader.SceneLoader;

/**
 * Обёртка атрибута тега из файла .pro
 * Имя атрибута, Формат значения атрибута, его значения и позиция в списке
 */
public class UnassignedPropertyBase extends UnassignedProperty {
    /**
     * Формат значения атрибута
     */
    protected final PropertyDescriptor descriptor;
    /**
     * Позиция в списке
     */
    protected final int index;
    /**
     * Значение атрибута
     */
    protected final String value;

    public UnassignedPropertyBase(PropertyDescriptor var1) {
        this(var1, (String) null, -1);
    }

    public UnassignedPropertyBase(PropertyDescriptor var1, int var2) {
        this(var1, (String) null, var2);
    }

    public UnassignedPropertyBase(PropertyDescriptor var1, String var2) {
        this(var1, var2, -1);
    }

    /**
     * @param var1 Имя атрибута и формат значения
     * @param var2 Значение атрибута
     * @param var3 Позиция в списке
     */
    public UnassignedPropertyBase(PropertyDescriptor var1, String var2, int var3) {
        this.descriptor = var1;
        this.value = var2;
        this.index = var3;
    }

    public boolean doAssign(SceneLoader var1, Object var2) {
        return this.descriptor.setAsString(var2, this.value);
    }

    public boolean doAssignToNewInstance(SceneLoader var1, Object var2) {
        return false;
    }

    public String getName() {
        return this.descriptor.getName();
    }

    public String getValue() {
        return this.value;
    }

    public String toString() {
        return this.getClass().getSimpleName() + ": " + this.descriptor.getName() + "=" + this.value;
    }
}
