package taikodom.render.loader.descriptors;

import all.LogPrinter;
import taikodom.render.loader.RenderAsset;
import taikodom.render.loader.SceneLoader;

import java.util.List;

public class ObjectUnassignedProperty extends UnassignedPropertyBase {
    private static LogPrinter logger = LogPrinter.K(ObjectUnassignedProperty.class);
    public List subProperties;

    public ObjectUnassignedProperty(PropertyDescriptor var1, String var2, boolean var3, int var4, RenderAsset var5, boolean var6) {
        super(var1, var2, var4);
    }

    public boolean doAssign(SceneLoader var1, Object var2) {
        RenderAsset var3 = var1.getAsset(this.value);
        return this.descriptor.set(var2, this.index, var3);
    }
}
