package taikodom.render.loader.descriptors;

import taikodom.render.loader.SceneLoader;

public abstract class UnassignedProperty {
    public abstract boolean doAssign(SceneLoader var1, Object var2);

    public abstract boolean doAssignToNewInstance(SceneLoader var1, Object var2);

    public abstract String getName();

    public abstract String getValue();
}
