package taikodom.render.loader.descriptors;

import taikodom.render.loader.RenderAsset;

/**
 * Обёртка атрибута тега из файла .pro
 * Имя атрибута и формат значения
 */
public abstract class PropertyDescriptor {
    /**
     * Имя атрибута
     */
    protected String name;
    /**
     * Тип значения Object
     */
    protected boolean isObject;
    /**
     * Тип значения Array
     */
    protected boolean isArray;
    /**
     * Тип значения в формате String, это может быть и String Например String
     */
    protected String propertyTypeName;

    public abstract Object getAsObject(RenderAsset var1);

    public abstract Object getObjectArrayItem(RenderAsset var1, int var2);

    public abstract boolean hasDefaultValue(RenderAsset var1);

    public abstract int arrayItemCount(RenderAsset var1);

    public abstract String getAsString(RenderAsset var1);

    public boolean isObject() {
        return this.isObject;
    }

    public boolean isObjectArray() {
        return this.isArray && this.isObject;
    }

    public boolean isStructArray() {
        return this.isArray && !this.isObject;
    }

    /**
     * Получить тип данных трибута
     *
     * @return
     */
    public String getPropertyTypeName() {
        return this.propertyTypeName;
    }

    /**
     * Получить имя атрибута
     *
     * @return
     */
    public String getName() {
        return this.name;
    }

    public abstract boolean setAsString(Object var1, String var2);

    public abstract boolean set(Object var1, int var2, Object var3);
}
