package taikodom.render.loader.provider;

import all.Hm;
import all.LogPrinter;
import taikodom.render.MilesSoundBuffer;
import taikodom.render.MilesSoundStream;
import taikodom.render.SoundBuffer;
import taikodom.render.loader.FormatProvider;
import taikodom.render.loader.SceneLoader;
import taikodom.render.loader.StockFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.*;

public class SoundBufferLoader extends FormatProvider implements Runnable {
    private static LogPrinter logger = LogPrinter.K(SoundBufferLoader.class);
    private static Set toLoadList = new HashSet();
    private static List loadingList = new ArrayList();
    private static int THREAD_INTERVAL = 200;
    private static int INTERVAL_BETWEEN_ENTRIES = 10;
    private boolean shouldStop = false;

    public SoundBufferLoader(SceneLoader var1, String... var2) {
        super(var1, var2);
        Thread var3 = new Thread(this, "SoundBufferLoader");
        var3.setDaemon(true);
        var3.start();
    }

    private static void loadBuffer(SoundBuffer var0) throws FileNotFoundException {
        File var1 = new File(var0.getFileName());
        if (!var1.exists()) {
            throw new FileNotFoundException(var0.getFileName());
        } else {
            ByteBuffer var2 = Hm.getByteBuffer(var1);
            var0.setData(var2, (int) var1.length());
        }
    }

    public static void addBufferRequest(SoundBuffer var0) {
        Set var1 = toLoadList;
        synchronized (toLoadList) {
            toLoadList.add(var0);
        }
    }

    public static void syncLoadBuffer(MilesSoundBuffer var0) {
        if (var0 != null) {
            synchronized (loadingList) {
                if (var0.getData() == null) {
                    try {
                        loadBuffer(var0);
                    } catch (FileNotFoundException var4) {
                        logger.error("Sound file not found: " + var4);
                    }
                }

                loadingList.remove(var0);
            }

            synchronized (toLoadList) {
                toLoadList.remove(var0);
            }
        }
    }

    public void loadFile(InputStream var1, String var2, String var3) {
        String var4 = var2.substring(var2.lastIndexOf("."));
        StockFile var5 = new StockFile();
        var5.setName(var2);
        if (!var4.equals(".mp3") && !var4.equals(".ogg")) {
            MilesSoundBuffer var7 = new MilesSoundBuffer();
            var7.setExtension(var4);
            var7.setName(var2);
            var7.setFileName(var3);
            this.addEntry(var5, var2, var7);
        } else {
            MilesSoundStream var6 = new MilesSoundStream();
            var6.setName(var2);
            var6.setFileName(var3);
            this.addEntry(var5, var2, var6);
        }

        this.addStockFile(var5);
    }

    public void finalize() {
        this.shouldStop = true;
    }

    public void run() {
        while (!this.shouldStop) {
            this.process();
            try {
                Thread.sleep((long) THREAD_INTERVAL);
            } catch (InterruptedException var2) {
                var2.printStackTrace();
            }
        }
    }

    private void process() {
        Set var1 = toLoadList;
        synchronized (toLoadList) {
            loadingList.addAll(toLoadList);
            toLoadList.clear();
        }

        List var10 = loadingList;
        synchronized (loadingList) {
            Iterator var3 = loadingList.iterator();

            while (true) {
                if (!var3.hasNext()) {
                    break;
                }

                SoundBuffer var2 = (SoundBuffer) var3.next();

                try {
                    loadBuffer(var2);
                    Thread.sleep((long) INTERVAL_BETWEEN_ENTRIES);
                } catch (FileNotFoundException var6) {
                    logger.error("Sound file not found./createProcessingInstructionSelector" + var6);
                } catch (Exception var7) {
                    var7.printStackTrace();
                }
            }
        }

        synchronized (toLoadList) {
            toLoadList.removeAll(loadingList);
            loadingList.clear();
        }
    }
}
