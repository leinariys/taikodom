package taikodom.render.loader.provider;

import all.LogPrinter;
import taikodom.render.textures.*;

import java.util.*;
import java.util.concurrent.Semaphore;

public class ResourceCleaner implements Runnable {
    static final int resourceLifeTime = 20000;
    static final int resourceAdaptationTime = 20000;
    private static final int DEGRADE_MIN_TIME_PER_TEXTURE = 15000;
    private static final int CLEANER_INTERVAL = 5000;
    private static final int MAX_AGE_TO_DEGRADE = 40000;
    private static final float LOW_THRESHOLD_FACTOR = 1.2F;
    private static final int FIRST_MEMORY_CAP = 40;
    private static final int SECOND_MEMORY_CAP = 60;
    private static final int THIRD_MEMORY_CAP = 80;
    private static final int FOURTH_MEMORY_CAP = 100;
    static List resources = new ArrayList(100);
    static Stack recentEntries = new Stack();
    static Semaphore loaderLock = new Semaphore(1);
    static Thread resCleanerThread;
    static int sizeDDSTextures;
    static int sizeGuiTextures;
    static int sizeImageTextures;
    static int sizeBinkTextures;
    static int sizeTotalTextures;
    private static LogPrinter logger = LogPrinter.K(ResourceCleaner.class);
    private static boolean LOG_RESOURCE_CLEANER = false;
    private static float lastTime;
    private static float deltaTime;
    private static Comparator comparator;
    boolean shouldStop = false;

    public ResourceCleaner() {
        resCleanerThread = new Thread(this, "ResourceCleaner");
        resCleanerThread.setDaemon(true);
        resCleanerThread.start();
    }

    public static void addTexture(Texture var0) {
        if (var0.getDataSource() != null) {
            if (var0.getDataSource() instanceof DDSDataSource) {
                var0.getDataSource().setLifeTime(20000);
            } else if (var0.getDataSource() instanceof BinkDataSource) {
                var0.getDataSource().setLifeTime(20000);
            } else if (var0.getDataSource() instanceof RGraphicsDataSource) {
                var0.getDataSource().setLifeTime(200000);
            }

            Stack var1 = recentEntries;
            synchronized (recentEntries) {
                recentEntries.push(var0);
            }
        }
    }

    public static void cleanDDS(int var0, float var1) {
        deltaTime = (float) System.nanoTime() - lastTime;
        lastTime = (float) System.nanoTime();

        try {
            for (int var2 = 0; var2 < resources.size(); ++var2) {
                Texture var3 = (Texture) resources.get(var2);
                TextureDataSource var4 = ((Texture) resources.get(var2)).getDataSource();
                if (var4 instanceof DDSDataSource && var4.getWidth() >= var0) {
                    if (!var3.isUpdatingData() && var3.isInitialized()) {
                        var3.setPersistencePriority(Math.max(0.0F, var3.getPersistencePriority() - deltaTime * 1.0E-8F));
                        long var5 = System.currentTimeMillis() - var3.getLastBindTime();
                        if ((float) var5 > (float) var4.getLifeTime() * var1 + var3.getPersistencePriority()) {
                            destroyTexture(var3, var4);
                            if (LOG_RESOURCE_CLEANER) {
                                logger.info("Texture " + var3.getName() + " wasnt bound in the last " + (float) var5 * 0.001F + "s. Destroying it.");
                            }

                            cleanTexture(var3);
                            resources.remove(var2);
                            --var2;
                            continue;
                        }

                        if (System.currentTimeMillis() - var3.getLastDegradationTime() > 15000L) {
                            var3.setLastDegradationTime(System.currentTimeMillis());
                            if (LOG_RESOURCE_CLEANER) {
                                logger.info("Texture " + var3.getName() + " wasnt bound in the last " + (float) var5 * 0.001F + "s. Destroying it.");
                            }

                            processDDS(var3, (DDSDataSource) var4);
                        }
                    }

                    Thread.sleep(10L);
                }
            }
        } catch (InterruptedException var7) {
            var7.printStackTrace();
        }

    }

    public static void cleanGui(int var0) {
        deltaTime = (float) System.nanoTime() - lastTime;
        lastTime = (float) System.nanoTime();

        for (int var1 = 0; var1 < resources.size(); ++var1) {
            Texture var2 = (Texture) resources.get(var1);
            TextureDataSource var3 = ((Texture) resources.get(var1)).getDataSource();
            if (var2.getDataSource() instanceof RGraphicsDataSource && var3.getWidth() >= var0) {
                if (!var2.isUpdatingData() && var2.isInitialized()) {
                    var2.setPersistencePriority(Math.max(0.0F, var2.getPersistencePriority() - deltaTime * 1.0E-8F));
                    if ((float) (System.currentTimeMillis() - var2.getLastBindTime()) > (float) var3.getLifeTime() + var2.getPersistencePriority()) {
                        destroyTexture(var2, var3);
                        cleanTexture(var2);
                        resources.remove(var1);
                        --var1;
                        continue;
                    }

                    if (System.currentTimeMillis() - var2.getLastBindTime() < 20000L) {
                        processImage(var2, var3);
                    }
                }

                try {
                    Thread.sleep(10L);
                } catch (InterruptedException var5) {
                    var5.printStackTrace();
                }
            }
        }

    }

    public static void cleanBink(int var0) {
        deltaTime = (float) System.nanoTime() - lastTime;
        lastTime = (float) System.nanoTime();

        for (int var1 = 0; var1 < resources.size(); ++var1) {
            Texture var2 = (Texture) resources.get(var1);
            TextureDataSource var3 = ((Texture) resources.get(var1)).getDataSource();
            if (var2.getDataSource() instanceof BinkDataSource && var3.getWidth() >= var0) {
                if (var2.isInitialized()) {
                    var2.setPersistencePriority(Math.max(0.0F, var2.getPersistencePriority() - deltaTime * 1.0E-8F));
                    if ((float) (System.currentTimeMillis() - var2.getLastBindTime()) > (float) var3.getLifeTime() + var2.getPersistencePriority()) {
                        destroyTexture(var2, var3);
                        cleanTexture(var2);
                        resources.remove(var1);
                        --var1;
                        continue;
                    }
                }

                try {
                    Thread.sleep(10L);
                } catch (InterruptedException var5) {
                    var5.printStackTrace();
                }
            }
        }

    }

    private static void destroyUnusedTextures(int var0, float var1) {
        deltaTime = (float) System.nanoTime() - lastTime;
        lastTime = (float) System.nanoTime();
        long var2 = System.currentTimeMillis();

        try {
            for (int var4 = 0; var4 < resources.size(); ++var4) {
                Texture var5 = (Texture) resources.get(var4);
                TextureDataSource var6 = ((Texture) resources.get(var4)).getDataSource();
                if (var6 instanceof DDSDataSource && var6.getWidth() >= var0) {
                    long var7 = var2 - var5.getLastBindTime();
                    if ((float) var7 <= (float) var6.getLifeTime() * var1) {
                        return;
                    }

                    if (!var5.isUpdatingData() && var5.isInitialized()) {
                        destroyTexture(var5, var6);
                        if (LOG_RESOURCE_CLEANER) {
                            logger.info("Texture " + var5.getName() + " wasnt bound in the last " + (float) var7 * 0.001F + "s. Destroying it.");
                        }

                        cleanTexture(var5);
                        resources.remove(var4);
                        --var4;
                    } else {
                        Thread.sleep(10L);
                    }
                }
            }
        } catch (InterruptedException var9) {
            var9.printStackTrace();
        }

    }

    private static void cleanTexture(Texture var0) {
        var0.getDataSource().setReady(false);
        var0.getDataSource().clearData();
    }

    private static void degradeFarTextures(int var0, float var1) {
        long var2 = System.currentTimeMillis();

        try {
            for (int var4 = resources.size() - 1; var4 >= 0; --var4) {
                Texture var5 = (Texture) resources.get(var4);
                TextureDataSource var6 = ((Texture) resources.get(var4)).getDataSource();
                if (var6 instanceof DDSDataSource && var6.getWidth() >= var0) {
                    long var7 = var2 - var5.getLastBindTime();
                    if ((float) var7 >= 40000.0F * var1) {
                        return;
                    }

                    if (var2 - var5.getLastDegradationTime() > 15000L) {
                        var5.setLastDegradationTime(var2);
                        if (LOG_RESOURCE_CLEANER) {
                            logger.info("Check if texture " + var5.getName() + " can be reduced to_q all lower resolution.");
                        }

                        processDDS(var5, (DDSDataSource) var6);
                    }

                    Thread.sleep(10L);
                }
            }
        } catch (InterruptedException var9) {
            var9.printStackTrace();
        }

    }

    private static void processOldUpdates() {
        for (int var0 = 0; var0 < resources.size(); ++var0) {
            Texture var1 = (Texture) resources.get(var0);
            if (System.currentTimeMillis() - var1.getDataSource().getLastRequestTime() > 30000L && var1.getDataSource().isReady()) {
                cleanTexture(var1);
            }
        }

    }

    private static void process() {
        Stack var0 = recentEntries;
        synchronized (recentEntries) {
            resources.addAll(recentEntries);
            recentEntries.clear();
        }

        if (comparator == null) {
            comparator = new Comparator() {
                @Override
                public int compare(Object o1, Object o2) {
                    return a((Texture) o1, (Texture) o2);
                }

                public int a(Texture var1, Texture var2) {
                    long var3 = var1.getLastBindTime();
                    long var5 = var2.getLastBindTime();
                    long var7 = var3 - (long) var1.getDataSource().getLifeTime();
                    long var9 = var5 - (long) var2.getDataSource().getLifeTime();
                    if (var7 < var9) {
                        return -1;
                    } else {
                        return var7 > var9 ? 1 : 0;
                    }
                }
            };
        }

        Collections.sort(resources, comparator);
        sizeGuiTextures = 0;
        sizeDDSTextures = 0;
        sizeImageTextures = 0;
        sizeBinkTextures = 0;

        for (int var3 = 0; var3 < resources.size(); ++var3) {
            Texture var1 = (Texture) resources.get(var3);
            if (var1.getDataSource() instanceof DDSDataSource) {
                sizeDDSTextures += var1.getMemorySize();
            } else if (var1.getDataSource() instanceof RGraphicsDataSource) {
                sizeGuiTextures += var1.getMemorySize();
            } else if (var1.getDataSource() instanceof Texture2DDataSource) {
                sizeImageTextures += var1.getMemorySize();
            } else if (var1.getDataSource() instanceof BinkDataSource) {
                sizeBinkTextures += var1.getMemorySize();
            }
        }

        sizeDDSTextures /= 1000000;
        sizeGuiTextures /= 1000000;
        sizeImageTextures /= 1000000;
        sizeBinkTextures /= 1000000;
        if (sizeDDSTextures > 100) {
            destroyUnusedTextures(32, 0.5F);
            degradeFarTextures(32, 0.7F);
        } else if (sizeDDSTextures > 80) {
            destroyUnusedTextures(32, 1.0F);
            degradeFarTextures(128, 0.8F);
        } else if (sizeDDSTextures > 60) {
            destroyUnusedTextures(128, 1.0F);
            degradeFarTextures(256, 0.9F);
        } else if (sizeDDSTextures > 40) {
            destroyUnusedTextures(512, 1.0F);
            degradeFarTextures(512, 1.0F);
        }

        if (sizeGuiTextures > 80) {
            cleanGui(0);
        } else if (sizeGuiTextures > 60) {
            cleanGui(32);
        } else if (sizeGuiTextures > 40) {
            cleanGui(256);
        } else if (sizeGuiTextures > 25) {
            cleanGui(512);
        } else if (sizeGuiTextures > 15) {
            cleanGui(1024);
        }

        if (sizeBinkTextures > 30) {
            cleanBink(0);
        } else if (sizeBinkTextures > 20) {
            cleanBink(32);
        } else if (sizeBinkTextures > 10) {
            cleanBink(256);
        } else if (sizeBinkTextures > 5) {
            cleanBink(512);
        }

        processOldUpdates();
    }

    private static void destroyTexture(Texture var0, TextureDataSource var1) {
        var0.destroyInternalTexture();
    }

    private static void processImage(Texture var0, TextureDataSource var1) {
        var1.setLifeTime(200000);
    }

    private static void processDDS(Texture var0, DDSDataSource var1) {
        var0.resetTextureArea();
        float var2 = var0.getScreenArea();
        var1.setActualAreaSize(var2);
        var2 *= 1.2F;
        var2 = var2 > (float) ImageLoader.getMinTextureSize() ? var2 : (float) ImageLoader.getMinTextureSize();
        if (var1.getDegradationLevel() < var1.getNumMipMaps() && var2 < (float) (var1.getMipMapResolution() >> 1)) {
            ImageLoader.addTexture(var0, var1.getMipMapResolution() >> 1, false);
        }

    }

    private static boolean degradeDDS(Texture var0, TextureDataSource var1) {
        if (var1.getWidth() > ImageLoader.getMinTextureSize()) {
            ImageLoader.addTexture(var0, var1.getWidth() >> 1, false);
            ImageLoader.addForcedUpdateTexture(var0);
            var1.incLifeTime(20000);
            return true;
        } else {
            destroyTexture(var0, var1);
            return false;
        }
    }

    public static void showTexturesStats() {
        boolean var0 = true;
        boolean var1 = false;
        boolean var2 = false;
        boolean var3 = false;
        int var4 = 0;
        int var5 = 0;
        int var6 = 0;
        int var7 = 0;
        logger.info("Num of textures: " + resources.size() + " : " + BaseTexture.getTextureMemoryUsed() + " Bytes allocated");

        for (int var8 = 0; var8 < resources.size(); ++var8) {
            Texture var9 = (Texture) resources.get(var8);
            TextureDataSource var10 = var9.getDataSource();
            if (var10 instanceof DDSDataSource) {
                if (var0) {
                    DDSDataSource var11 = (DDSDataSource) var10;
                    logger.info("Texture " + var11.getFileName() + "\n\t" + " Current Resolution:" + var11.getWidth() + " of " + var11.getRealWidth() + " " + " LifeTime:" + (System.currentTimeMillis() - var9.getLastBindTime()) + " of " + ((float) var10.getLifeTime() + var9.getPersistencePriority()) + " size:" + var9.getMemorySize() + " bytes - DDSImage");
                }

                var4 += var9.getMemorySize();
            } else if (var10 instanceof RGraphicsDataSource) {
                if (var1) {
                    logger.info("Texture Resolution:" + var9.getInternalTexture().getWidth() + "x" + var9.getInternalTexture().getHeight() + " LifeTime:" + (System.currentTimeMillis() - var9.getLastBindTime()) + " of " + ((float) var10.getLifeTime() + var9.getPersistencePriority()) + " size:" + var9.getInternalTexture().getEstimatedMemorySize() + " bytes - RGraphics2 created image.");
                }

                var5 += var9.getMemorySize();
            } else if (var10 instanceof BinkDataSource) {
                if (var3) {
                    logger.info("Texture Resolution:" + var9.getInternalTexture().getWidth() + "x" + var9.getInternalTexture().getHeight() + " LifeTime:" + (System.currentTimeMillis() - var9.getLastBindTime()) + " of " + ((float) var10.getLifeTime() + var9.getPersistencePriority()) + " size:" + var9.getInternalTexture().getEstimatedMemorySize() + " bytes - Bink created image.");
                }

                var7 += var9.getMemorySize();
            } else {
                if (var2) {
                    logger.info("Texture Resolution:" + var9.getInternalTexture().getWidth() + "x" + var9.getInternalTexture().getHeight() + var10.getFileName() + " " + " LifeTime:" + (System.currentTimeMillis() - var9.getLastBindTime()) + " of " + ((float) var10.getLifeTime() + var9.getPersistencePriority()) + " size:" + var9.getInternalTexture().getEstimatedMemorySize() + " bytes - Image.");
                }

                var6 += var9.getMemorySize();
            }
        }

        logger.info("internal total DDS texture size: " + var4);
        logger.info("internal total RGraphics2 texture size: " + var5);
        logger.info("internal total Image texture size: " + var6);
        logger.info("internal total bink texture size: " + var7);
        logger.info("internal total texture size: " + (var4 + var5 + var6 + var7));
    }

    public void finalize() {
        this.shouldStop = true;
    }

    public void run() {
        while (!this.shouldStop) {
            process();

            try {
                Thread.sleep(5000L);
            } catch (InterruptedException var2) {
                var2.printStackTrace();
            }
        }

    }
}
