package taikodom.render.loader.provider;

import taikodom.render.textures.Texture;

public class ImageLoaderEntry {
    int mipMapResolution;
    Texture texture;
    boolean singleLevel;
    int mipMapLevel;
}
