package taikodom.render.loader.provider;

import all.Hm;
import taikodom.render.loader.FormatProvider;
import taikodom.render.loader.SceneLoader;
import taikodom.render.loader.StockFile;
import taikodom.render.shaders.ShaderProgramObject;

import java.io.InputStream;

/**
 * Загрузчик шейдера
 */
public class ShaderLoader extends FormatProvider {
    /**
     * @param var1
     * @param var2
     */
    public ShaderLoader(SceneLoader var1, String... var2) {
        super(var1, var2);
    }

    /**
     * @param var1 Поток для чтения
     * @param var2 Окончание пути
     * @param var3 Полный путь к ресурсу
     */
    public void loadFile(InputStream var1, String var2, String var3) {
        int var4 = var2.toLowerCase().endsWith(".vert") ? 35633 : 35632;//определяем тип шейдера .vert = 35633  .frog = 35632
        String var5 = Hm.c(var1, "utf-8"); //содержимое файла
        ShaderProgramObject var6 = new ShaderProgramObject(var4, var5);
        var6.setName(var2);
        StockFile var7 = new StockFile();
        var7.setName(var2);
        this.addEntry(var7, var2, var6);
        this.addStockFile(var7);
    }
}
