package taikodom.render.loader.provider;

import all.LogPrinter;
import all.ain;
import com.sun.opengl.util.texture.TextureData;
import com.sun.opengl.util.texture.TextureData.Flusher;
import com.sun.opengl.util.texture.TextureIO;
import taikodom.render.DrawContext;
import taikodom.render.loader.FormatProvider;
import taikodom.render.loader.SceneLoader;
import taikodom.render.loader.StockFile;
import taikodom.render.textures.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.*;
import java.util.concurrent.Semaphore;

/**
 *
 */
public class ImageLoader extends FormatProvider implements Runnable {
    public static final int[] cubeMapTargets = new int[]{34069, 34070, 34071, 34072, 34073, 34074};
    protected static Semaphore processingLock = new Semaphore(1);
    static long textureTimeLimit = 5000L;
    static TextureData noTextureData;
    static TextureData noTextureData2;
    private static LogPrinter logger = LogPrinter.K(ImageLoader.class);
    private static boolean LOG_IMAGE_LOADER = false;
    private static Map currentTexturesMap;
    private static Map secondaryTexturesMap;
    private static List textureDestroyList;
    private static List textureUpdateList;
    private static int[] ddsTargets = new int[]{1024, 2048, 4096, 8192, 16384, 32768};
    private static int globalMinTextureSize = 16;
    private static int globalMaxMipMapSize = 256;
    private static ain rootpath;
    private static boolean processing = false;
    private static Comparator comparator;
    boolean shouldStop = false;

    /**
     * @param var1
     * @param var2
     */
    public ImageLoader(SceneLoader var1, String... var2) {
        super(var1, var2);
        currentTexturesMap = new HashMap();
        secondaryTexturesMap = new HashMap();
        Thread var3 = new Thread(this, "ImageLoader");
        var3.setDaemon(true);
        var3.start();
    }

    /**
     * @param var0
     * @return
     */
    private static int nextPowerOfTwo(int var0) {
        int var1;
        for (var1 = 1; var1 < var0; var1 <<= 1) {
            ;
        }

        return var1;
    }

    /**
     * @param var0
     * @param var1
     * @param var2
     * @return
     */
    public static boolean addTexture(Texture var0, int var1, boolean var2) {
        TextureDataSource var3 = var0.getDataSource();
        if (!(var3 instanceof DDSDataSource)) {
            return false;
        } else {
            ImageLoaderEntry var4 = new ImageLoaderEntry();
            var4.texture = var0;
            DDSDataSource var5 = (DDSDataSource) var3;
            if (var1 != -1 && var1 <= globalMaxMipMapSize) {
                var4.mipMapResolution = nextPowerOfTwo(var1);
            } else {
                var4.mipMapResolution = globalMaxMipMapSize;
            }

            int var6 = var5.getMipMapIDByResolution(var4.mipMapResolution);
            var4.mipMapLevel = var6;
            if (var4.mipMapLevel == var3.getDegradationLevel()) {
                return false;
            } else {
                if (var6 == 0) {
                    var4.mipMapResolution = var5.getRealWidth();
                }

                var4.singleLevel = var2;
                Map var7 = currentTexturesMap;
                synchronized (currentTexturesMap) {
                    currentTexturesMap.put(var0, var4);
                    return true;
                }
            }
        }
    }

    /**
     * @param var0
     */
    public static void reAddTexture(Texture var0) {
        Map var1 = currentTexturesMap;
        synchronized (currentTexturesMap) {
            TextureDataSource var2 = var0.getDataSource();
            if (var2 instanceof DDSDataSource) {
                DDSDataSource var3 = (DDSDataSource) var2;
                ImageLoaderEntry var4 = new ImageLoaderEntry();
                var2.setReady(true);
                var4.texture = var0;
                var4.singleLevel = false;
                var4.mipMapLevel = var3.getDegradationLevel();
                var4.mipMapResolution = var3.getMipMapResolution();
                currentTexturesMap.put(var0, var4);
            }
        }
    }

    /**
     * @param var0
     */
    public static void getFullDDSTexture(Texture var0) {
        TextureDataSource var1 = var0.getDataSource();
        if (var1 instanceof DDSDataSource) {
            ImageLoaderEntry var2 = new ImageLoaderEntry();
            var2.texture = var0;
            var2.mipMapResolution = globalMaxMipMapSize;
            var2.mipMapLevel = 0;
            var2.singleLevel = false;
            processEntry(var2);
        }

    }

    /**
     * @param var0
     * @param var1
     * @param var2
     * @param var3
     * @param var4
     * @param var5
     * @param var6
     * @return
     */
    public static TextureData newTextureDataByResolution(DDSLoader var0, int var1, int var2, int var3, boolean var4, int var5, InputStream var6) {
        int var7 = var0.getMipMapIDByResolution(var5);
        return newTextureData(var0, var1, var2, var3, var4, var7, var6);
    }

    /**
     * @param var0
     * @param var1
     * @param var2
     * @param var3
     * @param var4
     * @param var5
     * @param var6
     * @return
     */
    public static TextureData newTextureData(DDSLoader var0, int var1, int var2, int var3, boolean var4, int var5, InputStream var6) {
        DDSInfo var7;
        if (var4) {
            var7 = var0.getMipMaps(var1, var5, var0.getNumMipMaps(), var6);
        } else {
            var7 = var0.getMipMap(var1, var5, var6);
        }

        ByteBuffer[] var8 = var7.getDataList();
        if (var3 == 0) {
            switch (var0.getPixelFormat()) {
                case 20:
                    var3 = 32992;
                    break;
                default:
                    var3 = 32993;
            }
        }

        if (var7.isCompressed()) {
            switch (var7.getCompressionFormat()) {
                case 827611204:
                    var2 = 33776;
                    break;
                case 861165636:
                    var2 = 33778;
                    break;
                case 894720068:
                    var2 = 33779;
                    break;
                default:
                    throw new RuntimeException("Unsupported DDS compression format \"" + DDSLoader.getCompressionFormatName(var7.getCompressionFormat()) + "\"");
            }
        }

        if (var2 == 0) {
            switch (var0.getPixelFormat()) {
                case 20:
                    var2 = 6407;
                    break;
                default:
                    var2 = 6408;
            }
        }

        Flusher var9 = new Flusher() {
            public void flush() {
            }
        };
        TextureData var10;
        if (var4 && var0.getNumMipMaps() > 0) {
            var10 = new TextureData(var2, var7.getWidth(), var7.getHeight(), 0, var3, 5121, var7.isCompressed(), true, var8, var9);
            var10.setMipmap(true);
        } else {
            var4 = false;
            var10 = new TextureData(var2, var7.getWidth(), var7.getHeight(), 0, var3, 5121, var4, var7.isCompressed(), true, var7.getData(), var9);
            var10.setMipmap(false);
        }

        return var10;
    }

    public static int getMinTextureSize() {
        return globalMinTextureSize;
    }

    public static void setMinTextureSize(int var0) {
        globalMinTextureSize = var0;
    }

    public static int getMaxMipMapSize() {
        return globalMaxMipMapSize;
    }

    public static void setMaxMipMapSize(int var0) {
        globalMaxMipMapSize = var0;
    }

    /**
     * @param var0
     */
    private static void processEntry(ImageLoaderEntry var0) {
        if (LOG_IMAGE_LOADER) {
            logger.info("Requested all resolution " + var0.mipMapResolution + " for " + var0.texture.getName());
        }

        try {
            DDSDataSource var2 = (DDSDataSource) var0.texture.getDataSource();
            InputStream var1 = rootpath.concat(var0.texture.getDataSource().getFileName()).getInputStream();
            TextureData[] var3;
            if (!var2.isCubemap()) {
                var3 = new TextureData[]{newTextureData(var2.getLoader(), 0, 0, 0, true, var0.mipMapLevel, var1)};
                if (var3[0].getInternalFormat() == 0) {
                    var3[0].setInternalFormat(var3[0].getPixelFormat());
                }
            } else {
                var3 = new TextureData[6];

                for (int var4 = 0; var4 < 6; ++var4) {
                    var3[var4] = newTextureData(var2.getLoader(), ddsTargets[var4], 0, 0, true, var0.mipMapLevel, var1);
                    if (var3[var4].getInternalFormat() == 0) {
                        var3[var4].setInternalFormat(var3[var4].getPixelFormat());
                    }
                }
            }

            var1.close();
            var2.setMipMapResolution(var0.mipMapResolution);
            var2.setHeight(var3[0].getHeight());
            var2.setWidth(var3[0].getWidth());
            var2.setData(var3);
            var2.setDegradationLevel(var0.mipMapLevel);
            var2.setLastRequestTime(System.currentTimeMillis());
            var2.setReady(true);
            var0.texture.setOnlyUpdateNeeded(true);
        } catch (FileNotFoundException var5) {
            var5.printStackTrace();
        } catch (IOException var6) {
            var6.printStackTrace();
        }

    }

    public static TextureData getNoTextureData() {
        Flusher var0 = new Flusher() {
            public void flush() {
            }
        };
        if (noTextureData == null) {
            ByteBuffer var1 = ByteBuffer.allocate(256);
            noTextureData = new TextureData(6408, 8, 8, 0, 6408, 5121, false, false, true, var1, var0);
        }

        return noTextureData;
    }

    public static void clearStack() {
        Map var0 = currentTexturesMap;
        synchronized (currentTexturesMap) {
            currentTexturesMap.clear();
        }
    }

    public static void addForcedUpdateTexture(Texture var0) {
        if (textureUpdateList == null) {
            textureUpdateList = new ArrayList();
        }

        textureUpdateList.add(var0);
    }

    public static void processForcedUpdate() {
        if (textureUpdateList != null) {
            if (textureUpdateList.size() > 0) {
                Texture var0 = (Texture) textureUpdateList.remove(0);
                if (var0.isOnlyUpdateNeeded()) {
                    TextureData[] var1 = var0.getDataSource().getData();
                    if (var1.length > 1) {
                        for (int var2 = 0; var2 < var1.length; ++var2) {
                            var0.updateImage(var1[var2], cubeMapTargets[var2]);
                        }
                    } else {
                        var0.updateImage(var1[0], 0);
                    }

                    var0.getDataSource().clearData();
                }
            }

        }
    }

    public static void destroyTexture(Texture var0) {
        if (textureDestroyList == null) {
            textureDestroyList = new ArrayList();
        }

        if (var0 != null) {
            textureDestroyList.add(var0);
        }

    }

    public static void processDestroyList(DrawContext var0) {
        if (textureDestroyList != null) {
            if (textureDestroyList.size() > 0) {
                Texture var1 = (Texture) textureDestroyList.remove(0);
                BaseTexture.addTextureMemoryUsed(-var1.getInternalTexture().getEstimatedMemorySize());
                TextureDataSource var2 = var1.getDataSource();
                var2.setReady(false);
                var2.clearData();
                if (var2 instanceof DDSDataSource) {
                    DDSDataSource var3 = (DDSDataSource) var2;
                    var1.setInitialized(false);
                    var2.setWidth(globalMinTextureSize);
                    var2.setHeight(globalMinTextureSize);
                    var3.setMipMapResolution(globalMinTextureSize);
                    var2.setDegradationLevel(var3.getNumMipMaps() - 1);
                } else {
                    var1.setInitialized(false);
                }

                var1.updateImage(var0, getNoTextureData());
                var1.setOnlyUpdateNeeded(true);
            }

        }
    }

    /**
     *
     */
    public void finalize() {
        this.shouldStop = true;
    }

    /**
     * ЗАгрузчик определённого формата файлов
     *
     * @param var1 Поток для чтения
     * @param var2 Окончание пути
     * @param var3 Полный путь к ресурсу
     * @throws IOException
     */
    public void loadFile(InputStream var1, String var2, String var3) {
        this.loadImage(var1, var2, var2.replaceAll("^.*[.]([^.]*)$", "$1").toLowerCase());
    }

    /**
     * Загрузка dds
     *
     * @param var1 Поток для чтение
     * @param var2 Локальное имя файла
     * @param var3
     */
    protected void loadDDSImage(InputStream var1, String var2, boolean var3) {
        try {
            int var5 = globalMinTextureSize;
            DDSLoader var6 = new DDSLoader();
            var6.loadFromFile(var1);//Чтение заголовка файла
            Texture var7 = new Texture();
            var7.setName(var2);
            TextureData[] var4;
            if (!var6.isCubemap()) {//это плоская текстура
                var4 = new TextureData[]{newTextureDataByResolution(var6, 0, 0, 0, var3, var5, var1)};
                var4[0].setMustFlipVertically(false);
                if (var4[0].getInternalFormat() == 0) {
                    var4[0].setInternalFormat(var4[0].getPixelFormat());
                }
            } else {//Это карта куба
                var4 = new TextureData[6];

                for (int var8 = 0; var8 < 6; ++var8) {
                    var4[var8] = newTextureDataByResolution(var6, ddsTargets[var8], 0, 0, var3, var5, var1);
                    var4[var8].setMustFlipVertically(false);
                    if (var4[var8].getInternalFormat() == 0) {
                        var4[var8].setInternalFormat(var4[var8].getPixelFormat());
                    }
                }
            }

            DDSDataSource var11 = new DDSDataSource();
            var11.setLoader(var6);//Загрузчик текстуры
            var11.setFileName(var2);//Имя текстуры
            var11.setSampleData(var4);//Данные текстуры
            var11.setSampleWidth(var11.getWidth() < var5 ? var5 : var11.getWidth());
            var11.setSampleHeight(var11.getHeight() < var5 ? var5 : var11.getHeight());
            var11.setWidth(var11.getSampleWidth());
            var11.setHeight(var11.getSampleHeight());
            var11.setMipMapResolution(var5);
            var11.setDegradationLevel(var6.getMipMapIDByResolution(var5));
            var7.setDataSource(var11);
            var7.setSizeX(var6.getRealWidth());
            var7.setSizeY(var6.getRealHeight());
            StockFile var9 = new StockFile();
            var9.setName(var2);
            this.addEntry(var9, var2, var7);//Объединение имени ресурса и загруженных данных
            this.createMaterialForTexture(var2, var7, var9);
            this.addStockFile(var9);
        } catch (Exception var10) {
            throw new RuntimeException("Error reading " + var2, var10);
        }
    }

    /**
     * Создание материала для текстуры
     *
     * @param var1 Локальное имя ресурса
     * @param var2 Класс обёртка данных
     * @param var3
     */
    private void createMaterialForTexture(String var1, Texture var2, StockFile var3) {
        Material var4 = new Material();
        var4.setDiffuseTexture(var2);//Помещаем текстуру на определённый канал материала
        var4.setName(var1 + "/material");//Присваиваем имя
        this.addEntry(var3, var4.getName(), var4);
    }

    /**
     * Загрузка изображения dds, tga, bmp, jpg, gif, png
     *
     * @param var1
     * @param var2
     * @param var3
     */
    protected void loadImage(InputStream var1, String var2, String var3) {
        if ("dds".equals(var3)) {
            this.loadDDSImage(var1, var2, true);
        } else {
            try {
                TextureData var4 = TextureIO.newTextureData(var1, true, var3);
                Texture var5 = new Texture();
                var5.setName(var2);
                Texture2DDataSource var6 = new Texture2DDataSource();
                var6.setFileName(var2);
                var6.setData(var4);
                var6.setWidth(var4.getWidth());
                var6.setHeight(var4.getHeight());
                var6.setReady(true);
                var5.setDataSource(var6);
                var5.setSizeX(var4.getWidth());
                var5.setSizeY(var4.getHeight());
                StockFile var7 = new StockFile();
                var7.setName(var2);
                this.addEntry(var7, var2, var5);
                this.createMaterialForTexture(var2, var5, var7);
                this.addStockFile(var7);
            } catch (Exception var8) {
                throw new RuntimeException("Error reading " + var2, var8);
            }
        }
    }

    private void process() {
        if (!processing) {
            processing = true;
            Map var2 = currentTexturesMap;
            synchronized (currentTexturesMap) {
                Map var1 = currentTexturesMap;
                currentTexturesMap = secondaryTexturesMap;
                secondaryTexturesMap = var1;
            }

            Iterator var8 = secondaryTexturesMap.values().iterator();

            while (var8.hasNext()) {
                ImageLoaderEntry var3 = (ImageLoaderEntry) var8.next();
                if (var3.texture.isUpdatingData()) {
                    Map var4 = currentTexturesMap;
                    synchronized (currentTexturesMap) {
                        currentTexturesMap.put(var3.texture, var3);
                    }
                } else {
                    processEntry(var3);

                    try {
                        float var9 = (float) Math.sqrt((double) var3.mipMapResolution);
                        Thread.sleep((long) (var9 * 10.0F));
                    } catch (InterruptedException var6) {
                        var6.printStackTrace();
                    }
                }
            }

            secondaryTexturesMap.clear();
            processing = false;
        }
    }

    public void run() {
        while (!this.shouldStop) {
            this.process();

            try {
                Thread.sleep(100L);
            } catch (InterruptedException var2) {
                var2.printStackTrace();
            }
        }

    }

    public void setBasePath(ain var1) {
        rootpath = var1;
    }
}
