package taikodom.render.loader.provider;

import all.LogPrinter;
import all.XmlNode;
import all.axx;
import taikodom.render.loader.*;
import taikodom.render.loader.descriptors.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

/**
 * Загрузчик и парсер файлов с расширением .pro
 */
public class ProLoader extends FormatProvider {
    private static LogPrinter logger = LogPrinter.K(ProLoader.class);

    public ProLoader(SceneLoader var1, String... var2) {
        super(var1, var2);
    }

    /**
     * Парсинг тегов файла с расширением .pro
     *
     * @param var1 поток на чтение
     * @param var2 Пример data/hud.pro
     * @param var3 Пример C:\Projects\Java\TaikodomGameLIVE\.\data\hud.pro
     * @throws IOException
     */
    public void loadFile(InputStream var1, String var2, String var3) throws IOException {
        XmlNode var4 = (new axx()).a(var1, "utf-8");//Получили содержимое файла
        //Результат парсинга файла .pro, сначала список файлов из раздела includes,
        // затем объеденится  с рпарсингом раздела objects
        StockFile var5 = new StockFile();
        var5.setName(var2);

        XmlNode var7 = var4.mD("includes");//получить раздел includes
        XmlNode var8;
        if (var7 != null && var7.getChildrenTag().size() > 0) {
            Iterator var9 = var7.getChildrenTag().iterator();//Список объектов в разделе

            while (var9.hasNext()) {
                var8 = (XmlNode) var9.next();//Получаем строки внутренний список файлов Пример <include file="data/shaders/cristal_shader.frag"/>
                String var10 = var8.getAttribute("file");
                if (var10 != null) {
                    this.loader.addInclude(var5, var10);//Добавляем в список для загрузки
                }
            }
        }

        var8 = var4.mD("objects");//ПОлучить раздел objects
        if (var8 != null) {
            Iterator var17 = var8.getChildrenTag().iterator();//Список объектов в разделе

            label39:
            while (true) {
                if (!var17.hasNext()) {
                    break label39;
                }

                XmlNode var18 = (XmlNode) var17.next();//Объект из списка
                String var11 = var18.getTegName(); //Получение имени тега например SSoundSource для class SceneLoader
                ClassDescriptor var12 = this.loader.getClassDescriptor(var11);//Получили класс тега

                if (var12 == null) {
                    logger.warn("ProLoader class descriptor for: [" + var11 + "]");
                } else if (var18.getAttribute("name") == null) {
                    logger.warn("attribute not found: [name]");
                } else {
                    String var13 = var18.getAttribute("name");
                    String var14 = var18.getAttribute("clonesFrom");//клоны От
                    StockEntry var15 = new StockEntry(var13);//Результат парсинга раздела objects
                    if (var14 != null) {
                        var15.setClonesFrom(var14);
                    } else {
                        RenderAsset var16 = (RenderAsset) var12.createInstance();//создать экземпляр класса
                        if (var16 == null) {
                            continue;
                        }

                        var15.setAsset(var16);
                    }

                    this.parseXmlTag(var5, var12, var18, var15.unassignedProperties);
                    this.loader.addEntry(var5, var15);//Объединение записей
                }

            }
        }

        this.addStockFile(var5);
    }

    /**
     * Разбор атрибутов тега из файла .pro и потовков тега раздела objects
     *
     * @param var1
     * @param var2 класс обёртка атрибута файла .pro Например Shader
     * @param var3 Объект раздела objects в файле .pro
     * @param var4 ссылка на список атрибутов тега для заполнения
     */
    void parseXmlTag(StockFile var1, ClassDescriptor var2, XmlNode var3, List var4) {
        Iterator var6 = var3.getAttributes().entrySet().iterator();//Получили список атрибутов тега

        while (var6.hasNext()) {
            Entry var5 = (Entry) var6.next();//контейнер имя атрибута = значение
            PropertyDescriptor var7 = var2.getProperty((String) var5.getKey());
            if (var7 != null) {
                if (var7.isObjectArray()) {
                    logger.warn(var7);
                } else if (var7.isObject()) {
                    var4.add(new ObjectUnassignedProperty(var7, (String) var5.getValue(), false, -1, (RenderAsset) null, false));
                } else if (var7.isObjectArray()) {
                    logger.warn(var7);
                } else {
                    var4.add(new UnassignedPropertyBase(var7, (String) var5.getValue(), -1));
                }
            }
        }

        var6 = var3.getChildrenTag().iterator();//Список тегов потовков

        while (true) {
            label111:
            while (true) {
                PropertyDescriptor var8;
                int var9;
                XmlNode var18;
                do {
                    if (!var6.hasNext()) {
                        return;
                    }

                    var18 = (XmlNode) var6.next();
                    String var19 = var18.getTegName();
                    if (var18.getAttribute("index") != null) {
                        var9 = Integer.parseInt(var18.getAttribute("index"));
                        var8 = var2.getProperty(var19 + "[]");
                    } else {
                        var9 = -1;
                        var8 = var2.getProperty(var19);
                        if (var8 == null) {
                            var8 = var2.getProperty(var19 + "[]");
                        }
                    }
                } while (var8 == null);

                if (var8.isStructArray()) {
                    ClassDescriptor var20 = this.loader.getClassDescriptor(var8.getPropertyTypeName());
                    StructUnassignedProperty var21 = new StructUnassignedProperty(this.loader, var8, var9, var20.createInstance());
                    var4.add(var21);
                    Iterator var23 = var18.getAttributes().entrySet().iterator();

                    while (var23.hasNext()) {
                        Entry var22 = (Entry) var23.next();
                        PropertyDescriptor var24 = var20.getProperty((String) var22.getKey());
                        if (var24 != null) {
                            if (var24.isObjectArray()) {
                                logger.warn("PropertyDescriptor is object array " + (String) var22.getKey());
                            } else if (var24.isObject()) {
                                logger.warn("PropertyDescriptor is object " + (String) var22.getKey());
                                logger.warn(var22.getKey());
                            } else if (var24.isObjectArray()) {
                                logger.warn(var22.getKey());
                            } else {
                                var21.subProperties.add(new UnassignedPropertyBase(var24, (String) var22.getValue(), -1));
                            }
                        }
                    }
                } else if (var8.isObjectArray()) {
                    if (var18.getAttribute("name") != null) {
                        var4.add(new ObjectUnassignedProperty(var8, var18.getAttribute("name"), true, var9, (RenderAsset) null, false));
                    } else {
                        Iterator var11 = var18.getChildrenTag().iterator();

                        while (true) {
                            while (true) {
                                XmlNode var10;
                                ClassDescriptor var13;
                                String var14;
                                String var15;
                                RenderAsset var17;
                                while (true) {
                                    do {
                                        if (!var11.hasNext()) {
                                            continue label111;
                                        }

                                        var10 = (XmlNode) var11.next();
                                        String var12 = var10.getTegName();
                                        var13 = this.loader.getClassDescriptor(var12);
                                    } while (var13 == null);

                                    var14 = var10.getAttribute("name");
                                    var15 = var10.getAttribute("clonesFrom");
                                    if (var14 == null) {
                                        break;
                                    }

                                    StockEntry var16 = new StockEntry(var14);
                                    if (var15 != null) {
                                        var16.setClonesFrom(var15);
                                    } else {
                                        var17 = (RenderAsset) var13.createInstance();
                                        if (var17 == null) {
                                            continue;
                                        }

                                        var16.setAsset(var17);
                                    }

                                    this.parseXmlTag(var1, var13, var10, var16.unassignedProperties);
                                    this.loader.addEntry(var1, var16);
                                    var4.add(new ObjectUnassignedProperty(var8, var14, true, var9, (RenderAsset) null, false));
                                    break;
                                }

                                if (var14 != null) {
                                    break;
                                }

                                ObjectUnassignedProperty var25;
                                if (var15 != null) {
                                    var25 = new ObjectUnassignedProperty(var8, var15, true, var9, (RenderAsset) null, true);
                                } else {
                                    var17 = (RenderAsset) var13.createInstance();
                                    if (var17 == null) {
                                        continue;
                                    }

                                    var25 = new ObjectUnassignedProperty(var8, "", true, var9, var17, false);
                                }

                                this.parseXmlTag(var1, var13, var10, var25.subProperties);
                                var4.add(var25);
                                break;
                            }

                            if (var9 >= 0) {
                                ++var9;
                            }
                        }
                    }
                }
            }
        }
    }

    String getXML(StockFile var1) {
        XmlNode var2 = new XmlNode("root");
        var2.addAttribute("xmlns", "http://scene.gametoolkit.hoplon.com/scene");
        var2.addAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        var2.addAttribute("xsi:schemaLocation", "http://scene.gametoolkit.hoplon.com/scene ../scene.xsd");
        XmlNode var3;
        if (var1.getIncludes().size() > 0) {
            var3 = var2.addChildrenTag(new XmlNode("includes"));
            Iterator var5 = var1.getIncludes().iterator();

            while (var5.hasNext()) {
                String var4 = (String) var5.next();
                XmlNode var6 = var3.addChildrenTag(new XmlNode("include"));
                var6.addAttribute("file", var4);
            }
        }

        if (var1.getEntries().size() > 0) {
            var3 = var2.addChildrenTag(new XmlNode("objects"));

            for (int var10 = 0; var10 < var1.entryCount(); ++var10) {
                StockEntry var11 = var1.getEntry(var10);
                RenderAsset var12 = var11.getAsset();
                ClassDescriptor var7 = this.loader.getClassDescriptor(var12);
                XmlNode var8 = var3.addChildrenTag(new XmlNode(var7.getName()));
                var8.addAttribute("name", var11.getName());
                if (var11.getClonesFrom() != null) {
                    var8.addAttribute("clonesFrom", var11.getClonesFrom());
                }

                this.addXmlTag(var8, var12);
            }
        }

        String var9 = "<?xml version=\"1.0\"?>\n";
        return var9 + var2.toString();
    }

    void addXmlTag(XmlNode var1, RenderAsset var2) {
        ClassDescriptor var3 = this.loader.getClassDescriptor(var2);
        Iterator var5 = var3.getProperties().iterator();

        while (true) {
            PropertyDescriptor var4;
            label46:
            do {
                while (var5.hasNext()) {
                    var4 = (PropertyDescriptor) var5.next();
                    if (var4.isObjectArray()) {
                        continue label46;
                    }

                    if (var4.isObject()) {
                        RenderAsset var6 = (RenderAsset) var4.getAsObject(var2);
                        StockEntry var7 = this.loader.getEntry(var6);
                        if (var6 != null) {
                            var1.addAttribute(var4.getName(), var6.getName());
                        } else if (var7 != null) {
                            var1.addAttribute(var4.getName(), var7.getName());
                        }
                    } else if (!var4.hasDefaultValue(var2)) {
                        String var11 = var4.getAsString(var2);
                        var1.addAttribute(var4.getName(), var11);
                    }
                }

                return;
            } while (var4.arrayItemCount(var2) <= 0);

            int var12 = 0;

            for (int var13 = var4.arrayItemCount(var2); var12 < var13; ++var12) {
                RenderAsset var8 = (RenderAsset) var4.getObjectArrayItem(var2, var12);
                StockEntry var9 = this.loader.getEntry(var8);
                if (var9 == null && var8 != null) {
                    var9 = this.loader.getStockEntry(var8.getName());
                }

                XmlNode var10;
                if (var8 != null) {
                    var10 = var1.addChildrenTag(new XmlNode(var4.getName()));
                    var10.addAttribute("index", Integer.toString(var12));
                    var10.addAttribute("name", var8.getName());
                } else if (var9 != null) {
                    var10 = var1.addChildrenTag(new XmlNode(var4.getName()));
                    var10.addAttribute("index", Integer.toString(var12));
                    var10.addAttribute("name", var9.getName());
                }
            }
        }
    }
}
