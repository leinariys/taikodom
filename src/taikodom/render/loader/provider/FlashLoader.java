package taikodom.render.loader.provider;

import all.Hm;
import all.ol;
import taikodom.render.loader.FormatProvider;
import taikodom.render.loader.SceneLoader;
import taikodom.render.loader.StockFile;
import taikodom.render.textures.FlashTexture;

import java.io.File;
import java.io.InputStream;
import java.nio.ByteBuffer;

public class FlashLoader extends FormatProvider {
    public FlashLoader(SceneLoader var1, String... var2) {
        super(var1, var2);
    }

    public void loadFile(InputStream var1, String var2, String var3) {
        File var4 = new File(var3);
        ByteBuffer var5 = Hm.getByteBuffer(var4);
        ol var6 = new ol(var5, var4.length());
        var6.setName(var2 + "/player");
        FlashTexture var7 = new FlashTexture(var6);
        var7.setName(var2);
        StockFile var8 = new StockFile();
        var8.setName(var2);
        this.addEntry(var8, var2, var7);
        this.addEntry(var8, var2 + "/player", var6);
        this.addStockFile(var8);
    }
}
