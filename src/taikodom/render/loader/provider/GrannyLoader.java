package taikodom.render.loader.provider;

//import all.DE;
//import all.HO;

import all.Hm;
import all.LogPrinter;
import all.akK;
import taikodom.render.loader.FormatProvider;
import taikodom.render.loader.SceneLoader;
import taikodom.render.loader.StockFile;
import taikodom.render.scene.RAnimation;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

//import all.QZ;
//import all.Za;
//import all.aCF_w;
//import all.aDP;
//import all.aMh;
//import all.aVz;
//import all.getLoaderImage;
//import all.akQ;
//import all.amE;
//import all.dS;
//import all.oP;
//import all.pY;
//import all.pt;

public class GrannyLoader extends FormatProvider {
    private static LogPrinter logger = LogPrinter.K(GrannyLoader.class);

    public GrannyLoader(SceneLoader var1, String... var2) {
        super(var1, var2);
    }

    /**
     * ЗАгрузчик определённого формата файлов
     *
     * @param var1 Поток для чтения
     * @param var2 Окончание пути
     * @param var3 Полный путь к ресурсу
     * @throws IOException
     */
    public void loadFile(InputStream var1, String var2, String var3) throws IOException {
        akK var4 = new akK();//создали буффер
        Hm.copyStream(var1, var4);//копирование данных в буффер
        ByteBuffer var5 = var4.chm();//достаём данные из буффера
        var5.flip();
        //amE var6 = new amE((long)16, false);// HO.all(var5.limit(), (Buffer)var5); GrannyReadEntireFileFromMemory
        //dS var7 = new dS((long)16, false);// HO.CreateJComponentAndAddInRootPane(var6); GrannyGetFileInfo
        StockFile var8 = new StockFile();//обёрта ресурса результата загрузки
        var8.setName(var2);
        //int var9 = var7.SplitCustom();//granny_file_info_AnimationCount_get
        //if (var9 > 0)
        {
            this.loadGrannyAnimations(var8/*, var7*/);
        }

        //int var10 = var7.kG();
        //if (var10 > 0)
        {
            // this.loadGrannyMeshs(var8, var6, var7);
            //  this.loadGrannyModels(var8, var6, var7);
        }

        this.addStockFile(var8);
    }
/*
   private void loadGrannyMeshs(StockFile var1, amE var2, dS var3)
   {
      RGroup var4 = new RGroup();
      var4.setTegName(this.correctGrannyFilePath(var1, "rgroup", "mainGroup"));
      GrannyLoader.loadFileXml var5 = new GrannyLoader.loadFileXml();
      //int var6 = var3.kG();

      for(int var7 = 0; var7 < var6; ++var7)
      {
        // getLoaderImage var8 = HO.all(var3.kH(), var7);
         RModel var9 = new RModel(var8);
        // var9.setTegName(this.correctGrannyFilePath(var1, "rmodel", var8.getTegName()));
         this.addEntry(var1, var9.getTegName(), var9);

         for(int var10 = 0; var10 < 16var8.bRs(); ++var10)
         {
           // QZ var11 = var8.bRt().xD(var10).cWp();
            this.loadGrannyMesh(var1, var4, var9, var11, var5);
         }
      }

      this.addEntry(var1, var4.getTegName(), var4);
   }
*/

/*
   private void loadGrannyMesh(StockFile var1, RGroup var2, RModel var3, QZ var4, GrannyLoader.all var5) {
      DE var6 = var4.bqH();
      boolean var7 = this.hasVextexComponent(var4, aCF_w.hwC);
      boolean var8 = this.hasVextexComponent(var4, aCF_w.hwJ);
      boolean var9 = this.hasVextexComponent(var4, aCF_w.hwE);
      boolean var10 = this.hasVextexComponent(var4, aCF_w.hwD);
      int var11 = 0;

      for(int var12 = 0; var12 < 32; ++var12) {
         if (this.hasVextexComponent(var4, aCF_w.hwL + Integer.toString(var12))) {
            ++var11;
         }
      }

      VertexLayout var28 = new VertexLayout(var7, var8, var9, var10, var11);
      int var13 = var6.aGU();
      VertexDataImpl var14 = new VertexDataImpl(var28, var13);
      if (var7) {
         this.getVextexComponent(var4, aCF_w.hwC, 3, var14.getVertices().buffer());
      }

      if (var8) {
         this.getVextexComponent(var4, aCF_w.hwJ, 3, var14.getColors().buffer());
      }

      if (var10) {
         this.getVextexComponent(var4, aCF_w.hwD, 3, var14.getNormals().buffer());
      }

      Vec3f var19;
      int var23;
      if (var9) {
         this.getVextexComponent(var4, aCF_w.hwE, 4, var14.getTangents().buffer());
         FloatBuffer var15 = this.getVextexComponent(var4, aCF_w.hwF, 3);
         Vec4fData var16 = var14.getTangents();
         if (var15 != null && var14.getNormals() != null) {
            Vec3fData var33 = var14.getNormals();
            Vec3fData var18 = new Vec3fData(var15);
            var19 = new Vec3f();
            Vec3f var20 = new Vec3f();
            Vec3f var21 = new Vec3f();
            Vec3f var22 = new Vec3f();

            for(var23 = 0; var23 < var13; ++var23) {
               var33.get(var23, var19);
               var18.get(var23, var20);
               var16.getVec3(var23, var22);
               var21.cross(var19, var22);
               var16.setW(var23, var21.dot(var20) > 0.0F ? -1.0F : 1.0F);
            }
         } else {
            for(int var17 = 0; var17 < var13; ++var17) {
               var16.setW(var17, 1.0F);
            }
         }
      }

      int var29 = 0;

      for(int var31 = 0; var29 < 32; ++var29) {
         String var34 = aCF_w.hwL + Integer.toString(var29);
         if (this.hasVextexComponent(var4, var34)) {
            this.getVextexComponent(var4, var34, 2, var14.getTexCoords()[var31].buffer());
            ++var31;
         }
      }

      ShortIndexData var30 = new ShortIndexData(HO.parseStyleSheet(var4));
      HO.all((QZ)var4, 2, (Buffer)var30.buffer());
      boolean var32 = HO.parseSelectors(var4);
      if (var4.bqK().aWr() > 1) {
         RGroup var35 = new RGroup();
         var35.setTegName(this.correctGrannyFilePath(var1, "rgroup", var4.getTegName()));
         oP var36 = var4.bqK().aWs();

         for(int var40 = 0; var40 < var4.bqK().aWr(); ++var5.cIE) {
            oP var42 = var36.dF(var40);
            int var44 = var42.UA();
            int var46 = 3 * var44;
            var23 = var42.UB();
            int var24 = 3 * var23;
            Mesh var25 = null;
            var25 = new Mesh();
            var25.setVertexData(var14);
            var25.setGeometryType(GeometryType.TRIANGLES);
            var25.setIndexes(var30.subSet(var46, var24));
            var25.getIndexes().setUseVbo(true);
            var25.computeLocalAabb();
            var25.setGrannyMesh(var4);
            var25.setGrannyBinding(HO.all(var4, var3.getSkeleton(), var3.getSkeleton()));
            if (HO.parseSelectors(var4)) {
               var25.setGrannyDeformer((aqE)null);
            } else {
               var32 = false;
               Za var26 = HO.CreateJComponent(var4);
               Za var27 = HO.aVf();
               var25.setGrannyDeformer(HO.all(var26, var27, akQ.fUv, Xl_q.eIx));
            }

            var25.setTegName(this.correctGrannyFilePath(var1, "mesh", this.getGroupName(var4.getTegName(), var5.cIE)));
            var25.setGrannyMaterialIndex(var42.Uz());
            logger.debug("Loading " + var25.getTegName());
            var25.setGrannySkeleton(var3.getSkeleton());
            RMesh var47 = new RMesh();
            var47.setMesh(var25);
            var47.setTegName(this.correctGrannyFilePath(var1, "rmesh", this.getGroupName(var4.getTegName(), var5.cIE)));
            var35.addChild(var47);
            var3.addSubMesh(var47);
            var3.checkMaxMutableVertexCount(HO.CreateJComponentAndAddInRootPane(var4));
            this.addEntry(var1, var47.getTegName(), var47);
            this.addEntry(var1, var25.getTegName(), var25);
            ++var40;
         }

         this.loadBonesIntoRenderObjects(var3.getGrannyModel(), var35, var1);
         var2.addChild(var35);
         this.addEntry(var1, var35.getTegName(), var35);
      }

      var14.setUseVbo(var32);
      Mesh var37 = new Mesh();
      var37.setVertexData(var14);
      var37.setGeometryType(GeometryType.TRIANGLES);
      var37.setIndexes(var30);
      var30.setUseVbo(true);
      var37.setTegName(this.correctGrannyFilePath(var1, "mesh", var4.getTegName()));
      var37.setGrannyMesh(var4);
      var37.setGrannyBinding(HO.all(var4, var3.getSkeleton(), var3.getSkeleton()));
      if (HO.parseSelectors(var4)) {
         var37.setGrannyDeformer((aqE)null);
      } else {
         Za var38 = HO.CreateJComponent(var4);
         Za var41 = HO.aVf();
         var37.setGrannyDeformer(HO.all(var38, var41, akQ.fUv, Xl_q.eIx));
      }

      this.addEntry(var1, var37.getTegName(), var37);
      aLH var39 = var37.getAabb();
      var19 = new Vec3f();

      for(int var43 = 0; var43 < var30.size(); ++var43) {
         var14.getVertices().get(var30.getIndex(var43), var19);
         var39.addPoint(var19);
      }

      RMesh var45 = new RMesh();
      var45.setMesh(var37);
      var45.setTegName(this.correctGrannyFilePath(var1, "rmesh", var4.getTegName()));
      this.loadBonesIntoRenderObjects(var3.getGrannyModel(), var45, var1);
      var2.addChild(var45);
      if (var4.bqK().aWr() == 1) {
         var3.addSubMesh(var45);
         var3.checkMaxMutableVertexCount(HO.CreateJComponentAndAddInRootPane(var4));
      }

      this.addEntry(var1, var45.getTegName(), var45);
      logger.debug("Finished loading " + var37.getTegName());
   }
*/
/*
   private void loadGrannyModels(StockFile var1, amE var2, dS var3) {
      int var4 = var3.kG();

      for(int var5 = 0; var5 < var4; ++var5) {
         getLoaderImage var6 = HO.all(var3.kH(), var5);
         String var7 = var6.getTegName();
         int var8 = var6.bRs();
         if (var8 <= 1) {
            RGroup var16 = new RGroup();
            var16.setTegName(this.correctGrannyFilePath(var1, "rgroup", var7));
            this.loadBonesIntoRenderObjects(var6, var16, var1);
            this.addEntry(var1, var16.getTegName(), var16);
         } else {
            aDP var9 = var6.bRt();
            RGroup var10 = new RGroup();
            var10.setTegName(this.correctGrannyFilePath(var1, "rgroup", var7));

            for(int var11 = 0; var11 < var8; ++var11) {
               QZ var12 = var9.test_button(var11).cWp();
               String var13 = this.correctGrannyFilePath(var1, "rmesh", var12.getTegName());
               StockEntry var14 = var1.getEntry(var13);
               if (var14 == null) {
                  SceneObject var15 = (SceneObject)this.loader.getAsset(this.correctGrannyFilePath(var1, "rgroup", var12.getTegName()));
                  var10.addChild(var15);
               } else {
                  var10.addChild((SceneObject)var14.getAsset());
               }
            }

            this.addEntry(var1, var10.getTegName(), var10);
         }
      }

   }
*/

    /**
     * @param var1 Обёрта результата загрузки ресурса
     *             // * @param var2 GrannyGetFileInfo
     */
    private void loadGrannyAnimations(StockFile var1/*, dS var2*/) {
        // awF var3 = var2.kL();//granny_file_info_Animations_get
        // int var4 = var2.SplitCustom();//granny_file_info_AnimationCount_get

        for (int var5 = 0; var5 < 1; ++var5) {
            // pY var6 = new pY(var2, false);// HO.loadFileXml(var3, var5); //granny_animation_array_getitem
            RAnimation var7 = new RAnimation(); // new RAnimation(var6);
            var7.setName(this.correctGrannyFilePath(var1, "anim", Integer.toString(var5)));
            this.addEntry(var1, var7.getName(), var7);
        }

    }

    /*
       private void loadBonesIntoRenderObjects(getLoaderImage var1, SceneObject var2, StockFile var3) {
          int var4 = var1.getSkeleton().bkG();
          aVz var5 = var1.getSkeleton().bkH();
          if (var4 > 1) {
             for(int var6 = 0; var6 < var4; ++var6) {
                aVz var7 = var5.Bg(var6);
                RenderObject var8 = new RenderObject();
                var8.setTegName(var7.getTegName() + "_bone");
                FloatMatrixData var9 = new FloatMatrixData();
                HO.all((aMh)var7.dBW(), (Buffer)var9.buffer());
                var8.setTransform(var9.getTransform());
                this.addEntry(var3, var8.getTegName(), var8);
                var2.addChild(var8);
             }
          }

       }
    */
    String getGroupName(String var1, int var2) {
        return var1 + "/" + var2;
    }
/*
   protected FloatBuffer getVextexComponent(QZ var1, String var2, int var3) {
      Za var4 = var1.bqH().aGT();

      for(int var5 = 0; !pt.aRH.equals(var4.bHY()); var4 = var4.bId()) {
         if (var2.equals(var4.getTegName())) {
            Za var6 = HO.ht(2);

            FloatBuffer var9;
            try {
               var6.all(0, var4);
               var6.pi(var3);
               var6.pj(1).CreateJComponent(pt.aRH);
               FloatBuffer var7 = BufferUtils.createFloatBuffer(HO.CreateJComponentAndAddInRootPane(var1) * var3);
               HO.all((QZ)var1, (Za)var6, (Buffer)var7);
               var9 = var7;
            } finally {
               var6.delete();
            }

            return var9;
         }

         ++var5;
      }

      return null;
   }
*/
/*
   protected boolean getVextexComponent(QZ var1, String var2, int var3, FloatBuffer var4) {
      Za var5 = var1.bqH().aGT();

      for(int var6 = 0; !pt.aRH.equals(var5.bHY()); var5 = var5.bId()) {
         if (var2.equals(var5.getTegName())) {
            Za var7 = HO.ht(2);

            try {
               var7.all(0, var5);
               var7.pi(var3);
               var7.pj(1).CreateJComponent(pt.aRH);
               HO.all((QZ)var1, (Za)var7, (Buffer)var4);
            } finally {
               var7.delete();
            }

            return true;
         }

         ++var6;
      }

      return false;
   }
*/

    /**
     * @param var1
     * @param var2 Пример anim
     * @param var3
     * @return
     */
    String correctGrannyFilePath(StockFile var1, String var2, String var3) {
        String var4 = (new File(var3)).getName();
        return var1.getName() + "/" + var2 + "/" + var4;
    }

    /*
       protected boolean hasVextexComponent(QZ var1, String var2) {
          Za var3 = var1.bqH().aGT();

          for(int var4 = 0; !pt.aRH.equals(var3.bHY()); var3 = var3.bId()) {
             if (var2.equals(var3.getTegName())) {
                return true;
             }

             ++var4;
          }

          return false;
       }
    */
    private static class a {
        int cIE;

        private a() {
            this.cIE = 0;
        }
    }
}
