package taikodom.render.loader.provider;

import all.FileControl;
import all.LogPrinter;
import all.aK;
import taikodom.render.textures.BitmapFont;
import taikodom.render.textures.Font;
import taikodom.render.textures.WidthData;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.concurrent.Semaphore;

public class BitmapFontFactory {
    public static final String CACHE_DIRECTORY = "data/fontcache/";
    private static final String formatVersion = "1.0";
    static LogPrinter logger = LogPrinter.K(BitmapFontFactory.class);
    /**
     * Корневой путь. Каталог с кодом отрисовки графики
     */
    private static String rootPath;
    private static Semaphore fontConcurrentLock = new Semaphore(3);

    /**
     * Корневой путь. Каталог с кодом отрисовки графики
     *
     * @param var0 Пример C:\Projects\Java\TaikodomGameLIVE\.
     */
    public static void setRootPath(String var0) {
        rootPath = var0;
    }

    /**
     * @param var0
     * @return
     */
    public static BitmapFont getBitmapFont(final java.awt.Font var0) {
        final BitmapFont var1 = new BitmapFont();
        Thread var2 = new Thread() {
            public void run() {
                try {
                    BitmapFontFactory.fontConcurrentLock.acquire();
                    var1.initialize(var0);
                } catch (InterruptedException var5) {
                    var5.printStackTrace();
                } finally {
                    BitmapFontFactory.fontConcurrentLock.release();
                }

            }
        };
        var2.setDaemon(true);
        var2.start();
        return var1;
    }

    /**
     * @param var0
     * @return
     */
    public static BufferedImage readSnapshot(BitmapFont var0) {
        BufferedImage var1 = null;
        try {
            String var2 = "";
            java.awt.Font var3 = var0.getFont();
            var2 = var2 + var3.getFamily();
            var2 = var2 + var3.getFontName();
            var2 = var2 + var3.getName();
            var2 = var2 + var3.getSize();
            var2 = var2 + var3.getStyle();
            var2 = var2 + formatVersion;
            String var4 = rootPath + "/" + CACHE_DIRECTORY + aK.w(var2);
            FileControl var5 = new FileControl(var4 + ".bfd");
            if (!var5.exists()) {
                return null;
            } else {
                FileInputStream var6 = new FileInputStream(var5.getFile());
                DataInputStream var7 = new DataInputStream(var6);
                var0.setTextureResolution(var7.readInt());
                var0.setNumOfValidChars(var7.readInt());
                int var8 = var7.readInt();
                for (int var9 = 0; var9 < var8; ++var9) {
                    var0.getCharWidths().add(new WidthData(var7.readInt(), var7.readInt()));
                }
                var1 = ImageIO.read(var6);
                var6.close();
                return var1;
            }
        } catch (Exception var10) {//Недопустимый снимок шрифта. создавая все новое.
            logger.warn("Font snapshot invalid. creating all new one.");
            return null;
        }
    }

    /**
     * @param var0
     * @param var1
     * @param var2
     */
    public static void writeSnapshot(BufferedImage var0, int var1, BitmapFont var2) {
        try {
            String var3 = "";
            java.awt.Font var4 = var2.getFont();
            var3 = var3 + var4.getFamily();
            var3 = var3 + var4.getFontName();
            var3 = var3 + var4.getName();
            var3 = var3 + var4.getSize();
            var3 = var3 + var4.getStyle();
            var3 = var3 + formatVersion;
            File var5 = new File(rootPath + "/" + CACHE_DIRECTORY);
            if (!var5.exists()) {
                var5.mkdirs();
            }

            String var6 = rootPath + "/" + CACHE_DIRECTORY + aK.w(var3);
            FileControl var7 = new FileControl(var6 + ".bfd");
            FileOutputStream var8 = new FileOutputStream(var7.getFile());
            DataOutputStream var9 = new DataOutputStream(var8);
            var9.writeInt((int) var2.getTextureResolution());
            var9.writeInt(var2.getNumOfValidChars());
            var9.writeInt(var2.getCharWidths().size());

            for (int var10 = 0; var10 < var2.getCharWidths().size(); ++var10) {
                var9.writeInt(((WidthData) var2.getCharWidths().get(var10)).width);
                var9.writeInt(((WidthData) var2.getCharWidths().get(var10)).realWidth);
            }

            ImageIO.write(var0, "png", var8);
            var8.close();
        } catch (IOException var11) {//Шрифт-кеш не может быть вызван
            logger.warn("Font-cache could not be writed.");
        }
    }

    Font createBitmapFont(java.awt.Font var1) {
        return null;
    }
}
