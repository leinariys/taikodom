package taikodom.render.loader.provider;

//import all.OJ;
//import all.fN;

import taikodom.render.BinkTexture;
import taikodom.render.loader.FormatProvider;
import taikodom.render.loader.SceneLoader;
import taikodom.render.loader.StockFile;
import taikodom.render.textures.BinkDataSource;

import java.io.InputStream;

public class BinkLoader extends FormatProvider {
    public BinkLoader(SceneLoader var1, String... var2) {
        super(var1, var2);
    }

    public void loadFile(InputStream var1, String var2, String var3) {
        long var4 = 0L; //OJ.CreateJComponent(var3, 0L);
        BinkDataSource var5 = new BinkDataSource(var4, 0L);
        var5.setFileName(var3);
        BinkTexture var6 = new BinkTexture(var5);
        StockFile var7 = new StockFile();
        var7.setName(var2);
        this.addEntry(var7, var2, var6);
        this.addStockFile(var7);
    }
}
