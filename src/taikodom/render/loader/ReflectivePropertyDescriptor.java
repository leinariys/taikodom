package taikodom.render.loader;

import all.ds;
import taikodom.render.loader.descriptors.PropertyDescriptor;

import java.lang.reflect.Method;

public class ReflectivePropertyDescriptor extends PropertyDescriptor {
    private Method getter;
    private Method setter;
    private ds valueParser;

    public ReflectivePropertyDescriptor(ds var1, String var2, String var3, boolean var4, boolean var5) {
        this.isArray = var4;
        this.isObject = var5;
        this.valueParser = var1;
        this.propertyTypeName = var2;
        this.name = var3;
        this.isObject = var5;
    }

    public int arrayItemCount(RenderAsset var1) {
        return 0;
    }

    public Object getAsObject(RenderAsset var1) {
        try {
            return this.getter.invoke(var1);
        } catch (RuntimeException var3) {
            throw var3;
        } catch (Exception var4) {
            throw new RuntimeException(var4);
        }
    }

    public String getAsString(RenderAsset var1) {
        return String.valueOf(this.getAsObject(var1));
    }

    public RenderAsset getObjectArrayItem(RenderAsset var1, int var2) {
        return null;
    }

    public boolean hasDefaultValue(RenderAsset var1) {
        return false;
    }

    public String getName() {
        return this.name;
    }

    public void setGetter(Method var1) {
        this.getter = var1;
    }

    public void setSetter(Method var1) {
        this.setter = var1;
    }

    public boolean setAsString(Object var1, String var2) {
        return this.setAsString(var1, 0, var2);
    }

    public boolean setAsString(Object var1, int var2, String var3) {
        try {
            if (this.valueParser == null) {
                return false;
            } else {
                Object var4 = this.valueParser.O(var3);
                if (this.isArray) {
                    this.setter.invoke(var1, var2, var4);
                } else {
                    this.setter.invoke(var1, var4);
                }

                return true;
            }
        } catch (Exception var5) {
            var5.printStackTrace();
            return false;
        }
    }

    public boolean set(Object var1, int var2, Object var3) {
        try {
            if (this.isArray) {
                this.setter.invoke(var1, var2, var3);
            } else {
                this.setter.invoke(var1, var3);
            }

            return true;
        } catch (Exception var5) {
            var5.printStackTrace();
            return false;
        }
    }

    public String toString() {
        return "property " + this.propertyTypeName + " " + this.getName();
    }
}
