package taikodom.render.loader;

import gnu.trove.THashSet;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

/**
 * Фармат файлов поддерживаемый загрузсиком
 */
public abstract class FormatProvider {
    /**
     * Класс загрузчика Пример GrannyLoader ImageLoader BinkLoader FlashLoader SoundBufferLoader ProLoader ShaderLoader
     */
    protected SceneLoader loader;
    /**
     * Список расширений файлов поддерживаемые загрузчиком
     * "dds", "tga", "bmp", "jpg", "gif", "png"
     */
    protected Set supportedFormats = new THashSet();

    public FormatProvider(SceneLoader var1, String... var2) {
        this.loader = var1;
        this.supportedFormats.addAll((Collection) Arrays.asList(var2));
    }

    /**
     * Отправка файла определённого формата в специальный загрузчик
     *
     * @param var1 Поток для чтения
     * @param var2 Окончание пути
     * @param var3 Полный путь к ресурсу
     * @throws IOException
     */
    public abstract void loadFile(InputStream var1, String var2, String var3) throws IOException;

    /**
     * Добавляем результат парсинга раздела includes и objects из файла .pro
     *
     * @param var1 РЕзультат парсинка раздел includes и objects
     */
    protected void addStockFile(StockFile var1) {
        this.loader.addStockFile(var1);
    }

    /**
     * Объединение данных и обёртка в класс
     *
     * @param var1 Класс обёртка списка загруженных ресурсов
     * @param var2 Локальное имя файла
     * @param var3 Класс обёртка загруженного ресурса
     * @return
     */
    protected StockEntry addEntry(StockFile var1, String var2, RenderAsset var3) {
        return this.loader.addEntry(var1, var2, var3);
    }

    /**
     * Проверяем есть ли такой формат в списке
     *
     * @param var1 Искомый формат "dds", "tga", "bmp", "jpg", "gif", "png"
     * @return
     */
    public boolean check(String var1) {
        return this.supportedFormats.contains(var1);
    }
}
