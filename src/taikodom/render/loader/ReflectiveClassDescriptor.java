package taikodom.render.loader;

import all.*;
import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import taikodom.render.NotExported;
import taikodom.render.loader.descriptors.ClassDescriptor;
import taikodom.render.loader.descriptors.PropertyDescriptor;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Контейнер класса тега из файла .pro
 * class SceneLoader
 */
public class ReflectiveClassDescriptor extends ClassDescriptor {
    static final ds colorParser = new ds() {
        public Object O(String var1) {
            String[] var2 = var1.split("[ \t\r\n]+");
            return new Color(Float.parseFloat(var2[0]), Float.parseFloat(var2[1]), Float.parseFloat(var2[2]), Float.parseFloat(var2[3]));
        }
    };
    static final ds vec2Parser = new ds() {
        public Object O(String var1) {
            String[] var2 = var1.split("[ \t\r\n]+");
            return new aQKa(Float.parseFloat(var2[0]), Float.parseFloat(var2[1]));
        }
    };
    static final ds vec3fParser = new ds() {
        public Object O(String var1) {
            String[] var2 = var1.split("[ \t\r\n]+");
            return new Vec3f(Float.parseFloat(var2[0]), Float.parseFloat(var2[1]), Float.parseFloat(var2[2]));
        }
    };
    static final ds vec3dParser = new ds() {
        public Object O(String var1) {
            String[] var2 = var1.split("[ \t\r\n]+");
            return new Vector3dOperations(Double.parseDouble(var2[0]), Double.parseDouble(var2[1]), Double.parseDouble(var2[2]));
        }
    };
    private static LogPrinter logger = LogPrinter.K(SceneLoader.class);
    static final ds transformParser = new ds() {
        public Object O(String var1) {
            String[] var2 = var1.split("[ \t\r\n]+");
            bc_q var3;
            if (var2.length == 3) {
                var3 = new bc_q();
                var3.setTranslation(Double.parseDouble(var2[0]), Double.parseDouble(var2[1]), Double.parseDouble(var2[2]));
                return var3;
            } else if (var2.length == 6) {
                var3 = new bc_q();
                var3.y(Float.parseFloat(var2[3]));
                var3.z(Float.parseFloat(var2[4]));
                var3.A(Float.parseFloat(var2[5]));
                var3.setTranslation(Double.parseDouble(var2[0]), Double.parseDouble(var2[1]), Double.parseDouble(var2[2]));
                return var3;
            } else if (var2.length == 16) {
                var3 = new bc_q(Float.parseFloat(var2[0]), Float.parseFloat(var2[1]), Float.parseFloat(var2[2]), Float.parseFloat(var2[4]), Float.parseFloat(var2[5]), Float.parseFloat(var2[6]), Float.parseFloat(var2[8]), Float.parseFloat(var2[9]), Float.parseFloat(var2[10]), Double.parseDouble(var2[3]), Double.parseDouble(var2[7]), Double.parseDouble(var2[11]));
                var3.mX.normalize();
                return var3;
            } else {
                ReflectiveClassDescriptor.logger.error("Parsing all transform with wrong format: " + var1);
                return new bc_q();
            }
        }
    };
    static final ds matrix4fparser = new ds() {
        public Object O(String var1) {
            String[] var2 = var1.split("[ \t\r\n]+");
            ajK var3;
            if (var2.length == 3) {
                if (var2.length == 3) {
                    var3 = new ajK();
                    var3.rotateX(Float.parseFloat(var2[3]));
                    var3.rotateY(Float.parseFloat(var2[4]));
                    var3.rotateZ(Float.parseFloat(var2[5]));
                    var3.p(Float.parseFloat(var2[0]), Float.parseFloat(var2[1]), Float.parseFloat(var2[2]));
                    return var3;
                }
            } else if (var2.length == 6) {
                var3 = new ajK();
                var3.p(Float.parseFloat(var2[0]), Float.parseFloat(var2[1]), Float.parseFloat(var2[2]));
                return var3;
            }

            if (var2.length == 16) {
                return new ajK(Float.parseFloat(var2[0]), Float.parseFloat(var2[4]), Float.parseFloat(var2[8]), Float.parseFloat(var2[12]), Float.parseFloat(var2[1]), Float.parseFloat(var2[5]), Float.parseFloat(var2[9]), Float.parseFloat(var2[13]), Float.parseFloat(var2[2]), Float.parseFloat(var2[6]), Float.parseFloat(var2[10]), Float.parseFloat(var2[14]), Float.parseFloat(var2[3]), Float.parseFloat(var2[7]), Float.parseFloat(var2[11]), Float.parseFloat(var2[15]));
            } else {
                ReflectiveClassDescriptor.logger.error("Parsing all Matrix4f with wrong format: " + var1);
                return new ajK();
            }
        }
    };
    private final String name;
    private final Class clazz;
    private Map props = new HashMap();

    public ReflectiveClassDescriptor(String var1, Class var2) {
        this.name = var1;
        this.clazz = var2;
        this.getClassMethods(var2);
    }

    private static boolean isMethodValid(Method var0) {
        return var0.getName().length() >= 4 && var0.getAnnotation(NotExported.class) == null;
    }

    private void getClassMethods(Class var1) {
        Method[] var5;
        int var4 = (var5 = var1.getMethods()).length;

        for (int var3 = 0; var3 < var4; ++var3) {
            Method var2 = var5[var3];
            if (isMethodValid(var2)) {
                Class var6 = var2.getReturnType();
                String var7;
                String var8;
                Method var9;
                if ((var2.getName().startsWith("get") || var2.getName().startsWith("is")) && var2.getParameterTypes().length == 0 && !var6.equals(Void.TYPE)) {
                    try {
                        var7 = null;
                        var8 = null;
                        if (var2.getName().startsWith("get")) {
                            var8 = var2.getName().substring(3);
                            var7 = aK.u(var8);
                        } else {
                            var8 = var2.getName().substring(2);
                            var7 = aK.u(var8);
                        }

                        var9 = var1.getMethod("set" + var8, var6);
                        this.addPD(false, var7, var6, var2, (Method) null);
                        this.addPD(false, var7, var9.getParameterTypes()[0], (Method) null, var9);
                    } catch (SecurityException var12) {
                        var12.printStackTrace();
                    } catch (NoSuchMethodException var13) {
                        ;
                    }
                } else if (var2.getName().startsWith("get") && var2.getParameterTypes().length == 1 && !var6.equals(Void.TYPE)) {
                    var7 = var2.getName().substring(3);
                    var8 = aK.u(var7);

                    try {
                        var9 = var1.getMethod("set" + var7, Integer.TYPE, var6);
                        this.addPD(true, var8 + "[]", var6, var2, (Method) null);
                        this.addPD(true, var8 + "[]", var9.getParameterTypes()[1], (Method) null, var9);
                    } catch (SecurityException var10) {
                        var10.printStackTrace();
                    } catch (NoSuchMethodException var11) {
                        ;
                    }
                }
            }
        }

    }

    private ReflectivePropertyDescriptor addPD(boolean var1, String var2, Class var3, Method var4, Method var5) {
        ReflectivePropertyDescriptor var6 = (ReflectivePropertyDescriptor) this.props.get(var2);
        if (var6 == null) {
            ds var7 = this.parserFor(var3);
            boolean var8 = RenderAsset.class.isAssignableFrom(var3);
            var6 = new ReflectivePropertyDescriptor(var7, var3.getSimpleName(), var2, var1, var8);
            this.props.put(var2, var6);
        }

        if (var5 != null) {
            var6.setSetter(var5);
        }

        if (var4 != null) {
            var6.setGetter(var4);
        }

        return var6;
    }

    private ds parserFor(final Class var1) {
        if (var1.isEnum()) {
            return new de() {
                public Object O(String var1x) {
                    try {
                        Field var2 = var1.getField(var1x);
                        return var2.get((Object) null);
                    } catch (Exception var9) {
                        int var3 = Integer.parseInt(var1x);
                        Enum[] var4 = (Enum[]) var1.getEnumConstants();
                        Enum[] var8 = var4;
                        int var7 = var4.length;

                        for (int var6 = 0; var6 < var7; ++var6) {
                            Enum var5 = var8[var6];
                            if (var5.ordinal() == var3) {
                                return var5;
                            }
                        }

                        ReflectiveClassDescriptor.logger.error("Unable to_q find all enum value at " + var1.getName() + ", corresponding to_q " + var1x);
                        throw new RuntimeException(var9);
                    }
                }
            };
        } else if (var1 == String.class) {
            return new de() {
                public Object O(String var1) {
                    return var1;
                }
            };
        } else if (var1 == Vec3f.class) {
            return vec3fParser;
        } else if (var1 == aQKa.class) {
            return vec2Parser;
        } else if (var1 == bc_q.class) {
            return transformParser;
        } else if (var1 == ajK.class) {
            return matrix4fparser;
        } else if (var1 == Color.class) {
            return colorParser;
        } else {
            return (ds) (var1 == Vector3dOperations.class ? vec3dParser : de.c(var1));
        }
    }

    public String getName() {
        return this.name;
    }

    /**
     * создать экземпляр класса
     *
     * @return
     */
    public Object createInstance() {
        try {
            return this.clazz.newInstance();
        } catch (InstantiationException var2) {
            throw new RuntimeException(var2);
        } catch (IllegalAccessException var3) {
            throw new RuntimeException(var3);
        }
    }

    public PropertyDescriptor getProperty(String var1) {
        return (PropertyDescriptor) this.props.get(var1);
    }

    public Collection getProperties() {
        return this.props.values();
    }
}
