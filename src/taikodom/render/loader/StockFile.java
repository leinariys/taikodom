package taikodom.render.loader;

import java.util.*;

/**
 * Обёрта результата загрузки ресурса
 */
public class StockFile {

    /**
     * Указание имени файла каталога ресурсов Пример data/hud.pro
     */
    protected String name;
    /**
     * Список ресурсов из раздела includes
     */
    protected Set includes;
    /**
     * Список ресурсов из раздела objects
     */
    protected List entries;

    public StockFile() {
        this.includes = new TreeSet(String.CASE_INSENSITIVE_ORDER);
        this.entries = new ArrayList();
    }

    public String getName() {
        return this.name;
    }

    /**
     * Указание имени файла каталога ресурсов
     *
     * @param var1 Пример data/hud.pro
     */
    public void setName(String var1) {
        this.name = var1;
    }

    public StockEntry getEntry(String var1) {
        Iterator var3 = this.entries.iterator();

        while (var3.hasNext()) {
            StockEntry var2 = (StockEntry) var3.next();
            if (var2.getName().equals(var1)) {
                return var2;
            }
        }

        return null;
    }

    public Collection getIncludes() {
        return this.includes;
    }

    public void setIncludes(Collection var1) {
        this.includes.clear();
        this.includes.addAll(var1);
    }

    public List getEntries() {
        return this.entries;
    }

    public void setEntries(List var1) {
        this.entries = var1;
    }

    public void addEntry(StockEntry var1) {
        var1.setStockFile(this);
        this.entries.add(var1);
    }

    public int entryCount() {
        return this.entries.size();
    }

    public StockEntry getEntry(int var1) {
        return (StockEntry) this.entries.get(var1);
    }

    public void removeInclude(String var1) {
        this.includes.remove(var1);
    }
}
