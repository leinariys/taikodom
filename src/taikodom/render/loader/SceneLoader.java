package taikodom.render.loader;

import all.LogPrinter;
import com.sun.opengl.util.texture.TextureData;
import com.sun.opengl.util.texture.TextureData.Flusher;
import taikodom.render.PitchEntry;
import taikodom.render.enums.BlendType;
import taikodom.render.enums.DepthFuncType;
import taikodom.render.enums.TexMagFilter;
import taikodom.render.enums.TexMinFilter;
import taikodom.render.gui.GAim;
import taikodom.render.gui.GHudMark;
import taikodom.render.loader.descriptors.ClassDescriptor;
import taikodom.render.loader.descriptors.UnassignedProperty;
import taikodom.render.loader.provider.*;
import taikodom.render.scene.*;
import taikodom.render.shaders.Shader;
import taikodom.render.shaders.ShaderPass;
import taikodom.render.shaders.ShaderProgram;
import taikodom.render.shaders.parameters.*;
import taikodom.render.textures.*;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.*;

/**
 * Менеджер ресурсов сцены
 */
public class SceneLoader {
    private static LogPrinter logger = LogPrinter.K(SceneLoader.class);
    /**
     * Список отпарсиных файлов. ключ - локальный адрес файла .pro  = значение результат парсинга
     */
    protected Map stockFiles;
    /**
     * Загруженные бинарные данные ресурсов
     * Список объектов из раздела includes или objects ключ - содержание атрибута name  = значение результат парсинга
     */
    protected Map stockEntryCache;
    /**
     * Список файлов для загрузки из раздела  includes
     */
    protected Set missingIncludes;
    /**
     * Список видов загрузчиков
     */
    protected List providers;
    /**
     * Ключ = значение
     * Сопоставление имен тегов из файлов с расширением .pro  с классами
     */
    protected Map registeredDescriptors;
    protected ImageLoader imageLoader;
    ResourceCleaner resourceCleaner;
    private Texture defaultEmissiveTexture;
    private Texture defaultNormalTexture;
    private Texture defaultDiffuseTexture;
    private Shader defaultTrailShader;
    private Shader defaultSpaceDustShader;
    private Shader defaultBillboardShader;
    private Shader defaultHudMarkShader;
    private Material defaultMaterial;
    private Shader defaultBillboardShaderNoDepth;
    private Shader defaultVertexColorShader;
    private Shader defaultDecalShader;
    private Shader defaultDiffuseShader;
    private Shader defaultFlatShader;
    private Shader defaultWireframeShader;
    private Shader defaultAlphaTestShader;

    public SceneLoader() {
        this.stockFiles = Collections.synchronizedMap(new TreeMap(String.CASE_INSENSITIVE_ORDER));
        this.stockEntryCache = Collections.synchronizedMap(new TreeMap(String.CASE_INSENSITIVE_ORDER));
        this.missingIncludes = Collections.synchronizedSet(new TreeSet(String.CASE_INSENSITIVE_ORDER));
        this.providers = new ArrayList();
        this.registeredDescriptors = new TreeMap(String.CASE_INSENSITIVE_ORDER);
        this.providers.add(new GrannyLoader(this, new String[]{"gr2"}));
        this.imageLoader = new ImageLoader(this, new String[]{"dds", "tga", "bmp", "jpg", "gif", "png"});
        this.providers.add(this.imageLoader);
        this.resourceCleaner = new ResourceCleaner();
        this.providers.add(new BinkLoader(this, new String[]{"bik"}));
        this.providers.add(new FlashLoader(this, new String[]{"swf"}));
        this.providers.add(new SoundBufferLoader(this, new String[]{"wav", "mp3", "ogg"}));
        this.providers.add(new ProLoader(this, new String[]{"pro"}));
        this.providers.add(new ShaderLoader(this, new String[]{"vert", "frag"}));

        this.registerClassDescriptor(new ReflectiveClassDescriptor("ChannelInfo", ChannelInfo.class));
        this.registerClassDescriptor(new ReflectiveClassDescriptor("PannedMaterial", PannedMaterial.class));
        this.registerClassDescriptor(new ReflectiveClassDescriptor("RBillboard", RBillboard.class));
        this.registerClassDescriptor(new ReflectiveClassDescriptor("RNuclearLight", RNuclearLight.class));
        this.registerClassDescriptor(new ReflectiveClassDescriptor("RSceneAreaInfo", RSceneAreaInfo.class));
        this.registerClassDescriptor(new ReflectiveClassDescriptor("RLight", RLight.class));
        this.registerClassDescriptor(new ReflectiveClassDescriptor("RPanorama", RPanorama.class));
        this.registerClassDescriptor(new ReflectiveClassDescriptor("RSpacedust", RSpaceDust.class));
        this.registerClassDescriptor(new ReflectiveClassDescriptor("TiledTexture", TiledTexture.class));
        this.registerClassDescriptor(new ReflectiveClassDescriptor("PannedTexture", PannedTexture.class));
        this.registerClassDescriptor(new ReflectiveClassDescriptor("AnimatedTexture", AnimatedTexture.class));
        this.registerClassDescriptor(new ReflectiveClassDescriptor("SceneObject", SceneObject.class));
        this.registerClassDescriptor(new ReflectiveClassDescriptor("RParticleSystem", RParticleSystem.class));
        this.registerClassDescriptor(new ReflectiveClassDescriptor("RMesh", RMesh.class));
        this.registerClassDescriptor(new ReflectiveClassDescriptor("RModel", RModel.class));
        this.registerClassDescriptor(new ReflectiveClassDescriptor("RGroup", RGroup.class));
        this.registerClassDescriptor(new ReflectiveClassDescriptor("RLod", RLod.class));
        this.registerClassDescriptor(new ReflectiveClassDescriptor("Material", Material.class));
        this.registerClassDescriptor(new ReflectiveClassDescriptor("Shader", Shader.class));
        this.registerClassDescriptor(new ReflectiveClassDescriptor("ShaderProgram", ShaderProgram.class));
        this.registerClassDescriptor(new ReflectiveClassDescriptor("ShaderPass", ShaderPass.class));
        this.registerClassDescriptor(new ReflectiveClassDescriptor("RTrail", RTrail.class));
        this.registerClassDescriptor(new ReflectiveClassDescriptor("RSpaceDust", RSpaceDust.class));
        this.registerClassDescriptor(new ReflectiveClassDescriptor("FloatParameter", FloatParameter.class));
        this.registerClassDescriptor(new ReflectiveClassDescriptor("ColorParameter", ColorParameter.class));
        this.registerClassDescriptor(new ReflectiveClassDescriptor("IntParameter", IntParameter.class));
        this.registerClassDescriptor(new ReflectiveClassDescriptor("Vec2Parameter", Vec2fParameter.class));
        this.registerClassDescriptor(new ReflectiveClassDescriptor("Vec3Parameter", Vec3fParameter.class));
        this.registerClassDescriptor(new ReflectiveClassDescriptor("RTerrain", RTerrain.class));
        this.registerClassDescriptor(new ReflectiveClassDescriptor("RShape", RShape.class));
        this.registerClassDescriptor(new ReflectiveClassDescriptor("RHudMark", GHudMark.class));
        this.registerClassDescriptor(new ReflectiveClassDescriptor("RAim", GAim.class));
        this.registerClassDescriptor(new ReflectiveClassDescriptor("SPitchedSet", SPitchedSet.class));
        this.registerClassDescriptor(new ReflectiveClassDescriptor("PitchEntry", PitchEntry.class));
        this.registerClassDescriptor(new ReflectiveClassDescriptor("SSoundSource", SSoundSource.class)); //используется в class InterfaceSFXAddon
        this.registerClassDescriptor(new ReflectiveClassDescriptor("SSoundGroup", SSoundGroup.class));
        this.registerClassDescriptor(new ReflectiveClassDescriptor("SSourceElement", SSourceElement.class));
        this.registerClassDescriptor(new ReflectiveClassDescriptor("SMusic", SMusic.class));
        this.registerClassDescriptor(new ReflectiveClassDescriptor("SMultiLayerMusic", SMultiLayerMusic.class));
        this.registerClassDescriptor(new ReflectiveClassDescriptor("RFlowerShot", RFlowerShot.class));
        this.registerClassDescriptor(new ReflectiveClassDescriptor("RRotor", RRotor.class));
        this.registerClassDescriptor(new ReflectiveClassDescriptor("Socket", Socket.class));
    }

    /**
     * Для ProLoader соотнести атрибуты файла с название класса
     *
     * @param var1
     */
    public void registerClassDescriptor(ClassDescriptor var1) {
        this.registeredDescriptors.put(var1.getName(), var1);
    }

    /**
     * загруска ресурсов разных форматов pro, dds, tga, bmp, jpg, gif, png для сцены
     *
     * @param var1 Поток для чтения
     * @param var2 Окончание пути
     * @param var3 Полный путь к ресурсу
     * @throws IOException
     */
    public void loadFile(InputStream var1, String var2, String var3) throws IOException {
        //logger.debug("Loading " + var2);
        int var4 = var2.lastIndexOf(".");//ищим точку
        if (var4 == -1) {
            throw new IOException();
        } else {
            String var5 = var2.substring(var4 + 1).toLowerCase();//получили расширение без точки Пример pro
            int var6 = 0;
            Iterator var8 = this.providers.iterator();//Получили загрузчики форматов

            while (var8.hasNext()) {
                FormatProvider var7 = (FormatProvider) var8.next();//Достаём контейнер класс загрузчика и загружаемые им форматы
                if (var7.check(var5)) {
                    try {//Разбр содержимого файла специальным загрузчиком
                        var7.loadFile(var1, var2, var3);
                        ++var6;
                        break;
                    } catch (Exception var10) {
                        var10.printStackTrace();
                        throw new RuntimeException("Error loading file " + var2, var10);
                    }
                }
            }

            if (var6 == 0) {
                logger.error("There is SceneLoader provider to_q the file " + var2);
                throw new RuntimeException("There is SceneLoader provider to_q the file " + var2);
            } else if (var6 > 1) {
                logger.error("There are many providers to_q the file " + var2);
                throw new RuntimeException("There are many providers to_q the file " + var2);
            }
        }
    }

    /**
     * Получить ссылку на класса обёртку атрибута файла .pro
     *
     * @param var1
     * @return
     */
    public ClassDescriptor getClassDescriptor(String var1) {
        return (ClassDescriptor) this.registeredDescriptors.get(var1);
    }

    /**
     * добавление в список ресурсы указанные в файле с расширением .pro
     *
     * @param var1 обёртка адреса файла каталога data/hud.pro
     * @param var2 Пример data/gui/imageset/imageset_hud/target_aimprediction.png
     */
    public void addInclude(StockFile var1, String var2) {
        var1.includes.add(var2);
        if (!this.stockFiles.containsKey(var2)) {
            this.missingIncludes.add(var2);
        }

    }

    public StockEntry getStockEntry(String var1) {
        return (StockEntry) this.stockEntryCache.get(var1);
    }

    public ClassDescriptor getClassDescriptor(RenderAsset var1) {
        return null;
    }

    public StockEntry getEntry(RenderAsset var1) {
        return null;
    }

    public StockFile getStockFile(String var1) {
        return (StockFile) this.stockFiles.get(var1);
    }

    /**
     * Распределяем результат парсинга раздела includes и objects из файла .pro в списки загрузчика сцены
     *
     * @param var1 РЕзультат парсинка раздел includes и objects
     */
    void addStockFile(StockFile var1) {
        if (var1.getName() == null) {
            throw new RuntimeException("Unnamed stock file");
        } else {
            this.missingIncludes.remove(var1.getName());
            this.stockFiles.put(var1.getName(), var1);
            Iterator var3 = var1.entries.iterator();//Достаём список раздела objects

            while (var3.hasNext()) {
                StockEntry var2 = (StockEntry) var3.next();
                this.stockEntryCache.put(var2.getName(), var2);
            }

        }
    }

    public void addEntry(StockFile var1, StockEntry var2) {
        var1.addEntry(var2);
        this.stockEntryCache.put(var2.getName(), var2);
    }

    /**
     * Упаковка имени и данных в обёртку результата загрузки данных
     *
     * @param var1 обёртак результата загрузки ресурса
     * @param var2 Локальное имя файла
     * @param var3 данные ресурса
     * @return
     */
    StockEntry addEntry(StockFile var1, String var2, RenderAsset var3) {
        StockEntry var4 = new StockEntry(var2);//Задаём имя
        var4.setAsset(var3);//Данные текстуры
        var1.addEntry(var4);//помещяем в список объектов
        return var4;
    }

    public Collection allAssets() {
        return (Collection) this.stockEntryCache.keySet();
    }

    /**
     * создание объекта на сцене
     *
     * @param var1
     * @return
     */
    public RenderAsset instantiateAsset(String var1) {
        RenderAsset var2 = this.getAsset(var1);
        return var2 != null ? var2.cloneAsset() : null;
    }

    public RenderAsset getAsset(String var1) {
        StockEntry var2 = (StockEntry) this.stockEntryCache.get(var1);
        if (var2 == null) {
            return null;
        } else {
            RenderAsset var3 = var2.getAsset();
            if (!var2.isComplete() && !var2.completing) {
                var2.completing = true;
                if (var3 == null) {
                    if (var2.getClonesFrom() == null) {
                        return null;
                    }

                    RenderAsset var4 = this.getAsset(var2.getClonesFrom());
                    if (var4 == null) {
                        return null;
                    }
                    var2.setAsset(var4.cloneAsset());
                    var3 = var2.getAsset();
                }
                for (int var7 = 0; var7 < var2.unassignedProperties.size(); ++var7) {
                    UnassignedProperty var5 = (UnassignedProperty) var2.unassignedProperties.get(var7);
                    if (!var5.doAssign(this, var3)) {
                        ;
                    }
                }
                var2.unassignedProperties.clear();
                var2.completing = false;
                try {
                    var3.validate(this);
                } catch (Exception var6) {
                    logger.warn(var6.toString());
                }
                return var3;
            } else {
                return var3;
            }
        }
    }

    /**
     * Список файлов для загрузки из раздела  includes
     *
     * @return
     */
    public Collection getMissingIncludes() {
        return this.missingIncludes;
    }

    public Texture getDefaultSelfIluminationTexture() {
        if (this.defaultEmissiveTexture == null) {
            this.defaultEmissiveTexture = new Texture();
            ByteBuffer[] var1 = new ByteBuffer[]{ByteBuffer.allocate(4)};
            var1[0].put((byte) 0);
            var1[0].put((byte) 0);
            var1[0].put((byte) 0);
            var1[0].put((byte) -1);
            var1[0].position(0);
            TextureData var2 = new TextureData(6408, 1, 1, 0, 6408, 5121, false, false, var1, (Flusher) null);
            var2.setMipmap(false);
            this.defaultEmissiveTexture.setTextureData(0, var2);
            this.defaultEmissiveTexture.setName("Default emissive texture");
        }
        return this.defaultEmissiveTexture;
    }

    public Texture getDefaultNormalTexture() {
        if (this.defaultNormalTexture == null) {
            this.defaultNormalTexture = new Texture();
            ByteBuffer[] var1 = new ByteBuffer[]{ByteBuffer.allocate(4)};
            var1[0].put((byte) 127);
            var1[0].put((byte) 127);
            var1[0].put((byte) -1);
            var1[0].put((byte) 0);
            var1[0].position(0);
            TextureData var2 = new TextureData(6408, 1, 1, 0, 6408, 5121, false, false, var1, (Flusher) null);
            var2.setMipmap(false);
            this.defaultNormalTexture.setTextureData(0, var2);
            this.defaultNormalTexture.setName("Default normal texture");
        }
        return this.defaultNormalTexture;
    }

    public Texture getDefaultDiffuseTexture() {
        if (this.defaultDiffuseTexture == null) {
            this.defaultDiffuseTexture = new Texture();
            ByteBuffer[] var1 = new ByteBuffer[]{ByteBuffer.allocate(16)};
            var1[0].put((byte) -1);
            var1[0].put((byte) -1);
            var1[0].put((byte) 0);
            var1[0].put((byte) -1);
            var1[0].put((byte) 0);
            var1[0].put((byte) 0);
            var1[0].put((byte) -1);
            var1[0].put((byte) -1);
            var1[0].put((byte) 0);
            var1[0].put((byte) 0);
            var1[0].put((byte) -1);
            var1[0].put((byte) -1);
            var1[0].put((byte) -1);
            var1[0].put((byte) -1);
            var1[0].put((byte) 0);
            var1[0].put((byte) -1);
            var1[0].position(0);
            TextureData var2 = new TextureData(6408, 2, 2, 0, 6408, 5121, false, false, var1, (Flusher) null);
            var2.setMipmap(false);
            this.defaultDiffuseTexture.setTextureData(0, var2);
            this.defaultDiffuseTexture.setMagFilter(TexMagFilter.NEAREST);
            this.defaultDiffuseTexture.setMinFilter(TexMinFilter.NEAREST);
            this.defaultDiffuseTexture.setName("Default diffuse texture");
        }
        return this.defaultDiffuseTexture;
    }

    public Shader getDefaultTrailShader() {
        if (this.defaultTrailShader == null) {
            this.defaultTrailShader = new Shader();
            this.defaultTrailShader.setName("Default trail shader");
            ShaderPass var1 = new ShaderPass();
            var1.setName("Default trail pass");
            ChannelInfo var2 = new ChannelInfo();
            var2.setTexChannel(8);
            var1.setChannelTextureSet(0, var2);
            var1.setBlendEnabled(true);
            var1.setAlphaTestEnabled(true);
            var1.setColorMask(true);
            var1.setDepthMask(false);
            var1.setAlphaMask(false);
            var1.setColorArrayEnabled(true);
            var1.setDstBlend(BlendType.ONE);
            var1.setSrcBlend(BlendType.SRC_ALPHA);
            var1.getRenderStates().setCullFaceEnabled(true);
            this.defaultTrailShader.addPass(var1);
        }
        return this.defaultTrailShader;
    }

    public Shader getDefaultSpaceDustShader() {
        if (this.defaultSpaceDustShader == null) {
            this.defaultSpaceDustShader = new Shader();
            this.defaultSpaceDustShader.setName("Default SpaceDust shader");
            ShaderPass var1 = new ShaderPass();
            var1.setName("Default SpaceDust pass");
            ChannelInfo var2 = new ChannelInfo();
            var2.setTexChannel(8);
            var1.setChannelTextureSet(0, var2);
            var1.setBlendEnabled(true);
            var1.setAlphaTestEnabled(true);
            var1.setDepthMask(false);
            var1.setColorArrayEnabled(true);
            var1.setDstBlend(BlendType.ONE_MINUS_SRC_ALPHA);
            var1.setSrcBlend(BlendType.ONE);
            var1.getRenderStates().setCullFaceEnabled(false);
            this.defaultSpaceDustShader.addPass(var1);
        }
        return this.defaultSpaceDustShader;
    }

    public Shader getDefaultBillboardShader() {
        if (this.defaultBillboardShader == null) {
            this.defaultBillboardShader = new Shader();
            this.defaultBillboardShader.setName("Default billboard shader");
            ShaderPass var1 = new ShaderPass();
            var1.setName("Default billboard pass");
            ChannelInfo var2 = new ChannelInfo();
            var2.setTexChannel(8);
            var1.setChannelTextureSet(0, var2);
            var1.setBlendEnabled(true);
            var1.setAlphaTestEnabled(true);
            var1.setDstBlend(BlendType.ONE_MINUS_SRC_ALPHA);
            var1.setSrcBlend(BlendType.ONE);
            var1.setCullFaceEnabled(true);
            var1.getRenderStates().setCullFaceEnabled(true);
            this.defaultBillboardShader.addPass(var1);
        }
        return this.defaultBillboardShader;
    }

    public Shader getDefaultHudMarkShader() {
        if (this.defaultHudMarkShader == null) {
            this.defaultHudMarkShader = new Shader();
            this.defaultHudMarkShader.setName("Default hudmark shader");
            ShaderPass var1 = new ShaderPass();
            var1.setName("Default hudmark pass");
            ChannelInfo var2 = new ChannelInfo();
            var2.setTexChannel(8);
            var1.setChannelTextureSet(0, var2);
            var1.setBlendEnabled(true);
            var1.setAlphaTestEnabled(true);
            var1.setDstBlend(BlendType.ONE_MINUS_SRC_ALPHA);
            var1.setSrcBlend(BlendType.ONE);
            var1.setCullFaceEnabled(false);
            var1.getRenderStates().setCullFaceEnabled(false);
            this.defaultHudMarkShader.addPass(var1);
        }
        return this.defaultHudMarkShader;
    }

    public Collection getStockEntries() {
        return this.stockEntryCache.values();
    }

    public Material getDefaultMaterial() {
        if (this.defaultMaterial == null) {
            this.defaultMaterial = new Material();
            this.defaultMaterial.setName("Default material");
            this.defaultMaterial.setDiffuseTexture(this.getDefaultDiffuseTexture());
            this.defaultMaterial.setNormalTexture(this.getDefaultNormalTexture());
            this.defaultMaterial.setSelfIluminationTexture(this.getDefaultSelfIluminationTexture());
            this.defaultMaterial.setShader(this.getDefaultDiffuseShader());
        }
        return this.defaultMaterial;
    }

    public Shader getDefaultDiffuseShader() {
        if (this.defaultDiffuseShader == null) {
            this.defaultDiffuseShader = new Shader();
            this.defaultDiffuseShader.setName("Default diffuse shader");
            ShaderPass var1 = new ShaderPass();
            var1.setName("Default diffuse pass");
            ChannelInfo var2 = new ChannelInfo();
            var2.setTexChannel(8);
            var1.setChannelTextureSet(0, var2);
            var1.setAlphaTestEnabled(true);
            var1.setCullFaceEnabled(true);
            var1.getRenderStates().setCullFaceEnabled(true);
            this.defaultDiffuseShader.addPass(var1);
        }
        return this.defaultDiffuseShader;
    }

    public Shader getDefaultBillboardShaderNoDepth() {
        if (this.defaultBillboardShaderNoDepth == null) {
            this.defaultBillboardShaderNoDepth = new Shader();
            this.defaultBillboardShaderNoDepth.setName("Default billboard shader test_GLCanvas depth");
            ShaderPass var1 = new ShaderPass();
            var1.setName("Default billboard pass");
            ChannelInfo var2 = new ChannelInfo();
            var2.setTexChannel(8);
            var1.setChannelTextureSet(0, var2);
            var1.setBlendEnabled(true);
            var1.setAlphaTestEnabled(true);
            var1.setDepthMask(false);
            var1.setDstBlend(BlendType.ONE);
            var1.setSrcBlend(BlendType.ONE);
            var1.setCullFaceEnabled(true);
            var1.getRenderStates().setCullFaceEnabled(true);
            this.defaultBillboardShaderNoDepth.addPass(var1);
        }
        return this.defaultBillboardShaderNoDepth;
    }

    public Shader getDefaultVertexColorShader() {
        if (this.defaultVertexColorShader == null) {
            this.defaultVertexColorShader = new Shader();
            this.defaultVertexColorShader.setName("Default vertex color shader");
            ShaderPass var1 = new ShaderPass();
            var1.setName("Default vertex color pass");
            var1.setBlendEnabled(true);
            var1.setAlphaTestEnabled(true);
            var1.setDepthMask(false);
            var1.setDepthTestEnabled(false);
            var1.setDstBlend(BlendType.ONE_MINUS_SRC_ALPHA);
            var1.setSrcBlend(BlendType.SRC_ALPHA);
            var1.getRenderStates().setCullFaceEnabled(true);
            this.defaultVertexColorShader.addPass(var1);
        }
        return this.defaultVertexColorShader;
    }

    public Shader getDefaultWireframeShader() {
        if (this.defaultWireframeShader == null) {
            this.defaultWireframeShader = new Shader();
            this.defaultWireframeShader.setName("Default wireframe shader");
            ShaderPass var1 = new ShaderPass();
            var1.setName("Default wireframe pass");
            var1.setDepthMask(false);
            var1.setDepthTestEnabled(true);
            var1.setLineMode(true);
            var1.getRenderStates().setCullFaceEnabled(false);
            this.defaultWireframeShader.addPass(var1);
        }
        return this.defaultWireframeShader;
    }

    public Shader getDefaultDecalShader() {
        if (this.defaultDecalShader == null) {
            this.defaultDecalShader = new Shader();
            this.defaultDecalShader.setName("Default decal shader");
            ShaderPass var1 = new ShaderPass();
            var1.setName("Default decal pass");
            ChannelInfo var2 = new ChannelInfo();
            var2.setTexChannel(8);
            var1.setChannelTextureSet(0, var2);
            var1.setBlendEnabled(true);
            var1.setAlphaTestEnabled(true);
            var1.setDepthMask(false);
            var1.setDepthTestEnabled(true);
            var1.setDstBlend(BlendType.ONE_MINUS_SRC_ALPHA);
            var1.setSrcBlend(BlendType.ONE);
            var1.getRenderStates().setCullFaceEnabled(true);
            this.defaultDecalShader.addPass(var1);
        }
        return this.defaultDecalShader;
    }

    public Shader getDefaultFlatShader() {
        if (this.defaultFlatShader == null) {
            this.defaultFlatShader = new Shader();
            this.defaultFlatShader.setName("Default flat shader");
            ShaderPass var1 = new ShaderPass();
            var1.setName("Default flat pass");
            ChannelInfo var2 = new ChannelInfo();
            var2.setTexChannel(8);
            var1.setChannelTextureSet(0, var2);
            this.defaultFlatShader.addPass(var1);
        }
        return this.defaultFlatShader;
    }

    public Shader getDefaultAlphaTestShader() {
        if (this.defaultAlphaTestShader == null) {
            this.defaultAlphaTestShader = new Shader();
            this.defaultAlphaTestShader.setName("Default alpha test shader");
            ShaderPass var1 = new ShaderPass();
            var1.setName("Default alpha test pass");
            this.defaultAlphaTestShader.addPass(var1);
            var1.setBlendEnabled(true);
            var1.setAlphaTestEnabled(true);
            var1.setAlphaFunc(DepthFuncType.GEQUAL);
            var1.setAlphaRef(1.0F);
            var1.setDepthMask(false);
            var1.setDepthTestEnabled(false);
            var1.setSrcBlend(BlendType.DST_ALPHA);
        }
        return this.defaultAlphaTestShader;
    }

    public ImageLoader getImageLoader() {
        return this.imageLoader;
    }
}
