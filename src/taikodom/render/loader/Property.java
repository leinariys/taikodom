package taikodom.render.loader;

public @interface Property {
    String name();

    String label() default "";

    String group() default "";

    boolean persisted() default true;

    boolean editable() default true;

    boolean writable() default true;
}
