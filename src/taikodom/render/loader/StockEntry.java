package taikodom.render.loader;

import java.util.ArrayList;
import java.util.List;

/**
 * Обёрта результата чтения ресурса из раздела includes или objects , файла различного расширения
 */
public class StockEntry {
    /**
     * Список атрибутов тега и атрибутов дочерних тегов из файла .pro
     */
    public List unassignedProperties = new ArrayList();
    public boolean completing;
    /**
     * Ссылка на результат парсинга
     */
    protected StockFile stockFile;
    /**
     * Значение атрибута name в теге из файла .pro Пример name="TranspS2"
     */
    protected String name;
    protected boolean complete;
    protected String clonesFrom;
    protected boolean unloaded;
    /**
     * Ссылка на Класс обёртка тега Например Shader
     */
    private RenderAsset asset;

    public StockEntry(String var1) {
        this.name = var1;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String var1) {
        this.name = var1;
    }

    public StockFile getStockFile() {
        return this.stockFile;
    }

    public void setStockFile(StockFile var1) {
        this.stockFile = var1;
    }

    public boolean isComplete() {
        return this.complete;
    }

    public void setComplete(boolean var1) {
        this.complete = var1;
    }

    public String getClonesFrom() {
        return this.clonesFrom;
    }

    public void setClonesFrom(String var1) {
        this.clonesFrom = var1;
    }

    public boolean isUnloaded() {
        return this.unloaded;
    }

    public void setUnloaded(boolean var1) {
        this.unloaded = var1;
    }

    public RenderAsset getAsset() {
        return this.asset;
    }

    /**
     * Установить класс обёртки ресурса
     *
     * @param var1 Класс обёртка ресурса Например Shader
     */
    public void setAsset(RenderAsset var1) {
        this.asset = var1;
    }
}
