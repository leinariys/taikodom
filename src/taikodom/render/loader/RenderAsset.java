package taikodom.render.loader;

import taikodom.render.NotExported;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Базовый класс всех классов соответствующщие тегу в файлах с расширением .pro
 * class Shader ....
 */
public abstract class RenderAsset {
    private static final AtomicInteger nextId = new AtomicInteger();
    /**
     * Пример /data/gui/imageset/Splash_Screen.bik или fig_spa_bullfrog
     */
    protected String name;
    private int handle;

    public RenderAsset() {
        this.handle = nextId.getAndIncrement();
        this.name = "";
    }

    public RenderAsset(RenderAsset var1) {
        this.handle = var1.handle;
        this.name = var1.name;
    }

    public abstract RenderAsset cloneAsset();

    /**
     * Получить путь к файлу
     *
     * @return Пример /data/gui/imageset/Splash_Screen.bik
     */
    public String getName() {
        return this.name;
    }

    /**
     * Происвоить имя
     *
     * @param var1 Пример /data/gui/imageset/Splash_Screen.bik
     */
    public void setName(String var1) {
        this.name = var1;
    }

    public void invalidateHandle() {
        this.handle = nextId.getAndIncrement();
    }

    @NotExported
    public int getHandle() {
        return this.handle;
    }

    public void validate(SceneLoader var1) {
        if (this.name == null) {
            throw new RuntimeException("No 'name' property");
        }
    }

    public String toString() {
        return this.getClass().getSimpleName() + ": " + this.name;
    }

    public void releaseReferences() {
    }
}
