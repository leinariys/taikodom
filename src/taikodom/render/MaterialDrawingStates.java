package taikodom.render;

import taikodom.render.textures.ChannelInfo;

import java.util.ArrayList;
import java.util.List;

public class MaterialDrawingStates {
    public final List states = new ArrayList();
    public int numTextures;

    public ChannelInfo getTexInfo(int var1) {
        return (ChannelInfo) this.states.get(var1);
    }

    public List getStates() {
        return this.states;
    }

    public void setStates(List var1) {
        this.states.clear();
        this.states.addAll(var1);
    }

    public void clear() {
        this.states.clear();
    }
}
