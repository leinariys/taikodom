package taikodom.render.helpers;

import taikodom.render.RenderView;

public interface RenderTask {
    void run(RenderView var1);
}
