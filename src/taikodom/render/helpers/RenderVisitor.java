package taikodom.render.helpers;

import taikodom.render.scene.SceneObject;

public interface RenderVisitor {
    boolean visit(SceneObject var1);
}
