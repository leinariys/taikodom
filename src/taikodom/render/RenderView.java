package taikodom.render;

import all.*;
import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import com.sun.opengl.util.GLUT;
import taikodom.render.camera.Camera;
import taikodom.render.enums.LightType;
import taikodom.render.postProcessing.PostProcessingFX;
import taikodom.render.primitives.Light;
import taikodom.render.primitives.Primitive;
import taikodom.render.primitives.TreeRecord;
import taikodom.render.scene.LightRecordList;
import taikodom.render.scene.Scene;
import taikodom.render.scene.TreeRecordList;
import taikodom.render.scene.Viewport;
import taikodom.render.shaders.RenderStates;
import taikodom.render.shaders.Shader;
import taikodom.render.shaders.ShaderPass;
import taikodom.render.textures.ChannelInfo;
import taikodom.render.textures.Material;
import taikodom.render.textures.RenderTarget;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLContext;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;
import javax.vecmath.Matrix3f;
import java.util.Collections;
import java.util.Iterator;

/**
 * Отрисовка примитивов
 */
public class RenderView {
    private static RenderTarget framebufferRenderTarget;
    private final bc_q camTransform = new bc_q();
    private final bc_q camAffineTransform = new bc_q();
    private final Vector3dOperations gpuOffset = new Vector3dOperations();
    private final ajK camProjection = new ajK();
    private final ajK gpuCamAffine = new ajK();
    private final Viewport viewport = new Viewport();
    /**
     * Графическая библиотека, надстройка над OpenGL, использующая её функции для рисования более сложных объектов(квадратичных поверхностей).
     */
    GLU glu = new GLU();
    /**
     * Библиотека утилит для приложений под OpenGL, отвечает за системный уровень операций ввода-вывода
     * Функции: создание окна, управление окном, мониторинг за вводом с клавиатуры и событий мыши...
     */
    GLUT glut = new GLUT();
    /**
     * Квадрика - createProcessingInstructionSelector-мерная гиперповерхность, геометрические фигуры 2-го порядка, т.е. сфера, цилиндр, диск, конус.
     * Функция gluNewQuadric создает новый объект нужного нам типа (в реальности просто создается указатель на пустую структуру).
     */
    GLUquadric quadric;
    private RenderContext renderContext = new RenderContext();
    private DrawContext drawContext = new DrawContext();
    private RenderStates defaultStates = new RenderStates();
    private Color white = new Color(1.0F, 1.0F, 1.0F, 1.0F);
    private float[] one = new float[]{1.0F, 1.0F, 1.0F, 1.0F};
    private float[] zero = new float[]{0.0F, 0.0F, 0.0F, 0.0F};
    private float[] tempFloatArray = new float[]{0.0F, 0.0F, 0.0F, 0.0F};
    private RenderTarget renderTarget;
    private SceneView currentSceneView;
    private RenderInfo renderInfo = new RenderInfo();

    /**
     * @param var1
     */
    public RenderView(GLContext var1) {
        this.quadric = this.glu.gluNewQuadric();//Квадрик фигура пустышка, для заполнения
        this.getDrawContext().setRenderView(this);
        this.getDrawContext().setGlContext(var1);
        /**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
         * седержит методы для рисования примитивов (линии треугольники)*/
        this.setGL(var1.getGL());
        this.renderContext.setDc(this.drawContext);
    }

    /**
     * @param var1 Поверхность для рисования OpenGL и примитивы рисования(линии треугольники).
     */
    public RenderView(GLAutoDrawable var1) {
        this.quadric = this.glu.gluNewQuadric();//Квадрик фигура пустышка, для заполнения
        this.setViewport(0, 0, var1.getWidth(), var1.getHeight());//Сохроняем текущее положение холста
        this.setGL(var1.getGL());//сохроняем инструменты
        this.getDrawContext().setRenderView(this);
        this.getDrawContext().setGlContext(var1.getContext());
        this.renderContext.setDc(this.drawContext);
    }

    public static RenderTarget getFramebufferRenderTarget() {
        return framebufferRenderTarget;
    }

    public static void setFramebufferRenderTarget(RenderTarget var0) {
        framebufferRenderTarget = var0;
    }

    public void setStartFrame(int var1) {
        this.drawContext.setCurrentFrame((long) var1);
    }

    public void render(SceneView var1, double var2, float var4) {
        this.setRenderInfo(var1.getRenderInfo());
        this.renderTarget = var1.getRenderTarget();
        this.setViewport(var1.getViewport());
        this.renderContext.setSceneViewQuality(var1.getSceneViewQuality());
        this.renderContext.setAllowImpostors(var1.isAllowImpostors());
        this.currentSceneView = var1;
        this.render(var1.getScene(), var1.getCamera(), var2, var4);
    }

    public void render(Scene var1, Camera var2, double var3, float var5) {
        this.drawContext.setMaxShaderQuality(this.renderContext.getSceneViewQuality().getShaderQuality());
        if (this.drawContext.debugDraw()) {
            this.drawContext.log("----------------------------");
            this.drawContext.log("render");
        }

        this.renderContext.clearRecordsAndLights();
        this.drawContext.incCurrentFrame();
        this.drawContext.setElapsedTime(var5);
        this.drawContext.setTotalTime(var3);
        this.renderContext.setDeltaTime(var5);
        this.setupCamera(var1, var2);
        var1.render(this.renderContext);
        if (this.renderContext.getImpostorManager() != null && (this.currentSceneView == null || this.currentSceneView.isAllowImpostors())) {
            this.renderContext.getImpostorManager().updateImpostors(this.renderContext, this.currentSceneView);
        }

        this.renderContext.getRecords().sort();
        if (this.currentSceneView != null) {
            Iterator var7 = this.currentSceneView.getPostProcessingFXs().iterator();

            while (var7.hasNext()) {
                PostProcessingFX var6 = (PostProcessingFX) var7.next();
                if (var6.isEnabled()) {
                    var6.setSceneIntensity(var1.getColorCorrection());
                    var6.preRender(this.renderContext, this.currentSceneView);
                }
            }
        }

        if (this.renderTarget != null) {
            this.renderTarget.bind(this.drawContext);
        }

        this.renderEntries(var2);
        GL var9 = this.drawContext.getGL();
        if (this.renderInfo.isDrawAABBs()) {
            this.renderAABBs();
            var1.renderAABBs(var9, this.gpuOffset);
        }

        if (this.renderInfo.isDrawLights()) {
            this.renderLights();
        }

        if (this.renderTarget != null) {
            this.renderTarget.copyBufferToTexture(this.drawContext);
            this.renderTarget.unbind(this.drawContext);
        }

        this.drawContext.setRendering(false);
        if (this.currentSceneView != null) {
            Iterator var8 = this.currentSceneView.getPostProcessingFXs().iterator();

            while (var8.hasNext()) {
                PostProcessingFX var10 = (PostProcessingFX) var8.next();
                if (var10.isEnabled()) {
                    var10.render(this.renderContext, this.currentSceneView);
                }
            }
        }

    }

    /**
     * @param var1
     * @param var2
     */
    public void setupCamera(Scene var1, Camera var2) {
        var2.calculateProjectionMatrix();
        this.camProjection.set(var2.getProjection());
        this.camTransform.b(var2.getTransform());
        this.camTransform.getTranslation(this.gpuOffset);
        this.camTransform.a(this.camAffineTransform);
        this.renderContext.cameraTransform.set(this.camTransform.mX);
        this.renderContext.cameraAffineTransform.j(this.renderContext.cameraTransform);
        this.renderContext.setGpuOffset(this.gpuOffset);
        this.drawContext.setGpuOffset(this.gpuOffset);
        vv var3 = this.renderContext.getFrustum();
        var3.a(this.camTransform, this.camProjection);
        this.drawContext.gpuCamTransform.a((Matrix3f) this.camTransform.mX);
        this.drawContext.gpuCamTransform.p(0.0F, 0.0F, 0.0F);
        this.gpuCamAffine.k(this.drawContext.gpuCamTransform);
        this.drawContext.sceneConfig = var1.getSceneConfig();
        this.drawContext.setViewport(this.viewport.x, this.viewport.y, this.viewport.width, this.viewport.height);
        var2.getTransform().getTranslation(this.drawContext.vec3dTemp0);
        this.renderContext.currentScene = var1;
        this.renderContext.currentCamera = var2;
        var1.fillRenderViewAreaInfo(this.drawContext.vec3dTemp0, this);
    }

    /**
     * @param var1
     */
    public void renderEntries(Camera var1) {
        GL var2 = this.drawContext.getGL();
        var2.glViewport(this.viewport.x, this.viewport.y, this.viewport.width, this.viewport.height);
        var2.glClearColor(this.getClearColor().getRed(), this.getClearColor().getGreen(), this.getClearColor().getBlue(), this.getClearColor().getAlpha());
        var2.glScissor(this.viewport.x, this.viewport.y, this.viewport.width, this.viewport.height);
        var2.glEnable(3089);
        var2.glClear((this.isClearColorBuffer() ? 16384 : 0) | (this.isClearDepthBuffer() ? 256 : 0) | (this.isClearStencilBuffer() ? 1024 : 0));
        var2.glDisable(3089);
        var1.updateGLProjection(this.drawContext);
        this.updateGLModelview(this.drawContext);
        if (this.isWireframeOnly()) {
            this.renderTreeWireFrame();
        } else if (this.isNoShading()) {
            this.renderTreeNoShading();
        } else {
            var2.glEnableClientState(32884);
            this.drawContext.primitiveState.setStates(Collections.EMPTY_LIST);
            this.drawContext.primitiveState.setUseColorArray(false);
            this.drawContext.primitiveState.setUseNormalArray(false);
            this.drawContext.primitiveState.setUseTangentArray(false);
            var2.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
            this.renderTree();
        }

    }

    void renderTree() {
        if (this.drawContext.debugDraw()) {
            this.drawContext.log("renderTree");
        }

        TreeRecordList var1 = this.renderContext.getRecords();
        int var2 = 0;
        GL var3 = this.drawContext.getGL();
        RenderStates var4 = this.defaultStates;
        this.setupGLLights(this.drawContext);
        this.setupGLFog(this.drawContext);
        this.defaultStates.blindlyUpdateGLStates(this.drawContext);

        try {
            while (var2 < var1.size()) {
                TreeRecord var5 = var1.get(var2);
                Shader var6 = var5.shader;
                if (!var6.isCompiled()) {
                    var6.compile(this.drawContext);
                }

                int var7;
                for (var7 = var2; var7 < var1.size() && var1.get(var7).shader == var6; ++var7) {
                    ;
                }

                int var8 = var6.getShaderPasses().size();

                for (int var9 = 0; var9 < var8; ++var9) {
                    this.unbindPrimitive();
                    this.drawContext.currentMaterial = null;
                    this.drawContext.currentPrimitive = null;
                    this.drawContext.shaderParamContext.setLight((Light) null);
                    ShaderPass var10 = this.drawContext.currentPass = var6.getPass(var9);
                    var10.updateStates(this.drawContext, var4);
                    var4 = this.drawContext.currentPass.getRenderStates();
                    var4.fillPrimitiveStates(this.drawContext.primitiveState);
                    var4.fillMaterialStates(this.drawContext.materialState);
                    if (var10.getUseLights() == LightType.NONE) {
                        var10.updateShaderProgramAttribs(this.drawContext);
                        this.renderSubTree(this.drawContext, var2, var7, var10, var9, (Color) null);
                    } else if (var10.getUseFixedFunctionLighting()) {
                        this.renderSubTree(this.drawContext, var2, var7, var10, var9, this.white);
                    } else {
                        var3.glEnable(3089);
                        LightRecordList var11 = this.renderContext.getLights();
                        int var12 = var11.size();

                        for (int var13 = 0; var13 < var12; ++var13) {
                            LightRecordList.LightRecord var14 = var11.get(var13);
                            if (var14.light.setupScissorRectangle(this.renderContext)) {
                                this.drawContext.currentLight = var14;
                                this.drawContext.shaderParamContext.setLight(var14.light);
                                var10.updateShaderProgramAttribs(this.drawContext);
                                this.renderSubTreeByLight(this.drawContext, var2, var7, var10, var9);
                            }
                        }

                        var3.glDisable(3089);
                    }

                    var10.unbind(this.drawContext);
                }

                var2 = var7;
            }
        } catch (Exception var15) {
            System.err.println("Something wrong happened on render pipeline.");
            var15.printStackTrace();
        }

        var3.glDisableClientState(32884);
        this.unbindPrimitive();
        if (var4 != null) {
            this.defaultStates.updateGLStates(this.drawContext, var4);
        } else {
            this.defaultStates.blindlyUpdateGLStates(this.drawContext);
        }

    }

    void renderSphere(Pt var1, GL var2) {
        var2.glPushMatrix();
        var2.glTranslatef(var1.bny().x, var1.bny().y, var1.bny().z);
        this.glu.gluQuadricDrawStyle(this.quadric, 100012);
        this.glu.gluSphere(this.quadric, 0.5D, 10, 10);
        this.glu.gluQuadricDrawStyle(this.quadric, 100011);
        this.glu.gluSphere(this.quadric, (double) var1.getRadius(), 10, 10);
        var2.glPopMatrix();
    }

    void renderAABB(aLH var1, GL var2) {
        Vector3dOperations var3 = var1.dim();
        Vector3dOperations var4 = var1.din();
        var2.glVertex3d(var3.x, var3.y, var3.z);
        var2.glVertex3d(var4.x, var3.y, var3.z);
        var2.glVertex3d(var3.x, var3.y, var3.z);
        var2.glVertex3d(var3.x, var4.y, var3.z);
        var2.glVertex3d(var3.x, var3.y, var3.z);
        var2.glVertex3d(var3.x, var3.y, var4.z);
        var2.glVertex3d(var4.x, var4.y, var4.z);
        var2.glVertex3d(var3.x, var4.y, var4.z);
        var2.glVertex3d(var4.x, var4.y, var4.z);
        var2.glVertex3d(var4.x, var3.y, var4.z);
        var2.glVertex3d(var4.x, var4.y, var4.z);
        var2.glVertex3d(var4.x, var4.y, var3.z);
        var2.glVertex3d(var3.x, var4.y, var3.z);
        var2.glVertex3d(var4.x, var4.y, var3.z);
        var2.glVertex3d(var3.x, var4.y, var3.z);
        var2.glVertex3d(var3.x, var4.y, var4.z);
        var2.glVertex3d(var3.x, var4.y, var4.z);
        var2.glVertex3d(var3.x, var3.y, var4.z);
        var2.glVertex3d(var3.x, var3.y, var4.z);
        var2.glVertex3d(var4.x, var3.y, var4.z);
        var2.glVertex3d(var4.x, var3.y, var3.z);
        var2.glVertex3d(var4.x, var3.y, var4.z);
        var2.glVertex3d(var4.x, var3.y, var3.z);
        var2.glVertex3d(var4.x, var4.y, var3.z);
    }

    void renderLights() {
        GL var1 = this.drawContext.getGL();
        var1.glEnable(3042);
        var1.glBlendFunc(770, 771);
        var1.glColor3f(1.0F, 0.0F, 0.0F);
        int var2 = this.renderContext.lights.size();

        for (int var3 = 0; var3 < var2; ++var3) {
            LightRecordList.LightRecord var4 = this.renderContext.lights.get(var3);
            Light var5 = var4.light;
            var1.glColor3f(var5.getDiffuseColor().x, var5.getDiffuseColor().y, var5.getDiffuseColor().z);
            this.renderSphere(var4.cameraSpaceBoundingSphere, var1);
        }

        var1.glColor3f(1.0F, 1.0F, 1.0F);
        var1.glDisable(3042);
        var1.glBlendFunc(1, 1);
    }

    void renderAABBs() {
        GL var1 = this.drawContext.getGL();
        var1.glEnable(3042);
        var1.glBlendFunc(770, 771);
        var1.glColor3f(1.0F, 0.0F, 0.0F);
        int var2 = this.renderContext.getRecords().size();
        var1.glBegin(GL.GL_LINES);//Этот метод запускает процесс рисования

        for (int var3 = 0; var3 < var2; ++var3) {
            TreeRecord var4 = this.renderContext.getRecords().get(var3);
            this.renderAABB(var4.cameraSpaceBoundingBox, var1);
        }

        var1.glEnd();
        var1.glColor3f(1.0F, 1.0F, 1.0F);
        var1.glDisable(3042);
        var1.glBlendFunc(1, 1);
    }

    void renderTreeNoShading() {
        if (this.drawContext.debugDraw()) {
            this.drawContext.log("renderTreeWireFrame");
        }

        TreeRecordList var1 = this.renderContext.getRecords();
        GL var2 = this.drawContext.getGL();
        var2.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        var2.glEnableClientState(32884);
        var2.glEnableClientState(32888);
        var2.glDisable(2884);
        var2.glEnable(2929);
        var2.glEnable(3553);
        ChannelInfo var3 = new ChannelInfo();
        var3.setTexChannel(8);
        this.drawContext.primitiveState.clear();
        this.drawContext.materialState.clear();
        this.drawContext.primitiveState.states.add(var3);
        this.drawContext.materialState.states.add(var3);
        Primitive var4 = null;
        Material var5 = null;

        for (int var6 = 0; var6 < var1.size(); ++var6) {
            TreeRecord var7 = var1.get(var6);
            if (var4 == null || var4.getHandle() != var7.primitive.getHandle()) {
                if (var4 != null && var4.getUseVBO() && !var7.primitive.getUseVBO()) {
                    this.unbindPrimitive();
                }

                var4 = var7.primitive;
                var4.bind(this.drawContext);
            }

            if (var5 == null || var5.getHandle() != var7.material.getHandle()) {
                var2.glMatrixMode(5890);
                var7.material.bind(this.drawContext);
                var5 = var7.material;
                var2.glMatrixMode(5888);
            }

            var2.glPushMatrix();
            this.drawContext.tempBuffer0.clear();
            var7.gpuTransform.asColumnMajorBuffer(this.drawContext.tempBuffer0);
            this.drawContext.tempBuffer0.flip();
            var2.glMultMatrixf(this.drawContext.tempBuffer0);
            var4.draw(this.drawContext);
            var2.glPopMatrix();
        }

        var2.glPolygonMode(1028, 6914);
        var2.glPolygonMode(1029, 6914);
        var2.glDisableClientState(32884);
        var2.glDisableClientState(32888);
        var2.glDisable(3042);
        var2.glBlendFunc(1, 1);
        var2.glEnable(2884);
        var2.glDisable(3553);
        this.unbindPrimitive();
        this.defaultStates.blindlyUpdateGLStates(this.drawContext);
        this.drawContext.primitiveState.clear();
        this.drawContext.materialState.clear();
    }

    void renderTreeWireFrame() {
        if (this.drawContext.debugDraw()) {
            this.drawContext.log("renderTreeWireFrame");
        }

        TreeRecordList var1 = this.renderContext.getRecords();
        GL var2 = this.drawContext.getGL();
        var2.glEnable(3042);
        var2.glBlendFunc(770, 771);
        var2.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        var2.glPolygonMode(1028, 6913);
        var2.glPolygonMode(1029, 6913);
        var2.glEnableClientState(32884);
        var2.glDisable(2884);
        Primitive var3 = null;

        for (int var4 = 0; var4 < var1.size(); ++var4) {
            TreeRecord var5 = var1.get(var4);
            if (var3 != var5.primitive) {
                if (var3 != null && var3.getUseVBO() && !var5.primitive.getUseVBO()) {
                    this.unbindPrimitive();
                }

                var3 = var5.primitive;
                var3.bind(this.drawContext);
            }

            var2.glPushMatrix();
            this.drawContext.tempBuffer0.clear();
            var5.gpuTransform.asColumnMajorBuffer(this.drawContext.tempBuffer0);
            this.drawContext.tempBuffer0.flip();
            var2.glMultMatrixf(this.drawContext.tempBuffer0);
            if (var3 != null) {
                var3.draw(this.drawContext);
            }

            var2.glPopMatrix();
        }

        var2.glPolygonMode(1028, 6914);
        var2.glPolygonMode(1029, 6914);
        var2.glDisableClientState(32884);
        var2.glDisable(3042);
        var2.glBlendFunc(1, 1);
        var2.glEnable(2884);
        this.unbindPrimitive();
    }

    private void setupGLFog(DrawContext var1) {
        var1.getGL().glFogi(2917, this.getFogType());
        var1.getGL().glFogf(2915, this.getFogStart());
        var1.getGL().glFogf(2916, this.getFogEnd());
        var1.getGL().glFogf(2914, this.getFogExp());
        var1.floatBuffer60Temp0.clear();
        var1.getGL().glFogfv(2918, this.getFogColor().fillFloatBuffer(var1.floatBuffer60Temp0));
    }

    private void setupGLLights(DrawContext var1) {
        GL var2 = var1.getGL();
        this.sortLights();
        byte var3 = 8;
        int var4 = this.renderContext.lights.size();

        int var5;
        for (var5 = var4; var5 < var3; ++var5) {
            var2.glDisable(16384 + var5);
        }

        for (var5 = 0; var5 < var4 && var5 < var3; ++var5) {
            LightRecordList.LightRecord var6 = this.renderContext.lights.get(var5);
            Light var7 = var6.light;
            this.tempFloatArray[0] = var7.getDiffuseColor().x;
            this.tempFloatArray[1] = var7.getDiffuseColor().y;
            this.tempFloatArray[2] = var7.getDiffuseColor().z;
            this.tempFloatArray[3] = var7.getDiffuseColor().w;
            var2.glLightfv(16384 + var5, 4609, this.tempFloatArray, 0);
            var2.glLightfv(16384 + var5, 4610, this.zero, 0);
            if (var7.getLightType() == LightType.DIRECTIONAL_LIGHT) {
                this.tempFloatArray[0] = -var7.getDirection().x;
                this.tempFloatArray[1] = -var7.getDirection().y;
                this.tempFloatArray[2] = -var7.getDirection().z;
                this.tempFloatArray[3] = 0.0F;
                var2.glLightfv(16384 + var5, 4611, this.tempFloatArray, 0);
                var2.glLightf(16384 + var5, 4615, 0.0F);
                var2.glLightf(16384 + var5, 4616, 0.0F);
                var2.glLightf(16384 + var5, 4617, 0.0F);
            } else {
                Vec3f var8 = var6.cameraSpaceBoundingSphere.bny();
                this.tempFloatArray[0] = var8.x;
                this.tempFloatArray[1] = var8.y;
                this.tempFloatArray[2] = var8.z;
                this.tempFloatArray[3] = 1.0F;
                var2.glLightfv(16384 + var5, 4611, this.tempFloatArray, 0);
                if (var7.getLightType() == LightType.SPOT_LIGHT) {
                    float var9 = (float) (180.0D * Math.acos((double) var7.getCosCutoff()) / 3.141592653589793D);
                    var2.glLightf(16384 + var5, 4614, var9);
                    this.tempFloatArray[0] = var7.getDirection().x;
                    this.tempFloatArray[1] = var7.getDirection().y;
                    this.tempFloatArray[2] = var7.getDirection().z;
                    this.tempFloatArray[3] = 1.0F;
                    var2.glLightfv(16384 + var5, 4612, this.tempFloatArray, 0);
                    var2.glLightf(16384 + var5, 4613, var7.getSpotExponent());
                } else {
                    var2.glLightf(16384 + var5, 4614, 180.0F);
                }

                var2.glLightf(16384 + var5, 4615, 0.0F);
                var2.glLightf(16384 + var5, 4616, 1.0F / var7.getRadius());
                var2.glLightf(16384 + var5, 4617, 1.0F / (var7.getRadius() * var7.getRadius()));
            }

            var2.glEnable(16384 + var5);
        }

    }

    private void sortLights() {
        if (this.renderContext.lights.size() > 0) {
            for (int var1 = 0; var1 < this.renderContext.lights.size(); ++var1) {
                LightRecordList.LightRecord var2 = this.renderContext.lights.get(var1);
                var2.light.setCurrentDistanceToCamera(var2.light.getPosition().ax(this.camTransform.position));
            }

            this.renderContext.lights.sort();
        }

    }

    private void unbindPrimitive() {
        this.drawContext.getGL().glBindBufferARB(34962, 0);
        this.drawContext.getGL().glBindBufferARB(34963, 0);
    }

    public void updateGLModelview(DrawContext var1) {
        /**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
         * седержит методы для рисования примитивов (линии треугольники)*/
        GL var2 = var1.getGL();
        var2.glMatrixMode(5888);
        var2.glLoadIdentity();
        var1.tempBuffer0.clear();
        this.gpuCamAffine.asColumnMajorBuffer(var1.tempBuffer0);
        var1.tempBuffer0.flip();
        var2.glMultMatrixf(var1.tempBuffer0);
    }

    /**
     * GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
     * седержит методы для рисования примитивов (линии треугольники)
     *
     * @param var1
     */
    public void setGL(GL var1) {
        this.drawContext.setGL(var1);
    }

    private void renderSubTreeByLight(DrawContext var1, int var2, int var3, ShaderPass var4, int var5) {
        if (var1.debugDraw()) {
            var1.log("renderSubTreeByLight");
        }

        TreeRecordList var6 = this.renderContext.getRecords();

        for (int var7 = var2; var7 < var3; ++var7) {
            TreeRecord var8 = var6.get(var7);
            if (var4.getUseLights() == var1.currentLight.light.getLightType() && (var1.currentLight.light.getLightType() == LightType.DIRECTIONAL_LIGHT || var8.cameraSpaceBoundingBox.a(var1.currentLight.cameraSpaceBoundingSphere))) {
                var1.currentRecord = var8;
                GL var9 = var1.getGL();
                if (var1.currentMaterial == null || var8.material.getHandle() != var1.currentMaterial.getHandle()) {
                    var9.glMatrixMode(5890);
                    var8.material.bind(var1);
                    var9.glMatrixMode(5888);
                    var1.currentMaterial = var8.material;
                    var4.updateMaterialProgramAttribs(var1);
                    var8.material.updateShaderParameters(var5, var1);
                }

                if (var1.currentPrimitive == null || var8.primitive.getHandle() != var1.currentPrimitive.getHandle()) {
                    if (var1.currentPrimitive != null && var1.currentPrimitive.getUseVBO() && !var8.primitive.getUseVBO()) {
                        this.unbindPrimitive();
                    }

                    var8.primitive.bind(var1);
                    var1.currentPrimitive = var8.primitive;
                }

                var9.glPushMatrix();
                var4.updatePrimitiveProgramAttribs(var1);
                Color var10 = var8.color;
                if (var4.getUseSceneAmbientColor()) {
                    Color var11 = var8.areaInfo.ambientColor;
                    var9.glColor4f(var10.x * var11.x, var10.y * var11.y, var10.z * var11.z, var10.w * var11.w);
                } else {
                    var9.glColor4f(var10.x, var10.y, var10.z, var10.w);
                }

                var1.floatBuffer60Temp0.clear();
                var9.glMultMatrixf(var8.gpuTransform.b(var1.floatBuffer60Temp0));
                if (var8.occlusionQuery != null) {
                    var8.occlusionQuery.begin(var9);
                    var1.currentPrimitive.draw(var1);
                    var8.occlusionQuery.end(var9);
                } else {
                    var1.currentPrimitive.draw(var1);
                }

                var9.glPopMatrix();
            }
        }

        if (var1.currentMaterial != null) {
            var1.currentMaterial.unbind(var1);
        }

    }

    private void renderSubTree(DrawContext var1, int var2, int var3, ShaderPass var4, int var5, Color var6) {
        if (var1.debugDraw()) {
            var1.log("renderSubTree");
        }

        GL var7 = var1.getGL();
        var1.currentMaterial = null;
        var1.currentPrimitive = null;
        TreeRecordList var8 = this.renderContext.getRecords();

        for (int var9 = var2; var9 < var3; ++var9) {
            TreeRecord var10 = var8.get(var9);
            var1.currentRecord = var10;
            if (var1.currentMaterial == null || var10.material.getHandle() != var1.currentMaterial.getHandle()) {
                var7.glMatrixMode(5890);
                var10.material.bind(var1);
                var7.glMatrixMode(5888);
                this.unbindAdditionalTextures(var1);
                var1.currentMaterial = var10.material;
                var4.updateMaterialProgramAttribs(var1);
                var10.material.updateShaderParameters(var5, var1);
            }

            if (var1.currentPrimitive == null || var10.primitive.getHandle() != var1.currentPrimitive.getHandle()) {
                if (var1.currentPrimitive != null && var1.currentPrimitive.getUseVBO() && !var10.primitive.getUseVBO()) {
                    this.unbindPrimitive();
                }

                var10.primitive.bind(var1);
                var1.currentPrimitive = var10.primitive;
            }

            this.bindAdditionalTextures(var1, var4, var10);
            var7.glPushMatrix();
            var4.updatePrimitiveProgramAttribs(var1);
            if (var4.getUseFixedFunctionLighting()) {
                var1.floatBuffer60Temp0.clear();
                var7.glMaterialfv(1028, 4608, var10.areaInfo.ambientColor.fillFloatBuffer(var1.floatBuffer60Temp0));
                var7.glMaterialfv(1028, 4608, this.one, 0);
                var7.glLightfv(16384, 4608, var10.areaInfo.ambientColor.fillFloatBuffer(var1.floatBuffer60Temp0));
            }

            Color var11 = var10.color;
            if (var6 != null) {
                var7.glColor4f(var6.x, var6.y, var6.z, var6.w);
            } else if (var4.getUseSceneAmbientColor() && var10.areaInfo != null) {
                Color var12 = var10.areaInfo.ambientColor;
                var7.glColor4f(var11.x * var12.x, var11.y * var12.y, var11.z * var12.z, var11.w * var12.w);
            } else {
                var7.glColor4f(var11.x, var11.y, var11.z, var11.w);
            }

            var7.glMultMatrixf(var10.gpuTransform.b(var1.floatBuffer60Temp0));
            if (var10.occlusionQuery != null) {
                var10.occlusionQuery.begin(var7);
                var1.currentPrimitive.draw(var1);
                var10.occlusionQuery.end(var7);
            } else {
                var1.currentPrimitive.draw(var1);
            }

            var7.glPopMatrix();
        }

        if (var1.currentMaterial != null) {
            var1.currentMaterial.unbind(var1);
        }

        this.unbindAdditionalTextures(var1);
    }

    private void unbindAdditionalTextures(DrawContext var1) {
        if (var1.currentMaterial != null && var1.currentMaterial.isUseFramebufferRGB()) {
            for (int var2 = 0; var2 < var1.materialState.getStates().size(); ++var2) {
                if (var1.materialState.getTexInfo(var2).getTexChannel() == 14) {
                    if (framebufferRenderTarget != null) {
                        GL var3 = var1.getGL();
                        var3.glActiveTexture('蓀' + var2);
                        var3.glDisable(framebufferRenderTarget.getTexture().getTarget());
                    }
                    break;
                }
            }
        }

    }

    private void bindAdditionalTextures(DrawContext var1, ShaderPass var2, TreeRecord var3) {
        int var4;
        if (var2.getUseDiffuseCubemap() && var3.areaInfo != null && var3.areaInfo.diffuseCubemap != null) {
            for (var4 = 0; var4 < var1.materialState.getStates().size(); ++var4) {
                if (var1.materialState.getTexInfo(var4).getTexChannel() == 1) {
                    var3.areaInfo.diffuseCubemap.bind('蓀' + var4, var1);
                    var3.areaInfo.diffuseCubemap.addArea(1024.0F);
                    break;
                }
            }
        }

        if (var2.getUseReflectCubemap() && var3.areaInfo != null && var3.areaInfo.reflectiveCubemap != null) {
            for (var4 = 0; var4 < var1.materialState.getStates().size(); ++var4) {
                if (var1.materialState.getTexInfo(var4).getTexChannel() == 0) {
                    var3.areaInfo.reflectiveCubemap.bind('蓀' + var4, var1);
                    var3.areaInfo.reflectiveCubemap.addArea(1024.0F);
                    break;
                }
            }
        }

        if (var1.currentMaterial.isUseFramebufferRGB()) {
            for (var4 = 0; var4 < var1.materialState.getStates().size(); ++var4) {
                if (var1.materialState.getTexInfo(var4).getTexChannel() == 14) {
                    if (framebufferRenderTarget != null) {
                        framebufferRenderTarget.getTexture().bind('蓀' + var4, var1);
                    }
                    break;
                }
            }
        }

    }

    public boolean projectMouseIntoWorld(Camera var1, float var2, float var3, float var4, Vector3dOperations var5) {
        var5.x = (double) (var2 / (float) this.getViewport().width);
        var5.y = (double) (((float) this.getViewport().height - var3) / (float) this.getViewport().height);
        var5.z = (double) var4;
        ajK var6 = new ajK();
        aJF var7 = new aJF();
        aJF var8 = new aJF();
        var7.x = (float) (var5.x * 2.0D - 1.0D);
        var7.y = (float) (var5.y * 2.0D - 1.0D);
        var7.z = (float) (2.0D * var5.z - 1.0D);
        var7.w = 1.0F;
        ajK var9 = var1.getProjection();
        var6.invert(var9);
        var6.transform(var7, var8);
        if (var8.w == 0.0F) {
            return false;
        } else {
            var5.x = (double) (var8.x * var8.w);
            var5.y = (double) (var8.y * var8.w);
            var5.z = (double) (var8.z * var8.w);
            var1.getTransform().a(var5);
            return true;
        }
    }

    public boolean isGLSLSupported() {
        return true;
    }

    public DrawContext getDrawContext() {
        return this.drawContext;
    }

    public Viewport getViewport() {
        return this.viewport;
    }

    public void setViewport(Viewport var1) {
        this.viewport.set(var1);
    }

    /**
     * Записываем текущую позицию и размер холста для рисования
     *
     * @param var1
     * @param var2
     * @param var3
     * @param var4
     */
    public void setViewport(int var1, int var2, int var3, int var4) {
        this.viewport.x = var1;
        this.viewport.y = var2;
        this.viewport.width = var3;
        this.viewport.height = var4;
    }

    public RenderInfo getRenderInfo() {
        return this.renderInfo;
    }

    public void setRenderInfo(RenderInfo var1) {
        this.renderInfo.set(var1);
    }

    public Color getClearColor() {
        return this.renderInfo.getClearColor();
    }

    public Color getFogColor() {
        return this.renderInfo.getFogColor();
    }

    public void setFogColor(Color var1) {
        this.renderInfo.setFogColor(var1);
    }

    public float getFogEnd() {
        return this.renderInfo.getFogEnd();
    }

    public void setFogEnd(float var1) {
        this.renderInfo.setFogEnd(var1);
    }

    public float getFogExp() {
        return this.renderInfo.getFogExp();
    }

    public void setFogExp(float var1) {
        this.renderInfo.setFogExp(var1);
    }

    public float getFogStart() {
        return this.renderInfo.getFogStart();
    }

    public void setFogStart(float var1) {
        this.renderInfo.setFogStart(var1);
    }

    public int getFogType() {
        return this.renderInfo.getFogType();
    }

    public void setFogType(int var1) {
        this.renderInfo.setFogType(var1);
    }

    public boolean isClearColorBuffer() {
        return this.renderInfo.isClearColorBuffer();
    }

    public void setClearColorBuffer(boolean var1) {
        this.renderInfo.setClearColorBuffer(var1);
    }

    public boolean isClearDepthBuffer() {
        return this.renderInfo.isClearDepthBuffer();
    }

    public void setClearDepthBuffer(boolean var1) {
        this.renderInfo.setClearDepthBuffer(var1);
    }

    public boolean isClearStencilBuffer() {
        return this.renderInfo.isClearStencilBuffer();
    }

    public void setClearStencilBuffer(boolean var1) {
        this.renderInfo.setClearStencilBuffer(var1);
    }

    public boolean isWireframeOnly() {
        return this.renderInfo.isWireframeOnly();
    }

    public void setWireframeOnly(boolean var1) {
        this.renderInfo.setWireframeOnly(var1);
    }

    public boolean isNoShading() {
        return this.renderInfo.isNoShading();
    }

    public RenderContext getRenderContext() {
        return this.renderContext;
    }
}
