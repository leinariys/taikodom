package taikodom.render;

import com.sun.opengl.impl.Debug;
import com.sun.opengl.impl.GLContextImpl;
import com.sun.opengl.impl.GLDrawableHelper;

import javax.media.opengl.*;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.beans.Beans;
import java.lang.reflect.Method;
import java.security.AccessController;
import java.security.PrivilegedAction;
//jogl-gluegen-1.1.1-rc8.jar

/**
 * Свой GLCanvas с реализацией GLAutoDrawable
 * подкласс class JGLDesktop
 */
public class MyGLCanvas extends JApplet implements GLAutoDrawable {
    private static final boolean DEBUG = Debug.debug("MyGLCanvas");
    private static boolean disableBackgroundEraseInitialized;
    private static Method disableBackgroundEraseMethod;
    private GLDrawableHelper drawableHelper;
    private GLDrawable drawable;
    private GLContextImpl context;
    private boolean autoSwapBufferMode;
    private boolean sendReshape;
    private GraphicsConfiguration chosen;
    /**
     * Определяет набор возможностей OpenGL.
     */
    private GLCapabilities glCaps;
    private GLCapabilitiesChooser glCapChooser;
    private MyGLCanvas.e initAction;
    private MyGLCanvas.b displayAction;
    private MyGLCanvas.g swapBuffersAction;
    private MyGLCanvas.c displayOnEventDispatchThreadAction;
    private MyGLCanvas.d swapBuffersOnEventDispatchThreadAction;
    private MyGLCanvas.f destroyAction;

    public MyGLCanvas() {
        this((GLCapabilities) null);
    }

    /**
     * @param var1 Определяет набор возможностей OpenGL.
     */
    public MyGLCanvas(GLCapabilities var1) {
        this(var1, (GLCapabilitiesChooser) null, (GLContext) null, (GraphicsDevice) null);
    }

    /**
     * @param var1 Определяет набор возможностей OpenGL.
     * @param var2
     * @param var3
     * @param var4
     */
    public MyGLCanvas(GLCapabilities var1, GLCapabilitiesChooser var2, GLContext var3, GraphicsDevice var4) {
        this.drawableHelper = new GLDrawableHelper();
        this.autoSwapBufferMode = true;
        this.sendReshape = false;
        this.initAction = new MyGLCanvas.e();
        this.displayAction = new MyGLCanvas.b();
        this.swapBuffersAction = new MyGLCanvas.g();
        this.displayOnEventDispatchThreadAction = new MyGLCanvas.c();
        this.swapBuffersOnEventDispatchThreadAction = new MyGLCanvas.d();
        this.destroyAction = new MyGLCanvas.f();
        this.chosen = chooseGraphicsConfiguration(var1, var2, var4);
        if (this.chosen != null) {
            this.glCapChooser = var2;
            this.glCaps = var1;
        }

        if (!Beans.isDesignTime()) {
            this.drawable = GLDrawableFactory.getFactory().getGLDrawable(this, var1, var2);
            this.context = (GLContextImpl) this.drawable.createContext(var3);
            this.context.setSynchronized(true);
        }
    }

    private static GraphicsConfiguration chooseGraphicsConfiguration(GLCapabilities var0, GLCapabilitiesChooser var1, GraphicsDevice var2) {
        if (Beans.isDesignTime()) {
            return null;
        } else {
            AWTGraphicsConfiguration var3 = (AWTGraphicsConfiguration) GLDrawableFactory.getFactory().chooseGraphicsConfiguration(var0, var1, new AWTGraphicsDevice(var2));
            return var3 == null ? null : var3.getGraphicsConfiguration();
        }
    }

    public GraphicsConfiguration getGraphicsConfiguration() {
        GraphicsConfiguration var1 = super.getGraphicsConfiguration();
        if (var1 != null && this.chosen != null && !this.chosen.equals(var1)) {
            if (!this.chosen.getDevice().getIDstring().equals(var1.getDevice().getIDstring())) {
                GraphicsConfiguration var2 = chooseGraphicsConfiguration(this.glCaps, this.glCapChooser, var1.getDevice());
                if (var2 != null) {
                    this.chosen = var2;
                }
            }
            return this.chosen;
        } else {
            return var1 == null ? this.chosen : var1;
        }
    }

    public GLContext createContext(GLContext var1) {
        return this.drawable.createContext(var1);
    }

    public void setRealized(boolean var1) {
    }

    public void display() {
        this.maybeDoSingleThreadedWorkaround(this.displayOnEventDispatchThreadAction, this.displayAction);
    }

    public void paint(Graphics var1) {
        if (Beans.isDesignTime()) {
            var1.setColor(Color.BLACK);
            var1.fillRect(0, 0, this.getWidth(), this.getHeight());
            FontMetrics var2 = var1.getFontMetrics();
            String var3 = this.getName();
            if (var3 == null) {
                var3 = this.getClass().getName();
                int var4 = var3.lastIndexOf(46);
                if (var4 >= 0) {
                    var3 = var3.substring(var4 + 1);
                }
            }

            Rectangle2D var5 = var2.getStringBounds(var3, var1);
            var1.setColor(Color.WHITE);
            var1.drawString(var3, (int) (((double) this.getWidth() - var5.getWidth()) / 2.0D), (int) (((double) this.getHeight() + var5.getHeight()) / 2.0D));
        } else {
            this.display();
        }
    }

    public void paintComponents(Graphics var1) {
    }

    public void paintAll(Graphics var1) {
    }

    public void addNotify() {
        super.addNotify();
        if (!Beans.isDesignTime()) {
            this.disableBackgroundErase();
            this.drawable.setRealized(true);
        }

        if (DEBUG) {
            System.err.println("GLCanvas.addNotify()");
        }

    }

    public void removeNotify() {
        if (Beans.isDesignTime()) {
            super.removeNotify();
        } else {
            try {
                if (Threading.isSingleThreaded() && !Threading.isOpenGLThread()) {
                    if (Thread.holdsLock(this.getTreeLock())) {
                        this.destroyAction.run();
                    } else {
                        Threading.invokeOnOpenGLThread(this.destroyAction);
                    }
                } else {
                    this.destroyAction.run();
                }
            } finally {
                this.drawable.setRealized(false);
                super.removeNotify();
                if (DEBUG) {
                    System.err.println("GLCanvas.removeNotify()");
                }

            }
        }

    }

    public void reshape(int var1, int var2, int var3, int var4) {
        super.reshape(var1, var2, var3, var4);
        this.sendReshape = true;
    }

    public void update(Graphics var1) {
        this.paint(var1);
    }

    /**
     * GLEventListener - Чтобы ваша программа могла использовать графический API JOGL
     */
    public void addGLEventListener(GLEventListener var1) {
        this.drawableHelper.addGLEventListener(var1);
    }

    public void removeGLEventListener(GLEventListener var1) {
        this.drawableHelper.removeGLEventListener(var1);
    }

    public GLContext getContext() {
        return this.context;
    }

    /**
     * GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
     * седержит методы для рисования примитивов (линии треугольники)
     */
    public GL getGL() {
        return Beans.isDesignTime() ? null : this.getContext().getGL();
    }

    public void setGL(GL var1) {
        if (!Beans.isDesignTime()) {
            this.getContext().setGL(var1);
        }

    }

    public boolean getAutoSwapBufferMode() {
        return this.drawableHelper.getAutoSwapBufferMode();
    }

    public void setAutoSwapBufferMode(boolean var1) {
        this.drawableHelper.setAutoSwapBufferMode(var1);
    }

    public void swapBuffers() {
        this.maybeDoSingleThreadedWorkaround(this.swapBuffersOnEventDispatchThreadAction, this.swapBuffersAction);
    }

    public GLCapabilities getChosenGLCapabilities() {
        return this.drawable == null ? null : this.drawable.getChosenGLCapabilities();
    }

    private void maybeDoSingleThreadedWorkaround(Runnable var1, Runnable var2) {
        if (Threading.isSingleThreaded() && !Threading.isOpenGLThread()) {
            Threading.invokeOnOpenGLThread(var1);
        } else {
            this.drawableHelper.invokeGL(this.drawable, this.context, var2, this.initAction);
        }

    }

    private void disableBackgroundErase() {
        if (!disableBackgroundEraseInitialized) {
            try {
                AccessController.doPrivileged(new PrivilegedAction() {
                    public Object run() {
                        try {
                            MyGLCanvas.disableBackgroundEraseMethod = MyGLCanvas.this.getToolkit().getClass().getDeclaredMethod("disableBackgroundErase", Canvas.class);
                            MyGLCanvas.disableBackgroundEraseMethod.setAccessible(true);
                        } catch (Exception var1) {
                            ;
                        }

                        return null;
                    }
                });
            } catch (Exception var2) {
                ;
            }

            disableBackgroundEraseInitialized = true;
        }

        if (disableBackgroundEraseMethod != null) {
            try {
                disableBackgroundEraseMethod.invoke(this.getToolkit(), this);
            } catch (Exception var1) {
                ;
            }
        }

    }

    public GLDrawable getDrawable() {
        return this.drawable;
    }

    class f implements Runnable {
        public void run() {
            GLContext var1 = GLContext.getCurrent();
            if (var1 == MyGLCanvas.this.context) {
                MyGLCanvas.this.context.release();
            }

            MyGLCanvas.this.context.destroy();
        }
    }

    class b implements Runnable {
        public void run() {
            if (MyGLCanvas.this.sendReshape) {
                int var1 = MyGLCanvas.this.getWidth();
                int var2 = MyGLCanvas.this.getHeight();
                MyGLCanvas.this.getGL().glViewport(0, 0, var1, var2);
                MyGLCanvas.this.drawableHelper.reshape(MyGLCanvas.this, 0, 0, var1, var2);
                MyGLCanvas.this.sendReshape = false;
            }

            MyGLCanvas.this.drawableHelper.display(MyGLCanvas.this);
        }
    }

    class c implements Runnable {
        public void run() {
            MyGLCanvas.this.drawableHelper.invokeGL(MyGLCanvas.this.drawable, MyGLCanvas.this.context, MyGLCanvas.this.displayAction, MyGLCanvas.this.initAction);
        }
    }

    class e implements Runnable {
        public void run() {
            MyGLCanvas.this.drawableHelper.init(MyGLCanvas.this);
        }
    }

    class g implements Runnable {
        public void run() {
            MyGLCanvas.this.drawable.swapBuffers();
        }
    }

    class d implements Runnable {
        public void run() {
            MyGLCanvas.this.drawableHelper.invokeGL(MyGLCanvas.this.drawable, MyGLCanvas.this.context, MyGLCanvas.this.swapBuffersAction, MyGLCanvas.this.initAction);
        }
    }
}
