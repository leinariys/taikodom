package taikodom.render;

import taikodom.render.loader.RenderAsset;
import taikodom.render.scene.SoundSource;

/**
 * Реализация класса указанного в имени тегов из файлов с расширением .pro
 */
public class PitchEntry extends RenderAsset {
    final float PITCH_ENTRY_GAIN_CORRECT = 1.5F;
    SoundSource source = new MilesSoundSource();
    SoundBuffer buffer;
    float initialControllerValue;
    float finalControllerValue;
    float initialPitchValue;
    float finalPitchValue;
    float currentPitch;
    float currentGain;
    float parentGain;
    boolean isFirst;
    boolean isLast;
    float gainMultiplier;

    public PitchEntry() {
        this.initialControllerValue = 0.0F;
        this.finalControllerValue = 1.0F;
        this.initialPitchValue = 1.0F;
        this.finalPitchValue = 1.0F;
        this.currentPitch = 0.0F;
        this.currentGain = 0.0F;
        this.isFirst = false;
        this.isLast = false;
        this.parentGain = 1.0F;
        this.buffer = null;
        this.gainMultiplier = 1.0F;
    }

    PitchEntry(PitchEntry var1) {
        if (var1.getSoundBuffer() != null) {
            this.setSoundBuffer(var1.getSoundBuffer().cloneAsset());
        } else {
            this.buffer = null;
        }

        this.initialControllerValue = var1.initialControllerValue;
        this.finalControllerValue = var1.finalControllerValue;
        this.initialPitchValue = var1.initialPitchValue;
        this.finalPitchValue = var1.finalPitchValue;
        this.currentPitch = var1.currentPitch;
        this.currentGain = var1.currentGain;
        this.isFirst = var1.isFirst;
        this.isLast = var1.isLast;
        this.parentGain = var1.parentGain;
        this.gainMultiplier = var1.gainMultiplier;
    }

    public void releaseReferences() {
        if (this.source != null) {
            this.source.releaseReferences();
        }

    }

    public float getInitialControllerValue() {
        return this.initialControllerValue;
    }

    public void setInitialControllerValue(float var1) {
        this.initialControllerValue = var1;
    }

    public float getFinalControllerValue() {
        return this.finalControllerValue;
    }

    public void setFinalControllerValue(float var1) {
        this.finalControllerValue = var1;
    }

    public float getInitialPitchValue() {
        return this.initialPitchValue;
    }

    public void setInitialPitchValue(float var1) {
        this.initialPitchValue = var1;
    }

    public float getFinalPitchValue() {
        return this.finalPitchValue;
    }

    public void setFinalPitchValue(float var1) {
        this.finalPitchValue = var1;
    }

    public boolean getFirst() {
        return this.isFirst;
    }

    public void setFirst(boolean var1) {
        this.isFirst = var1;
    }

    public boolean getLast() {
        return this.isLast;
    }

    public void setLast(boolean var1) {
        this.isLast = var1;
    }

    public void setParentGain(float var1) {
        this.parentGain = var1;
    }

    public SoundSource getSoundSource() {
        return this.source;
    }

    public void setSoundSource(SoundSource var1) {
        this.source = var1;
    }

    public SoundBuffer getSoundBuffer() {
        return this.buffer;
    }

    public void setSoundBuffer(SoundBuffer var1) {
        this.buffer = var1;
        this.source.setBuffer(this.buffer);
    }

    public boolean update(float var1, StepContext var2) {
        float var3 = this.finalControllerValue - this.initialControllerValue;
        if (var3 == 0.0F) {
            return false;
        } else {
            float var4 = this.currentGain;
            float var5 = this.currentPitch;
            float var6 = (var1 - this.initialControllerValue) / var3;
            this.currentPitch = var6 * (this.finalPitchValue - this.initialPitchValue) + this.initialPitchValue;
            var6 = var6 * 2.0F - 1.0F;
            var6 = var6 > 1.0F ? 1.0F : var6;
            var6 = var6 < -1.0F ? -1.0F : var6;
            this.currentGain = (float) Math.cos((double) var6 * 1.5707963267948966D);
            this.currentGain *= 1.5F * this.gainMultiplier;
            this.currentGain = this.currentGain > 1.0F ? 1.0F : this.currentGain;
            this.currentGain = this.currentGain < 0.0F ? 0.0F : this.currentGain;
            if (this.isFirst && var6 < 0.0F) {
                this.currentGain = 1.0F;
            }

            if (this.isLast && var6 > 0.0F) {
                this.currentGain = 1.0F;
            }

            this.currentGain *= this.parentGain;
            if (this.source != null) {
                if (this.currentGain <= 0.0F) {
                    this.source.stop();
                } else {
                    if (!this.source.isPlaying()) {
                        this.source.play(0);
                    }

                    if (var4 != this.currentGain) {
                        this.source.setGain(this.currentGain);
                    }

                    if (var5 != this.currentPitch) {
                        this.source.setPitch(this.currentPitch);
                    }
                }

                this.source.step(var2);
            }

            return true;
        }
    }

    public float getGainMultiplier() {
        return this.gainMultiplier;
    }

    public void setGainMultiplier(float var1) {
        this.gainMultiplier = var1;
    }

    public PitchEntry cloneAsset() {
        return new PitchEntry(this);
    }
}
