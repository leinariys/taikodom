package taikodom.render;

import all.Vector3dOperations;
import all.ajD;
import all.ajK;
import com.hoplon.geometry.Vec3f;
import taikodom.render.camera.Camera;

public class StepContext {
    public final Vector3dOperations vec3dTemp0 = new Vector3dOperations();
    public final Vector3dOperations vec3dTemp1 = new Vector3dOperations();
    public final Vector3dOperations vec3dTemp2 = new Vector3dOperations();
    public final Vec3f vec3ftemp0 = new Vec3f();
    public final Vec3f vec3ftemp1 = new Vec3f();
    public final Vec3f vec3ftemp2 = new Vec3f();
    public final ajK matrix4fTemp0 = new ajK();
    public final ajK matrix4fTemp1 = new ajK();
    public final ajK matrix4fTemp2 = new ajK();
    public final ajD matrix3fTemp0 = new ajD();
    public final ajD matrix3fTemp1 = new ajD();
    public final ajD matrix3fTemp2 = new ajD();
    private long numObjectsStepped;
    private long numParticleSystemStepped;
    private float deltaTimeInSeconds;
    private Camera camera;

    public Camera getCamera() {
        return this.camera;
    }

    public void setCamera(Camera var1) {
        this.camera = var1;
    }

    public float getDeltaTime() {
        return this.deltaTimeInSeconds;
    }

    public void setDeltaTime(float var1) {
        this.deltaTimeInSeconds = var1;
    }

    public void incObjectSteps(long var1) {
        this.numObjectsStepped += var1;
    }

    public void incParticleSystemStepped() {
        ++this.numParticleSystemStepped;
    }

    public long getNumObjectsStepped() {
        return this.numObjectsStepped;
    }

    public void reset() {
        this.numObjectsStepped = 0L;
        this.deltaTimeInSeconds = 0.0F;
        this.numParticleSystemStepped = 0L;
    }

    public boolean getEditMode() {
        return false;
    }

    public long getNumParticleSystemStepped() {
        return this.numParticleSystemStepped;
    }
}
