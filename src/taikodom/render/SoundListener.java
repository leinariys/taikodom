package taikodom.render;

import taikodom.render.scene.SceneObject;

public abstract class SoundListener extends SceneObject {
    protected float gain = 1.0F;

    float getGain() {
        return this.gain;
    }

    void setGain(float var1) {
        this.gain = var1;
    }

    public boolean isNeedToStep() {
        return true;
    }
}
