package taikodom.render;

//import all.Vb;
//import all.aDx;
//import all.aNZ;
//import all.afz;
//import all.ic;

import org.lwjgl.BufferUtils;
import taikodom.render.loader.RenderAsset;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

public class MilesSoundStream extends RenderAsset {
    public static final long SMP_PLAYING = 4L;
    public static final long SMP_DONE = 2L;
    public static final long SMP_STOPPED = 8L;
    private static int openedStreams = 0;
    protected FloatBuffer tempFloat = BufferUtils.createFloatBuffer(1);
    protected IntBuffer tempInt = BufferUtils.createIntBuffer(1);
    protected String fileName = "";
    float fadeInValue;
    //  private ic stream;
    private boolean isFadingOut = false;
    private boolean isFadingIn;

    public static int getOpenedStreams() {
        return openedStreams;
    }

    public RenderAsset cloneAsset() {
        return this;
    }

    public boolean play() {
   /*   if (this.stream == null) {
         if (!this.loadStream()) {
            return false;
         }

         ++openedStreams;
         ++SoundSource.numSoundSources;
         afz.CreateJComponent((ic)this.stream, 0);
         afz.CreateJComponent(this.stream);
      } else {
         afz.setGreen((ic)this.stream, 0);
      }
*/
        return true;
    }

    public boolean play(int var1) {
    /*  if (this.stream == null) {
         if (!this.loadStream()) {
            return false;
         }

         ++openedStreams;
         ++SoundSource.numSoundSources;
         afz.CreateJComponent(this.stream, var1);
         afz.CreateJComponent(this.stream);
      } else {
         afz.setGreen((ic)this.stream, 0);
      }
*/
        return true;
    }

    /*
       private boolean loadStream() {
          try {
             this.stream = afz.all((aNZ)SoundMiles.getInstance().getDig(), (String)this.fileName, 0);
          } catch (Exception var2) {
             var2.printStackTrace();
          }

          return this.stream != null;
       }
    */
    public void stop() {
        this.releaseReferences();
    }

    /*
       public void pause() {
          if (this.stream != null) {
             afz.setGreen((ic)this.stream, 1);
          }

       }
    *//*
   public void releaseReferences() {
      if (this.stream != null) {
         afz.setGreen(this.stream);
         --openedStreams;
         --SoundSource.numSoundSources;
         this.stream = null;
      }

   }
*/
    public void setLoopCount(int var1) {
   /*   if (this.stream != null) {
         afz.CreateJComponent(this.stream, var1);
      }
*/
    }

    /*
       public int getStatus() {
          return this.stream != null ? afz.endPage(this.stream) : 0;
       }
    */
    public boolean isPlaying() {
    /*  if (this.stream != null) {
         return afz.endPage(this.stream) == 4;
      } else {
         return false;
      }*/
        return false;
    }

    /*
       public Vb getMilesSample() {
          return this.stream != null ? afz.CreateJComponent(this.stream) : null;
       }
    */
    public void setGain(float var1) {
    /*  if (this.stream != null) {
         Vb var2 = afz.CreateJComponent(this.stream);
         if (var2 != null) {
            afz.setGreen(var2, var1, var1);
         }
      }
*/
    }

    public boolean fadeOut(float var1) {
       /*
      if (this.stream == null) {
         return false;
      } else {
         Vb var2 = this.getMilesSample();
         if (var2 == null) {
            return false;
         } else if (afz.AIL_find_filter("Volume Ramp Filter", this.tempInt) != 0) {
            this.isFadingOut = true;
            this.isFadingIn = false;
            int var3 = this.tempInt.get(0);
            afz.all(var2, aDx.hBQ, (long)var3);
            float var4 = 0.0F;
            afz.all(var2, aDx.hBQ, (long)var3);
            afz.all((Vb)var2, (Buffer)this.tempFloat, (Buffer)null);
            float var5 = 1.0F;
            afz.all(var2, aDx.hBQ, var5);
            afz.setGreen(var2, aDx.hBQ, var1);
            var5 = 0.0F;
            afz.all(var2, aDx.hBQ, var5);
            return true;
         } else {
            return false;
         }
      }
      */
        return true;
    }

    public boolean fadeIn(float var1) {
   /*   if (this.stream == null) {
         return false;
      } else {
         Vb var2 = this.getMilesSample();
         if (var2 == null) {
            return false;
         } else if (afz.AIL_find_filter("Volume Ramp Filter", this.tempInt) != 0) {
            this.isFadingIn = true;
            this.isFadingOut = false;
            int var3 = this.tempInt.get(0);
            afz.all(var2, aDx.hBQ, (long)var3);
            float var4 = 0.0F;
            afz.all(var2, aDx.hBQ, (long)var3);
            float var5 = 0.0F;
            afz.all(var2, aDx.hBQ, var5);
            afz.setGreen(var2, aDx.hBQ, var1);
            afz.all((Vb)var2, (Buffer)this.tempFloat, (Buffer)null);
            var5 = 1.0F;
            this.fadeInValue = var5;
            afz.all(var2, aDx.hBQ, var5);
            return true;
         } else {
            return false;
         }
      }
*/
        return true;
    }

    public int step(StepContext var1) {
  /*    Vb var2 = this.getMilesSample();
      if (var2 != null) {
         int var3 = this.getStatus();
         if ((long)var3 == 2L) {
            this.releaseReferences();
            return 1;
         } else {
            if (this.isFadingOut) {
               this.tempFloat.put(0, 1.0F);
               afz.all(var2, (aDx)aDx.hBQ, (String)"Ramp At", (Buffer)this.tempFloat, (Buffer)null, (Buffer)null);
               if (this.tempFloat.get(0) <= 0.005F) {
                  this.releaseReferences();
                  this.isFadingOut = false;
                  return 1;
               }
            } else if (this.isFadingIn) {
               this.tempFloat.put(0, 1.0F);
               afz.all(var2, (aDx)aDx.hBQ, (String)"Ramp At", (Buffer)this.tempFloat, (Buffer)null, (Buffer)null);
               if (this.tempFloat.get(0) >= this.fadeInValue - 0.005F) {
                  this.isFadingIn = false;
                  return 0;
               }
            }

            return 0;
         }
      } else {
         return 1;
      }
*/
        return 1;
    }

    public int getSamplePositionMs() {
       /*
      if (this.stream == null) {

      } else {
         afz.all((ic)this.stream, (Buffer)null, (Buffer)this.tempInt);
         return this.tempInt.get(0);
      }
      */
        return 0;
    }

    public void setSamplePositionMs(int var1) {
       /*
      if (this.stream != null) {
         afz.endPage(this.stream, var1);
      }
*/
    }

    public String getFileName() {
        return this.fileName;
    }

    public void setFileName(String var1) {
        this.fileName = var1;
    }
}
