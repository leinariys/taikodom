package taikodom.render;

import taikodom.render.primitives.Billboard;
import taikodom.render.shaders.RenderStates;

import javax.media.opengl.GL;

/**
 *
 */
public class BinkVideo implements Video {
    BinkTexture bink;
    Billboard billboard = new Billboard();
    RenderStates states = new RenderStates();
    float texAspectX;
    float texAspectY;

    public BinkVideo(BinkTexture var1) {
        this.bink = var1;
        this.texAspectX = (float) var1.getVideoSizeX() / (float) var1.getSizeX();
        this.texAspectY = (float) var1.getVideoSizeY() / (float) var1.getSizeY();
    }

    public void render(DrawContext var1) {/**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
     * седержит методы для рисования примитивов (линии треугольники)*/
        GL var2 = var1.getGL();
        this.states.blindlyUpdateGLStates(var1);
        var2.glScalef((float) var1.getScreenWidth(), (float) var1.getScreenHeight(), 1.0F);
        this.bink.bind(var1);
        var2.glBegin(GL.GL_QUADS);//Этот метод запускает процесс рисования. Рассматривает каждую группу из четырех вершин как независимый четырехугольник.
        var2.glTexCoord2f(0.0F, this.texAspectY);
        var2.glVertex2f(0.0F, 1.0F);
        var2.glTexCoord2f(this.texAspectX, this.texAspectY);
        var2.glVertex2f(1.0F, 1.0F);
        var2.glTexCoord2f(this.texAspectX, 0.0F);
        var2.glVertex2f(1.0F, 0.0F);
        var2.glTexCoord2f(0.0F, 0.0F);
        var2.glVertex2f(0.0F, 0.0F);
        var2.glEnd();
        if (this.bink.isEnded()) {
            this.stop();
        }

    }

    public void update(DrawContext var1) {
    }

    public void stop() {
    }

    public void play() {
    }

    public boolean isEnded() {
        return this.bink.isEnded();
    }
}
