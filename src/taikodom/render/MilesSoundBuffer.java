package taikodom.render;

import all.LogPrinter;
import taikodom.render.loader.provider.SoundBufferLoader;

import java.nio.Buffer;

//import all.aHS;
//import all.afz;

public class MilesSoundBuffer extends SoundBuffer {
    static LogPrinter logger = LogPrinter.K(MilesSoundBuffer.class);
    static int totalMilesBufferUsage = 0;
    private String extension = "";
    ///  private aHS soundInfo = new aHS();
    private String fileName = "";

    public static int getTotalBytes() {
        return totalMilesBufferUsage;
    }

    public boolean setData(Buffer var1, int var2) {
        if (var1 == null) {
            return false;
        } else {
            synchronized (this) {
                this.length = var2;
                this.data = var1;
                totalMilesBufferUsage += var2;
                // aHS var4 = this.getSoundInfo();
                // this.fillInternalBuffer(var4);
                return true;
            }
        }
    }

    public int getLength() {
        return this.length;
    }

    public String getExtension() {
        return this.extension;
    }

    public void setExtension(String var1) {
        this.extension = var1;
    }

    public Buffer getData() {
        if (this.data == null) {
            SoundBufferLoader.addBufferRequest(this);
        }

        return this.data;
    }

    /*
       public aHS getSoundInfo() {
          aHS var1 = this.soundInfo;
          this.fillInternalBuffer(var1);
          return this.soundInfo;
       }
    */
/*
   private void fillInternalBuffer(aHS var1) {
      if (var1.ddK() == null && this.extension.equals(".wav")) {
         if (afz.all(this.data, var1) == 0) {
            logger.warn("Unable to_q loadFileCSS WAV info from file " + this.fileName);
         } else {
            this.sampleRate = (int)var1.ddM();
            this.bitDepth = var1.DH();
            this.channels = var1.getChannels();
            this.dataLenght = var1.ddL();
            this.timeLenght = 8.0F * (float)this.dataLenght / (float)(this.bitDepth * this.channels * this.sampleRate);
         }
      }

   }
*/
    public int getSampleRate() {
        return this.sampleRate;
    }

    public int getBitDepth() {
        return this.bitDepth;
    }

    public int getBufferId() {
        return 0;
    }

    public int getChannels() {
        return this.channels;
    }

    public long getSampleCount() {
        return 1L;
    }

    public float getTimeLength() {
        return this.timeLenght;
    }

    public boolean isValid() {
        return true;
    }

    public MilesSoundBuffer cloneAsset() {
        return this;
    }

    public String getFileName() {
        return this.fileName;
    }

    public void setFileName(String var1) {
        this.fileName = var1;
        this.extension = var1.substring(var1.lastIndexOf("."));
    }

    public void forceSyncLoading() {
        if (this.data == null) {
            SoundBufferLoader.syncLoadBuffer(this);
        }
    }
}
