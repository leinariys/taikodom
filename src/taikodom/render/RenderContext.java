package taikodom.render;

import all.*;
import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import taikodom.render.camera.Camera;
import taikodom.render.impostor.ImpostorManager;
import taikodom.render.primitives.*;
import taikodom.render.scene.*;
import taikodom.render.shaders.Shader;
import taikodom.render.textures.Material;
import taikodom.render.textures.RenderTarget;
import taikodom.render.textures.Texture;

import javax.vecmath.Matrix3f;

public class RenderContext {
    private static LogPrinter logger = LogPrinter.K(RenderContext.class);
    public final Vec3f vec3fTemp0 = new Vec3f();
    public final Vec3f vec3fTemp1 = new Vec3f();
    public final Vec3f vec3fTemp2 = new Vec3f();
    public final Vec3f vec3fTemp3 = new Vec3f();
    public final Vector3dOperations vec3dTemp0 = new Vector3dOperations();
    public final Vector3dOperations vec3dTemp1 = new Vector3dOperations();
    public final Vector3dOperations vec3dTemp2 = new Vector3dOperations();
    public final aQKa vec2fTemp0 = new aQKa();
    public final aQKa vec2fTemp1 = new aQKa();
    public final aQKa vec2fTemp2 = new aQKa();
    public final bc_q transformTemp0 = new bc_q();
    public final ajK cameraTransform = new ajK();
    public final ajK cameraAffineTransform = new ajK();
    protected final SceneViewQuality sceneViewQuality = new SceneViewQuality();
    private final Vector3dOperations gpuOffset = new Vector3dOperations();
    private final vv frustum = new Ch();
    public double absoluteTime;
    public boolean render;
    protected Color ambientLight;
    protected int clearBuffers;
    protected Color clearColor;
    protected Camera currentCamera;
    protected Scene currentScene;
    protected float deltaTimeInSeconds;
    protected boolean forceSimpleShader;
    protected LightRecordList lights = new LightRecordList();
    protected SceneObject parentObject;
    protected TreeRecordList records = new TreeRecordList();
    protected RenderTarget renderTarget;
    private ImpostorManager impostorManager;
    private DrawContext dc;
    private long currentFrame;
    private int particleSystemsRendered;
    private int billboardsRendered;
    private int meshesRendered;
    private int trailsRendered;
    private boolean cullOutSmallObjects = true;
    private boolean allowImpostors = false;
    private int numImpostorsUpdated;

    public Scene getCurrentScene() {
        return this.currentScene;
    }

    public void setCurrentScene(Scene var1) {
        this.currentScene = var1;
    }

    public void addLight(Light var1) {
        LightRecordList.LightRecord var2 = this.lights.add();
        var2.light = var1;
        var2.cameraSpaceBoundingSphere.bny().sub(var1.getPosition(), this.gpuOffset);
        var2.cameraSpaceBoundingSphere.setRadius(var1.getRadius());
    }

    private TreeRecord addPrimitive(Shader var1, Material var2, Primitive var3, bc_q var4, Color var5, aLH var6, OcclusionQuery var7, int var8, RenderAreaInfo var9, double var10) {
        TreeRecord var12 = this.records.add();
        var12.shader = var1.getBestShader(this.dc);
        var12.material = var2;
        var12.primitive = var3;
        var12.transform.b(var4);
        var12.gpuTransform.a((Matrix3f) var4.mX);
        var12.gpuTransform.m03 = (float) (var4.position.x - this.gpuOffset.x);
        var12.gpuTransform.m13 = (float) (var4.position.y - this.gpuOffset.y);
        var12.gpuTransform.m23 = (float) (var4.position.z - this.gpuOffset.z);
        var12.squaredDistanceToCamera = var10;
        if (var6 != null) {
            var12.cameraSpaceBoundingBox.din().sub(var6.din(), this.gpuOffset);
            var12.cameraSpaceBoundingBox.dim().sub(var6.dim(), this.gpuOffset);
        } else {
            var12.cameraSpaceBoundingBox.reset();
        }

        var12.color.set(var5);
        var12.occlusionQuery = var7;
        var12.entryPriority = var8;
        var12.areaInfo = var9;
        return var12;
    }

    public TreeRecord addPrimitive(Material var1, Primitive var2, bc_q var3, Color var4, OcclusionQuery var5, int var6, RenderObject var7, aLH var8, double var9) {
        if (var2 == null) {
            logger.error("Trying to_q add all null primitive to_q TreeRecord.");
            return null;
        } else {
            if (this.currentFrame > var7.getLastFrameRendered()) {
                var7.setLastFrameRendered(this.currentFrame);
            }

            if (var1 == null) {
                logger.error("Null material for primitive " + var2);
                return null;
            } else if (var1.getShader() == null) {
                logger.error("Null shader for material " + var1.getName());
                return null;
            } else {
                RenderAreaInfo var11 = var7.getRenderAreaInfo();
                if (this.currentScene != null) {
                    var11 = this.currentScene.fillRenderAreaInfo(var3.position.x, var3.position.y, var3.position.z, var7.getRenderAreaInfo());
                }

                return this.addPrimitive(var1.getShader(), var1, var2, var3, var4, var8, var5, var6, var11, var9);
            }
        }
    }

    public void clearRecords() {
        this.records.clear();
        this.particleSystemsRendered = this.billboardsRendered = this.meshesRendered = this.trailsRendered = this.numImpostorsUpdated = 0;
    }

    public void clearRecordsAndLights() {
        this.clearRecords();
        this.lights.clear();
    }

    public void createRenderTargetAndTexture(int var1, int var2, String var3) {
    }

    public Camera getCamera() {
        return this.currentCamera;
    }

    public void setCamera(Camera var1) {
        this.currentCamera = var1;
    }

    public TreeRecordList getRecords() {
        return this.records;
    }

    public Texture getRenderTargetTexture() {
        return null;
    }

    public void setCamera(ajK var1, float var2, float var3, float var4, float var5) {
    }

    public void setCameraOrientation(ajK var1) {
    }

    public void setClearBuffers(boolean var1, boolean var2, boolean var3) {
    }

    public void setFogParameters(int var1, float var2, float var3, float var4, Color var5) {
    }

    public void setupGLFog() {
    }

    public void setupGLLights() {
    }

    public void sortLights() {
    }

    public void sortSubTree() {
    }

    public LightRecordList getLights() {
        return this.lights;
    }

    public float getDeltaTime() {
        return this.deltaTimeInSeconds;
    }

    public void setDeltaTime(float var1) {
        this.deltaTimeInSeconds = var1;
    }

    public Vector3dOperations getGpuOffset() {
        return this.gpuOffset;
    }

    public void setGpuOffset(Vector3dOperations var1) {
        this.gpuOffset.aA(var1);
    }

    public DrawContext getDc() {
        return this.dc;
    }

    public void setDc(DrawContext var1) {
        this.dc = var1;
    }

    public vv getFrustum() {
        return this.frustum;
    }

    public void incParticleSystemsRendered() {
        ++this.particleSystemsRendered;
    }

    public void incBillboardsRendered() {
        ++this.billboardsRendered;
    }

    public void incMeshesRendered() {
        ++this.meshesRendered;
    }

    public void incTrailsRendered() {
        ++this.trailsRendered;
    }

    public int getParticleSystemsRendered() {
        return this.particleSystemsRendered;
    }

    public int getBillboardsRendered() {
        return this.billboardsRendered;
    }

    public int getMeshesRendered() {
        return this.meshesRendered;
    }

    public int getTrailsRendered() {
        return this.trailsRendered;
    }

    public void setMainLight(Light var1) {
        this.dc.setMainLight(var1);
    }

    public int getNumLights() {
        return this.lights.size();
    }

    public int getNumVisibleObjects() {
        return this.records.size();
    }

    public float getSizeOnScreen(Vector3dOperations var1, double var2) {
        float var4 = (float) var1.ax(this.currentCamera.getPosition());
        if (var4 == 0.0F) {
            return (float) this.dc.getScreenWidth();
        } else {
            float var5 = var4 * 2.0F * this.currentCamera.getTanHalfFovY();
            float var6 = (float) this.dc.getScreenWidth() / var5;
            float var7 = (float) (var2 * (double) var6);
            return var7;
        }
    }

    public double getDistanceForSize(double var1, float var3) {
        double var4 = (double) var3 / var1;
        double var6 = (double) this.dc.getScreenWidth() / var4;
        double var8 = var6 / (double) (2.0F * this.currentCamera.getTanHalfFovY());
        return var8;
    }

    public boolean isCullOutSmallObjects() {
        return this.cullOutSmallObjects;
    }

    public void setCullOutSmallObjects(boolean var1) {
        this.cullOutSmallObjects = var1;
    }

    public SceneViewQuality getSceneViewQuality() {
        return this.sceneViewQuality;
    }

    public void setSceneViewQuality(SceneViewQuality var1) {
        this.sceneViewQuality.set(var1);
    }

    public boolean isAllowImpostors() {
        return this.allowImpostors && this.impostorManager != null;
    }

    public void setAllowImpostors(boolean var1) {
        this.allowImpostors = var1;
    }

    public void addImpostor(SceneObject var1) {
        this.impostorManager.addImpostor(var1);
    }

    public ImpostorManager getImpostorManager() {
        return this.impostorManager;
    }

    public void setImpostorManager(ImpostorManager var1) {
        this.impostorManager = var1;
    }

    public void incNumImpostorsUpdated() {
        ++this.numImpostorsUpdated;
    }

    public int getNumImpostorsUpdated() {
        return this.numImpostorsUpdated;
    }
}
