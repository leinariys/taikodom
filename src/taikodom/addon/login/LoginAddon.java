package taikodom.addon.login;

import all.*;
import taikodom.addon.messagedialog.MessageDialogAddon;
import taikodom.infra.script.I18NString;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;

//import all.aDk;

/**
 * Панель авторизации/регистрации начальный экран
 */
@awK("taikodom.addon.login")
public class LoginAddon implements fo {
    private String pathReleaseNotesXML = "release-notes.xml";
    private String pathNotActivatedXML = "notActivated.xml";
    private String pathLoginXML = "login.xml";
    private String pathEulaXML = "eula.xml";

    private GK kj;
    /**
     * Панель авторизации главный экран
     */
    private PanelCustomWrapper loginUI;
    private WindowCustomWrapper qy;

    private WindowCustomWrapper qA;
    private akH qB;
    private JTextField loginField;
    private JPasswordField passwordField;
    private JCheckBox userSaveCheckBox;
    /**
     * Значение из настроек если есть [user] username
     */
    private String autoCompleteUsername;
    private PanelCustomWrapper newsPanel;
    private WindowCustomWrapper eulaWindow;
    private rT qI;
    private rT qJ;
    private rT qK;
    private all.e qL;
    private TextfieldCustomWrapper qM;
    private JButton qN;
    private rT qO;
    private WindowCustomWrapper qP;
    private ahM qQ;
    private ahM qR;
    private rT qS;

    /**
     * Передача логирования и локолизации
     *
     * @param var1 class GK Обёртка класса аддона логирование и локализация
     */
    public void InitAddon(vW var1) {
        //if (var1.dL() == null)
        {
            this.kj = (GK) var1;
            this.qB = new akH(this.kj);
            this.qI = new rT() {
                @Override
                public void b(Object var1) {
                    a((aUU) var1);
                }

                public void a(aUU var1) {
                    switch (var1.dAD()) {
                        case 1:
                            LoginAddon.this.show();
                            break;
                        case 2:
                            LoginAddon.this.close();
                        case 3:
                        case 4:
                        default:
                            break;
                        case 5:
                            LoginAddon.this.destroy();
                    }

                }
            };

            this.qK = new rT() {
                @Override
                public void b(Object var1) {
                    a((aAp_w) var1);
                }

                public void a(aAp_w var1) {
                    LoginAddon.this.show();
                    LoginAddon.this.hw();
                }
            };

            this.qJ = new rT() {
                @Override
                public void b(Object var1) {
                    a((RJ) var1);
                }

                public void a(RJ var1) {
                    ((MessageDialogAddon) LoginAddon.this.kj.U(MessageDialogAddon.class)).CreateMessageDialog(LoginAddon.this.kj.translate("You were disconnected from the server"), new aEP() {
                        public void a(int var1, String var2, boolean var3) {
                            System.exit(-1);
                        }

                        public Nn.atitle yD() {
                            return Nn.atitle.info;
                        }
                    }, LoginAddon.this.kj.translate("Ok"));
                }
            };

            this.qO = new rT() {
                @Override
                public void b(Object var1) {
                    a((iV_w) var1);
                }

                public void a(iV_w var1) {
                    LoginAddon.this.a(var1.BJ(), var1.BJ() == null);
                }
            };

            this.qS = new rT() {
                @Override
                public void b(Object var1) {
                    a((iJ) var1);
                }

                public void a(iJ var1) {
                    LoginAddon.this.b(var1.BJ(), var1.BJ() == null);
                }
            };

            this.kj.aVU().a((Class) iV_w.class, (aVH) this.qO);
            this.kj.aVU().a((Class) iJ.class, (aVH) this.qS);
            this.kj.aVU().a((Class) aUU.class, (aVH) this.qI);
            this.kj.aVU().a((Class) RJ.class, (aVH) this.qJ);
            this.kj.aVU().a((Class) aAp_w.class, (aVH) this.qK);
            this.init();
        }
    }

    /**
     * Показать UI
     */
    private void show() {
        if (this.loginUI == null) {
            this.init();
        }
        this.loginUI.setVisible(true);
        if (this.autoCompleteUsername.equals("")) {
            this.loginField.requestFocus();
        } else {
            this.passwordField.requestFocus();
        }
    }

    /**
     * Скрыть UI
     */
    private void hide() {
        this.loginUI.setVisible(false);
    }

    private void init() {
        this.loginUI = (PanelCustomWrapper) this.kj.bHv().CreatJComponentFromXML(pathLoginXML);
        this.autoCompleteUsername = this.qB.getUserUsername();
        //System.out.println("DEBUG2 -" + this.loginUI.countComponents() );
        this.ht(); //Creat main panel
        this.hu(); //Creat close app
        this.hv(); //Creat panel login
        this.hz(); //Creat mes not connect to server
        this.hA(); //Creat Notes
        this.loginUI.setLocation(0, 0);
        final Dimension var1 = this.kj.bHv().getScreenSize();
        this.loginUI.setSize(var1);
        final String var2 = this.kj.getLineConfig("imageset", "login");
        if (var2 != null && !var2.isEmpty()) {
            final String var3 = this.kj.getLineConfig("imagelist", "login");
            if (var3 != null && !var3.isEmpty()) {
                Thread var4 = new Thread("Login getFile Reescaler") {
                    public void run() {
                        String[] imagelistValue = var3.split(" ");// imagelist
                        int var2x = (int) Math.floor(Math.random() * (double) imagelistValue.length);
                        String var3x = var2 + imagelistValue[var2x];
                        PictureCustomWrapper var4 = (PictureCustomWrapper) LoginAddon.this.loginUI.findJComponent("backgroundContainer");
                        var4.setImage(var3x);
                        PictureCustomWrapper.setSizeImage(var4, var1);
                    }
                };
                var4.run();
            }
        }

        ((JComponent) this.loginUI).validate();
        this.hs();
    }

    private void hs() {
        if (this.qB != null && !this.qB.getLocation().equalsIgnoreCase("pt")) {
            // ((MessageDialogAddon)this.kj.U(MessageDialogAddon.class)).a(Nn.atitle.info, this.kj.translate("english-version-not-ready"), this.kj.translate("OK"));
        }

        if (this.qB.getUserLastRunVersion().length() == 0) {
            this.hy();
        }
    }


    void destroy() {
        if (this.loginUI != null) {
            this.loginUI.setVisible(false);
            this.loginUI = null;
        }
    }

    private void close() {
        //aDk var1 = this.kj.ald().bhe();
        this.qB.jf(this.userSaveCheckBox.isSelected() ? /*var1.getTegName()*/"" : "");
        this.hide();
    }

    public void stop() {
        if (this.loginUI != null) {
            this.kj.aVU().a(this.qI);
        }

        this.destroy();
    }

    private void ht() {

        //System.out.println("DEBUG6 -" + ((il)this.loginUI).findJLabel("versionLabel") );
        //aCo return null;
        //TODO не находит графику
        this.loginUI.findJLabel("versionLabel").setText("1.0.4933.55");
        this.loginUI.findJButton("terms").addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent var1) {
                LoginAddon.this.b((ahM) null, true);
            }
        });
        this.loginUI.findJButton("eula").addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent var1) {
                LoginAddon.this.a((ahM) null, true);
            }
        });
        this.loginUI.findJButton("notes").addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent var1) {
                LoginAddon.this.hB();
            }
        });
        this.loginUI.findJButton("register").addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent var1) {
                LoginAddon.this.hy();
            }
        });
        this.loginUI.findJButton("forgot").addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent var1) {
                LoginAddon.this.qB.cgD();
            }
        });
        this.loginUI.findJButton("support").addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent var1) {
                LoginAddon.this.qB.cgE();
            }
        });
        this.loginUI.findJButton("website").addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent var1) {
                LoginAddon.this.qB.cgF();
            }
        });
        this.loginUI.findJButton("settings").addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent var1) {
                LoginAddon.this.qB.cgG();
            }
        });
        this.loginUI.findJButton("credits").addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent var1) {
                LoginAddon.this.qB.cgH();
            }
        });
    }

    private void hu() {
        this.loginUI.findJButton("closeButton").addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent var1) {
                LoginAddon.this.qB.setExit();
            }
        });
        this.qN = this.loginUI.findJButton("enterButton");
        this.qN.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent var1) {
                LoginAddon.this.startLogin();
            }
        });
    }

    private void hv() {
        this.loginField = this.loginUI.findJTextField("login");
        this.loginField.setText(this.autoCompleteUsername);
        this.passwordField = (JPasswordField) this.loginUI.findJComponent("password");
        this.loginField.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent var1) {
                if (var1.getKeyChar() == '\n') {
                    LoginAddon.this.startLogin();
                }

            }
        });
        this.passwordField.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent var1) {
                if (var1.getKeyChar() == '\n') {
                    LoginAddon.this.startLogin();
                }

            }
        });
        this.userSaveCheckBox = (JCheckBox) this.loginUI.findJComponent("saveUserCheckBox");
        this.userSaveCheckBox.setSelected(!this.autoCompleteUsername.equals(""));
    }

    protected void hw() {
        if (this.qA != null) {
            this.qA.setVisible(true);
        } else {
            this.qA = (WindowCustomWrapper) this.kj.bHv().CreatJComponentFromXML(pathNotActivatedXML);
            this.qA.pack();

            String var1;
            try {
                var1 = this.qB.cgK();
            } catch (Exception var5) {
                var1 = "";
            }

            HtmlCustomWrapper var2 = (HtmlCustomWrapper) this.qA.findJComponent("content");
            var2.setText(var1);
            this.qA.findJTextField("username").setText(this.loginField.getText());
            this.qM = this.qA.findJTextField("email");

            String var3;
            try {
                var3 = this.qB.ar(this.loginField.getText(), String.valueOf(this.passwordField.getPassword()));
            } catch (Exception var4) {
                var3 = "";
            }

            this.qM.setText(var3);
            this.qA.findJButton("sendEmailButton").addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent var1) {
                    LoginAddon.this.hx();
                }
            });
            this.qA.findJButton("closeButton").addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent var1) {
                    LoginAddon.this.qA.setVisible(false);
                }
            });
            this.qA.center();
            this.qA.setVisible(true);
        }
    }

    protected void hx() {
        boolean var1 = this.qB.m(this.loginField.getText(), String.valueOf(this.passwordField.getPassword()), this.qM.getText());
        if (var1) {
            this.qA.setVisible(false);
        }

    }

    private void hy() {
        if (this.qL == null) {
            this.qL = new all.e(this.kj, this.qB.getLocation(), this.qB.urlCreateUser);
        }

        this.qL.xK();
    }

    private void a(ahM var1, boolean var2) {
        if (this.qy == null) {
            this.qy = (WindowCustomWrapper) this.kj.bHv().CreatJComponentFromXML(pathEulaXML);
            JButton var3 = this.qy.findJButton("acceptButton");
            this.qy.setTitle(this.kj.translate("End User License Agreement"));
            this.qy.pack();
            this.qy.findJButton("refuseButton").addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent var1) {
                    if (LoginAddon.this.qQ != null) {
                        LoginAddon.this.qQ.n(false);
                        LoginAddon.this.qQ = null;
                    }

                    LoginAddon.this.qB.setExit();
                }
            });
            var3.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent var1) {
                    LoginAddon.this.qy.setVisible(false);
                    HtmlCustomWrapper var2 = (HtmlCustomWrapper) LoginAddon.this.qy.findJComponent("content");
                    var2.setText("");
                    if (LoginAddon.this.qQ != null) {
                        LoginAddon.this.qQ.n(true);
                        LoginAddon.this.qQ = null;
                    }

                }
            });
        }

        this.qQ = var1;
        this.a(this.qy, "end_user_license_in_game", var2);
    }

    private void b(ahM var1, boolean var2) {
        if (this.qP == null) {
            this.qP = (WindowCustomWrapper) this.kj.bHv().CreatJComponentFromXML(pathEulaXML);
            this.qP.setTitle(this.kj.translate("Terms Of Use"));
            this.qP.pack();
            this.qP.findJButton("refuseButton").addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent var1) {
                    if (LoginAddon.this.qR != null) {
                        LoginAddon.this.qR.n(false);
                        LoginAddon.this.qR = null;
                    }

                    LoginAddon.this.qB.setExit();
                }
            });
            this.qP.findJButton("acceptButton").addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent var1) {
                    LoginAddon.this.qP.setVisible(false);
                    HtmlCustomWrapper var2 = (HtmlCustomWrapper) LoginAddon.this.qP.findJComponent("content");
                    var2.setText("");
                    if (LoginAddon.this.qR != null) {
                        LoginAddon.this.qR.n(true);
                        LoginAddon.this.qR = null;
                    }

                }
            });
        }

        this.qR = var1;
        this.a(this.qP, "terms_of_use_in_game", var2);
    }

    private void a(final WindowCustomWrapper var1, final String var2, boolean var3) {
        JButton var4 = var1.findJButton("refuseButton");
        final JButton var5 = var1.findJButton("acceptButton");
        if (var3) {
            var4.setVisible(false);
            var1.setClosable(true);
            var5.setText(this.kj.translate("OK"));
            var5.setEnabled(true);
        } else {
            var1.setClosable(false);
            var4.setVisible(true);
            var5.setEnabled(false);
            var5.setText(this.kj.translate("Accept"));
        }

        final a var6 = new a(var1.findJLabel("loading"));
        final IContainerCustomWrapper var7 = var1.ce("switcher");
        final CardLayout var8 = (CardLayout) var7.getLayout();
        var6.start();
        var8.show((Container) var7, "loading");
        Thread var9 = new Thread("Some Contract page") {
            public void run() {
                String var1x = aSj.a("http://www.taikodom.com.br/" + var2, "locale=" + I18NString.getCurrentLocation(), new File("data/web-cache/"));
                HtmlCustomWrapper var2x = (HtmlCustomWrapper) var1.findJComponent("content");
                if (var1x != null) {
                    var2x.setText(var1x);
                } else {
                    var2x.setText(LoginAddon.this.kj.translate("Error retrieving Terms of Use"));
                }

                var6.canceled(true);
                var8.show((Container) var7, "contents");
                var5.setEnabled(true);
            }
        };
        var9.setDaemon(true);
        var9.start();
        var1.center();
        var1.setVisible(true);
    }

    private void hz() {
        this.newsPanel = (PanelCustomWrapper) this.loginUI.findJComponent("newsPanel");
        this.newsPanel.setVisible(false);
        Thread var1 = new Thread("Init news panel") {
            public void run() {
                String var1;
                try {
                    var1 = LoginAddon.this.qB.loadServerStatusTxt();
                } catch (RuntimeException var3) {
                    var1 = "Could not connect to server.";
                    var3.printStackTrace();
                }

                if (LoginAddon.this.newsPanel != null) {
                    JTextComponent var23 = ((JTextComponent) LoginAddon.this.newsPanel.findJComponent("content"));
                    if (var23 != null) {
                        var23.setText(var1);
                    }
                    LoginAddon.this.newsPanel.setVisible(true);
                }
            }
        };
        var1.setDaemon(true);
        var1.start();
    }

    private void hA() {
        this.eulaWindow = (WindowCustomWrapper) this.kj.bHv().CreatJComponentFromXML(pathReleaseNotesXML);
        this.eulaWindow.setTitle(this.kj.translate("Release Notes"));
        this.eulaWindow.pack();
        this.eulaWindow.center();
        JButton var1 = this.eulaWindow.findJButton("acceptButton");
        var1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent var1) {
                LoginAddon.this.eulaWindow.setVisible(false);
            }
        });
        String var2 = this.qB.releaseNotesTxt();
        JTextComponent var23 = ((JTextComponent) this.eulaWindow.findJComponent("content"));
        if (var23 != null) {
            var23.setText(var2);
        }
    }

    private void hB() {
        this.eulaWindow.setVisible(true);
    }

    /**
     * Отправка логина и пароля
     */
    public void startLogin() {
        this.kj.aVU().h(new LoginPasswordValue(this.loginField.getText(), String.valueOf(this.passwordField.getPassword()), false));
        this.userSaveCheckBox.requestFocus();
    }

    private class c extends ImageIcon {
        private static final int BORDER = 6;
        private boolean cW;

        public c(Image var2) {
            super(var2);
        }

        public int getIconHeight() {
            return super.getIconHeight() + BORDER;
        }

        public int getIconWidth() {
            return super.getIconWidth() + BORDER;
        }

        public void start() {
            this.cW = true;
        }

        public void stop() {
            this.cW = false;
        }

        public synchronized void paintIcon(Component var1, Graphics var2, int var3, int var4) {
            Graphics2D var5 = (Graphics2D) var2.create();
            var5.translate(3, 3);
            if (this.cW) {
                var5.rotate((double) (System.currentTimeMillis() / 10L % 360L) * 3.141592653589793D / 180.0D, (double) (var3 + this.getImage().getWidth(var1) / 2), (double) (var4 + this.getImage().getHeight(var1) / 2));
            } else {
                var5.rotate(0.0D);
            }

            var5.drawImage(this.getImage(), var3, var4, var1);
        }
    }

    private class a extends aen {
        final JLabel qk;
        final c ql;

        public a(JLabel var2) {
            this.qk = var2;
            this.ql = LoginAddon.this.new c(LoginAddon.this.kj.bHv().adz().getImage(ImagesetGui.imagesetLogin.getPath() + "loading_icon"));
            var2.setIcon(this.ql);
        }

        public void start() {
            this.ql.start();
            LoginAddon.this.kj.alf().b("UpdateTargetPositiontask", this, 10L);
        }

        public void run() {
            this.qk.repaint();
        }
    }
}
