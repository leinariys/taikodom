package taikodom.addon.input;

import all.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.Map.Entry;

//import all.akU;
//import all.t;

/**
 * Менеджер устройств ввода клава джойстик, и их локализация
 * Командный переводчик Addon
 */
@awK("taikodom.addon.input")
public class CommandTranslatorAddon implements fo {
    private static final String JJ = "input";
    /**
     * Менеджер настроек
     */
    private ConfigManager JK;
    private boolean disabled;
    private int JL = -1;
    private Map JM = new HashMap();
    private Map JN = new HashMap();
    private Set JO = new HashSet();
    private String JP;
    private vW kj;
    private aAq_q JQ;

    public void InitAddon(vW var1) {

        this.kj = var1;
        this.JL = 1;
        this.JQ = new aAq_q(this.kj);
        this.JK = this.kj.getEngineGame().getConfigManager();
        this.pk();
        this.pl();
        this.disabled = false;
        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(this.JQ);
        final JRootPane var2 = this.kj.getEngineGame().getEngineGraphics().aee().getRootPane();
        aWs var3 = new aWs() {
            public boolean i(Component var1) {
                vW var2 = CommandTranslatorAddon.this.kj;
                if (var2 != null) {/*
               t var3 = var2.alb();
               if (var3 != null && var3.anX())
               {
                  akU var4 = var3.dL();
                  if (var4 != null && !var4.bQB())
                  {
                     return false;
                  }
               }*/
                }

                return true;
            }
        };
        if (ComponentManager.getCssHolder(var2.getContentPane()) == null) {
            ((JComponent) var2.getContentPane()).updateUI();
        }

        if (ComponentManager.getCssHolder(var2.getLayeredPane()) == null) {
            new ComponentManager("layeredPane", var2.getLayeredPane());
        }

        ComponentManager.getCssHolder(var2.getContentPane()).a(var3);
        ComponentManager.getCssHolder(var2.getLayeredPane()).a(var3);
        MouseMotionAdapter var4 = new MouseMotionAdapter() {
            private int czY;
            private int czZ;

            public void mouseDragged(MouseEvent var1) {
                float var2x = (float) var2.getWidth();
                float var3 = (float) var2.getHeight();
                int var4 = var1.getX();
                int var5 = var1.getY();
                int var6 = var4 - this.czY;
                int var7 = var5 - this.czZ;
                this.czY = var4;
                this.czZ = var5;
                CommandTranslatorAddon.this.h(new aIU((float) var4 / var2x, (float) var5 / var3, (float) var6 / var2x, (float) var7 / var3, var4, var5, var6, var7, Nb.lQ(var1.getModifiersEx())));
            }

            public void mouseMoved(MouseEvent var1) {
                float var2x = (float) var2.getWidth();
                float var3 = (float) var2.getHeight();
                int var4 = var1.getX();
                int var5 = var1.getY();
                int var6 = var4 - this.czY;
                int var7 = var5 - this.czZ;
                this.czY = var4;
                this.czZ = var5;
                if ((float) var4 != var2x / 2.0F || (float) var5 != var3 / 2.0F) {
                    CommandTranslatorAddon.this.h(new aIU((float) var4 / var2x, (float) var5 / var3, (float) var6 / var2x, (float) var7 / var3, var4, var5, var6, var7, Nb.lQ(var1.getModifiersEx())));
                }

            }
        };
        MouseWheelListener var5 = new MouseWheelListener() {
            public void mouseWheelMoved(MouseWheelEvent var1) {
                float var2x = (float) var2.getWidth();
                float var3 = (float) var2.getHeight();
                if (var1.getScrollType() == 0) {
                    CommandTranslatorAddon.this.h(new mu(var1.getWheelRotation() < 0 ? 3 : 4, true, (float) var1.getX() / var2x, (float) var1.getY() / var3, var1.getX(), var1.getY(), Nb.lQ(var1.getModifiersEx())));
                }

            }
        };
        MouseAdapter var6 = new MouseAdapter() {
            public void mousePressed(MouseEvent var1) {
                float var2x = (float) var2.getWidth();
                float var3 = (float) var2.getHeight();
                var2.requestFocus();
                CommandTranslatorAddon.this.h(new mu(ja.ca(var1.getButton()), true, (float) var1.getX() / var2x, (float) var1.getY() / var3, var1.getX(), var1.getY(), Nb.lQ(var1.getModifiersEx())));
            }

            public void mouseReleased(MouseEvent var1) {
                float var2x = (float) var2.getWidth();
                float var3 = (float) var2.getHeight();
                CommandTranslatorAddon.this.h(new mu(ja.ca(var1.getButton()), false, (float) var1.getX() / var2x, (float) var1.getY() / var3, var1.getX(), var1.getY(), Nb.lQ(var1.getModifiersEx())));
                int var4 = var1.getModifiersEx();
                if (var4 != 0) {
                    CommandTranslatorAddon.this.h(new mu(ja.ca(var1.getButton()), false, (float) var1.getX() / var2x, (float) var1.getY() / var3, var1.getX(), var1.getY(), 0));
                }

            }
        };
        var2.addMouseListener(var6);
        var2.addMouseWheelListener(var5);
        var2.addMouseMotionListener(var4);
    }

    private void h(Object var1) {
        this.kj.aVU().h(var1);
    }

    public void stop() {
    }

    public void pk() {
        this.JN.clear();
        aPP_t[] var4;
        int var3 = (var4 = aPP_t.values()).length;

        label65:
        for (int var2 = 0; var2 < var3; ++var2) {
            aPP_t var1 = var4[var2];
            ConfigManagerSection var5 = null;

            try {//Получаем раздел
                var5 = var1 == aPP_t.ALL ? this.JK.getSection("input") : this.JK.getSection("input." + var1.name());
            } catch (InvalidSectionNameException var14) {
            }

            if (var5 != null) {
                Iterator var7 = var5.getPropertys().entrySet().iterator();

                while (true) {
                    String var8;
                    String[] var9;
                    String var10;
                    int var11;
                    int var12;
                    String[] var13;
                    do {
                        do {
                            if (!var7.hasNext()) {
                                continue label65;
                            }

                            Entry var6 = (Entry) var7.next();
                            var8 = (String) var6.getKey();
                            var9 = String.valueOf(((ConfigManagerProperty) var6.getValue()).getValueProperty()).split("[, \t;:]+");

                            if (this.JN.get(var8) != null) {
                                System.err.println("** Key " + var8 + " is already mapped ***");//is already mapped
                                var12 = (var13 = (String[]) this.JN.get(var8)).length;

                                for (var11 = 0; var11 < var12; ++var11) {
                                    var10 = var13[var11];
                                    var9 = (String[]) He.b(var9, var10);
                                }
                            }
                        } while (var9 == null);
                    } while (var9.length <= 0);

                    this.JN.put(var8, var9);
                    var13 = var9;
                    var12 = var9.length;

                    for (var11 = 0; var11 < var12; ++var11) {
                        var10 = var13[var11];
                        this.kj.aVU().a((String) (var10 + "_KEY"), (Object) var8);
                    }
                }
            }
        }

    }

    public void enable() {
        this.disabled = false;
    }

    public void disable() {
        this.disabled = true;
    }

    public String ae(String var1) {
        return (String) this.JM.get(var1);
    }

    private void pl() {
        this.kj.aVU().a((Class) mg.class, (aVH) (new ags() {
            @Override
            public boolean WinInet(Object var1) {
                return c((mg) var1);
            }

            public boolean c(mg var1) {
                if (CommandTranslatorAddon.this.disabled) {
                    return true;
                } else {
                    String var2 = Kx.ls(var1.Nh());
                    String var3 = Kx.ls(CommandTranslatorAddon.this.JL & (var1.getModifiers() | CommandTranslatorAddon.this.JQ.cHM()));
                    if (!var3.equals("UNDEFINED") && !var3.equals(var2)) {
                        var2 = var3 + "_" + var2;
                    }

                    boolean var4 = var1.isPressed();
                    if (var4) {
                        if (CommandTranslatorAddon.this.JO.contains(var1.getUnicode())) {
                            return true;
                        }

                        CommandTranslatorAddon.this.JO.add(var1.getUnicode());
                    } else {
                        CommandTranslatorAddon.this.JO.remove(var1.getUnicode());
                    }

                    CommandTranslatorAddon.this.a(var2, var4, var1);
                    return true;
                }
            }
        }));

        this.kj.aVU().a((Class) mu.class, (aVH) (new ags() {
            @Override
            public boolean WinInet(Object var1) {
                return c((mu) var1);
            }

            public boolean c(mu var1) {
                if (CommandTranslatorAddon.this.disabled) {
                    return true;
                } else {
                    String var2 = rq.bf(var1.getButton());
                    String var3 = Kx.ls(CommandTranslatorAddon.this.JL & (var1.getModifiers() | CommandTranslatorAddon.this.JQ.cHM()));
                    if (!var3.equals("UNDEFINED")) {
                        var2 = var3 + "_" + var2;
                    }

                    CommandTranslatorAddon.this.a(var2, var1.isPressed(), var1);
                    return true;
                }
            }
        }));

        this.kj.aVU().a((Class) OA.class, (aVH) (new ags() {
            @Override
            public boolean WinInet(Object var1) {
                return b((OA) var1);
            }

            public boolean b(OA var1) {
                if (CommandTranslatorAddon.this.disabled) {
                    return true;
                } else {
                    CommandTranslatorAddon.this.a(xA_w.bf(var1.getButton()), var1.isPressed(), var1);
                    return true;
                }
            }
        }));
        this.kj.aVU().a((Class) Et_g.class, (aVH) (new ags() {
            @Override
            public boolean WinInet(Object var1) {
                return a((Et_g) var1);
            }

            public boolean a(Et_g var1) {
                if (CommandTranslatorAddon.this.disabled) {
                    return true;
                } else {
                    String var2 = QK.bf(var1.getButton());
                    if (var2 != CommandTranslatorAddon.this.JP && CommandTranslatorAddon.this.JP != "UNDEFINED") {
                        CommandTranslatorAddon.this.a(CommandTranslatorAddon.this.JP, false, var1);
                    }

                    CommandTranslatorAddon.this.JP = var2;
                    if (var2 != "UNDEFINED") {
                        CommandTranslatorAddon.this.a(var2, true, var1);
                    }

                    return true;
                }
            }
        }));
    }

    private void a(String var1, boolean var2, jq var3) {
        if (var1 != null) {
            String[] var4 = (String[]) this.JN.get(var1);
            if (var4 != null && var4.length > 0) {
                String[] var8 = var4;
                int var7 = var4.length;

                for (int var6 = 0; var6 < var7; ++var6) {
                    String var5 = var8[var6];
                    this.kj.aVU().d(var5, new aSd(var5, var3, var2));
                }
            }

        }
    }

    public String af(String var1) {
        Iterator var3 = this.JN.entrySet().iterator();

        while (var3.hasNext()) {
            Entry var2 = (Entry) var3.next();
            String[] var7;
            int var6 = (var7 = (String[]) var2.getValue()).length;

            for (int var5 = 0; var5 < var6; ++var5) {
                String var4 = var7[var5];
                if (var4.equals(var1)) {
                    String[] var8 = ((String) var2.getKey()).split("_");
                    if (var8.length == 1) {
                        return var8[0];
                    }

                    String var9 = "";

                    for (int var10 = 0; var10 < var8.length; ++var10) {
                        var9 = var9 + var8[var10];
                        if (var10 < var8.length - 1) {
                            var9 = var9 + "+";
                        }
                    }

                    return var9;
                }
            }
        }

        return "";
    }
}
