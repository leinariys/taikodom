package taikodom.addon.messagedialog;

import all.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@awK("taikodom.addon.messagedialog")
public class MessageDialogAddon implements fo {
    // $FF: synthetic field
    private static int[] nR;
    private vW kj;
    private WindowCustomWrapper dialogUI;
    private ags nN;
    private ags nO;
    private rT nP;
    private JCheckBox nQ;

    // $FF: synthetic method
    static int[] gr() {
        int[] var10000 = nR;
        if (nR != null) {
            return var10000;
        } else {
            int[] var0 = new int[Nn.atitle.values().length];

            try {
                var0[Nn.atitle.question.ordinal()] = 2;
            } catch (NoSuchFieldError var4) {
                ;
            }

            try {
                var0[Nn.atitle.error.ordinal()] = 4;
            } catch (NoSuchFieldError var3) {
                ;
            }

            try {
                var0[Nn.atitle.info.ordinal()] = 1;
            } catch (NoSuchFieldError var2) {
                ;
            }

            try {
                var0[Nn.atitle.warning.ordinal()] = 3;
            } catch (NoSuchFieldError var1) {
                var1.printStackTrace();
            }

            nR = var0;
            return var0;
        }
    }

    /**
     * Передаём в аддон логирование и локализацию
     *
     * @param var1 class GK Обёртка класса аддона логирование и локализация
     */
    public void InitAddon(vW var1) {
        this.kj = var1;
        this.nN = new ags() {
            @Override
            public boolean WinInet(Object var1) {
                return a((aqX) var1);
            }

            public boolean a(aqX var1) {
                String var2 = adg.format(var1.KI().get(), var1.getParams());
                aEP var3 = new aEP() {
                    public void a(int var1x, String var2, boolean var3) {
                        MessageDialogAddon.this.gq();
                    }

                    public Nn.atitle yD() {
                        return var1.crM() == null ? super.yD() : var1.crM();
                    }
                };
                MessageDialogAddon var4 = MessageDialogAddon.this;
                synchronized (MessageDialogAddon.this) {
                    while (MessageDialogAddon.this.dialogUI != null) {
                        try {
                            this.wait();
                        } catch (InterruptedException var5) {
                            ;
                        }
                    }

                    MessageDialogAddon.this.dialogUI = MessageDialogAddon.this.CreateMessageDialog(var2, var3, MessageDialogAddon.this.kj.translate("OK"));
                    return false;
                }
            }
        };

        this.nO = new ags() {
            @Override
            public boolean WinInet(Object var1) {
                return a((Nn) var1);
            }

            public boolean a(Nn var1) {
                String var2 = adg.format(var1.getMessage(), var1.getParams());
                aEP var3 = new aEP() {
                    public void a(int var1x, String var2, boolean var3) {
                        MessageDialogAddon.this.gq();
                    }

                    public Nn.atitle yD() {
                        return var1.crM() == null ? super.yD() : var1.crM();
                    }
                };
                MessageDialogAddon var4 = MessageDialogAddon.this;
                synchronized (MessageDialogAddon.this) {
                    while (MessageDialogAddon.this.dialogUI != null) {
                        try {
                            this.wait();
                        } catch (InterruptedException var5) {
                            ;
                        }
                    }

                    MessageDialogAddon.this.dialogUI = MessageDialogAddon.this.CreateMessageDialog(var2, var3, MessageDialogAddon.this.kj.translate("OK"));
                    return false;
                }
            }
        };

        this.nP = new rT() {
            @Override
            public void b(Object var1) {
                a((to_q) var1);
            }

            public void a(to_q var1) {
                if (MessageDialogAddon.this.dialogUI != null) {
                    MessageDialogAddon.this.gq();
                    var1.adC();
                }
            }
        };
        ajJ_q var2 = this.kj.aVU();
        var2.a((Class) aqX.class, (aVH) this.nN);
        var2.a((Class) Nn.class, (aVH) this.nO);
        var2.a((Class) to_q.class, (aVH) this.nP);
    }

    public void stop() {
    }

    private void gq() {
        synchronized (this) {
            if (this.dialogUI != null) {
                this.dialogUI.setVisible(false);
                this.dialogUI.destroy();
                this.dialogUI = null;
                this.notify();
            }
        }
    }

    public WindowCustomWrapper CreateMessageDialog(String var1, aEP var2, String... var3) {
        if (var2 == null) {
            var2 = new aEP();
        }

        final boolean var4 = var2.VC();
        this.dialogUI = (WindowCustomWrapper) this.kj.bHv().CreatJComponentFromXML("messageDialog.xml");
        Nn.atitle var5 = var2.yD();
        ComponentManager.getCssHolder(this.dialogUI).setAttribute("class", var5.title);
        ComponentManager.getCssHolder(this.dialogUI).clear();
        RepeaterCustomWrapper var6 = this.dialogUI.ch("buttons");
        final FormattedFieldCustomWrapper var7 = (FormattedFieldCustomWrapper) this.dialogUI.findJComponent("input");
        this.nQ = (JCheckBox) this.dialogUI.findJComponent("checkbox");
        String var8 = var2.cyE();
        if (var8 != null) {
            this.nQ.setText(var8);
            this.nQ.setVisible(true);
        } else {
            // this.nQ.setVisible(false);
        }

        aEP finalVar = var2;

        var6.a(new RepeaterCustomWrapper.a() {
            @Override
            public void a(Object var1, Component var2) {
                JButton var3 = (JButton) var2;
                var3.setText(((h) var1).text);
                var3.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent var1x) {
                        MessageDialogAddon.this.gq();
                        finalVar.a(((h) var1).index, var4 ? var7.getText() : null, MessageDialogAddon.this.nQ.isSelected());
                    }
                });
            }
        });

        for (int var10 = 0; var10 < var3.length; ++var10) {
            var6.G(new h(var3[var10], var10));
        }

        TextAreaCustomWrapper var12 = (TextAreaCustomWrapper) this.dialogUI.findJComponent("message");
        var12.setText(var1);
        var12.setSize(var12.getPreferredSize());
        String var11 = var2.getHeader();
        if (var11 == null) {
            var11 = this.a(var5);
        }

        if (var11 == null) {
            this.dialogUI.findJComponent("header").setVisible(false);
        } else {
            this.dialogUI.findJLabel("header").setText(var11);
        }

        var7.setVisible(var4);
        var7.setEnabled(var4);
        if (var4) {
            var7.requestFocus();
            var2.a(var7);
        }

        this.dialogUI.pack();
        this.dialogUI.center();
        this.dialogUI.setVisible(true);
        this.dialogUI.setAlwaysOnTop(true);
        return this.dialogUI;
    }

    public WindowCustomWrapper a(final Nn.atitle var1, String var2, String... var3) {
        return this.CreateMessageDialog(var2, new aEP() {
            public Nn.atitle yD() {
                return var1;
            }
        }, var3);
    }

    public WindowCustomWrapper a(String var1, aFH var2) {
        if (var2 == null) {
            var2 = new aFH();
        }

        this.dialogUI = (WindowCustomWrapper) this.kj.bHv().CreatJComponentFromXML("quantityDialog.xml");
        final JSpinner var3 = (JSpinner) this.dialogUI.findJComponent("amount");
        var3.setModel(new SpinnerNumberModel(var2.getValue(), var2.getMinimum(), var2.getMaximum(), var2.cZi()));
        aFH finalVar = var2;

        this.dialogUI.findJButton("canceled").addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent var1) {
                finalVar.a(false, ((Integer) ((SpinnerNumberModel) var3.getModel()).getValue()).intValue());
                MessageDialogAddon.this.gq();
            }
        });

        this.dialogUI.findJButton("confirm").addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent var1) {
                finalVar.a(true, ((Integer) ((SpinnerNumberModel) var3.getModel()).getValue()).intValue());
                MessageDialogAddon.this.gq();
            }
        });

        this.dialogUI.pack();
        this.dialogUI.center();
        this.dialogUI.setVisible(true);
        this.dialogUI.setAlwaysOnTop(true);
        return this.dialogUI;
    }

    private String a(Nn.atitle var1) {
        switch (gr()[var1.ordinal()]) {
            case 2:
                return this.kj.translate("Corfirmation");
            case 3:
                return this.kj.translate("Warning");
            case 4:
                return this.kj.translate("Error");
            default:
                return this.kj.translate("Info");
        }
    }

    /**
     * Текст в кнопке
     */
    private class h {
        /**
         * Текст в кнопке
         */
        String text;

        /**
         * Индекс
         */
        int index;

        /**
         * Задать текст и индекс для кнопки
         *
         * @param var2
         * @param var3
         */
        public h(String var2, int var3) {
            this.text = var2;
            this.index = var3;
        }
    }
}
