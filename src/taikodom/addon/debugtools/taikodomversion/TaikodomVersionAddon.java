package taikodom.addon.debugtools.taikodomversion;

import all.*;

import java.awt.*;

/**
 * Аддон показывает версию
 */
@awK("taikodom.addon.debugtools.taikodomversion")
public class TaikodomVersionAddon implements fo {
    private WindowCustomWrapper versionUI;

    /**
     * Передача логирования и локолизации
     *
     * @param var1 class GK Обёртка класса аддона логирование и локализация
     */
    public void InitAddon(vW var1) {
        // if (var1.ala() != null && var1.ala().aLQ().deu())
        {
            this.versionUI = (WindowCustomWrapper) var1.bHv().CreatJComponentFromXML("taikodomversion.xml");
            ComponentManager.getCssHolder(this.versionUI).aO(false);
            //TODO не находит графику
            this.versionUI.findJLabel("version").setText("1.0.4933.55");
            this.versionUI.pack();
            Dimension var2 = this.versionUI.getPreferredSize();   //Размер компонента java.awt.Dimension[width=200,height=16]
            Dimension var3 = var1.bHv().getScreenSize();    //Размер окна java.awt.Dimension[width=1024,height=780]
            this.versionUI.setLocation(var3.width - var2.width, var3.height - var2.height);//824  764
            this.versionUI.setVisible(true);
        }
    }

    /**
     * Остановить работу аддона
     */
    public void stop() {
        if (this.versionUI != null) {
            this.versionUI.destroy();
        }
    }
}
