package taikodom.addon.fx;

import all.*;
import taikodom.render.scene.SoundObject;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

//import all.adq;
//import all.auX;
//import all.yG;

/**
 * Озвучка интерфейса
 */
@awK("taikodom.addon.fx")
public class InterfaceSFXAddon implements fo {
    private static final int iYf = 2000;
    private static final LogPrinter logger = LogPrinter.setClass(InterfaceSFXAddon.class);
    private final Map iYh = new HashMap();
    private final Timer iYq = new Timer(100, new ActionListener() {
        public void actionPerformed(ActionEvent var1) {
            ArrayList var2 = new ArrayList();
            Iterator var4 = InterfaceSFXAddon.this.iYh.entrySet().iterator();

            while (true) {
                Entry var3;
                SoundObject var5;
                do {
                    if (!var4.hasNext()) {
                        var4 = var2.iterator();

                        while (var4.hasNext()) {
                            SoundObject var6 = (SoundObject) var4.next();
                            InterfaceSFXAddon.this.iYh.remove(var6);
                        }

                        if (InterfaceSFXAddon.this.iYh.size() == 0) {
                            InterfaceSFXAddon.this.iYq.stop();
                        }

                        return;
                    }

                    var3 = (Entry) var4.next();
                    var5 = (SoundObject) var3.getKey();
                } while (System.currentTimeMillis() - ((Long) var3.getValue()).longValue() <= iYf && !var5.isPlaying() && !var5.isDisposed());

                var5.stop();
                var2.add(var5);
            }
        }
    });
    private vW kj;
    /**
     * Базовые элементы интерфейса
     */
    private BaseUItegXML aPy;
    private boolean enabled = true;
    private Map iYg = new HashMap();
    private ags iYi;
    private ags iYj;
    private ags iYk;
    private ags iYl;
    private ags glE;
    private ags fgV;
    private ags iYm;
    private ags iYn;
    private ags fwc;
    private ags iYo;
    private ags iYp;

    private void dAM() {
        this.kj.aVU().a((Class) xF.class, (aVH) this.glE);
        this.kj.aVU().a((Class) LY.class, (aVH) this.iYl);
//      this.kj.aVU().add((Class)null/*auX.class*/, (aVH)this.iYm);
        this.kj.aVU().a((Class) avs_q.class, (aVH) this.iYn);
//      this.kj.aVU().add((Class)null/*adq.class*/, (aVH)this.iYp);
        this.kj.aVU().a((Class) Wj.class, (aVH) this.iYo);
        this.kj.aVU().a((Class) Sq.class, (aVH) this.fgV);
    }

    private void iG() {
        this.iYi = new ags() {
            @Override
            public boolean WinInet(Object var1) {
                return a((dG) var1);
            }

            public boolean a(dG var1) {
                InterfaceSFXAddon.this.aPy.any().A(var1.getFile(), var1.getHandle());
                return false;
            }
        };

        this.iYj = new ags() {
            @Override
            public boolean WinInet(Object var1) {
                return a((auD_t) var1);
            }

            public boolean a(auD_t var1) {
                if (var1.aDJ()) {
                    InterfaceSFXAddon.this.d(var1.aDI());
                } else {
                    InterfaceSFXAddon.this.a(var1.aDI(), false, var1.cxL());
                }
                return false;
            }
        };

        this.iYk = new ags() {
            @Override
            public boolean WinInet(Object var1) {
                return a((CZ) var1);
            }

            public boolean a(CZ var1) {
                if (var1.aDJ()) {
                    InterfaceSFXAddon.this.d(var1.aDI());
                } else {
                    ArrayList var2 = new ArrayList();
                    Iterator var4 = InterfaceSFXAddon.this.iYg.values().iterator();

                    b var3;
                    while (var4.hasNext()) {
                        var3 = (b) var4.next();
                        if (var3.bIo() == a.exV) {
                            var2.add(var3);
                        }
                    }

                    var4 = var2.iterator();

                    while (var4.hasNext()) {
                        var3 = (b) var4.next();
                        InterfaceSFXAddon.this.d(var3.bIn());
                    }

                    InterfaceSFXAddon.this.a(var1.aDI(), false, true);
                }

                return false;
            }
        };

        this.fwc = new ags() {
            @Override
            public boolean WinInet(Object var1) {
                return a((mq) var1);
            }

            public boolean a(mq var1) {
                switch (var1.aiH()) {
                    case 0:
                        InterfaceSFXAddon.this.enabled = false;
                        break;
                    case 1:
                        InterfaceSFXAddon.this.enabled = true;
                }

                return false;
            }
        };

        this.glE = new ags() {
            @Override
            public boolean WinInet(Object var1) {
                return b((xF) var1);
            }

            public boolean b(xF var1) {
                InterfaceSFXAddon.this.c((aeO) tV_q.btU);
                return false;
            }
        };

        this.iYl = new ags() {
            @Override
            public boolean WinInet(Object var1) {
                return a((LY) var1);
            }

            public boolean a(LY var1) {
            /*if (var1.afR() instanceof yG)
            {
               InterfaceSFXAddon.this.CreateJComponent((aeO)tV_q.buk);
            } else */
                {
                    InterfaceSFXAddon.this.c((aeO) tV_q.buj);
                }

                return false;
            }
        };

        this.iYm = new ags() {
            public boolean WinInet(Object var1) {
                InterfaceSFXAddon.this.c((aeO) tV_q.buj);
                return false;
            }
        };


        this.iYn = new ags() {
            @Override
            public boolean WinInet(Object var1) {
                return a((avs_q) var1);
            }

            public boolean a(avs_q var1) {
                InterfaceSFXAddon.this.c((aeO) tV_q.btR);
                return false;
            }
        };

        this.iYp = new ags() {
            public boolean WinInet(Object var1) {
                InterfaceSFXAddon.this.c((aeO) tV_q.buh);
                return false;
            }
        };

        this.iYo = new ags() {
            @Override
            public boolean WinInet(Object var1) {
                return c((Wj) var1);
            }

            public boolean c(Wj var1) {
                // if (InterfaceSFXAddon.this.kj.dL() != null && aMU.iqm.compareTo(InterfaceSFXAddon.this.kj.dL().cXm()) < 0)
                {
                    InterfaceSFXAddon.this.c((aeO) tV_q.btT);
                }

                return false;
            }
        };

        this.fgV = new ags() {
            @Override
            public boolean WinInet(Object var1) {
                return b((Sq) var1);
            }

            public boolean b(Sq var1) {
                if (var1.fMR == Sq.a.edZ) {
                    InterfaceSFXAddon.this.c((aeO) tV_q.bup);
                }

                return false;
            }
        };
    }

    private void c(aeO var1) {
        this.a(var1, false, false);
    }

    private void a(aeO var1, boolean var2, boolean var3) {
        if (this.enabled) {
            if (!var2) {
                b var4 = (b) this.iYg.get(var1);
                if (var4 != null && var4.bIo() == a.exU && var4.bIp().isPlaying()) {
                    return;
                }

                SoundObject var5 = this.aPy.any().C(var1.getHandle(), var1.getFile());
                if (var5 == null) {
                    if (this.kj.getEngineGraphics().aeg() > 0.0F) {
                        logger.error("SoundPlayer failed to create: " + var1.getFile() + " - " + var1.getHandle());
                    }

                    return;
                }

                if (var4 != null) {
                    var4.bIp().stop();
                    this.iYg.remove(var1);
                }

                a var6;
                if (var3) {
                    var6 = a.exT;
                } else {
                    if (var5.getLoopCount() != 0 && var5.getLoopCount() <= 1) {
                        return;
                    }

                    var6 = a.exU;
                }

                this.iYg.put(var1, new b(var1, var5, var6));
            } else {
                this.d(var1);
            }

        }
    }

    private void dAN() {
        this.kj.aVU().a(this.glE);
        this.kj.aVU().a(this.iYl);
        // this.kj.aVU().WinInet(this.iYm);
        this.kj.aVU().a(this.iYn);
        // this.kj.aVU().WinInet(this.iYp);
        this.kj.aVU().a(this.iYo);
        this.kj.aVU().a(this.fgV);
    }

    public void InitAddon(vW var1) {
        this.kj = var1;
        this.aPy = BaseUItegXML.thisClass;
        this.aPy.a((Ma) Ix.aWp());
        this.iG();
        this.kj.aVU().a((Class) dG.class, (aVH) this.iYi);
        this.kj.aVU().a((Class) auD_t.class, (aVH) this.iYj);
        this.kj.aVU().a((Class) CZ.class, (aVH) this.iYk);
        this.kj.aVU().a((Class) mq.class, (aVH) this.fwc);
        this.dAM();
    }

    public void stop() {
        this.kj.aVU().a(this.iYi);
        this.kj.aVU().a(this.iYj);
        this.kj.aVU().a(this.iYk);
        this.kj.aVU().a(this.fwc);
        this.dAN();
    }

    private void d(aeO var1) {
        b var2 = (b) this.iYg.get(var1);
        if (var2 != null) {
            this.iYg.remove(var2);
            SoundObject var3 = var2.bIp();
            if (!var3.isPlaying() && !var3.isDisposed()) {
                if (var3.getSource() == null) {
                    this.iYh.put(var3, System.currentTimeMillis());
                    this.iYq.restart();
                }
            } else {
                var3.stop();
            }
        }

    }

    private static enum a {
        exT,
        exU,
        exV;
    }

    private class b {
        private final aeO cDc;
        private final a eNt;
        private final SoundObject eNu;

        public b(aeO var2, SoundObject var3, a var4) {
            this.cDc = var2;
            this.eNu = var3;
            this.eNt = var4;
        }

        public aeO bIn() {
            return this.cDc;
        }

        public a bIo() {
            return this.eNt;
        }

        public SoundObject bIp() {
            return this.eNu;
        }
    }
}
