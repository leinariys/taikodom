package taikodom.addon.fx;

import all.*;

//import all.rP;


@awK("taikodom.addon.fx")
public class MusicAddon implements fo {
    private static final long cUc = 2000L;
    private vW kj;
    private ags cUd;
    private ags Rh;
    private ags cUe;
    private ags bwZ;

    public void InitAddon(vW var1) {
        this.kj = var1;
        this.cUd = new ags() {
            // $FF: synthetic field
            private int[] hxI;

            @Override
            public boolean WinInet(Object var1) {
                return b((Sq) var1);
            }

            public boolean b(Sq var1) {
                switch (cRQ()[var1.fMR.ordinal()]) {
                    case 1:
                        MusicAddon.this.aPa();
                        MusicAddon.this.aPb();
                        MusicAddon.this.b(agH.fyQ, 3000.0F);
                        // if (MusicAddon.this.kj.dL().cXm() == aMU.iqu || MusicAddon.this.kj.dL().cXm() == aMU.iqt)
                    {
                        MusicAddon.this.a((aeO) agH.fyO);
                    }
                    case 2:
                    default:
                        return false;
                }
            }

            // $FF: synthetic method
            int[] cRQ() {
                int[] var10000 = hxI;
                if (hxI != null) {
                    return var10000;
                } else {
                    int[] var0 = new int[Sq.a.values().length];

                    try {
                        var0[Sq.a.edX.ordinal()] = 1;
                    } catch (NoSuchFieldError var4) {
                        ;
                    }

                    try {
                        var0[Sq.a.edZ.ordinal()] = 3;
                    } catch (NoSuchFieldError var3) {
                        ;
                    }

                    try {
                        var0[Sq.a.eea.ordinal()] = 4;
                    } catch (NoSuchFieldError var2) {
                        ;
                    }

                    try {
                        var0[Sq.a.edY.ordinal()] = 2;
                    } catch (NoSuchFieldError var1) {
                        ;
                    }

                    hxI = var0;
                    return var0;
                }
            }
        };

        this.Rh = new ags() {
            // $FF: synthetic field
            private int[] Zn;

            @Override
            public boolean WinInet(Object var1) {
                return b((aaP_q) var1);
            }

            public boolean b(aaP_q var1) {
                switch (BK()[var1.bMO().ordinal()]) {
                    case 2:
                        MusicAddon.this.aPa();
                        MusicAddon.this.aPb();
                        //final rP var2 = MusicAddon.this.kj.dL().bQx().azW();
                        // if (var2 != null)
                    {
                        aeO var3 = new aeO() {
                            public String getHandle() {
                                return "habitat_commercial_ambience";
                                // return var2.ip() + "_ambience";
                            }

                            public String getFile() {
                                return "data/audio/ambiences/ambiences.pro";
                            }
                        };

                        if (var3.getHandle().contains("birth")) {
                            MusicAddon.this.a(var3, 4000.0F);
                        } else {
                            MusicAddon.this.b(var3, 2000.0F);
                        }
                    }
                    case 1:
                    default:
                        return false;
                }
            }

            // $FF: synthetic method
            int[] BK() {
                int[] var10000 = Zn;
                if (Zn != null) {
                    return var10000;
                } else {
                    int[] var0 = new int[aaP_q.a.values().length];

                    try {
                        var0[aaP_q.a.fkt.ordinal()] = 1;
                    } catch (NoSuchFieldError var4) {
                        ;
                    }

                    try {
                        var0[aaP_q.a.fku.ordinal()] = 2;
                    } catch (NoSuchFieldError var3) {
                        ;
                    }

                    try {
                        var0[aaP_q.a.fkv.ordinal()] = 3;
                    } catch (NoSuchFieldError var2) {
                        ;
                    }

                    try {
                        var0[aaP_q.a.fkw.ordinal()] = 4;
                    } catch (NoSuchFieldError var1) {
                        ;
                    }

                    Zn = var0;
                    return var0;
                }
            }
        };
        this.cUe = new ags() {
            @Override
            public boolean WinInet(Object var1) {
                return b((aUU) var1);
            }

            public boolean b(aUU var1) {
                switch (var1.dAD()) {
                    case 1:
                        MusicAddon.this.a((aeO) agH.fyP);
                        break;
                    case 2:
                        MusicAddon.this.aPb();
                }

                return false;
            }
        };

        this.bwZ = new ags() {
            // $FF: synthetic field
            private int[] JB;

            @Override
            public boolean WinInet(Object var1) {
                return a((oI) var1);
            }

            public boolean a(oI var1) {
                switch (pg()[var1.Us().ordinal()]) {
                    case 10:
                        MusicAddon.this.aPa();
                        MusicAddon.this.aPb();
                        MusicAddon.this.a((aeO) agH.fyN);
                        break;
                    case 11:
                        MusicAddon.this.aPa();
                        MusicAddon.this.aPb();
                        MusicAddon.this.b(agH.fyQ, 3000.0F);
                }

                return false;
            }

            // $FF: synthetic method
            int[] pg() {
                int[] var10000 = JB;
                if (JB != null) {
                    return var10000;
                } else {
                    int[] var0 = new int[oI.a.values().length];

                    try {
                        var0[oI.a.iPq.ordinal()] = 22;
                    } catch (NoSuchFieldError var30) {
                        ;
                    }

                    try {
                        var0[oI.a.iPw.ordinal()] = 28;
                    } catch (NoSuchFieldError var29) {
                        ;
                    }

                    try {
                        var0[oI.a.iPx.ordinal()] = 29;
                    } catch (NoSuchFieldError var28) {
                        ;
                    }

                    try {
                        var0[oI.a.iPf.ordinal()] = 11;
                    } catch (NoSuchFieldError var27) {
                        ;
                    }

                    try {
                        var0[oI.a.iPy.ordinal()] = 30;
                    } catch (NoSuchFieldError var26) {
                        ;
                    }

                    try {
                        var0[oI.a.iPj.ordinal()] = 15;
                    } catch (NoSuchFieldError var25) {
                        ;
                    }

                    try {
                        var0[oI.a.iPo.ordinal()] = 20;
                    } catch (NoSuchFieldError var24) {
                        ;
                    }

                    try {
                        var0[oI.a.iPs.ordinal()] = 24;
                    } catch (NoSuchFieldError var23) {
                        ;
                    }

                    try {
                        var0[oI.a.iPi.ordinal()] = 14;
                    } catch (NoSuchFieldError var22) {
                        ;
                    }

                    try {
                        var0[oI.a.iPk.ordinal()] = 16;
                    } catch (NoSuchFieldError var21) {
                        ;
                    }

                    try {
                        var0[oI.a.iPm.ordinal()] = 18;
                    } catch (NoSuchFieldError var20) {
                        ;
                    }

                    try {
                        var0[oI.a.iPr.ordinal()] = 23;
                    } catch (NoSuchFieldError var19) {
                        ;
                    }

                    try {
                        var0[oI.a.iPv.ordinal()] = 27;
                    } catch (NoSuchFieldError var18) {
                        ;
                    }

                    try {
                        var0[oI.a.iPt.ordinal()] = 25;
                    } catch (NoSuchFieldError var17) {
                        ;
                    }

                    try {
                        var0[oI.a.iPa.ordinal()] = 6;
                    } catch (NoSuchFieldError var16) {
                        ;
                    }

                    try {
                        var0[oI.a.iPc.ordinal()] = 8;
                    } catch (NoSuchFieldError var15) {
                        ;
                    }

                    try {
                        var0[oI.a.iOW.ordinal()] = 2;
                    } catch (NoSuchFieldError var14) {
                        ;
                    }

                    try {
                        var0[oI.a.iPd.ordinal()] = 9;
                    } catch (NoSuchFieldError var13) {
                        ;
                    }

                    try {
                        var0[oI.a.iOZ.ordinal()] = 5;
                    } catch (NoSuchFieldError var12) {
                        ;
                    }

                    try {
                        var0[oI.a.iOV.ordinal()] = 1;
                    } catch (NoSuchFieldError var11) {
                        ;
                    }

                    try {
                        var0[oI.a.iOX.ordinal()] = 3;
                    } catch (NoSuchFieldError var10) {
                        ;
                    }

                    try {
                        var0[oI.a.iOY.ordinal()] = 4;
                    } catch (NoSuchFieldError var9) {
                        ;
                    }

                    try {
                        var0[oI.a.iPb.ordinal()] = 7;
                    } catch (NoSuchFieldError var8) {
                        ;
                    }

                    try {
                        var0[oI.a.iPl.ordinal()] = 17;
                    } catch (NoSuchFieldError var7) {
                        ;
                    }

                    try {
                        var0[oI.a.iPu.ordinal()] = 26;
                    } catch (NoSuchFieldError var6) {
                        ;
                    }

                    try {
                        var0[oI.a.iPe.ordinal()] = 10;
                    } catch (NoSuchFieldError var5) {
                        ;
                    }

                    try {
                        var0[oI.a.iPp.ordinal()] = 21;
                    } catch (NoSuchFieldError var4) {
                        ;
                    }

                    try {
                        var0[oI.a.iPn.ordinal()] = 19;
                    } catch (NoSuchFieldError var3) {
                        ;
                    }

                    try {
                        var0[oI.a.iPg.ordinal()] = 12;
                    } catch (NoSuchFieldError var2) {
                        ;
                    }

                    try {
                        var0[oI.a.iPh.ordinal()] = 13;
                    } catch (NoSuchFieldError var1) {
                        ;
                    }

                    JB = var0;
                    return var0;
                }
            }
        };
        this.kj.aVU().a((Class) Sq.class, (aVH) this.cUd);
        this.kj.aVU().a((Class) aaP_q.class, (aVH) this.Rh);
        this.kj.aVU().a((Class) aUU.class, (aVH) this.cUe);
        this.kj.aVU().a((Class) oI.class, (aVH) this.bwZ);
    }

    public void stop() {
        this.kj.aVU().a(this.cUd);
        this.kj.aVU().a(this.Rh);
        this.kj.aVU().a(this.cUe);
        this.kj.aVU().a(this.bwZ);
    }

    private void a(aeO var1) {
        this.a(var1, 0.0F);
    }

    private void a(aeO var1, float var2) {
        EngineGraphics engineGraphics = this.kj.getEngineGraphics();
        if (engineGraphics != null) {
            engineGraphics.bV(var1.getFile());
            engineGraphics.c(var1.getHandle(), var2);
        }
    }

    private void b(aeO var1) {
        this.b(var1, 0.0F);
    }

    private void b(aeO var1, float var2) {
        EngineGraphics var3 = this.kj.getEngineGraphics();
        if (var3 != null) {
            var3.bV(var1.getFile());
            var3.b(var1.getHandle(), var2);
        }
    }

    private void aPa() {
        this.kj.getEngineGraphics().dr(2000.0F);
    }

    private void aPb() {
        this.kj.getEngineGraphics().ds(2000.0F);
    }
}
