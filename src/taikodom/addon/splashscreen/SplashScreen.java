package taikodom.addon.splashscreen;

import all.*;
import taikodom.render.*;
import taikodom.render.gui.GBillboard;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collection;
import java.util.Iterator;

/**
 * Показ начальной заставки
 */
@awK("taikodom.addon.splashscreen")
public class SplashScreen implements fo {
    /**
     * Панель с видео
     */
    private PanelCustomWrapper panelCustomWrapper;
    /**
     * class GK Обёртка класса аддона логирование и локализация
     */
    private vW kj;
    private ags ijP;
    private aVH ijQ;
    /**
     * Графический движок
     */
    private EngineGraphics engineGraphics;
    /**
     * Видео
     */
    private Image splashScreen;
    private GBillboard ijS;

    /**
     * Передаём в аддон логирование и локализацию
     *
     * @param var1 class GK Обёртка класса аддона логирование и локализация
     */
    public void InitAddon(vW var1) {
        this.kj = var1;
        this.engineGraphics = var1.getEngineGraphics();
        Collection var2 = this.kj.aVU().b(aUU.class);
        Iterator var4 = var2.iterator();

        while (var4.hasNext()) {
            aUU var3 = (aUU) var4.next();
            if (var3.dAD() == 0) {
                this.kj.aVU().g(var3);
                this.open();
                break;
            }
        }
    }

    private void open() {
        agC.bWV().aRS();//Скрыть курсор

        this.splashScreen = this.kj.bHv().adz().getImage("/data/gui/imageset/Splash_Screen.bik");
        ((BinkTexture) ((TexBackedImage) this.splashScreen).getTexture()).play();
        this.panelCustomWrapper = new PanelCustomWrapper();
        this.kj.bHv().addInRootPane(this.panelCustomWrapper);//Добавить в рут панель содержимого новый компонент
        this.panelCustomWrapper.setLocation(0, 0);
        this.panelCustomWrapper.setSize(this.kj.bHv().getScreenWidth(), this.kj.bHv().getScreenHeight()); //1024 - 780
        this.panelCustomWrapper.setVisible(true);
        BinkVideo var1 = new BinkVideo((BinkTexture) ((TexBackedImage) this.splashScreen).getTexture());
        this.engineGraphics = this.kj.getEngineGraphics();//Получить ссылку на графический движок
        this.engineGraphics.eI(1000);
        this.engineGraphics.a((Video) var1);//Передать видео в плеер

        this.ijQ = new ags() {
            @Override
            public boolean WinInet(Object var1) {
                return b((to_q) var1);
            }

            public boolean b(to_q var1) {
                SplashScreen.this.stopVideo();
                return false;
            }
        };

        this.ijP = new ags() {
            @Override
            public boolean WinInet(Object var1) {
                return b((ExclusiveVideoPlayer) var1);
            }

            public boolean b(ExclusiveVideoPlayer var1) {
                SplashScreen.this.stopVideo();
                return false;
            }
        };

        //Остановить видео по щелчку курсора (в оригинале не работает)
        this.panelCustomWrapper.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent var1) {
                SplashScreen.this.stopVideo();
            }
        });

        this.kj.aVU().a("endVideo", ExclusiveVideoPlayer.class, this.ijP);
        this.kj.aVU().a(to_q.class, this.ijQ);
        //SplashScreen.this.stopVideo();//TODO  убрать
    }

    protected void stopVideo() {
        this.kj.aVU().h(new aUU((short) 1));
        this.engineGraphics.stopVideo();
        this.stop();
    }

    public void stop() {
        agC.bWV().aRR();
        if (this.panelCustomWrapper != null) {
            this.panelCustomWrapper.getParent().remove(this.panelCustomWrapper);
            this.panelCustomWrapper = null;
        }

        if (this.splashScreen != null) {
            ((BinkTexture) ((TexBackedImage) this.splashScreen).getTexture()).releaseReferences();
        }

        if (this.engineGraphics != null) {
            this.engineGraphics.aex();
        }

        this.kj.aVU().a(this.ijP);
        this.kj.aVU().a(this.ijQ);
    }
}
