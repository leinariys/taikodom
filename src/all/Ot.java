package all;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface Ot {
    a drz() default a.IMMEDIATE;

    long Ur() default 5000L;

    public static enum a {
        dLq,
        dLr,
        dLs, NOT_PERSISTED, IMMEDIATE
    }
}
