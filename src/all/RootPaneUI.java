package all;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicRootPaneUI;
import java.awt.*;

/**
 * RootPaneUI
 */
public class RootPaneUI extends BasicRootPaneUI implements aIh {
    private ComponentManager Rp;

    public RootPaneUI(JRootPane var1) {
        this.Rp = new ComponentManager("rootpane", var1);
        var1.setOpaque(false);
        var1.setBorder(BorderWrapper.getInstance());
    }

    public static ComponentUI createUI(JComponent var0) {
        return new RootPaneUI((JRootPane) var0);
    }

    public ComponentManager wy() {
        return this.Rp;
    }

    public Dimension getPreferredSize(JComponent var1) {
        return this.wy().c(var1, super.getPreferredSize(var1));
    }

    public Dimension getMinimumSize(JComponent var1) {
        return this.wy().b(var1, super.getMinimumSize(var1));
    }

    public Dimension getMaximumSize(JComponent var1) {
        return this.wy().a(var1, super.getMaximumSize(var1));
    }

    public void paint(Graphics var1, JComponent var2) {
        RM.a((aIh) this, (Graphics) var1, (JComponent) var2);
        Border var3 = var2.getBorder();
        if (var3 != null) {
            var3.paintBorder(var2, var1, 0, 0, var2.getWidth(), var2.getHeight());
        }
    }

    /**
     * TODO похлже это public pv wy()
     *
     * @return Графический элемент
     */
    @Override
    public aeK wz() {
        return this.Rp;
    }
}
