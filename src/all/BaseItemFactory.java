package all;

import java.awt.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;

/**
 * Базовые элементы интерфейса
 * тег desktoppane JDesktopPane
 * class BaseUItegXML DesktoppaneCustom/ScrollableCustom  BaseItemFactory
 */
public abstract class BaseItemFactory extends BaseItem {
    private static final LogPrinter logger = LogPrinter.setClass(BaseItemFactory.class);

    public Container CreatAllComponentCustom(MapKeyValue var1, Container var2, XmlNode var3) {
        Container var4 = (Container) super.CreateUI(var1, var2, var3);
        this.CreatChildComponentCustom(var1, var4, var3);
        return var4;
    }

    protected void CreatChildComponentCustom(MapKeyValue var1, Container var2, XmlNode var3) {
        Iterator var5 = var3.getChildrenTag().iterator();

        while (var5.hasNext()) {
            XmlNode var4 = (XmlNode) var5.next();
            BaseItem var6 = var1.getClassBaseUi(var4.getTegName().toLowerCase());
            if (var6 != null) {
                Component var7 = var6.CreateUI(var1, var2, var4);
                if (var7 instanceof RepeaterCustomWrapper) {
                    if (var2 instanceof PanelCustomWrapper) {
                        ((PanelCustomWrapper) var2).addChild((RepeaterCustomWrapper) var7);
                    } else if (var2 instanceof RepeaterCustomWrapper) {
                        ((RepeaterCustomWrapper) var2).a((RepeaterCustomWrapper) var7);
                    } else if (var2 instanceof WindowCustomWrapper) {
                        ((WindowCustomWrapper) var2).a((RepeaterCustomWrapper) var7);
                    } else {
                        logger.error("Container for Repeater must be all Panel, Repeater or Window.");
                    }
                } else {
                    this.createLayoutData(var1, var2, var7, var4);
                }
            } else if ("import".equals(var4.getTegName())) {
                try {
                    URL var9 = new URL(var1.getPathFile(), var4.getAttribute("src"));
                    this.createLayoutData(var1, var2, var1.getBaseUiTegXml().CreateJComponent((new MapKeyValue(var1, var9)), (URL) var9), (XmlNode) null);
                } catch (MalformedURLException var8) {
                    throw new RuntimeException(var8);
                }
            } else {
                logger.error("There is test_GLCanvas component named '" + var4.getTegName() + "'");
            }
        }

    }

    protected void createLayout(MapKeyValue var1, Container var2, XmlNode var3) {
        super.applyAttribute(var1, (Component) var2, (XmlNode) var3);
        String var4 = var3.getAttribute("layout");
        if (var4 != null) {
            if ("null".equals(var4)) {
                var2.setLayout((LayoutManager) null);
            } else {
                var4 = var4.trim();
                String var5 = null;
                int var6 = -1;
                int var7 = 0;

                for (int var8 = var4.length(); var7 < var8; ++var7) {
                    char var9 = var4.charAt(var7);
                    if (var9 == '{' || var9 == ' ' || var9 == '(') {
                        var6 = var7;
                        break;
                    }
                }

                if (var6 != -1) {
                    var5 = var4.substring(var6);
                    var4 = var4.substring(0, var6).trim();
                }

                IBaseLayout var10 = var1.getClassLayoutUi(var4);
                if (var10 != null) {
                    LayoutManager var11 = var10.createLayout(var2, var5);
                    var2.setLayout(var11);
                    aeK var12 = ComponentManager.getCssHolder(var2);
                    if (var12 != null) {
                        var12.setAttribute("layoutManager", var4);
                    }
                }

            }
        }
    }

    protected void createLayoutData(MapKeyValue var1, Container var2, Component var3, XmlNode var4) {
        LayoutManager var5 = var2.getLayout();
        if (var4 != null && var5 != null) {
            String var6 = var4.getAttribute("layoutData");
            if (var6 != null) {
                aeK var7 = ComponentManager.getCssHolder(var2);
                if (var7 != null) {
                    String var8 = var7.getAttribute("layoutManager");
                    IBaseLayout var9 = var1.getClassLayoutUi(var8);
                    if (var9 != null) {
                        var2.add(var3, var9.createGridBagConstraints(var6));
                        return;
                    }
                }
            }

            var2.add(var3, var6);
            if (!var2.isFocusable()) {
                var3.setFocusable(false);
            }

            if (!var2.isEnabled()) {
                var3.setEnabled(false);
            }

        } else {
            var2.add(var3);
            var3.setFocusable(var2.isFocusable());
            var3.setEnabled(var2.isEnabled());
        }
    }
}
