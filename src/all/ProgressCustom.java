package all;

import javax.swing.*;
import java.awt.*;

/**
 *
 */
public class ProgressCustom extends BaseItem {
    private static final LogPrinter logger = LogPrinter.setClass(ProgressCustom.class);

    /**
     * Создание базового Component интерфейса
     * Вызывается из class BaseItem
     *
     * @param var1
     * @param var2
     * @param var3
     * @return
     */
    @Override
    protected JComponent CreatComponentCustom(MapKeyValue var1, Container var2, XmlNode var3) {
        return new ProgressBarCustomWrapper();
    }

    protected void a(MapKeyValue var1, ProgressBarCustomWrapper var2, XmlNode var3) {
        super.applyAttribute(var1, (Component) var2, (XmlNode) var3);
        Integer var4 = checkValueAttribut((XmlNode) var3, (String) "min", (LogPrinter) logger);
        if (var4 != null) {
            var2.setMinimum(var4.intValue());
        }

        var4 = checkValueAttribut((XmlNode) var3, (String) "max", (LogPrinter) logger);
        if (var4 != null) {
            var2.setMaximum(var4.intValue());
        }

        var4 = checkValueAttribut((XmlNode) var3, (String) "value", (LogPrinter) logger);
        if (var4 != null) {
            var2.setValue(var4.intValue());
        }

        if ("true".equals(var3.getAttribute("segmented"))) {
            var2.w(true);
            var4 = checkValueAttribut((XmlNode) var3, (String) "cell-distance", (LogPrinter) logger);
            if (var4 != null) {
                var2.W(var4.intValue());
            }

            var4 = checkValueAttribut((XmlNode) var3, (String) "num-cells", (LogPrinter) logger);
            if (var4 != null) {
                var2.V(var4.intValue());
                var2.x(false);
            }

            var4 = checkValueAttribut((XmlNode) var3, (String) "cell-width", (LogPrinter) logger);
            if (var4 != null) {
                var2.U(var4.intValue());
                var2.x(true);
            }
        }

    }
}
