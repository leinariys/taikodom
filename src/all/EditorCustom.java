package all;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.awt.*;

public final class EditorCustom extends TextComponentCustom {
    /**
     * Создание базового Component интерфейса
     * Вызывается из class BaseItem
     *
     * @param var1
     * @param var2
     * @param var3
     * @return
     */
    @Override
    protected JComponent CreatComponentCustom(MapKeyValue var1, Container var2, XmlNode var3) {
        return new EditorCustomWrapper();
    }

    protected void a(MapKeyValue var1, EditorCustomWrapper var2, XmlNode var3) {
        String var4 = var3.getAttribute("contentType");
        if (var4 != null) {
            var2.setContentType(var4);
        }

        super.a(var1, (JTextComponent) var2, var3);
        if ("false".equalsIgnoreCase(var3.getAttribute("editable"))) {
            var2.setEditable(false);
        }

        XmlNode var5 = var3.c(0, "text", (String) null, (String) null);
        if (var5 != null) {
            var2.setText(var5.getText());
        }

    }

}
