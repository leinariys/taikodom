package all;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map.Entry;

public class xa extends Zu implements ajJ_q {
    ajJ_q bFz;
    /**
     * Обработчик события
     */
    aVH bFA = new aVH() {
        public void k(String var1, Object var2) {
            xa.this.l(var1, var2);
        }

        public void f(String var1, Object var2) {
            xa.this.m(var1, var2);
        }
    };

    public xa(ajJ_q var1) {
        this.bFz = var1;
    }

    public void setxa(ajJ_q var1) {
        this.bFz = var1;
    }

    public void l(String var1, Object var2) {
        super.l(var1, var2);
    }

    public void m(String var1, Object var2) {
        super.m(var1, var2);
    }

    public void d(String var1, Object var2) {
        this.bFz.d(var1, var2);
    }

    public void h(Object var1) {
        this.bFz.h(var1);
    }

    public void publish(Object var1) {
        this.bFz.publish(var1);
    }

    public void a(String var1, Object var2) {
        this.bFz.a(var1, var2);
    }

    public void g(Object var1) {
        this.bFz.g(var1);
    }

    public void b(String var1, Object var2) {
        this.bFz.b(var1, var2);
    }

    /**
     * Добавить в список
     *
     * @param var1 Привер  endVideo из аддона SplashScreen
     * @param var2 Привер ExclusiveVideoPlayer
     * @param var3 Обработчик события
     */
    public void a(String var1, Class var2, aVH var3) {
        super.a(var1, var2, var3);
        this.bFz.a(var1, var2, this.bFA);
    }

    public void a(aVH var1) {
        Iterator var3 = this.eNC.entrySet().iterator();

        while (var3.hasNext()) {
            Entry var2 = (Entry) var3.next();
            if (((Collection) var2.getValue()).remove(var1) && ((Collection) var2.getValue()).size() == 0) {
                this.bFz.b((String) ((Zu.aMapse) var2.getKey()).getKey(), ((Zu.aMapse) var2.getKey()).getClazz(), this.bFA);
            }
        }

    }

    public void b(String var1, Class var2, aVH var3) {
        super.b(var1, var2, var3);
        Collection var4 = this.eNC.aR(new Zu.aMapse(var1, var2));
        if (var4 == null || var4.size() == 0) {
            this.bFz.b(var1, var2, this.bFA);
        }

    }

    public void dispose() {
        this.eNC.clear();
        this.bFz.a(this.bFA);
    }

    public Collection a(String var1, Class var2) {
        return this.bFz.a(var1, var2);
    }

    public Collection b(Class var1) {
        return this.a("", var1);
    }

    public Collection e(String var1, Object var2) {
        return this.bFz.e(var1, var2);
    }

    public Collection j(Object var1) {
        return this.e("", var1);
    }

    public void i(Object var1) {
        this.bFz.i(var1);
    }


    public void c(String var1, Object var2) {
        this.bFz.c(var1, var2);
    }
}
