package all;

import java.io.Serializable;

//Разделитель селекторов Пример picture.size4
public class ConditionalSelector implements IConditionalSelector, Serializable {
    private ISimpleSelector bek;//Пример picture
    private Condition bel;//Пример .size4

    public ConditionalSelector(ISimpleSelector var1, Condition var2) {
        this.bek = var1;
        this.bel = var2;
    }

    public short getSelectorType() {
        return 0;
    }

    public ISimpleSelector getSimpleSelector() {
        return this.bek;
    }

    public Condition getCondition() {
        return this.bel;
    }

    public String toString() {
        return this.bek.toString() + this.bel.toString();
    }
}
