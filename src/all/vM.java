package all;

public class vM {
    private static final boolean bzQ = true;
    private static final boolean bzR = false;
    aHt bzW;
    aHt bzX;
    aHt bzY;
    int size;
    private int bzS = 65536;
    private aHt bzT;
    private int bzU;
    private aMq bzV = new aMq(1024);
    private aWq bzZ;

    public vM() {
    }

    public vM(aWq var1) {
        this.bzZ = var1;
    }

    private static boolean e(aHt var0) {
        return var0 == null ? true : var0.hXm;
    }

    private static aHt f(aHt var0) {
        return var0 == null ? null : var0.hXi;
    }

    private static void a(aHt var0, boolean var1) {
        if (var0 != null) {
            var0.hXm = var1;
        }

    }

    private static aHt g(aHt var0) {
        return var0 == null ? null : var0.hXj;
    }

    private static aHt h(aHt var0) {
        return var0 == null ? null : var0.hXk;
    }

    public void clear() {
        int var1 = this.bzS;
        int var2 = this.bzU;
        aHt var3;
        if (var2 < var1) {
            var3 = this.bzT;

            aHt var4;
            for (var4 = this.bzX; var2 < var1 && var4 != null; ++var2) {
                aHt var5 = var4.hXl;
                if (var4.hXn) {
                    //var4.hXh.reset();
                    //this.bzV.put(var4.ehD.yn().getClassBaseUi(), var4.hXh);
                }

                var4.reset();
                var4.hXl = var3;
                var3 = var4;
                var4 = var5;
            }

            if (var4 != null) {
                while (var4 != null) {
                    if (var4.hXn) {
                        //var4.hXh.reset();
                        //  this.bzV.put(var4.ehD.yn().getClassBaseUi(), var4.hXh);
                    }

                    var4.reset();
                    var4 = var4.hXl;
                }
            }

            this.bzU = var2;
            this.bzT = var3;
        } else {
            for (var3 = this.bzX; var3 != null; var3 = var3.hXl) {
                var3.reset();
            }
        }

        this.bzW = this.bzX = this.bzY = null;
        this.size = 0;
    }

    public void a(aHt var1) {
        long var2 = var1.agR;
        aHt var4 = this.bzW;
        if (this.bzW == null) {
            this.bzW = var1;
            this.bzX = this.bzY = var1;
            this.size = 1;
        } else {
            aHt var5;
            byte var6;
            do {
                var5 = var4;
                long var7 = var4.agR;
                if (var2 < var7) {
                    var4 = var4.hXj;
                    var6 = -1;
                } else {
                    var4 = var4.hXk;
                    var6 = 1;
                }
            } while (var4 != null);

            var1.hXi = var5;
            if (var6 < 0) {
                var5.hXj = var1;
            } else {
                var5.hXk = var1;
            }

            this.b(var1);
            if (this.bzY != null) {
                this.bzY.hXl = var1;
                this.bzY = var1;
            } else {
                this.bzX = this.bzY = var1;
                var1.hXl = null;
            }

            ++this.size;
        }
    }

    public void akX() {
        aHt var1;
        for (var1 = this.bzW; var1.hXj != null; var1 = var1.hXj) {
            ;
        }

        this.bzX = var1;
        aHt var2 = var1;

        for (int var3 = 0; var1 != null; ++var3) {
            var2 = var1;
            if (var1.hXk != null) {
                for (var1 = var1.hXk; var1.hXj != null; var1 = var1.hXj) {
                    ;
                }
            } else {
                aHt var4 = var1;

                for (var1 = var1.hXi; var1 != null && var4 == var1.hXk; var1 = var1.hXi) {
                    var4 = var1;
                }
            }

            var2.hXl = var1;
        }

        this.bzY = var2;
        this.bzY.hXl = null;
    }

    public aHt a(long var1, af var3) {
        aHt var4 = this.bzW;
        aHt var5 = var4;

        while (var5 != null) {
            var4 = var5;
            long var6 = var5.agR;
            if (var1 < var6) {
                var5 = var5.hXj;
            } else {
                if (var1 <= var6) {
                    return var5;
                }

                var5 = var5.hXk;
            }
        }

        if (var3 != null && var3.du()) {
            return null;
        } else {
            var5 = this.b(var1, var3);
            if (var4 == null) {
                this.bzW = var5;
                this.bzX = this.bzY = var5;
            } else {
                if (var1 < var4.agR) {
                    var4.hXj = var5;
                } else {
                    var4.hXk = var5;
                }

                var5.hXi = var4;
                this.bzY.hXl = var5;
                this.bzY = var5;
                this.b(var5);
            }

            ++this.size;
            return var5;
        }
    }

    protected aHt b(long var1, af var3) {
        if (var3 == null) {
            throw new RuntimeException();
        } else {
            aHt var4;
            if (this.bzU > 0) {
                var4 = this.bzT;
                if ((this.bzT = var4.hXl) == null) {
                    this.bzU = 0;
                } else {
                    --this.bzU;
                }
            } else {
                var4 = new aHt();
            }
/*
         Jd var5 = (Jd)this.bzV.aN(var3.yn().getClassBaseUi());
         if (var5 == null) {
            var5 = (Jd)var3.yn().W();
         } else {
            var5.parseSelectors(var3.yn());
         }

         var5.all(this.bzZ, var3);
         */
            var4.a(this.bzZ, var3/*, var5*/);
            return var4;
        }
    }

    private void b(aHt var1) {
        var1.hXm = false;

        while (var1 != null && var1 != this.bzW && !var1.hXi.hXm) {
            aHt var2;
            if (f(var1) == g(f(f(var1)))) {
                var2 = h(f(f(var1)));
                if (!e(var2)) {
                    a(f(var1), true);
                    a(var2, true);
                    a(f(f(var1)), false);
                    var1 = f(f(var1));
                } else {
                    if (var1 == h(f(var1))) {
                        var1 = f(var1);
                        this.c(var1);
                    }

                    a(f(var1), true);
                    a(f(f(var1)), false);
                    this.d(f(f(var1)));
                }
            } else {
                var2 = g(f(f(var1)));
                if (!e(var2)) {
                    a(f(var1), true);
                    a(var2, true);
                    a(f(f(var1)), false);
                    var1 = f(f(var1));
                } else {
                    if (var1 == g(f(var1))) {
                        var1 = f(var1);
                        this.d(var1);
                    }

                    a(f(var1), true);
                    a(f(f(var1)), false);
                    this.c(f(f(var1)));
                }
            }
        }

        this.bzW.hXm = true;
    }

    private void c(aHt var1) {
        if (var1 != null) {
            aHt var2 = var1.hXk;
            var1.hXk = var2.hXj;
            if (var2.hXj != null) {
                var2.hXj.hXi = var1;
            }

            var2.hXi = var1.hXi;
            if (var1.hXi == null) {
                this.bzW = var2;
            } else if (var1.hXi.hXj == var1) {
                var1.hXi.hXj = var2;
            } else {
                var1.hXi.hXk = var2;
            }

            var2.hXj = var1;
            var1.hXi = var2;
        }

    }

    private void d(aHt var1) {
        if (var1 != null) {
            aHt var2 = var1.hXj;
            var1.hXj = var2.hXk;
            if (var2.hXk != null) {
                var2.hXk.hXi = var1;
            }

            var2.hXi = var1.hXi;
            if (var1.hXi == null) {
                this.bzW = var2;
            } else if (var1.hXi.hXk == var1) {
                var1.hXi.hXk = var2;
            } else {
                var1.hXi.hXj = var2;
            }

            var2.hXk = var1;
            var1.hXi = var2;
        }

    }

    public aHt akY() {
        return this.bzX;
    }

    public aHt akZ() {
        return this.bzY;
    }

    public int size() {
        return this.size;
    }
}
