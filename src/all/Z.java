package all;

import java.awt.*;

public class Z extends BorderWrapper {
    private Component component;

    public Z(Component var1) {
        this.component = var1;
    }

    public Insets getBorderInsets(Component var1) {
        return super.getBorderInsets(this.component);
    }

    public void paintBorder(Component var1, Graphics var2, int var3, int var4, int var5, int var6) {
        super.paintBorder(this.component, var2, var3, var4, var5, var6);
    }

    public Insets a(PropertiesUiFromCss var1, Component var2) {
        return super.a(var1, this.component);
    }
}
