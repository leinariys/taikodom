package all;

import java.io.Serializable;

public class To implements xr, Serializable {
    private Condition eiV;
    private Condition eiW;

    public To(Condition var1, Condition var2) {
        this.eiV = var1;
        this.eiW = var2;
    }

    public short getConditionType() {
        return 0;
    }

    public Condition anF() {
        return this.eiV;
    }

    public Condition anG() {
        return this.eiW;
    }

    public String toString() {
        return this.anF().toString() + this.anG().toString();
    }
}
