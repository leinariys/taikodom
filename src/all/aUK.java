package all;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.WeakHashMap;

public class aUK extends AbstractSet {
    private static final Object iXD = new Object();
    private WeakHashMap iXE = new WeakHashMap();

    public boolean add(Object var1) {
        return this.iXE.put(var1, iXD) != null;
    }

    public boolean remove(Object var1) {
        return this.iXE.remove(var1) != null;
    }

    public boolean contains(Object var1) {
        return this.iXE.containsKey(var1);
    }

    public Iterator iterator() {
        return this.iXE.keySet().iterator();
    }

    public int size() {
        return this.iXE.size();
    }
}
