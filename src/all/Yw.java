package all;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public final class Yw implements Serializable {
    private static final long serialVersionUID = 1L;
    private List eMg;
    private List eMh;
    private List eMi;
    private aQQb eMj;
    private int dby = 0;

    public aQQb bHC() {
        if (this.eMj == null) {
            this.eMj = new aQQb();
        }

        return this.eMj;
    }

    public void a(aQQb var1) {
        this.eMj = var1;
    }

    public List bHD() {
        if (this.eMg == null) {
            this.eMg = new LinkedList();
        }

        return this.eMg;
    }

    public int bHE() {
        return this.eMg == null ? 0 : this.eMg.size();
    }

    public int bHF() {
        return this.dby;
    }

    public List bHG() {
        if (this.eMi == null) {
            this.eMi = new ArrayList();
        }

        return this.eMi;
    }

    /*
       public void all(KO var1) {
          this.bHG().add(var1);
          ++this.dby;
       }

       public void all(AD var1) {
          this.bHD().add(var1);
       }
    */
    public boolean bHH() {
        return this.eMg != null;
    }

    public boolean bHI() {
        return this.eMi != null;
    }

    public void v(List var1) {
        this.eMg = var1;
    }

    public void w(List var1) {
        this.eMi = var1;
    }

    public boolean bHJ() {
        return this.eMh != null;
    }

    public List bHK() {
        if (this.eMh == null) {
            this.eMh = new ArrayList();
        }

        return this.eMh;
    }

    public void x(List var1) {
        this.eMh = var1;
    }
}
