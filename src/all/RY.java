package all;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;

import javax.vecmath.Tuple3f;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class RY extends yK {
    private static final long serialVersionUID = 1L;
    protected Vec3f[] ebY;
    protected int[] ebZ;

    public RY(Vec3f[] var1, int[] var2) {
        this.ebY = var1;
        this.ebZ = var2;
    }

    public Vec3f[] bsr() {
        return this.ebY;
    }

    public void a(Vec3f[] var1) {
        this.ebY = var1;
    }

    public int[] bss() {
        return this.ebZ;
    }

    public void b(int[] var1) {
        this.ebZ = var1;
    }

    public byte getShapeType() {
        return 4;
    }

    protected void growBoundingBox(ajK var1, BoundingBox var2) {
        Vec3f var3 = new Vec3f();
        Vec3f[] var7 = this.ebY;
        int var6 = this.ebY.length;

        for (int var5 = 0; var5 < var6; ++var5) {
            Vec3f var4 = var7[var5];
            var1.b((Tuple3f) var4, (Tuple3f) var3);
            var2.addPoint(var3);
        }

    }

    public void getFarthest(Vec3f var1, Vec3f var2) {
        Vec3f var3 = null;
        float var4 = Float.NEGATIVE_INFINITY;
        Vec3f[] var8;
        int var7 = (var8 = this.bsr()).length;

        for (int var6 = 0; var6 < var7; ++var6) {
            Vec3f var5 = var8[var6];
            float var9 = var1.dot(var5);
            if (var9 > var4) {
                var4 = var9;
                var3 = var5;
            }
        }

        if (var3 != null) {
            var2.set(var3);
        }

    }

    public RY toMesh() {
        return this;
    }

    public void getCenter(Vec3f var1) {
        Vec3f var2 = new Vec3f();
        Vec3f var3 = new Vec3f();
        Vec3f[] var7 = this.ebY;
        int var6 = this.ebY.length;

        for (int var5 = 0; var5 < var6; ++var5) {
            Vec3f var4 = var7[var5];
            if (var4.x < var3.x) {
                var3.x = var4.x;
            }

            if (var4.y < var3.y) {
                var3.y = var4.y;
            }

            if (var4.z < var3.z) {
                var3.z = var4.z;
            }

            if (var4.x > var2.x) {
                var2.x = var4.x;
            }

            if (var4.y > var2.y) {
                var2.y = var4.y;
            }

            if (var4.z > var2.z) {
                var2.z = var4.z;
            }
        }

        var1.x = var3.x + (var2.x - var3.x) / 2.0F;
        var1.y = var3.y + (var2.y - var3.y) / 2.0F;
        var1.z = var3.z + (var2.z - var3.z) / 2.0F;
    }

    private void writeObject(ObjectOutputStream var1) throws IOException {
        var1.writeInt(this.ebY.length);
        Vec3f[] var5 = this.ebY;
        int var4 = this.ebY.length;

        int var3;
        for (var3 = 0; var3 < var4; ++var3) {
            Vec3f var2 = var5[var3];
            var1.writeObject(var2);
        }

        var1.writeInt(this.ebZ.length);
        int[] var7 = this.ebZ;
        var4 = this.ebZ.length;

        for (var3 = 0; var3 < var4; ++var3) {
            int var6 = var7[var3];
            var1.writeInt(var6);
        }

    }

    private void readObject(ObjectInputStream var1) throws IOException, ClassNotFoundException {
        int var2 = var1.readInt();
        this.ebY = new Vec3f[var2];

        int var3;
        for (var3 = 0; var3 < var2; ++var3) {
            this.ebY[var3] = (Vec3f) var1.readObject();
        }

        var2 = var1.readInt();
        this.ebZ = new int[var2];

        for (var3 = 0; var3 < var2; ++var3) {
            this.ebZ[var3] = var1.readInt();
        }

    }

    public void calculateLocalInertia(float var1, Vec3f var2) {
    }

    public void getAabb(xf_g var1, Vec3f var2, Vec3f var3) {
    }

    public Vec3f getLocalScaling() {
        return null;
    }

    public void setLocalScaling(Vec3f var1) {
    }
}
