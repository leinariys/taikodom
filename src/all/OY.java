package all;

import com.hoplon.geometry.Vec3f;

public class OY extends aoW {
    public Vec3f h(float var1, float var2, float var3) {
        Vec3f var4 = (Vec3f) this.get();
        var4.set(var1, var2, var3);
        return var4;
    }

    public Vec3f ac(Vec3f var1) {
        Vec3f var2 = (Vec3f) this.get();
        var2.set(var1);
        return var2;
    }

    protected Vec3f bnc() {
        return new Vec3f();
    }

    protected void r(Vec3f var1, Vec3f var2) {
        var1.set(var2);
    }

    @Override
    protected Object create() {
        return null;
    }

    @Override
    protected void copy(Object var1, Object var2) {

    }
}
