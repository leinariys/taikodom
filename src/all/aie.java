package all;

import org.w3c.dom.css.CSSPageRule;
import org.w3c.dom.css.CSSRule;
import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.css.CSSStyleSheet;

import java.io.Serializable;
import java.io.StringReader;

public class aie implements Serializable, CSSPageRule {
    private SS qq = null;
    private CSSRule qr = null;
    private String fMO = null;
    private String fMP = null;
    private CSSStyleDeclaration fMQ = null;

    public aie(SS var1, CSSRule var2, String var3, String var4) {
        this.qq = var1;
        this.qr = var2;
        this.fMO = var3;
        this.fMP = var4;
    }

    public short getType() {
        return 6;
    }

    public String getCssText() {
        String var1 = this.getSelectorText();
        return "@page " + var1 + (var1.length() > 0 ? " " : "") + this.getStyle().getCssText();
    }

    public void setCssText(String var1) {
        if (this.qq != null && this.qq.isReadOnly()) {
            throw new qN((short) 7, 2);
        } else {
            try {
                InputSource var2 = new InputSource(new StringReader(var1));
                ParserCss var3 = ParserCss.getParserCss();
                CSSRule var4 = var3.f(var2);
                if (var4.getType() == 6) {
                    this.fMO = ((aie) var4).fMO;
                    this.fMP = ((aie) var4).fMP;
                    this.fMQ = ((aie) var4).fMQ;
                } else {
                    throw new qN((short) 13, 9);
                }
            } catch (CSSException var5) {
                throw new qN((short) 12, 0, var5.getMessage());
            }/* catch (IOException var6) {
            throw new qN((short)12, 0, var6.getMessage());
         }*/
        }
    }

    public CSSStyleSheet getParentStyleSheet() {
        return this.qq;
    }

    public CSSRule getParentRule() {
        return this.qr;
    }

    public String getSelectorText() {
        return (this.fMO != null ? this.fMO : "") + (this.fMP != null ? ":" + this.fMP : "");
    }

    public void setSelectorText(String var1) {
    }

    public CSSStyleDeclaration getStyle() {
        return this.fMQ;
    }

    protected void iN(String var1) {
        this.fMO = var1;
    }

    protected void iO(String var1) {
        this.fMP = var1;
    }

    public void a(aHx var1) {
        this.fMQ = var1;
    }
}
