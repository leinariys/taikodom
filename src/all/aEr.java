package all;

public class aEr {
    private static final int hFv = 4;
    private int[] hFw;
    private int size = 0;

    public aEr() {
        this.resize(16);
    }

    public static int cXz() {
        return 16;
    }

    public static int i(long var0, int var2) {
        switch (var2) {
            case 0:
            default:
                return (int) (var0 & 65535L) & '\uffff';
            case 1:
                return (int) ((var0 & 4294901760L) >>> 16) & '\uffff';
            case 2:
                return (int) ((var0 & 281470681743360L) >>> 32) & '\uffff';
        }
    }

    public int cXy() {
        while (this.size + 1 >= this.capacity()) {
            this.resize(this.capacity() * 2);
        }

        return this.size++;
    }

    public int size() {
        return this.size;
    }

    public int capacity() {
        return this.hFw.length / 4;
    }

    public void clear() {
        this.size = 0;
    }

    public void resize(int var1) {
        int[] var2 = this.hFw;
        this.hFw = new int[var1 * 4];
        if (var2 != null) {
            System.arraycopy(var2, 0, this.hFw, 0, Math.min(var2.length, this.hFw.length));
        }

    }

    public void a(int var1, aEr var2, int var3) {
        int[] var4 = this.hFw;
        int[] var5 = var2.hFw;
        var4[var1 * 4 + 0] = var5[var3 * 4 + 0];
        var4[var1 * 4 + 1] = var5[var3 * 4 + 1];
        var4[var1 * 4 + 2] = var5[var3 * 4 + 2];
        var4[var1 * 4 + 3] = var5[var3 * 4 + 3];
    }

    public void swap(int var1, int var2) {
        int[] var3 = this.hFw;
        int var4 = var3[var1 * 4 + 0];
        int var5 = var3[var1 * 4 + 1];
        int var6 = var3[var1 * 4 + 2];
        int var7 = var3[var1 * 4 + 3];
        var3[var1 * 4 + 0] = var3[var2 * 4 + 0];
        var3[var1 * 4 + 1] = var3[var2 * 4 + 1];
        var3[var1 * 4 + 2] = var3[var2 * 4 + 2];
        var3[var1 * 4 + 3] = var3[var2 * 4 + 3];
        var3[var2 * 4 + 0] = var4;
        var3[var2 * 4 + 1] = var5;
        var3[var2 * 4 + 2] = var6;
        var3[var2 * 4 + 3] = var7;
    }

    public int az(int var1, int var2) {
        switch (var2) {
            case 0:
            default:
                return this.hFw[var1 * 4 + 0] & '\uffff';
            case 1:
                return this.hFw[var1 * 4 + 0] >>> 16 & '\uffff';
            case 2:
                return this.hFw[var1 * 4 + 1] & '\uffff';
        }
    }

    public long xE(int var1) {
        return (long) this.hFw[var1 * 4 + 0] & 4294967295L | ((long) this.hFw[var1 * 4 + 1] & 65535L) << 32;
    }

    public void i(int var1, long var2) {
        this.hFw[var1 * 4 + 0] = (int) var2;
        this.z(var1, 2, (short) ((int) ((var2 & 281470681743360L) >>> 32)));
    }

    public void j(int var1, long var2) {
        this.A(var1, 0, (short) ((int) var2));
        this.hFw[var1 * 4 + 2] = (int) (var2 >>> 16);
    }

    public void z(int var1, int var2, int var3) {
        switch (var2) {
            case 0:
                this.hFw[var1 * 4 + 0] = this.hFw[var1 * 4 + 0] & -65536 | var3 & '\uffff';
                break;
            case 1:
                this.hFw[var1 * 4 + 0] = this.hFw[var1 * 4 + 0] & '\uffff' | (var3 & '\uffff') << 16;
                break;
            case 2:
                this.hFw[var1 * 4 + 1] = this.hFw[var1 * 4 + 1] & -65536 | var3 & '\uffff';
        }

    }

    public int aA(int var1, int var2) {
        switch (var2) {
            case 0:
            default:
                return this.hFw[var1 * 4 + 1] >>> 16 & '\uffff';
            case 1:
                return this.hFw[var1 * 4 + 2] & '\uffff';
            case 2:
                return this.hFw[var1 * 4 + 2] >>> 16 & '\uffff';
        }
    }

    public long xF(int var1) {
        return ((long) this.hFw[var1 * 4 + 1] & 4294901760L) >>> 16 | ((long) this.hFw[var1 * 4 + 2] & 4294967295L) << 16;
    }

    public void A(int var1, int var2, int var3) {
        switch (var2) {
            case 0:
                this.hFw[var1 * 4 + 1] = this.hFw[var1 * 4 + 1] & '\uffff' | (var3 & '\uffff') << 16;
                break;
            case 1:
                this.hFw[var1 * 4 + 2] = this.hFw[var1 * 4 + 2] & -65536 | var3 & '\uffff';
                break;
            case 2:
                this.hFw[var1 * 4 + 2] = this.hFw[var1 * 4 + 2] & '\uffff' | (var3 & '\uffff') << 16;
        }

    }

    public int xG(int var1) {
        return this.hFw[var1 * 4 + 3];
    }

    public void aB(int var1, int var2) {
        this.hFw[var1 * 4 + 3] = var2;
    }

    public boolean xH(int var1) {
        return this.xG(var1) >= 0;
    }

    public int xI(int var1) {
        assert !this.xH(var1);

        return -this.xG(var1);
    }

    public int xJ(int var1) {
        assert this.xH(var1);

        return this.xG(var1) & 2097151;
    }

    public int xK(int var1) {
        assert this.xH(var1);

        return this.xG(var1) >>> 21;
    }
}
