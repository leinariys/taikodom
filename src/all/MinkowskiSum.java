package all;

import com.hoplon.geometry.Vec3f;

public class MinkowskiSum extends aMa {
    private final xf_g hQl = new xf_g();
    private final xf_g hQm = new xf_g();
    private azD hQn;
    private azD hQo;

    public MinkowskiSum(azD var1, azD var2) {
        this.hQn = var1;
        this.hQo = var2;
        this.hQl.setIdentity();
        this.hQm.setIdentity();
    }

    public Vec3f localGetSupportingVertexWithoutMargin(Vec3f var1) {
        this.stack.bcH().push();

        Vec3f var7;
        try {
            Vec3f var2 = (Vec3f) this.stack.bcH().get();
            Vec3f var3 = (Vec3f) this.stack.bcH().get();
            Vec3f var4 = (Vec3f) this.stack.bcH().get();
            rS.a(var2, var1, this.hQl.bFF);
            var3.set(this.hQn.localGetSupportingVertexWithoutMargin(var2));
            this.hQl.G(var3);
            rS.a(var2, var1, this.hQm.bFF);
            var4.set(this.hQo.localGetSupportingVertexWithoutMargin(var2));
            this.hQm.G(var4);
            Vec3f var5 = (Vec3f) this.stack.bcH().get();
            var5.add(var3, var4);
            var7 = (Vec3f) this.stack.bcH().aq(var5);
        } finally {
            this.stack.bcH().pop();
        }

        return var7;
    }

    public void batchedUnitVectorGetSupportingVertexWithoutMargin(Vec3f[] var1, Vec3f[] var2, int var3) {
        for (int var4 = 0; var4 < var3; ++var4) {
            var2[var4].set(this.localGetSupportingVertexWithoutMargin(var1[var4]));
        }

    }

    public void getAabb(xf_g var1, Vec3f var2, Vec3f var3) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public tK arV() {
        return tK.bsN;
    }

    public void calculateLocalInertia(float var1, Vec3f var2) {
        assert false;

        var2.set(0.0F, 0.0F, 0.0F);
    }

    public String getName() {
        return "MinkowskiSum";
    }

    public float getMargin() {
        return this.hQn.getMargin() + this.hQo.getMargin();
    }

    public void k(xf_g var1) {
        this.hQl.a(var1);
    }

    public void l(xf_g var1) {
        this.hQm.a(var1);
    }

    public void m(xf_g var1) {
        var1.a(this.hQl);
    }

    public void n(xf_g var1) {
        var1.a(this.hQm);
    }
}
