package all;

import gnu.trove.TObjectIntHashMap;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.awt.*;

public class FormattedFieldCustom extends TextComponentCustom {
    private static final LogPrinter logger = LogPrinter.setClass(FormattedFieldCustomWrapper.class);
    private static TObjectIntHashMap model = new TObjectIntHashMap();

    static {
        model.put("LEFT", 2);
        model.put("CENTER", 0);
        model.put("RIGHT", 4);
        model.put("LEADING", 10);
        model.put("TRAILING", 11);
    }

    /**
     * Создание базового Component интерфейса
     * Вызывается из class BaseItem
     *
     * @param var1
     * @param var2
     * @param var3
     * @return
     */
    @Override
    protected JComponent CreatComponentCustom(MapKeyValue var1, Container var2, XmlNode var3) {
        return new FormattedFieldCustomWrapper();
    }

    protected void a(MapKeyValue var1, FormattedFieldCustomWrapper var2, XmlNode var3) {
        super.a(var1, (JTextComponent) var2, var3);
        var2.setFormat(var3.getAttribute("format"));
        Integer var4 = checkValueAttribut((XmlNode) var3, (String) "cols", (LogPrinter) logger);
        if (var4 != null) {
            var2.setColumns(var4.intValue());
        }

        String var5 = var3.getAttribute("horizontalAlignment");
        if (var5 != null) {
            var2.setHorizontalAlignment(model.get(var5.toUpperCase()));
        }

    }
}
