package all;

public class ju {
    private int akl;
    private float akm;
    private int height;
    private String path;
    private int version;
    private int width;

    public ju(String var1, int var2, int var3, int var4, float var5, int var6) {
        this.path = var1;
        this.width = var2;
        this.height = var3;
        this.akl = var4;
        this.akm = var5;
        this.version = var6;
    }

    public int Fq() {
        return this.akl;
    }

    public float Fr() {
        return this.akm;
    }

    public int getHeight() {
        return this.height;
    }

    public void setHeight(int var1) {
        this.height = var1;
    }

    public String getPath() {
        return this.path;
    }

    public int getVersion() {
        return this.version;
    }

    public int getWidth() {
        return this.width;
    }

    public void setWidth(int var1) {
        this.width = var1;
    }
}
