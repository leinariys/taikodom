package all;

import javax.swing.*;
import javax.swing.plaf.basic.BasicButtonListener;
import java.awt.event.MouseEvent;

//aGt.java
public class ButtonUIListener extends BasicButtonListener {
    private static final BaseUItegXML baseUItegXML = BaseUItegXML.thisClass;
    private final ComponentManager componentManager;

    public ButtonUIListener(AbstractButton var1, ComponentManager var2) {
        super(var1);
        this.componentManager = var2;
    }

    public void mouseClicked(MouseEvent var1) {
        super.mouseClicked(var1);
        baseUItegXML.cx(this.componentManager.Vr().atX());
    }

    public void mouseEntered(MouseEvent var1) {
        super.mouseEntered(var1);
        baseUItegXML.e(this.componentManager.Vr().atU(), false);
    }

    public void mousePressed(MouseEvent var1) {
        super.mousePressed(var1);
        baseUItegXML.cx(this.componentManager.Vr().atV());
    }

    public void mouseReleased(MouseEvent var1) {
        super.mouseReleased(var1);
        baseUItegXML.cx(this.componentManager.Vr().atW());
    }
}
