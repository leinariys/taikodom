package all;

import java.util.Enumeration;
import java.util.Locale;
import java.util.Vector;

public class ParseSpecificationCSS2 implements ads, IParser {
    private final int[] evp;
    private final int[] evq;
    private final int[] evr;
    private final int[] evs;
    private final a[] evt;
    public abV evg;
    public zV evh;
    public zV evi;
    public boolean lookingAhead;
    private InputSource fileCss;
    private Locale _locale;
    private DocumentHandler evb;
    private ErrorHandler evc;
    private SelectorFactory evd;
    private IConditionFactory eve;
    private boolean evf;
    private int evj;
    private zV evk;
    private zV evl;
    private int evm;
    private boolean evn;
    private int evo;
    private boolean evu;
    private int evv;
    private Vector evw;
    private int[] evx;
    private int evy;
    private int[] evz;
    private int evA;

    public ParseSpecificationCSS2() {
        this((ho) null);
    }

    public ParseSpecificationCSS2(ho var1) {
        this.fileCss = null;
        this._locale = null;
        this.evb = null;
        this.evc = null;
        this.evd = new sT_q();
        this.eve = new ConditionFactory();
        this.evf = true;
        this.lookingAhead = false;
        this.evp = new int[95];
        this.evq = new int[]{0, 50331650, 50331650, 268435456, 50331650, 50331650, -536212224, -536212224, 50331650, 50331650, -267776768, 2, 2, 2, 9437184, 2, 0, 2, 2, 537529600, 128, 2, 537529600, 2, 537529600, 537529600, 2, 2, 2, 2, 2, 1024, 1024, 2, 0, 512, 2, 0, 2, 2, 0, 512, 2, 0, 2, 2, 4224, 2, 2, 73728, 2, 73728, 73730, 24576, 2, 2, 0, 512, 2, 0, 128, 2, 2, 656640, 656640, 656640, 656640, 658688, 2048, 2, 2, 201359360, 2, 1048576, 2, 201359360, 2, 2, 0, 2, 0, 512, 2, 0, 2, 0, 2, 9990272, 4224, 24576, 0, 9961472, 2, 2, 2};
        this.evr = new int[]{1, 0, 0, 0, 0, 0, 16777218, 16777218, 0, 0, 16777219, 0, 0, 0, 0, 0, 16777216, 0, 0, 16777218, 0, 0, 16777218, 0, 16777218, 16777218, 0, 0, 0, 0, 0, 16777216, 16777216, 0, 16777216, 0, 0, 16777216, 0, 0, 16777216, 0, 0, 16777216, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 16777216, 0, 0, 16777216, 0, 0, 0, 0, 0, 0, 0, 16777216, 16777216, 0, 0, 0, 0, 16777216, 0, 0, 0, 0, 25165824, 0, 16777216, 0, 0, 16777216, 0, 4, 0, 167772152, 0, 0, 12058608, 167772152, 0, 0, 0};
        this.evs = new int[95];
        this.evt = new a[2];
        this.evu = false;
        this.evv = 0;
        this.evw = new Vector();
        this.evy = -1;
        this.evz = new int[100];
        this.evg = new abV(var1);
        this.evh = new zV();
        this.evj = -1;
        this.evo = 0;

        int var2;
        for (var2 = 0; var2 < 95; ++var2) {
            this.evp[var2] = -1;
        }

        for (var2 = 0; var2 < this.evt.length; ++var2) {
            this.evt[var2] = new a();
        }

    }

    public ParseSpecificationCSS2(abV var1) {
        this.fileCss = null;
        this._locale = null;
        this.evb = null;
        this.evc = null;
        this.evd = new sT_q();
        this.eve = new ConditionFactory();
        this.evf = true;
        this.lookingAhead = false;
        this.evp = new int[95];
        this.evq = new int[]{0, 50331650, 50331650, 268435456, 50331650, 50331650, -536212224, -536212224, 50331650, 50331650, -267776768, 2, 2, 2, 9437184, 2, 0, 2, 2, 537529600, 128, 2, 537529600, 2, 537529600, 537529600, 2, 2, 2, 2, 2, 1024, 1024, 2, 0, 512, 2, 0, 2, 2, 0, 512, 2, 0, 2, 2, 4224, 2, 2, 73728, 2, 73728, 73730, 24576, 2, 2, 0, 512, 2, 0, 128, 2, 2, 656640, 656640, 656640, 656640, 658688, 2048, 2, 2, 201359360, 2, 1048576, 2, 201359360, 2, 2, 0, 2, 0, 512, 2, 0, 2, 0, 2, 9990272, 4224, 24576, 0, 9961472, 2, 2, 2};
        this.evr = new int[]{1, 0, 0, 0, 0, 0, 16777218, 16777218, 0, 0, 16777219, 0, 0, 0, 0, 0, 16777216, 0, 0, 16777218, 0, 0, 16777218, 0, 16777218, 16777218, 0, 0, 0, 0, 0, 16777216, 16777216, 0, 16777216, 0, 0, 16777216, 0, 0, 16777216, 0, 0, 16777216, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 16777216, 0, 0, 16777216, 0, 0, 0, 0, 0, 0, 0, 16777216, 16777216, 0, 0, 0, 0, 16777216, 0, 0, 0, 0, 25165824, 0, 16777216, 0, 0, 16777216, 0, 4, 0, 167772152, 0, 0, 12058608, 167772152, 0, 0, 0};
        this.evs = new int[95];
        this.evt = new a[2];
        this.evu = false;
        this.evv = 0;
        this.evw = new Vector();
        this.evy = -1;
        this.evz = new int[100];
        this.evg = var1;
        this.evh = new zV();
        this.evj = -1;
        this.evo = 0;

        int var2;
        for (var2 = 0; var2 < 95; ++var2) {
            this.evp[var2] = -1;
        }

        for (var2 = 0; var2 < this.evt.length; ++var2) {
            this.evt[var2] = new a();
        }

    }

    public void setLocale(Locale var1) {
        this._locale = var1;
        throw new CSSException(CSSException.SAC_NOT_SUPPORTED_ERR);
    }

    public void setDocumentHandler(DocumentHandler var1) {
        this.evb = var1;
    }

    public void setSelectorFactory(SelectorFactory var1) {
        this.evd = var1;
    }

    public void setConditionFactory(IConditionFactory var1) {
        this.eve = var1;
    }

    public void setErrorHandler(ErrorHandler var1) {
        this.evc = var1;
    }

    public void parseStyleSheet(InputSource var1) {
        this.fileCss = var1;
        this.a(this.m(var1));

        try {
            this.bBB();
        } catch (aNQ var3) {
            throw new CSSException(CSSException.SAC_SYNTAX_ERR, var3.getMessage(), var3);
        }
    }

    public void parseStyleSheet(String var1) {
        this.parseStyleSheet(new InputSource(var1));
    }

    public void parseStyleDeclaration(InputSource var1) {
        this.fileCss = var1;
        this.a(this.m(var1));

        try {
            this.bBV();
        } catch (aNQ var3) {
            throw new CSSException(CSSException.SAC_SYNTAX_ERR, var3.getMessage(), var3);
        }
    }

    public void parseRule(InputSource var1) {
        this.fileCss = var1;
        this.a(this.m(var1));

        try {
            this.bBD();
        } catch (aNQ var3) {
            throw new CSSException(CSSException.SAC_SYNTAX_ERR, var3.getMessage(), var3);
        }
    }

    public String getParserVersion() {
        return "http://www.w3.org/TR/REC-CSS2";
    }

    public SelectorList parseSelectors(InputSource var1) {
        this.fileCss = var1;
        this.a(this.m(var1));
        SelectorList var2 = null;

        try {
            var2 = this.bBS();
            return var2;
        } catch (aNQ var4) {
            throw new CSSException(CSSException.SAC_SYNTAX_ERR, var4.getMessage(), var4);
        }
    }

    public LexicalUnit parsePropertyValue(InputSource var1) {
        this.fileCss = var1;
        this.a(this.m(var1));
        LexicalUnit var2 = (LexicalUnit) null;

        try {
            var2 = this.bBY();
            return var2;
        } catch (aNQ var4) {
            throw new CSSException(CSSException.SAC_SYNTAX_ERR, var4.getMessage(), var4);
        }
    }

    public boolean parsePriority(InputSource var1) {
        this.fileCss = var1;
        this.a(this.m(var1));
        boolean var2 = false;

        try {
            var2 = this.bBX();
            return var2;
        } catch (aNQ var4) {
            throw new CSSException(CSSException.SAC_SYNTAX_ERR, var4.getMessage(), var4);
        }
    }

    private ho m(InputSource var1) {
        return var1.getCharacterStream() != null ? new OW(var1.getCharacterStream(), 1, 1) : null;
    }

    private Locator bBA() {
        return null;
    }

    public final void bBB() throws aNQ {
        try {
            this.evb.startDocument(this.fileCss);
            this.bBC();
            this.oO(0);
        } finally {
            this.evb.endDocument(this.fileCss);
        }

    }

    public final void bBC() throws aNQ {
        switch (this.evj == -1 ? this.bCF() : this.evj) {
            case 32:
                this.bBE();
                break;
            default:
                this.evp[0] = this.evo;
        }

        while (true) {
            switch (this.evj == -1 ? this.bCF() : this.evj) {
                case 1:
                case 24:
                case 25:
                    switch (this.evj == -1 ? this.bCF() : this.evj) {
                        case 1:
                            this.oO(1);
                            continue;
                        case 24:
                            this.oO(24);
                            continue;
                        case 25:
                            this.oO(25);
                            continue;
                        default:
                            this.evp[2] = this.evo;
                            this.oO(-1);
                            throw new aNQ();
                    }
                default:
                    this.evp[1] = this.evo;

                    label120:
                    while (true) {
                        switch (this.evj == -1 ? this.bCF() : this.evj) {
                            case 28:
                                this.bBG();

                                while (true) {
                                    switch (this.evj == -1 ? this.bCF() : this.evj) {
                                        case 1:
                                        case 24:
                                        case 25:
                                            switch (this.evj == -1 ? this.bCF() : this.evj) {
                                                case 1:
                                                    this.oO(1);
                                                    continue;
                                                case 24:
                                                    this.oO(24);
                                                    continue;
                                                case 25:
                                                    this.oO(25);
                                                    continue;
                                                default:
                                                    this.evp[5] = this.evo;
                                                    this.oO(-1);
                                                    throw new aNQ();
                                            }
                                        default:
                                            this.evp[4] = this.evo;
                                            continue label120;
                                    }
                                }
                            default:
                                this.evp[3] = this.evo;

                                label108:
                                while (true) {
                                    switch (this.evj == -1 ? this.bCF() : this.evj) {
                                        case 8:
                                        case 10:
                                        case 11:
                                        case 17:
                                        case 19:
                                        case 29:
                                        case 30:
                                        case 31:
                                        case 33:
                                        case 56:
                                            switch (this.evj == -1 ? this.bCF() : this.evj) {
                                                case 8:
                                                case 10:
                                                case 11:
                                                case 17:
                                                case 19:
                                                case 56:
                                                    this.bBR();
                                                    break;
                                                case 29:
                                                    this.bBL();
                                                    break;
                                                case 30:
                                                    this.bBH();
                                                    break;
                                                case 31:
                                                    this.bBN();
                                                    break;
                                                case 33:
                                                    this.bBF();
                                                    break;
                                                default:
                                                    this.evp[7] = this.evo;
                                                    this.oO(-1);
                                                    throw new aNQ();
                                            }

                                            while (true) {
                                                switch (this.evj == -1 ? this.bCF() : this.evj) {
                                                    case 1:
                                                    case 24:
                                                    case 25:
                                                        switch (this.evj == -1 ? this.bCF() : this.evj) {
                                                            case 1:
                                                                this.oO(1);
                                                                continue;
                                                            case 24:
                                                                this.oO(24);
                                                                continue;
                                                            case 25:
                                                                this.oO(25);
                                                                continue;
                                                            default:
                                                                this.evp[9] = this.evo;
                                                                this.oO(-1);
                                                                throw new aNQ();
                                                        }
                                                    default:
                                                        this.evp[8] = this.evo;
                                                        continue label108;
                                                }
                                            }
                                        default:
                                            this.evp[6] = this.evo;
                                            return;
                                    }
                                }
                        }
                    }
            }
        }
    }

    public final void bBD() throws aNQ {
        switch (this.evj == -1 ? this.bCF() : this.evj) {
            case 8:
            case 10:
            case 11:
            case 17:
            case 19:
            case 56:
                this.bBR();
                break;
            case 28:
                this.bBG();
                break;
            case 29:
                this.bBL();
                break;
            case 30:
                this.bBH();
                break;
            case 31:
                this.bBN();
                break;
            case 32:
                this.bBE();
                break;
            case 33:
                this.bBF();
                break;
            default:
                this.evp[10] = this.evo;
                this.oO(-1);
                throw new aNQ();
        }

    }

    public final void bBE() throws aNQ {
        this.oO(32);

        while (true) {
            switch (this.evj == -1 ? this.bCF() : this.evj) {
                case 1:
                    this.oO(1);
                    break;
                default:
                    this.evp[11] = this.evo;
                    this.oO(20);

                    while (true) {
                        switch (this.evj == -1 ? this.bCF() : this.evj) {
                            case 1:
                                this.oO(1);
                                break;
                            default:
                                this.evp[12] = this.evo;
                                this.oO(9);
                                return;
                        }
                    }
            }
        }
    }

    public final void bBF() throws aNQ {
        this.oO(33);
        String var1 = this.bCa();
        this.evb.ignorableAtRule(var1);
    }

    public final void bBG() throws aNQ {
        asq var2 = new asq();
        this.oO(28);

        while (true) {
            switch (this.evj == -1 ? this.bCF() : this.evj) {
                case 1:
                    this.oO(1);
                    break;
                default:
                    this.evp[13] = this.evo;
                    zV var1;
                    switch (this.evj == -1 ? this.bCF() : this.evj) {
                        case 20:
                            var1 = this.oO(20);
                            break;
                        case 21:
                        case 22:
                        default:
                            this.evp[14] = this.evo;
                            this.oO(-1);
                            throw new aNQ();
                        case 23:
                            var1 = this.oO(23);
                    }

                    while (true) {
                        switch (this.evj == -1 ? this.bCF() : this.evj) {
                            case 1:
                                this.oO(1);
                                break;
                            default:
                                this.evp[15] = this.evo;
                                switch (this.evj == -1 ? this.bCF() : this.evj) {
                                    case 56:
                                        this.a(var2);
                                        break;
                                    default:
                                        this.evp[16] = this.evo;
                                }

                                this.oO(9);
                                this.evb.importStyle(this.unescape(var1.image), var2, (String) null);
                                return;
                        }
                    }
            }
        }
    }

    public final void bBH() throws aNQ {
        boolean var1 = false;
        asq var2 = new asq();

        try {
            this.oO(30);

            while (true) {
                switch (this.evj == -1 ? this.bCF() : this.evj) {
                    case 1:
                        this.oO(1);
                        break;
                    default:
                        this.evp[17] = this.evo;
                        this.a(var2);
                        var1 = true;
                        this.evb.startMedia((SACMediaList) var2);
                        this.oO(5);

                        while (true) {
                            switch (this.evj == -1 ? this.bCF() : this.evj) {
                                case 1:
                                    this.oO(1);
                                    break;
                                default:
                                    this.evp[18] = this.evo;
                                    switch (this.evj == -1 ? this.bCF() : this.evj) {
                                        case 8:
                                        case 10:
                                        case 11:
                                        case 17:
                                        case 19:
                                        case 29:
                                        case 33:
                                        case 56:
                                            this.bBI();
                                            break;
                                        default:
                                            this.evp[19] = this.evo;
                                    }

                                    this.oO(6);
                                    return;
                            }
                        }
                }
            }
        } finally {
            if (var1) {
                this.evb.endMedia((SACMediaList) var2);
            }

        }
    }

    public final void a(asq var1) throws aNQ {
        String var2 = this.bBK();

        label31:
        while (true) {
            switch (this.evj == -1 ? this.bCF() : this.evj) {
                case 7:
                    this.oO(7);

                    while (true) {
                        switch (this.evj == -1 ? this.bCF() : this.evj) {
                            case 1:
                                this.oO(1);
                                break;
                            default:
                                this.evp[21] = this.evo;
                                var1.add(var2);
                                var2 = this.bBK();
                                continue label31;
                        }
                    }
                default:
                    this.evp[20] = this.evo;
                    var1.add(var2);
                    return;
            }
        }
    }

    public final void bBI() throws aNQ {
        label38:
        while (true) {
            switch (this.evj == -1 ? this.bCF() : this.evj) {
                case 8:
                case 10:
                case 11:
                case 17:
                case 19:
                case 56:
                    this.bBR();
                    break;
                case 29:
                    this.bBL();
                    break;
                case 33:
                    this.bBF();
                    break;
                default:
                    this.evp[22] = this.evo;
                    this.oO(-1);
                    throw new aNQ();
            }

            while (true) {
                switch (this.evj == -1 ? this.bCF() : this.evj) {
                    case 1:
                        this.oO(1);
                        break;
                    default:
                        this.evp[23] = this.evo;
                        switch (this.evj == -1 ? this.bCF() : this.evj) {
                            case 8:
                            case 10:
                            case 11:
                            case 17:
                            case 19:
                            case 29:
                            case 33:
                            case 56:
                                continue label38;
                            default:
                                this.evp[24] = this.evo;
                                return;
                        }
                }
            }
        }
    }

    public final void bBJ() throws aNQ {
        switch (this.evj == -1 ? this.bCF() : this.evj) {
            case 8:
            case 10:
            case 11:
            case 17:
            case 19:
            case 56:
                this.bBR();
                break;
            case 29:
                this.bBL();
                break;
            case 33:
                this.bBF();
                break;
            default:
                this.evp[25] = this.evo;
                this.oO(-1);
                throw new aNQ();
        }

    }

    public final String bBK() throws aNQ {
        zV var1 = this.oO(56);

        while (true) {
            switch (this.evj == -1 ? this.bCF() : this.evj) {
                case 1:
                    this.oO(1);
                    break;
                default:
                    this.evp[26] = this.evo;
                    return var1.image;
            }
        }
    }

    public final void bBL() throws aNQ {
        zV var1 = null;
        String var2 = null;
        boolean var3 = false;

        try {
            this.oO(29);

            while (true) {
                switch (this.evj == -1 ? this.bCF() : this.evj) {
                    case 1:
                        this.oO(1);
                        break;
                    default:
                        this.evp[27] = this.evo;
                        label348:
                        switch (this.evj == -1 ? this.bCF() : this.evj) {
                            case 10:
                            case 56:
                                if (this.oM(2)) {
                                    var1 = this.oO(56);

                                    while (true) {
                                        switch (this.evj == -1 ? this.bCF() : this.evj) {
                                            case 1:
                                                this.oO(1);
                                                break;
                                            default:
                                                this.evp[28] = this.evo;
                                                break label348;
                                        }
                                    }
                                } else {
                                    switch (this.evj == -1 ? this.bCF() : this.evj) {
                                        case 10:
                                            var2 = this.bBM();

                                            while (true) {
                                                switch (this.evj == -1 ? this.bCF() : this.evj) {
                                                    case 1:
                                                        this.oO(1);
                                                        break;
                                                    default:
                                                        this.evp[30] = this.evo;
                                                        break label348;
                                                }
                                            }
                                        case 56:
                                            var1 = this.oO(56);
                                            var2 = this.bBM();

                                            while (true) {
                                                switch (this.evj == -1 ? this.bCF() : this.evj) {
                                                    case 1:
                                                        this.oO(1);
                                                        break;
                                                    default:
                                                        this.evp[29] = this.evo;
                                                        break label348;
                                                }
                                            }
                                        default:
                                            this.evp[31] = this.evo;
                                            this.oO(-1);
                                            throw new aNQ();
                                    }
                                }
                            default:
                                this.evp[32] = this.evo;
                        }

                        this.oO(5);

                        while (true) {
                            switch (this.evj == -1 ? this.bCF() : this.evj) {
                                case 1:
                                    this.oO(1);
                                    break;
                                default:
                                    this.evp[33] = this.evo;
                                    var3 = true;
                                    this.evb.startPage(var1 != null ? this.unescape(var1.image) : null, var2);
                                    switch (this.evj == -1 ? this.bCF() : this.evj) {
                                        case 56:
                                            this.bBW();
                                            break;
                                        default:
                                            this.evp[34] = this.evo;
                                    }

                                    label315:
                                    while (true) {
                                        switch (this.evj == -1 ? this.bCF() : this.evj) {
                                            case 9:
                                                this.oO(9);

                                                while (true) {
                                                    switch (this.evj == -1 ? this.bCF() : this.evj) {
                                                        case 1:
                                                            this.oO(1);
                                                            break;
                                                        default:
                                                            this.evp[36] = this.evo;
                                                            switch (this.evj == -1 ? this.bCF() : this.evj) {
                                                                case 56:
                                                                    this.bBW();
                                                                    continue label315;
                                                                default:
                                                                    this.evp[37] = this.evo;
                                                                    continue label315;
                                                            }
                                                    }
                                                }
                                            default:
                                                this.evp[35] = this.evo;
                                                this.oO(6);
                                                return;
                                        }
                                    }
                            }
                        }
                }
            }
        } finally {
            if (var3) {
                this.evb.endPage(var1 != null ? this.unescape(var1.image) : null, var2);
            }

        }
    }

    public final String bBM() throws aNQ {
        this.oO(10);
        zV var1 = this.oO(56);
        return var1.image;
    }

    public final void bBN() throws aNQ {
        boolean var1 = false;

        try {
            this.oO(31);

            while (true) {
                switch (this.evj == -1 ? this.bCF() : this.evj) {
                    case 1:
                        this.oO(1);
                        break;
                    default:
                        this.evp[38] = this.evo;
                        this.oO(5);

                        while (true) {
                            switch (this.evj == -1 ? this.bCF() : this.evj) {
                                case 1:
                                    this.oO(1);
                                    break;
                                default:
                                    this.evp[39] = this.evo;
                                    var1 = true;
                                    this.evb.startFontFace();
                                    switch (this.evj == -1 ? this.bCF() : this.evj) {
                                        case 56:
                                            this.bBW();
                                            break;
                                        default:
                                            this.evp[40] = this.evo;
                                    }

                                    label196:
                                    while (true) {
                                        switch (this.evj == -1 ? this.bCF() : this.evj) {
                                            case 9:
                                                this.oO(9);

                                                while (true) {
                                                    switch (this.evj == -1 ? this.bCF() : this.evj) {
                                                        case 1:
                                                            this.oO(1);
                                                            break;
                                                        default:
                                                            this.evp[42] = this.evo;
                                                            switch (this.evj == -1 ? this.bCF() : this.evj) {
                                                                case 56:
                                                                    this.bBW();
                                                                    continue label196;
                                                                default:
                                                                    this.evp[43] = this.evo;
                                                                    continue label196;
                                                            }
                                                    }
                                                }
                                            default:
                                                this.evp[41] = this.evo;
                                                this.oO(6);
                                                return;
                                        }
                                    }
                            }
                        }
                }
            }
        } finally {
            if (var1) {
                this.evb.endFontFace();
            }

        }
    }

    public final LexicalUnit b(LexicalUnit var1) throws aNQ {
        switch (this.evj == -1 ? this.bCF() : this.evj) {
            case 7:
                this.oO(7);

                while (true) {
                    switch (this.evj == -1 ? this.bCF() : this.evj) {
                        case 1:
                            this.oO(1);
                            break;
                        default:
                            this.evp[45] = this.evo;
                            return new LexicalUnit(var1, (short) 0);
                    }
                }
            case 12:
                this.oO(12);

                while (true) {
                    switch (this.evj == -1 ? this.bCF() : this.evj) {
                        case 1:
                            this.oO(1);
                            break;
                        default:
                            this.evp[44] = this.evo;
                            return new LexicalUnit(var1, (short) 4);
                    }
                }
            default:
                this.evp[46] = this.evo;
                this.oO(-1);
                throw new aNQ();
        }
    }

    public final char bBO() throws aNQ {
        char var1 = ' ';
        switch (this.evj == -1 ? this.bCF() : this.evj) {
            case 1:
                this.oO(1);
                switch (this.evj == -1 ? this.bCF() : this.evj) {
                    case 13:
                    case 16:
                        switch (this.evj == -1 ? this.bCF() : this.evj) {
                            case 13:
                                this.oO(13);
                                var1 = '+';
                                break;
                            case 14:
                            case 15:
                            default:
                                this.evp[49] = this.evo;
                                this.oO(-1);
                                throw new aNQ();
                            case 16:
                                this.oO(16);
                                var1 = '>';
                        }

                        while (true) {
                            switch (this.evj == -1 ? this.bCF() : this.evj) {
                                case 1:
                                    this.oO(1);
                                    break;
                                default:
                                    this.evp[50] = this.evo;
                                    return var1;
                            }
                        }
                    case 14:
                    case 15:
                    default:
                        this.evp[51] = this.evo;
                        return var1;
                }
            case 13:
                this.oO(13);
                var1 = '+';

                while (true) {
                    switch (this.evj == -1 ? this.bCF() : this.evj) {
                        case 1:
                            this.oO(1);
                            break;
                        default:
                            this.evp[47] = this.evo;
                            return var1;
                    }
                }
            case 16:
                this.oO(16);
                var1 = '>';

                while (true) {
                    switch (this.evj == -1 ? this.bCF() : this.evj) {
                        case 1:
                            this.oO(1);
                            break;
                        default:
                            this.evp[48] = this.evo;
                            return var1;
                    }
                }
            default:
                this.evp[52] = this.evo;
                this.oO(-1);
                throw new aNQ();
        }
    }

    public final char bBP() throws aNQ {
        switch (this.evj == -1 ? this.bCF() : this.evj) {
            case 13:
                this.oO(13);
                return '+';
            case 14:
                this.oO(14);
                return '-';
            default:
                this.evp[53] = this.evo;
                this.oO(-1);
                throw new aNQ();
        }
    }

    public final String bBQ() throws aNQ {
        zV var1 = this.oO(56);

        while (true) {
            switch (this.evj == -1 ? this.bCF() : this.evj) {
                case 1:
                    this.oO(1);
                    break;
                default:
                    this.evp[54] = this.evo;
                    return this.unescape(var1.image);
            }
        }
    }

    public final void bBR() throws aNQ {
        SelectorList var1 = null;
        boolean var2 = false;

        try {
            var1 = this.bBS();
            this.oO(5);

            while (true) {
                switch (this.evj == -1 ? this.bCF() : this.evj) {
                    case 1:
                        this.oO(1);
                        break;
                    default:
                        this.evp[55] = this.evo;
                        var2 = true;
                        this.evb.startSelector(var1);
                        switch (this.evj == -1 ? this.bCF() : this.evj) {
                            case 56:
                                this.bBW();
                                break;
                            default:
                                this.evp[56] = this.evo;
                        }

                        label168:
                        while (true) {
                            switch (this.evj == -1 ? this.bCF() : this.evj) {
                                case 9:
                                    this.oO(9);

                                    while (true) {
                                        switch (this.evj == -1 ? this.bCF() : this.evj) {
                                            case 1:
                                                this.oO(1);
                                                break;
                                            default:
                                                this.evp[58] = this.evo;
                                                switch (this.evj == -1 ? this.bCF() : this.evj) {
                                                    case 56:
                                                        this.bBW();
                                                        continue label168;
                                                    default:
                                                        this.evp[59] = this.evo;
                                                        continue label168;
                                                }
                                        }
                                    }
                                default:
                                    this.evp[57] = this.evo;
                                    this.oO(6);
                                    return;
                            }
                        }
                }
            }
        } finally {
            if (var2) {
                this.evb.endSelector(var1);
            }

        }

    }

    public final SelectorList bBS() throws aNQ {
        ayE var1 = new ayE();

        ISelector var2 = this.bBT();

        label38:
        while (true) {
            switch (this.evj == -1 ? this.bCF() : this.evj) {
                case 7:
                    this.oO(7);

                    while (true) {
                        switch (this.evj == -1 ? this.bCF() : this.evj) {
                            case 1:
                                this.oO(1);
                                break;
                            default:
                                this.evp[61] = this.evo;
                                var1.add(var2);
                                var2 = this.bBT();
                                continue label38;
                        }
                    }
                default:
                    this.evp[60] = this.evo;
                    var1.add(var2);
                    return var1;
            }
        }
    }

    public final ISelector bBT() {
        try {
            ISelector var1;
            char var2;
            for (var1 = this.a((ISelector) null, ' '); this.oN(2); var1 = this.a(var1, var2)) {
                var2 = this.bBO();
            }

            while (true) {
                switch (this.evj == -1 ? this.bCF() : this.evj) {
                    case 1:
                        this.oO(1);
                        break;
                    default:
                        this.evp[62] = this.evo;
                        return var1;
                }
            }
        } catch (aNQ var3) {
            this.bBZ();
            throw new Error("Missing return statement in function");
        }
    }

    public final ISelector a(ISelector var1, char var2) throws aNQ {
        Object var3;
        Condition var4;
        var3 = null;
        var4 = null;
        label86:
        switch (this.evj == -1 ? this.bCF() : this.evj) {
            case 8:
            case 10:
            case 17:
            case 19:
                var3 = this.evd.createElementSelector((String) null, (String) null);

                while (true) {
                    switch (this.evj == -1 ? this.bCF() : this.evj) {
                        case 8:
                            var4 = this.b(var4);
                            break;
                        case 10:
                            var4 = this.d(var4);
                            break;
                        case 17:
                            var4 = this.c(var4);
                            break;
                        case 19:
                            var4 = this.e(var4);
                            break;
                        default:
                            this.evp[65] = this.evo;
                            this.oO(-1);
                            throw new aNQ();
                    }

                    switch (this.evj == -1 ? this.bCF() : this.evj) {
                        case 8:
                        case 10:
                        case 17:
                        case 19:
                            break;
                        default:
                            this.evp[66] = this.evo;
                            break label86;
                    }
                }
            case 11:
            case 56:
                var3 = this.bBU();

                while (true) {
                    switch (this.evj == -1 ? this.bCF() : this.evj) {
                        case 8:
                        case 10:
                        case 17:
                        case 19:
                            switch (this.evj == -1 ? this.bCF() : this.evj) {
                                case 8:
                                    var4 = this.b(var4);
                                    continue;
                                case 10:
                                    var4 = this.d(var4);
                                    continue;
                                case 17:
                                    var4 = this.c(var4);
                                    continue;
                                case 19:
                                    var4 = this.e(var4);
                                    continue;
                                default:
                                    this.evp[64] = this.evo;
                                    this.oO(-1);
                                    throw new aNQ();
                            }
                        default:
                            this.evp[63] = this.evo;
                            break label86;
                    }
                }
            default:
                this.evp[67] = this.evo;
                this.oO(-1);
                throw new aNQ();
        }

        if (var4 != null) {
            var3 = this.evd.createConditionalSelector((ISimpleSelector) var3, (Condition) var4);
        }

        if (var1 != null) {
            switch (var2) {
                case ' ':
                    var1 = this.evd.createDescendantSelector((ISelector) var1, (ISimpleSelector) var3);
                    break;
                case '+':
                    var1 = this.evd.createDirectAdjacentSelector(((ISelector) var1).getSelectorType(), (ISelector) var1, (ISimpleSelector) var3);
                    break;
                case '>':
                    var1 = this.evd.createChildSelector((ISelector) var1, (ISimpleSelector) var3);
            }
        } else {
            var1 = (ISelector)/*(ElementSelector)*/var3;
        }

        return (ISelector) var1;
    }

    public final Condition b(Condition var1) throws aNQ {
        this.oO(8);
        zV var2 = this.oO(56);
        akt_q var3 = this.eve.D((String) null, var2.image);
        return (Condition) (var1 == null ? var3 : this.eve.a(var1, var3));
    }

    public final ISimpleSelector bBU() throws aNQ {
        switch (this.evj == -1 ? this.bCF() : this.evj) {
            case 11:
                this.oO(11);
                return this.evd.createElementSelector((String) null, (String) null);
            case 56:
                zV var1 = this.oO(56);
                return this.evd.createElementSelector((String) null, this.unescape(var1.image));
            default:
                this.evp[68] = this.evo;
                this.oO(-1);
                throw new aNQ();
        }
    }

    public final Condition c(Condition var1) throws aNQ {
        String var3 = null;
        String var4 = null;
        byte var5 = 0;
        this.oO(17);

        while (true) {
            switch (this.evj == -1 ? this.bCF() : this.evj) {
                case 1:
                    this.oO(1);
                    break;
                default:
                    this.evp[69] = this.evo;
                    zV var2 = this.oO(56);
                    var3 = this.unescape(var2.image);

                    while (true) {
                        switch (this.evj == -1 ? this.bCF() : this.evj) {
                            case 1:
                                this.oO(1);
                                break;
                            default:
                                this.evp[70] = this.evo;
                                label96:
                                switch (this.evj == -1 ? this.bCF() : this.evj) {
                                    case 15:
                                    case 26:
                                    case 27:
                                        switch (this.evj == -1 ? this.bCF() : this.evj) {
                                            case 15:
                                                this.oO(15);
                                                var5 = 1;
                                                break;
                                            case 26:
                                                this.oO(26);
                                                var5 = 2;
                                                break;
                                            case 27:
                                                this.oO(27);
                                                var5 = 3;
                                                break;
                                            default:
                                                this.evp[71] = this.evo;
                                                this.oO(-1);
                                                throw new aNQ();
                                        }

                                        while (true) {
                                            switch (this.evj == -1 ? this.bCF() : this.evj) {
                                                case 1:
                                                    this.oO(1);
                                                    break;
                                                default:
                                                    this.evp[72] = this.evo;
                                                    switch (this.evj == -1 ? this.bCF() : this.evj) {
                                                        case 20:
                                                            var2 = this.oO(20);
                                                            var4 = this.unescape(var2.image);
                                                            break;
                                                        case 56:
                                                            var2 = this.oO(56);
                                                            var4 = var2.image;
                                                            break;
                                                        default:
                                                            this.evp[73] = this.evo;
                                                            this.oO(-1);
                                                            throw new aNQ();
                                                    }

                                                    while (true) {
                                                        switch (this.evj == -1 ? this.bCF() : this.evj) {
                                                            case 1:
                                                                this.oO(1);
                                                                break;
                                                            default:
                                                                this.evp[74] = this.evo;
                                                                break label96;
                                                        }
                                                    }
                                            }
                                        }
                                    default:
                                        this.evp[75] = this.evo;
                                }

                                this.oO(18);
                                akt_q var6 = null;
                                switch (var5) {
                                    case 0:
                                        var6 = this.eve.a(var3, (String) null, false, (String) null);
                                        break;
                                    case 1:
                                        var6 = this.eve.a(var3, (String) null, false, var4);
                                        break;
                                    case 2:
                                        var6 = this.eve.b(var3, (String) null, false, var4);
                                        break;
                                    case 3:
                                        var6 = this.eve.c(var3, (String) null, false, var4);
                                }

                                return (Condition) (var1 == null ? var6 : this.eve.a(var1, var6));
                        }
                    }
            }
        }
    }

    public final Condition d(Condition var1) throws aNQ {
        this.oO(10);
        zV var3;
        switch (this.evj == -1 ? this.bCF() : this.evj) {
            case 55:
                var3 = this.oO(55);
                String var4 = this.unescape(var3.image);

                while (true) {
                    switch (this.evj == -1 ? this.bCF() : this.evj) {
                        case 1:
                            this.oO(1);
                            break;
                        default:
                            this.evp[76] = this.evo;
                            var3 = this.oO(56);
                            String var5 = this.unescape(var3.image);

                            while (true) {
                                switch (this.evj == -1 ? this.bCF() : this.evj) {
                                    case 1:
                                        this.oO(1);
                                        break;
                                    default:
                                        this.evp[77] = this.evo;
                                        this.oO(21);
                                        if (var4.equalsIgnoreCase("lang(")) {
                                            aIb var7 = this.eve.eM(this.unescape(var5));
                                            return (Condition) (var1 == null ? var7 : this.eve.a(var1, var7));
                                        }

                                        throw new CSSParseException("Invalid pseudo function name " + var4, this.bBA());
                                }
                            }
                    }
                }
            case 56:
                var3 = this.oO(56);
                String var6 = this.unescape(var3.image);
                akt_q var2 = this.eve.E((String) null, var6);
                return (Condition) (var1 == null ? var2 : this.eve.a(var1, var2));
            default:
                this.evp[78] = this.evo;
                this.oO(-1);
                throw new aNQ();
        }
    }

    public final Condition e(Condition var1) throws aNQ {
        zV var2 = this.oO(19);
        akt_q var3 = this.eve.eL(var2.image.substring(1));
        return (Condition) (var1 == null ? var3 : this.eve.a(var1, var3));
    }

    public final void bBV() throws aNQ {
        this.oO(5);

        while (true) {
            switch (this.evj == -1 ? this.bCF() : this.evj) {
                case 1:
                    this.oO(1);
                    break;
                default:
                    this.evp[79] = this.evo;
                    switch (this.evj == -1 ? this.bCF() : this.evj) {
                        case 56:
                            this.bBW();
                            break;
                        default:
                            this.evp[80] = this.evo;
                    }

                    label57:
                    while (true) {
                        switch (this.evj == -1 ? this.bCF() : this.evj) {
                            case 9:
                                this.oO(9);

                                while (true) {
                                    switch (this.evj == -1 ? this.bCF() : this.evj) {
                                        case 1:
                                            this.oO(1);
                                            break;
                                        default:
                                            this.evp[82] = this.evo;
                                            switch (this.evj == -1 ? this.bCF() : this.evj) {
                                                case 56:
                                                    this.bBW();
                                                    continue label57;
                                                default:
                                                    this.evp[83] = this.evo;
                                                    continue label57;
                                            }
                                    }
                                }
                            default:
                                this.evp[81] = this.evo;
                                this.oO(6);
                                return;
                        }
                    }
            }
        }
    }

    public final void bBW() {
        boolean var3 = false;

        try {
            String var1 = this.bBQ();
            this.oO(10);

            while (true) {
                switch (this.evj == -1 ? this.bCF() : this.evj) {
                    case 1:
                        this.oO(1);
                        break;
                    default:
                        this.evp[84] = this.evo;
                        LexicalUnit var2 = this.bBY();
                        switch (this.evj == -1 ? this.bCF() : this.evj) {
                            case 34:
                                var3 = this.bBX();
                                break;
                            default:
                                this.evp[85] = this.evo;
                        }

                        this.evb.property(var1, var2, var3);
                        return;
                }
            }
        } catch (aNQ var4) {
            this.bCc();
        }
    }

    public final boolean bBX() throws aNQ {
        this.oO(34);

        while (true) {
            switch (this.evj == -1 ? this.bCF() : this.evj) {
                case 1:
                    this.oO(1);
                    break;
                default:
                    this.evp[86] = this.evo;
                    return true;
            }
        }
    }

    public final LexicalUnit bBY() throws aNQ {
        LexicalUnit var1 = this.c((LexicalUnit) null);
        LexicalUnit var2 = var1;

        while (true) {
            switch (this.evj == -1 ? this.bCF() : this.evj) {
                case 7:
                case 12:
                case 13:
                case 14:
                case 19:
                case 20:
                case 23:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49:
                case 50:
                case 51:
                case 52:
                case 53:
                case 54:
                case 55:
                case 56:
                case 59:
                    switch (this.evj == -1 ? this.bCF() : this.evj) {
                        case 7:
                        case 12:
                            var2 = this.b(var2);
                            break;
                        default:
                            this.evp[88] = this.evo;
                    }

                    var2 = this.c(var2);
                    break;
                case 8:
                case 9:
                case 10:
                case 11:
                case 15:
                case 16:
                case 17:
                case 18:
                case 21:
                case 22:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 57:
                case 58:
                default:
                    this.evp[87] = this.evo;
                    return var1;
            }
        }
    }

    public final LexicalUnit c(LexicalUnit var1) throws aNQ {
        char var3 = ' ';
        Object var4 = null;
        switch (this.evj == -1 ? this.bCF() : this.evj) {
            case 13:
            case 14:
                var3 = this.bBP();
                break;
            default:
                this.evp[89] = this.evo;
        }

        zV var2;
        label60:
        switch (this.evj == -1 ? this.bCF() : this.evj) {
            case 19:
                var4 = this.f(var1);
                break;
            case 20:
                var2 = this.oO(20);
                var4 = new LexicalUnit(var1, (short) 36, var2.image);
                break;
            case 21:
            case 22:
            case 24:
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
            case 57:
            case 58:
            default:
                this.evp[91] = this.evo;
                this.oO(-1);
                throw new aNQ();
            case 23:
                var2 = this.oO(23);
                var4 = new LexicalUnit(var1, (short) 24, var2.image);
                break;
            case 35:
                var2 = this.oO(35);
                var4 = new LexicalUnit(var1, (short) 12, var2.image);
                break;
            case 36:
            case 37:
            case 38:
            case 39:
            case 40:
            case 41:
            case 42:
            case 43:
            case 44:
            case 45:
            case 46:
            case 47:
            case 48:
            case 49:
            case 50:
            case 52:
            case 53:
            case 55:
                switch (this.evj == -1 ? this.bCF() : this.evj) {
                    case 36:
                        var2 = this.oO(36);
                        var4 = LexicalUnit.i(var1, this.a(var3, var2.image));
                        break label60;
                    case 37:
                        var2 = this.oO(37);
                        var4 = LexicalUnit.j(var1, this.a(var3, var2.image));
                        break label60;
                    case 38:
                        var2 = this.oO(38);
                        var4 = LexicalUnit.c(var1, this.a(var3, var2.image));
                        break label60;
                    case 39:
                        var2 = this.oO(39);
                        var4 = LexicalUnit.d(var1, this.a(var3, var2.image));
                        break label60;
                    case 40:
                        var2 = this.oO(40);
                        var4 = LexicalUnit.e(var1, this.a(var3, var2.image));
                        break label60;
                    case 41:
                        var2 = this.oO(41);
                        var4 = LexicalUnit.f(var1, this.a(var3, var2.image));
                        break label60;
                    case 42:
                        var2 = this.oO(42);
                        var4 = LexicalUnit.g(var1, this.a(var3, var2.image));
                        break label60;
                    case 43:
                        var2 = this.oO(43);
                        var4 = LexicalUnit.h(var1, this.a(var3, var2.image));
                        break label60;
                    case 44:
                        var2 = this.oO(44);
                        var4 = LexicalUnit.k(var1, this.a(var3, var2.image));
                        break label60;
                    case 45:
                        var2 = this.oO(45);
                        var4 = LexicalUnit.l(var1, this.a(var3, var2.image));
                        break label60;
                    case 46:
                        var2 = this.oO(46);
                        var4 = LexicalUnit.m(var1, this.a(var3, var2.image));
                        break label60;
                    case 47:
                        var2 = this.oO(47);
                        var4 = LexicalUnit.n(var1, this.a(var3, var2.image));
                        break label60;
                    case 48:
                        var2 = this.oO(48);
                        var4 = LexicalUnit.o(var1, this.a(var3, var2.image));
                        break label60;
                    case 49:
                        var2 = this.oO(49);
                        var4 = LexicalUnit.p(var1, this.a(var3, var2.image));
                        break label60;
                    case 50:
                        var2 = this.oO(50);
                        var4 = LexicalUnit.q(var1, this.a(var3, var2.image));
                        break label60;
                    case 51:
                    case 54:
                    default:
                        this.evp[90] = this.evo;
                        this.oO(-1);
                        throw new aNQ();
                    case 52:
                        var2 = this.oO(52);
                        var4 = LexicalUnit.b(var1, this.a(var3, var2.image));
                        break label60;
                    case 53:
                        var2 = this.oO(53);
                        var4 = LexicalUnit.perseValueColor(var1, this.a(var3, var2.image));
                        break label60;
                    case 55:
                        var4 = this.d(var1);
                        break label60;
                }
            case 51:
                var2 = this.oO(51);
                int var5 = this.hm(var2.image);
                var4 = LexicalUnit.a(var1, this.a(var3, var2.image.substring(0, var5 + 1)), var2.image.substring(var5 + 1));
                break;
            case 54:
                var4 = this.e(var1);
                break;
            case 56:
                var2 = this.oO(56);
                var4 = new LexicalUnit(var1, (short) 35, var2.image);
                break;
            case 59:
                var2 = this.oO(59);
                var4 = new LexicalUnit(var1, (short) 39, var2.image);
        }

        while (true) {
            switch (this.evj == -1 ? this.bCF() : this.evj) {
                case 1:
                    this.oO(1);
                    break;
                default:
                    this.evp[92] = this.evo;
                    return (LexicalUnit) var4;
            }
        }
    }

    public final LexicalUnit d(LexicalUnit var1) throws aNQ {
        zV var2 = this.oO(55);

        while (true) {
            switch (this.evj == -1 ? this.bCF() : this.evj) {
                case 1:
                    this.oO(1);
                    break;
                default:
                    this.evp[93] = this.evo;
                    LexicalUnit var3 = this.bBY();
                    this.oO(21);
                    if (var2.image.equalsIgnoreCase("counter(")) {
                        return LexicalUnit.a(var1, var3);
                    }

                    if (var2.image.equalsIgnoreCase("counters(")) {
                        return LexicalUnit.b(var1, var3);
                    }

                    if (var2.image.equalsIgnoreCase("attr(")) {
                        return LexicalUnit.c(var1, var3);
                    }

                    if (var2.image.equalsIgnoreCase("rect(")) {
                        return LexicalUnit.d(var1, var3);
                    }

                    return LexicalUnit.a(var1, var2.image.substring(0, var2.image.length() - 1), var3);
            }
        }
    }

    public final LexicalUnit e(LexicalUnit var1) throws aNQ {
        this.oO(54);

        while (true) {
            switch (this.evj == -1 ? this.bCF() : this.evj) {
                case 1:
                    this.oO(1);
                    break;
                default:
                    this.evp[94] = this.evo;
                    LexicalUnit var2 = this.bBY();
                    this.oO(21);
                    return LexicalUnit.e(var1, var2);
            }
        }
    }

    public final LexicalUnit f(LexicalUnit var1) throws aNQ {
        zV var2 = this.oO(19);
        byte var3 = 1;
        int var4 = 0;
        int var5 = 0;
        int var6 = 0;
        int var7 = var2.image.length() - 1;
        if (var7 == 3) {
            var4 = Integer.parseInt(var2.image.substring(var3 + 0, var3 + 1), 16);
            var5 = Integer.parseInt(var2.image.substring(var3 + 1, var3 + 2), 16);
            var6 = Integer.parseInt(var2.image.substring(var3 + 2, var3 + 3), 16);
            var4 |= var4 << 4;
            var5 |= var5 << 4;
            var6 |= var6 << 4;
        } else if (var7 == 6) {
            var4 = Integer.parseInt(var2.image.substring(var3 + 0, var3 + 2), 16);
            var5 = Integer.parseInt(var2.image.substring(var3 + 2, var3 + 4), 16);
            var6 = Integer.parseInt(var2.image.substring(var3 + 4, var3 + 6), 16);
        }

        LexicalUnit var8 = LexicalUnit.perseValueColor((LexicalUnit) null, (float) var4);
        LexicalUnit var9 = LexicalUnit.a(var8);
        LexicalUnit var10 = LexicalUnit.perseValueColor(var9, (float) var5);
        LexicalUnit var11 = LexicalUnit.a(var10);
        LexicalUnit.perseValueColor(var11, (float) var6);
        return LexicalUnit.e(var1, var8);
    }

    float a(char var1, String var2) {
        return (float) (var1 == '-' ? -1 : 1) * Float.parseFloat(var2);
    }

    int hm(String var1) {
        int var2;
        for (var2 = 0; var2 < var1.length() && !Character.isLetter(var1.charAt(var2)); ++var2) {
            ;
        }

        return var2 - 1;
    }

    String unescape(String var1) {
        int var2 = var1.length();
        StringBuffer var3 = new StringBuffer(var2);

        for (int var4 = 0; var4 < var2; ++var4) {
            char var5 = var1.charAt(var4);
            if (var5 != '\\') {
                var3.append(var5);
            } else {
                ++var4;
                if (var4 >= var2) {
                    throw new CSSParseException("invalid string " + var1, this.bBA());
                }

                var5 = var1.charAt(var4);
                switch (var5) {
                    case '\n':
                    case '\f':
                        break;
                    case '\r':
                        if (var4 + 1 < var2 && var1.charAt(var4 + 1) == '\n') {
                            ++var4;
                        }
                        break;
                    case '0':
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                    case 'A':
                    case 'B':
                    case 'C':
                    case 'D':
                    case 'E':
                    case 'F':
                    case 'a':
                    case 'b':
                    case 'c':
                    case 'd':
                    case 'e':
                    case 'f':
                        int var6 = Character.digit(var5, 16);
                        byte var7 = 0;

                        for (int var8 = 16; var4 + 1 < var2 && var7 < 6; ++var4) {
                            var5 = var1.charAt(var4 + 1);
                            if (Character.digit(var5, 16) == -1) {
                                if (var5 == ' ') {
                                    ++var4;
                                }
                                break;
                            }

                            var6 = var6 * 16 + Character.digit(var5, 16);
                            var8 *= 16;
                        }

                        var3.append((char) var6);
                        break;
                    default:
                        var3.append(var5);
                }
            }
        }

        return var3.toString();
    }

    void bBZ() {
        for (zV var1 = this.oQ(1); var1.kind != 7 && var1.kind != 9 && var1.kind != 5 && var1.kind != 0; var1 = this.oQ(1)) {
            this.bCE();
        }

    }

    String bCa() {
        StringBuffer var1 = new StringBuffer();
        int var2 = 0;
        zV var3 = this.oQ(0);
        if (var3.image != null) {
            var1.append(var3.image);
        }

        do {
            var3 = this.bCE();
            if (var3.kind == 0) {
                break;
            }

            var1.append(var3.image);
            if (var3.kind == 5) {
                ++var2;
            } else if (var3.kind == 6) {
                --var2;
            } else if (var3.kind == 9 && var2 <= 0) {
                break;
            }
        } while (var3.kind != 6 || var2 > 0);

        return var1.toString();
    }

    void bCb() {
        if (!this.evf) {
            aNQ var1 = this.bCG();
            System.err.println("** error_skipblock **\n" + var1.toString());
        }

        int var2 = 0;

        zV var3;
        do {
            var3 = this.bCE();
            if (var3.kind == 5) {
                ++var2;
            } else if (var3.kind == 6) {
                --var2;
            } else if (var3.kind == 0) {
                break;
            }
        } while (var3.kind != 6 || var2 > 0);

    }

    void bCc() {
        if (!this.evf) {
            aNQ var1 = this.bCG();
            System.err.println("** error_skipdecl **\n" + var1.toString());
        }

        for (zV var2 = this.oQ(1); var2.kind != 9 && var2.kind != 6 && var2.kind != 0; var2 = this.oQ(1)) {
            this.bCE();
        }

    }

    private final boolean oM(int var1) {
        this.evm = var1;
        this.evl = this.evk = this.evh;
        boolean var2 = !this.bCD();
        this.V(0, var1);
        return var2;
    }

    private final boolean oN(int var1) {
        this.evm = var1;
        this.evl = this.evk = this.evh;
        boolean var2 = !this.bCd();
        this.V(1, var1);
        return var2;
    }

    private final boolean bCd() {
        if (this.bCs()) {
            return true;
        } else if (this.evm == 0 && this.evk == this.evl) {
            return false;
        } else if (this.bCy()) {
            return true;
        } else {
            return this.evm == 0 && this.evk == this.evl ? false : false;
        }
    }

    private final boolean bCe() {
        if (this.oP(13)) {
            return true;
        } else {
            return this.evm == 0 && this.evk == this.evl ? false : false;
        }
    }

    private final boolean bCf() {
        zV var1 = this.evk;
        if (this.bCe()) {
            this.evk = var1;
            if (this.bCw()) {
                return true;
            }

            if (this.evm == 0 && this.evk == this.evl) {
                return false;
            }
        } else if (this.evm == 0 && this.evk == this.evl) {
            return false;
        }

        return false;
    }

    private final boolean bCg() {
        if (this.oP(11)) {
            return true;
        } else {
            return this.evm == 0 && this.evk == this.evl ? false : false;
        }
    }

    private final boolean bCh() {
        if (this.bCx()) {
            return true;
        } else {
            return this.evm == 0 && this.evk == this.evl ? false : false;
        }
    }

    private final boolean bCi() {
        if (this.oP(1)) {
            return true;
        } else if (this.evm == 0 && this.evk == this.evl) {
            return false;
        } else {
            zV var1 = this.evk;
            if (this.bCf()) {
                this.evk = var1;
            } else if (this.evm == 0 && this.evk == this.evl) {
                return false;
            }

            return false;
        }
    }

    private final boolean bCj() {
        if (this.bCz()) {
            return true;
        } else {
            return this.evm == 0 && this.evk == this.evl ? false : false;
        }
    }

    private final boolean bCk() {
        if (this.oP(16)) {
            return true;
        } else if (this.evm == 0 && this.evk == this.evl) {
            return false;
        } else {
            do {
                zV var1 = this.evk;
                if (this.bCB()) {
                    this.evk = var1;
                    return false;
                }
            } while (this.evm != 0 || this.evk != this.evl);

            return false;
        }
    }

    private final boolean bCl() {
        if (this.bCu()) {
            return true;
        } else {
            return this.evm == 0 && this.evk == this.evl ? false : false;
        }
    }

    private final boolean bCm() {
        if (this.oP(56)) {
            return true;
        } else {
            return this.evm == 0 && this.evk == this.evl ? false : false;
        }
    }

    private final boolean bCn() {
        zV var1 = this.evk;
        if (this.bCm()) {
            this.evk = var1;
            if (this.bCg()) {
                return true;
            }

            if (this.evm == 0 && this.evk == this.evl) {
                return false;
            }
        } else if (this.evm == 0 && this.evk == this.evl) {
            return false;
        }

        return false;
    }

    private final boolean bCo() {
        if (this.oP(13)) {
            return true;
        } else if (this.evm == 0 && this.evk == this.evl) {
            return false;
        } else {
            do {
                zV var1 = this.evk;
                if (this.bCA()) {
                    this.evk = var1;
                    return false;
                }
            } while (this.evm != 0 || this.evk != this.evl);

            return false;
        }
    }

    private final boolean bCp() {
        if (this.bCC()) {
            return true;
        } else {
            return this.evm == 0 && this.evk == this.evl ? false : false;
        }
    }

    private final boolean bCq() {
        zV var1 = this.evk;
        if (this.bCp()) {
            this.evk = var1;
            if (this.bCl()) {
                this.evk = var1;
                if (this.bCj()) {
                    this.evk = var1;
                    if (this.bCh()) {
                        return true;
                    }

                    if (this.evm == 0 && this.evk == this.evl) {
                        return false;
                    }
                } else if (this.evm == 0 && this.evk == this.evl) {
                    return false;
                }
            } else if (this.evm == 0 && this.evk == this.evl) {
                return false;
            }
        } else if (this.evm == 0 && this.evk == this.evl) {
            return false;
        }

        return false;
    }

    private final boolean bCr() {
        if (this.oP(1)) {
            return true;
        } else {
            return this.evm == 0 && this.evk == this.evl ? false : false;
        }
    }

    private final boolean bCs() {
        zV var1 = this.evk;
        if (this.bCo()) {
            this.evk = var1;
            if (this.bCk()) {
                this.evk = var1;
                if (this.bCi()) {
                    return true;
                }

                if (this.evm == 0 && this.evk == this.evl) {
                    return false;
                }
            } else if (this.evm == 0 && this.evk == this.evl) {
                return false;
            }
        } else if (this.evm == 0 && this.evk == this.evl) {
            return false;
        }

        return false;
    }

    private final boolean bCt() {
        if (this.bCq()) {
            return true;
        } else if (this.evm == 0 && this.evk == this.evl) {
            return false;
        } else {
            do {
                zV var1 = this.evk;
                if (this.bCq()) {
                    this.evk = var1;
                    return false;
                }
            } while (this.evm != 0 || this.evk != this.evl);

            return false;
        }
    }

    private final boolean bCu() {
        if (this.oP(8)) {
            return true;
        } else {
            return this.evm == 0 && this.evk == this.evl ? false : false;
        }
    }

    private final boolean bCv() {
        if (this.bCn()) {
            return true;
        } else {
            return this.evm == 0 && this.evk == this.evl ? false : false;
        }
    }

    private final boolean bCw() {
        if (this.oP(16)) {
            return true;
        } else {
            return this.evm == 0 && this.evk == this.evl ? false : false;
        }
    }

    private final boolean bCx() {
        if (this.oP(10)) {
            return true;
        } else {
            return this.evm == 0 && this.evk == this.evl ? false : false;
        }
    }

    private final boolean bCy() {
        zV var1 = this.evk;
        if (this.bCv()) {
            this.evk = var1;
            if (this.bCt()) {
                return true;
            }

            if (this.evm == 0 && this.evk == this.evl) {
                return false;
            }
        } else if (this.evm == 0 && this.evk == this.evl) {
            return false;
        }

        return false;
    }

    private final boolean bCz() {
        if (this.oP(17)) {
            return true;
        } else {
            return this.evm == 0 && this.evk == this.evl ? false : false;
        }
    }

    private final boolean bCA() {
        if (this.oP(1)) {
            return true;
        } else {
            return this.evm == 0 && this.evk == this.evl ? false : false;
        }
    }

    private final boolean bCB() {
        if (this.oP(1)) {
            return true;
        } else {
            return this.evm == 0 && this.evk == this.evl ? false : false;
        }
    }

    private final boolean bCC() {
        if (this.oP(19)) {
            return true;
        } else {
            return this.evm == 0 && this.evk == this.evl ? false : false;
        }
    }

    private final boolean bCD() {
        if (this.oP(56)) {
            return true;
        } else if (this.evm == 0 && this.evk == this.evl) {
            return false;
        } else {
            do {
                zV var1 = this.evk;
                if (this.bCr()) {
                    this.evk = var1;
                    return false;
                }
            } while (this.evm != 0 || this.evk != this.evl);

            return false;
        }
    }

    public void a(ho var1) {
        this.evg.a(var1);
        this.evh = new zV();
        this.evj = -1;
        this.evo = 0;

        int var2;
        for (var2 = 0; var2 < 95; ++var2) {
            this.evp[var2] = -1;
        }

        for (var2 = 0; var2 < this.evt.length; ++var2) {
            this.evt[var2] = new a();
        }

    }

    public void a(abV var1) {
        this.evg = var1;
        this.evh = new zV();
        this.evj = -1;
        this.evo = 0;

        int var2;
        for (var2 = 0; var2 < 95; ++var2) {
            this.evp[var2] = -1;
        }

        for (var2 = 0; var2 < this.evt.length; ++var2) {
            this.evt[var2] = new a();
        }

    }

    private final zV oO(int var1) throws aNQ {
        zV var2 = this.evh;
        if (this.evh.ccs != null) {
            this.evh = this.evh.ccs;
        } else {
            this.evh = this.evh.ccs = this.evg.bCE();
        }

        this.evj = -1;
        if (this.evh.kind != var1) {
            this.evh = var2;
            this.evy = var1;
            throw this.bCG();
        } else {
            ++this.evo;
            if (++this.evv > 100) {
                this.evv = 0;

                for (int var3 = 0; var3 < this.evt.length; ++var3) {
                    for (a var4 = this.evt[var3]; var4 != null; var4 = var4.hJj) {
                        if (var4.hJg < this.evo) {
                            var4.hJh = null;
                        }
                    }
                }
            }

            return this.evh;
        }
    }

    private final boolean oP(int var1) {
        if (this.evk == this.evl) {
            --this.evm;
            if (this.evk.ccs == null) {
                this.evl = this.evk = this.evk.ccs = this.evg.bCE();
            } else {
                this.evl = this.evk = this.evk.ccs;
            }
        } else {
            this.evk = this.evk.ccs;
        }

        if (this.evu) {
            int var2 = 0;

            zV var3;
            for (var3 = this.evh; var3 != null && var3 != this.evk; var3 = var3.ccs) {
                ++var2;
            }

            if (var3 != null) {
                this.U(var1, var2);
            }
        }

        return this.evk.kind != var1;
    }

    public final zV bCE() {
        if (this.evh.ccs != null) {
            this.evh = this.evh.ccs;
        } else {
            this.evh = this.evh.ccs = this.evg.bCE();
        }

        this.evj = -1;
        ++this.evo;
        return this.evh;
    }

    public final zV oQ(int var1) {
        zV var2 = this.lookingAhead ? this.evk : this.evh;

        for (int var3 = 0; var3 < var1; ++var3) {
            if (var2.ccs != null) {
                var2 = var2.ccs;
            } else {
                var2 = var2.ccs = this.evg.bCE();
            }
        }

        return var2;
    }

    private final int bCF() {
        return (this.evi = this.evh.ccs) == null ? (this.evj = (this.evh.ccs = this.evg.bCE()).kind) : (this.evj = this.evi.kind);
    }

    private void U(int var1, int var2) {
        if (var2 < 100) {
            if (var2 == this.evA + 1) {
                this.evz[this.evA++] = var1;
            } else if (this.evA != 0) {
                this.evx = new int[this.evA];

                for (int var3 = 0; var3 < this.evA; ++var3) {
                    this.evx[var3] = this.evz[var3];
                }

                boolean var7 = false;
                Enumeration var4 = this.evw.elements();

                label47:
                do {
                    int[] var5;
                    do {
                        if (!var4.hasMoreElements()) {
                            break label47;
                        }

                        var5 = (int[]) var4.nextElement();
                    } while (var5.length != this.evx.length);

                    var7 = true;

                    for (int var6 = 0; var6 < this.evx.length; ++var6) {
                        if (var5[var6] != this.evx[var6]) {
                            var7 = false;
                            break;
                        }
                    }
                } while (!var7);

                if (!var7) {
                    this.evw.addElement(this.evx);
                }

                if (var2 != 0) {
                    this.evz[(this.evA = var2) - 1] = var1;
                }
            }

        }
    }

    public final aNQ bCG() {
        this.evw.removeAllElements();
        boolean[] var1 = new boolean[78];

        int var2;
        for (var2 = 0; var2 < 78; ++var2) {
            var1[var2] = false;
        }

        if (this.evy >= 0) {
            var1[this.evy] = true;
            this.evy = -1;
        }

        int var3;
        for (var2 = 0; var2 < 95; ++var2) {
            if (this.evp[var2] == this.evo) {
                for (var3 = 0; var3 < 32; ++var3) {
                    if ((this.evq[var2] & 1 << var3) != 0) {
                        var1[var3] = true;
                    }

                    if ((this.evr[var2] & 1 << var3) != 0) {
                        var1[32 + var3] = true;
                    }

                    if ((this.evs[var2] & 1 << var3) != 0) {
                        var1[64 + var3] = true;
                    }
                }
            }
        }

        for (var2 = 0; var2 < 78; ++var2) {
            if (var1[var2]) {
                this.evx = new int[1];
                this.evx[0] = var2;
                this.evw.addElement(this.evx);
            }
        }

        this.evA = 0;
        this.bCH();
        this.U(0, 0);
        int[][] var4 = new int[this.evw.size()][];

        for (var3 = 0; var3 < this.evw.size(); ++var3) {
            var4[var3] = (int[]) this.evw.elementAt(var3);
        }

        return new aNQ(this.evh, var4, tokenImage);
    }

    public final void enable_tracing() {
    }

    public final void disable_tracing() {
    }

    private final void bCH() {
        this.evu = true;

        for (int var1 = 0; var1 < 2; ++var1) {
            a var2 = this.evt[var1];

            do {
                if (var2.hJg > this.evo) {
                    this.evm = var2.hJi;
                    this.evl = this.evk = var2.hJh;
                    switch (var1) {
                        case 0:
                            this.bCD();
                            break;
                        case 1:
                            this.bCd();
                    }
                }

                var2 = var2.hJj;
            } while (var2 != null);
        }

        this.evu = false;
    }

    private final void V(int var1, int var2) {
        a var3;
        for (var3 = this.evt[var1]; var3.hJg > this.evo; var3 = var3.hJj) {
            if (var3.hJj == null) {
                var3 = var3.hJj = new a();
                break;
            }
        }

        var3.hJg = this.evo + var2 - this.evm;
        var3.hJh = this.evh;
        var3.hJi = var2;
    }

    static final class a {
        int hJg;
        zV hJh;
        int hJi;
        a hJj;
    }
}
