package all;

//package org.w3c.css.sac;
public interface ICharacterDataSelector extends ISimpleSelector {
    String getData();
}
