package all;

import org.apache.commons.logging.Log;
import taikodom.render.loader.RenderAsset;
import taikodom.render.scene.RenderObject;
import taikodom.render.scene.Scene;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Менеджер загрузаки файлов .trail
 * gl.java
 */
public class LoaderTrail implements ILoaderTrail {
    private static final Log logger = LogPrinter.K(LoaderTrail.class);
    private final Pm PP;
    private final Comparator PS = new Comparator() {
        @Override
        public int compare(Object o1, Object o2) {
            return a((aGU) o1, (aGU) o2);
        }

        public int a(aGU var1, aGU var2) {
            return var2.priority - var1.priority;
        }
    };
    Executor executor;
    private List PH = new ArrayList();
    private List PJ = new ArrayList();
    private List PK = new LinkedList();
    private ReentrantLock PL = new ReentrantLock();
    /**
     * Двигатель отрисовки графики
     */
    private EngineGraphics engineGraphics;
    private int PM = 0;
    private Ru PN = new Ru();
    private Xn PO;
    private aUh PQ;
    private akd PR;
    private Thread thread;
    private volatile aGU PT;
    private LinkedBlockingQueue PU = new LinkedBlockingQueue(500);

    /**
     * @param var1 Движок отрисовки графики
     */
    public LoaderTrail(EngineGraphics var1) {
        this.engineGraphics = var1;
        this.PP = new Pm(this.engineGraphics);
        this.PQ = new aUh();
        this.PR = new akd(new Ru());
    }

    protected Xn vq() {
        return this.PO;
    }

    public void a(Xn var1) {
        this.PO = var1;
    }

    public Ru.a ar(String var1) {
        ain var2 = this.engineGraphics.aen().concat("data/models/trails/" + var1 + ".trail");
        Ru.a var3 = null;

        try {
            var3 = this.PN.k(var2);
        } catch (IOException var6) {
            logger.warn("Trying to_q load all null trail file, ignoring");
        } finally {
            ;
        }

        return var3;
    }

    public void a(String var1, hQ_q var2, String var3, boolean var4) {
        if (var1 != null && var2 != null) {
            List var5 = this.PK;
            synchronized (this.PK) {
                this.PK.add(new Va(var1, var2, this.PQ, var4));
                Collections.sort(this.PK, this.PS);
            }

            ++this.PM;
        } else {
            logger.warn(var3 + " is trying to_q load all null file / null resource, ignoring");
        }
    }

    public void a(String var1, String var2, Scene var3, NJ var4, String var5, int var6) {
        if (var1 == null || var2 == null && var4 != null) {
            logger.warn(var5 + " is trying to_q load all null file / null resource, ignoring");
        } else {
            List var7 = this.PK;
            synchronized (this.PK) {
                this.PK.add(new acj(var1, var2, var4, var3, this.PP, var6));
                Collections.sort(this.PK, this.PS);
            }

            ++this.PM;
        }
    }

    public void a(String var1, String var2, Scene var3, NJ var4, String var5) {
        this.a(var1, var2, var3, var4, var5, 0);
    }

    public void a(String var1, ST var2, String var3) {
        if (var1 != null && var2 != null) {
            ain var4 = this.engineGraphics.aen().concat("data/models/trails/" + var1 + ".trail");
            List var5 = this.PK;
            synchronized (this.PK) {
                this.PK.add(new PU(var1, var4, var2, this.PR));
                Collections.sort(this.PK, this.PS);
            }

            ++this.PM;
        } else {
            logger.warn(var3 + " is trying to_q load all null file / null trail resource, ignoring");
        }
    }

    public void vr() {
        List var1 = this.PK;
        synchronized (this.PK) {
            aGU var2;
            for (Iterator var3 = this.PK.iterator(); var3.hasNext(); --var2.priority) {
                var2 = (aGU) var3.next();
            }

        }
    }

    public void a(String[] var1) {
        for (int var2 = 0; var2 < var1.length; ++var2) {
            this.a(var1[var2], (String) null, (Scene) null, (NJ) null, "Scync loader for cache ");
        }

    }

    public void as(String var1) {
        this.engineGraphics.bV(var1);
    }

    public RenderObject j(String var1, String var2) {
        return (RenderObject) this.k(var1, var2);
    }

    private RenderAsset k(String var1, String var2) {
        this.engineGraphics.bV(var1);
        return this.engineGraphics.bT(var2);
    }

    public boolean isReady() {
        return this.PK.isEmpty();
    }

    /**
     * Запустить загрузку файлов Менеджер загрузаки файлов .trail
     */
    public void start() {
        this.thread = new Thread("Resource loader thread") {
            public void run() {
                while (LoaderTrail.this.thread == Thread.currentThread()) {
                    LoaderTrail.this.gZ();

                    try {
                        sleep(20L);
                    } catch (InterruptedException var1) {
                        ;
                    }
                }

            }
        };
        this.thread.setDaemon(true);
        this.thread.start();
    }

    public void stop() {
        if (this.thread != null && this.thread.isAlive()) {
            this.thread.interrupt();

            try {
                this.thread.join();
            } catch (InterruptedException var2) {
                var2.printStackTrace();
            }
        }

    }

    public void gZ() {
        if (this.PK.size() > 0) {
            List var2 = this.PK;
            aGU var1;
            synchronized (this.PK) {
                var1 = (aGU) this.PK.remove(0);
            }

            Xn var21 = this.vq();
            boolean var3 = var21.brL();

            try {
                if (var1.fileName == null || var1.fileName.length() <= 0) {
                    logger.error("Trying to_q load all resource with test_GLCanvas filename!");
                    logger.error("current items to_q process " + this.PK.size());
                    return;
                }

                this.PT = var1;
                var1.hTO.b(var1);
            } catch (aOX var18) {
                logger.error(var18.getMessage());
            } catch (Exception var19) {
                logger.error(var19.getMessage(), var19);
            } finally {
                if (var3) {
                    var21.brK();
                }

            }

            if (var1.hTN != null) {
                if (var1.hTN.length() <= 0) {
                    logger.error("Trying to_q load all resource with test_GLCanvas name! " + var1.fileName);
                    logger.error("current items to_q process " + this.PK.size());
                    return;
                }

                var1.hTO.c(var1);
                this.a(var1);
            }
        }

    }

    private void vs() {
        this.executor.execute(new Runnable() {
            public void run() {
                LoaderTrail.this.PM = 0;
                if (LoaderTrail.logger.isDebugEnabled()) {
                    LoaderTrail.logger.debug("loading completed");
                }

                if (!LoaderTrail.this.PH.isEmpty()) {
                    ArrayList var1 = new ArrayList();
                    var1.addAll(LoaderTrail.this.PH);
                    Iterator var3 = var1.iterator();

                    while (var3.hasNext()) {
                        ado var2 = (ado) var3.next();
                        var2.Xy();
                    }

                }
            }
        });
    }

    private void vt() {
        this.PL.lock();
        ArrayList var1 = new ArrayList();
        var1.addAll(this.PJ);
        this.PL.unlock();
        if (!var1.isEmpty()) {
            if (logger.isDebugEnabled()) {
                logger.debug("file loaded. total: " + this.PM + " | to_q go: " + this.PK.size());
            }

            Iterator var3 = var1.iterator();

            while (var3.hasNext()) {
                aJP var2 = (aJP) var3.next();
                var2.ad(this.PM, this.PK.size());
            }

        }
    }

    public void a(ado var1) {
        this.PL.lock();
        this.PH.add(var1);
        this.PL.unlock();
    }

    public void b(ado var1) {
        this.PL.lock();
        this.PH.remove(var1);
        this.PL.unlock();
    }

    public void a(aJP var1) {
        this.PL.lock();
        this.PJ.add(var1);
        this.PL.unlock();
    }

    public void b(aJP var1) {
        this.PL.lock();
        this.PJ.remove(var1);
        this.PL.unlock();
    }

    protected void a(aGU var1) {
        try {
            this.PU.put(var1);
        } catch (InterruptedException var3) {
            var3.printStackTrace();
        }

    }

    public void vu() {
        aGU var1 = null;

        while ((var1 = (aGU) this.PU.poll()) != null) {
            var1.boi();
            all.aGU finalVar = var1;
            this.executor.execute(new Runnable() {
                public void run() {
                    finalVar.boh();
                    LoaderTrail.this.vt();
                }
            });
            if (this.PK.isEmpty() && this.PU.isEmpty()) {
                this.executor.execute(new Runnable() {
                    public void run() {
                        LoaderTrail.this.vs();
                    }
                });
            }
        }

    }

    public RenderAsset getAsset(String var1) {
        return this.engineGraphics.bT(var1);
    }

    public RenderAsset l(String var1, String var2) {
        aGU var3 = this.PT;
        return var3 != null && var1.equalsIgnoreCase(var3.fileName) ? null : this.engineGraphics.bU(var2);
    }

    public void dispose() {
        Thread var1 = this.thread;
        this.thread = null;
        var1.interrupt();

        try {
            var1.join();
        } catch (InterruptedException var3) {
            var3.printStackTrace();
        }

    }

    public void setExecutor(Executor var1) {
        this.executor = var1;
    }
}
