package all;

import javax.swing.*;
import java.awt.*;
import java.util.StringTokenizer;

/**
 * Базовые элементы интерфейса
 * тег window JInternalFrame nx
 * class BaseUItegXML WindowCustom  BaseItemFactory
 */
public class WindowCustom extends BaseItemFactory {

    /**
     * Создание базового Component интерфейса
     * Вызывается из class BaseItem
     *
     * @param var1 Пример all.MapKeyValue@1325f4b
     * @param var2 Пример javax.swing.JRootPane[,0,0,1024x780,invalid,layout=javax.swing.JRootPane$RootLayout,alignmentX=0.0,alignmentY=0.0,border=all.BorderWrapper@f34226,flags=16777664,maximumSize=,minimumSize=,preferredSize=]
     * @param var3 Пример <window class="empty" transparent="true" alwaysOnTop="true" focusable="false" layout="GridLayout" css="taikodomversion.css">...</window>
     * @return
     */
    @Override
    public JComponent CreatComponentCustom(MapKeyValue var1, Container var2, XmlNode var3) {
        JComponent var4 = new WindowCustomWrapper();
        this.CreatChildComponentCustom(var1, var4, var3); //я добавил не помогло
        return var4;
    }

    protected void a(MapKeyValue var1, WindowCustomWrapper var2, XmlNode var3) {
        super.createLayout(var1, var2, var3);
        String var4;
        if ("true".equals(var3.getAttribute("transparent"))) {
            var2.cpU();
            var2.setClosable(false);
            var2.setIconifiable(false);
            var2.setMaximizable(false);
            var2.setResizable(false);
            var2.fV(false);
        } else {
            var2.setResizable(!"false".equals(var3.getAttribute("resizable")));
            var2.setAlwaysOnTop(!"false".equals(var3.getAttribute("alwaysOnTop")));
            if ("false".equals(var3.getAttribute("has-title"))) {
                var2.cpU();
            } else {
                var2.setClosable(!"false".equals(var3.getAttribute("closable")));
                var2.setIconifiable("true".equals(var3.getAttribute("iconifiable")));
                var2.setMaximizable("true".equals(var3.getAttribute("maximizable")));
                var2.fV(!"false".equals(var3.getAttribute("movable")));
                var2.setFocusable(!"false".equals(var3.getAttribute("focusable")));
                var4 = var3.getAttribute("title");
                if (var4 != null) {
                    var2.setTitle(var4);
                }
            }
        }

        if ("true".equals(var3.getAttribute("modal"))) {
            var2.setModal(true);
        }

        var4 = var3.getAttribute("size");
        if (var4 != null) {
            StringTokenizer var5 = new StringTokenizer(var4, ",");

            assert var5.countTokens() == 2;

            var2.setSize(Integer.parseInt(var5.nextToken().trim()), Integer.parseInt(var5.nextToken().trim()));
        }
    }
}
