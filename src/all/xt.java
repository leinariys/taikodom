package all;

import javax.swing.*;
import java.awt.*;

public class xt extends ImageIcon {
    private static final long serialVersionUID = 1L;
    private static final int BORDER = 6;
    private boolean cW;

    public xt(Image var1) {
        super(var1);
    }

    public int getIconHeight() {
        return super.getIconHeight() + BORDER;
    }

    public int getIconWidth() {
        return super.getIconWidth() + BORDER;
    }

    public void start() {
        this.cW = true;
    }

    public void stop() {
        this.cW = false;
    }

    public synchronized void paintIcon(Component var1, Graphics var2, int var3, int var4) {
        Graphics2D var5 = (Graphics2D) var2.create();
        var5.translate(3, 3);
        if (this.cW) {
            var5.rotate((double) (System.currentTimeMillis() / 10L % 360L) * 3.141592653589793D / 180.0D, (double) (var3 + this.getImage().getWidth(var1) / 2), (double) (var4 + this.getImage().getHeight(var1) / 2));
        } else {
            var5.rotate(0.0D);
        }

        var5.drawImage(this.getImage(), var3, var4, var1);
    }
}
