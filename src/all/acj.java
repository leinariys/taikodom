package all;

import taikodom.render.loader.RenderAsset;
import taikodom.render.scene.Scene;
import taikodom.render.scene.SceneObject;

public class acj extends aGU {
    public RenderAsset fbb;
    public NJ fbc;
    private Scene fba;

    public acj(String var1, String var2, NJ var3, Scene var4, aOT var5, int var6) {
        super(var1, var2, var5);
        this.priority = var6;
        this.fba = var4;
        this.fbc = var3;
    }

    protected RenderAsset bOA() {
        return this.fbb;
    }

    protected void c(RenderAsset var1) {
        this.fbb = var1;
    }

    protected void boh() {
        this.fbc.a(this.bOA());
    }

    protected void boi() {
        RenderAsset var1 = this.bOA();
        if (var1 instanceof SceneObject && this.fba != null) {
            this.fba.addChild((SceneObject) var1);
        }

    }
}
