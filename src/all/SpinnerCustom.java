package all;

import javax.swing.*;
import java.awt.*;

/**
 * Счетчик JSpinner
 * отображает текст и 2-е кнопки вверхи вниз
 */
public class SpinnerCustom extends BaseItem {
    private static final LogPrinter logger = LogPrinter.setClass(SpinnerCustom.class);

    /**
     * Создание базового Component интерфейса
     * Вызывается из class BaseItem
     *
     * @param var1
     * @param var2
     * @param var3
     * @return
     */
    @Override
    protected JComponent CreatComponentCustom(MapKeyValue var1, Container var2, XmlNode var3) {
        return new JSpinner();
    }

    protected void a(MapKeyValue var1, JSpinner var2, XmlNode var3) {
        super.applyAttribute(var1, (Component) var2, (XmlNode) var3);
        SpinnerNumberModel var4 = new SpinnerNumberModel();
        var4.setMinimum(checkValueAttribut((XmlNode) var3, (String) "min", (LogPrinter) logger));
        var4.setMaximum(checkValueAttribut((XmlNode) var3, (String) "max", (LogPrinter) logger));
        Integer var5 = checkValueAttribut((XmlNode) var3, (String) "step", (LogPrinter) logger);
        var4.setStepSize(var5 != null ? var5.intValue() : 1);
        var5 = checkValueAttribut((XmlNode) var3, (String) "value", (LogPrinter) logger);
        var4.setValue(var5 != null ? var5 : (var4.getMinimum() != null ? var4.getMinimum() : Integer.valueOf(0)));
        var2.setModel(var4);
    }
}
