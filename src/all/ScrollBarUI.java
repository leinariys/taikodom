package all;

import taikodom.render.scene.SoundObject;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicScrollBarUI;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;

//import javax.swing.plaf.basic.BasicScrollBarUI.ArrowButtonListener;
//import javax.swing.plaf.basic.BasicScrollBarUI.TrackListener;

/**
 * ScrollBarUI
 */
public class ScrollBarUI extends BasicScrollBarUI implements aIh {
    private static final BaseUItegXML aPy = BaseUItegXML.thisClass;
    private final JScrollBar scrollbar;
    private SoundObject ccY;
    private ComponentManager Rp;
    private JComponent ccZ;
    private JComponent cda;
    private JComponent cdb;
    private JComponent cdc;

    public ScrollBarUI(JScrollBar var1) {
        this.scrollbar = var1;
        this.Rp = new ComponentManager("scrollbar", var1);
        var1.setBackground((Color) null);
        var1.setOpaque(false);
    }

    public static ComponentUI createUI(JComponent var0) {
        return new ScrollBarUI((JScrollBar) var0);
    }

    // $FF: synthetic method
    static boolean d(ScrollBarUI var0) {
        return var0.isDragging;
    }

    protected void installListeners() {
        super.installListeners();
        this.scrollTimer = new a(60, this.scrollListener);
        this.scrollTimer.setInitialDelay(300);
    }

    protected ArrowButtonListener createArrowButtonListener() {
        return new b();
    }

    public void update(Graphics var1, JComponent var2) {
        RM.a((aIh) this, (Graphics) var1, (JComponent) var2);
        super.update(var1, var2);
    }

    public ComponentManager wy() {
        return this.Rp;
    }

    public Dimension getPreferredSize(JComponent var1) {
        int var2 = 0;
        int var3 = 0;
        int var4;
        Dimension var5;
        if (this.scrollbar.getOrientation() == 1) {
            for (var4 = 0; var4 < var1.getComponentCount(); ++var4) {
                var5 = var1.getComponent(var4).getPreferredSize();
                var3 += var5.height;
                var2 = Math.max(var2, var5.width);
            }
        } else {
            for (var4 = 0; var4 < var1.getComponentCount(); ++var4) {
                var5 = var1.getComponent(var4).getPreferredSize();
                var2 += var5.width;
                var3 = Math.max(var3, var5.height);
            }
        }

        return this.wy().c(var1, new Dimension(var2, var3));
    }

    public Dimension getMinimumSize(JComponent var1) {
        return this.wy().b(var1, super.getMinimumSize(var1));
    }

    public Dimension getMaximumSize(JComponent var1) {
        return this.wy().a(var1, super.getMaximumSize(var1));
    }

    protected void paintTrack(Graphics var1, JComponent var2, Rectangle var3) {
        JComponent var4 = this.e(var2);
        var4.setBounds(var3);
        var4.setVisible(true);
    }

    protected Rectangle getThumbBounds() {
        return super.getThumbBounds();
    }

    protected void paintThumb(Graphics var1, JComponent var2, Rectangle var3) {
        if (this.scrollbar.isEnabled()) {
            JComponent var4 = this.f(var2);
            int var5 = (this.isThumbRollover() ? 32 : 0) | (this.isDragging ? 16 : 0);
            ComponentManager.getCssHolder(var4).q(48, var5);
            var4.setBounds(var3);
            var4.setVisible(true);
        }
    }

    protected JButton createDecreaseButton(int var1) {
        String var2 = this.fR(var1);
        if (var2 == null) {
            return null;
        } else {
            JButton var3 = new JButton();
            ComponentManager.getCssHolder(var3).setAttribute("name", var2);
            var3.setName(var2);
            this.scrollbar.add(var3);
            return var3;
        }
    }

    protected JButton createIncreaseButton(int var1) {
        String var2 = this.fR(var1);
        if (var2 == null) {
            return null;
        } else {
            JButton var3 = new JButton();
            ComponentManager.getCssHolder(var3).setAttribute("name", var2);
            var3.setName(var2);
            this.scrollbar.add(var3);
            return var3;
        }
    }

    private String fR(int var1) {
        switch (var1) {
            case 1:
                return "start";
            case 2:
            case 4:
            case 6:
            default:
                return null;
            case 3:
                return "right";
            case 5:
                return "end";
            case 7:
                return "left";
        }
    }

    protected JComponent e(JComponent var1) {
        if (this.scrollbar.getOrientation() == 1) {
            if (this.cdc == null) {
                this.cdc = new JLabel();
                this.cdc.setName("vertical-track");
                ComponentManager.getCssHolder(this.cdc).setAttribute("name", "vertical-track");
                var1.add(this.cdc);
            }

            if (this.cdb != null) {
                this.cdb.setVisible(false);
            }

            return this.cdc;
        } else {
            if (this.cdb == null) {
                this.cdb = new JLabel();
                this.cdb.setName("horizontal-track");
                ComponentManager.getCssHolder(this.cdb).setAttribute("name", "horizontal-track");
                var1.add(this.cdb);
            }

            if (this.cdc != null) {
                this.cdc.setVisible(false);
            }

            return this.cdb;
        }
    }

    protected JComponent f(JComponent var1) {
        if (this.scrollbar.getOrientation() == 1) {
            if (this.cda == null) {
                this.cda = new JLabel();
                this.cda.setName("vertical-thumb");
                ComponentManager.getCssHolder(this.cda).setAttribute("name", "vertical-thumb");
                var1.add(this.cda, 0);
            }

            if (this.ccZ != null) {
                this.ccZ.setVisible(false);
            }

            return this.cda;
        } else {
            if (this.ccZ == null) {
                this.ccZ = new JLabel();
                this.ccZ.setName("horizontal-thumb");
                ComponentManager.getCssHolder(this.ccZ).setAttribute("name", "horizontal-thumb");
                var1.add(this.ccZ, 0);
            }

            if (this.cda != null) {
                this.cda.setVisible(false);
            }

            return this.ccZ;
        }
    }

    protected TrackListener createTrackListener() {
        return new TrackListener() {
            public void mousePressed(MouseEvent var1) {
                boolean var2 = ScrollBarUI.d(ScrollBarUI.this);
                super.mousePressed(var1);
                if (!SwingUtilities.isRightMouseButton(var1) && (ScrollBarUI.this.getSupportsAbsolutePositioning() || !SwingUtilities.isMiddleMouseButton(var1))) {
                    if (var2 != ScrollBarUI.d(ScrollBarUI.this)) {
                        ScrollBarUI.this.f(ScrollBarUI.this.scrollbar).repaint();
                    }

                }
            }

            public void mouseReleased(MouseEvent var1) {
                boolean var2 = ScrollBarUI.d(ScrollBarUI.this);
                super.mouseReleased(var1);
                if (!SwingUtilities.isRightMouseButton(var1) && (ScrollBarUI.this.getSupportsAbsolutePositioning() || !SwingUtilities.isMiddleMouseButton(var1))) {
                    if (var2 != ScrollBarUI.d(ScrollBarUI.this)) {
                        ScrollBarUI.this.f(ScrollBarUI.this.scrollbar).repaint();
                    }

                }
            }
        };
    }

    /**
     * TODO похлже это public pv wy()
     *
     * @return Графический элемент
     */
    @Override
    public aeK wz() {
        return this.Rp;
    }

    protected class b extends ArrowButtonListener {
        protected b() {
            super();
        }

        public void mousePressed(MouseEvent var1) {
            super.mousePressed(var1);
            ScrollBarUI.aPy.cx(ScrollBarUI.this.Rp.Vr().atV());
            if (!"start".equals(var1.getComponent().getName()) && !"left".equals(var1.getComponent().getName())) {
                if (("end".equals(var1.getComponent().getName()) || "right".equals(var1.getComponent().getName())) && ScrollBarUI.this.scrollbar.getValue() + ScrollBarUI.this.scrollbar.getModel().getExtent() >= ScrollBarUI.this.scrollbar.getMaximum()) {
                    return;
                }
            } else if (ScrollBarUI.this.scrollbar.getValue() <= ScrollBarUI.this.scrollbar.getMinimum()) {
                return;
            }

            ScrollBarUI.this.ccY = ScrollBarUI.aPy.cx(ScrollBarUI.this.Rp.Vr().auj());
        }

        public void mouseReleased(MouseEvent var1) {
            super.mouseReleased(var1);
        }
    }

    protected class a extends Timer {
        private static final long serialVersionUID = 3421712483686032200L;

        public a(int var2, ActionListener var3) {
            super(var2, var3);
        }

        public void stop() {
            super.stop();
            if (ScrollBarUI.this.ccY != null) {
                ScrollBarUI.this.ccY.stop();
            }

        }
    }
}
