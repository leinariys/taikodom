package all;

import com.hoplon.geometry.Vec3f;

import javax.vecmath.*;

//TODO на удаление
public class ajD extends Matrix3f //implements afA_q
{
    public ajD() {
        this.setIdentity();
    }

    public ajD(float[] var1) {
        super(var1[0], var1[3], var1[6], var1[1], var1[4], var1[7], var1[2], var1[5], var1[8]);
    }

    public ajD(Matrix3d var1) {
        super(var1);
    }

    public ajD(ajD var1) {
        super(var1);
    }

    public ajD(float var1, float var2, float var3, float var4, float var5, float var6, float var7, float var8, float var9) {
        super(var1, var4, var7, var2, var5, var8, var3, var6, var9);
    }

    public void c(float[] var1) {
        super.m00 = var1[0];
        super.m01 = var1[1];
        super.m02 = var1[2];
        super.m10 = var1[3];
        super.m11 = var1[4];
        super.m12 = var1[5];
        super.m20 = var1[6];
        super.m21 = var1[7];
        super.m22 = var1[8];
    }

    public float[] ceB() {
        float[] var1 = new float[]{super.m00, super.m01, super.m02, super.m10, super.m11, super.m12, super.m20, super.m21, super.m22};
        return var1;
    }

    public void transform(Tuple3d var1) {
        double var2 = var1.x;
        double var4 = var1.y;
        double var6 = var1.z;
        var1.x = (double) this.m00 * var2 + (double) this.m01 * var4 + (double) this.m02 * var6;
        var1.y = (double) this.m10 * var2 + (double) this.m11 * var4 + (double) this.m12 * var6;
        var1.z = (double) this.m20 * var2 + (double) this.m21 * var4 + (double) this.m22 * var6;
    }

    public final void rotateX(float var1) {
        float var2 = (float) Math.sin((double) var1);
        float var3 = (float) Math.cos((double) var1);
        float var4 = this.m10;
        float var5 = this.m11;
        float var6 = this.m12;
        float var7 = this.m20;
        float var8 = this.m21;
        float var9 = this.m22;
        this.m10 = var3 * var4 - var2 * var7;
        this.m11 = var3 * var5 - var2 * var8;
        this.m12 = var3 * var6 - var2 * var9;
        this.m20 = var2 * var4 + var3 * var7;
        this.m21 = var2 * var5 + var3 * var8;
        this.m22 = var2 * var6 + var3 * var9;
    }

    public final void rotateY(float var1) {
        float var2 = (float) Math.sin((double) var1);
        float var3 = (float) Math.cos((double) var1);
        float var4 = this.m00;
        float var5 = this.m01;
        float var6 = this.m02;
        float var7 = this.m20;
        float var8 = this.m21;
        float var9 = this.m22;
        this.m00 = var3 * var4 + var2 * var7;
        this.m01 = var3 * var5 + var2 * var8;
        this.m02 = var3 * var6 + var2 * var9;
        this.m20 = -var2 * var4 + var3 * var7;
        this.m21 = -var2 * var5 + var3 * var8;
        this.m22 = -var2 * var6 + var3 * var9;
    }

    public final void rotateZ(float var1) {
        float var2 = (float) Math.sin((double) var1);
        float var3 = (float) Math.cos((double) var1);
        float var4 = this.m00;
        float var5 = this.m01;
        float var6 = this.m02;
        float var7 = this.m10;
        float var8 = this.m11;
        float var9 = this.m12;
        this.m00 = var3 * var4 - var2 * var7;
        this.m01 = var3 * var5 - var2 * var8;
        this.m02 = var3 * var6 - var2 * var9;
        this.m10 = var2 * var4 + var3 * var7;
        this.m11 = var2 * var5 + var3 * var8;
        this.m12 = var2 * var6 + var3 * var9;
    }

    public void a(Vector3d var1, Vector3d var2) {
        float var3 = 1.0F / (this.m00 * this.m00 + this.m10 * this.m10 + this.m20 * this.m20);
        float var4 = 1.0F / (this.m01 * this.m01 + this.m11 * this.m11 + this.m21 * this.m21);
        float var5 = 1.0F / (this.m02 * this.m02 + this.m12 * this.m12 + this.m22 * this.m22);
        float var6 = this.m00 * var3;
        float var7 = this.m10 * var4;
        float var8 = this.m20 * var5;
        float var9 = this.m01 * var3;
        float var10 = this.m11 * var4;
        float var11 = this.m21 * var5;
        float var12 = this.m02 * var3;
        float var13 = this.m12 * var4;
        float var14 = this.m22 * var5;
        double var15 = var1.x;
        double var17 = var1.y;
        double var19 = var1.z;
        var2.x = var15 * (double) var6 + var17 * (double) var7 + var19 * (double) var8;
        var2.y = var15 * (double) var9 + var17 * (double) var10 + var19 * (double) var11;
        var2.z = var15 * (double) var12 + var17 * (double) var13 + var19 * (double) var14;
    }

    public void a(Vector3f var1, Vector3f var2) {
        float var3 = 1.0F / (this.m00 * this.m00 + this.m10 * this.m10 + this.m20 * this.m20);
        float var4 = 1.0F / (this.m01 * this.m01 + this.m11 * this.m11 + this.m21 * this.m21);
        float var5 = 1.0F / (this.m02 * this.m02 + this.m12 * this.m12 + this.m22 * this.m22);
        float var6 = this.m00 * var3;
        float var7 = this.m10 * var4;
        float var8 = this.m20 * var5;
        float var9 = this.m01 * var3;
        float var10 = this.m11 * var4;
        float var11 = this.m21 * var5;
        float var12 = this.m02 * var3;
        float var13 = this.m12 * var4;
        float var14 = this.m22 * var5;
        float var15 = var1.x;
        float var16 = var1.y;
        float var17 = var1.z;
        var2.x = var15 * var6 + var16 * var7 + var17 * var8;
        var2.y = var15 * var9 + var16 * var10 + var17 * var11;
        var2.z = var15 * var12 + var16 * var13 + var17 * var14;
    }

    public void a(Vector3f var1) {
        this.a(var1, var1);
    }

    public void a(Vector3d var1) {
        this.a(var1, var1);
    }

    public void set(ajK var1) {
        this.m00 = var1.m00;
        this.m01 = var1.m01;
        this.m02 = var1.m02;
        this.m10 = var1.m10;
        this.m11 = var1.m11;
        this.m12 = var1.m12;
        this.m20 = var1.m20;
        this.m21 = var1.m21;
        this.m22 = var1.m22;
    }

    public boolean anA() {
        float var1 = this.m00 + this.m01 + this.m02 + this.m10 + this.m11 + this.m12 + this.m20 + this.m21 + this.m22;
        return !Float.isNaN(var1) && !Float.isInfinite(var1);
    }

    public Vec3f ceC() {
        return new Vec3f(this.m00, this.m10, this.m20);
    }

    public Vec3f ceD() {
        return new Vec3f(this.m01, this.m11, this.m21);
    }

    public Vec3f ceE() {
        return new Vec3f(this.m02, this.m12, this.m22);
    }

    public void readExternal(Vm_q var1) {
        this.m00 = var1.gZ("m00");
        this.m01 = var1.gZ("m01");
        this.m02 = var1.gZ("m02");
        this.m10 = var1.gZ("m10");
        this.m11 = var1.gZ("m11");
        this.m12 = var1.gZ("m12");
        this.m20 = var1.gZ("m20");
        this.m21 = var1.gZ("m21");
        this.m22 = var1.gZ("m22");
    }

    public void writeExternal(att var1) {
        var1.a("m00", this.m00);
        var1.a("m01", this.m01);
        var1.a("m02", this.m02);
        var1.a("m10", this.m10);
        var1.a("m11", this.m11);
        var1.a("m12", this.m12);
        var1.a("m20", this.m20);
        var1.a("m21", this.m21);
        var1.a("m22", this.m22);
    }
}
