package all;

import java.util.HashMap;
import java.util.Map;

public class Ks {
    private static ThreadLocal dpR = new ThreadLocal() {
        @Override
        protected Map initialValue() {
            return new HashMap();
        }
    };

    public static QW D(Class var0) {
        Map var1 = (Map) dpR.get();
        QW var2 = (QW) var1.get(var0);
        if (var2 == null) {
            var2 = new QW(var0);
            var1.put(var0, var2);
        }

        return var2;
    }
}
