package all;

import taikodom.render.loader.RenderAsset;
import taikodom.render.scene.Scene;

/**
 * Интерфейс Менеджера загрузаки фаулов .trail
 */
public interface ILoaderTrail {
    /**
     * Запустить загрузку файлов Менеджер загрузаки файлов .trail
     */
    void start();

    void stop();

    void a(String var1, String var2, Scene var3, NJ var4, String var5);

    void a(String var1, String var2, Scene var3, NJ var4, String var5, int var6);

    void vr();

    void a(String var1, ST var2, String var3);

    void a(String[] var1);

    void a(ado var1);

    void b(ado var1);

    void a(aJP var1);

    void b(aJP var1);

    void vu();

    RenderAsset getAsset(String var1);

    RenderAsset l(String var1, String var2);

    void dispose();

    Ru.a ar(String var1);

    void as(String var1);
}
