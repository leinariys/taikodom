package all;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicListUI;
import java.awt.*;

/**
 * ListUI
 */
public class ListUI extends BasicListUI implements aIh {
    private ComponentManager Rp;

    public ListUI(JList var1) {
        this.Rp = new ComponentManager("list", var1);
        var1.setOpaque(false);
    }

    public static ComponentUI createUI(JComponent var0) {
        return new ListUI((JList) var0);
    }

    protected void paintCell(Graphics var1, int var2, Rectangle var3, ListCellRenderer var4, ListModel var5, ListSelectionModel var6, int var7) {
        Object var8 = var5.getElementAt(var2);
        boolean var9 = this.list.hasFocus() && var2 == var7;
        boolean var10 = var6.isSelectedIndex(var2);
        Component var11 = var4.getListCellRendererComponent(this.list, var8, var2, var10, var9);
        aeK var12 = ComponentManager.getCssHolder(var11);
        if (var12 != null) {
            int var13 = 0;
            if (var10) {
                var13 |= 2;
            }

            if (var9) {
                var13 |= 128;
            }

            var12.q(130, var13);
        }

        this.rendererPane.paintComponent(var1, var11, this.list, var3.x, var3.y, var3.width, var3.height, true);
        var12.Vq();
    }

    public void installUI(JComponent var1) {
        super.installUI(var1);
        JList var2 = (JList) var1;
        var2.setCellRenderer(new DefaultListCellRenderer());
    }

    public void update(Graphics var1, JComponent var2) {
        RM.a((aIh) this, (Graphics) var1, (JComponent) var2);
        this.paint(var1, var2);
    }

    public Dimension getPreferredSize(JComponent var1) {
        RM.a((aIh) this, (JComponent) var1);
        return this.wy().c(var1, super.getPreferredSize(var1));
    }

    public Dimension getMinimumSize(JComponent var1) {
        RM.a((aIh) this, (JComponent) var1);
        return this.wy().b(var1, super.getMinimumSize(var1));
    }

    public Dimension getMaximumSize(JComponent var1) {
        RM.a((aIh) this, (JComponent) var1);
        return this.wy().a(var1, super.getMaximumSize(var1));
    }

    public ComponentManager wy() {
        return this.Rp;
    }

    /**
     * TODO похлже это public pv wy()
     *
     * @return Графический элемент
     */
    @Override
    public aeK wz() {
        return this.Rp;
    }
}
