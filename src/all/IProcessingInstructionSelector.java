package all;

//package org.w3c.css.sac;
public interface IProcessingInstructionSelector extends ISimpleSelector {
    String getTarget();

    String getData();
}
