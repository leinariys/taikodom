package all;

import java.util.HashMap;

public enum ImagesetGui {
    imagesetNeutral("data/gui/imageset/imageset_neutral/"),
    corporations("data/corporations/"),
    imagesetCorp("data/gui/imageset/imageset_corp/"),
    imagesetIconography("data/gui/imageset/imageset_iconography/"),
    imagesetItems("data/gui/imageset/imageset_items/"),
    imagesetCore("data/gui/imageset/imageset_core/"),
    imagesetCorpimages("data/gui/imageset/imageset_corpimages/"),
    imagesetLoading("data/gui/imageset/imageset_loading/"),
    imagesetHud("data/gui/imageset/imageset_hud/"),
    imagesetAvatar("data/gui/imageset/imageset_avatar/"),
    imagesetAvatarCreation("data/gui/imageset/imageset_avatar_creation/"),
    imagesetCitizenshipNeo("data/gui/imageset/imageset_citizenship_neo/"),
    imagesetIconbuttonNeo("data/gui/imageset/imageset_iconbutton_neo/"),
    imagesetLogin("data/gui/imageset/imageset_login/");

    private String path;

    private ImagesetGui(String var3) {
        this.path = var3;
    }

    public static void check() {
        HashMap var0 = new HashMap();
        ImagesetGui[] var4;
        int var3 = (var4 = values()).length;//Соличество папок с ресурсами
        for (int var2 = 0; var2 < var3; ++var2) {
            ImagesetGui var1 = var4[var2];
            if (var0.containsKey(var1.path)) {
                System.err.println("ImageSet path '" + var1.path + "' is set for multiple ImageSets: " + var0.get(var1.path) + " and " + var1);
            } else {
                var0.put(var1.path, var1);//добавляет путь и значение
            }
        }
    }

    public String getPath() {
        return "res://" + this.path;
    }
}
