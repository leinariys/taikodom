package all;

import com.hoplon.geometry.Vec3f;

public abstract class ND extends yK {
    protected float collisionMargin = 0.0F;

    public abstract void a(aTC_q var1, Vec3f var2, Vec3f var3);

    public float getMargin() {
        return this.collisionMargin;
    }

    public void setMargin(float var1) {
        this.collisionMargin = var1;
    }
}
