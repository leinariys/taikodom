package all;

//package org.w3c.css.sac;
public class CSSException extends RuntimeException {
    public static short SAC_UNSPECIFIED_ERR = 0;
    public static short SAC_NOT_SUPPORTED_ERR = 1;
    public static short SAC_SYNTAX_ERR = 2;
    protected String s;
    protected Exception e;
    protected short code;

    public CSSException() {
    }

    public CSSException(String var1) {
        this.code = SAC_UNSPECIFIED_ERR;
        this.s = var1;
    }

    public CSSException(Exception var1) {
        this.code = SAC_UNSPECIFIED_ERR;
        this.e = var1;
    }

    public CSSException(short var1) {
        this.code = var1;
    }

    public CSSException(short var1, String var2, Exception var3) {
        this.code = var1;
        this.s = var2;
        this.e = var3;
    }

    public String getMessage() {
        if (this.s != null) {
            return this.s;
        } else {
            return this.e != null ? this.e.getMessage() : null;
        }
    }

    public short getCode() {
        return this.code;
    }

    public Exception getException() {
        return this.e;
    }
}
