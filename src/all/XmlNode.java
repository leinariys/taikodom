package all;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.*;
import java.util.Map.Entry;

/**
 * DOM представление .xml  документа
 */
public class XmlNode implements Cloneable {
    private static final LogPrinter logger = LogPrinter.setClass(XmlNode.class);
    private static final String EMPTY_STRING = "";

    /**
     * Имя тега
     */
    private String name = EMPTY_STRING;

    /**
     * Текстовое содержимое тега
     */
    private String text;

    /**
     * Атрибуты тега ключ - значение
     */
    private Map listAttribute = null;

    /**
     * Список тегов потомков
     */
    private List listChildrenTag = null;

    /**
     * UserData
     */
    private Object userData;

    /**
     * Флаг для сигнала вхождения в потомков
     */
    private boolean flagBool = false;

    public XmlNode() {
    }

    /**
     * @param name Имя тега
     */
    public XmlNode(String name) {
        this.name = name;
    }

    /**
     * @param var1
     * @param var2
     * @param var3
     */
    public XmlNode(String var1, String var2, String... var3) {
        this.name = var1;
        this.my(var2);
        if (var3 != null) {
            for (int var4 = 0; var4 + 1 < var3.length; var4 += 2) {
                this.addAttribute(var3[var4], var3[var4 + 1]);
            }
        }
    }

    public static String mA(String var0) {
        StringBuilder var1 = new StringBuilder();
        int var2 = var0 == null ? -1 : var0.length();

        for (int var3 = 0; var3 < var2; ++var3) {
            char var4 = var0.charAt(var3);
            if ((var4 < 'a' || var4 > 'z') && (var4 < 'A' || var4 > 'Z') && (var4 < '0' || var4 > '9')) {
                var1.append(String.format("_%02x", Integer.valueOf(var4)));
            } else {
                var1.append(var4);
            }
        }

        return var1.toString();
    }

    public static String mB(String var0) {
        StringBuilder var1 = new StringBuilder();
        int var2 = var0 == null ? -1 : var0.length();

        for (int var3 = 0; var3 < var2; ++var3) {
            char var4 = var0.charAt(var3);
            if (var4 >= 'a' && var4 <= 'z' || var4 >= 'A' && var4 <= 'Z' || var4 >= '0' && var4 <= '9') {
                var1.append(var4);
            } else if (var4 == '_') {
                var1.append((char) Integer.parseInt(var0.substring(var3 + 1, var3 + 3), 16));
                var3 += 2;
            } else {
                System.out.println("WARNING: character '" + var4 + "' illegal for html, ignoring it");
            }
        }

        return var1.toString();
    }

    /**
     * Получить Флаг для сигнала вхождения в потомков
     *
     * @return
     */
    public boolean getFlagBool() //dcE
    {
        return this.flagBool;
    }

    /**
     * Задать Флаг для сигнала вхождения в потомков
     */
    public void setFlagBool(Boolean vat1) {
        this.flagBool = vat1;
    }

    /**
     * @param var1
     */
    public void a(XmlNode var1) {
        var1.a(this);
        if (this.listChildrenTag != null && this.listChildrenTag.size() > 0) {
            Iterator var3 = (new ArrayList(this.listChildrenTag)).iterator();

            while (var3.hasNext()) {
                XmlNode var2 = (XmlNode) var3.next();
                var2.a(var1);
            }
        }
    }

    public Object clone() {
        return this.dcF();
    }

    public XmlNode dcF() {
        XmlNode var1 = new XmlNode(this.name);
        var1.text = this.text;
        if (this.listAttribute != null) {
            var1.getListAttribute().putAll(this.listAttribute);
        }

        if (this.listChildrenTag != null) {
            List var2 = var1.getChildrenTag();
            Iterator var4 = this.listChildrenTag.iterator();

            while (var4.hasNext()) {
                XmlNode var3 = (XmlNode) var4.next();
                var2.add(var3.dcF());
            }
        }

        var1.userData = this.userData;
        return var1;
    }

    /**
     * Получить название тега
     *
     * @return
     */
    public String getTegName() {
        return this.name;
    }

    /**
     * Задать название тега
     *
     * @param name
     */
    public void setTegName(String name) {
        this.name = name;
    }

    /**
     * Текстовое содержимое тега
     *
     * @return
     */
    public String getText() {
        return this.text;
    }

    /**
     * Задать текстовое содержимое тега
     *
     * @param text
     * @return
     */
    public XmlNode my(String text) {
        if (text != null && text.trim().length() == 0) {
            this.text = EMPTY_STRING;
        } else {
            this.text = text;
        }
        return this;
    }

    /**
     * Добавить потомка с заданым тегом
     *
     * @param name Название Тега: window, label
     * @return
     */
    public XmlNode addChildrenTag(String name) {
        XmlNode node = new XmlNode(name);
        this.getChildrenTag().add(node);
        return node;
    }

    /**
     * Добавить потомка с заданым тегом
     *
     * @param child Контейнер Тега
     * @return
     */
    public XmlNode addChildrenTag(XmlNode child) {
        this.getChildrenTag().add(child);
        return child;
    }

    /**
     * Добавить атрибут тега
     *
     * @param key   Имя атрибута
     * @param value Значение атрибута
     * @return
     */
    public XmlNode addAttribute(String key, String value) {
        this.getListAttribute().put(key, value);
        return this;
    }

    /**
     * Получить Полное содержимое тега
     *
     * @return
     */
    public String bPF() {
        ByteArrayOutputStream var1 = new ByteArrayOutputStream();
        PrintWriter var2 = new PrintWriter(var1);
        this.d(var2);
        var2.flush();
        return new String(var1.toByteArray());
    }

    /**
     * Получить Полное содержимое тега
     *
     * @param var1
     */
    public void d(PrintWriter var1) {
        this.b(EMPTY_STRING, var1);
    }

    /**
     * Получить Полное содержимое тега
     *
     * @param var1
     * @param var2
     */
    private void b(String var1, PrintWriter var2) {
        var2.append(var1);
        String var3;
        if (this.name != null) {
            var3 = mA(this.name);
            var2.append("<").append(var3);
            Set var4 = this.getListAttribute().entrySet();

            Entry var5;
            for (Iterator var6 = var4.iterator(); var6.hasNext(); var2.append(" ").append((CharSequence) var5.getKey()).append("=").append('"').append((CharSequence) var5.getValue()).append('"')) {
                var5 = (Entry) var6.next();
                if (var4.size() > 1) {
                    var2.append("\n" + var1 + "  ");
                }
            }
        } else {
            var3 = null;
        }

        if (this.listChildrenTag != null && !this.getChildrenTag().isEmpty()) {
            var2.append(">").append("\n");
            Iterator var8 = this.getChildrenTag().iterator();

            while (var8.hasNext()) {
                XmlNode var7 = (XmlNode) var8.next();
                var7.b(var1 + "   ", var2);
                var2.append("\n");
            }

            if (this.text != null) {
                var2.append(var1).append("   ").append(this.text).append("\n");
            }

            var2.append(var1);
        } else {
            if (this.text == null || this.text.trim().length() == 0) {
                var2.append(" />");
                return;
            }

            var2.append(">").append(this.text);
        }

        if (this.name != null) {
            var2.append("</").append(var3).append(">");
        }

    }

    public XmlNode aP(String var1, String var2) {
        return this.o((String) null, var1, var2);
    }

    public XmlNode mC(String var1) {
        return this.c(0, var1, (String) null, (String) null);
    }

    public XmlNode o(String var1, String var2, String var3) {
        return this.c(0, var1, var2, var3);
    }

    public XmlNode aQ(String var1, String var2) {
        return this.p((String) null, var1, var2);
    }

    public XmlNode mD(String var1) {
        return this.c(Integer.MAX_VALUE, var1, (String) null, (String) null);
    }

    public XmlNode p(String var1, String var2, String var3) {
        XmlNode var4 = this.c(Integer.MAX_VALUE, var1, var2, var3);
        if (var4 == null) {
            logger.error("element " + var1 + " with " + var2 + " of value " + var3 + " not found!");
        }

        return var4;
    }

    public XmlNode c(int var1, String var2, String var3, String var4) {
        if (this.listChildrenTag == null) {
            return null;
        } else {
            Iterator var6 = this.getChildrenTag().iterator();

            while (var6.hasNext()) {
                XmlNode var5 = (XmlNode) var6.next();
                boolean var7 = var2 == null || var2.equals(var5.getTegName());
                boolean var8 = var3 == null || var4.equals(var5.getAttribute(var3));
                if (var7 && var8) {
                    return var5;
                }

                if (var1 > 0) {
                    XmlNode var9 = var5.c(var1 - 1, var2, var3, var4);
                    if (var9 != null) {
                        return var9;
                    }
                }
            }

            return null;
        }
    }

    public List aR(String var1, String var2) {
        return this.q((String) null, var1, var2);
    }

    public List aS(String var1, String var2) {
        return this.d(0, (String) null, var1, var2);
    }

    public List mE(String var1) {
        return this.q(var1, (String) null, (String) null);
    }

    public List q(String var1, String var2, String var3) {
        return this.d(Integer.MAX_VALUE, var1, var2, var3);
    }

    public List mF(String var1) {
        return this.d(0, var1, (String) null, (String) null);
    }

    public List g(int var1, String var2) {
        return this.d(var1, var2, (String) null, (String) null);
    }

    public List d(int var1, String var2, String var3, String var4) {
        LinkedList var5 = new LinkedList();
        this.a(var1, var5, var2, var3, var4);
        return var5;
    }

    public void a(int var1, List var2, String var3, String var4, String var5) {
        if (this.listChildrenTag != null) {
            Iterator var7 = this.getChildrenTag().iterator();

            while (var7.hasNext()) {
                XmlNode var6 = (XmlNode) var7.next();
                boolean var8 = var3 == null || var3.equals(var6.getTegName());
                boolean var9 = var4 == null || var5.equals(var6.getAttribute(var4));
                if (var8 && var9) {
                    var2.add(var6);
                }

                if (var1 > 0) {
                    var6.a(var1 - 1, var2, var3, var4, var5);
                }
            }

        }
    }

    public String getAttribute(String var1) {
        return this.getAttribute(var1, (String) null);
    }

    public String getAttribute(String var1, String var2) {
        String var3 = (String) this.getListAttribute().get(var1);
        return var3 == null ? var2 : var3;
    }

    public String toString() {
        StringBuilder var1 = new StringBuilder();
        var1.append("<").append(this.name);
        Set var2 = this.getListAttribute().entrySet();
        Iterator var4 = var2.iterator();

        while (var4.hasNext()) {
            Entry var3 = (Entry) var4.next();
            var1.append(' ').append((String) var3.getKey()).append("=").append('"').append((String) var3.getValue()).append('"');
        }

        if (this.listChildrenTag != null && !this.getChildrenTag().isEmpty()) {
            var1.append(">").append("...");
        } else {
            if (this.text == null) {
                var1.append(" />");
                return var1.toString();
            }

            var1.append(">...");
        }

        var1.append("</").append(this.name).append(">");
        return var1.toString();
    }

    public String dcG() {
        if (this.listChildrenTag != null && !this.getChildrenTag().isEmpty()) {
            StringWriter var1 = new StringWriter();
            PrintWriter var2 = new PrintWriter(var1);
            Iterator var4 = this.listChildrenTag.iterator();

            while (var4.hasNext()) {
                XmlNode var3 = (XmlNode) var4.next();
                var3.d(var2);
            }

            var2.flush();
            return var1.toString();
        } else {
            return EMPTY_STRING;
        }
    }

    public Object getUserData() {
        return this.userData;
    }

    public void setUserData(Object var1) {
        this.userData = var1;
    }

    public void mG(String var1) {
        if (this.listChildrenTag != null) {
            Iterator var2 = this.getChildrenTag().iterator();

            while (var2.hasNext()) {
                XmlNode var3 = (XmlNode) var2.next();
                if (var1.equals(var3.getTegName())) {
                    var2.remove();
                    return;
                }
            }

        }
    }

    public int count() {
        if (this.listChildrenTag == null) {
            return 0;
        } else {
            int var1 = this.getChildrenTag().size();

            XmlNode var2;
            for (Iterator var3 = this.getChildrenTag().iterator(); var3.hasNext(); var1 += var2.count()) {
                var2 = (XmlNode) var3.next();
            }

            return var1;
        }
    }

    private Map getListAttribute() {
        if (this.listAttribute == null) {
            this.listAttribute = new LinkedHashMap();
        }
        return this.listAttribute;
    }

    /**
     * Получить список всех атрибутов тега и их значение
     *
     * @return Пример {class=empty, transparent=true, alwaysOnTop=true, focusable=false, style=vertical-align:fill; text-align:fill, layout=GridLayout, css=taikodomversion.css}
     */
    public Map getAttributes() {
        return this.listAttribute != null ? Collections.unmodifiableMap(this.listAttribute) : Collections.EMPTY_MAP;
    }

    /**
     * Получить лист потомков
     *
     * @return
     */
    public List getChildrenTag() {
        if (this.listChildrenTag == null) {
            this.listChildrenTag = new ArrayList();
        }

        return this.listChildrenTag;
    }

    public void clearChildrenTag() {
        this.getChildrenTag().clear();
    }
}
