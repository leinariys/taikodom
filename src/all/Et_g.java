package all;

/**
 * Джостик локализация   JoystickHatEvent
 * class CommandTranslatorAddon
 */
public class Et_g extends aOG {
    public static adl cSc = new adl("JoystickHatEvent");
    private int button;
    private boolean aCJ;

    public Et_g(int var1, boolean var2) {
        this.button = var1;
        this.aCJ = var2;
    }

    public final int getButton() {
        return this.button;
    }

    public final boolean getState() {
        return this.aCJ;
    }

    public final boolean isPressed() {
        return this.aCJ;
    }

    public adl Fp() {
        return cSc;
    }
}
