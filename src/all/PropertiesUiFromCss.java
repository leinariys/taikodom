package all;

import org.w3c.dom.css.CSSPrimitiveValue;
import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.css.CSSValue;
import org.w3c.dom.css.CSSValueList;
import taikodom.render.TDesktop;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Свойства UI компонента заполненое из CSS
 */
public class PropertiesUiFromCss {
    private static final Color cds = new Color(0, 0, 0, 0);
    static Map cdt = new HashMap();
    static Map cdu = new HashMap();
    static Map cdv = new HashMap();
    static Map cdw = new HashMap();
    static Map cdx = new HashMap();

    static {
        cdw.put("super", vI.bzG);
        cdw.put("top", vI.bzG);
        cdw.put("bottom", vI.bzH);
        cdw.put("middle", vI.bzI);
        cdw.put("center", vI.bzI);
        cdw.put("fill", vI.bzJ);
        cdx.put("ltr", ComponentOrientation.LEFT_TO_RIGHT);
        cdx.put("rtl", ComponentOrientation.RIGHT_TO_LEFT);
        cdt.put("left", new Tu(0.0F, Tu.a.ejc));
        cdt.put("top", new Tu(0.0F, Tu.a.ejc));
        cdt.put("center", new Tu(50.0F, Tu.a.ejc));
        cdt.put("bottom", new Tu(100.0F, Tu.a.ejc));
        cdt.put("right", new Tu(100.0F, Tu.a.ejc));
        cdu.put("stretch", xo.bFT);
        cdu.put("repeat", xo.bFP);
        cdu.put("repeat-x", xo.bFQ);
        cdu.put("repeat-x-stretch-y", xo.bFX);
        cdu.put("repeat-y", xo.bFR);
        cdu.put("repeat-y-stretch-x", xo.bFW);
        cdu.put("no-repeat", xo.bFS);
        cdv.put("middle", aRU.iLh);
        cdv.put("center", aRU.iLh);
        cdv.put("left", aRU.iLf);
        cdv.put("right", aRU.iLg);
        cdv.put("fill", aRU.iLi);
    }

    protected final aeK cdV;
    protected Color[] backgroundColor;
    protected Image backgroundFill;
    protected Image[] backgroundImage;
    protected jM_q[] backgroundPosition;
    protected xo[] backgroundRepeat;
    protected Boolean blurBehind;
    protected Image glowImage;
    protected Image borderBottomImage;
    protected xo borderBottomImageRepeat;
    protected Image borderBottomLeftImage;
    protected Image borderBottomRightImage;
    protected Color borderColorMultiplier;
    protected Image borderLeftImage;
    protected xo borderLeftImageRepeat;
    protected Image borderRightImage;
    protected xo borderRightImageRepeat;
    protected jM_q borderSpacing;
    protected String borderStyle;
    protected Image borderTopImage;
    protected xo borderTopImageRepeat;
    protected Image borderTopLeftImage;
    protected Image borderTopRightImage;
    protected Cursor cursor;
    protected Color color;
    protected Color colorMultiplier;
    protected Font font;//Шрифт
    protected int hash;
    protected Tu height;
    protected aRU textAlign;
    protected Image iconImage;
    protected Icon iconIcon;
    protected Tu left;
    protected Image borderLeftFill;
    protected Tu marginBottom;
    protected Tu marginLeft;
    protected Tu marginRight;
    protected Tu marginTop;
    protected Tu maxHeight;
    protected Tu maxWidth;
    protected Tu minHeight;
    protected Dimension cei;
    protected Tu minWidth;
    protected Tu paddingBottom;
    protected Tu paddingLeft;
    protected Tu paddingRight;
    protected Tu paddingTop;
    protected Image borderRightFill;
    protected Color shadowColor;
    protected Tu fontShadowX;
    protected Tu fontShadowY;
    protected Tu top;
    protected vI verticalAlign;
    protected Tu width;
    private String mouseOverSound;
    private String mousePressSound;
    private String mouseReleaseSound;
    private String mouseClickSound;
    private String keyPressSound;
    private String keyReleaseSound;
    private String keyTypedSound;
    private String actionSound;
    private String dropSucessSound;
    private String dropFailSound;
    private String dragStartSound;
    private String openSound;
    private String closeSound;
    private String collapseSound;
    private String expandSound;
    private String scrollSound;
    private String typeAnimationSound;
    private ComponentOrientation direction;

    public PropertiesUiFromCss(aeK var1) {
        this.cdV = var1;
        this.setCssToUI(new aca(var1), true);//Присваение параметров из стиля css системе событий
    }

    public PropertiesUiFromCss(aeK var1, CSSStyleDeclaration var2) {
        this.cdV = var1;
        if (var2 != null) {
            this.setCssToUI(new aca(var1, var2), false);
        }

    }

    public static PropertiesUiFromCss f(Component var0) {
        aeK var1 = ComponentManager.getCssHolder(var0);
        return var1 != null ? var1.Vp() : null;
    }

    //Находим систему событий
    public static PropertiesUiFromCss g(Component var0) {
        aeK var1 = ComponentManager.getCssHolder(var0);
        return var1 != null ? var1.Vp() : null;
    }

    public static PropertiesUiFromCss a(ComponentUI var0) {
        return var0 instanceof aIh ? ((aIh) var0).wz().Vp() : null;
    }

    public static TDesktop h(Component var0) {
        while (!(var0 instanceof TDesktop)) {
            if (var0 == null) {
                return null;
            }

            var0 = ((Component) var0).getParent();
        }

        return (TDesktop) var0;
    }

    public Color fS(int var1) {
        PropertiesUiFromCss var2 = this.cdV.Vr();
        if (var2 != null && var2 != this && var2.backgroundColor != null && var2.backgroundColor.length > var1) {
            return var2.backgroundColor[var1];
        } else {
            return this.backgroundColor != null && this.backgroundColor.length > var1 ? this.backgroundColor[var1] : cds;
        }
    }

    public Image atg() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        return var1 != null && var1 != this && var1.backgroundFill != null ? var1.backgroundFill : this.backgroundFill;
    }

    public Image fT(int var1) {
        PropertiesUiFromCss var2 = this.cdV.Vr();
        if (var2 != null && var2 != this && var2.backgroundImage != null && var2.backgroundImage.length > var1) {
            return var2.backgroundImage[var1];
        } else {
            return this.backgroundImage != null && this.backgroundImage.length > var1 ? this.backgroundImage[var1] : null;
        }
    }

    public jM_q fU(int var1) {
        PropertiesUiFromCss var2 = this.cdV.Vr();
        if (var2 != null && var2 != this && var2.backgroundPosition != null && var2.backgroundPosition.length > var1) {
            return var2.backgroundPosition[var1];
        } else {
            return this.backgroundPosition != null && this.backgroundPosition.length > var1 ? this.backgroundPosition[var1] : jM_q.apf;
        }
    }

    public xo fV(int var1) {
        PropertiesUiFromCss var2 = this.cdV.Vr();
        if (var2 != null && var2 != this && var2.backgroundRepeat != null && var2.backgroundRepeat.length > var1) {
            return var2.backgroundRepeat[var1];
        } else {
            return this.backgroundRepeat != null && this.backgroundRepeat.length > var1 ? this.backgroundRepeat[var1] : xo.bFT;
        }
    }

    public Boolean ath() {
        return this.blurBehind;
    }

    public Image ati() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        return var1 != null && var1 != this && var1.borderBottomImage != null ? var1.borderBottomImage : this.borderBottomImage;
    }

    public Image atj() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        return var1 != null && var1 != this && var1.glowImage != null ? var1.glowImage : this.glowImage;
    }

    public xo atk() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        return var1 != null && var1 != this && var1.borderBottomImageRepeat != null ? var1.borderBottomImageRepeat : this.borderBottomImageRepeat;
    }

    public Image atl() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        return var1 != null && var1 != this && var1.borderBottomLeftImage != null ? var1.borderBottomLeftImage : this.borderBottomLeftImage;
    }

    public Image atm() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        return var1 != null && var1 != this && var1.borderBottomRightImage != null ? var1.borderBottomRightImage : this.borderBottomRightImage;
    }

    public Color atn() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        return var1 != null && var1 != this && var1.borderColorMultiplier != null ? var1.borderColorMultiplier : this.borderColorMultiplier;
    }

    public Image ato() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        return var1 != null && var1 != this && var1.borderLeftImage != null ? var1.borderLeftImage : this.borderLeftImage;
    }

    public xo atp() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        return var1 != null && var1 != this && var1.borderLeftImageRepeat != null ? var1.borderLeftImageRepeat : this.borderLeftImageRepeat;
    }

    public Image atq() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        return var1 != null && var1 != this && var1.borderRightImage != null ? var1.borderRightImage : this.borderRightImage;
    }

    public xo atr() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        return var1 != null && var1 != this && var1.borderRightImageRepeat != null ? var1.borderRightImageRepeat : this.borderRightImageRepeat;
    }

    public jM_q ats() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        return var1 != null && var1 != this && var1.borderSpacing != null ? var1.borderSpacing : this.borderSpacing;
    }

    public String getBorderStyle() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        return var1 != null && var1 != this && var1.borderStyle != null ? var1.borderStyle : this.borderStyle;
    }

    public Image att() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        return var1 != null && var1 != this && var1.borderTopImage != null ? var1.borderTopImage : this.borderTopImage;
    }

    public xo atu() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        return var1 != null && var1 != this && var1.borderTopImageRepeat != null ? var1.borderTopImageRepeat : this.borderTopImageRepeat;
    }

    public Image atv() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        return var1 != null && var1 != this && var1.borderTopLeftImage != null ? var1.borderTopLeftImage : this.borderTopLeftImage;
    }

    public Image atw() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        return var1 != null && var1 != this && var1.borderTopRightImage != null ? var1.borderTopRightImage : this.borderTopRightImage;
    }

    public Color getColor() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        if (var1 != null && var1 != this && var1.color != null) {
            return var1.color;
        } else if (this.color == null) {
            aeK var2 = this.cdV.Vo();
            return var2 != null ? var2.Vp().getColor() : Color.white;
        } else {
            return this.color;
        }
    }

    public Color getColorMultiplier() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        return var1 != null && var1 != this && var1.colorMultiplier != null ? var1.colorMultiplier : this.colorMultiplier;
    }

    public void setColorMultiplier(Color var1) {
        this.colorMultiplier = var1;
        this.hash = 0;
    }

    public Cursor getCursor() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        return var1 != null && var1 != this && var1.cursor != null ? var1.cursor : this.cursor;
    }

    public Font getFont() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        return var1 != null && var1 != this && var1.font != null ? var1.font : this.font;
    }

    public Tu atx() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        return var1 != null && var1 != this && var1.height != null ? var1.height : this.height;
    }

    public aRU aty() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        return var1 != null && var1 != this && var1.textAlign != null ? var1.textAlign : this.textAlign;
    }

    public Icon getIcon() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        if (var1 != null && var1 != this && var1.iconImage != null) {
            return var1.getIcon();
        } else {
            if (this.iconImage != null && this.iconIcon == null) {
                this.iconIcon = new Dc(new ImageIcon(this.iconImage));
            }

            return this.iconIcon;
        }
    }

    public Image getIconImage() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        return var1 != null && var1 != this && var1.iconImage != null ? var1.iconImage : this.iconImage;
    }

    public Tu atz() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        return var1 != null && var1 != this && var1.left != null ? var1.left : this.left;
    }

    public Image atA() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        return var1 != null && var1 != this && var1.borderLeftFill != null ? var1.borderLeftFill : this.borderLeftFill;
    }

    public Tu atB() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        return var1 != null && var1 != this && var1.marginBottom != null ? var1.marginBottom : this.marginBottom;
    }

    public Tu atC() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        return var1 != null && var1 != this && var1.marginLeft != null ? var1.marginLeft : this.marginLeft;
    }

    public Tu atD() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        return var1 != null && var1 != this && var1.marginRight != null ? var1.marginRight : this.marginRight;
    }

    public Tu atE() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        return var1 != null && var1 != this && var1.marginTop != null ? var1.marginTop : this.marginTop;
    }

    public Tu atF() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        return var1 != null && var1 != this && var1.maxHeight != null ? var1.maxHeight : this.maxHeight;
    }

    public Tu atG() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        return var1 != null && var1 != this && var1.maxWidth != null ? var1.maxWidth : this.maxWidth;
    }

    public Tu atH() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        return var1 != null && var1 != this && var1.minHeight != null ? var1.minHeight : this.minHeight;
    }

    public Dimension getMinimumSize() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        return var1 != null && var1 != this && var1.cei != null ? var1.cei : this.cei;
    }

    public Tu atI() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        return var1 != null && var1 != this && var1.minWidth != null ? var1.minWidth : this.minWidth;
    }

    public Tu atJ() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        return var1 != null && var1 != this && var1.paddingBottom != null ? var1.paddingBottom : this.paddingBottom;
    }

    public Tu atK() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        return var1 != null && var1 != this && var1.paddingLeft != null ? var1.paddingLeft : this.paddingLeft;
    }

    public Tu atL() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        return var1 != null && var1 != this && var1.paddingRight != null ? var1.paddingRight : this.paddingRight;
    }

    public Tu atM() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        return var1 != null && var1 != this && var1.paddingTop != null ? var1.paddingTop : this.paddingTop;
    }

    public Image atN() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        return var1 != null && var1 != this && var1.borderRightFill != null ? var1.borderRightFill : this.borderRightFill;
    }

    public Color getShadowColor() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        if (var1 != null && var1 != this && var1.shadowColor != null) {
            return var1.shadowColor;
        } else if (this.shadowColor == null) {
            aeK var2 = this.cdV.Vo();
            return var2 != null ? var2.Vp().getShadowColor() : Color.black;
        } else {
            return this.shadowColor;
        }
    }

    public float atO() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        if (var1 != null && var1 != this && var1.fontShadowX != null) {
            return var1.atO();
        } else if (this.fontShadowX == null) {
            aeK var2 = this.cdV.Vo();
            return var2 != null ? var2.Vp().atO() : 0.0F;
        } else {
            return this.fontShadowX.value;
        }
    }

    public float atP() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        if (var1 != null && var1 != this && var1.fontShadowY != null) {
            return var1.atP();
        } else if (this.fontShadowY == null) {
            aeK var2 = this.cdV.Vo();
            return var2 != null ? var2.Vp().atP() : 0.0F;
        } else {
            return this.fontShadowY.value;
        }
    }

    public Tu atQ() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        return var1 != null && var1 != this && var1.top != null ? var1.top : this.top;
    }

    public vI atR() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        return var1 != null && var1 != this && var1.verticalAlign != null ? var1.verticalAlign : this.verticalAlign;
    }

    public Tu atS() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        return var1 != null && var1 != this && var1.width != null ? var1.width : this.width;
    }

    public int hashCode() {
        int var1 = this.hash;
        if (var1 == 0) {
            var1 = 206944958;
            var1 = var1 * 31 ^ RM.hashCode(this.cdV);
            var1 = var1 * 31 ^ RM.hashCode(this.backgroundColor);
            var1 = var1 * 31 ^ RM.hashCode(this.backgroundFill);
            var1 = var1 * 31 ^ RM.hashCode(this.backgroundImage);
            var1 = var1 * 31 ^ RM.hashCode(this.backgroundPosition);
            var1 = var1 * 31 ^ RM.hashCode(this.backgroundRepeat);
            var1 = var1 * 31 ^ RM.hashCode(this.blurBehind);
            var1 = var1 * 31 ^ RM.hashCode(this.borderColorMultiplier);
            var1 = var1 * 31 ^ RM.hashCode(this.borderSpacing);
            var1 = var1 * 31 ^ RM.hashCode(this.borderStyle);
            var1 = var1 * 31 ^ RM.hashCode(this.borderBottomImage);
            var1 = var1 * 31 ^ RM.hashCode(this.borderBottomLeftImage);
            var1 = var1 * 31 ^ RM.hashCode(this.borderBottomRightImage);
            var1 = var1 * 31 ^ RM.hashCode(this.color);
            var1 = var1 * 31 ^ RM.hashCode(this.colorMultiplier);
            var1 = var1 * 31 ^ RM.hashCode(this.font);
            var1 = var1 * 31 ^ RM.hashCode(this.height);
            var1 = var1 * 31 ^ RM.hashCode(this.textAlign);
            var1 = var1 * 31 ^ RM.hashCode(this.iconImage);
            var1 = var1 * 31 ^ RM.hashCode(this.iconIcon);
            var1 = var1 * 31 ^ RM.hashCode(this.left);
            var1 = var1 * 31 ^ RM.hashCode(this.borderLeftFill);
            var1 = var1 * 31 ^ RM.hashCode(this.borderLeftImage);
            var1 = var1 * 31 ^ RM.hashCode(this.marginBottom);
            var1 = var1 * 31 ^ RM.hashCode(this.marginLeft);
            var1 = var1 * 31 ^ RM.hashCode(this.marginRight);
            var1 = var1 * 31 ^ RM.hashCode(this.marginTop);
            var1 = var1 * 31 ^ RM.hashCode(this.maxHeight);
            var1 = var1 * 31 ^ RM.hashCode(this.maxWidth);
            var1 = var1 * 31 ^ RM.hashCode(this.minHeight);
            var1 = var1 * 31 ^ RM.hashCode(this.cei);
            var1 = var1 * 31 ^ RM.hashCode(this.minWidth);
            var1 = var1 * 31 ^ RM.hashCode(this.paddingBottom);
            var1 = var1 * 31 ^ RM.hashCode(this.paddingLeft);
            var1 = var1 * 31 ^ RM.hashCode(this.paddingRight);
            var1 = var1 * 31 ^ RM.hashCode(this.paddingTop);
            var1 = var1 * 31 ^ RM.hashCode(this.borderRightFill);
            var1 = var1 * 31 ^ RM.hashCode(this.borderRightImage);
            var1 = var1 * 31 ^ RM.hashCode(this.shadowColor);
            var1 = var1 * 31 ^ RM.hashCode(this.fontShadowX);
            var1 = var1 * 31 ^ RM.hashCode(this.fontShadowY);
            var1 = var1 * 31 ^ RM.hashCode(this.top);
            var1 = var1 * 31 ^ RM.hashCode(this.borderTopImage);
            var1 = var1 * 31 ^ RM.hashCode(this.borderTopLeftImage);
            var1 = var1 * 31 ^ RM.hashCode(this.borderTopRightImage);
            var1 = var1 * 31 ^ RM.hashCode(this.verticalAlign);
            var1 = var1 * 31 ^ RM.hashCode(this.width);
            if (var1 == 0) {
                var1 = 1;
            }

            this.hash = var1;
        }

        return var1;
    }

    //Присваение параметров из стиля css системе событий
    protected void setCssToUI(aca var1, boolean var2) {
        CSSValue var3 = var1.a(this.cdV, (String) "vertical-align", false);
        if (var3 != null) {//Параметр vertical-align в css стиле, если есть присваисаем  его
            String var4 = var3.getCssText().toLowerCase();
            this.verticalAlign = (vI) cdw.get(var4);
        }
        //Заглушка если параметр vertical-align в стиле не задан
        if (this.verticalAlign == null && var2) {
            this.verticalAlign = vI.bzI;
        }

        ILoaderImageInterface var21 = BaseUItegXML.thisClass.getLoaderImage();
        var3 = var1.a(this.cdV, (String) "text-align", false);
        String var5;
        if (var3 != null) {//Параметр в css стиле, если есть присваисаем  его
            var5 = var3.getCssText().toLowerCase();
            this.textAlign = (aRU) cdv.get(var5);
        }
        //Заглушка если параметр в стиле не задан
        if (this.textAlign == null && var2) {
            this.textAlign = aRU.iLh;
        }

        var5 = var1.e(this.cdV, "cursor", false);
        if (var5 != null) {//Параметр в css стиле, если есть присваисаем  его
            Tu var6 = var1.getValue(this.cdV, "cursor-top", false, Tu.fEE);
            Tu var7 = var1.getValue(this.cdV, "cursor-left", false, Tu.fEE);
            this.cursor = var21.getNull(var5, new Point((int) var7.value, (int) var6.value));
        }

        var3 = var1.a(this.cdV, (String) "direction", false);
        if (var3 != null) {//Параметр в css стиле, если есть присваисаем  его
            String var22 = var3.getCssText().toLowerCase();
            this.direction = (ComponentOrientation) cdx.get(var22);
        }

        if (this.direction == null && var2) {//Заглушка если параметр в стиле не задан
            this.direction = ComponentOrientation.UNKNOWN;
        }

        this.borderLeftImageRepeat = this.a(var1.a(this.cdV, (String) "border-left-image-repeat", false), var2 ? xo.bFV : null);
        this.borderRightImageRepeat = this.a(var1.a(this.cdV, (String) "border-right-image-repeat", false), var2 ? xo.bFV : null);
        this.borderBottomImageRepeat = this.a(var1.a(this.cdV, (String) "border-bottom-image-repeat", false), var2 ? xo.bFU : null);
        this.borderTopImageRepeat = this.a(var1.a(this.cdV, (String) "border-top-image-repeat", false), var2 ? xo.bFU : null);
        this.borderColorMultiplier = var1.g(this.cdV, "border-color-multiplier", false);
        this.color = var1.a(this.cdV, false);
        FontWrapper var23 = var1.parseFont(this.cdV);//работа со шрифтими
        if (var23 != null) {//Параметр в css стиле, если есть присваисаем  его
            this.font = BaseUItegXML.thisClass.getFont(var23.getFontName(), var23.getFontStyle(), var23.getFontSize());
        }

        var3 = var1.a(this.cdV, (String) "background-color", false);
        int var8;
        int var10;
        CSSValueList var24;
        if (var3 != null) {
            if (var3.getCssValueType() == 2) {
                var24 = (CSSValueList) var3;
                var8 = var24.getLength();
                Color[] var9 = new Color[var8];

                for (var10 = 0; var10 < var8; ++var10) {
                    var9[var10] = var1.d(var24.item(var10));
                }

                this.backgroundColor = var9;
            } else {
                this.backgroundColor = new Color[]{var1.d(var3)};
            }
        }

        this.mouseOverSound = var1.e(this.cdV, "mouse-over-sound", false);
        this.mousePressSound = var1.e(this.cdV, "mouse-press-sound", false);
        this.mouseReleaseSound = var1.e(this.cdV, "mouse-release-sound", false);
        this.keyPressSound = var1.e(this.cdV, "key-press-sound", false);
        this.keyReleaseSound = var1.e(this.cdV, "key-release-sound", false);
        this.keyTypedSound = var1.e(this.cdV, "key-typed-sound", false);
        this.mouseClickSound = var1.e(this.cdV, "mouse-click-sound", false);
        this.actionSound = var1.e(this.cdV, "action-sound", false);
        this.dropSucessSound = var1.e(this.cdV, "drop-sucess-sound", false);
        this.dropFailSound = var1.e(this.cdV, "drop-fail-sound", false);
        this.dragStartSound = var1.e(this.cdV, "drag-start-sound", false);
        this.openSound = var1.e(this.cdV, "open-sound", false);
        this.closeSound = var1.e(this.cdV, "close-sound", false);
        this.collapseSound = var1.e(this.cdV, "collapse-sound", false);
        this.expandSound = var1.e(this.cdV, "expand-sound", false);
        this.scrollSound = var1.e(this.cdV, "scroll-sound", false);
        this.typeAnimationSound = var1.e(this.cdV, "type-animation-sound", false);
        this.borderStyle = var1.e(this.cdV, "border-style", false);
        this.borderTopLeftImage = var21.getImage(var1.e(this.cdV, "border-top-left-image", false));
        this.borderTopRightImage = var21.getImage(var1.e(this.cdV, "border-top-right-image", false));
        this.borderTopImage = var21.getImage(var1.e(this.cdV, "border-top-image", false));
        var3 = var1.a(this.cdV, (String) "background-image", false);
        if (var3 != null) {
            if (var3.getCssValueType() == 2) {
                var24 = (CSSValueList) var3;
                var8 = var24.getLength();
                Image[] var27 = new Image[var8];

                for (var10 = 0; var10 < var8; ++var10) {
                    var27[var10] = var21.getImage(this.a(var24.item(var10)));
                }

                this.backgroundImage = var27;
            } else {
                this.backgroundImage = new Image[]{var21.getImage(this.a(var3))};
            }
        }

        this.backgroundRepeat = this.a(var1.a(this.cdV, (String) "background-repeat", false), (xo[]) null);
        this.glowImage = var21.getImage(var1.e(this.cdV, "glow-image", false));
        this.borderLeftImage = var21.getImage(var1.e(this.cdV, "border-left-image", false));
        this.borderRightImage = var21.getImage(var1.e(this.cdV, "border-right-image", false));
        this.borderBottomImage = var21.getImage(var1.e(this.cdV, "border-bottom-image", false));
        this.borderBottomLeftImage = var21.getImage(var1.e(this.cdV, "border-bottom-left-image", false));
        this.borderBottomRightImage = var21.getImage(var1.e(this.cdV, "border-bottom-right-image", false));
        this.backgroundFill = var21.getImage(var1.e(this.cdV, "background-fill", false));
        this.borderLeftFill = var21.getImage(var1.e(this.cdV, "border-left-fill", false));
        this.borderRightFill = var21.getImage(var1.e(this.cdV, "border-right-fill", false));
        String var25 = var1.e(this.cdV, "blur-behind", false);
        if (var25 != null) {
            this.blurBehind = "true".equals(var25);
        }

        this.iconImage = var21.getImage(var1.e(this.cdV, "iconImage", false));
        Tu var26 = var2 ? Tu.fEC : null;
        this.marginTop = var1.getValue(this.cdV, "margin-top", false, var26);
        this.marginBottom = var1.getValue(this.cdV, "margin-bottom", false, var26);
        this.marginLeft = var1.getValue(this.cdV, "margin-left", false, var26);
        this.marginRight = var1.getValue(this.cdV, "margin-right", false, var26);
        this.paddingTop = var1.getValue(this.cdV, "padding-top", false, var26);
        this.paddingBottom = var1.getValue(this.cdV, "padding-bottom", false, var26);
        this.paddingLeft = var1.getValue(this.cdV, "padding-left", false, var26);
        this.paddingRight = var1.getValue(this.cdV, "padding-right", false, var26);
        this.colorMultiplier = var1.g(this.cdV, "color-multiplier", false);
        this.fontShadowX = var1.getValue(this.cdV, "font-shadow-x", false, var26);
        this.fontShadowY = var1.getValue(this.cdV, "font-shadow-y", false, var26);
        this.maxHeight = var1.getValue(this.cdV, "max-height", false, var26);
        this.maxWidth = var1.getValue(this.cdV, "max-width", false, var26);
        this.borderSpacing = var1.e(var1.a(this.cdV, (String) "border-spacing", false));
        CSSValue var11 = var1.a(this.cdV, (String) "background-position", false);
        if (var11 != null) {
            Tu var12 = null;
            Tu var13 = null;
            ArrayList var14 = new ArrayList();
            int var15 = 1;
            if (var11.getCssValueType() == 2) {
                var15 = ((CSSValueList) var11).getLength();
            }

            for (int var16 = 0; var16 < var15; ++var16) {
                CSSValue var17;
                if (var11.getCssValueType() == 2) {
                    var17 = ((CSSValueList) var11).item(var16);
                } else {
                    var17 = var11;
                }

                if (var17.getCssValueType() == 1) {
                    CSSPrimitiveValue var18 = (CSSPrimitiveValue) var17;
                    String var19 = var18.getCssText().toLowerCase();
                    Tu var20 = (Tu) cdt.get(var19);
                    if (var20 == null) {
                        if (var16 == 0) {
                            var12 = avI.c(var17);
                        } else if (var16 == 1) {
                            var13 = avI.c(var17);
                        }
                    } else {
                        if ("left".equals(var19) || "right".equals(var19)) {
                            var12 = var20;
                            if (var13 == null) {
                                var13 = new Tu(50.0F, Tu.a.ejc);
                            }
                        }

                        if ("center".equals(var19)) {
                            if (var12 == null) {
                                var12 = var20;
                            }

                            if (var13 == null) {
                                var13 = var20;
                            }
                        }

                        if ("bottom".equals(var19) || "top".equals(var19)) {
                            var13 = var20;
                            if (var12 == null) {
                                var12 = new Tu(50.0F, Tu.a.ejc);
                            }
                        }
                    }
                }

                if ((var16 & 1) != 0 || var16 == var15 - 1) {
                    jM_q var32 = new jM_q(var12 == null ? Tu.fEE : var12, var13 == null ? Tu.fEE : var13);
                    var14.add(var32);
                }
            }

            this.backgroundPosition = (jM_q[]) var14.toArray(new jM_q[var14.size()]);
        }

        Tu var28;
        this.minHeight = var28 = var1.getValue(this.cdV, "min-height", false, var26);
        Tu var29;
        this.minWidth = var29 = var1.getValue(this.cdV, "min-width", false, var26);
        if (var28 != null && var29 != null && var28 != Tu.fEC && var29 != Tu.fEC) {
            int var30 = (int) var28.value;
            int var31 = (int) var29.value;
            this.cei = new Dimension(var31, var30);
        }

        this.height = var1.getValue(this.cdV, "height", false, var26);
        this.width = var1.getValue(this.cdV, "width", false, var26);
        this.top = var1.getValue(this.cdV, "top", false, var26);
        this.left = var1.getValue(this.cdV, "left", false, var26);
    }

    private String a(CSSValue var1) {
        if (var1 == null) {
            return null;
        } else if (var1.getCssValueType() != 1) {
            return null;
        } else {
            CSSPrimitiveValue var2 = (CSSPrimitiveValue) var1;
            return var2.getStringValue();
        }
    }

    private jM_q b(CSSValue var1) {
        Tu var2 = null;
        Tu var3 = null;

        for (int var4 = 0; var4 < 2; ++var4) {
            CSSValue var5;
            if (var1.getCssValueType() == 2) {
                var5 = ((CSSValueList) var1).item(var4);
            } else {
                if (var4 > 0) {
                    break;
                }

                var5 = var1;
            }

            if (var5.getCssValueType() == 1) {
                CSSPrimitiveValue var6 = (CSSPrimitiveValue) var5;
                String var7 = var6.getCssText().toLowerCase();
                Tu var8 = (Tu) cdt.get(var7);
                if (var8 != null) {
                    if ("left".equals(var7) || "right".equals(var7)) {
                        var2 = var8;
                        if (var3 == null) {
                            var3 = new Tu(50.0F, Tu.a.ejc);
                        }
                    }

                    if ("center".equals(var7)) {
                        if (var2 == null) {
                            var2 = var8;
                        }

                        if (var3 == null) {
                            var3 = var8;
                        }
                    }

                    if ("bottom".equals(var7) || "top".equals(var7)) {
                        var3 = var8;
                        if (var2 == null) {
                            var2 = new Tu(50.0F, Tu.a.ejc);
                        }
                    }
                } else if (var4 == 0) {
                    var2 = avI.c(var5);
                } else if (var4 == 1) {
                    var3 = avI.c(var5);
                }
            }
        }

        jM_q var9 = new jM_q(var2 == null ? Tu.fEE : var2, var3 == null ? Tu.fEE : var3);
        return var9;
    }

    private xo a(CSSValue var1, xo var2) {
        if (var1 != null) {
            String var3 = var1.getCssText().toLowerCase();
            xo var4 = (xo) cdu.get(var3);
            return var4 == null ? var2 : var4;
        } else {
            return var2;
        }
    }

    private xo[] a(CSSValue var1, xo[] var2) {
        if (var1 == null) {
            return var2;
        } else if (var1.getCssValueType() != 2) {
            return new xo[]{this.a(var1, xo.bFT)};
        } else {
            CSSValueList var3 = (CSSValueList) var1;
            int var4 = var3.getLength();
            xo[] var5 = new xo[var4];

            for (int var6 = 0; var6 < var4; ++var6) {
                var5[var6] = this.a(var3.item(var6), xo.bFT);
            }

            return var5;
        }
    }

    public void a(Image var1) {
        if (this.backgroundImage == null) {
            this.backgroundImage = new Image[]{var1};
        } else {
            this.backgroundImage[0] = var1;
        }

    }

    public int atT() {
        PropertiesUiFromCss var1 = this.cdV.Vr();
        if (var1 != null && var1 != this && var1.backgroundImage != null) {
            return var1.backgroundImage.length;
        } else {
            return this.backgroundImage != null ? this.backgroundImage.length : 0;
        }
    }

    public ComponentOrientation getComponentOrientation() {
        return this.direction;
    }

    public String atU() {
        PropertiesUiFromCss var1 = this.cdV.Vp();
        return var1 != null && var1 != this && var1.mouseOverSound != null ? var1.mouseOverSound : this.mouseOverSound;
    }

    public String atV() {
        PropertiesUiFromCss var1 = this.cdV.Vp();
        return var1 != null && var1 != this && var1.mousePressSound != null ? var1.mousePressSound : this.mousePressSound;
    }

    public String atW() {
        PropertiesUiFromCss var1 = this.cdV.Vp();
        return var1 != null && var1 != this && var1.mouseReleaseSound != null ? var1.mouseReleaseSound : this.mouseReleaseSound;
    }

    public String atX() {
        PropertiesUiFromCss var1 = this.cdV.Vp();
        return var1 != null && var1 != this && var1.mouseClickSound != null ? var1.mouseClickSound : this.mouseClickSound;
    }

    public String atY() {
        PropertiesUiFromCss var1 = this.cdV.Vp();
        return var1 != null && var1 != this && var1.keyReleaseSound != null ? var1.keyReleaseSound : this.keyReleaseSound;
    }

    public String atZ() {
        PropertiesUiFromCss var1 = this.cdV.Vp();
        return var1 != null && var1 != this && var1.keyPressSound != null ? var1.keyPressSound : this.keyPressSound;
    }

    public String aua() {
        PropertiesUiFromCss var1 = this.cdV.Vp();
        return var1 != null && var1 != this && var1.keyTypedSound != null ? var1.keyTypedSound : this.keyTypedSound;
    }

    public String aub() {
        PropertiesUiFromCss var1 = this.cdV.Vp();
        return var1 != null && var1 != this && var1.actionSound != null ? var1.actionSound : this.actionSound;
    }

    public String auc() {
        PropertiesUiFromCss var1 = this.cdV.Vp();
        return var1 != null && var1 != this && var1.dropSucessSound != null ? var1.dropSucessSound : this.dropSucessSound;
    }

    public String aud() {
        PropertiesUiFromCss var1 = this.cdV.Vp();
        return var1 != null && var1 != this && var1.dropFailSound != null ? var1.dropFailSound : this.dropFailSound;
    }

    public String aue() {
        PropertiesUiFromCss var1 = this.cdV.Vp();
        return var1 != null && var1 != this && var1.dragStartSound != null ? var1.dragStartSound : this.dragStartSound;
    }

    public String auf() {
        PropertiesUiFromCss var1 = this.cdV.Vp();
        return var1 != null && var1 != this && var1.openSound != null ? var1.openSound : this.openSound;
    }

    public String aug() {
        PropertiesUiFromCss var1 = this.cdV.Vp();
        return var1 != null && var1 != this && var1.expandSound != null ? var1.expandSound : this.expandSound;
    }

    public String auh() {
        PropertiesUiFromCss var1 = this.cdV.Vp();
        return var1 != null && var1 != this && var1.closeSound != null ? var1.closeSound : this.closeSound;
    }

    public String aui() {
        PropertiesUiFromCss var1 = this.cdV.Vp();
        return var1 != null && var1 != this && var1.collapseSound != null ? var1.collapseSound : this.collapseSound;
    }

    public String auj() {
        PropertiesUiFromCss var1 = this.cdV.Vp();
        return var1 != null && var1 != this && var1.scrollSound != null ? var1.scrollSound : this.scrollSound;
    }

    public String auk() {
        PropertiesUiFromCss var1 = this.cdV.Vp();
        return var1 != null && var1 != this && var1.typeAnimationSound != null ? var1.typeAnimationSound : this.typeAnimationSound;
    }
}
