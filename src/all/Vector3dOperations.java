package all;

import com.hoplon.geometry.Vec3f;

import javax.vecmath.Tuple3d;
import javax.vecmath.Tuple3f;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

//aJR
public class Vector3dOperations extends Vector3d implements afA_q, ti {


    public Vector3dOperations() {
    }

    public Vector3dOperations(Vector3dOperations var1) {
        this.x = var1.x;
        this.y = var1.y;
        this.z = var1.z;
    }

    public Vector3dOperations(IV var1) {
        this.x = var1.x;
        this.y = var1.y;
        this.z = var1.z;
    }

    public Vector3dOperations(double var1, double var3, double var5) {
        this.x = var1;
        this.y = var3;
        this.z = var5;
    }

    public Vector3dOperations(Vec3f var1) {
        this.x = (double) var1.x;
        this.y = (double) var1.y;
        this.z = (double) var1.z;
    }

    public static double n(Vector3dOperations var0, Vector3dOperations var1) {
        return Math.sqrt((var0.x - var1.x) * (var0.x - var1.x) + (var0.y - var1.y) * (var0.y - var1.y) + (var0.z - var1.z) * (var0.z - var1.z));
    }

    public static double o(Vector3dOperations var0, Vector3dOperations var1) {
        return (var0.x - var1.x) * (var0.x - var1.x) + (var0.y - var1.y) * (var0.y - var1.y) + (var0.z - var1.z) * (var0.z - var1.z);
    }

    public static float p(Vector3dOperations var0, Vector3dOperations var1) {
        return (float) ((var0.x - var1.x) * (var0.x - var1.x) + (var0.y - var1.y) * (var0.y - var1.y) + (var0.z - var1.z) * (var0.z - var1.z));
    }

    public static Vector3dOperations f(Tuple3d var0, Tuple3d var1) {
        return new Vector3dOperations(var0.x - var1.x, var0.y - var1.y, var0.z - var1.z);
    }

    public static Vector3dOperations c(Vector3dOperations var0, Vector3dOperations var1, Vector3dOperations var2) {
        Vector3dOperations var3 = var2.f((Tuple3d) var0);
        Vector3dOperations var4 = var1.f((Tuple3d) var0);
        var4.cox();
        double var5 = var0.f((Tuple3d) var1).length();
        double var7 = var4.az(var3);
        if (var7 <= 0.0D) {
            return var0;
        } else if (var7 >= var5) {
            return var1;
        } else {
            Vector3dOperations var9 = var4.Z(var7);
            Vector3dOperations var10 = var0.d((Tuple3d) var9);
            return var10;
        }
    }

    public static Vector3dOperations i(ajK var0, Tuple3f var1) {
        float var2 = var1.x;
        float var3 = var1.y;
        float var4 = var1.z;
        float var5 = var2 * var0.m00 + var3 * var0.m01 + var4 * var0.m02;
        float var6 = var2 * var0.m10 + var3 * var0.m11 + var4 * var0.m12;
        float var7 = var2 * var0.m20 + var3 * var0.m21 + var4 * var0.m22;
        return new Vector3dOperations((double) var5, (double) var6, (double) var7);
    }

    public static Vector3dOperations e(ajK var0, Tuple3d var1) {
        double var2 = var1.x;
        double var4 = var1.y;
        double var6 = var1.z;
        double var8 = var2 * (double) var0.m00 + var4 * (double) var0.m01 + var6 * (double) var0.m02;
        double var10 = var2 * (double) var0.m10 + var4 * (double) var0.m11 + var6 * (double) var0.m12;
        double var12 = var2 * (double) var0.m20 + var4 * (double) var0.m21 + var6 * (double) var0.m22;
        return new Vector3dOperations(var8, var10, var12);
    }

    public static Vector3dOperations f(ajK var0, Tuple3d var1) {
        double var2 = var1.x;
        double var4 = var1.y;
        double var6 = var1.z;
        double var8 = var2 * (double) var0.m00 + var4 * (double) var0.m01 + var6 * (double) var0.m02 + (double) var0.m03;
        double var10 = var2 * (double) var0.m10 + var4 * (double) var0.m11 + var6 * (double) var0.m12 + (double) var0.m13;
        double var12 = var2 * (double) var0.m20 + var4 * (double) var0.m21 + var6 * (double) var0.m22 + (double) var0.m23;
        return new Vector3dOperations(var8, var10, var12);
    }

    public static Vector3dOperations j(ajK var0, Tuple3f var1) {
        float var2 = var1.x;
        float var3 = var1.y;
        float var4 = var1.z;
        float var5 = var2 * var0.m00 + var3 * var0.m01 + var4 * var0.m02 + var0.m03;
        float var6 = var2 * var0.m10 + var3 * var0.m11 + var4 * var0.m12 + var0.m13;
        float var7 = var2 * var0.m20 + var3 * var0.m21 + var4 * var0.m22 + var0.m23;
        return new Vector3dOperations((double) var5, (double) var6, (double) var7);
    }

    public static Vector3dOperations d(Vector3dOperations var0, Vector3dOperations var1, Vector3dOperations var2) {
        Vector3dOperations var3 = var1.f((Tuple3d) var0);
        double var4 = var3.length();
        var3 = var3.Z(1.0D / var4);
        Vector3dOperations var6 = var2.f((Tuple3d) var0);
        double var7 = var3.az(var6);
        if (var7 <= 0.0D) {
            return new Vector3dOperations(var0);
        } else {
            return var7 >= 1.0D ? new Vector3dOperations(var1) : var0.d((Tuple3d) var3.Z(var7 * var4));
        }
    }

    public static double e(Vector3dOperations var0, Vector3dOperations var1, Vector3dOperations var2) {
        Vector3dOperations var3 = d(var0, var1, var2);
        return n(var3, var2);
    }

    public double ax(Vector3dOperations var1) {
        return Math.sqrt((this.x - var1.x) * (this.x - var1.x) + (this.y - var1.y) * (this.y - var1.y) + (this.z - var1.z) * (this.z - var1.z));
    }

    public Vector3dOperations Z(double var1) {
        Vector3dOperations var3 = new Vector3dOperations();
        var3.x = this.x * var1;
        var3.y = this.y * var1;
        var3.z = this.z * var1;
        return var3;
    }

    public Vector3dOperations aa(double var1) {
        this.scale(var1);
        return this;
    }

    public Vector3dOperations d(Tuple3d var1) {
        Vector3dOperations var2 = new Vector3dOperations();
        var2.x = this.x + var1.x;
        var2.y = this.y + var1.y;
        var2.z = this.z + var1.z;
        return var2;
    }

    public Vector3dOperations e(Tuple3d var1) {
        super.add(var1);
        return this;
    }

    public Vector3dOperations q(Tuple3f var1) {
        Vector3dOperations var2 = new Vector3dOperations();
        var2.x = this.x + (double) var1.x;
        var2.y = this.y + (double) var1.y;
        var2.z = this.z + (double) var1.z;
        return var2;
    }

    public Vector3dOperations r(Tuple3f var1) {
        this.x += (double) var1.x;
        this.y += (double) var1.y;
        this.z += (double) var1.z;
        return this;
    }

    public void add(Tuple3f var1) {
        this.x += (double) var1.x;
        this.y += (double) var1.y;
        this.z += (double) var1.z;
    }

    public Vector3dOperations f(Tuple3d var1) {
        double var2 = this.x - var1.x;
        double var4 = this.y - var1.y;
        double var6 = this.z - var1.z;
        return new Vector3dOperations(var2, var4, var6);
    }

    public Vector3dOperations s(Tuple3f var1) {
        double var2 = this.x - (double) var1.x;
        double var4 = this.y - (double) var1.y;
        double var6 = this.z - (double) var1.z;
        return new Vector3dOperations(var2, var4, var6);
    }

    public Vector3dOperations g(Tuple3d var1) {
        this.x -= var1.x;
        this.y -= var1.y;
        this.z -= var1.z;
        return this;
    }

    public Vector3dOperations e(Tuple3d var1, Tuple3d var2) {
        this.x = var1.x - var2.x;
        this.y = var1.y - var2.y;
        this.z = var1.z - var2.z;
        return this;
    }

    public Vector3dOperations t(Tuple3f var1) {
        this.x -= (double) var1.x;
        this.y -= (double) var1.y;
        this.z -= (double) var1.z;
        return this;
    }

    public void sub(Tuple3f var1) {
        this.x -= (double) var1.x;
        this.y -= (double) var1.y;
        this.z -= (double) var1.z;
    }

    public Vector3dOperations o(double var1, double var3, double var5) {
        this.x -= var1;
        this.y -= var3;
        this.z -= var5;
        return this;
    }

    public Vector3dOperations p(double var1, double var3, double var5) {
        Vector3dOperations var7 = new Vector3dOperations();
        var7.x = this.x - var1;
        var7.y = this.y - var3;
        var7.z = this.z - var5;
        return var7;
    }

    public void q(double var1, double var3, double var5) {
        this.x -= var1;
        this.y -= var3;
        this.z -= var5;
    }

    public void cox() {
        double var1 = this.lengthSquared();
        if (var1 > 0.0D && var1 != 1.0D) {
            var1 = Math.sqrt(var1);
            this.x /= var1;
            this.y /= var1;
            this.z /= var1;
        }

    }

    public Vector3dOperations b(Vector3d var1) {
        return new Vector3dOperations(this.y * var1.z - this.z * var1.y, this.z * var1.x - this.x * var1.z, this.x * var1.y - this.y * var1.x);
    }

    public Vector3dOperations e(Vector3f var1) {
        return new Vector3dOperations(this.y * (double) var1.z - this.z * (double) var1.y, this.z * (double) var1.x - this.x * (double) var1.z, this.x * (double) var1.y - this.y * (double) var1.x);
    }

    public String toString() {
        return "(" + this.x + ", " + this.y + ", " + this.z + ")";
    }

    public Vector3dOperations dfW() {
        double var1 = this.length();
        if (var1 > 0.0D) {
            double var3 = 1.0D / var1;
            double var5 = this.x * var3;
            double var7 = this.y * var3;
            double var9 = this.z * var3;
            return new Vector3dOperations(var5, var7, var9);
        } else {
            return new Vector3dOperations(this);
        }
    }

    public Vector3dOperations dfX() {
        double var1 = this.length();
        if (var1 > 0.0D) {
            double var3 = 1.0D / var1;
            this.x *= var3;
            this.y *= var3;
            this.z *= var3;
        }

        return this;
    }

    public Vector3dOperations dfY() {
        return new Vector3dOperations(this);
    }

    public void ay(Vector3dOperations var1) {
        var1.x = this.x;
        var1.y = this.y;
        var1.z = this.z;
    }

    public double a(ti var1) {
        return this.x * var1.adw() + this.y * var1.adx() + this.z * var1.ady();
    }

    public double b(te_w var1) {
        return this.x * (double) var1.ada() + this.y * (double) var1.adb() + this.z * (double) var1.adc();
    }

    public double a(IV var1) {
        return this.x * var1.x + this.y * var1.y + this.z * var1.z;
    }

    public double az(Vector3dOperations var1) {
        return this.x * var1.x + this.y * var1.y + this.z * var1.z;
    }

    public double get(int var1) {
        switch (var1) {
            case 0:
                return this.x;
            case 1:
                return this.y;
            case 2:
                return this.z;
            default:
                throw new IllegalArgumentException();
        }
    }

    public void set(int var1, double var2) {
        switch (var1) {
            case 0:
                this.x = var2;
                break;
            case 1:
                this.y = var2;
                break;
            case 2:
                this.z = var2;
                break;
            default:
                throw new IllegalArgumentException();
        }

    }

    public double adw() {
        return this.x;
    }

    public double adx() {
        return this.y;
    }

    public double ady() {
        return this.z;
    }

    public void aA(Vector3dOperations var1) {
        this.x = var1.x;
        this.y = var1.y;
        this.z = var1.z;
    }

    public Vec3f getVec3f() {
        return new Vec3f((float) this.x, (float) this.y, (float) this.z);
    }

    public void setVec3f(Vec3f var1) {
        this.x = (double) var1.x;
        this.y = (double) var1.y;
        this.z = (double) var1.z;
    }

    public Vec3f aB(Vector3dOperations var1) {
        return new Vec3f((float) (this.x - var1.x), (float) (this.y - var1.y), (float) (this.z - var1.z));
    }

    public Vec3f bH(Vec3f var1) {
        return new Vec3f((float) (this.x - (double) var1.x), (float) (this.y - (double) var1.y), (float) (this.z - (double) var1.z));
    }

    public Vec3f r(double var1, double var3, double var5) {
        return new Vec3f((float) (this.x - var1), (float) (this.y - var3), (float) (this.z - var5));
    }

    public boolean equals(Object var1) {
        if (var1 == null) {
            return false;
        } else if (!(var1 instanceof Vector3dOperations)) {
            return false;
        } else {
            Vector3dOperations var2 = (Vector3dOperations) var1;
            return var2.x == this.x && var2.y == this.y && var2.z == this.z;
        }
    }

    public int hashCode() {
        long var1 = Double.doubleToLongBits(this.x) + Double.doubleToLongBits(this.y) + Double.doubleToLongBits(this.z);
        return (int) (var1 ^ var1 >>> 32);
    }

    public void E(float var1, float var2, float var3) {
        this.x += (double) var1;
        this.y += (double) var2;
        this.z += (double) var3;
    }

    public void ab(double var1) {
        this.x *= var1;
        this.y *= var1;
        this.z *= var1;
    }

    public Vector3dOperations s(double var1, double var3, double var5) {
        this.x = var1;
        this.y = var3;
        this.z = var5;
        return this;
    }

    public Vector3dOperations h(Tuple3d var1) {
        this.x = var1.x;
        this.y = var1.y;
        this.z = var1.z;
        return this;
    }

    public Vector3dOperations u(Tuple3f var1) {
        this.x = (double) var1.x;
        this.y = (double) var1.y;
        this.z = (double) var1.z;
        return this;
    }

    public float t(double var1, double var3, double var5) {
        float var7 = (float) (this.x - var1);
        float var8 = (float) (this.y - var3);
        float var9 = (float) (this.z - var5);
        return var7 * var7 + var8 * var8 + var9 * var9;
    }

    public Vector3dOperations dfZ() {
        return new Vector3dOperations(-this.x, -this.y, -this.z);
    }

    public Vector3dOperations dga() {
        this.negate();
        return this;
    }

    public float aC(Vector3dOperations var1) {
        return p(this, var1);
    }

    public boolean anA() {
        float var1 = (float) (this.x + this.y + this.z);
        return !Float.isNaN(var1) && var1 != Float.NEGATIVE_INFINITY && var1 != Float.POSITIVE_INFINITY;
    }

    public void readExternal(ObjectInput var1) throws IOException {
        this.x = var1.readDouble();
        this.y = var1.readDouble();
        this.z = var1.readDouble();
    }

    public void writeExternal(ObjectOutput var1) throws IOException {
        var1.writeDouble(this.x);
        var1.writeDouble(this.y);
        var1.writeDouble(this.z);
    }

    public boolean a(Vector3dOperations var1, double var2) {
        return Math.abs(var1.x - this.x) <= var2 && Math.abs(var1.y - this.y) <= var2 && Math.abs(var1.z - this.z) <= var2;
    }

    public void a(ajK var1, Tuple3f var2) {
        float var3 = var2.x;
        float var4 = var2.y;
        float var5 = var2.z;
        this.x = (double) (var3 * var1.m00 + var4 * var1.m01 + var5 * var1.m02);
        this.y = (double) (var3 * var1.m10 + var4 * var1.m11 + var5 * var1.m12);
        this.z = (double) (var3 * var1.m20 + var4 * var1.m21 + var5 * var1.m22);
    }

    public void a(ajD var1, Tuple3f var2) {
        float var3 = var2.x;
        float var4 = var2.y;
        float var5 = var2.z;
        this.x = (double) (var3 * var1.m00 + var4 * var1.m01 + var5 * var1.m02);
        this.y = (double) (var3 * var1.m10 + var4 * var1.m11 + var5 * var1.m12);
        this.z = (double) (var3 * var1.m20 + var4 * var1.m21 + var5 * var1.m22);
    }

    public void a(ajD var1, Tuple3d var2) {
        double var3 = var2.x;
        double var5 = var2.y;
        double var7 = var2.z;
        this.x = var3 * (double) var1.m00 + var5 * (double) var1.m01 + var7 * (double) var1.m02;
        this.y = var3 * (double) var1.m10 + var5 * (double) var1.m11 + var7 * (double) var1.m12;
        this.z = var3 * (double) var1.m20 + var5 * (double) var1.m21 + var7 * (double) var1.m22;
    }

    public void a(ajK var1, Tuple3d var2) {
        double var3 = var2.x;
        double var5 = var2.y;
        double var7 = var2.z;
        this.x = var3 * (double) var1.m00 + var5 * (double) var1.m01 + var7 * (double) var1.m02;
        this.y = var3 * (double) var1.m10 + var5 * (double) var1.m11 + var7 * (double) var1.m12;
        this.z = var3 * (double) var1.m20 + var5 * (double) var1.m21 + var7 * (double) var1.m22;
    }

    public Vector3dOperations g(ajK var1, Tuple3f var2) {
        this.a(var1, var2);
        return this;
    }

    public void b(ajK var1, Tuple3d var2) {
        double var3 = var2.x;
        double var5 = var2.y;
        double var7 = var2.z;
        this.x = var3 * (double) var1.m00 + var5 * (double) var1.m01 + var7 * (double) var1.m02 + (double) var1.m03;
        this.y = var3 * (double) var1.m10 + var5 * (double) var1.m11 + var7 * (double) var1.m12 + (double) var1.m13;
        this.z = var3 * (double) var1.m20 + var5 * (double) var1.m21 + var7 * (double) var1.m22 + (double) var1.m23;
    }

    public Vector3dOperations c(ajK var1, Tuple3d var2) {
        this.b(var1, var2);
        return this;
    }

    public Vector3dOperations d(ajK var1, Tuple3d var2) {
        this.a(var1, var2);
        return this;
    }

    public void e(ajK var1, Tuple3f var2) {
        float var3 = var2.x;
        float var4 = var2.y;
        float var5 = var2.z;
        this.x = (double) (var3 * var1.m00 + var4 * var1.m01 + var5 * var1.m02 + var1.m03);
        this.y = (double) (var3 * var1.m10 + var4 * var1.m11 + var5 * var1.m12 + var1.m13);
        this.z = (double) (var3 * var1.m20 + var4 * var1.m21 + var5 * var1.m22 + var1.m23);
    }

    public Vector3dOperations h(ajK var1, Tuple3f var2) {
        this.e(var1, var2);
        return this;
    }

    public Vector3dOperations i(Tuple3d var1) {
        return new Vector3dOperations(this.x * var1.x, this.y * var1.y, this.z * var1.z);
    }

    public Vector3dOperations j(Tuple3d var1) {
        this.x *= var1.x;
        this.y *= var1.y;
        this.z *= var1.z;
        return this;
    }

    public void k(Tuple3d var1) {
        this.x *= var1.x;
        this.y *= var1.y;
        this.z *= var1.z;
    }

    public Vector3dOperations v(Tuple3f var1) {
        return new Vector3dOperations(this.x * (double) var1.x, this.y * (double) var1.y, this.z * (double) var1.z);
    }

    public Vector3dOperations w(Tuple3f var1) {
        this.x *= (double) var1.x;
        this.y *= (double) var1.y;
        this.z *= (double) var1.z;
        return this;
    }

    public void h(Tuple3f var1) {
        this.x *= (double) var1.x;
        this.y *= (double) var1.y;
        this.z *= (double) var1.z;
    }

    public void c(Vector3f var1) {
        this.v((double) var1.x, (double) var1.y, (double) var1.z);
    }

    public Vector3dOperations c(Vector3d var1) {
        return this.u(var1.x, var1.y, var1.z);
    }

    public void d(Vector3d var1) {
        this.v(var1.x, var1.y, var1.z);
    }

    public Vector3dOperations f(Vector3f var1) {
        return this.u((double) var1.x, (double) var1.y, (double) var1.z);
    }

    public Vector3dOperations u(double var1, double var3, double var5) {
        double var7 = this.y * var5 - this.z * var3;
        double var9 = this.z * var1 - this.x * var5;
        double var11 = this.x * var3 - this.y * var1;
        this.x = var7;
        this.y = var9;
        this.z = var11;
        return this;
    }

    public void v(double var1, double var3, double var5) {
        double var7 = this.y * var5 - this.z * var3;
        double var9 = this.z * var1 - this.x * var5;
        double var11 = this.x * var3 - this.y * var1;
        this.x = var7;
        this.y = var9;
        this.z = var11;
    }

    public Vector3dOperations w(double var1, double var3, double var5) {
        double var7 = this.y * var5 - this.z * var3;
        double var9 = this.z * var1 - this.x * var5;
        double var11 = this.x * var3 - this.y * var1;
        return new Vector3dOperations(var7, var9, var11);
    }

    public Vector3dOperations s(ajK var1) {
        return f((ajK) var1, this);
    }

    public Vector3dOperations t(ajK var1) {
        double var2 = this.x * (double) var1.m00 + this.y * (double) var1.m01 + this.z * (double) var1.m02;
        double var4 = this.x * (double) var1.m10 + this.y * (double) var1.m11 + this.z * (double) var1.m12;
        double var6 = this.x * (double) var1.m20 + this.y * (double) var1.m21 + this.z * (double) var1.m22;
        return new Vector3dOperations(var2, var4, var6);
    }

    public void u(ajK var1) {
        double var2 = this.x * (double) var1.m00 + this.y * (double) var1.m01 + this.z * (double) var1.m02 + (double) var1.m03;
        double var4 = this.x * (double) var1.m10 + this.y * (double) var1.m11 + this.z * (double) var1.m12 + (double) var1.m13;
        double var6 = this.x * (double) var1.m20 + this.y * (double) var1.m21 + this.z * (double) var1.m22 + (double) var1.m23;
        this.x = var2;
        this.y = var4;
        this.z = var6;
    }

    public void r(ajK var1) {
        double var2 = this.x * (double) var1.m00 + this.y * (double) var1.m01 + this.z * (double) var1.m02;
        double var4 = this.x * (double) var1.m10 + this.y * (double) var1.m11 + this.z * (double) var1.m12;
        double var6 = this.x * (double) var1.m20 + this.y * (double) var1.m21 + this.z * (double) var1.m22;
        this.x = var2;
        this.y = var4;
        this.z = var6;
    }

    public Vector3dOperations v(ajK var1) {
        this.u(var1);
        return this;
    }

    public Vector3dOperations w(ajK var1) {
        this.r(var1);
        return this;
    }

    public void a(float var1, Tuple3f var2, Tuple3d var3) {
        this.x = (double) (var1 * var2.x) + var3.x;
        this.y = (double) (var1 * var2.y) + var3.y;
        this.z = (double) (var1 * var2.z) + var3.z;
    }

    public boolean isValid() {
        return !Double.isNaN(this.x) && !Double.isInfinite(this.x) && !Double.isNaN(this.y) && !Double.isInfinite(this.y) && !Double.isNaN(this.z) && !Double.isInfinite(this.z);
    }

    public void x(double var1, double var3, double var5) {
        this.x += var1;
        this.y += var3;
        this.z += var5;
    }

    public Vector3dOperations y(double var1, double var3, double var5) {
        return new Vector3dOperations(this.x + var1, this.y + var3, this.z + var5);
    }

    public Vector3dOperations z(double var1, double var3, double var5) {
        this.x += var1;
        this.y += var3;
        this.z += var5;
        return this;
    }

    public void readExternal(Vm_q var1) {
        this.x = var1.getValue("x");
        this.y = var1.getValue("y");
        this.z = var1.getValue("z");
    }

    public void writeExternal(att var1) {
        var1.setValue("x", this.x);
        var1.setValue("y", this.y);
        var1.setValue("z", this.z);
    }
}
