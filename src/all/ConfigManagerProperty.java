package all;

/**
 * Обёртка  значения настройки
 */
public class ConfigManagerProperty {
    /**
     * Тип значения
     */
    private Class typeProperty;
    /**
     * Значение
     */
    private Object valueProperty;
    /**
     * Флаг что настройка по умолчанию
     */
    private boolean isDefaultConfig;

    /**
     * Новое значение
     *
     * @param typeProperty
     * @param valueProperty
     * @param isDefaultConfig
     */
    ConfigManagerProperty(Class typeProperty, Object valueProperty, boolean isDefaultConfig) {
        this.typeProperty = typeProperty;
        this.valueProperty = valueProperty;
        this.isDefaultConfig = isDefaultConfig;
    }

    /**
     * Сравнить значения
     *
     * @param item Проверяемое значение
     * @return true - одинаковые
     */
    public boolean equals(ConfigManagerProperty item) {
        if (this == item) {
            return true;
        } else if (item == null) {
            return false;
        } else if (this.getClass() != item.getClass()) {
            return false;
        } else {
            return this.typeProperty == item.typeProperty && (this.valueProperty == null && item.valueProperty == null || this.valueProperty.equals(item.valueProperty));
        }
    }

    /**
     * Сделать копию обёртки значения
     *
     * @return
     */
    public ConfigManagerProperty clone() {
        return new ConfigManagerProperty(this.typeProperty, this.valueProperty, this.isDefaultConfig);
    }

    /**
     * Тип значения
     *
     * @return
     */
    public Class getTypeProperty() {
        return this.typeProperty;
    }

    /**
     * Значение
     *
     * @return
     */
    public Object getValueProperty() {
        return this.valueProperty;
    }

    /**
     * Перезаписать новым значением
     *
     * @param valueProperty
     * @throws Exception
     */
    public void setValue(Object valueProperty) throws Exception {
        if (valueProperty.getClass() != this.typeProperty) {
            throw new Exception("New value needs the same type");
        } else {
            this.valueProperty = valueProperty;
            this.isDefaultConfig = false;
        }
    }

    /**
     * Грязность
     *
     * @return
     */
    public final boolean isDefaultConfig() {
        return this.isDefaultConfig;
    }

    /**
     * Грязность
     *
     * @param isDefaultConfig
     */
    public void setDefaultConfig(boolean isDefaultConfig) {
        this.isDefaultConfig = isDefaultConfig;
    }

    public String toString() {
        return String.valueOf(this.valueProperty);
    }
}
