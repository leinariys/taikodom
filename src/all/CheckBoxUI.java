package all;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicButtonListener;
import javax.swing.plaf.basic.BasicCheckBoxUI;
import java.awt.*;

/**
 * CheckBoxUI
 */
public class CheckBoxUI extends BasicCheckBoxUI implements aIh {
    private ComponentManager Rp;

    public CheckBoxUI(JCheckBox var1) {
        var1.setRolloverEnabled(true);
        var1.setOpaque(false);
        this.Rp = new UY("checkbox", var1);
    }

    public static ComponentUI createUI(JComponent var0) {
        return new CheckBoxUI((JCheckBox) var0);
    }

    protected BasicButtonListener createButtonListener(AbstractButton var1) {
        return new ButtonUIListener(var1, this.Rp);
    }

    public void update(Graphics var1, JComponent var2) {
        RM.a((aIh) this, (Graphics) var1, (AbstractButton) ((AbstractButton) var2));
        super.paint(var1, var2);
    }

    public void paint(Graphics var1, JComponent var2) {
        super.paint(var1, var2);
    }

    public Dimension getPreferredSize(JComponent var1) {
        RM.a((aIh) this, (AbstractButton) ((AbstractButton) var1));
        return this.wy().c(var1, super.getPreferredSize(var1));
    }

    public Dimension getMinimumSize(JComponent var1) {
        RM.a((aIh) this, (AbstractButton) ((AbstractButton) var1));
        return this.wy().b(var1, super.getMinimumSize(var1));
    }

    public Dimension getMaximumSize(JComponent var1) {
        RM.a((aIh) this, (AbstractButton) ((AbstractButton) var1));
        return this.wy().b(var1, super.getMaximumSize(var1));
    }

    public ComponentManager wy() {
        return this.Rp;
    }

    /**
     * TODO похлже это public pv wy()
     *
     * @return Графический элемент
     */
    @Override
    public aeK wz() {
        return this.Rp;
    }
}
