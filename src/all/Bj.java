package all;

import org.w3c.dom.css.*;

import java.io.Serializable;
import java.io.StringReader;
import java.util.Vector;

public class Bj implements Serializable, CSSPrimitiveValue, CSSValueList {
    private Object _value;

    public Bj(RGBColor var1) {
        this._value = null;
        this._value = var1;
    }

    public Bj(LexicalUnit var1, boolean var2) {
        this._value = null;
        if (var1.getParameters() != null) {
            if (var1.getLexicalUnitType() == 38) {
                this._value = new aTP(var1.getParameters());
            } else if (var1.getLexicalUnitType() == 27) {
                this._value = new ColorCSS(var1.getParameters());
            } else if (var1.getLexicalUnitType() == 25) {
                this._value = new akw(false, var1.getParameters());
            } else if (var1.getLexicalUnitType() == 26) {
                this._value = new akw(true, var1.getParameters());
            } else {
                this._value = var1;
            }
        } else if (!var2 && var1.getNextLexicalUnit() != null) {
            Vector var3 = new Vector();

            for (LexicalUnit var4 = var1; var4 != null; var4 = var4.getNextLexicalUnit()) {
                if (var4.getLexicalUnitType() != 0 && var4.getLexicalUnitType() != 4) {
                    var3.addElement(new Bj(var4, true));
                }
            }

            this._value = var3;
        } else {
            this._value = var1;
        }

    }

    public Bj(LexicalUnit var1) {
        this(var1, false);
    }

    public String getCssText() {
        if (this.getCssValueType() == 2) {
            StringBuffer var1 = new StringBuffer();
            Vector var2 = (Vector) this._value;
            LexicalUnit var3 = (LexicalUnit) ((Bj) var2.elementAt(0))._value;

            while (var3 != null) {
                var1.append(var3.toString());
                LexicalUnit var4 = var3;
                var3 = var3.getNextLexicalUnit();
                if (var3 != null && var3.getLexicalUnitType() != 0 && var3.getLexicalUnitType() != 4 && var4.getLexicalUnitType() != 4) {
                    var1.append(" ");
                }
            }

            return var1.toString();
        } else {
            return this._value.toString();
        }
    }

    public void setCssText(String var1) {
        try {
            InputSource var2 = new InputSource(new StringReader(var1));
            ParserCss var3 = ParserCss.getParserCss();
            Bj var4 = (Bj) var3.e(var2);
            this._value = var4._value;
        } catch (Exception var5) {
            throw new qN((short) 12, 0, var5.getMessage());
        }
    }

    public short getCssValueType() {
        return (short) (this._value instanceof Vector ? 2 : 1);
    }

    public short getPrimitiveType() {
        if (this._value instanceof LexicalUnit) {
            LexicalUnit var1 = (LexicalUnit) this._value;
            switch (var1.getLexicalUnitType()) {
                case 12:
                    return 21;
                case 13:
                case 14:
                    return 1;
                case 15:
                    return 3;
                case 16:
                    return 4;
                case 17:
                    return 5;
                case 18:
                    return 8;
                case 19:
                    return 6;
                case 20:
                    return 7;
                case 21:
                    return 9;
                case 22:
                    return 10;
                case 23:
                    return 2;
                case 24:
                    return 20;
                case 25:
                case 26:
                case 27:
                case 38:
                default:
                    break;
                case 28:
                    return 11;
                case 29:
                    return 13;
                case 30:
                    return 12;
                case 31:
                    return 14;
                case 32:
                    return 15;
                case 33:
                    return 17;
                case 34:
                    return 16;
                case 35:
                    return 21;
                case 36:
                    return 19;
                case 37:
                    return 22;
                case 39:
                case 40:
                case 41:
                    return 19;
                case 42:
                    return 18;
            }
        } else {
            if (this._value instanceof aTP) {
                return 24;
            }

            if (this._value instanceof ColorCSS) {
                return 25;
            }

            if (this._value instanceof akw) {
                return 23;
            }
        }

        return 0;
    }

    public void setFloatValue(short var1, float var2) {
        this._value = LexicalUnit.perseValueColor((LexicalUnit) null, var2);
    }

    public float getFloatValue(short var1) {
        if (this._value instanceof LexicalUnit) {
            LexicalUnit var2 = (LexicalUnit) this._value;
            return var2.getFloatValue();
        } else {
            throw new qN((short) 15, 10);
        }
    }

    public void setStringValue(short var1, String var2) {
        switch (var1) {
            case 19:
                this._value = LexicalUnit.a((LexicalUnit) null, (String) var2);
                break;
            case 20:
                this._value = LexicalUnit.c((LexicalUnit) null, (String) var2);
                break;
            case 21:
                this._value = LexicalUnit.b((LexicalUnit) null, (String) var2);
                break;
            case 22:
                throw new qN((short) 9, 19);
            default:
                throw new qN((short) 15, 11);
        }

    }

    public String getStringValue() {
        if (this._value instanceof LexicalUnit) {
            LexicalUnit var1 = (LexicalUnit) this._value;
            if (var1.getLexicalUnitType() == 35 || var1.getLexicalUnitType() == 36 || var1.getLexicalUnitType() == 24 || var1.getLexicalUnitType() == 37) {
                return var1.getStringValue();
            }
        } else if (this._value instanceof Vector) {
            return null;
        }

        throw new qN((short) 15, 11);
    }

    public Counter getCounterValue() {
        if (!(this._value instanceof Counter)) {
            throw new qN((short) 15, 12);
        } else {
            return (Counter) this._value;
        }
    }

    public Rect getRectValue() {
        if (!(this._value instanceof Rect)) {
            throw new qN((short) 15, 13);
        } else {
            return (Rect) this._value;
        }
    }

    public RGBColor getRGBColorValue() {
        if (!(this._value instanceof RGBColor)) {
            throw new qN((short) 15, 14);
        } else {
            return (RGBColor) this._value;
        }
    }

    public int getLength() {
        return this._value instanceof Vector ? ((Vector) this._value).size() : 0;
    }

    public CSSValue item(int var1) {
        return this._value instanceof Vector ? (CSSValue) ((Vector) this._value).elementAt(var1) : null;
    }

    public String toString() {
        return this.getCssText();
    }
}
