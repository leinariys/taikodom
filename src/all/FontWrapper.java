package all;

/**
 * Обёртка шрифта
 */
public class FontWrapper {
    private final String fontName;// Название шрпфти Например  tahoma
    private final int fontStyle;// Стиль шрифта font-style: normal | italic | oblique | inherit
    private final int fontSize;// Размер шрифта

    public FontWrapper(String var1, int var2, int var3) {
        this.fontName = var1;
        this.fontStyle = var2;
        this.fontSize = var3;
    }

    //название шрпфти Например  tahoma
    public String getFontName() {
        return this.fontName;
    }

    // Стиль шрифта font-style: normal | italic | oblique | inherit
    public int getFontStyle() {
        return this.fontStyle;
    }

    // Размер шрифта
    public int getFontSize() {
        return this.fontSize;
    }
}
