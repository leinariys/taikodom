package all;

import org.w3c.dom.Node;
import org.w3c.dom.css.CSSRule;
import org.w3c.dom.css.CSSRuleList;
import org.w3c.dom.css.CSSStyleSheet;
import org.w3c.dom.stylesheets.MediaList;
import org.w3c.dom.stylesheets.StyleSheet;

import java.io.Serializable;
import java.io.StringReader;

public class SS implements Serializable, CSSStyleSheet {
    private boolean disabled = false;
    private Node ownerNode = null;
    private StyleSheet parentStyleSheet = null;
    private String href = null;
    private String title = null;
    private MediaList media = null;
    private CSSRule ownerRule = null;
    private boolean readOnly = false;
    private RK cssRules = null;

    public String getType() {
        return "text/css";
    }

    public boolean getDisabled() {
        return this.disabled;
    }

    public void setDisabled(boolean var1) {
        this.disabled = var1;
    }

    public Node getOwnerNode() {
        return this.ownerNode;
    }

    public StyleSheet getParentStyleSheet() {
        return this.parentStyleSheet;
    }

    public String getHref() {
        return this.href;
    }

    public void setHref(String var1) {
        this.href = var1;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String var1) {
        this.title = var1;
    }

    public MediaList getMedia() {
        return this.media;
    }

    public void setMedia(String var1) {
    }

    public CSSRule getOwnerRule() {
        return this.ownerRule;
    }

    public CSSRuleList getCssRules() {
        return this.cssRules;
    }

    public int insertRule(String var1, int var2) {
        if (this.readOnly) {
            throw new qN((short) 7, 2);
        } else {
            try {
                InputSource var3 = new InputSource(new StringReader(var1));
                ParserCss var4 = ParserCss.getParserCss();
                var4.a(this);
                CSSRule var5 = var4.f(var3);
                if (this.getCssRules().getLength() > 0) {
                    byte var6 = -1;
                    if (var5.getType() == 2) {
                        if (var2 != 0) {
                            var6 = 15;
                        } else if (this.getCssRules().item(0).getType() == 2) {
                            var6 = 16;
                        }
                    } else if (var5.getType() == 3 && var2 <= this.getCssRules().getLength()) {
                        for (int var7 = 0; var7 < var2; ++var7) {
                            short var8 = this.getCssRules().item(var7).getType();
                            if (var8 != 2 || var8 != 3) {
                                var6 = 17;
                                break;
                            }
                        }
                    }

                    if (var6 > -1) {
                        throw new qN((short) 3, var6);
                    }
                }

                ((RK) this.getCssRules()).add(var5, var2);
                return var2;
            } catch (ArrayIndexOutOfBoundsException var9) {
                throw new qN((short) 1, 1, var9.getMessage());
            } catch (CSSException var10) {
                throw new qN((short) 12, 0, var10.getMessage());
            } /*catch (IOException var11) {
            throw new qN((short)12, 0, var11.getMessage());
         }*/
        }
    }

    public void deleteRule(int var1) {
        if (this.readOnly) {
            throw new qN((short) 7, 2);
        } else {
            try {
                ((RK) this.getCssRules()).delete(var1);
            } catch (ArrayIndexOutOfBoundsException var3) {
                throw new qN((short) 1, 1, var3.getMessage());
            }
        }
    }

    public boolean isReadOnly() {
        return this.readOnly;
    }

    public void setReadOnly(boolean var1) {
        this.readOnly = var1;
    }

    public void a(Node var1) {
        this.ownerNode = var1;
    }

    public void a(StyleSheet var1) {
        this.parentStyleSheet = var1;
    }

    public void a(CSSRule var1) {
        this.ownerRule = var1;
    }

    public void a(RK var1) {
        this.cssRules = var1;
    }

    public String toString() {
        return this.cssRules.toString();
    }
}
