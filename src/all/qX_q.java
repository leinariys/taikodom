package all;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class qX_q extends aOv implements Externalizable {
    private static final long serialVersionUID = 1L;
    private boolean aZj;
    private Xf_q Uk;

    public qX_q() {
    }

    public qX_q(Xf_q var1, boolean var2) {
        this.Uk = var1;
        this.aZj = var2;
    }

    public boolean Yg() {
        return this.aZj;
    }

    public Xf_q yz() {
        return this.Uk;
    }

    public void readExternal(ObjectInput var1) throws IOException, ClassNotFoundException {
        super.readExternal(var1);
        this.aZj = var1.readBoolean();
        this.Uk = (Xf_q) GZ.dah.b(var1);
    }

    public void writeExternal(ObjectOutput var1) throws IOException {
        super.writeExternal(var1);
        var1.writeBoolean(this.aZj);
        GZ.dah.a((ObjectOutput) var1, (Object) this.Uk);
    }

    public akO a(b var1, Jz var2) {
        return ((af) this.Uk.bFf()).a((b) var1, (aOv) this);
    }
}
