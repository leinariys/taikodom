package all;

import com.hoplon.geometry.Vec3f;

public abstract class adK extends ob {
    protected final Vec3f localScaling = new Vec3f(1.0F, 1.0F, 1.0F);
    protected final Vec3f implicitShapeDimensions = new Vec3f();
    protected float collisionMargin = 0.04F;
    protected Kt stack = Kt.bcE();

    public void getAabb(xf_g var1, Vec3f var2, Vec3f var3) {
        this.getAabbSlow(var1, var2, var3);
    }

    public void getAabbSlow(xf_g var1, Vec3f var2, Vec3f var3) {
        this.stack.bcH().push();

        try {
            float var4 = this.getMargin();

            for (int var5 = 0; var5 < 3; ++var5) {
                Vec3f var6 = this.stack.bcH().h(0.0F, 0.0F, 0.0F);
                JL.a(var6, var5, 1.0F);
                Vec3f var7 = (Vec3f) this.stack.bcH().get();
                rS.a(var7, var6, var1.bFF);
                Vec3f var8 = this.stack.bcH().ac(this.localGetSupportingVertex(var7));
                Vec3f var9 = this.stack.bcH().ac(var8);
                var1.G(var9);
                JL.a(var3, var5, JL.b(var9, var5) + var4);
                JL.a(var6, var5, -1.0F);
                rS.a(var7, var6, var1.bFF);
                var9.set(this.localGetSupportingVertex(var7));
                var1.G(var9);
                JL.a(var2, var5, JL.b(var9, var5) - var4);
            }
        } finally {
            this.stack.bcH().pop();
        }

    }

    public Vec3f localGetSupportingVertex(Vec3f var1) {
        this.stack.bcH().push();

        Vec3f var5;
        try {
            Vec3f var2 = this.stack.bcH().ac(this.localGetSupportingVertexWithoutMargin(var1));
            if (this.getMargin() != 0.0F) {
                Vec3f var3 = this.stack.bcH().ac(var1);
                if (var3.lengthSquared() < 1.4210855E-14F) {
                    var3.set(-1.0F, -1.0F, -1.0F);
                }

                var3.normalize();
                var2.scaleAdd(this.getMargin(), var3, var2);
            }

            var5 = (Vec3f) this.stack.bcH().aq(var2);
        } finally {
            this.stack.bcH().pop();
        }

        return var5;
    }

    public Vec3f getLocalScaling() {
        return this.localScaling;
    }

    public void setLocalScaling(Vec3f var1) {
        this.localScaling.set(var1);
    }

    public float getMargin() {
        return this.collisionMargin;
    }

    public void setMargin(float var1) {
        this.collisionMargin = var1;
    }

    public int getNumPreferredPenetrationDirections() {
        return 0;
    }

    public void getPreferredPenetrationDirection(int var1, Vec3f var2) {
        throw new InternalError();
    }
}
