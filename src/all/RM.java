package all;

import taikodom.render.graphics2d.RGraphics2;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.plaf.ComponentUI;
import javax.swing.text.JTextComponent;
import java.awt.*;
import java.awt.image.ImageObserver;

public class RM {
    // $FF: synthetic field
    private static int[] eaL;

    public static void a(ComponentUI var0, Graphics var1, JComponent var2) {
        if (ComponentManager.getCssHolder(var2).getAttribute("debug") != null) {
            var1.setColor(Color.RED);
            var1.drawRect(0, 0, var2.getWidth(), var2.getHeight());
            var1.drawRect(var2.getWidth(), 0, var2.getWidth(), var2.getHeight());
            var1.drawLine(0, 0, var2.getWidth(), 0);
            var1.drawLine(0, 0, var2.getWidth(), var2.getHeight());
            if (var2.getName() != null && var2.getFont() != null) {
                var1.setFont(var2.getFont());
                var1.setColor(Color.LIGHT_GRAY);
                var1.drawString(var2.getName(), 0, 12);
            }
        }

    }

    /**
     * @param var0
     * @param var1
     */
    public static void a(aIh var0, JComponent var1) {
        aeK var2 = var0.wz();
        PropertiesUiFromCss var3 = var2.Vp();
        if (var3 != null) {
            a(var1, var3);
        }

    }

    public static void a(aIh var0, AbstractButton var1) {
        aeK var2 = var0.wz();
        PropertiesUiFromCss var3 = var2.Vp();
        if (var3 != null) {
            a(var1, var3);
        }

    }

    public static void a(aIh var0, JLabel var1) {
        aeK var2 = var0.wz();
        PropertiesUiFromCss var3 = var2.Vp();
        if (var3 != null) {
            a(var1, var3);
        }

    }

    public static void a(aIh var0, JTextComponent var1) {
        aeK var2 = var0.wz();
        PropertiesUiFromCss var3 = var2.Vp();
        if (var3 != null) {
            a(var1, var3);
        }

    }

    private static boolean eq(Object var0, Object var1) {
        if (var0 == null) {
            return var1 != null;
        } else {
            return var0.equals(var1);
        }
    }

    /**
     * @param var0
     * @param var1
     */
    private static void a(JComponent var0, PropertiesUiFromCss var1) {
        if (!eq(var0.getFont(), var1.getFont())) {
            var0.setFont(var1.getFont());
        }

        if (!eq(var0.getForeground(), var1.getColor())) {
            var0.setForeground(var1.getColor());
        }

        if (!eq(var1.getCursor(), var0.getCursor())) {
            var0.setCursor(var1.getCursor());
        }

        if (!eq(var0.getComponentOrientation() != null, var1.getComponentOrientation())) {
            var0.setComponentOrientation(var1.getComponentOrientation());
        }

    }

    private static void a(AbstractButton var0, PropertiesUiFromCss var1) {
        if (!eq(var1.aty(), var1.aty().akW())) {
            var0.setHorizontalAlignment(var1.aty().akW());
        }

        if (!eq(var1.atR(), var1.atR().akW())) {
            var0.setVerticalAlignment(var1.atR().akW());
        }

        if (!eq(var1.getIconImage(), var0.getIcon())) {
            var0.setIcon(var1.getIcon());
        }

        a((JComponent) var0, (PropertiesUiFromCss) var1);
    }

    private static void a(JLabel var0, PropertiesUiFromCss var1) {
        int var2 = var1.aty().akW();
        if (!eq(var1.aty(), var2)) {
            var0.setHorizontalAlignment(var2);
        }

        var2 = var1.atR().akW();
        if (!eq(var1.atR(), var2)) {
            var0.setVerticalAlignment(var2);
        }

        Icon var3 = var1.getIcon();
        if (!eq(var3, var0.getIcon())) {
            var0.setIcon(var3);
        }

        a((JComponent) var0, (PropertiesUiFromCss) var1);
    }

    private static void a(JTextComponent var0, PropertiesUiFromCss var1) {
        Color var2 = var1.getColor();
        if (var0.getCaretColor() != var2) {
            var0.setCaretColor(var2);
        }

        a((JComponent) var0, (PropertiesUiFromCss) var1);
    }

    /**
     * @param var0
     * @param var1
     */
    private static void a(Graphics var0, PropertiesUiFromCss var1) {
        if (var0 instanceof RGraphics2) {
            Color var2 = var1.getColorMultiplier();
            RGraphics2 var3 = (RGraphics2) var0;
            if (var2 != null && var3.getParent() != null) {
                var3.getParent().setColorMultiplier(var2);
                var3.setColorMultiplier(Color.WHITE);
            }
        }

    }

    /**
     * @param var0 all.RootPaneWrapper@1d73ff8
     * @param var1 taikodom.render.graphics2d.RGraphics2[font=java.awt.Font[family=Dialog,name=Dialog,style=plain,size=12],color=java.awt.Color[r=0,parseSelectors=0,setGreen=0]]
     * @param var2 javax.swing.JRootPane[,0,0,1024x780,layout=javax.swing.JRootPane$RootLayout,alignmentX=0.0,alignmentY=0.0,border=all.BorderWrapper@11954cc,flags=16777664,maximumSize=,minimumSize=,preferredSize=]
     */
    public static void a(aIh var0, Graphics var1, JComponent var2) {

        a((ComponentUI) var0, var1, var2);
        aeK var3 = var0.wz();
        PropertiesUiFromCss var4 = var3.Vp();

        if (var4 != null) {
            a(var1, var4);
            a(var2, var4);
            a((Graphics) var1, (Component) var2, (PropertiesUiFromCss) var4);
        }

    }

    /**
     * @param var0
     * @param var1
     * @param var2
     */
    public static void a(Graphics var0, Component var1, PropertiesUiFromCss var2) {
        a(var0, var1, var2, (Rectangle) null);
    }

    public static void a(Graphics var0, Component var1, PropertiesUiFromCss var2, Rectangle var3) {
        int var4 = var1.getWidth();
        int var5 = var1.getHeight();
        int var8;
        int var9;
        int var10;
        if (var0 instanceof RGraphics2) {
            if (var2.atj() != null) {
                Image var6 = var2.atj();
                Rectangle var7 = var0.getClipBounds();
                var8 = var6.getWidth(var1);
                var9 = var6.getHeight(var1);
                var10 = var8 / 3;
                int var11 = var9 / 3;
                byte var12 = 0;
                int var14 = var8 - var10;
                byte var16 = 0;
                int var18 = var9 - var11;
                int var20 = var7.x - var10 / 2;
                int var21 = var7.x + var10 / 2;
                int var22 = var7.x + var7.width - var10 / 2;
                int var23 = var7.x + var7.width + var10 / 2;
                int var24 = var7.y - var11 / 2;
                int var25 = var7.y + var11 / 2;
                int var26 = var7.y + var7.height - var11 / 2;
                int var27 = var7.y + var7.height + var11 / 2;
                var0.setClip(var20, var24, var7.width + var10, var7.height + var11);
                var0.drawImage(var6, var20, var24, var21, var25, var12, var16, var10, var11, (ImageObserver) null);
                var0.drawImage(var6, var21, var24, var22, var25, var10, var16, var14, var11, (ImageObserver) null);
                var0.drawImage(var6, var22, var24, var23, var25, var14, var16, var8, var11, (ImageObserver) null);
                var0.drawImage(var6, var20, var25, var21, var26, var12, var11, var10, var18, (ImageObserver) null);
                var0.drawImage(var6, var22, var25, var23, var26, var14, var11, var8, var18, (ImageObserver) null);
                var0.drawImage(var6, var20, var26, var21, var27, var12, var18, var10, var9, (ImageObserver) null);
                var0.drawImage(var6, var21, var26, var22, var27, var10, var18, var14, var9, (ImageObserver) null);
                var0.drawImage(var6, var22, var26, var23, var27, var14, var18, var8, var9, (ImageObserver) null);
                var0.setClip(var7);
            }

            if (var2.ath() == Boolean.TRUE) {
                Insets var28 = a(var1, var2);
                ((RGraphics2) var0).blur(var28.left, var28.top, var4 - var28.right - var28.left, var5 - var28.bottom - var28.top);
            }
        }

        int var29 = var2.atT();
        if (var29 > 0) {
            Insets var31 = a(var1, var2);
            int var30;
            if (var3 == null) {
                var30 = var31.left;
                var8 = var31.top;
                var9 = var4 - var31.right;
                var10 = var5 - var31.bottom;
            } else {
                var30 = var3.x + var31.left;
                var8 = var3.y + var31.top;
                var9 = var30 + var3.width - var31.right - var31.left;
                var10 = var8 + var3.height - var31.top - var31.bottom;
            }

            if (var0 instanceof RGraphics2 && var2.ath() == Boolean.TRUE) {
                ((RGraphics2) var0).blur(0, 0, var4, var5 + 1);
            }

            for (int var32 = 0; var32 < var29; ++var32) {
                Image var13 = var2.fT(var32);
                if (var13 != null) {
                    Color var33 = var2.atn();
                    jM_q var15 = var2.fU(var32);
                    a(var0, var13, var2.fV(var32), var15, var30, var8, var9, var10, var33, var1);
                }
            }
        }

    }

    public static void a(Graphics var0, Image var1, xo var2, jM_q var3, int var4, int var5, int var6, int var7, Color var8, ImageObserver var9) {
        int var10 = var1.getWidth(var9);
        int var11 = var1.getHeight(var9);
        int var14 = var6 - var4;
        int var15 = var7 - var5;
        int var12;
        int var13;
        if (var3 != jM_q.apd) {
            Tu var16 = var3.getHorizon();
            if (var16.fEH == Tu.a.ejc) {
                var13 = var4 + (int) Math.ceil((double) (var16.jp((float) var14) - var16.jp((float) var10)));
                var16.jp((float) var10);
            } else {
                var16.jp((float) var14);
                var13 = var4 + var16.jp((float) var14);
            }

            Tu var17 = var3.getVertical();
            if (var17.fEH == Tu.a.ejc) {
                var12 = var5 + (int) Math.ceil((double) (var17.jp((float) var15) - var17.jp((float) var11)));
                var16.jp((float) var10);
            } else {
                var16.jp((float) var15);
                var12 = var5 + var17.jp((float) var15);
            }
        } else {
            var13 = var4;
            var12 = var5;
        }

        if (var2 == null) {
            var2 = xo.bFT;
        }

        float var18;
        switch (brE()[var2.ordinal()]) {
            case 1:
                var18 = (float) (var7 - var5) / (float) var11;
                float var19 = (float) (var6 - var4) / (float) var10;
                var0.drawImage(var1, var4, var5, var6, var7, 0, 0, (int) ((float) var10 * var19), (int) ((float) var11 * var18), var8, var9);
                break;
            case 2:
                var18 = (float) (var6 - var4) / (float) var10;
                var11 = Math.min(var7 - var12, var11);
                var0.drawImage(var1, var4, var12, var6, var12 + var11, 0, 0, (int) ((float) var10 * var18), var11, var8, var9);
                break;
            case 3:
                var10 = Math.min(var6 - var13, var10);
                var18 = (float) (var7 - var5) / (float) var11;
                var0.drawImage(var1, var13, var5, var13 + var10, var7, 0, 0, var10, (int) ((float) var11 * var18), var8, var9);
                break;
            case 4:
                var10 = Math.min(var6 - var13, var10);
                var11 = Math.min(var7 - var12, var11);
                var0.drawImage(var1, var13, var12, var13 + var10, var12 + var11, 0, 0, var10, var11, var8, var9);
                break;
            case 5:
                var0.drawImage(var1, var4, var5, var6, var7, 0, 0, var10, var11, var8, var9);
                break;
            case 6:
                var11 = Math.min(var7 - var12, var11);
                var0.drawImage(var1, var4, var12, var6, var12 + var11, 0, 0, var10, var11, var8, var9);
                break;
            case 7:
                var10 = Math.min(var6 - var13, var10);
                var0.drawImage(var1, var13, var5, var13 + var10, var7, 0, 0, var10, var11, var8, var9);
                break;
            case 8:
                var18 = (float) (var7 - var5) / (float) var11;
                var0.drawImage(var1, var4, var5, var6, var7, 0, 0, var10, (int) ((float) var11 * var18), var8, var9);
                break;
            case 9:
                var18 = (float) (var6 - var4) / (float) var10;
                var0.drawImage(var1, var4, var5, var6, var7, 0, 0, (int) ((float) var10 * var18), var11, var8, var9);
        }

    }

    private static Insets a(Component var0, PropertiesUiFromCss var1) {
        Insets var2;
        if (var0 instanceof JComponent) {
            Border var3 = ((JComponent) var0).getBorder();

            if (var3 instanceof BorderWrapper) {
                var2 = ((BorderWrapper) var3).a(var1, var0);
            } else {
                var2 = ((JComponent) var0).getInsets();
            }

        } else {
            BorderWrapper var4 = BorderWrapper.getInstance();
            var2 = ((BorderWrapper) var4).a(var1, var0);
        }

        return var2;
    }

    public static void a(aIh var0, Graphics var1, AbstractButton var2) {
        aeK var3 = var0.wz();
        PropertiesUiFromCss var4 = var3.Vp();
        if (var4 != null) {
            a(var1, var4);
            a(var2, var4);
            a((Graphics) var1, (Component) var2, (PropertiesUiFromCss) var4);
        }

        a((ComponentUI) ((ComponentUI) var0), (Graphics) var1, (JComponent) var2);
    }

    public static void a(aIh var0, Graphics var1, JTextComponent var2) {
        aeK var3 = var0.wz();
        PropertiesUiFromCss var4 = var3.Vp();
        if (var4 != null) {
            a(var1, var4);
            a(var2, var4);
            a((Graphics) var1, (Component) var2, (PropertiesUiFromCss) var4);
        }

        a((ComponentUI) ((ComponentUI) var0), (Graphics) var1, (JComponent) var2);
    }

    public static void a(aIh var0, Graphics var1, JLabel var2) {
        aeK var3 = var0.wz();
        PropertiesUiFromCss var4 = var3.Vp();
        if (var4 != null) {
            a(var1, var4);
            a(var2, var4);
            a((Graphics) var1, (Component) var2, (PropertiesUiFromCss) var4);
        }

        a((ComponentUI) ((ComponentUI) var0), (Graphics) var1, (JComponent) var2);
    }

    public static int hashCode(Object var0) {
        return var0 == null ? 0 : var0.hashCode();
    }

    public static int nd(int var0) {
        return var0;
    }

    public static int l(short var0) {
        return var0;
    }

    public static int a(byte var0) {
        return var0;
    }

    public static int dp(boolean var0) {
        return var0 ? 1 : 0;
    }

    public static int hs(float var0) {
        return Float.floatToIntBits(var0);
    }

    public static int D(double var0) {
        long var2 = Double.doubleToLongBits(var0);
        return (int) (var2 ^ var2 >>> 32);
    }

    public static int fF(long var0) {
        return (int) (var0 ^ var0 >>> 32);
    }

    // $FF: synthetic method
    static int[] brE() {
        int[] var10000 = eaL;
        if (eaL != null) {
            return var10000;
        } else {
            int[] var0 = new int[xo.values().length];

            try {
                var0[xo.bFS.ordinal()] = 4;
            } catch (NoSuchFieldError var9) {
                ;
            }

            try {
                var0[xo.bFP.ordinal()] = 1;
            } catch (NoSuchFieldError var8) {
                ;
            }

            try {
                var0[xo.bFQ.ordinal()] = 2;
            } catch (NoSuchFieldError var7) {
                ;
            }

            try {
                var0[xo.bFX.ordinal()] = 9;
            } catch (NoSuchFieldError var6) {
                ;
            }

            try {
                var0[xo.bFR.ordinal()] = 3;
            } catch (NoSuchFieldError var5) {
                ;
            }

            try {
                var0[xo.bFW.ordinal()] = 8;
            } catch (NoSuchFieldError var4) {
                ;
            }

            try {
                var0[xo.bFT.ordinal()] = 5;
            } catch (NoSuchFieldError var3) {
                ;
            }

            try {
                var0[xo.bFU.ordinal()] = 6;
            } catch (NoSuchFieldError var2) {
                ;
            }

            try {
                var0[xo.bFV.ordinal()] = 7;
            } catch (NoSuchFieldError var1) {
                ;
            }

            eaL = var0;
            return var0;
        }
    }
}
