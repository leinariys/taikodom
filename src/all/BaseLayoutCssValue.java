package all;

import gnu.trove.TObjectIntHashMap;
import org.w3c.dom.css.CSSPrimitiveValue;
import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.css.CSSValue;
import org.w3c.dom.css.CSSValueList;

import java.awt.*;

public abstract class BaseLayoutCssValue implements IBaseLayout {
    public Object createGridBagConstraints(String var1) {
        return var1;
    }

    protected float getValueCss(CSSStyleDeclaration var1, String var2, float var3) {
        CSSValue var4 = var1.getPropertyCSSValue(var2);
        return this.getValueCss(var4, var3);
    }

    protected int getValueCss(CSSStyleDeclaration var1, String var2, int var3) {
        CSSValue var4 = var1.getPropertyCSSValue(var2);
        return (int) this.getValueCss(var4, (float) var3);
    }

    protected float getValueCss(CSSValue var1, float var2) {
        return var1 != null && var1.getCssValueType() == 1 ? ((CSSPrimitiveValue) var1).getFloatValue((short) 0) : var2;
    }

    protected int getValueCss(CSSStyleDeclaration var1, TObjectIntHashMap var2, String var3, int var4) {
        String var5 = var1.getPropertyValue(var3);
        if (var5 != null) {
            var5 = var5.replace('-', '_').toUpperCase().trim();
            if (var2.containsKey(var5)) {
                return var2.get(var5);
            }
        }
        return (int) this.getValueCss(var1, var3, (float) var4);
    }

    protected double[] getValueCss(CSSStyleDeclaration var1, String var2, double[] var3) {
        CSSValue var4 = var1.getPropertyCSSValue(var2);
        if (var4 != null) {
            if (var4.getCssValueType() == 2) {
                CSSValueList var5 = (CSSValueList) var4;
                double[] var6 = new double[var5.getLength()];

                for (int var7 = 0; var7 < var5.getLength(); ++var7) {
                    var6[var7] = (double) this.getValueCss(var5.item(var7), 0.0F);
                }

                return var6;
            }

            if (var4.getCssValueType() == 1) {
                return new double[]{(double) ((CSSPrimitiveValue) var4).getFloatValue((short) 0)};
            }
        }

        return var3;
    }

    protected int[] getValueCss(CSSStyleDeclaration var1, String var2, int[] var3) {
        CSSValue var4 = var1.getPropertyCSSValue(var2);
        if (var4 != null) {
            if (var4.getCssValueType() == 2) {
                CSSValueList var5 = (CSSValueList) var4;
                int[] var6 = new int[var5.getLength()];

                for (int var7 = 0; var7 < var5.getLength(); ++var7) {
                    var6[var7] = (int) this.getValueCss(var5.item(var7), 0.0F);
                }

                return var6;
            }

            if (var4.getCssValueType() == 1) {
                return new int[]{(int) ((CSSPrimitiveValue) var4).getFloatValue((short) 0)};
            }
        }
        return var3;
    }

    protected Insets getValueCss(CSSStyleDeclaration var1, String var2, Insets var3) {
        CSSValue var4 = var1.getPropertyCSSValue(var2);
        Insets var5;
        if (var4 instanceof CSSValueList) {
            var5 = new Insets(0, 0, 0, 0);
            CSSValueList var6 = (CSSValueList) var4;
            if (var6.getLength() >= 4) {
                var5.top = (int) this.getValueCss(var6.item(0), 0.0F);
                var5.left = (int) this.getValueCss(var6.item(1), 0.0F);
                var5.bottom = (int) this.getValueCss(var6.item(2), 0.0F);
                var5.right = (int) this.getValueCss(var6.item(3), 0.0F);
            } else if (var6.getLength() >= 2) {
                var5.top = var5.bottom = (int) this.getValueCss(var6.item(0), 0.0F);
                var5.left = var5.right = (int) this.getValueCss(var6.item(1), 0.0F);
            } else if (var6.getLength() == 1) {
                var5.top = var5.bottom = var5.left = var5.right = (int) this.getValueCss(var6.item(0), 0.0F);
            } else {
                var5.top = var5.bottom = var5.left = var5.right = this.getValueCss(var1, var2, 0);
            }

            return var5;
        } else if (var4 != null) {
            var5 = new Insets(0, 0, 0, 0);
            var5.top = var5.bottom = var5.left = var5.right = this.getValueCss(var1, var2, 0);
            return var5;
        } else {
            return var3;
        }
    }
}
