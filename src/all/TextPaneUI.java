package all;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicTextPaneUI;
import javax.swing.text.JTextComponent;
import java.awt.*;

/**
 * TextPaneUI
 */
public class TextPaneUI extends BasicTextPaneUI implements aIh {
    private final JComponent cIX;
    private final Ek bHX;
    private ComponentManager Rp;

    public TextPaneUI(JComponent var1) {
        this.cIX = var1;
        this.Rp = new ComponentManager("textpane", var1);
        var1.setOpaque(false);
        this.bHX = new Ek(this.Rp);
    }

    public static ComponentUI createUI(JComponent var0) {
        return new TextPaneUI(var0);
    }

    protected void installListeners() {
        super.installListeners();
        this.cIX.addKeyListener(this.bHX);
        this.cIX.addMouseListener(this.bHX);
    }

    protected void uninstallListeners() {
        super.uninstallListeners();
        this.cIX.removeKeyListener(this.bHX);
        this.cIX.removeMouseListener(this.bHX);
    }

    public void update(Graphics var1, JComponent var2) {
        RM.a((aIh) this, (Graphics) var1, (JTextComponent) ((JTextComponent) var2));
        super.paint(var1, var2);
    }

    protected void paintBackground(Graphics var1) {
    }

    public Dimension getMinimumSize(JComponent var1) {
        RM.a((aIh) this, (JTextComponent) ((JTextComponent) var1));
        return this.wy().b(var1, super.getMinimumSize(var1));
    }

    public Dimension getMaximumSize(JComponent var1) {
        RM.a((aIh) this, (JTextComponent) ((JTextComponent) var1));
        return this.wy().a(var1, super.getMaximumSize(var1));
    }

    public Dimension getPreferredSize(JComponent var1) {
        RM.a((aIh) this, (JTextComponent) ((JTextComponent) var1));
        return this.Rp.c(var1, super.getPreferredSize(var1));
    }

    public ComponentManager wy() {
        return this.Rp;
    }

    /**
     * TODO похлже это public pv wy()
     *
     * @return Графический элемент
     */
    @Override
    public aeK wz() {
        return this.Rp;
    }
}
