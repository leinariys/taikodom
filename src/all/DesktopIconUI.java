package all;

import javax.swing.*;
import javax.swing.JInternalFrame.JDesktopIcon;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicDesktopIconUI;
import java.awt.*;

/**
 * DesktopIconUI
 */
public class DesktopIconUI extends BasicDesktopIconUI implements aIh {
    private ComponentManager Rp;

    public DesktopIconUI(JDesktopIcon var1) {
        this.Rp = new ComponentManager("desktopicon", var1);
        var1.setOpaque(false);
    }

    public static ComponentUI createUI(JComponent var0) {
        return new DesktopIconUI((JDesktopIcon) var0);
    }

    public void update(Graphics var1, JComponent var2) {
        RM.a((aIh) this, (Graphics) var1, (JComponent) var2);
        this.paint(var1, var2);
    }

    /**
     * @param var1 Пример javax.swing.JInternalFrame$JDesktopIcon[,0,0,0x0,invalid,hidden,layout=java.awt.BorderLayout,alignmentX=0.0,alignmentY=0.0,border=all.Zp@185afb5,flags=16777216,maximumSize=,minimumSize=,preferredSize=]
     * @return
     */
    public Dimension getPreferredSize(JComponent var1) {
        RM.a((aIh) this, (JComponent) var1);
        return this.wy().c(var1, super.getPreferredSize(var1));
    }

    public Dimension getMinimumSize(JComponent var1) {
        RM.a((aIh) this, (JComponent) var1);
        return this.wy().b(var1, super.getMinimumSize(var1));
    }

    public Dimension getMaximumSize(JComponent var1) {
        RM.a((aIh) this, (JComponent) var1);
        return this.wy().a(var1, super.getMaximumSize(var1));
    }

    public ComponentManager wy() {
        return this.Rp;
    }

    protected void installComponents() {
        this.iconPane = new BasicInternalFrameTitlePaneCustomWrapper(this.frame);
        this.desktopIcon.setLayout(new BorderLayout());
        this.desktopIcon.add(this.iconPane, "Center");
    }

    /**
     * TODO похлже это public pv wy()
     *
     * @return Графический элемент
     */
    @Override
    public aeK wz() {
        return this.Rp;
    }
}
