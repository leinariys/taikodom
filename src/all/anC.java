package all;

import taikodom.render.SoundBuffer;
import taikodom.render.loader.RenderAsset;
import taikodom.render.scene.SSoundGroup;
import taikodom.render.scene.SSoundSource;

/**
 * class Ix
 */
public class anC implements NJ {
    public void a(RenderAsset var1) {
        if (var1 instanceof SSoundSource) {
            SSoundSource var2 = (SSoundSource) var1;
            var2.getBuffer().getData();
        } else if (var1 instanceof SoundBuffer) {
            SoundBuffer var4 = (SoundBuffer) var1;
            var4.getData();
        } else if (var1 instanceof SSoundGroup) {
            SSoundGroup var5 = (SSoundGroup) var1;

            for (int var3 = 0; var3 < var5.bufferCount(); ++var3) {
                var5.getBuffer(var3).getData();
            }
        }
    }
}
