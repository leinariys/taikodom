package all;

import java.awt.*;
import java.util.Map;

public class akP {
    private String name;
    private Component component;
    private Map attributes;

    public akP(String var1, Component var2) {
        this.name = var1 != null ? var1 : var2.getName();
        this.component = var2;
    }

    public String getName() {
        return this.name;
    }

    public Component getComponent() {
        return this.component;
    }

    public void setAttributes(Map var1) {
        this.attributes = var1;
    }

    public Object getAttribute(String var1) {
        return this.attributes == null ? null : this.attributes.get(var1);
    }
}
