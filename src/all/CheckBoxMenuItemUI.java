package all;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicMenuItemUI;
import java.awt.*;

/**
 * CheckBoxMenuItemUI
 */
public class CheckBoxMenuItemUI extends BasicMenuItemUI implements aIh {
    private ComponentManager Rp;

    public CheckBoxMenuItemUI(JMenuItem var1) {
        this.Rp = new ComponentManager("menuitem", var1);
        var1.setOpaque(false);
    }

    public static ComponentUI createUI(JComponent var0) {
        return new CheckBoxMenuItemUI((JMenuItem) var0);
    }

    public void update(Graphics var1, JComponent var2) {
        RM.a((aIh) this, (Graphics) var1, (AbstractButton) ((AbstractButton) var2));
        this.paint(var1, var2);
    }

    protected void paintBackground(Graphics var1, JMenuItem var2, Color var3) {
    }

    public Dimension getPreferredSize(JComponent var1) {
        RM.a((aIh) this, (AbstractButton) ((AbstractButton) var1));
        return this.wy().c(var1, super.getPreferredSize(var1));
    }

    public Dimension getMinimumSize(JComponent var1) {
        RM.a((aIh) this, (AbstractButton) ((AbstractButton) var1));
        return this.wy().b(var1, super.getMinimumSize(var1));
    }

    public Dimension getMaximumSize(JComponent var1) {
        RM.a((aIh) this, (AbstractButton) ((AbstractButton) var1));
        return this.wy().a(var1, super.getMaximumSize(var1));
    }

    public ComponentManager wy() {
        return this.Rp;
    }

    /**
     * TODO похлже это public pv wy()
     *
     * @return Графический элемент
     */
    @Override
    public aeK wz() {
        return this.Rp;
    }
}
