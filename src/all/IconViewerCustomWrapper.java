package all;

import taikodom.render.graphics2d.TexPolygon;

import javax.swing.*;
import java.awt.*;

public class IconViewerCustomWrapper extends PictureCustomWrapper {

    private static final int duN = 8;
    private TexPolygon duO;
    private int duP;

    public IconViewerCustomWrapper(ILoaderImageInterface var1) {
        this(var1, 8);
    }

    public IconViewerCustomWrapper(ILoaderImageInterface var1, int var2) {
        super(var1);
        this.duP = var2;
    }

    public void lB(int var1) {
        this.duP = var1;
    }

    public void setIcon(Icon var1) {
        if (var1 instanceof ImageIcon) {
            this.b(((ImageIcon) var1).getImage());
        } else {
            super.setIcon(var1);
        }

    }

    protected void b(Image var1) {
        this.duO = new TexPolygon(var1);
        this.duO.createOctagon(this.duP);
        super.setIcon(new ImageIcon(var1) {
            private static final long serialVersionUID = 1L;

            public synchronized void paintIcon(Component var1, Graphics var2, int var3, int var4) {
                Graphics2D var5 = (Graphics2D) var2.create(var3, var4, var1.getWidth(), var1.getHeight());
                var5.fill(IconViewerCustomWrapper.this.duO);
            }
        });
    }
}
