package all;

import org.xmlpull.mxp1.MXParser;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.*;
import java.util.Stack;

public class axx {
    int[] aQl = new int[2];

    public XmlNode aD(String var1, String var2) throws IOException {
        return this.a((InputStream) (new FileInputStream(var1)), (String) var2);
    }

    public XmlNode a(InputStream var1, String var2) throws IOException {
        try {
            MXParser var3 = new MXParser();
            var3.setInput(new InputStreamReader(var1, var2));
            return (new axx()).a((XmlPullParser) var3);
        } catch (XmlPullParserException var4) {
            throw new RuntimeException(var4);
        }
    }

    public XmlNode a(StringReader var1) throws IOException {
        try {
            MXParser var2 = new MXParser();
            var2.setInput(var1);
            return (new axx()).a((XmlPullParser) var2);
        } catch (XmlPullParserException var3) {
            throw new RuntimeException(var3);
        }
    }

    private XmlNode a(XmlPullParser var1) throws IOException, XmlPullParserException {
        XmlNode var2 = new XmlNode();
        Stack var3 = new Stack();
        var3.push(var2);
        boolean var4 = false;

        while (!var4) {
            int var5 = var1.next();
            switch (var5) {
                case 0:
                default:
                    break;
                case 1:
                    var4 = true;
                    break;
                case 2:
                    XmlNode var6 = this.a((XmlNode) var3.lastElement(), var1);
                    var3.add(var6);
                    break;
                case 3:
                    var3.pop();
                    break;
                case 4:
                    this.b((XmlNode) var3.lastElement(), var1);
            }
        }

        return (XmlNode) var2.getChildrenTag().get(0);
    }

    private XmlNode a(XmlNode var1, XmlPullParser var2) {
        XmlNode var3 = var1.addChildrenTag(XmlNode.mB(var2.getName()));

        for (int var4 = 0; var4 < var2.getAttributeCount(); ++var4) {
            String var5 = var2.getAttributeName(var4);
            String var6 = var2.getAttributeValue(var4);
            var3.addAttribute(var5, var6);
        }

        return var3;
    }

    private void b(XmlNode var1, XmlPullParser var2) {
        char[] var3 = var2.getTextCharacters(this.aQl);
        int var4 = this.aQl[0];
        int var5 = this.aQl[1];
        var1.my(new String(var3, var4, var5));
    }
}
