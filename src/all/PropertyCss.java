package all;

import org.w3c.dom.css.CSSValue;

import java.io.Serializable;

public class PropertyCss implements Serializable {
    private String name;
    private CSSValue cssValue;
    private boolean isImportant;

    public PropertyCss(String var1, CSSValue var2, boolean var3) {
        this.name = var1.intern();
        this.cssValue = var2;
        this.isImportant = var3;
    }

    public String getName() {
        return this.name;
    }

    public CSSValue getCssValue() {
        return this.cssValue;
    }

    public void setCssValue(CSSValue var1) {
        this.cssValue = var1;
    }

    public boolean getImportant() {
        return this.isImportant;
    }

    public void setImportant(boolean var1) {
        this.isImportant = var1;
    }

    public String toString() {
        return this.name + ": " + this.cssValue.toString() + (this.isImportant ? " !isImportant" : "");
    }
}
