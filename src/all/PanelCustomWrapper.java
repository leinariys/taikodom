package all;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Базовые элементы интерфейса
 * тег panel JPanel aCo
 * class BaseUItegXML PanelCustom  BaseItemFactory
 */
public class PanelCustomWrapper extends JPanel implements IContainerCustomWrapper {
    /**
     *
     */
    protected List childComponent = new ArrayList();

    /**
     * Размер текстуры
     */
    private Rectangle graphicsClipSize;

    /**
     *
     */
    private a bJg;

    public PanelCustomWrapper() {
        this.setBackground(Color.black);
        this.bJg = new a(this);
    }

    public PanelCustomWrapper(LayoutManager var1) {
        super(var1);
        this.setBackground(Color.black);
    }


    /**
     * Задать размер Rectangle
     *
     * @param var1
     */
    public void c(Rectangle var1) {
        this.graphicsClipSize = var1;
    }

    /**
     * Задать размер Rectangle
     *
     * @param var1
     * @param var2
     * @param var3
     * @param var4
     */
    public void setBoundsss(int var1, int var2, int var3, int var4) {
        if (this.graphicsClipSize == null) {
            this.graphicsClipSize = new Rectangle(var1, var2, var3, var4);
        } else {
            this.graphicsClipSize.x = var1;
            this.graphicsClipSize.y = var2;
            this.graphicsClipSize.width = var3;
            this.graphicsClipSize.height = var4;
        }
    }

    /**
     * @param var1
     */
    public void paint(Graphics var1) {
        if (this.graphicsClipSize != null) {
            var1.setClip(this.graphicsClipSize.x, this.graphicsClipSize.y, this.graphicsClipSize.width, this.graphicsClipSize.height);
        }

        super.paint(var1);
        if (this.bJg != null) {
            this.bJg.paint(var1);
        }
    }

    public void center() {
        if (this.getParent() != null) {
            this.setLocation((this.getParent().getWidth() - this.getWidth()) / 2, (this.getParent().getHeight() - this.getHeight()) / 2);
        }
    }


    /**
     * Получить Rectangle
     *
     * @return
     */
    public Rectangle cPL() {
        return this.graphicsClipSize;
    }

    /**
     * @param var1
     * @return
     */
    public ProgressBarCustomWrapper findJProgressBar(String var1) {
        Component[] var5 = this.getComponents();
        int var4 = var5.length;

        for (int var3 = 0; var3 < var4; ++var3) {
            Component var2 = var5[var3];
            if (var2 instanceof ProgressBarCustomWrapper && var1.equals(var2.getName())) {
                return (ProgressBarCustomWrapper) var2;
            }

            if (var2 instanceof IContainerCustomWrapper) {
                ProgressBarCustomWrapper var6 = ((IContainerCustomWrapper) var2).findJProgressBar(var1);
                if (var6 != null) {
                    return var6;
                }
            }
        }
        return null;
    }

    /**
     * @param var1
     * @return
     */
    public JButton findJButton(String var1) {
        Component[] var5 = this.getComponents();
        int var4 = var5.length;

        for (int var3 = 0; var3 < var4; ++var3) {
            Component var2 = var5[var3];
            if (var2 instanceof JButton && var1.equals(var2.getName())) {
                return (JButton) var2;
            }

            if (var2 instanceof IContainerCustomWrapper) {
                JButton var6 = ((IContainerCustomWrapper) var2).findJButton(var1);
                if (var6 != null) {
                    return var6;
                }
            }
        }
        return null;
    }

    /**
     * Поиск любого компонента по имени
     *
     * @param var1
     * @return
     */
    public Component findJComponent(String var1) {
        Iterator var3 = this.childComponent.iterator();
        Component var2;
        while (var3.hasNext()) {
            var2 = (Component) var3.next();
            System.out.println("findJComponent1 " + var2);
            if (var1.equals(var2.getName())) {
                return var2;
            }
        }
        Component[] var5 = this.getComponents();
        int var4 = var5.length;

        for (int var7 = 0; var7 < var4; ++var7) {
            var2 = var5[var7];
            System.out.println("findJComponent2 " + var2);
            if (var1.equals(var2.getName())) {
                return var2;
            }

            if (var2 instanceof IContainerCustomWrapper) {
                Component var6 = ((IContainerCustomWrapper) var2).findJComponent(var1);
                if (var6 != null) {
                    return var6;
                }
            }
        }
        return null;
    }

    /**
     * @param var1
     * @return
     */
    public TextfieldCustomWrapper findJTextField(String var1)//JTextField
    {
        Component[] var5 = this.getComponents();
        int var4 = var5.length;
        for (int var3 = 0; var3 < var4; ++var3) {
            Component var2 = var5[var3];
            if (var2 instanceof TextfieldCustomWrapper && var1.equals(var2.getName())) {
                return (TextfieldCustomWrapper) var2;
            }
            if (var2 instanceof IContainerCustomWrapper) {
                TextfieldCustomWrapper var6 = ((IContainerCustomWrapper) var2).findJTextField(var1);
                if (var6 != null) {
                    return var6;
                }
            }
        }
        return null;
    }

    /**
     * Поиск графического элемента по имени атрибута name Label
     *
     * @param var1 Например versionLabel
     * @return
     */
    public JLabel findJLabel(String var1) {
        //TODO ERROR 0 length
        Component[] var5 = this.getComponents();
        int var4 = var5.length;
        for (int var3 = 0; var3 < var4; ++var3) {
            Component var2 = var5[var3];
            if (var2 instanceof JLabel && var1.equals(var2.getName())) {
                return (JLabel) var2;
            }

            if (var2 instanceof IContainerCustomWrapper) {
                JLabel var6 = ((IContainerCustomWrapper) var2).findJLabel(var1);
                if (var6 != null) {
                    return var6;
                }
            }
        }
        return null;
    }

    /**
     * Поиск графического элемента по имени атрибута name ComboBox
     *
     * @param var1
     * @return
     */
    public JComboBox findJComboBox(String var1) {
        Component[] var5 = this.getComponents();
        int var4 = var5.length;
        for (int var3 = 0; var3 < var4; ++var3) {
            Component var2 = var5[var3];
            if (var2 instanceof JComboBox && var1.equals(var2.getName())) {
                return (JComboBox) var2;
            }

            if (var2 instanceof IContainerCustomWrapper) {
                JComboBox var6 = ((IContainerCustomWrapper) var2).findJComboBox(var1);
                if (var6 != null) {
                    return var6;
                }
            }
        }
        return null;
    }

    public IContainerCustomWrapper ce(String var1) {
        Component[] var5 = this.getComponents();
        int var4 = var5.length;

        for (int var3 = 0; var3 < var4; ++var3) {
            Component var2 = var5[var3];
            if (var2 instanceof IContainerCustomWrapper) {
                if (var1.equals(var2.getName())) {
                    return (IContainerCustomWrapper) var2;
                }

                IContainerCustomWrapper var6 = ((IContainerCustomWrapper) var2).ce(var1);
                if (var6 != null) {
                    return var6;
                }
            }
        }
        return null;
    }

    public RepeaterCustomWrapper ch(String var1) {
        if (var1 == null) {
            return null;
        } else {
            Iterator var3 = this.childComponent.iterator();

            while (var3.hasNext()) {
                RepeaterCustomWrapper var2 = (RepeaterCustomWrapper) var3.next();
                if (var1.equals(var2.getName())) {
                    return var2;
                }
            }

            Component[] var5 = this.getComponents();
            int var4 = var5.length;

            for (int var8 = 0; var8 < var4; ++var8) {
                Component var7 = var5[var8];
                if (var7 instanceof IContainerCustomWrapper) {
                    RepeaterCustomWrapper var6 = ((IContainerCustomWrapper) var7).ch(var1);
                    if (var6 != null) {
                        return var6;
                    }
                }
            }
            return null;
        }
    }

    public void addChild(RepeaterCustomWrapper var1) {
        this.childComponent.add(var1);
    }


    public void pack() {
        try {
            this.setSize(this.getPreferredSize());
            this.validate();
        } catch (NullPointerException var1) {
            System.out.println("Exception-" + var1);
        }
    }

    public Component add(Component var1) {
        return var1 instanceof RepeaterCustomWrapper ? var1 : super.add(var1);
    }

    public String getElementName() {
        return "panel";
    }

    public void setEnabled(boolean var1) {
        Component[] var5 = this.getComponents();
        int var4 = var5.length;

        for (int var3 = 0; var3 < var4; ++var3) {
            Component var2 = var5[var3];
            var2.setEnabled(var1);
        }

        super.setEnabled(var1);
    }

    public void setFocusable(boolean var1) {
        Component[] var5 = this.getComponents();
        int var4 = var5.length;

        for (int var3 = 0; var3 < var4; ++var3) {
            Component var2 = var5[var3];
            var2.setFocusable(var1);
        }

        super.setFocusable(var1);
    }

    //Вызывается когда в class BaseUItegXML компонет добовляется в рут панель var6.add(var4);
    public void validate() {
        super.validate();
        if (!this.isValid()) {
            synchronized (this.getTreeLock()) {
                if (!this.isValid()) {
                    this.validateTree();
                }
            }
        }
    }

    public void removeNotify() {
        if (this.getParent() != null) {
            this.getParent().repaint(this.getX(), this.getY(), this.getWidth(), this.getHeight());
        }
        super.removeNotify();
    }

    public void remove(int var1) {
        Component var2 = this.getComponent(var1);
        if (var2 != null) {
            this.repaint(var2.getX(), var2.getY(), var2.getWidth(), var2.getHeight());
        }
        super.remove(var1);
    }

    public int getAlpha() {
        PropertiesUiFromCss var1 = PropertiesUiFromCss.g(this);
        if (var1 != null) {
            Color var2 = var1.getColorMultiplier();
            if (var2 != null) {
                return var2.getAlpha();
            }
        }
        return 255;
    }

    public void setAlpha(int var1) {
        PropertiesUiFromCss var2 = PropertiesUiFromCss.g(this);
        if (var2 != null) {
            var2.setColorMultiplier(new Color(var1, var1, var1, var1));
        }
    }

    //Вызывается когда в class BaseUItegXML компонет добовляется в рут панель var6.add(var4);
    public void validateTree() {
        try {
            super.validateTree();
        } catch (NullPointerException var1) {
            System.out.println("Exception-" + var1);
        }
    }

    public wz cpV() {
        return (wz) this.getClientProperty("gui.TooltipProvider");
    }

    public void a(wz var1) {
        this.putClientProperty("gui.TooltipProvider", var1);
    }

    public void Kk() {
        this.bJg.Kk();
    }

    public void Kl() {
        this.bJg.Kl();
    }

    public void destroy() {
        if (this.getParent() != null) {
            this.getParent().remove(this);
        }
    }
}
