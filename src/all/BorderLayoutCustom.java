package all;

import org.w3c.dom.css.CSSStyleDeclaration;

import java.awt.*;
import java.io.StringReader;

public class BorderLayoutCustom extends BaseLayoutCssValue {
    private static LogPrinter logger = LogPrinter.K(BorderLayoutCustom.class);

    public LayoutManager createLayout(Container var1, String var2) {
        BorderLayout var3 = new BorderLayout();
        if (var2 != null && !(var2 = var2.trim()).isEmpty()) {
            try {
                CSSStyleDeclaration var4 = ParserCss.getParserCss().getCSSStyleDeclaration(new InputSource(new StringReader(var2)));
                var3.setHgap(this.getValueCss(var4, "hgap", var3.getHgap()));
                var3.setVgap(this.getValueCss(var4, "vgap", var3.getVgap()));
            } catch (Exception var6) {
                logger.warn(var6);
            }
        }
        return var3;
    }
}
