package all;

import gnu.trove.TIntHashSet;

import javax.swing.text.JTextComponent;
import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * Джостик константы клавиш
 * class CommandTranslatorAddon
 */
public class aAq_q implements KeyEventDispatcher {
    private final vW kj;
    long hdO;
    boolean hdP;
    private int hdM;
    private TIntHashSet hdN = new TIntHashSet();

    public aAq_q(vW var1) {
        this.kj = var1;
    }

    public int cHM() {
        return this.hdM;
    }

    public boolean dispatchKeyEvent(KeyEvent var1) {
        if (var1.getID() == 402 && var1.getKeyCode() == 18) {
            this.b(var1);
            return true;
        } else {
            Component var2 = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
            BaseUItegXML var3 = BaseUItegXML.thisClass;
            //akU var4 = this.kj.dL();
            boolean var5 = var2 instanceof JTextComponent && ((JTextComponent) var2).isEditable();
            //if (var4 != null)
            {
                //t var6 = var4.dxc();
                if (/*var6 != null && var6.anX() && !var6.dL().bQB() ||*/ var2 == var3.getRootPane() || var2 == var3.getRootPane().getParent().getParent() || var2 == var3.getRootPane().getParent()) {
                    this.b(var1);
                    var1.consume();
                    return true;
                }
            }

            if (var2 != null && !var2.isShowing()) {
                this.b(var1);
                return false;
            } else if (var1.getKeyCode() == 20 && !var5) {
                this.b(var1);
                return false;
            } else if (var2 instanceof Panel) {
                this.b(var1);
                return false;
            } else if (var5 && var1.getKeyCode() == 27) {
                var2.transferFocusUpCycle();
                return false;
            } else if (var1.getKeyCode() == 27) {
                this.b(var1);
                return false;
            } else if (var5 || var1.getModifiers() == 0 && this.hdM == 0) {
                if (var1.getID() == 402 && this.hdN.contains(var1.getKeyCode())) {
                    this.b(var1);
                    return false;
                } else {
                    return false;
                }
            } else {
                this.b(var1);
                return false;
            }
        }
    }

    private void b(KeyEvent var1) {
        int var3;
        switch (var1.getID()) {
            case 400:
                if (var1.getKeyCode() == 20 && this.hdP && (double) (System.nanoTime() - this.hdO) < 2.0E9D) {
                    return;
                }

                this.d(new mg(Nb.lP(var1.getKeyCode()), true, var1.getKeyChar(), Nb.lQ(var1.getModifiersEx()), true));
                break;
            case 401:
                this.hdN.add(var1.getKeyCode());
                if (var1.getKeyCode() == 20 && this.hdP && (double) (System.nanoTime() - this.hdO) < 1.0E9D) {
                    return;
                }

                int var6 = Nb.lP(var1.getKeyCode());
                if (var1.getKeyCode() == 32) {
                    this.hdM = var1.getKeyCode();
                    this.d(new mg(var6, true, var1.getKeyChar(), Nb.lQ(var1.getModifiersEx()), false));
                } else if (var1.getKeyCode() == 16) {
                    this.d(new mg(Nb.lP(var1.getKeyCode()), true, var1.getKeyChar(), 0, false));
                } else if (var1.getKeyCode() == 17) {
                    this.d(new mg(Nb.lP(var1.getKeyCode()), true, var1.getKeyChar(), 0, false));
                } else {
                    var3 = var1.getModifiersEx();
                    if (this.hdM != 0) {
                        var3 |= this.hdM;
                    }

                    this.d(new mg(Nb.lP(var1.getKeyCode()), true, var1.getKeyChar(), Nb.lQ(var3), false));
                }
                break;
            case 402:
                this.hdN.remove(var1.getKeyCode());
                if (var1.getKeyCode() == 20) {
                    if (this.hdP && (double) (System.nanoTime() - this.hdO) < 1.0E9D) {
                        this.hdP = false;
                        return;
                    }

                    this.hdO = System.nanoTime();
                    this.hdP = true;

                    try {
                        Toolkit.getDefaultToolkit().setLockingKeyState(20, !Toolkit.getDefaultToolkit().getLockingKeyState(20));
                    } catch (Exception var5) {
                        System.out.println("Exception-" + var5);
                    }
                }

                byte var2 = 32;
                var3 = Nb.lP(var1.getKeyCode());
                int var4 = var1.getModifiersEx();
                if (var3 == var2 && this.hdM == 32) {
                    this.d(new mg(var3, false, var1.getKeyChar(), Nb.lQ(var4), false));
                    this.hdM = 0;
                } else {
                    if (this.hdM != 0) {
                        var4 |= this.hdM;
                    }

                    this.d(new mg(Nb.lP(var1.getKeyCode()), false, var1.getKeyChar(), Nb.lQ(var4), false));
                }

                if (var4 != 0) {
                    this.d(new mg(var3, false, var1.getKeyChar(), 0, false));
                }
        }

    }

    void d(mg var1) {
        this.kj.getEngineGame().getEventManager().h(var1);
    }
}
