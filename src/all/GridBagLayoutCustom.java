package all;

import gnu.trove.TObjectIntHashMap;
import org.w3c.dom.css.CSSStyleDeclaration;

import java.awt.*;
import java.io.StringReader;

public class GridBagLayoutCustom extends BaseLayoutCssValue {
    private static final TObjectIntHashMap limitation = new TObjectIntHashMap();
    private static LogPrinter logger = LogPrinter.K(GridBagLayoutCustom.class);

    static {
        limitation.put("ABOVE_BASELINE", 1024);
        limitation.put("ABOVE_BASELINE_LEADING", 1280);
        limitation.put("ABOVE_BASELINE_TRAILING", 1536);
        limitation.put("BASELINE", 256);
        limitation.put("BASELINE_LEADING", 512);
        limitation.put("BASELINE_TRAILING", 768);
        limitation.put("BELOW_BASELINE", 1792);
        limitation.put("BELOW_BASELINE_LEADING", 2048);
        limitation.put("BELOW_BASELINE_TRAILING", 2304);
        limitation.put("CENTER", 10);
        limitation.put("EAST", 13);
        limitation.put("FIRST_LINE_END", 24);
        limitation.put("FIRST_LINE_START", 23);
        limitation.put("LAST_LINE_END", 26);
        limitation.put("LAST_LINE_START ", 25);
        limitation.put("LINE_END", 22);
        limitation.put("LINE_START", 21);
        limitation.put("NORTH", 11);
        limitation.put("NORTHEAST", 12);
        limitation.put("NORTHWEST", 18);
        limitation.put("PAGE_END", 20);
        limitation.put("PAGE_START", 19);
        limitation.put("SOUTH", 15);
        limitation.put("SOUTHEAST", 14);
        limitation.put("SOUTHWEST", 16);
        limitation.put("WEST", 17);
        limitation.put("NONE", 0);
        limitation.put("HORIZONTAL", 2);
        limitation.put("VERTICAL", 3);
        limitation.put("BOTH", 1);
    }

    public Object createGridBagConstraints(String var1) {
        GridBagConstraints var2 = new GridBagConstraints();
        if (var1 == null) {
            return var2;
        } else {
            try {
                CSSStyleDeclaration var3 = ParserCss.getParserCss().getCSSStyleDeclaration(new InputSource(new StringReader("{" + var1.trim() + "}")));
                var2.gridx = (int) this.getValueCss(var3, "gridx", (float) var2.gridx);
                var2.gridy = (int) this.getValueCss(var3, "gridy", (float) var2.gridx);
                var2.gridwidth = (int) this.getValueCss(var3, "gridwidth", (float) var2.gridwidth);
                var2.gridheight = (int) this.getValueCss(var3, "gridheight", (float) var2.gridheight);
                var2.weightx = (double) this.getValueCss(var3, "weightx", (float) var2.weightx);
                var2.weighty = (double) this.getValueCss(var3, "weighty", (float) var2.weighty);
                var2.anchor = this.getValueCss(var3, limitation, "anchor", var2.anchor);
                var2.fill = this.getValueCss(var3, limitation, "fill", var2.fill);
                var2.ipadx = (int) this.getValueCss(var3, "ipadx", (float) var2.ipadx);
                var2.ipady = (int) this.getValueCss(var3, "ipady", (float) var2.ipady);
                var2.insets = this.getValueCss(var3, "insets", var2.insets);
            } catch (Exception var4) {
                logger.error(var4);
            }

            return var2;
        }
    }

    public LayoutManager createLayout(Container var1, String var2) {
        GridBagLayout var3 = new GridBagLayout();
        if (var2 != null && !(var2 = var2.trim()).isEmpty()) {
            try {
                CSSStyleDeclaration var4 = ParserCss.getParserCss().getCSSStyleDeclaration(new InputSource(new StringReader(var2)));
                var3.columnWidths = this.getValueCss(var4, "columnWidths", var3.columnWidths);
                var3.rowHeights = this.getValueCss(var4, "rowHeights", var3.rowHeights);
                var3.columnWeights = this.getValueCss(var4, "columnWeights", var3.columnWeights);
                var3.rowWeights = this.getValueCss(var4, "rowWeights", var3.rowWeights);
            } catch (Exception var5) {
                logger.warn(var5);
            }
        }
        return var3;
    }
}
