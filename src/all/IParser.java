package all;

import java.util.Locale;

//package org.w3c.css.sac;
public interface IParser {
    void setLocale(Locale var1);

    void setDocumentHandler(DocumentHandler var1);

    void setSelectorFactory(SelectorFactory var1);

    void setConditionFactory(IConditionFactory var1);

    void setErrorHandler(ErrorHandler var1);

    void parseStyleSheet(InputSource var1);

    void parseStyleSheet(String var1);

    void parseStyleDeclaration(InputSource var1);

    void parseRule(InputSource var1);

    String getParserVersion();

    SelectorList parseSelectors(InputSource var1);

    LexicalUnit parsePropertyValue(InputSource var1);

    boolean parsePriority(InputSource var1);
}
