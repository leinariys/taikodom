package all;

/**
 * class InterfaceSFXAddon
 */
public class auD_t {
    private final boolean gEh;
    private final aeO gEi;
    private final boolean stop;

    public auD_t(aeO var1) {
        this(var1, false, false);
    }

    public auD_t(aeO var1, boolean var2) {
        this(var1, var2, false);
    }

    public auD_t(aeO var1, boolean var2, boolean var3) {
        this.gEi = var1;
        this.stop = var2;
        this.gEh = var3;
    }

    public aeO aDI() {
        return this.gEi;
    }

    public boolean cxL() {
        return this.gEh;
    }

    public boolean aDJ() {
        return this.stop;
    }
}
