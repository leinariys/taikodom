package all;

import javax.vecmath.*;
import java.nio.FloatBuffer;

public class aJF extends Vector4f //implements afA_q
{
    public aJF() {
    }

    public aJF(float[] var1) {
        super(var1);
    }

    public aJF(Vector4f var1) {
        super(var1);
    }

    public aJF(Vector4d var1) {
        super(var1);
    }

    public aJF(Tuple4f var1) {
        super(var1);
    }

    public aJF(Tuple4d var1) {
        super(var1);
    }

    public aJF(Tuple3f var1) {
        super(var1);
    }

    public aJF(float var1, float var2, float var3, float var4) {
        super(var1, var2, var3, var4);
    }

    public aJF nScale(float var1) {
        float var2 = this.x * var1;
        float var3 = this.y * var1;
        float var4 = this.z * var1;
        float var5 = this.w * var1;
        return new aJF(var2, var3, var4, var5);
    }

    public aJF sScale(float var1) {
        this.x *= var1;
        this.y *= var1;
        this.z *= var1;
        this.w *= var1;
        return this;
    }

    public aJF nSub(Tuple4f var1) {
        aJF var2 = new aJF(this);
        var2.sub(var1);
        return var2;
    }

    public FloatBuffer fillBuffer(FloatBuffer var1) {
        var1.clear();
        var1.put(this.x);
        var1.put(this.y);
        var1.put(this.z);
        var1.put(this.w);
        var1.flip();
        return var1;
    }


//TODO На удаление
/*
   @Override
   public void readExternal(Vm_q var1) {
      this.x = var1.gZ("x");
      this.y = var1.gZ("y");
      this.z = var1.gZ("z");
      this.w = var1.gZ("w");
   }
   @Override
   public void writeExternal(att var1) {
      var1.all("x", this.x);
      var1.all("y", this.y);
      var1.all("z", this.z);
      var1.all("w", this.w);
   }
*/
}
