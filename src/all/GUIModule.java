package all;

import taikodom.render.DrawContext;
import taikodom.render.JGLDesktop;
import taikodom.render.RenderView;
import taikodom.render.graphics2d.RGraphics2;
import taikodom.render.graphics2d.RGraphicsContext;
import taikodom.render.textures.RenderTarget;
import taikodom.render.textures.SimpleTexture;

import javax.media.opengl.GL;
import javax.swing.*;
import java.awt.*;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedList;
import java.util.List;

/**
 * GUIModule
 * HL.java
 */
public class GUIModule {
    public static String aGx = "GUIModule";
    static LogPrinter logger = LogPrinter.K(GUIModule.class);
    boolean hYN = false;
    Rectangle ddg = new Rectangle();
    ain rootPathRender;
    private EngineGraphics engineGraphics;
    private RenderView rv;
    private ILoaderImageInterface Th;
    /**
     * Загрузчик шрифтов
     */
    private pq_q hYO;
    private long fKP;
    private long fKQ;
    private RGraphics2 hYP;
    private RGraphics2 hYQ;
    private RenderTarget renderTarget;
    private SimpleTexture texture;
    private boolean hYR = true;

    public GUIModule() {
        agC.a(new agC(this));
    }

    /**
     * Настройка
     *
     * @param rootPathRender  Каталог с кодом отрисовки графики
     * @param pathGuiImageset "/data/gui/imageset/"
     * @param pathFonts       "/data/fonts/"
     * @param engineGraphics  Графический движок
     * @param var5
     */
    public void setProperties(ain rootPathRender, String pathGuiImageset, String pathFonts, EngineGraphics engineGraphics, aBg var5) {
        this.rootPathRender = rootPathRender;
        this.engineGraphics = engineGraphics;
        aKb.getSharedInstance();
        JGLDesktop var6 = this.engineGraphics.aee();
        RepaintManager var7 = RepaintManager.currentManager(var6);
        var7.setDoubleBufferingEnabled(false);
        var6.getRootPane().setDoubleBuffered(false);
        var6.getRootPane().setOpaque(false);
        var6.getRootPane().setBackground(new Color(0, 0, 0, 0));
        var6.getContentPane().setBackground(new Color(0, 0, 0, 0));
        var6.getLayeredPane().setBackground(new Color(0, 0, 0, 0));
        var6.getLayeredPane().setOpaque(false);
        var6.setBackground(new Color(0, 0, 0, 0));
        this.rv = new RenderView(this.engineGraphics.aef());
        this.rv.setViewport(0, 0, var6.getWidth(), var6.getHeight());
        /**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
         */
        javax.media.opengl.GL var8 = var6.getGL();
        this.rv.setGL(var8);
        this.rv.getDrawContext().setMaxShaderQuality(this.engineGraphics.getShaderQuality());
        RGraphicsContext var9 = new RGraphicsContext(this.rv.getDrawContext());
        this.hYP = new RGraphics2(var9);
        this.hYQ = new RGraphics2(var9);
        this.hYO = new pq_q(this);
        LoaderImageBikSwfPng var10 = new LoaderImageBikSwfPng(this.engineGraphics);
        var10.setRootPathRender(this.rootPathRender);
        this.Th = var10;
        ToolTipManager.sharedInstance().setInitialDelay(700);
        ToolTipManager.sharedInstance().setDismissDelay(Integer.MAX_VALUE);
        ToolTipManager.sharedInstance().setReshowDelay(500);
    }

    public void dispose() {
    }

    public int getScreenWidth() {
        return this.ddH().getWidth();
    }

    public int getScreenHeight() {
        return this.ddH().getHeight();
    }

    public boolean step(float var1) {
        if (!this.hYR) {
            return true;
        } else {
            DrawContext var2 = this.rv.getDrawContext();
            var2.setMaxShaderQuality(this.engineGraphics.getShaderQuality());
            /**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
             */
            javax.media.opengl.GL var3 = var2.getGL();
            var3.glMatrixMode(5888);
            var3.glLoadIdentity();
            var3.glMatrixMode(5889);
            var3.glLoadIdentity();
            //Значение настройки [render] xres yres
            JGLDesktop var4 = this.engineGraphics.aee();
            var3.glViewport(0, 0, var4.getWidth(), var4.getHeight());
            var3.glDisable(2929);
            var3.glOrtho(0.0D, (double) var4.getWidth(), (double) var4.getHeight(), 0.0D, -1.0D, 1.0D);
            var3.glMatrixMode(5888);
            var3.glDisable(2884);
            var2.setCurrentFrame(10L);
            if (!this.hYN) {
                if (this.renderTarget == null) {
                    this.renderTarget = new RenderTarget();
                    this.renderTarget.setWidth(this.getScreenWidth());
                    this.renderTarget.setHeight(this.getScreenHeight());
                    this.renderTarget.create(this.getScreenWidth(), this.getScreenHeight(), false, var2, true);
                    this.texture = new SimpleTexture();
                    //Простая текстура для графического интерфейса
                    this.texture.setName("Simple texture for GUI");
                    this.texture.createEmptyRGBA(this.getScreenWidth(), this.getScreenHeight(), 32);
                    this.renderTarget.bind(var2);
                    try {
                        var3.glDisable(3089);
                        var3.glClearColor(0.0F, 0.0F, 0.0F, 0.0F);
                        var3.glClear(16384);
                        this.renderTarget.copyBufferToTexture(var2);

                    } catch (Exception var24) {
                        var24.printStackTrace();

                    } finally {
                        this.renderTarget.unbind(var2);
                    }
                }
                //Менеджер перерисовки
                amC var5 = (amC) RepaintManager.currentManager((JComponent) null);
                if (this.ddg.width != 0 && this.ddg.height != 0) {
                    if (var2.getMaxShaderQuality() <= 1) {
                        this.renderTarget.bind(var2);
                    }

                    try {
                        int var6 = var4.getHeight() - this.ddg.y - this.ddg.height;
                        var3.glClearColor(0.0F, 0.0F, 0.0F, 0.0F);
                        var3.glClear(16384);
                        var3.glMatrixMode(5888);
                        var3.glLoadIdentity();
                        var3.glMatrixMode(5889);
                        var3.glLoadIdentity();
                        var3.glViewport(0, 0, var4.getWidth(), var4.getHeight());
                        var3.glDisable(2929);
                        var3.glDisable(3042);
                        var3.glOrtho(0.0D, (double) var4.getWidth(), (double) var4.getHeight(), 0.0D, -1.0D, 1.0D);
                        var3.glMatrixMode(5888);
                        this.hYP.getGuiScene().draw(this.renderTarget);
                        this.texture.copyFromCurrentBuffer(var3, this.ddg.x, var6, this.ddg.x, var6, this.ddg.width, this.ddg.height);
                    } catch (Exception var23) {
                        var23.printStackTrace();
                        var5.markCompletelyDirty(var4.getRootPane());
                    } finally {
                        if (var2.getMaxShaderQuality() <= 1) {
                            this.renderTarget.unbind(var2);
                        }

                    }
                }

                long var27 = var5.cjJ();// var5.cjJ()=1
                if (var27 == this.fKP)//this.fKP=0
                {
                    if (this.fKQ + 3000L < atM.currentTimeMillis()) {
                        this.fKQ = atM.currentTimeMillis();
                        var5.markCompletelyDirty(var4.getRootPane());
                    }
                } else if (var27 != this.fKP) {
                    this.fKP = var27;
                    this.hYN = true;
                    try {
                        SwingUtilities.invokeAndWait(new a(var4, this.ddg, var5));
                    } catch (InterruptedException var21) {
                        var21.printStackTrace();
                    } catch (InvocationTargetException var22) {
                        var22.printStackTrace();
                    }
                }
            }

            return false;
        }
    }

    public void render() {
        if (this.hYR) {
            DrawContext var1 = this.rv.getDrawContext();
            var1.setMaxShaderQuality(this.engineGraphics.getShaderQuality());
            javax.media.opengl.GL var2 = var1.getGL();
            var2.glMatrixMode(5888);
            var2.glLoadIdentity();
            var2.glMatrixMode(5889);
            var2.glLoadIdentity();
            JGLDesktop var3 = this.engineGraphics.aee();
            var2.glViewport(0, 0, var3.getWidth(), var3.getHeight());
            var2.glDisable(2929);
            var2.glOrtho(0.0D, (double) var3.getWidth(), (double) var3.getHeight(), 0.0D, -1.0D, 1.0D);
            var2.glMatrixMode(5888);
            if (this.renderTarget != null) {
                this.texture.bind(var1);
                var2.glEnable(3042);
                var2.glBlendFunc(1, 771);
                var2.glDisableClientState(32884);
                var2.glDisableClientState(32888);
                var2.glEnable(3553);
                var2.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
                var2.glBegin(GL.GL_QUADS);//Этот метод запускает процесс рисования
                float var4 = (float) this.renderTarget.getHeight() / (float) this.renderTarget.getTexture().getSizeY();
                var2.glTexCoord2f(0.0F, var4);
                var2.glVertex2f(0.0F, 0.0F);
                var2.glTexCoord2f(0.0F, 0.0F);
                var2.glVertex2f(0.0F, (float) this.renderTarget.getHeight());
                float var5 = (float) this.renderTarget.getWidth() / (float) this.renderTarget.getTexture().getSizeX();
                var2.glTexCoord2f(var5, 0.0F);
                var2.glVertex2f((float) this.renderTarget.getWidth(), (float) this.renderTarget.getHeight());
                var2.glTexCoord2f(var5, var4);
                var2.glVertex2f((float) this.renderTarget.getWidth(), 0.0F);
                var2.glEnd();
                var2.glBindTexture(3553, 0);
            }

        }
    }

    public List aeo() {
        if (this.rv.getDrawContext().eventList == null) {
            this.rv.getDrawContext().eventList = new LinkedList();
        }

        return this.rv.getDrawContext().eventList;
    }

    public ILoaderImageInterface adz() {
        return this.Th;
    }

    /**
     * Получить загрузчик шрифтов
     *
     * @return
     */
    public pq_q ddG() {
        return this.hYO;
    }

    public JGLDesktop ddH() {
        return this.engineGraphics.aee();
    }

    public Cursor ddI() {
        return this.ddH().getCursor();
    }

    public void a(Cursor var1) {
        this.ddH().setCursor(var1);
    }

    public boolean ddJ() {
        return this.hYR;
    }

    public void jA(boolean var1) {
        this.hYR = var1;
    }

    private final class a implements Runnable {
        private JGLDesktop ddf;
        private Rectangle ddg;
        private amC ddh;

        /**
         * @param var2 all.EngineGraphics$1[panel0,3,26,1024x768,invalid,layout=java.awt.BorderLayout,rootPane=javax.swing.JRootPane[,0,0,1024x768,layout=javax.swing.JRootPane$RootLayout,alignmentX=0.0,alignmentY=0.0,border=all.BorderWrapper@135b478,flags=16777664,maximumSize=,minimumSize=,preferredSize=],rootPaneCheckingEnabled=true]
         * @param var3 java.awt.Rectangle[x=0,y=0,width=0,height=0]
         * @param var4 {}
         */
        a(JGLDesktop var2, Rectangle var3, amC var4) {
            this.ddf = var2;
            this.ddg = var3;
            this.ddh = var4;
        }

        @Override
        public void run() {
            GUIModule.this.hYQ.reset();
            GUIModule.this.hYQ.getContext().startRendering();

            try {
                Rectangle var1 = this.ddh.a(this.ddf.getRootPane(), GUIModule.this.hYQ.create());
                this.ddg.x = var1.x;
                this.ddg.y = var1.y;
                this.ddg.width = var1.width;
                this.ddg.height = var1.height;
            } finally {
                GUIModule.this.hYQ.getContext().stopRendering();
            }

            GUIModule.this.hYP = GUIModule.this.hYQ;
            GUIModule.this.hYN = false;
        }
    }
}
