package all;

import java.awt.*;

public class Tu {
    public static Tu fEC = new Tu(0.0F, (a) null);
    public static Tu fED;
    public static Tu fEE;
    public static Tu fEF;
    public static Tu fEG;
    // $FF: synthetic field
    private static int[] fEI;

    static {
        fED = new Tu(1.0E9F, a.ejd);
        fEE = new Tu(0.0F, a.ejd);
        fEF = new Tu(0.0F, a.ejc);
        fEG = new Tu(100.0F, a.ejc);
    }

    public final float value;
    public final a fEH;

    public Tu(float var1, a var2) {
        this.value = var1;
        this.fEH = var2;
    }

    // $FF: synthetic method
    static int[] bXq() {
        int[] var10000 = fEI;
        if (fEI != null) {
            return var10000;
        } else {
            int[] var0 = new int[a.values().length];

            try {
                var0[a.ejb.ordinal()] = 1;
            } catch (NoSuchFieldError var4) {
                ;
            }

            try {
                var0[a.ejc.ordinal()] = 2;
            } catch (NoSuchFieldError var3) {
                ;
            }

            try {
                var0[a.eje.ordinal()] = 4;
            } catch (NoSuchFieldError var2) {
                ;
            }

            try {
                var0[a.ejd.ordinal()] = 3;
            } catch (NoSuchFieldError var1) {
                ;
            }

            fEI = var0;
            return var0;
        }
    }

    public int jp(float var1) {
        if (this.fEH != null) {
            switch (bXq()[this.fEH.ordinal()]) {
                case 1:
                    return Math.round(this.value * var1);
                case 2:
                    return Math.round(this.value * var1 / 100.0F);
                case 3:
                    return Math.round(this.value);
                case 4:
                    int var2 = Toolkit.getDefaultToolkit().getScreenResolution();
                    return Math.round(this.value / 72.0F / (float) var2);
            }
        }

        return Math.round(this.value);
    }

    public int jq(float var1) {
        int var2 = Toolkit.getDefaultToolkit().getScreenResolution();
        if (this.fEH != null) {
            switch (bXq()[this.fEH.ordinal()]) {
                case 1:
                    return Math.round(this.value * var1);
                case 2:
                    return Math.round(this.value * var1 / 100.0F);
                case 3:
                    return Math.round(this.value * (float) var2 / 72.0F);
                case 4:
                    return Math.round(this.value);
            }
        }

        return Math.round(this.value);
    }

    public boolean equals(Object var1) {
        if (var1 == this) {
            return true;
        } else if (var1 instanceof Tu) {
            Tu var2 = (Tu) var1;
            return var2.value == this.value && var2.fEH == this.fEH;
        } else {
            return super.equals(var1);
        }
    }

    public int hashCode() {
        return Float.floatToIntBits(this.value) * 31 ^ (this.fEH != null ? this.fEH.ordinal() : -1);
    }

    public String toString() {
        return this.value + " " + this.fEH;
    }

    public static enum a {
        ejb,
        ejc,
        ejd,
        eje;
    }
}
