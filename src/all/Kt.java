package all;

public class Kt {
    private static final LogPrinter logger = LogPrinter.K(Kt.class);
    private static final ThreadLocal dpR = new ThreadLocal() {
        protected Kt doo() {
            Kt var1 = new Kt((Kt) null);
            var1.thread = Thread.currentThread();
            return var1;
        }
    };
    private final OY dpT;
    private final VZ dpU;
    private final Cg dpV;
    private final aqJ dpW;
    private final ayj dpX;
    private final yb dpY;
    private final aT dpZ;
    private final Co dqa;
    protected Thread thread;
    private int dpS;

    private Kt() {
        this.dpT = new OY();
        this.dpU = new VZ();
        this.dpV = new Cg();
        this.dpW = new aqJ();
        this.dpX = new ayj();
        this.dpY = new yb();
        this.dpZ = new aT();
        this.dqa = new Co(Float.TYPE);
    }

    // $FF: synthetic method
    Kt(Kt var1) {
        this();
    }

    public static Kt bcE() {
        return (Kt) dpR.get();
    }

    private Kt bcC() {
        Thread var1 = Thread.currentThread();
        if (this.thread == var1) {
            return this;
        } else {
            if (this.dpS < 10) {
                if (logger.isDebugEnabled()) {
                    logger.debug("Using wrong stack for this thread (" + var1 + ") should be (" + this.thread + ") ", new Throwable());
                }

                ++this.dpS;
            }

            return bcE();
        }
    }

    public Kt bcD() {
        Thread var1 = Thread.currentThread();
        return this.thread == var1 ? this : bcE();
    }

    public void bcF() {
        this.bcH().push();
        this.bcJ().push();
        this.bcK().push();
        this.bcL().push();
    }

    public void bcG() {
        this.bcH().pop();
        this.bcJ().pop();
        this.bcK().pop();
        this.bcL().pop();
    }

    public OY bcH() {
        return this.bcC().dpT;
    }

    public VZ bcI() {
        return this.bcC().dpU;
    }

    public Cg bcJ() {
        return this.bcC().dpV;
    }

    public aqJ bcK() {
        return this.bcC().dpW;
    }

    public ayj bcL() {
        return this.bcC().dpX;
    }

    public yb bcM() {
        return this.bcC().dpY;
    }

    public aT bcN() {
        return this.bcC().dpZ;
    }

    public Co bcO() {
        return this.bcC().dqa;
    }
}
