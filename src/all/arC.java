package all;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class arC extends ks_q {
    protected static final int gsF = 32;
    protected static final int gsG = 255;
    protected static final int gsH = 127;
    protected static final int gsI = 23;
    private static final int gbL = -126;
    private final int evX;
    private final int evY;
    private final int evZ;
    private final int ewa;
    private final int ewb;
    private final int gsJ;
    private final int gsK;
    private final int gsL;
    private final int ewf;
    private final float minValue;
    private final float maxValue;
    private final float gsM;
    private final boolean signed;
    private final boolean ewi;
    private final int gsN;
    private final int gsO;
    private final int gsP;
    private final int ewm;
    protected String name;

    public arC(boolean var1, int var2, int var3, boolean var4, int var5) {
        this((String) null, var1, var2, var3, var4, var5);
    }

    public arC(String var1, boolean var2, int var3, int var4, boolean var5, int var6) {
        this.name = var1 != null ? var1 : this.getClass().getSimpleName();
        this.signed = var2;
        this.evX = var3;
        this.evY = var4;
        this.ewi = var5;
        this.gsJ = (1 << var4) - 1;
        this.gsK = (1 << var3) - 1;
        this.ewb = var6;
        this.ewa = -(this.gsK - var6 - (var5 ? 2 : 1));
        this.ewf = -127 + -this.ewa + 1;
        this.evZ = (var2 ? 1 : 0) + var3 + var4;
        this.gsP = var4 < 23 ? 1 << 23 - var4 - 1 : 0;
        this.gsN = 23 - var4;
        this.gsO = 32 - this.evZ;
        this.gsL = 1 << this.evZ - 1;
        this.ewm = (this.evZ >> 3) + ((this.evZ & 7) != 0 ? 1 : 0);
        this.minValue = Float.intBitsToFloat(this.ewa + 127 << 23);
        this.maxValue = Float.intBitsToFloat(var6 + 127 << 23 | this.gsJ << 23 - var4);
        this.gsM = 23 <= var4 ? 0.0F : Float.intBitsToFloat(1065353216 | (1 << 23 - var4) - 1) - 1.0F;
    }

    public static int tR(int var0) {
        int var1 = 0;

        for (int var2 = 16; var2 > 0; var2 >>= 1) {
            if (var0 >>> var2 != 0) {
                var0 >>>= var2;
                var1 |= var2;
            }
        }

        return var1 + var0;
    }

    public static int rb(int var0) {
        int var1 = 0;
        if (var0 >>> 16 != 0) {
            var0 >>>= 16;
            var1 += 16;
        }

        if (var0 >>> 8 != 0) {
            var0 >>>= 8;
            var1 += 8;
        }

        if (var0 >>> 4 != 0) {
            var0 >>>= 4;
            var1 += 4;
        }

        if (var0 >>> 2 != 0) {
            var0 >>>= 2;
            var1 += 2;
        }

        if (var0 >>> 1 != 0) {
            var0 >>>= 1;
            ++var1;
        }

        return var1 + var0;
    }

    public int Kc() {
        return this.evZ;
    }

    public int Ka() {
        return this.ewb;
    }

    public int Kb() {
        return this.ewa;
    }

    public int Ke() {
        return this.evX;
    }

    public int Kd() {
        return this.evY;
    }

    public float Kf() {
        return this.maxValue;
    }

    public float Kg() {
        return this.minValue;
    }

    public int aT(float var1) {
        int var2 = Float.floatToIntBits(var1);
        int var3 = var2 & 8388607;
        int var4 = var2 >> 23 & 255;
        if (var4 != 255) {
            if ((var3 & this.gsP) != 0) {
                var2 += this.gsP;
                var4 = var2 >> 23 & 255;
                var3 = var2 & 8388607;
            }

            if (var4 != 0) {
                var4 += this.ewf;
                if (var4 <= 0) {
                    if (var4 > -this.evY) {
                        var3 = (var3 | 8388608) >>> 1 - var4;
                    } else {
                        var3 = 0;
                    }

                    var4 = 0;
                } else if (var4 >= this.gsK && (var4 != this.gsK || this.ewi)) {
                    var4 = this.ewb + -this.ewa + 1;
                    var3 = 8388607;
                }
            } else if (this.ewa > -126) {
                if (this.ewa - -126 < this.evY) {
                    var3 >>= this.ewa - -126;
                } else {
                    var3 = 0;
                }
            }
        } else if (!this.ewi) {
            var4 += this.ewf;
            if (var4 <= 0 && var4 > -126) {
                var3 = (var3 | 8388608) >> 1 - var4;
                var4 = 0;
            }
        }

        int var5;
        if (this.signed) {
            var5 = var2 >> this.gsO & this.gsL | (var4 & this.gsK) << this.evY | var3 >> this.gsN;
            return var5;
        } else {
            var5 = (var4 & this.gsK) << this.evY | var3 >> this.gsN;
            return var5;
        }
    }

    public float cF(int var1) {
        int var2 = var1 >> this.evY & this.gsK;
        int var3 = var1 & this.gsJ;
        int var4;
        if (var2 == this.gsK) {
            if (this.ewi) {
                var2 = 255;
            } else {
                var2 -= this.ewf;
            }
        } else if (var2 != 0) {
            var2 -= this.ewf;
        } else if (this.ewa > -126 && var3 != 0) {
            var4 = this.evY - rb(var3) + 1;
            int var5 = 126 + this.ewa;
            if (var5 > var4) {
                var2 = this.ewa - var4 + 127;
                var3 = var3 << var4 & this.gsJ;
            } else {
                var3 = var3 << var5 & this.gsJ;
            }
        }

        if (this.signed) {
            var4 = var1 << this.gsO & Integer.MIN_VALUE | var2 << 23 | var3 << this.gsN;
            return Float.intBitsToFloat(var4);
        } else {
            var4 = var2 << 23 | var3 << this.gsN;
            return Float.intBitsToFloat(var4);
        }
    }

    public void a(ObjectOutput var1, float var2) throws IOException {
        switch (this.ewm) {
            case 1:
                var1.writeByte(this.aT(var2));
                break;
            case 2:
                var1.writeShort(this.aT(var2));
                break;
            case 3:
                var1.writeByte(this.aT(var2) >> 16);
                var1.writeShort(this.aT(var2));
                break;
            case 4:
                var1.writeInt(this.aT(var2));
        }

        throw new IllegalArgumentException();
    }

    public float d(ObjectInput var1) throws IOException {
        switch (this.ewm) {
            case 1:
                return this.cF(var1.readUnsignedByte() & 255);
            case 2:
                return this.cF(var1.readUnsignedShort());
            case 3:
                int var2 = var1.readUnsignedByte();
                int var3 = var1.readUnsignedShort();
                return this.cF(var2 << 16 | var3);
            case 4:
                return this.cF(var1.readInt());
            default:
                throw new IllegalArgumentException();
        }
    }

    public float Kh() {
        return this.gsM;
    }

    public boolean Ki() {
        return this.signed;
    }

    public String getName() {
        return this.name;
    }

    public boolean Kj() {
        return this.ewi;
    }
}
