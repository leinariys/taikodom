package all;

import java.io.Serializable;

public class fE implements akt_q, Serializable {
    private String LN;

    public fE(String var1) {
        this.LN = var1;
    }

    public short getConditionType() {
        return 5;
    }

    public String getNamespaceURI() {
        return null;
    }

    public String getLocalName() {
        return null;
    }

    public boolean getSpecified() {
        return true;
    }

    public String getValue() {
        return this.LN;
    }

    public String toString() {
        return "#" + this.getValue();
    }
}
