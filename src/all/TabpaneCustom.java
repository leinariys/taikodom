package all;

import javax.swing.*;
import java.awt.*;
import java.util.Iterator;

public class TabpaneCustom extends BaseItemFactory {
    /**
     * Создание базового Component интерфейса
     * Вызывается из class BaseItem
     *
     * @param var1
     * @param var2
     * @param var3
     * @return
     */
    @Override
    protected JComponent CreatComponentCustom(MapKeyValue var1, Container var2, XmlNode var3) {
        return new TabpaneCustomWrapper();
    }

    protected void a(MapKeyValue var1, TabpaneCustomWrapper var2, XmlNode var3) {
        Iterator var5 = var3.getChildrenTag().iterator();

        while (var5.hasNext()) {
            XmlNode var4 = (XmlNode) var5.next();
            if ("tab".equals(var4.getTegName())) {
                String var6 = var4.getAttribute("title");
                var2.addTab(var6 == null ? "" : var6, (Component) (new a((a) null)).CreatAllComponentCustom(var1, var2, var4));
            }
        }

    }

    protected void b(MapKeyValue var1, TabpaneCustomWrapper var2, XmlNode var3) {
        super.createLayout(var1, var2, var3);
        String var4 = var3.getAttribute("tab-placement");
        if (var4 != null) {
            if ("bottom".equals(var4)) {
                var2.setTabPlacement(3);
            } else if ("left".equals(var4)) {
                var2.setTabPlacement(2);
            } else if ("right".equals(var4)) {
                var2.setTabPlacement(4);
            } else if ("top".equals(var4)) {
                var2.setTabPlacement(1);
            }

        }
    }

    private class a extends BaseItemFactory {
        private a() {
        }

        // $FF: synthetic method
        a(a var2) {
            this();
        }

        /**
         * Создание базового Component интерфейса
         * Вызывается из class BaseItem
         *
         * @param var1
         * @param var2
         * @param var3
         * @return
         */
        @Override
        protected JComponent CreatComponentCustom(MapKeyValue var1, Container var2, XmlNode var3) {
            return new TabpaneCustomWrapper.a();
        }

    }

}
