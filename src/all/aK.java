package all;

import java.io.*;
import java.lang.reflect.Array;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class aK {
    public static String t(String var0) {
        return var0.length() > 0 ? Character.toUpperCase(var0.charAt(0)) + var0.substring(1) : var0;
    }

    public static CharSequence a(CharSequence var0) {
        if (var0.length() > 0) {
            char var1 = var0.charAt(0);
            char var2 = Character.toUpperCase(var1);
            if (var2 != var1) {
                return "" + var2 + var0.subSequence(1, var0.length());
            }
        }

        return var0;
    }

    public static CharSequence b(CharSequence var0) {
        if (var0.length() > 0) {
            char var1 = var0.charAt(0);
            char var2 = Character.toLowerCase(var1);
            if (var2 != var1) {
                return "" + var2 + var0.subSequence(1, var0.length());
            }
        }

        return var0;
    }

    public static String u(String var0) {
        return var0.length() > 0 ? Character.toLowerCase(var0.charAt(0)) + var0.substring(1) : var0;
    }

    public static String v(String var0) {
        StringBuffer var1 = new StringBuffer();

        for (int var2 = 0; var2 < var0.length(); ++var2) {
            char var3 = var0.charAt(var2);
            if (Character.isUpperCase(var3)) {
                var1.append('_').append(var3);
            } else {
                var1.append(Character.toUpperCase(var3));
            }
        }

        return var1.toString();
    }

    public static String w(String var0) {
        try {
            MessageDigest var1 = MessageDigest.getInstance("md5");
            var1.update(var0.getBytes("utf8"));
            byte[] var2 = var1.digest();
            return toHexString(var2);
        } catch (NoSuchAlgorithmException var3) {
            var3.printStackTrace();
        } catch (UnsupportedEncodingException var4) {
            var4.printStackTrace();
        }

        return String.valueOf(var0.hashCode());
    }

    public static String a(byte[] var0) {
        MessageDigest var1;
        try {
            var1 = MessageDigest.getInstance("md5");
        } catch (NoSuchAlgorithmException var3) {
            throw new RuntimeException(var3);
        }

        var1.update(var0);
        byte[] var2 = var1.digest();
        return toHexString(var2);
    }

    public static byte[] b(byte[] var0) {
        MessageDigest var1;
        try {
            var1 = MessageDigest.getInstance("md5");
        } catch (NoSuchAlgorithmException var3) {
            throw new RuntimeException(var3);
        }

        var1.update(var0);
        return var1.digest();
    }

    public static String x(String var0) {
        try {
            MessageDigest var1 = MessageDigest.getInstance("sha-1");
            var1.update(var0.getBytes("utf8"));
            byte[] var2 = var1.digest();
            return toHexString(var2);
        } catch (NoSuchAlgorithmException var3) {
            var3.printStackTrace();
        } catch (UnsupportedEncodingException var4) {
            var4.printStackTrace();
        }

        return String.valueOf(var0.hashCode());
    }

    public static String toHexString(byte[] var0) {
        return a(var0, (String) null);
    }

    public static String a(byte[] var0, String var1) {
        return a(var0, var1, 0);
    }

    public static String a(byte[] var0, String var1, int var2) {
        StringBuilder var3 = new StringBuilder();
        if (var0 == null) {
            return "null";
        } else {
            int var4 = 0;
            byte[] var8 = var0;
            int var7 = var0.length;

            for (int var6 = 0; var6 < var7; ++var6) {
                byte var5 = var8[var6];
                if (var1 != null && var3.length() > 0) {
                    var3.append(var1);
                }

                if ((255 & var5) < 16) {
                    var3.append("0").append(Integer.toHexString(255 & var5));
                } else {
                    var3.append(Integer.toHexString(255 & var5));
                }

                ++var4;
                if (var2 > 0 && var4 >= var2) {
                    var3.append("\n");
                    var4 = 0;
                }
            }

            return new String(var3);
        }
    }

    public static String a(byte[] var0, int var1, int var2, String var3, int var4) {
        StringBuilder var5 = new StringBuilder();
        if (var0 == null) {
            return "null";
        } else {
            int var6 = 0;

            while (true) {
                --var2;
                if (var2 < 0) {
                    return new String(var5);
                }

                byte var7 = var0[var1];
                if (var3 != null && var5.length() > 0) {
                    var5.append(var3);
                }

                if ((255 & var7) < 16) {
                    var5.append("0").append(Integer.toHexString(255 & var7));
                } else {
                    var5.append(Integer.toHexString(255 & var7));
                }

                ++var6;
                if (var4 > 0 && var6 >= var4) {
                    var5.append("\n");
                    var6 = 0;
                }

                ++var1;
            }
        }
    }

    public static String b(byte[] var0, int var1, int var2, String var3, int var4) {
        return a("", var0, var1, var2, var3, var4);
    }

    public static String a(String var0, byte[] var1, int var2, int var3, String var4, int var5) {
        StringBuilder var6 = new StringBuilder();
        if (var1 == null) {
            return "null";
        } else {
            int var7 = var2;

            for (int var8 = var2 + var3; var7 < var8; var7 += var5) {
                var6.append(var0);
                int var9 = var7 + var5;
                int var10 = Math.min(var8, var9);
                var6.append(String.format("%04d - ", var7 - var2));

                int var11;
                byte var12;
                for (var11 = var7; var11 < var10; ++var11) {
                    var12 = var1[var11];
                    if ((255 & var12) < 16) {
                        var6.append("0").append(Integer.toHexString(255 & var12));
                    } else {
                        var6.append(Integer.toHexString(255 & var12));
                    }

                    var6.append(var4);
                }

                while (var11 < var9) {
                    var6.append("..");
                    var6.append(var4);
                    ++var11;
                }

                var6.append(" [");

                for (var11 = var7; var11 < var10; ++var11) {
                    var12 = var1[var11];
                    var6.append(var12 > 31 ? (char) var12 : (var12 == 0 ? "_" : "?"));
                }

                var6.append("]\n");
            }

            return var6.toString();
        }
    }

    public static String c(byte[] var0) {
        StringBuffer var1 = new StringBuffer();
        byte[] var5 = var0;
        int var4 = var0.length;

        for (int var3 = 0; var3 < var4; ++var3) {
            byte var2 = var5[var3];
            int var6 = 8;

            while (true) {
                --var6;
                if (var6 < 0) {
                    break;
                }

                var1.append((char) (1 == (1 & var2 >> var6) ? '1' : '0'));
            }
        }

        return var1.toString();
    }

    public static ByteBuffer a(File var0) {
        try {
            RandomAccessFile var1 = new RandomAccessFile(var0, "r");

            try {
                ByteBuffer var2 = ByteBuffer.allocateDirect((int) var1.length());
                int var3 = var1.getChannel().read(var2);
                if ((long) var3 >= var1.length()) {
                    var2.flip();
                    ByteBuffer var5 = var2;
                    return var5;
                }
            } finally {
                var1.close();
            }

            return null;
        } catch (FileNotFoundException var10) {
            var10.printStackTrace();
        } catch (IOException var11) {
            var11.printStackTrace();
        }

        return null;
    }

    public static String b(byte[] var0, String var1) {
        try {
            return new String(var0, var1);
        } catch (UnsupportedEncodingException var2) {
            return null;
        }
    }

    public static String a(byte[] var0, int var1, int var2, String var3) {
        try {
            return new String(var0, var1, var2, var3);
        } catch (UnsupportedEncodingException var4) {
            return null;
        }
    }

    public static String a(Class var0) {
        int var1 = var0.getName().lastIndexOf(46);
        return var1 == -1 ? var0.getName() : var0.getName().substring(var1 + 1);
    }

    public static String f(Object var0) {
        try {
            return String.valueOf(var0);
        } catch (Exception var3) {
            Exception var1 = var3;

            try {
                return "Error at toString " + var1.toString();
            } catch (Exception var2) {
                return "Seriour error at toString " + var3.getClass();
            }
        }
    }

    public static byte[] copyOf(byte[] var0, int var1) {
        byte[] var2 = new byte[var1];
        System.arraycopy(var0, 0, var2, 0, Math.min(var0.length, var1));
        return var2;
    }

    public static byte[] copyOfRange(byte[] var0, int var1, int var2) {
        int var3 = var2 - var1;
        if (var3 < 0) {
            throw new IllegalArgumentException(var1 + " > " + var2);
        } else {
            byte[] var4 = new byte[var3];
            System.arraycopy(var0, var1, var4, 0, Math.min(var0.length - var1, var3));
            return var4;
        }
    }

    public static Object[] copyOf(Object[] var0, int var1) {
        return copyOf(var0, var1, var0.getClass());
    }

    public static Object[] copyOf(Object[] var0, int var1, Class var2) {
        Object[] var3 = var2 == Object[].class ? new Object[var1] : (Object[]) Array.newInstance(var2.getComponentType(), var1);
        System.arraycopy(var0, 0, var3, 0, Math.min(var0.length, var1));
        return var3;
    }

    public static long[] copyOf(long[] var0, int var1) {
        long[] var2 = new long[var1];
        System.arraycopy(var0, 0, var2, 0, Math.min(var0.length, var1));
        return var2;
    }
}
