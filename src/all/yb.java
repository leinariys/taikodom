package all;

import javax.vecmath.Vector4f;

public class yb extends aoW {
    public Vector4f k(float var1, float var2, float var3, float var4) {
        Vector4f var5 = (Vector4f) this.get();
        var5.set(var1, var2, var3, var4);
        return var5;
    }

    public Vector4f a(Vector4f var1) {
        Vector4f var2 = (Vector4f) this.get();
        var2.set(var1);
        return var2;
    }

    protected Vector4f apx() {
        return new Vector4f();
    }

    protected void a(Vector4f var1, Vector4f var2) {
        var1.set(var2);
    }

    @Override
    protected Object create() {
        return null;
    }

    @Override
    protected void copy(Object var1, Object var2) {

    }
}
