package all;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;

public class StreamRootPathRender extends URLStreamHandler {
    /**
     * Каталог с кодом отрисовки графики
     */
    private final ain rootPathRender;

    /**
     * Задать Каталог с кодом отрисовки графики
     *
     * @param var1
     */
    public StreamRootPathRender(ain var1) {
        this.rootPathRender = var1;
    }

    protected URLConnection openConnection(URL var1) throws IOException {
        if ("res".equalsIgnoreCase(var1.getProtocol())) {
            String var2 = var1.toString();
            ain var3 = this.rootPathRender.concat(var2.substring("res://".length()));
            return var3.getFile().toURI().toURL().openConnection();
        } else {
            throw new IOException("Unknow protocol: " + var1.getProtocol() + " at " + var1);
        }
    }
}
