package all;

import org.xmlpull.mxp1.MXParser;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Stack;

/**
 * Загрузчик XML документа
 */
public class LoaderFileXML {
    int[] positionText = new int[2];

    /**
     * @param var1 Поток для чтения
     * @param var2 кодировка utf-8
     * @return
     * @throws IOException
     */
    public XmlNode loadFileXml(InputStream var1, String var2) throws IOException {
        try {
            MXParser var3 = new MXParser();
            var3.setInput(new InputStreamReader(var1, var2));
            return this.loadFileXml(var3);
        } catch (XmlPullParserException var4) {
            throw new RuntimeException(var4);
        }
    }

    /**
     * Пробегаемся по документу
     *
     * @param var1
     * @return
     * @throws IOException
     * @throws XmlPullParserException
     */
    private XmlNode loadFileXml(XmlPullParser var1) throws IOException, XmlPullParserException {
        XmlNode var2 = new XmlNode();//Создаём Пустой Тег
        Stack var3 = new Stack(); //Флагбток, последовательность иерархии
        var3.push(var2);
        boolean var4 = false;

        while (!var4) {
            int var5 = var1.next();
            switch (var5) {
                case XmlPullParser.START_DOCUMENT:// начало документа
                default:
                    break;
                case XmlPullParser.END_DOCUMENT:// конец документа
                    var4 = true;
                    break;
                case XmlPullParser.START_TAG:// начало тэга <company>
                    XmlNode var6 = this.loadFileXml((XmlNode) var3.lastElement(), var1);
                    var3.add(var6);
                    break;
                case XmlPullParser.END_TAG:// конец тэга </company>
                    var3.pop();//Вытащить объект из стека
                    break;
                case XmlPullParser.TEXT:// содержимое элемента <company>TEXT</company>
                    this.b((XmlNode) var3.lastElement(), var1);
            }
        }
        return (XmlNode) var2.getChildrenTag().get(0);//Достаём из пустого тега
    }

    /**
     * Создать потомка
     *
     * @param var1 Родительский элемент
     * @param var2 Тег с атрибутами
     * @return
     */
    private XmlNode loadFileXml(XmlNode var1, XmlPullParser var2) {
        XmlNode var3 = var1.addChildrenTag(var2.getName()); //Добавить потомка с заданым тегом
        //Заполняем атрибуты
        for (int var4 = 0; var4 < var2.getAttributeCount(); ++var4) {
            String var5 = var2.getAttributeName(var4);
            String var6 = var2.getAttributeValue(var4);
            var3.addAttribute(var5, var6);
        }
        return var3;
    }

    /**
     * Задать текстовое содержимое тега
     *
     * @param var1 Родительский элемент
     * @param var2 Содержимое
     */
    private void b(XmlNode var1, XmlPullParser var2) {
        //Позиция текста
        char[] var3 = var2.getTextCharacters(this.positionText);
        int var4 = this.positionText[0];
        int var5 = this.positionText[1];
        var1.my(new String(var3, var4, var5));
    }
}
