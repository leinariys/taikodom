package all;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeCellRenderer;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

public class TE implements TreeCellRenderer {
    private Map gqW = new HashMap();

    public Component getTreeCellRendererComponent(JTree var1, Object var2, boolean var3, boolean var4, boolean var5, int var6, boolean var7) {
        a var8 = (a) this.gqW.get(var2);
        if (var8 == null) {
            var8 = new a();
            Object var9 = var2;
            if (var2 instanceof DefaultMutableTreeNode) {
                var9 = ((DefaultMutableTreeNode) var2).getUserObject();
            }

            if (var9 instanceof Component) {
                var8.add((Component) var9);
            } else {
                var8.add(new JLabel(var2.toString()));
            }

            this.gqW.put(var2, var8);
        }

        return var8;
    }

    public static class a extends JPanel implements IComponentCustomWrapper {
        private static final long serialVersionUID = 1L;

        public String getElementName() {
            return "treecell";
        }
    }
}
