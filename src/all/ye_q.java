package all;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ye_q {
    private final BoundingBox dVu;
    BoundingBox[] dVs;
    ye_q[] dVt;
    List bV;
    private boolean leaf;
    private int dVv;
    private int dVw;
    private int level;

    public ye_q(BoundingBox var1, int var2) {
        this.dVs = new BoundingBox[8];
        this.dVt = new ye_q[8];
        this.bV = new ArrayList();
        this.leaf = true;
        this.dVv = 15;
        this.dVw = 2;
        this.level = var2;
        this.dVu = var1;
        Vec3f var3 = var1.dqQ();
        Vec3f var4 = var1.dqP();
        Vec3f var5 = var1.bny();
        this.dVs[0] = new BoundingBox(var4, var5);
        this.dVs[1] = new BoundingBox(var5, var3);
        this.dVs[2] = new BoundingBox(new Vec3f(var4.x, var4.y, var5.z), new Vec3f(var5.x, var5.y, var3.z));
        this.dVs[3] = new BoundingBox(new Vec3f(var4.x, var5.y, var4.z), new Vec3f(var5.x, var3.y, var5.z));
        this.dVs[4] = new BoundingBox(new Vec3f(var4.x, var5.y, var5.z), new Vec3f(var5.x, var3.y, var3.z));
        this.dVs[5] = new BoundingBox(new Vec3f(var5.x, var4.y, var4.z), new Vec3f(var3.x, var5.y, var5.z));
        this.dVs[6] = new BoundingBox(new Vec3f(var5.x, var4.y, var5.z), new Vec3f(var3.x, var5.y, var3.z));
        this.dVs[7] = new BoundingBox(new Vec3f(var5.x, var5.y, var4.z), new Vec3f(var3.x, var3.y, var5.z));
    }

    public ye_q(BoundingBox var1) {
        this(var1, 0);
    }

    public void b(BoundingBox var1, Object var2) {
        if (this.dVu.a(var1)) {
            if (!this.leaf || this.bV.size() >= this.dVv && this.level != this.dVw) {
                this.leaf = false;

                for (int var3 = 0; var3 < 8; ++var3) {
                    if (this.dVs[var3].a(var1)) {
                        if (this.dVt[var3] == null) {
                            this.dVt[var3] = new ye_q(this.dVs[var3]);
                        }

                        this.dVt[var3].b(var1, var2);
                    }

                    Iterator var5 = this.bV.iterator();

                    while (var5.hasNext()) {
                        a var4 = (a) var5.next();
                        if (this.dVs[var3].a(var4.bJB)) {
                            if (this.dVt[var3] == null) {
                                this.dVt[var3] = new ye_q(this.dVs[var3], this.level + 1);
                            }

                            this.dVt[var3].b(var4.bJB, var4.obj);
                        }
                    }
                }

                if (this.bV.size() != this.dVv && this.bV.size() != 0) {
                    throw new IllegalStateException("Should never clear if its different from 0 or " + this.dVv);
                } else {
                    this.bV.clear();
                }
            } else {
                this.bV.add(new a(var1, var2));
            }
        }
    }

    public boolean b(BoundingBox var1) {
        if (!this.dVu.a(var1)) {
            return false;
        } else {
            if (this.leaf && this.bV.size() > 0) {
                Iterator var3 = this.bV.iterator();

                while (var3.hasNext()) {
                    a var2 = (a) var3.next();
                    if (var2.bJB.a(var1)) {
                        return true;
                    }
                }
            }

            for (int var4 = 0; var4 < 8; ++var4) {
                if (this.dVt[var4] != null && this.dVt[var4].b(var1)) {
                    return true;
                }
            }

            return false;
        }
    }

    public void a(nA var1) {
        if (var1.a(this.dVu)) {
            int var2;
            if (this.leaf) {
                var2 = this.bV.size();

                while (true) {
                    --var2;
                    if (var2 < 0) {
                        break;
                    }

                    a var3 = (a) this.bV.get(var2);
                    var1.a(var3.bJB, var3.obj);
                }
            } else {
                for (var2 = 0; var2 < 8; ++var2) {
                    if (this.dVt[var2] != null) {
                        this.dVt[var2].a(var1);
                    }
                }
            }
        }

    }

    public boolean b(nA var1) {
        if (!var1.a(this.dVu)) {
            return false;
        } else {
            if (this.bV.size() > 0) {
                Iterator var3 = this.bV.iterator();

                while (var3.hasNext()) {
                    a var2 = (a) var3.next();
                    if (var1.a(var2.bJB, var2.obj)) {
                        return true;
                    }
                }
            }

            for (int var4 = 0; var4 < 8; ++var4) {
                if (this.dVt[var4] != null && this.dVt[var4].b(var1)) {
                    return true;
                }
            }

            return false;
        }
    }

    public void t(List var1) {
        Iterator var3 = this.bV.iterator();

        while (var3.hasNext()) {
            a var2 = (a) var3.next();
            var1.add(var2.obj);
        }

        for (int var4 = 0; var4 < 8; ++var4) {
            if (this.dVt[var4] != null) {
                this.dVt[var4].t(var1);
            }
        }

    }

    static class a {
        private final Object obj;
        private final BoundingBox bJB;

        public a(BoundingBox var1, Object var2) {
            this.bJB = var1;
            this.obj = var2;
        }
    }
}
