package all;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class apO implements Externalizable {

    private long id;
    private transient int hashCode;

    public apO() {
    }

    public apO(long var1) {
        this.id = var1;
        this.hashCode = this.cqp();
    }

    public long cqo() {
        return this.id;
    }

    public int hashCode() {
        return this.hashCode;
    }

    public boolean equals(Object var1) {
        if (this == var1) {
            return true;
        } else if (var1 == null) {
            return false;
        } else if (this.getClass() != var1.getClass()) {
            return false;
        } else {
            return this.id == ((apO) var1).id;
        }
    }

    public String toString() {
        return "OID[id=" + this.id + "]";
    }

    public void readExternal(ObjectInput var1) throws IOException {
        this.id = var1.readLong();
        this.hashCode = this.cqp();
    }

    private int cqp() {
        return 31 + (int) (this.id ^ this.id >>> 32);
    }

    public void writeExternal(ObjectOutput var1) throws IOException {
        var1.writeLong(this.id);
    }
}
