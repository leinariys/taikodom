package all;

import java.awt.*;

/**
 * abstract class BaseItem
 */
public interface JC {
    /**
     * Создание графического Component
     *
     * @param var1 ключ - значение,  класса перевода и путь
     * @param var2 Рут панель Пример avax.swing.JRootPane[,0,0,1024x780,invalid,layout=javax.swing.JRootPane$RootLayout,alignmentX=0.0,alignmentY=0.0,border=all.BorderWrapper@f34226,flags=16777664,maximumSize=,minimumSize=,preferredSize=]
     * @param var3 Пример <window class="empty" transparent="true" alwaysOnTop="true" focusable="false" layout="GridLayout" css="taikodomversion.css">...</window>
     * @return
     */
    Component d(MapKeyValue var1, Container var2, XmlNode var3);
}
