package all;

import com.hoplon.geometry.Vec3f;

import java.io.Serializable;

public abstract class adJ implements Serializable {
    // $FF: synthetic field
    private static int[] fnk;
    protected final Vec3f scaling = new Vec3f(1.0F, 1.0F, 1.0F);
    private aUv BD = new aUv();

    // $FF: synthetic method
    static int[] bUd() {
        int[] var10000 = fnk;
        if (fnk != null) {
            return var10000;
        } else {
            int[] var0 = new int[aop.values().length];

            try {
                var0[aop.ghC.ordinal()] = 2;
            } catch (NoSuchFieldError var5) {
                System.out.println("Exception-" + var5);
            }

            try {
                var0[aop.ghF.ordinal()] = 5;
            } catch (NoSuchFieldError var4) {
                System.out.println("Exception-" + var4);
            }

            try {
                var0[aop.ghB.ordinal()] = 1;
            } catch (NoSuchFieldError var3) {
                System.out.println("Exception-" + var3);
            }

            try {
                var0[aop.ghD.ordinal()] = 3;
            } catch (NoSuchFieldError var2) {
                System.out.println("Exception-" + var2);
            }

            try {
                var0[aop.ghE.ordinal()] = 4;
            } catch (NoSuchFieldError var1) {
                System.out.println("Exception-" + var1);
            }

            fnk = var0;
            return var0;
        }
    }

    public void a(SP var1, Vec3f var2, Vec3f var3) {
        int var4 = 0;
        int var6 = this.bUc();
        Vec3f[] var8 = new Vec3f[]{new Vec3f(), new Vec3f(), new Vec3f()};
        Vec3f var10 = new Vec3f(this.getScaling());

        for (int var5 = 0; var5 < var6; ++var5) {
            this.b(this.BD, var5);
            var4 += this.BD.iXn * 3;
            int var7;
            int var9;
            int var11;
            label34:
            switch (bUd()[this.BD.iXo.ordinal()]) {
                case 3:
                    var7 = 0;

                    while (true) {
                        if (var7 >= this.BD.iXn) {
                            break label34;
                        }

                        var11 = var7 * this.BD.iXm;
                        var9 = this.BD.iXl.getInt(var11 + 0) * this.BD.stride;
                        var8[0].set(this.BD.iXi.getFloat(var9 + 0) * var10.x, this.BD.iXi.getFloat(var9 + 4) * var10.y, this.BD.iXi.getFloat(var9 + 8) * var10.z);
                        var9 = this.BD.iXl.getInt(var11 + 4) * this.BD.stride;
                        var8[1].set(this.BD.iXi.getFloat(var9 + 0) * var10.x, this.BD.iXi.getFloat(var9 + 4) * var10.y, this.BD.iXi.getFloat(var9 + 8) * var10.z);
                        var9 = this.BD.iXl.getInt(var11 + 8) * this.BD.stride;
                        var8[2].set(this.BD.iXi.getFloat(var9 + 0) * var10.x, this.BD.iXi.getFloat(var9 + 4) * var10.y, this.BD.iXi.getFloat(var9 + 8) * var10.z);
                        var1.b(var8, var5, var7);
                        ++var7;
                    }
                case 4:
                    var7 = 0;

                    while (true) {
                        if (var7 >= this.BD.iXn) {
                            break label34;
                        }

                        var11 = var7 * this.BD.iXm;
                        var9 = (this.BD.iXl.getShort(var11 + 0) & '\uffff') * this.BD.stride;
                        var8[0].set(this.BD.iXi.getFloat(var9 + 0) * var10.x, this.BD.iXi.getFloat(var9 + 4) * var10.y, this.BD.iXi.getFloat(var9 + 8) * var10.z);
                        var9 = (this.BD.iXl.getShort(var11 + 2) & '\uffff') * this.BD.stride;
                        var8[1].set(this.BD.iXi.getFloat(var9 + 0) * var10.x, this.BD.iXi.getFloat(var9 + 4) * var10.y, this.BD.iXi.getFloat(var9 + 8) * var10.z);
                        var9 = (this.BD.iXl.getShort(var11 + 4) & '\uffff') * this.BD.stride;
                        var8[2].set(this.BD.iXi.getFloat(var9 + 0) * var10.x, this.BD.iXi.getFloat(var9 + 4) * var10.y, this.BD.iXi.getFloat(var9 + 8) * var10.z);
                        var1.b(var8, var5, var7);
                        ++var7;
                    }
                default:
                    assert this.BD.iXo == aop.ghD || this.BD.iXo == aop.ghE;
            }

            this.qh(var5);
        }

        this.BD.dAi();
    }

    public void x(Vec3f var1, Vec3f var2) {
        a var3 = new a((a) null);
        var1.set(-1.0E30F, -1.0E30F, -1.0E30F);
        var2.set(1.0E30F, 1.0E30F, 1.0E30F);
        this.a(var3, var1, var2);
        var1.set(var3.cam);
        var2.set(var3.can);
    }

    public abstract void a(aUv var1, int var2);

    public abstract void b(aUv var1, int var2);

    public abstract void qg(int var1);

    public abstract void qh(int var1);

    public abstract int bUc();

    public abstract void qi(int var1);

    public abstract void qj(int var1);

    public Vec3f getScaling() {
        return this.scaling;
    }

    public void setScaling(Vec3f var1) {
        this.scaling.set(var1);
    }

    private static class a implements SP {
        public final Vec3f cam;
        public final Vec3f can;

        private a() {
            this.cam = new Vec3f(1.0E30F, 1.0E30F, 1.0E30F);
            this.can = new Vec3f(-1.0E30F, -1.0E30F, -1.0E30F);
        }

        // $FF: synthetic method
        a(a var1) {
            this();
        }

        public void b(Vec3f[] var1, int var2, int var3) {
            JL.o(this.cam, var1[0]);
            JL.p(this.can, var1[0]);
            JL.o(this.cam, var1[1]);
            JL.p(this.can, var1[1]);
            JL.o(this.cam, var1[2]);
            JL.p(this.can, var1[2]);
        }
    }
}
