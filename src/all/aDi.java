package all;

import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Логирование часов потока
 */
public abstract class aDi extends Thread {
    private final SimpleDateFormat dBH;
    private WatchDog hyo = new WatchDog();
    private int hyp;
    private long hyq;
    private eC cVg;
    private long hyr;
    private long hys;
    private PrintWriter eqC;

    public aDi(String var1) {
        super(var1);
        this.cVg = atM.cVg;
        this.hyr = this.cVg.currentTimeMillis();
        this.hys = this.cVg.currentTimeMillis();
        this.dBH = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS ");
        this.eqC = new PrintWriter(System.out, true);
        this.setDaemon(true);
    }

    public void a(eC var1) {
        this.cVg = var1;
        this.hyr = var1.currentTimeMillis();
        this.hys = var1.currentTimeMillis();
    }

    public void run() {
        this.hyo.setPrintWriter(this.eqC);

        while (true) {
            this.doRun();
        }
    }

    protected void doRun() {
        long var1 = this.cVg.currentTimeMillis();
        long var3 = this.cR(var1);
        if (var3 > 100L) {
            PoolThread.sleep(10L);
        } else if (var3 > 50L) {
            PoolThread.sleep(5L);
        } else if (var3 > 10L) {
            PoolThread.sleep(1L);
        } else {
            PoolThread.sleep(5L);
        }

        var1 = this.cVg.currentTimeMillis();
        var3 = this.cR(var1);
        if (var3 > 200L) {
            this.hyo.caL();
            this.hyr = var1 - var3;
            ++this.hyp;
            this.hys = 0L;
        } else {
            if (this.hys == 0L) {
                this.hyq += var1 - this.hyr;
            }

            this.hys = var1;
            if (this.hyp > 5000 || this.hyp > 0 && var1 - this.hyr > 1000L) {
                String var5 = this.dBH.format(new Date(System.currentTimeMillis()));
                this.eqC.println(var5 + "----------------------- Glitch log --------------------------");
                this.eqC.println(var5 + " Glitch " + this.hyq + " ms " + this.hyp + " samples " + this.hyr);
                this.eqC.println(var5 + "--------------------------------------------------------------");
                this.hyo.f((Thread) null);
                this.eqC.println(var5 + "----------------------- Glitch log end --------------------------");
                this.eqC.flush();
                this.hyo.reset();
                this.hyq = 0L;
                this.hyp = 0;
            }
        }

    }

    public void setPrintWriter(PrintWriter var1) {
        this.eqC = var1;
    }

    protected abstract long cR(long var1);
}
