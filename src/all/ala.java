package all;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.io.UTFDataFormatException;

public final class ala extends OutputStream implements Serializable, oU_q //TODO На удаление
{
    static final ks_q fVj = new amS_q();
    static final ks_q fVk = new arC("Tiny Float", true, 4, 3, true, 7);
    static final ks_q fVl = new arC("24 bit FloatEncoding", true, 8, 15, true, 127);
    static final int[] emM = new int[32];

    static {
        int var0 = 0;

        for (int var1 = 0; var1 < 32; ++var1) {
            emM[var1] = var0;
            var0 = var0 << 1 | 1;
        }

    }

    private byte[] buf;
    private int count;
    private int fVh;
    private int fVi;

    //Создание буфера сообщения  размером 16 byte
    public ala() {
        this(16);
    }

    public ala(int var1) {
        this.fVh = -1;
        this.fVi = 0;
        if (var1 < 0) {
            throw new IllegalArgumentException("Invalid size: " + var1);
        } else {
            this.buf = new byte[var1];
        }
    }

    public void d(int var1, long var2) {
        if (var1 > 32) {
            this.writeBits(32, (int) (var2 >> var1 - 32));
            this.writeBits(var1 - 32, (int) var2);
        } else {
            this.writeBits(var1, (int) var2);
        }

    }

    public void writeBits(int var1, int var2) {
        int var3 = this.fVi;

        while (var1 > 0) {
            if (var1 < var3) {
                this.buf[this.fVh] = (byte) (this.buf[this.fVh] | (var2 & emM[var1]) << var3 - var1);
                this.fVi = var3 - var1;
                return;
            }

            if (var1 == var3) {
                this.buf[this.fVh] = (byte) (this.buf[this.fVh] | var2 & emM[var1]);
                this.fVi = 0;
                return;
            }

            if (var3 == 0) {
                this.ensure(this.count + 1);
                this.fVh = this.count;
                this.buf[this.count] = 0;
                ++this.count;
                var3 = 8;
            } else {
                this.buf[this.fVh] = (byte) (this.buf[this.fVh] | var2 >> var1 - var3 & emM[var3]);
                var1 -= var3;
                var3 = 0;
            }
        }

    }

    //Запись в буфер сообщения  байт сообщения
    public final void write(int var1) {
        int var2 = this.count + 1;//следующее место байта для записи
        this.ensure(var2);
        this.buf[this.count] = (byte) var1;//записываем байт сообщения
        this.count = var2;//указываем следующее место для записи
    }

    public final void write(byte[] var1, int var2, int var3) {
        if (var3 != 0) {
            int var4 = this.count + var3;
            this.ensure(var4);
            System.arraycopy(var1, var2, this.buf, this.count, var3);
            this.count = var4;
        }
    }

    //обеспечивать/проверка что сообщение влезит а буфер
    private final void ensure(int var1) {
        if (var1 > this.buf.length) {
            byte[] var2 = new byte[Math.max(this.buf.length << 1, var1)];
            System.arraycopy(this.buf, 0, var2, 0, this.count);
            this.buf = var2;
        }

    }

    public final void reset() {
        this.count = 0;
        this.fVh = -1;
        this.fVi = 0;
    }

    public final void seek(int var1) {
        this.count = var1;
    }

    public final byte[] toByteArray() {
        if (this.buf.length != this.count) {
            byte[] var1 = new byte[this.count];
            System.arraycopy(this.buf, 0, var1, 0, this.count);
            return var1;
        } else {
            return this.buf;
        }
    }

    public final int size() {
        return this.count;
    }

    public final void writeBoolean(boolean var1) {
        this.writeBits(1, var1 ? 1 : 0);
    }

    //Запись в буфер сообщения байт сообщения
    public final void writeByte(int var1) {
        this.write(var1);
    }

    private final void sn(int var1) {
        this.write(var1);
    }

    private final void so(int var1) {
        int var2 = this.count + 2;
        this.ensure(var2);
        this.buf[this.count] = (byte) (var1 >>> 8 & 255);
        this.buf[this.count + 1] = (byte) (var1 >>> 0 & 255);
        this.count = var2;
    }

    private void sp(int var1) {
        int var2 = this.count + 3;
        this.ensure(var2);
        this.buf[this.count] = (byte) (var1 >>> 16 & 255);
        this.buf[this.count + 1] = (byte) (var1 >>> 8 & 255);
        this.buf[this.count + 2] = (byte) (var1 >>> 0 & 255);
        this.count = var2;
    }

    private void hP(long var1) {
        int var3 = this.count + 5;
        this.ensure(var3);
        this.buf[this.count] = (byte) ((int) (var1 >>> 32));
        this.buf[this.count + 1] = (byte) ((int) (var1 >>> 24));
        this.buf[this.count + 2] = (byte) ((int) (var1 >>> 16));
        this.buf[this.count + 3] = (byte) ((int) (var1 >>> 8));
        this.buf[this.count + 4] = (byte) ((int) (var1 >>> 0));
        this.count = var3;
    }

    private void hQ(long var1) {
        int var3 = this.count + 6;
        this.ensure(var3);
        this.buf[this.count] = (byte) ((int) (var1 >>> 40));
        this.buf[this.count + 1] = (byte) ((int) (var1 >>> 32));
        this.buf[this.count + 2] = (byte) ((int) (var1 >>> 24));
        this.buf[this.count + 3] = (byte) ((int) (var1 >>> 16));
        this.buf[this.count + 4] = (byte) ((int) (var1 >>> 8));
        this.buf[this.count + 5] = (byte) ((int) (var1 >>> 0));
        this.count = var3;
    }

    private void hR(long var1) {
        int var3 = this.count + 7;
        this.ensure(var3);
        this.buf[this.count] = (byte) ((int) (var1 >>> 48));
        this.buf[this.count + 1] = (byte) ((int) (var1 >>> 40));
        this.buf[this.count + 2] = (byte) ((int) (var1 >>> 32));
        this.buf[this.count + 3] = (byte) ((int) (var1 >>> 24));
        this.buf[this.count + 4] = (byte) ((int) (var1 >>> 16));
        this.buf[this.count + 5] = (byte) ((int) (var1 >>> 8));
        this.buf[this.count + 6] = (byte) ((int) (var1 >>> 0));
        this.count = var3;
    }

    public final void writeShort(int var1) {
        short var2 = (short) var1;
        short var3;
        boolean var4;
        if (var2 < 0) {
            var4 = true;
            var3 = (short) (-var2);
        } else {
            var4 = false;
            var3 = var2;
        }

        if (var3 <= 255 && var3 >= 0) {
            this.writeBits(1, 0);
            this.writeBits(1, var4 ? 1 : 0);
            this.sn(var3);
        } else {
            this.writeBits(1, 1);
            this.so(var2);
        }

    }

    public final void writeChar(int var1) {
        var1 &= 65535;
        if (var1 <= 255) {
            this.writeBits(1, 0);
            this.sn(var1);
        } else {
            this.writeBits(1, 1);
            this.so(var1);
        }

    }

    public final void sq(int var1) {
        int var2 = this.count + 4;
        this.ensure(var2);
        this.buf[this.count] = (byte) (var1 >>> 24 & 255);
        this.buf[this.count + 1] = (byte) (var1 >>> 16 & 255);
        this.buf[this.count + 2] = (byte) (var1 >>> 8 & 255);
        this.buf[this.count + 3] = (byte) (var1 >>> 0 & 255);
        this.count = var2;
    }

    public final void writeInt(int var1) {
        boolean var3 = false;
        boolean var2;
        int var4;
        if (var1 < 0) {
            var2 = true;
            var4 = -var1;
        } else {
            var4 = var1;
            var2 = false;
        }

        if (var4 <= 16777215 && var4 >= 0) {
            if (var4 <= 255) {
                this.writeBits(2, 0);
                this.writeBits(1, var2 ? 1 : 0);
                this.writeByte(var4);
            } else if (var4 <= 65535) {
                this.writeBits(2, 1);
                this.writeBits(1, var2 ? 1 : 0);
                this.so(var4);
            } else {
                this.writeBits(2, 2);
                this.writeBits(1, var2 ? 1 : 0);
                this.sp(var4);
            }
        } else {
            this.writeBits(2, 3);
            this.sq(var1);
        }
    }

    public final void sr(int var1) {
        if (var1 < 0) {
            throw new IllegalArgumentException(var1 + "<0");
        } else {
            if (var1 <= 255) {
                this.writeBits(2, 0);
                this.sn(var1);
            } else if (var1 <= 65535) {
                this.writeBits(2, 1);
                this.so(var1);
            } else if (var1 <= 16777215) {
                this.writeBits(2, 2);
                this.sp(var1);
            } else {
                this.writeBits(2, 3);
                this.sq(var1);
            }

        }
    }

    private final void hS(long var1) {
        int var3 = this.count + 8;
        this.ensure(var3);
        this.buf[this.count] = (byte) ((int) (var1 >>> 56));
        this.buf[this.count + 1] = (byte) ((int) (var1 >>> 48));
        this.buf[this.count + 2] = (byte) ((int) (var1 >>> 40));
        this.buf[this.count + 3] = (byte) ((int) (var1 >>> 32));
        this.buf[this.count + 4] = (byte) ((int) (var1 >>> 24));
        this.buf[this.count + 5] = (byte) ((int) (var1 >>> 16));
        this.buf[this.count + 6] = (byte) ((int) (var1 >>> 8));
        this.buf[this.count + 7] = (byte) ((int) (var1 >>> 0));
        this.count = var3;
    }

    public final void writeLong(long var1) {
        boolean var3;
        long var4;
        if (var1 < 0L) {
            var4 = -var1;
            var3 = true;
        } else {
            var3 = false;
            var4 = var1;
        }

        if (var4 < 0L) {
            this.writeBits(3, 7);
            this.hS(var1);
        } else {
            if (var4 <= 255L) {
                this.writeBits(3, 0);
                this.writeBits(1, var3 ? 1 : 0);
                this.sn((byte) ((int) var4));
            } else if (var4 <= 65535L) {
                this.writeBits(3, 1);
                this.writeBits(1, var3 ? 1 : 0);
                this.so((short) ((int) var4));
            } else if (var4 <= 16777215L) {
                this.writeBits(3, 2);
                this.writeBits(1, var3 ? 1 : 0);
                this.sp((int) var4);
            } else if (var4 <= -1L) {
                this.writeBits(3, 3);
                this.writeBits(1, var3 ? 1 : 0);
                this.sq((int) var4);
            } else if (var4 <= 1099511627775L) {
                this.writeBits(3, 4);
                this.writeBits(1, var3 ? 1 : 0);
                this.hP(var4);
            } else if (var4 <= 281474976710655L) {
                this.writeBits(3, 5);
                this.writeBits(1, var3 ? 1 : 0);
                this.hQ(var4);
            } else if (var4 <= 72057594037927935L) {
                this.writeBits(3, 6);
                this.writeBits(1, var3 ? 1 : 0);
                this.hR(var4);
            } else {
                this.writeBits(3, 7);
                this.hS(var1);
            }

        }
    }

    public final void writeFloat(float var1) {
        int var2 = (int) var1;
        if ((float) var2 == var1 && var1 != 0.0F) {
            this.writeBits(3, 4);
            this.writeInt(var2);
        } else {
            int var3 = Float.floatToRawIntBits(var1);
            int var4 = fVk.aT(var1);
            if (Float.floatToRawIntBits(fVk.cF(var4)) == var3) {
                this.writeBits(3, 5);
                this.sn(var4);
            } else {
                var4 = fVj.aT(var1);
                if (Float.floatToRawIntBits(fVj.cF(var4)) == var3) {
                    this.writeBits(3, 6);
                    this.so(var4);
                } else {
                    var4 = fVl.aT(var1);
                    if (Float.floatToRawIntBits(fVl.cF(var4)) == var3) {
                        this.writeBits(3, 7);
                        this.sp(var4);
                    } else {
                        this.writeBits(1, 0);
                        this.sq(var3);
                    }
                }
            }
        }
    }

    public final void writeDouble(double var1) {
        float var3 = (float) var1;
        if ((double) var3 == var1) {
            this.writeBits(1, 1);
            this.writeFloat(var3);
        } else {
            this.writeBits(1, 0);
            this.hS(Double.doubleToLongBits(var1));
        }
    }

    public final void writeBytes(String var1) {
        int var2 = var1.length();
        this.ensure(this.count + var2);
        int var3 = this.count;

        for (int var4 = 0; var4 < var2; ++var4) {
            this.buf[var3++] = (byte) var1.charAt(var4);
        }

        this.count = var3;
    }

    public final void writeChars(String var1) {
        int var2 = var1.length();
        this.ensure(this.count + (var2 << 1));

        for (int var3 = 0; var3 < var2; ++var3) {
            char var4 = var1.charAt(var3);
            this.writeChar(var4);
        }

    }

    public final void writeUTF(String var1) throws UTFDataFormatException {
        int var2 = var1.length();
        int var3 = 0;

        char var4;
        int var6;
        for (var6 = 0; var6 < var2; ++var6) {
            var4 = var1.charAt(var6);
            if (var4 <= '\u007f') {
                ++var3;
            } else if (var4 > '\u07ff') {
                var3 += 3;
            } else {
                var3 += 2;
            }
        }

        if (var3 > 65535) {
            throw new UTFDataFormatException("encoded string too long: " + var3 + " bytes");
        } else {
            this.writeShort(var3);
            this.ensure(this.count + var3);
            int var5 = this.count;

            for (var6 = 0; var6 < var2; ++var6) {
                var4 = var1.charAt(var6);
                if (var4 > '\u007f') {
                    break;
                }

                this.buf[var5++] = (byte) var4;
            }

            for (; var6 < var2; ++var6) {
                var4 = var1.charAt(var6);
                if (var4 <= '\u007f') {
                    this.buf[var5++] = (byte) var4;
                } else if (var4 > '\u07ff') {
                    this.buf[var5++] = (byte) (224 | var4 >> 12 & 15);
                    this.buf[var5++] = (byte) (128 | var4 >> 6 & 63);
                    this.buf[var5++] = (byte) (128 | var4 & 63);
                } else {
                    this.buf[var5++] = (byte) (192 | var4 >> 6 & 31);
                    this.buf[var5++] = (byte) (128 | var4 & 63);
                }
            }

            this.count = var5;
        }
    }

    public final int getCount() {
        return this.count;
    }

    public final void writeTo(OutputStream var1) throws IOException {
        var1.write(this.buf, 0, this.count);
    }

    public final byte[] chG() {
        return this.buf;
    }

    public String toString() {
        return aK.b(this.buf, 0, this.buf.length, " ", 16);
    }
}
