package all;

import java.io.IOException;

/**
 * Чтение файлов с именами классов InfraScripts, GameScripts
 */
public class SH extends agO implements FP {
    private final atZ djq;

    public SH() {
        this((atZ) null);
    }

    public SH(atZ var1) {
        this.djq = var1;
        this.eu(!acF.fdO);
    }

    /**
     * Читает список класов из файлов  InfraScripts, GameScripts
     *
     * @return
     */
    public ajs ate() {
        ajs var1;
        if ((var1 = super.ate()) != null) {
            return var1;
        } else {
            //Инструмент для чтения файла
            GA var2 = new GA(this.djq);
            try {
                var2.d(this.getClass().getResource("/taikodom/infra/script/InfraScripts"));
                var2.d(this.getClass().getResource("/taikodom/game/script/GameScripts"));
            } catch (IOException var4) {
                throw new RuntimeException(var4);
            }
            super.b((ajs) var2);
            return var2;
        }
    }
}
