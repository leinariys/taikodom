package all;

/**
 * обёртка данных логин пароль
 * class LoginAddon
 */
public class LoginPasswordValue {
    private final String password;
    private final String username;
    private boolean saveData;

    /**
     * Установка логина и пароля
     *
     * @param var1 Логин
     * @param var2 Пароль
     * @param var3
     */
    public LoginPasswordValue(String var1, String var2, boolean var3) {
        this.username = var1;
        this.password = var2;
        this.saveData = var3;
    }

    /**
     * ПОлучить парол
     *
     * @return
     */
    public String getPassword() {
        return this.password;
    }

    /**
     * Получить логин
     *
     * @return
     */
    public String getUsername() {
        return this.username;
    }

    public boolean getSaveData() {
        return this.saveData;
    }
}
