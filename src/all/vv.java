package all;

public class vv {
    static double[] bzs = new double[16];
    protected final bc_q tempTransform = new bc_q();
    private BM_q[] bzp = new BM_q[6];
    private int[][] bzq = new int[6][6];
    private double[] bzr = new double[6];

    public vv() {
        for (int var1 = 0; var1 < 6; ++var1) {
            this.bzp[var1] = new BM_q();
        }

    }

    public void a(bc_q var1, ajK var2) {
        var1.a(this.tempTransform);
        ajD var3 = this.tempTransform.mX;
        Vector3dOperations var4 = this.tempTransform.position;
        bzs[0] = (double) (var3.m00 * var2.m00 + var3.m10 * var2.m01 + var3.m20 * var2.m02);
        bzs[1] = (double) (var3.m00 * var2.m10 + var3.m10 * var2.m11 + var3.m20 * var2.m12);
        bzs[2] = (double) (var3.m00 * var2.m20 + var3.m10 * var2.m21 + var3.m20 * var2.m22);
        bzs[3] = (double) (var3.m00 * var2.m30 + var3.m10 * var2.m31 + var3.m20 * var2.m32);
        bzs[4] = (double) (var3.m01 * var2.m00 + var3.m11 * var2.m01 + var3.m21 * var2.m02);
        bzs[5] = (double) (var3.m01 * var2.m10 + var3.m11 * var2.m11 + var3.m21 * var2.m12);
        bzs[6] = (double) (var3.m01 * var2.m20 + var3.m11 * var2.m21 + var3.m21 * var2.m22);
        bzs[7] = (double) (var3.m01 * var2.m30 + var3.m11 * var2.m31 + var3.m21 * var2.m32);
        bzs[8] = (double) (var3.m02 * var2.m00 + var3.m12 * var2.m01 + var3.m22 * var2.m02);
        bzs[9] = (double) (var3.m02 * var2.m10 + var3.m12 * var2.m11 + var3.m22 * var2.m12);
        bzs[10] = (double) (var3.m02 * var2.m20 + var3.m12 * var2.m21 + var3.m22 * var2.m22);
        bzs[11] = (double) (var3.m02 * var2.m30 + var3.m12 * var2.m31 + var3.m22 * var2.m32);
        bzs[12] = var4.x * (double) var2.m00 + var4.y * (double) var2.m01 + var4.z * (double) var2.m02 + (double) var2.m03;
        bzs[13] = var4.x * (double) var2.m10 + var4.y * (double) var2.m11 + var4.z * (double) var2.m12 + (double) var2.m13;
        bzs[14] = var4.x * (double) var2.m20 + var4.y * (double) var2.m21 + var4.z * (double) var2.m22 + (double) var2.m23;
        bzs[15] = var4.x * (double) var2.m30 + var4.y * (double) var2.m31 + var4.z * (double) var2.m32 + (double) var2.m33;
        this.bzp[a.eXS.ordinal()].c(bzs[3] - bzs[0], bzs[7] - bzs[4], bzs[11] - bzs[8], bzs[15] - bzs[12]);
        this.bzp[a.eXR.ordinal()].c(bzs[3] + bzs[0], bzs[7] + bzs[4], bzs[11] + bzs[8], bzs[15] + bzs[12]);
        this.bzp[a.eXU.ordinal()].c(bzs[3] + bzs[1], bzs[7] + bzs[5], bzs[11] + bzs[9], bzs[15] + bzs[13]);
        this.bzp[a.eXT.ordinal()].c(bzs[3] - bzs[1], bzs[7] - bzs[5], bzs[11] - bzs[9], bzs[15] - bzs[13]);
        this.bzp[a.eXW.ordinal()].c(bzs[3] - bzs[2], bzs[7] - bzs[6], bzs[11] - bzs[10], bzs[15] - bzs[14]);
        this.bzp[a.eXV.ordinal()].c(bzs[3] + bzs[2], bzs[7] + bzs[6], bzs[11] + bzs[10], bzs[15] + bzs[14]);

        for (int var5 = 0; var5 < 6; ++var5) {
            this.bzp[var5].normalize();
            this.bzq[var5][0] = 3;
            this.bzq[var5][3] = 0;
            this.bzq[var5][1] = 4;
            this.bzq[var5][4] = 1;
            this.bzq[var5][2] = 5;
            this.bzq[var5][5] = 2;
            if (this.bzp[var5].getNormal().x <= 0.0D) {
                this.bzq[var5][0] = 0;
                this.bzq[var5][3] = 3;
            }

            if (this.bzp[var5].getNormal().y <= 0.0D) {
                this.bzq[var5][1] = 1;
                this.bzq[var5][4] = 4;
            }

            if (this.bzp[var5].getNormal().z <= 0.0D) {
                this.bzq[var5][2] = 2;
                this.bzq[var5][5] = 5;
            }
        }

    }

    public boolean a(Pt var1) {
        for (int var2 = 0; var2 < 6; ++var2) {
            if (this.bzp[var2].getNormal().b((te_w) var1.bny()) + this.bzp[var2].dnz() < (double) (-var1.getRadius())) {
                return false;
            }
        }

        return true;
    }

    public boolean a(aVS var1) {
        for (int var2 = 0; var2 < 6; ++var2) {
            if (this.bzp[var2].getNormal().az(var1.zO()) + this.bzp[var2].dnz() < -var1.dCJ()) {
                return false;
            }
        }

        return true;
    }

    public boolean a(aLH var1) {
        Vector3dOperations var2 = var1.dim();
        this.bzr[0] = var2.x;
        this.bzr[1] = var2.y;
        this.bzr[2] = var2.z;
        Vector3dOperations var3 = var1.din();
        this.bzr[3] = var3.x;
        this.bzr[4] = var3.y;
        this.bzr[5] = var3.z;

        for (int var4 = 0; var4 < 6; ++var4) {
            Vector3dOperations var5 = this.bzp[var4].getNormal();
            if (var5.x * this.bzr[this.bzq[var4][0]] + var5.y * this.bzr[this.bzq[var4][1]] + var5.z * this.bzr[this.bzq[var4][2]] + this.bzp[var4].dnz() < 0.0D) {
                return false;
            }
        }

        return true;
    }

    public int b(aLH var1) {
        boolean var2 = false;
        boolean var3 = false;
        Vector3dOperations var4 = var1.dim();
        this.bzr[0] = var4.x;
        this.bzr[1] = var4.y;
        this.bzr[2] = var4.z;
        Vector3dOperations var5 = var1.din();
        this.bzr[3] = var5.x;
        this.bzr[4] = var5.y;
        this.bzr[5] = var5.z;

        for (int var6 = 0; var6 < 6; ++var6) {
            Vector3dOperations var7 = this.bzp[var6].getNormal();
            if (var7.x * this.bzr[this.bzq[var6][0]] + var7.y * this.bzr[this.bzq[var6][1]] + var7.z * this.bzr[this.bzq[var6][2]] + this.bzp[var6].dnz() < 0.0D) {
                return 0;
            }

            if (var7.x * this.bzr[this.bzq[var6][3]] + var7.y * this.bzr[this.bzq[var6][4]] + var7.z * this.bzr[this.bzq[var6][5]] + this.bzp[var6].dnz() <= 0.0D) {
                var2 = true;
            }
        }

        byte var8;
        if (var2) {
            var8 = 1;
        } else {
            var8 = 2;
        }

        return var8;
    }

    public boolean v(Vector3dOperations var1) {
        for (int var2 = 0; var2 < 6; ++var2) {
            if (this.bzp[var2].aM(var1) == BM_q.a.cqo) {
                return false;
            }
        }

        return true;
    }

    public BM_q[] akT() {
        return this.bzp;
    }

    public String toString() {
        String var1 = "";

        for (int var2 = 0; var2 < 6; ++var2) {
            var1 = var1 + a.values()[var2] + ":" + this.bzp[var2] + "\n";
        }

        return var1;
    }

    public static enum a {
        eXR,
        eXS,
        eXT,
        eXU,
        eXV,
        eXW;
    }
}
