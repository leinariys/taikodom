package all;

import gnu.trove.TIntObjectHashMap;
import gnu.trove.TObjectIntHashMap;
import org.w3c.dom.css.CSSRule;
import org.w3c.dom.css.CSSStyleDeclaration;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.ImageObserver;
import java.io.StringReader;
import java.util.*;
import java.util.Map.Entry;

/**
 * Система событий графического Component
 * отрибуты тега
 * Держатель CSS
 * pv.class
 */
public class ComponentManager implements aeK {
    private static final int aSs = 150;
    private static final TObjectIntHashMap stateEvent = new TObjectIntHashMap();
    private static final ahv aSw = new ahv() {
        public int getLength() {
            return 0;
        }

        public asi qR(int var1) {
            return null;
        }
    };
    public static int STATE_ARMED = 1;
    public static int STATE_SELECTED = 2;
    public static int STATE_ENABLED = 4;
    public static int STATE_DISABLED = 8;
    public static int STATE_PRESSED = 16;
    public static int STATE_ROLLOVER = 32;
    public static int STATE_READ_ONLY = 64;
    public static int STATE_FOCUSED = 128;
    public static int STATE_EXPANDED = 256;
    /**
     * Список графический компонент = обработчик событий
     */
    static ComponentGraphicsResource comGrapRes;
    /**
     * Обработчик события курсор над объектом
     */
    private static MousEvent mousEvent = new MousEvent();
    /**
     * Прием событий фокусировки клавиатуры
     */
    private static KeyboardEvent keyboardEvent = new KeyboardEvent();

    static {
        comGrapRes = new ComponentGraphicsResource(16, 0.75F, 16, ComponentGraphicsResource.i.eeK, ComponentGraphicsResource.i.eeJ, EnumSet.of(ComponentGraphicsResource.h.dRd));
        stateEvent.put("armed", STATE_ARMED);
        stateEvent.put("selected", STATE_SELECTED);
        stateEvent.put("expanded", STATE_EXPANDED);
        stateEvent.put("enabled", STATE_ENABLED);
        stateEvent.put("disabled", STATE_DISABLED);
        stateEvent.put("pressed", STATE_PRESSED);
        stateEvent.put("hover", STATE_ROLLOVER);
        stateEvent.put("over", STATE_ROLLOVER);
        stateEvent.put("rollover", STATE_ROLLOVER);
        stateEvent.put("read-only", STATE_READ_ONLY);
        stateEvent.put("focused", STATE_FOCUSED);
    }

    /**
     * Ссылка на графический компонент
     */
    private final Component currentComponent;
    protected boolean mouseOver;
    protected boolean aSJ;
    protected aWs aSK;
    protected boolean focused;
    float aSB = 1.0F;
    Color color;
    /**
     * Список атрибутов тега ключ = значение
     */
    private Map aSx;
    /**
     * Стиль
     * vertical-align:fill; text-align:fill
     */
    private String aSy;
    private TIntObjectHashMap aSz;
    /**
     * CSS стиль
     */
    private avI cssNode = null;
    private CSSStyleDeclaration aSC;
    private ComponentManager aSD;
    /**
     * Потомок графического Component
     */
    private Component parentComponent;
    /**
     * Имя xml тега, список в class BaseUItegXML
     *
     * @return Пример progress
     */
    private String name;
    private ahv aSF;
    private int aSG;
    private int aSH;
    private PropertiesUiFromCss aSI;
    private int aSL;
    private int aSM;
    private boolean aSN = true;
    /**
     * Добавлен ли Обработчик события курсор над объектом
     */
    private boolean isMouseListener;

    /**
     * Задаём графический Component, устанавливаем имя тега
     *
     * @param var1
     * @param var2
     */
    public ComponentManager(String var1, Component var2) {
        this.currentComponent = var2;
        if (var2 instanceof IComponentCustomWrapper) {
            String var3 = ((IComponentCustomWrapper) var2).getElementName();
            this.name = var3 != null ? var3 : var1;
        } else {
            this.name = var1;
        }
        //Присвиваем графическому Component этот обработчик событий
        if (var2 instanceof JComponent) {
            ((JComponent) var2).putClientProperty(aeK.cssHolder, this);
        }

        comGrapRes.put(var2, this);
        if (var2 instanceof JTextComponent) {
            //Обработчик события курсор над объектом
            this.isMouseListener = true;
            var2.addMouseListener(mousEvent);
        }
        //Задать Прием событий фокусировки клавиатуры
        var2.addFocusListener(keyboardEvent);
    }

    /**
     * Поиск Система событий для этого графический Component из аддона
     *
     * @param var0 Графический Component компонент аддона
     * @return Система событий для этого графического Component
     */
    public static aeK getCssHolder(Component var0) {
        aeK var1;
        if (var0 instanceof JComponent) {
            var1 = (aeK) ((JComponent) var0).getClientProperty(aeK.cssHolder);//Привязка
            return var1;
        } else {
            var1 = (aeK) comGrapRes.get(var0);
            return var1;
        }
    }

    public static Collection Vn() {
        ArrayList var0 = new ArrayList();
        var0.addAll(comGrapRes.values());
        return var0;
    }

    public CSSStyleDeclaration Vh() {
        if (this.aSy != null && this.aSC == null) {
            try {
                this.aSC = this.aY(this.aSy);
            } catch (Exception var2) {
                var2.printStackTrace();
                this.aSC = new aHx((CSSRule) null);
            }
        }

        return this.aSC;
    }

    private CSSStyleDeclaration aY(String var1) {
        ParserCss var2 = ParserCss.getParserCss();
        CSSStyleDeclaration var3 = var2.getCSSStyleDeclaration(new InputSource(new StringReader("{" + var1 + "}")));
        if (var3 != null) {
            EQ.parseRule(var3);
        }

        return var3;
    }

    public String getAttribute(String var1) {
        return this.aSx == null ? null : (String) this.aSx.get(var1);
    }

    /**
     * Задать атрибут
     *
     * @param var1 ключ
     * @param var2 значение
     */
    public void setAttribute(String var1, String var2) {
        if (this.aSx == null) {
            this.aSx = new HashMap();
        }

        this.aSx.put(var1, var2);
        this.clear();
    }

    public void aZ(String var1) {
        this.setAttribute("class", var1);
        this.clear();
    }

    /**
     * @param var1
     */
    public void setAttributes(Map var1) {
        Iterator var3 = var1.entrySet().iterator();

        while (var3.hasNext()) {
            Entry var2 = (Entry) var3.next();
            this.setAttribute((String) var2.getKey(), (String) var2.getValue());
        }

        this.clear();
    }

    public boolean hasAttribute(String var1) {
        return this.aSx == null ? false : this.aSx.containsKey(var1);
    }

    protected int Vi() {
        int var1 = this.aSH;
        if (this.currentComponent.isEnabled()) {
            var1 |= 4;
        } else {
            var1 |= 8;
        }

        if ((this.aSG & 128) == 0 && this.focused) {
            var1 |= 128;
        }

        if ((this.aSG & 32) == 0 && this.mouseOver) {
            var1 |= 32;
        }

        return var1;
    }

    /**
     * Получить СSS стиль
     *
     * @return
     */
    public avI getCssNode() {
        return this.cssNode;
    }

    /**
     * Установить CSS стиль
     *
     * @param var1
     */
    public void setCssNode(avI var1) {
        this.cssNode = var1;
    }

    public Dimension a(JComponent var1, Dimension var2) {
        PropertiesUiFromCss var3 = this.Vp();
        if (var3 != null) {
            Tu var4 = var3.atG();
            Tu var5 = var3.atF();
            if (var4 != Tu.fEC && var5 != Tu.fEC) {
                return new Dimension((int) var4.value, (int) var5.value);
            }
        }

        return var2;
    }

    public Dimension b(JComponent var1, Dimension var2) {
        PropertiesUiFromCss var3 = this.Vp();
        if (var3 != null) {
            Tu var4 = var3.atI();
            Tu var5 = var3.atH();
            int var6 = 0;
            int var7 = 0;
            if (var4 == Tu.fEC && var5 == Tu.fEC && var2 == null) {
                return null;
            } else {
                Container var8 = var1.getParent();
                int var9 = var8 != null ? var8.getWidth() : 0;
                int var10 = var8 != null ? var8.getHeight() : 0;
                if (var2 == null) {
                    var2 = var1.minimumSize();
                }

                if (var4 != Tu.fEC) {
                    var6 = var4.jp((float) var9);
                } else if (var2 != null) {
                    var6 = var2.width;
                }

                if (var5 != Tu.fEC) {
                    var7 = var5.jp((float) var10);
                } else if (var2 != null) {
                    var7 = var2.height;
                }

                Tu var11 = var3.atG();
                Tu var12 = var3.atF();
                int var13;
                if (var11 != Tu.fEC) {
                    var13 = var11.jp((float) var9);
                    if (var6 > var13) {
                        var6 = var13;
                    }
                }

                if (var12 != Tu.fEC) {
                    var13 = var12.jp((float) var10);
                    if (var7 > var13) {
                        var7 = var13;
                    }
                }

                return new Dimension(var6, var7);
            }
        } else {
            return var2;
        }
    }

    public Dimension c(JComponent var1, Dimension var2) {
        PropertiesUiFromCss var3 = this.Vp();
        if (var3 != null) {
            Tu var4 = var3.atS();
            Tu var5 = var3.atx();
            int var6 = 0;
            int var7 = 0;
            if (var4 == Tu.fEC && var5 == Tu.fEC && var2 == null) {
                return null;
            } else {
                Container var8 = var1.getParent();
                int var9 = var8 != null ? var8.getWidth() : 0;
                int var10 = var8 != null ? var8.getHeight() : 0;
                if (var4 != Tu.fEC) {
                    var6 = var4.jp((float) var9);
                    if (var2 != null) {
                        var6 = Math.max(var2.width, var6);
                    }
                } else if (var2 != null) {
                    var6 = var2.width;
                }

                if (var5 != Tu.fEC) {
                    var7 = var5.jp((float) var10);
                    if (var2 != null) {
                        var7 = Math.max(var2.height, var7);
                    }
                } else if (var2 != null) {
                    var7 = var2.height;
                }

                Image var11 = var3.fT(0);
                if (var11 != null) {
                    int var12 = var11.getWidth((ImageObserver) null);
                    int var13 = var11.getHeight((ImageObserver) null);
                    if (var12 != -1) {
                        var6 = Math.max(var12, var6);
                    }

                    if (var13 != -1) {
                        var7 = Math.max(var13, var7);
                    }
                }

                Tu var17 = var3.atI();
                Tu var18 = var3.atH();
                int var14;
                if (var17 != Tu.fEC) {
                    var14 = var17.jp((float) var9);
                    if (var6 < var14) {
                        var6 = var14;
                    }
                }

                if (var18 != Tu.fEC) {
                    var14 = var18.jp((float) var10);
                    if (var7 < var14) {
                        var7 = var14;
                    }
                }

                Tu var19 = var3.atG();
                Tu var15 = var3.atF();
                int var16;
                if (var19 != Tu.fEC) {
                    var16 = var19.jp((float) var9);
                    if (var6 > var16) {
                        var6 = var16;
                    }
                }

                if (var15 != Tu.fEC) {
                    var16 = var15.jp((float) var10);
                    if (var7 > var16) {
                        var7 = var16;
                    }
                }

                return new Dimension(var6, var7);
            }
        } else {
            return var2;
        }
    }

    /**
     * @param var1 Пример selected, over, pressed, disabled
     * @return
     */
    public boolean ba(String var1) {
        int var2 = stateEvent.get(var1);
        if (var2 != 0) {
            int var3 = this.Vi();
            return (var3 & var2) != 0;
        } else {
            return false;
        }
    }

    /**
     * Установить стиль
     *
     * @param var1 vertical-align:fill; text-align:fill
     */
    public void setStyle(String var1) {
        if (this.aSy != var1) {
            if (this.aSy != null && this.aSy.equals(var1)) {
                return;
            }
            this.aSy = var1;
            this.aSC = null;
            this.clear();
        }
    }

    /**
     * Чистка списка
     */
    public void clear() {
        if (this.aSz != null) {
            this.aSz.clear();
        }
        ++this.aSL;
    }

    public ahv Vl() {
        if (this.aSF == null) {
            if (!(this.currentComponent instanceof Container)) {
                this.aSF = aSw;
            } else {
                final Container var1 = (Container) this.currentComponent;
                this.aSF = new ahv() {
                    public int getLength() {
                        return var1.getComponentCount();
                    }

                    public asi qR(int var1x) {
                        return ComponentManager.getCssHolder(var1.getComponent(var1x));
                    }
                };
            }
        }

        return this.aSF;
    }

    public String getTextContent() {
        return "";
    }

    public String getNodeName() {
        return this.name;
    }

    public int getNodeType() {
        return 0;
    }

    public asi Vm() {
        if (this.parentComponent != this.currentComponent.getParent() || this.aSD == null) {
            this.parentComponent = this.currentComponent.getParent();
            if (this.parentComponent != null) {
                this.aSD = (ComponentManager) getCssHolder(this.parentComponent);
                if (this.aSD == null) {
                    for (Object var1 = this.parentComponent; var1 != null; var1 = ((Component) var1).getParent()) {
                        this.aSD = (ComponentManager) getCssHolder((Component) var1);
                        if (this.aSD != null) {
                            break;
                        }
                    }
                }
            }
        }
        return this.aSD;
    }

    public float getAlpha() {
        return this.aSB;
    }

    public void setAlpha(float var1) {
        this.aSB = var1;
    }

    public Color getColor() {
        return this.color;
    }

    public void setColor(Color var1) {
        this.color = var1;
    }

    public aeK Vo() {
        if (this.parentComponent != this.currentComponent.getParent()) {
            this.parentComponent = this.currentComponent.getParent();
            this.aSD = (ComponentManager) getCssHolder(this.parentComponent);
        }

        return this.aSD;
    }

    public Component getCurrentComponent() {
        return this.currentComponent;
    }

    /**
     * @return
     */
    public PropertiesUiFromCss Vp() {
        if (this.parentComponent != this.currentComponent.getParent()) {
            this.parentComponent = this.currentComponent.getParent();
            this.aSD = (ComponentManager) getCssHolder(this.parentComponent);
            this.aSM = this.aSD != null ? this.aSD.aSL : -1;
            this.clear();
        } else if (this.aSD != null && this.aSM != this.aSD.aSL) {
            this.aSM = this.aSD != null ? this.aSD.aSL : -1;
            this.clear();
        }

        int var1 = this.Vi();
        PropertiesUiFromCss var2;
        if (this.aSz == null) {
            this.aSz = new TIntObjectHashMap();
            var2 = null;
        } else {
            var2 = (PropertiesUiFromCss) this.aSz.get(var1);
        }

        if (var2 == null) {
            try {
                var2 = new PropertiesUiFromCss(this);
                this.aSz.put(var1, var2);
            } catch (Exception var4) {
                var4.printStackTrace();
                return null;
            }
        }

        return var2;
    }

    public void Vq() {
        this.aSG = 0;
        this.aSH = 0;
    }

    public void q(int var1, int var2) {
        this.aSG = var1;
        this.aSH = var2;
    }

    public PropertiesUiFromCss Vr() {
        if (this.aSI == null) {
            this.aSI = new PropertiesUiFromCss(this, this.Vh());
        }

        return this.aSI;
    }

    public boolean Vs() {
        return this.aSN && (this.aSK == null || this.aSK.i(this.currentComponent));
    }

    /**
     * @param var1
     */
    public void aO(boolean var1) {
        this.aSN = var1;
    }

    /**
     * Добавлен ли Обработчик события курсор над объектом
     *
     * @return true-Добавлен
     */
    public boolean Vt() {
        return this.isMouseListener;
    }

    /**
     * Добавить/удалить Обработчик события курсор над объектом
     *
     * @param var1 true-Добавить
     */
    public void aP(boolean var1) {
        if (var1 != this.isMouseListener) {
            if (var1) {
                this.currentComponent.addMouseListener(mousEvent);
            } else {
                this.currentComponent.removeMouseListener(mousEvent);
            }
            this.isMouseListener = var1;
        }
    }

    public aWs Vu() {
        return this.aSK;
    }

    public void a(aWs var1) {
        this.aSK = var1;
    }

    /**
     * Прием событий фокусировки клавиатуры
     */
    private static final class KeyboardEvent implements FocusListener {
        public void focusGained(FocusEvent var1) {
            System.out.println("DEBUG8 -" + var1);
            Component var2 = var1.getComponent();
            ((ComponentManager) ComponentManager.getCssHolder(var2)).focused = true;
            var2.repaint(150L);
            if (var2 instanceof JTextComponent && var2.getParent() instanceof JViewport && var2.getParent().getParent() instanceof JScrollPane) {
                ((ComponentManager) ComponentManager.getCssHolder(var2.getParent())).focused = true;
                ((ComponentManager) ComponentManager.getCssHolder(var2.getParent().getParent())).focused = true;
                var2.getParent().getParent().repaint(150L);
            }

        }

        public void focusLost(FocusEvent var1) {
            Component var2 = var1.getComponent();
            ((ComponentManager) ComponentManager.getCssHolder(var2)).focused = false;
            var2.repaint(150L);
            if (var2 instanceof JTextComponent && var2.getParent() instanceof JViewport && var2.getParent().getParent() instanceof JScrollPane) {
                ((ComponentManager) ComponentManager.getCssHolder(var2.getParent())).focused = false;
                ((ComponentManager) ComponentManager.getCssHolder(var2.getParent().getParent())).focused = false;
                var2.getParent().getParent().repaint(150L);
            }

        }
    }

    /**
     * Обработчик события курсор над объектом
     */
    private static final class MousEvent extends MouseAdapter {
        public void mouseEntered(MouseEvent var1) {
            Component var2 = var1.getComponent();
            ((ComponentManager) ComponentManager.getCssHolder(var2)).mouseOver = true;
            var2.repaint(150L);
            if (var2 instanceof JTextComponent && var2.getParent() instanceof JViewport && var2.getParent().getParent() instanceof JScrollPane) {
                ((ComponentManager) ComponentManager.getCssHolder(var2.getParent())).mouseOver = true;
                ((ComponentManager) ComponentManager.getCssHolder(var2.getParent().getParent())).mouseOver = true;
                var2.getParent().getParent().repaint(150L);
            }

        }

        public void mouseExited(MouseEvent var1) {
            Component var2 = var1.getComponent();
            ((ComponentManager) ComponentManager.getCssHolder(var2)).mouseOver = false;
            var2.repaint(150L);
            if (var2 instanceof JTextComponent && var2.getParent() instanceof JViewport && var2.getParent().getParent() instanceof JScrollPane) {
                ((ComponentManager) ComponentManager.getCssHolder(var2.getParent())).mouseOver = false;
                ((ComponentManager) ComponentManager.getCssHolder(var2.getParent().getParent())).mouseOver = false;
                var2.getParent().getParent().repaint(150L);
            }

        }

        public void mousePressed(MouseEvent var1) {
            ((ComponentManager) ComponentManager.getCssHolder(var1.getComponent())).aSJ = true;
            var1.getComponent().repaint(150L);
        }

        public void mouseReleased(MouseEvent var1) {
            ((ComponentManager) ComponentManager.getCssHolder(var1.getComponent())).aSJ = false;
            var1.getComponent().repaint(50L);
        }
    }
}
