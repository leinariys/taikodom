package all;

import java.awt.*;

public interface ILoaderImageInterface {
    Image getImage(String var1);

    Cursor getNull(String var1, Point var2);
}
