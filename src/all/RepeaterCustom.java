package all;

import javax.swing.*;
import java.awt.*;

public class RepeaterCustom extends BaseItemFactory {
    /**
     * Создание базового Component интерфейса
     * Вызывается из class BaseItem
     *
     * @param var1
     * @param var2
     * @param var3
     * @return
     */
    @Override
    protected JComponent CreatComponentCustom(MapKeyValue var1, Container var2, XmlNode var3) {
        return new RepeaterCustomWrapper(var1, var2, var3);
    }

}
