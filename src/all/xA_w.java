package all;

import java.util.HashMap;
import java.util.Map;

/**
 * Константы клавиш джостика
 * class CommandTranslatorAddon
 */
public class xA_w {
    public static Map Ow = new HashMap();
    public static Map Ox = new HashMap();
    public static int bGK = 0;
    public static int bGL = 1;
    public static int bGM = 2;
    public static int bGN = 3;
    public static int bGO = 4;
    public static int bGP = 5;
    public static int bGQ = 6;
    public static int bGR = 7;
    public static int bGS = 8;
    public static int bGT = 9;
    public static int bGU = 10;
    public static int bGV = 11;
    public static int bGW = 10;
    public static int bGX = 11;
    public static int bGY = 12;
    public static int bGZ = 13;
    public static int bHa = 14;

    static {
        Ow.put(Integer.valueOf(bGK), "JOY_BTN_0");
        Ow.put(Integer.valueOf(bGL), "JOY_BTN_1");
        Ow.put(Integer.valueOf(bGM), "JOY_BTN_2");
        Ow.put(Integer.valueOf(bGN), "JOY_BTN_3");
        Ow.put(Integer.valueOf(bGO), "JOY_BTN_4");
        Ow.put(Integer.valueOf(bGP), "JOY_BTN_5");
        Ow.put(Integer.valueOf(bGQ), "JOY_BTN_6");
        Ow.put(Integer.valueOf(bGR), "JOY_BTN_7");
        Ow.put(Integer.valueOf(bGS), "JOY_BTN_8");
        Ow.put(Integer.valueOf(bGT), "JOY_BTN_9");
        Ow.put(Integer.valueOf(bGU), "JOY_BTN_10");
        Ow.put(Integer.valueOf(bGV), "JOY_BTN_11");
        Ox.put("JOY_BTN_0", Integer.valueOf(bGK));
        Ox.put("JOY_BTN_1", Integer.valueOf(bGL));
        Ox.put("JOY_BTN_2", Integer.valueOf(bGM));
        Ox.put("JOY_BTN_3", Integer.valueOf(bGN));
        Ox.put("JOY_BTN_4", Integer.valueOf(bGO));
        Ox.put("JOY_BTN_5", Integer.valueOf(bGP));
        Ox.put("JOY_BTN_6", Integer.valueOf(bGQ));
        Ox.put("JOY_BTN_7", Integer.valueOf(bGR));
        Ox.put("JOY_BTN_8", Integer.valueOf(bGS));
        Ox.put("JOY_BTN_9", Integer.valueOf(bGT));
        Ox.put("JOY_BTN_10", Integer.valueOf(bGU));
        Ox.put("JOY_BTN_11", Integer.valueOf(bGV));
    }

    public static int ao(String var0) {
        Integer var1 = (Integer) Ox.get(var0);
        return var1 != null ? var1.intValue() : 0;
    }

    public static String bf(int var0) {
        String var1 = (String) Ow.get(var0);
        return var1 != null ? var1 : "UNDEFINED";
    }
}
