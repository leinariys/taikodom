package all;

import java.io.*;
import java.nio.ByteBuffer;

/**
 * Работа с каталогами файлов и папок
 */
public class FileControl implements ain {
    /**
     * Логирование
     */
    static LogPrinter logger = LogPrinter.K(FileControl.class);

    /**
     * Папка для работы
     */
    private File file;

    /**
     * Папка для работы
     *
     * @param file Папка
     */
    public FileControl(File file) {
        this.file = file;
    }

    /**
     * Папка для работы
     *
     * @param var1 Папка
     */
    public FileControl(String var1) {
        this(new File(var1));
    }

    /**
     * Создает новый экземпляр файла из родительского абстрактного пути и строки дочернего пути.
     *
     * @param var1 Исходный абстрактный путь
     * @param var2 Строка имени дочернего пути (имя файла)
     */
    public FileControl(File var1, String var2) {
        this(new File(var1, var2));
    }

    /**
     * Проверяет, существует ли файл или каталог
     *
     * @return true, когда существует файл или каталог; false в противном случае
     */
    public boolean exists() {
        return this.file.exists();
    }

    /**
     * Создает новый путь к файлу из родительского абстрактного пути и строки дочернего пути.
     *
     * @param var1 Строка имени дочернего пути (имя файла)
     */
    public ain concat(String var1) {
        return new FileControl(this.file, var1);
    }

    /**
     * Получить имя файла дирректории
     *
     * @return имя файла / дирректории
     */
    public String getName() {
        return this.file.getName();
    }

    /**
     * Получить радительскую папку
     *
     * @return ссылка на файл
     */
    public ain getParentFile() {
        return new FileControl(this.file.getParentFile());
    }

    /**
     * Это дирректория
     *
     * @return true, дирректория; false в противном случае
     */
    public boolean isDir() {
        return this.file.isDirectory();
    }

    /**
     * Получить список файлов и папок в каталоге
     *
     * @return список файлов и папок
     */
    public ain[] getChildren() {
        //Список файлов и папок
        File[] var1 = this.file.listFiles();
        ain[] listFile = new ain[var1.length];

        for (int i = 0; i < var1.length; ++i) {
            listFile[i] = new FileControl(var1[i]);
        }

        return listFile;
    }

    public ain[] a(final FY var1) {
        File[] var2 = this.file.listFiles(new FilenameFilter() {
            public boolean accept(File var1x, String var2) {
                return var1.a(FileControl.this, var2);
            }
        });
        ain[] var3 = new ain[var2.length];

        for (int var4 = 0; var4 < var2.length; ++var4) {
            var3[var4] = new FileControl(var2[var4]);
        }

        return var3;
    }

    public void mkdirs() {
        this.file.mkdirs();
    }

    public InputStream getInputStream() throws FileNotFoundException {
        // logger.debug("openning " + this.file);
        return new FileInputStream(this.file);
    }

    public PrintWriter getPrintWriter() throws FileNotFoundException {
        return new PrintWriter(new BufferedOutputStream(new FileOutputStream(this.file)));
    }

    public OutputStream getOutputStream() throws FileNotFoundException {
        this.getParentFile().mkdirs();
        return new FileOutputStream(this.file);
    }

    public boolean delete() {
        return this.file.delete();
    }

    /**
     * @return
     */
    public String getPath() {
        return this.file.getPath();
    }

    public boolean renameTo(ain var1) {
        return this.file.renameTo(((FileControl) var1).file);
    }

    /**
     * @return
     */
    public ain getAbsoluteFile() {
        return new FileControl(this.file.getAbsoluteFile());
    }

    public long lastModified() {
        return this.file.lastModified();
    }

    public File getFile() {
        return this.file;
    }

    public String toString() {
        return this.file == null ? "null" : this.file.toString();
    }

    public int hashCode() {
        return this.file.hashCode();
    }

    public boolean equals(Object var1) {
        if (var1 == null) {
            return false;
        } else if (var1.getClass() != FileControl.class) {
            return false;
        } else {
            FileControl var2 = (FileControl) var1;
            return this.file.equals(var2.file);
        }
    }

    public ByteBuffer BI() {
        return ByteBuffer.wrap(this.getByte());
    }

    /**
     * Получить массив byte
     *
     * @return
     */
    public byte[] getByte() {
        return Hm.getByte(this.file);
    }

    /**
     * Записать массив byte
     *
     * @param var1 массив byte
     */
    public void setByte(byte[] var1) {
        Hm.setByte(this.file, var1);
    }

    public long length() {
        return this.file.length();
    }
}
