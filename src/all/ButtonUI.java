package all;

import sun.swing.SwingUtilities2;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicButtonListener;
import javax.swing.plaf.basic.BasicButtonUI;
import java.awt.*;

/**
 * ButtonUI
 */
public class ButtonUI extends BasicButtonUI implements aIh {
    private ComponentManager componentManager;

    public ButtonUI(JButton var1) {
        var1.setRolloverEnabled(true);
        var1.setOpaque(false);
        this.componentManager = new UY("button", var1);
    }

    public static ComponentUI createUI(JComponent c) {
        return new ButtonUI((JButton) c);
    }

    protected BasicButtonListener createButtonListener(AbstractButton var1) {
        return new ButtonUIListener(var1, this.componentManager);
    }

    public void update(Graphics var1, JComponent var2) {
        RM.a((aIh) this, (Graphics) var1, (AbstractButton) ((AbstractButton) var2));
        this.paint(var1, var2);
    }

    protected void paintText(Graphics var1, JComponent var2, Rectangle var3, String var4) {
        FontMetrics var5 = SwingUtilities2.getFontMetrics(var2, var1);
        int var6 = ((AbstractButton) var2).getDisplayedMnemonicIndex();
        PropertiesUiFromCss var7 = PropertiesUiFromCss.f(var2);
        if (var7 == null) {
            super.paintText(var1, var2, var3, var4);
        } else {
            var1.setColor(var7.getColor());
            SwingUtilities2.drawStringUnderlineCharAt(var2, var1, var4, var6, var3.x + this.getTextShiftOffset(), var3.y + var5.getAscent() + this.getTextShiftOffset());
        }
    }

    public Dimension getPreferredSize(JComponent var1) {
        RM.a((aIh) this, (AbstractButton) ((AbstractButton) var1));
        return this.wy().c(var1, super.getPreferredSize(var1));
    }

    public Dimension getMinimumSize(JComponent var1) {
        RM.a((aIh) this, (AbstractButton) ((AbstractButton) var1));
        return this.wy().b(var1, super.getMinimumSize(var1));
    }

    public Dimension getMaximumSize(JComponent var1) {
        RM.a((aIh) this, (AbstractButton) ((AbstractButton) var1));
        return this.wy().a(var1, super.getMaximumSize(var1));
    }

    public ComponentManager wy() {
        return this.componentManager;
    }

    /**
     * TODO похлже это public pv wy()
     *
     * @return Графический элемент
     */
    @Override
    public aeK wz() {
        return this.componentManager;
    }
}
