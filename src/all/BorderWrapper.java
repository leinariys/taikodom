package all;

import javax.swing.border.Border;
import javax.swing.plaf.InsetsUIResource;
import java.awt.*;
import java.awt.image.ImageObserver;

public class BorderWrapper implements Border {
    public static Insets duz = new InsetsUIResource(0, 0, 0, 0);
    private static BorderWrapper currentInstance;
    private static jM_q duB;
    private static jM_q duC;
    private static jM_q duD;
    private static jM_q duE;

    static {
        duB = new jM_q(new Tu(0.0F, Tu.a.ejc), new Tu(0.0F, Tu.a.ejc));
        duC = new jM_q(new Tu(0.0F, Tu.a.ejc), new Tu(0.0F, Tu.a.ejc));
        duD = new jM_q(new Tu(100.0F, Tu.a.ejc), new Tu(0.0F, Tu.a.ejc));
        duE = new jM_q(new Tu(0.0F, Tu.a.ejc), new Tu(100.0F, Tu.a.ejc));
    }

    public static BorderWrapper getInstance() {
        if (currentInstance == null) {
            currentInstance = new BorderWrapper();
        }

        return currentInstance;
    }


    public static Border getTextFieldBorder() {
        return new BorderWrapper();
    }

    ////Вызывается когда в class BaseUItegXML компонет добовляется в рут панель var6.add(var4);
    public Insets getBorderInsets(Component var1) {
        PropertiesUiFromCss var2 = PropertiesUiFromCss.g(var1);
        if (var2 == null) {
            return new Insets(0, 0, 0, 0);
        } else {
            int width = var1.getWidth();
            int height = var1.getHeight();
            Insets var5 = this.a(var2, var1);
            var5.left += var2.atK().jp((float) width);
            var5.right += var2.atL().jp((float) width);
            var5.top += var2.atM().jp((float) height);
            var5.bottom += var2.atJ().jp((float) height);
            return var5;
        }
    }

    public Insets a(PropertiesUiFromCss var1, Component var2) {
        int width = var2.getWidth();//Размер экрана
        int height = var2.getHeight();
        if (var1 == null) {
            return new Insets(0, 0, 0, 0);
        } else {
            int var5 = var1.atC().jp((float) width);
            int var6 = var1.atD().jp((float) width);
            int var7 = var1.atE().jp((float) height);
            int var8 = var1.atB().jp((float) height);
            int var9 = var5;
            int var10 = var6;
            int var11 = var7;
            int var12 = var8;
            if ("compound".equals(var1.getBorderStyle())) {
                Image var13 = var1.atv();
                Image var14 = var1.atw();
                Image var15 = var1.att();
                Image var16 = var1.ato();
                Image var17 = var1.atq();
                Image var18 = var1.atl();
                Image var19 = var1.atm();
                Image var20 = var1.ati();
                int var21 = 0;
                int var22 = 0;
                int var23 = 0;
                if (var13 != null) {
                    var21 = var13.getHeight(var2);
                }

                if (var15 != null) {
                    var22 = var15.getHeight(var2);
                }

                if (var14 != null) {
                    var23 = var14.getHeight(var2);
                }

                int var24 = Math.max(var21, Math.max(var22, var23));
                int var25 = 0;
                int var26 = 0;
                int var27 = 0;
                if (var18 != null) {
                    var25 = var18.getHeight(var2);
                }

                if (var20 != null) {
                    var26 = var20.getHeight(var2);
                }

                if (var19 != null) {
                    var27 = var19.getHeight(var2);
                }

                int var28 = Math.max(var25, Math.max(var26, var27));
                int var29 = 0;
                int var30 = 0;
                if (var16 != null) {
                    var29 = var16.getWidth(var2);
                }

                if (var17 != null) {
                    var30 = var17.getWidth(var2);
                }

                var9 = var5 + var29;
                var10 = var6 + var30;
                var11 = var7 + var24;
                var12 = var8 + var28;
            }

            return new Insets(var11, var9, var12, var10);
        }
    }

    public boolean isBorderOpaque() {
        return false;
    }

    public void paintBorder(Component var1, Graphics var2, int var3, int var4, int var5, int var6) {
        PropertiesUiFromCss var7 = PropertiesUiFromCss.g(var1);
        if (var7 != null) {
            if ("compound".equals(var7.getBorderStyle())) {
                Image var8 = var7.atv();
                Image var9 = var7.att();
                Image var10 = var7.atw();
                Image var11 = var7.ato();
                Image var12 = var7.atq();
                Image var13 = var7.atl();
                Image var14 = var7.ati();
                Image var15 = var7.atm();
                int var16 = var1.getWidth();
                int var17 = var1.getHeight();
                int var18 = var7.atC().jp((float) var16);
                int var19 = var7.atD().jp((float) var16);
                int var20 = var7.atE().jp((float) var17);
                int var21 = var7.atB().jp((float) var17);
                int var22 = var5 - var18 - var19;
                int var23 = var6 - var20 - var21;
                int var24 = var3 + var18;
                int var25 = var4 + var20;
                var2.translate(var24, var25);
                int var26 = 0;
                int var27 = 0;
                int var28 = 0;
                int var29 = 0;
                int var30 = 0;
                if (var8 != null) {
                    var26 = var8.getWidth(var1);
                    var28 = var8.getHeight(var1);
                }

                if (var9 != null) {
                    var29 = var9.getHeight(var1);
                }

                if (var10 != null) {
                    var27 = var10.getWidth(var1);
                    var30 = var10.getHeight(var1);
                }

                int var31 = Math.max(var28, Math.max(var29, var30));
                int var32 = 0;
                int var33 = 0;
                int var34 = 0;
                int var35 = 0;
                int var36 = 0;
                if (var13 != null) {
                    var32 = var13.getWidth(var1);
                    var34 = var13.getHeight(var1);
                }

                if (var14 != null) {
                    var35 = var14.getHeight(var1);
                }

                if (var15 != null) {
                    var33 = var15.getWidth(var1);
                    var36 = var15.getHeight(var1);
                }

                int var37 = Math.max(var34, Math.max(var35, var36));
                int var38 = 0;
                int var39 = 0;
                if (var11 != null) {
                    var38 = var11.getWidth(var1);
                }

                if (var12 != null) {
                    var39 = var12.getWidth(var1);
                }

                Color var40 = var7.atn();
                if (var8 != null) {
                    var2.drawImage(var8, 0, var31 - var28, var26, var31, 0, 0, var26, var28, var40, var1);
                }

                if (var9 != null) {
                    this.a(var2, var9, var7.atu(), duB, var26, var31 - var29, var22 - var27, var31, var40, var1);
                }

                if (var10 != null) {
                    var2.drawImage(var10, var22 - var27, var31 - var30, var22, var31, 0, 0, var27, var30, var40, var1);
                }

                if (var11 != null) {
                    this.a(var2, var11, var7.atp(), duC, 0, var31, var38, var23 - var37, var40, var1);
                }

                if (var12 != null) {
                    this.a(var2, var12, var7.atr(), duD, var22 - var39, var31, var22, var23 - var37, var40, var1);
                }

                if (var13 != null) {
                    var2.drawImage(var13, 0, var23 - var37, var32, var23, 0, 0, var32, var34, var40, var1);
                }

                if (var14 != null) {
                    this.a(var2, var14, var7.atk(), duE, var32, var23 - var37, var22 - var33, var23, var40, var1);
                }

                if (var15 != null) {
                    var2.drawImage(var15, var22 - var33, var23 - var37, var22, var23, 0, 0, var33, var36, var40, var1);
                }

                var2.translate(-var24, -var25);
            }

        }
    }

    private void a(Graphics var1, Image var2, xo var3, jM_q var4, int var5, int var6, int var7, int var8, Color var9, ImageObserver var10) {
        RM.a(var1, var2, var3, var4, var5, var6, var7, var8, var9, var10);
    }
}
