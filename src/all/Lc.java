package all;

import com.hoplon.geometry.Vec3f;

public abstract class Lc {
    protected final Kt stack = Kt.bcE();

    public abstract void getAabb(xf_g var1, Vec3f var2, Vec3f var3);

    public void a(Vec3f var1, float[] var2) {
        this.stack.bcF();

        try {
            Vec3f var3 = (Vec3f) this.stack.bcH().get();
            xf_g var4 = (xf_g) this.stack.bcJ().get();
            var4.setIdentity();
            Vec3f var5 = (Vec3f) this.stack.bcH().get();
            Vec3f var6 = (Vec3f) this.stack.bcH().get();
            this.getAabb(var4, var5, var6);
            var3.sub(var6, var5);
            var2[0] = var3.length() * 0.5F;
            var3.add(var5, var6);
            var1.scale(0.5F, var3);
        } finally {
            this.stack.bcG();
        }

    }

    public float beU() {
        this.stack.bcH().push();

        float var4;
        try {
            Vec3f var1 = (Vec3f) this.stack.bcH().get();
            float[] var2 = new float[1];
            this.a(var1, var2);
            var2[0] += var1.length();
            var4 = var2[0];
        } finally {
            this.stack.bcH().pop();
        }

        return var4;
    }

    public void a(xf_g var1, Vec3f var2, Vec3f var3, float var4, Vec3f var5, Vec3f var6) {
        this.stack.bcH().push();

        try {
            this.getAabb(var1, var5, var6);
            float var7 = var6.x;
            float var8 = var6.y;
            float var9 = var6.z;
            float var10 = var5.x;
            float var11 = var5.y;
            float var12 = var5.z;
            Vec3f var13 = this.stack.bcH().ac(var2);
            var13.scale(var4);
            if (var13.x > 0.0F) {
                var7 += var13.x;
            } else {
                var10 += var13.x;
            }

            if (var13.y > 0.0F) {
                var8 += var13.y;
            } else {
                var11 += var13.y;
            }

            if (var13.z > 0.0F) {
                var9 += var13.z;
            } else {
                var12 += var13.z;
            }

            float var14 = var3.length() * this.beU() * var4;
            Vec3f var15 = this.stack.bcH().h(var14, var14, var14);
            var5.set(var10, var11, var12);
            var6.set(var7, var8, var9);
            var5.sub(var15);
            var6.add(var15);
        } finally {
            this.stack.bcH().pop();
        }

    }

    public abstract Vec3f getLocalScaling();

    /*
       public boolean aiE() {
          return this.arV().aiE();
       }

       public boolean aiF() {
          return this.arV().aiF();
       }

       public boolean aiG() {
          return this.arV().aiG();
       }

       public boolean isCompound() {
          return this.arV().isCompound();
       }

       public boolean isInfinite() {
          return this.arV().isInfinite();
       }

       public abstract tK arV();
    */
    public abstract void setLocalScaling(Vec3f var1);

    public abstract void calculateLocalInertia(float var1, Vec3f var2);

    public abstract String getName();

    public abstract float getMargin();

    public abstract void setMargin(float var1);
}
