package all;

import org.w3c.dom.css.CSSStyleDeclaration;

public interface RC extends asi {
    ahv Vl();

    String getTextContent();

    boolean hasAttribute(String var1);

    String getAttribute(String var1);

    void setAttribute(String var1, String var2);

    CSSStyleDeclaration Vh();
}
