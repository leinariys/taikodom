package all;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Vv_q {
    private Map esV = Collections.synchronizedMap(new HashMap());
    private Map esW = Collections.synchronizedMap(new HashMap());

    public aDR a(aDR var1, se_q var2, boolean var3) {
        var1.d(var2);
        this.esW.put(var2, var1);
        var1.hge = var3;
        return var1;
    }

    public aDR f(b var1) {
        if (var1 == null) {
            return null;
        } else {
            aDR var2 = (aDR) this.esV.get(var1);
            if (var2 == null) {
                synchronized (this) {
                    if (var2 == null) {
                        var2 = new aDR(var1, (arL) null);
                        this.esV.put(var1, var2);
                    }
                }
            }

            return var2;
        }
    }

    public aDR g(b var1) throws InterruptedException {
        aDR var2 = (aDR) this.esV.remove(var1);
        if (var2 != null) {
            var2.cleanUp();
            this.esW.remove(var2.hDb);
        }

        return var2;
    }

    public aDR d(se_q var1) {
        return (aDR) this.esW.get(var1);
    }

    public int bAZ() {
        return this.esV.size();
    }

    public Set bBa() {
        return this.esV.isEmpty() ? aKU.dgI() : aKU.v(this.esV.values());
    }
}
