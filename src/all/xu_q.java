package all;

import java.io.EOFException;
import java.io.Serializable;

public abstract class xu_q implements Serializable {

    public static int TRANSPORT_COMMAND = 0;
    public static int TRANSPORT_RESPONSE = 1;
    public static int BROADCAST_COMMAND = 2;
    public static int BROADCAST_RESPONSE = 3;
    private final ala out;
    private final aMf in;
    private int messageId;
    private int threadId = 0;
    private transient long receivedTimeMilis;
    private boolean finished;
    private boolean hasWritten;

    protected xu_q() {
        this.out = new ala(128);
        this.in = null;
    }

    public xu_q(aMf var1) throws EOFException {
        this.messageId = var1.readInt();
        if (var1.readBoolean()) {
            this.threadId = this.messageId;
        } else {
            this.threadId = var1.readInt();
        }

        this.in = var1;
        this.out = null;
    }

    public abstract int getType();

    public final int getMessageId() {
        return this.messageId;
    }

    public final void setMessageId(int var1) {
        if (this.hasWritten) {
            throw new IllegalStateException("Cannot change message id after writing.");
        } else {
            this.messageId = var1;
        }
    }

    public final int getThreadId() {
        return this.threadId;
    }

    public final void setThreadId(int var1) {
        if (this.hasWritten) {
            throw new IllegalStateException("Cannot change thread id after writing.");
        } else {
            this.threadId = var1;
        }
    }

    public final boolean isBlocking() {
        return this.getThreadId() != 0;
    }

    public final long receivedTimeMilis() {
        return this.receivedTimeMilis;
    }

    public final void setReceivedTimeMilis(long var1) {
        this.receivedTimeMilis = var1;
    }

    public final aMf getInputStream() {
        return this.in;
    }

    public final ala getOutputStream() {
        this.hasWritten = true;
        if (this.getType() > 3) {
            throw new IllegalArgumentException("Invalid transport message type (bigger than 3)");
        } else {
            this.out.writeBits(2, this.getType());
            this.out.writeInt(this.getMessageId());
            if (this.getMessageId() == this.getThreadId()) {
                this.out.writeBoolean(true);
            } else {
                this.out.writeBoolean(false);
                this.out.writeInt(this.getThreadId());
            }

            return this.out;
        }
    }

    public final byte[] toByteArray() {
        this.finish();
        return this.out.toByteArray();
    }

    public final byte[] getOutBuffer() {
        this.finish();
        return this.out.chG();
    }

    private void finish() {
        if (!this.finished) {
            this.finished = true;
        }
    }

    public final int getOutLength() {
        return this.out.getCount();
    }

    public String toString() {
        StringBuilder var1 = (new StringBuilder(this.getClass().getName())).append(" ").append(this.messageId).append(" ").append(this.threadId);
        if (this.out != null) {
            var1.append("\r\n").append(this.out);
        } else if (this.in != null) {
            var1.append("\r\n").append(this.in);
        }

        return var1.toString();
    }
}
