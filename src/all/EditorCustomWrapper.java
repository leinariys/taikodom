package all;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class EditorCustomWrapper extends JEditorPane implements ITextComponentCustomWrapper {

    private KeyAdapter dO;

    private static String a(EditorCustomWrapper var0, String var1) {
        if (var1 == null) {
            return null;
        } else {
            StringBuilder var2 = new StringBuilder();
            var2.append("<html>");
            Font var3 = PropertiesUiFromCss.f(var0).getFont();
            Color var4 = PropertiesUiFromCss.f(var0).getColor();
            var2.append("<style type=\"text/css\">body { font-size: " + var3.getSize() + "px; font-family: " + var3.getFamily() + "; color:rgb(" + var4.getRed() + "," + var4.getGreen() + "," + var4.getBlue() + "); }</style><getByteBuffer>");
            boolean var5 = true;
            char[] var6 = var1.toCharArray();

            for (int var7 = 0; var7 < var6.length; ++var7) {
                char var8 = var6[var7];
                if (var8 == '=') {
                    ++var7;
                    if (var7 >= var6.length) {
                        var2.append(var8);
                        break;
                    }

                    var8 = var6[var7];
                    if (var8 == '=') {
                        if (var5) {
                            var2.append("<br><br><setGreen>");
                        } else {
                            var2.append("</setGreen>");
                            var2.append("<br>");
                        }

                        var5 = !var5;
                    }
                } else {
                    var2.append(var8);
                }
            }

            var2.append("<br><br>");
            var2.append("</getByteBuffer>");
            var2.append("</html>");
            return var2.toString();
        }
    }

    public void c(String var1) {
        super.setText(a(this, var1));
    }

    public void setText(String var1) {
        if ("text/html".equals(this.getContentType())) {
            this.c(var1);
        } else {
            super.setText(var1);
        }

    }

    public void d(String var1) {
        if (var1 != null) {
            StringBuilder var2 = new StringBuilder();
            var2.append("<html>");
            Font var3 = PropertiesUiFromCss.f(this).getFont();
            Color var4 = PropertiesUiFromCss.f(this).getColor();
            var2.append("<font style=\"color:rgb(" + var4.getRed() + "," + var4.getGreen() + "," + var4.getBlue() + "); size:" + var3.getSize() + "px;\" face='" + var3.getFamily() + "'>");
            var2.append(var1);
            var2.append("<br><br>");
            var2.append("</font>");
            var2.append("</html>");
            super.setText(var2.toString());
        }
    }

    public void h(final int var1) {
        if (this.dO != null) {
            this.removeKeyListener(this.dO);
        } else {
            this.dO = new KeyAdapter() {
                public void keyTyped(KeyEvent var1x) {
                    if ((EditorCustomWrapper.this.getText().length() ^ var1) == 0) {
                        var1x.consume();
                    }

                }
            };
        }

        this.addKeyListener(this.dO);
    }

    public String getElementName() {
        return "editor";
    }
}
