package all;

import gnu.trove.THashSet;
import gnu.trove.TObjectProcedure;

import java.io.IOException;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectableChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Приём входящих сообщений
 */
public class agX {
    static LogPrinter logger = LogPrinter.K(agX.class);
    private final Selector fFD = Selector.open();
    private final TObjectProcedure fFI = new TObjectProcedure() {
        @Override
        public boolean execute(Object o) {
            return e((SelectionKey) o);
        }

        public boolean e(SelectionKey var1) {
            try {
                if ((var1.interestOps() & 4) == 0) {
                    ((yV) var1.attachment()).a(var1);
                } else {
                    var1.interestOps(5);
                }
            } catch (Throwable var3) {
                System.out.println("Exception-" + var3);
                agX.this.a(var3, var1);
            }

            return true;
        }
    };
    private Thread thread;
    private ReentrantLock fFz = new ReentrantLock();
    private THashSet fFA = new THashSet();
    private THashSet fFB = new THashSet();
    private LinkedBlockingQueue fFC = new LinkedBlockingQueue();
    private boolean running = false;
    private aBE fFE;
    private int fFF;
    private int fFG;
    private int fFH;

    public agX() throws IOException {
    }

    public boolean isRunning() {
        return this.running;
    }

    public void close() {
        if (this.running) {
            this.running = false;
            this.fFD.wakeup();
            this.thread.interrupt();

            try {
                this.thread.join(1000L);
            } catch (InterruptedException var7) {
                System.out.println("Exception-" + var7);
            }
        }

        try {
            Selector var1 = this.fFD;
            synchronized (this.fFD) {
                Iterator var3 = this.fFD.keys().iterator();

                while (var3.hasNext()) {
                    SelectionKey var2 = (SelectionKey) var3.next();

                    try {
                        var2.channel().close();
                    } catch (Throwable var6) {
                        logger.error(var6.getMessage());
                    }
                }
            }
        } catch (Exception var9) {
            ;
        }

        try {
            this.fFD.close();
        } catch (Exception var5) {
            ;
        }

    }

    public void start() {
        this.thread = new Thread(new Runnable() {
            public void run() {
                agX.this.bXF();
            }
        }, "NIO Pooler Thread");
        this.thread.setDaemon(true);
        this.running = true;
        this.thread.start();
    }

    public void a(final XF_w var1) throws ClosedChannelException {
        if (this.running && Thread.currentThread() != this.thread) {
            this.fFC.add(new Runnable() {
                public void run() {
                    try {
                        synchronized (agX.this.fFD) {
                            agX.this.a(var1);
                        }
                    } catch (IOException var3) {
                        var3.printStackTrace();
                    }

                }
            });
            this.fFD.wakeup();
        } else {
            SelectableChannel var2 = var1.getChannel();
            SelectionKey var3 = var2.register(this.fFD, var1.bgp());
            logger.info("Channel added: " + var3.channel());
            var3.attach(var1);
            var1.a(var3, this);

            try {
                var1.bgu();
            } catch (IOException var7) {
                var7.printStackTrace();

                try {
                    var2.close();
                } catch (IOException var6) {
                    var6.printStackTrace();
                    var3.cancel();
                }
            }
        }

    }

    /**
     * Слушатель порта
     */
    protected void bXF() {
        logger.info("Listening");

        try {
            while (this.running) {
                ++this.fFF;
                this.fFD.select(50L);

                while (this.fFC.size() > 0) {
                    try {
                        Runnable var1 = (Runnable) this.fFC.take();
                        var1.run();
                    } catch (Throwable var10) {
                        var10.printStackTrace();
                    }
                }

                this.fFz.lock();

                try {
                    if (this.fFA.size() > 0) {
                        THashSet var13 = this.fFA;
                        this.fFA = this.fFB;
                        this.fFB = var13;
                        this.fFA.clear();
                    }
                } finally {
                    this.fFz.unlock();
                }

                if (this.fFB.size() > 0) {
                    this.fFG += this.fFB.size();
                    this.fFB.forEach(this.fFI);
                    this.fFB.clear();
                }

                Set var14 = this.fFD.selectedKeys();
                if (var14.size() > 0) {
                    for (Iterator var2 = var14.iterator(); var2.hasNext(); var2.remove()) {
                        SelectionKey var3 = (SelectionKey) var2.next();

                        try {
                            ((XF_w) var3.attachment()).b(var3);
                        } catch (Throwable var9) {
                            this.a(var9, var3);
                        }
                    }
                } else {
                    ++this.fFH;
                }

                if (this.fFE != null) {
                    this.fFE.bpy();
                }
            }
        } catch (Exception var12) {
            var12.printStackTrace();
        }

        this.running = false;
        logger.info("poolCount          = " + this.fFF);
        logger.info("forcedCount        = " + this.fFG);
        logger.info("uselessWakeupCount = " + this.fFH);
    }

    private void a(Throwable var1, SelectionKey var2) {
        if (var1 instanceof IOException) {
            logger.debug("Connection closed. Please do not worry about the following exception.", var1);
            logger.warn("Connection closed due to_q exception: " + var2);
        } else {
            logger.error("Connection closed. Please do not worry about the following exception.", var1);
        }

        try {
            if (var2.isValid()) {
                var2.channel().close();
            }
        } catch (Throwable var11) {
            var11.printStackTrace();
        } finally {
            try {
                ((XF_w) var2.attachment()).bgr();
            } catch (Throwable var10) {
                var10.printStackTrace();
            }

        }

    }

    void d(SelectionKey var1) {
        if (Thread.currentThread() != this.thread) {
            this.fFz.lock();

            try {
                if (this.fFA.add(var1) && this.fFA.size() == 1) {
                    this.fFD.wakeup();
                }
            } finally {
                this.fFz.unlock();
            }
        } else {
            var1.interestOps(5);
        }

    }

    public aBE bXG() {
        return this.fFE;
    }

    public void a(aBE var1) {
        this.fFE = var1;
    }

    public Set bXH() {
        return this.fFD.keys();
    }
}
