package all;

/**
 * Джостик локализация   JoystickButtonEvent
 * class CommandTranslatorAddon
 */
public class OA extends aOG {
    public static adl dMj = new adl("JoystickButtonEvent");
    private int button;
    private boolean aCJ;

    public OA(int var1, boolean var2) {
        this.button = var1;
        this.aCJ = var2;
    }

    public final int getButton() {
        return this.button;
    }

    public final boolean getState() {
        return this.aCJ;
    }

    public final boolean isPressed() {
        return this.aCJ;
    }

    public adl Fp() {
        return dMj;
    }
}
