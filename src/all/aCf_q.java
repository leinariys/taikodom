package all;

import com.hoplon.geometry.Vec3f;

public class aCf_q implements acr {
    private static final int cRb = 32;
    protected final Kt stack = Kt.bcE();
    private akr lL;
    private azD hmM;
    private azD hmN;

    public aCf_q(azD var1, azD var2, akr var3) {
        this.hmM = var1;
        this.hmN = var2;
        this.lL = var3;
    }

    public boolean a(xf_g var1, xf_g var2, xf_g var3, xf_g var4, acr.a var5) {
        this.stack.bcF();

        try {
            MinkowskiSum var6 = new MinkowskiSum(this.hmM, this.hmN);
            MinkowskiSum var7 = var6;
            xf_g var8 = (xf_g) this.stack.bcJ().get();
            xf_g var9 = (xf_g) this.stack.bcJ().get();
            var8.b(var1);
            var8.c(var3);
            var9.b(var2);
            var9.c(var4);
            this.lL.reset();
            var6.l(this.stack.bcJ().d(var8.bFF));
            float var10 = 0.0F;
            Vec3f var11 = (Vec3f) this.stack.bcH().get();
            var11.negate(var8.bFG);
            Vec3f var12 = (Vec3f) this.stack.bcH().get();
            var12.sub(var9.bFG, var8.bFG);
            var12.negate();
            Vec3f var13 = this.stack.bcH().ac(var11);
            Vec3f var14 = (Vec3f) this.stack.bcH().get();
            Vec3f var15 = this.stack.bcH().ac(var6.localGetSupportingVertex(var12));
            var14.sub(var13, var15);
            int var16 = 32;
            Vec3f var17 = this.stack.bcH().h(0.0F, 0.0F, 0.0F);
            Vec3f var10000 = (Vec3f) this.stack.bcH().get();
            float var18 = var14.lengthSquared();
            float var19 = 1.0E-4F;
            Vec3f var20 = (Vec3f) this.stack.bcH().get();
            Vec3f var21 = (Vec3f) this.stack.bcH().get();

            while (var18 > var19 && var16-- != 0) {
                var21.set(var7.localGetSupportingVertex(var14));
                var20.sub(var13, var21);
                float var23 = var14.dot(var20);
                if (var23 > 0.0F) {
                    float var22 = var14.dot(var12);
                    if (var22 >= -1.4210855E-14F) {
                        return false;
                    }

                    var10 -= var23 / var22;
                    var13.scaleAdd(var10, var12, var11);
                    this.lL.reset();
                    var20.sub(var13, var21);
                    var17.set(var14);
                }

                this.lL.p(var20, var13, var21);
                if (this.lL.aK(var14)) {
                    var18 = var14.lengthSquared();
                } else {
                    var18 = 0.0F;
                }
            }

            var5.fkF = var10;
            var5.Oj.set(var17);
        } finally {
            this.stack.bcG();
        }

        return true;
    }
}
