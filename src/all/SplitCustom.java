package all;

import javax.swing.*;
import java.awt.*;
import java.util.Iterator;

public class SplitCustom extends BaseItem {
    private static final LogPrinter logger = LogPrinter.setClass(BaseItemFactory.class);

    protected SplitCustomWrapper o(MapKeyValue var1, Container var2, XmlNode var3) {
        return new SplitCustomWrapper();
    }


    /**
     * Создание базового Component интерфейса
     * Вызывается из class BaseItem
     *
     * @param var1
     * @param var2
     * @param var3
     * @return
     */
    @Override
    protected JComponent CreatComponentCustom(MapKeyValue var1, Container var2, XmlNode var3) {
        SplitCustomWrapper var4 = (SplitCustomWrapper) super.CreateUI(var1, var2, var3);
        this.a(var1, var2, var4, var3);
        return var4;
    }

    private void a(MapKeyValue var1, Container var2, JSplitPane var3, XmlNode var4) {
        int var5 = 0;
        Iterator var7 = var4.getChildrenTag().iterator();

        while (var7.hasNext()) {
            XmlNode var6 = (XmlNode) var7.next();
            BaseItem var8 = var1.getClassBaseUi(var6.getTegName().toLowerCase());
            if (var8 != null) {
                Component var9 = var8.CreateUI(var1, var2, var6);
                switch (var5) {
                    case 0:
                        var3.setLeftComponent(var9);
                        ++var5;
                        break;
                    case 1:
                        var3.setRightComponent(var9);
                        ++var5;
                        break;
                    default:
                        logger.error("Split Pane only supports 2 insides panels");
                }
            } else {
                logger.error("There is test_GLCanvas component named '" + var6.getTegName() + "'");
            }
        }

    }


    protected void a(MapKeyValue var1, SplitCustomWrapper var2, XmlNode var3) {
        super.applyAttribute(var1, (Component) var2, (XmlNode) var3);
        var2.setOrientation("horizontal".equals(var3.getAttribute("orientation")) ? 1 : 0);
        Integer var4 = checkValueAttribut((XmlNode) var3, (String) "dividerLocation", (LogPrinter) logger);
        if (var4 != null) {
            var2.setDividerLocation(var4.intValue());
        }

        var2.setOneTouchExpandable("true".equals(var3.getAttribute("oneTouchExpandable")));
        var2.setContinuousLayout("true".equals(var3.getAttribute("continuousLayout")));
    }

    public static class a extends BaseItem {
        /**
         * Создание базового Component интерфейса
         * Вызывается из class BaseItem
         *
         * @param var1
         * @param var2
         * @param var3
         * @return
         */
        @Override
        protected JComponent CreatComponentCustom(MapKeyValue var1, Container var2, XmlNode var3) {
            return new JLabel();
        }

    }
}
