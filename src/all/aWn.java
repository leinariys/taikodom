package all;

import java.io.Serializable;

public class aWn implements akt_q, Serializable {
    private String _localName;
    private String LN;

    public aWn(String var1, String var2) {
        this._localName = var1;
        this.LN = var2;
    }

    public short getConditionType() {
        return 4;
    }

    public String getNamespaceURI() {
        return null;
    }

    public String getLocalName() {
        return this._localName;
    }

    public boolean getSpecified() {
        return true;
    }

    public String getValue() {
        return this.LN;
    }

    public String toString() {
        return this.getValue() != null ? "[" + this.getLocalName() + "=\"" + this.getValue() + "\"]" : "[" + this.getLocalName() + "]";
    }
}
