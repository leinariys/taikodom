package all;

import javax.swing.*;
import java.awt.*;

public class Dc implements Icon {
    private Icon icon;

    public Dc(Icon var1) {
        this.icon = var1;
    }

    public int getIconHeight() {
        return this.icon.getIconHeight();
    }

    public int getIconWidth() {
        return this.icon.getIconWidth();
    }

    public void paintIcon(Component var1, Graphics var2, int var3, int var4) {
        this.icon.paintIcon(var1, var2, var3, var4);
    }
}
