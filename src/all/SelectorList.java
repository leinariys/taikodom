package all;

//package org.w3c.css.sac;
public interface SelectorList {
    int getLength();

    ISelector item(int var1);
}
