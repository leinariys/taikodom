package all;

/**
 * class MusicAddon
 */
public enum agH implements aeO {
    fyN("avatar_creation"),
    fyO("inside_station"),
    fyP("login_screen"),
    fyQ("habitat_commercial_ambience", true);

    public static String fyR = "data/audio/ambiences/ambiences.pro";
    public static String fyS = "data/audio/music/playlist.pro";
    private String handle;
    private String file;

    private agH(String var3) {
        this.handle = var3;
        this.file = "data/audio/music/playlist.pro";
    }

    private agH(String var3, boolean var4) {
        this.handle = var3;
        if (var4) {
            this.file = "data/audio/ambiences/ambiences.pro";
        } else {
            this.file = "data/audio/music/playlist.pro";
        }

    }

    public String getHandle() {
        return this.handle;
    }

    public String getFile() {
        return this.file;
    }
}
