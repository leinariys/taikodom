package all;

import java.util.HashMap;
import java.util.Map;

/**
 * Соотношение клавиш клавиатуры  class Kx
 * class CommandTranslatorAddon
 */
public class Nb {
    private static final Map dCX = new HashMap();
    private static final Map dCY = new HashMap();

    static {
        L(8, 8);
        L(9, 9);
        L(12, 12);
        L(10, 13);
        L(19, 19);
        L(27, 27);
        L(32, 32);
        L(517, 33);
        L(152, 34);
        L(515, 36);
        L(150, 38);
        L(222, 39);
        L(519, 40);
        L(522, 41);
        L(151, 42);
        L(521, 43);
        L(44, 44);
        L(45, 45);
        L(46, 46);
        L(47, 47);
        L(48, 48);
        L(49, 49);
        L(50, 50);
        L(51, 51);
        L(52, 52);
        L(53, 53);
        L(54, 54);
        L(55, 55);
        L(56, 56);
        L(57, 57);
        L(513, 58);
        L(59, 59);
        L(153, 60);
        L(61, 61);
        L(160, 62);
        L(512, 64);
        L(91, 91);
        L(92, 92);
        L(93, 93);
        L(523, 95);
        L(192, 96);
        L(65, 97);
        L(66, 98);
        L(67, 99);
        L(68, 100);
        L(69, 101);
        L(70, 102);
        L(71, 103);
        L(72, 104);
        L(73, 105);
        L(74, 106);
        L(75, 107);
        L(76, 108);
        L(77, 109);
        L(78, 110);
        L(79, 111);
        L(80, 112);
        L(81, 113);
        L(82, 114);
        L(83, 115);
        L(84, 116);
        L(85, 117);
        L(86, 118);
        L(87, 119);
        L(88, 120);
        L(89, 121);
        L(90, 122);
        L(127, 127);
        L(96, 256);
        L(97, 257);
        L(98, 258);
        L(99, 259);
        L(100, 260);
        L(101, 261);
        L(102, 262);
        L(103, 263);
        L(104, 264);
        L(105, 265);
        L(46, 266);
        L(111, 267);
        L(106, 268);
        L(109, 269);
        L(107, 270);
        L(38, 273);
        L(40, 274);
        L(39, 275);
        L(37, 276);
        L(155, 277);
        L(36, 278);
        L(35, 279);
        L(33, 280);
        L(34, 281);
        L(112, 282);
        L(113, 283);
        L(114, 284);
        L(115, 285);
        L(116, 286);
        L(117, 287);
        L(118, 288);
        L(119, 289);
        L(120, 290);
        L(121, 291);
        L(122, 292);
        L(123, 293);
        L(61440, 294);
        L(61441, 295);
        L(61442, 296);
        L(144, 300);
        L(20, 301);
        L(145, 302);
        L(16, 304);
        L(17, 306);
        L(18, 308);
        L(157, 310);
        L(31, 313);
        L(65312, 314);
        L(156, 315);
        L(154, 316);
        L(19, 318);
        L(525, 319);
        L(516, 321);
    }

    private static void L(int var0, int var1) {
        dCX.put(var0, var1);
        dCY.put(var1, var0);
    }

    public static int lP(int var0) {
        Integer var1 = (Integer) dCX.get(var0);
        return var1 == null ? var0 : var1.intValue();
    }

    public static int cb(int var0) {
        Integer var1 = (Integer) dCY.get(var0);
        return var1 == null ? var0 : var1.intValue();
    }

    public static int lQ(int var0) {
        int var1 = 0;
        if ((var0 & 512) != 0) {
            var1 |= 256;
        }

        if ((var0 & 128) != 0) {
            var1 |= 64;
        }

        if ((var0 & 64) != 0) {
            var1 |= 1;
        }

        if ((var0 & 32) != 0) {
            var1 |= 32;
        }

        return var1;
    }

    public static int lR(int var0) {
        int var1 = 0;
        if ((var0 & 256) != 0) {
            var1 |= 512;
        }

        if ((var0 & 64) != 0) {
            var1 |= 128;
        }

        if ((var0 & 1) != 0) {
            var1 |= 64;
        }

        if ((var0 & 32) != 0) {
            var1 |= 32;
        }

        return var1;
    }
}
