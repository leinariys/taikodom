package all;

//package org.w3c.css.sac;
//DocumentHandler
public interface DocumentHandler {
    void startDocument(InputSource var1);

    void endDocument(InputSource var1);

    void comment(String var1);

    void ignorableAtRule(String var1);

    void namespaceDeclaration(String var1, String var2);

    void importStyle(String var1, SACMediaList var2, String var3);

    void startMedia(SACMediaList var1);

    void endMedia(SACMediaList var1);

    void startPage(String var1, String var2);

    void endPage(String var1, String var2);

    void startFontFace();

    void endFontFace();

    void startSelector(SelectorList var1);

    void endSelector(SelectorList var1);

    void property(String var1, LexicalUnit var2, boolean var3);
}
