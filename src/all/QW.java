package all;

import java.util.ArrayList;

public class QW {
    private Class dXq;
    private ArrayList list = new ArrayList();

    public QW(Class var1) {
        this.dXq = var1;
    }

    private Object create() {
        try {
            return this.dXq.newInstance();
        } catch (InstantiationException var2) {
            throw new IllegalStateException(var2);
        } catch (IllegalAccessException var3) {
            throw new IllegalStateException(var3);
        }
    }

    public Object get() {
        return this.list.size() > 0 ? this.list.remove(this.list.size() - 1) : this.create();
    }

    public void release(Object var1) {
        this.list.add(var1);
    }
}
