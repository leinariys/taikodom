package all;

import javax.swing.*;
import java.awt.*;

public final class RadiobuttonCustom extends AbstractButtonCustom {
    /**
     * Создание базового Component интерфейса
     * Вызывается из class BaseItem
     *
     * @param var1
     * @param var2
     * @param var3
     * @return
     */
    @Override
    protected JComponent CreatComponentCustom(MapKeyValue var1, Container var2, XmlNode var3) {
        return new JRadioButton();
    }

    @Override
    protected AbstractButton f(MapKeyValue var1, Container var2, XmlNode var3) {
        return null;
    }

}
