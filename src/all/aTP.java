package all;

import org.w3c.dom.css.CSSPrimitiveValue;
import org.w3c.dom.css.Rect;

import java.io.Serializable;

public class aTP implements Serializable, Rect {
    private CSSPrimitiveValue iSW;
    private CSSPrimitiveValue iSX;
    private CSSPrimitiveValue iSY;
    private CSSPrimitiveValue iSZ;

    public aTP(LexicalUnit var1) {
        this.iSW = new Bj(var1, true);
        LexicalUnit var2 = var1.getNextLexicalUnit();
        var2 = var2.getNextLexicalUnit();
        this.iSX = new Bj(var2, true);
        var2 = var2.getNextLexicalUnit();
        var2 = var2.getNextLexicalUnit();
        this.iSY = new Bj(var2, true);
        var2 = var2.getNextLexicalUnit();
        var2 = var2.getNextLexicalUnit();
        this.iSZ = new Bj(var2, true);
    }

    public CSSPrimitiveValue getTop() {
        return this.iSX;
    }

    public CSSPrimitiveValue getRight() {
        return this.iSY;
    }

    public CSSPrimitiveValue getBottom() {
        return this.iSZ;
    }

    public CSSPrimitiveValue getLeft() {
        return this.iSW;
    }

    public String toString() {
        return "rect(" + this.iSW.toString() + ", " + this.iSX.toString() + ", " + this.iSY.toString() + ", " + this.iSZ.toString() + ")";
    }
}
