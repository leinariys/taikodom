package all;

import javax.swing.plaf.ComponentUI;
import java.awt.*;

public class HBoxCustomWrapper extends BoxCustomWrapper {
    private static final long serialVersionUID = 1L;

    protected Dimension b(int var1, int var2) {
        int var3 = this.getComponentCount();
        int var4 = 0;
        int var5 = 0;

        for (int var6 = 0; var6 < var3; ++var6) {
            Component var7 = this.getComponent(var6);
            if (var7.isVisible()) {
                Dimension var8 = this.d(var7, var1, var2);
                var4 += var8.width;
                var5 = Math.max(var5, var8.height);
            }
        }

        return new Dimension(var4, var5);
    }

    protected Dimension a(int var1, int var2) {
        int var3 = this.getComponentCount();
        int var4 = 0;
        int var5 = 0;

        for (int var6 = 0; var6 < var3; ++var6) {
            Component var7 = this.getComponent(var6);
            if (var7.isVisible()) {
                Dimension var8 = this.c(var7, var1, var2);
                var4 += var8.width;
                var5 = Math.max(var5, var8.height);
            }
        }

        return new Dimension(var4, var5);
    }

    public void layout() {
        int var1 = this.getComponentCount();
        PropertiesUiFromCss var2 = PropertiesUiFromCss.a((ComponentUI) this.getUI());
        vI var3 = var2.atR();
        aRU var4 = var2.aty();
        Insets var5 = this.amu();
        Dimension var6 = this.getSize();
        Dimension var7 = new Dimension(var6.width - var5.right - var5.left, var6.height - var5.top - var5.bottom);
        int var8 = this.amv();
        int var9 = var2.ats().getHorizon().jp((float) var7.width);
        int var10 = var9 * (var8 - 1);
        b var11 = new b((float) (var7.width - var10), var1);

        int var12;
        for (var12 = 0; var12 < var1; ++var12) {
            var11.b(var12, this.getComponent(var12), var7.width - var10, var7.height);
        }

        var11.Mq();
        var12 = var4.t(var7.width, (int) var11.ghM + var10);

        for (int var13 = 0; var13 < var1; ++var13) {
            Component var14 = this.getComponent(var13);
            if (var11.bjD[var13]) {
                Dimension var15 = var11.bjC[var13];
                int var16 = var11.ghH[var13];
                int var17 = Math.min(var7.height, var15.height);
                Point var18 = this.a(var14, var7.width, var7.height);
                int var19 = var3.t(var7.height, var17);
                int var20 = var3 == vI.bzJ ? var7.height : var17;
                var14.setBounds(var12 + var18.x + var5.left, var19 + var18.y + var5.top, var16, var20);
                var12 += var16 + var9;
            }
        }

    }

    public String getElementName() {
        return "hbox";
    }
}
