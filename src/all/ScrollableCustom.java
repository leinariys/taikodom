package all;

import javax.swing.*;
import java.awt.*;

/**
 * Базовые элементы интерфейса
 * тег desktoppane JScrollPane ScrollableCustomWrapper
 * class BaseUItegXML ScrollableCustom BaseItemFactory
 */
public class ScrollableCustom extends BaseItemFactory {
    private static final LogPrinter logger = LogPrinter.setClass(ScrollableCustom.class);

    /**
     * Создание базового Component интерфейса
     * Вызывается из class BaseItem
     *
     * @param var1
     * @param var2
     * @param var3
     * @return
     */
    @Override
    protected JComponent CreatComponentCustom(MapKeyValue var1, Container var2, XmlNode var3) {
        return new ScrollableCustomWrapper();
    }

    public ScrollableCustomWrapper CreatChildComponent(MapKeyValue var1, Container var2, XmlNode var3) {
        ScrollableCustomWrapper var4 = (ScrollableCustomWrapper) super.CreatAllComponentCustom(var1, var2, var3);
        String var5 = var3.getAttribute("show-horizontal");
        if ("never".equals(var5)) {
            var4.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        } else if ("always".equals(var5)) {
            var4.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        } else if ("as-needed".equals(var5)) {
            var4.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        }

        var5 = var3.getAttribute("show-vertical");
        if ("never".equals(var5)) {
            var4.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
        } else if ("always".equals(var5)) {
            var4.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        } else if ("as-needed".equals(var5)) {
            var4.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        }
        return var4;
    }

    protected void CreatChildrenZero(MapKeyValue var1, ScrollableCustomWrapper var2, XmlNode var3) {
        if (var3.getChildrenTag().size() > 0) {
            var3 = (XmlNode) var3.getChildrenTag().get(0);
            BaseItem var4 = var1.getClassBaseUi(var3.getTegName());
            if (var4 instanceof RepeaterCustom) {
                logger.error("Container for Repeater must be all Box.");
            }

            if (var4 != null) {
                Component var5 = var4.CreateUI(var1, var2, var3);
                var2.setViewportView(var5);
                if (var5 instanceof TableCustomWrapper) {
                    TableHeaderCustomWrapper.b var6 = new TableHeaderCustomWrapper.b();
                    var2.setColumnHeader(var6);
                }
            } else {
                logger.error("There is test_GLCanvas component named '" + var3.getTegName() + "'");
            }

        }
    }

}
