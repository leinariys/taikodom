package all;

import javax.swing.*;
import javax.swing.JTable.DropLocation;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicTableUI;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import java.awt.*;

/**
 * TableUI
 */
public class TableUI extends BasicTableUI implements aIh {
    private ComponentManager Rp;
    private PanelCustomWrapper fLT;

    public TableUI(JTable var1) {
        this.Rp = new ComponentManager("table", var1);
        var1.setOpaque(false);
        var1.setBackground(new Color(0, 0, 0, 0));
    }

    public static ComponentUI createUI(JComponent var0) {
        return new TableUI((JTable) var0);
    }

    public void installUI(JComponent var1) {
        this.table = (JTable) var1;
        this.rendererPane = new aqV("cell");
        this.fLT = new PanelCustomWrapper() {
            private static final long serialVersionUID = 1L;

            public String getElementName() {
                return "row";
            }
        };
        this.table.add(this.rendererPane);
        this.table.add(this.fLT);
        this.installDefaults();
        this.installListeners();
        this.installKeyboardActions();
    }

    public void paint(Graphics var1, JComponent var2) {
        Rectangle var3 = var1.getClipBounds();
        Rectangle var4 = this.table.getBounds();
        var4.x = var4.y = 0;
        if (this.table.getRowCount() > 0 && this.table.getColumnCount() > 0 && var4.intersects(var3)) {
            boolean var5 = this.table.getComponentOrientation().isLeftToRight();
            Point var6 = var3.getLocation();
            if (!var5) {
                ++var6.x;
            }

            Point var7 = new Point(var3.x + var3.width - (var5 ? 1 : 0), var3.y + var3.height);
            int var8 = this.table.rowAtPoint(var6);
            int var9 = this.table.rowAtPoint(var7);
            if (var8 == -1) {
                var8 = 0;
            }

            if (var9 == -1) {
                var9 = this.table.getRowCount() - 1;
            }

            int var10 = this.table.columnAtPoint(var5 ? var6 : var7);
            int var11 = this.table.columnAtPoint(var5 ? var7 : var6);
            if (var10 == -1) {
                var10 = 0;
            }

            if (var11 == -1) {
                var11 = this.table.getColumnCount() - 1;
            }

            RM.a((aIh) this, (Graphics) var1, (JComponent) var2);
            this.a(var1, var8, var9, var10, var11);
            this.b(var1, var8, var9, var10, var11);
            this.a(var1);
        }
    }

    private void a(Graphics var1, int var2, int var3, int var4, int var5) {
        var1.setColor(this.table.getGridColor());
        Rectangle var6 = this.table.getCellRect(var2, 0, false);
        Rectangle var7 = this.table.getCellRect(var3, this.table.getColumnCount() - 1, false);
        Rectangle var8 = var6.union(var7);

        for (int var9 = var2; var9 <= var3; ++var9) {
            ComponentManager.getCssHolder(this.fLT).q(2, this.table.isRowSelected(var9) ? 2 : 0);
            var8.height = this.table.getRowHeight(var9);
            RM.a(var1, this.fLT, PropertiesUiFromCss.g(this.fLT), var8);
            this.fLT.getBorder().paintBorder(this.fLT, var1, var8.x, var8.y, var8.width, var8.height);
            var8.y += var8.height;
        }

    }

    private void b(Graphics var1, int var2, int var3, int var4, int var5) {
        JTableHeader var6 = this.table.getTableHeader();
        TableColumn var7 = var6 == null ? null : var6.getDraggedColumn();
        TableColumnModel var8 = this.table.getColumnModel();
        int var9 = var8.getColumnMargin();
        Rectangle var10;
        TableColumn var11;
        int var12;
        int var13;
        int var14;
        if (this.table.getComponentOrientation().isLeftToRight()) {
            for (var13 = var2; var13 <= var3; ++var13) {
                var10 = this.table.getCellRect(var13, var4, false);

                for (var14 = var4; var14 <= var5; ++var14) {
                    var11 = var8.getColumn(var14);
                    var12 = var11.getWidth();
                    var10.width = var12 - var9;
                    if (var11 != var7) {
                        this.a(var1, var10, var13, var14);
                    }

                    var10.x += var12;
                }
            }
        } else {
            for (var13 = var2; var13 <= var3; ++var13) {
                var10 = this.table.getCellRect(var13, var4, false);
                var11 = var8.getColumn(var4);
                if (var11 != var7) {
                    var12 = var11.getWidth();
                    var10.width = var12 - var9;
                    this.a(var1, var10, var13, var4);
                }

                for (var14 = var4 + 1; var14 <= var5; ++var14) {
                    var11 = var8.getColumn(var14);
                    var12 = var11.getWidth();
                    var10.width = var12 - var9;
                    var10.x -= var12;
                    if (var11 != var7) {
                        this.a(var1, var10, var13, var14);
                    }
                }
            }
        }

        if (var7 != null) {
            this.a(var1, var2, var3, var7, var6.getDraggedDistance());
        }

        this.rendererPane.removeAll();
    }

    private void a(Graphics var1, Rectangle var2, int var3, int var4) {
        if (this.table.isEditing() && this.table.getEditingRow() == var3 && this.table.getEditingColumn() == var4) {
            Component var7 = this.table.getEditorComponent();
            var7.setBounds(var2);
        } else {
            TableCellRenderer var5 = this.table.getCellRenderer(var3, var4);
            Component var6 = this.table.prepareRenderer(var5, var3, var4);
            this.rendererPane.paintComponent(var1, var6, this.table, var2.x, var2.y, var2.width, var2.height, true);
        }

    }

    private void a(Graphics var1, int var2, int var3, TableColumn var4, int var5) {
        int var6 = this.a(var4);
        Rectangle var7 = this.table.getCellRect(var2, var6, true);
        Rectangle var8 = this.table.getCellRect(var3, var6, true);
        Rectangle var9 = var7.union(var8);
        PropertiesUiFromCss var10 = this.Rp.Vp();
        if (var10 != null) {
            var1.setColor(var10.getColor());
        }

        var1.setColor(new Color(0.4F, 0.4F, 0.4F, 0.4F));
        var1.fillRect(var9.x, var9.y, var9.width, var9.height);
        var9.x += var5;
        var1.setColor(new Color(0.4F, 0.4F, 0.4F, 0.4F));
        var1.fillRect(var9.x, var9.y, var9.width, var9.height);
        if (this.table.getShowVerticalLines()) {
            var1.setColor(this.table.getGridColor());
            int var11 = var9.x;
            int var12 = var9.y;
            int var13 = var11 + var9.width - 1;
            int var14 = var12 + var9.height - 1;
            var1.drawLine(var11 - 1, var12, var11 - 1, var14);
            var1.drawLine(var13, var12, var13, var14);
        }

    }

    private void a(Graphics var1) {
        DropLocation var2 = this.table.getDropLocation();
        if (var2 != null) {
            Color var3 = UIManager.getColor("Table.dropLineColor");
            Color var4 = UIManager.getColor("Table.dropLineShortColor");
            if (var3 != null || var4 != null) {
                Rectangle var5 = this.a(var2);
                int var6;
                int var7;
                if (var5 != null) {
                    var6 = var5.x;
                    var7 = var5.width;
                    if (var3 != null) {
                        this.a(var5, true);
                        var1.setColor(var3);
                        var1.fillRect(var5.x, var5.y, var5.width, var5.height);
                    }

                    if (!var2.isInsertColumn() && var4 != null) {
                        var1.setColor(var4);
                        var1.fillRect(var6, var5.y, var7, var5.height);
                    }
                }

                var5 = this.b(var2);
                if (var5 != null) {
                    var6 = var5.y;
                    var7 = var5.height;
                    if (var3 != null) {
                        this.a(var5, false);
                        var1.setColor(var3);
                        var1.fillRect(var5.x, var5.y, var5.width, var5.height);
                    }

                    if (!var2.isInsertRow() && var4 != null) {
                        var1.setColor(var4);
                        var1.fillRect(var5.x, var6, var5.width, var7);
                    }
                }

            }
        }
    }

    private Rectangle a(DropLocation var1) {
        if (!var1.isInsertRow()) {
            return null;
        } else {
            int var2 = var1.getRow();
            int var3 = var1.getColumn();
            if (var3 >= this.table.getColumnCount()) {
                --var3;
            }

            Rectangle var4 = this.table.getCellRect(var2, var3, true);
            if (var2 >= this.table.getRowCount()) {
                --var2;
                Rectangle var5 = this.table.getCellRect(var2, var3, true);
                var4.y = var5.y + var5.height;
            }

            if (var4.y == 0) {
                var4.y = -1;
            } else {
                var4.y -= 2;
            }

            var4.height = 3;
            return var4;
        }
    }

    private Rectangle b(DropLocation var1) {
        if (!var1.isInsertColumn()) {
            return null;
        } else {
            boolean var2 = this.table.getComponentOrientation().isLeftToRight();
            int var3 = var1.getColumn();
            Rectangle var4 = this.table.getCellRect(var1.getRow(), var3, true);
            if (var3 >= this.table.getColumnCount()) {
                --var3;
                var4 = this.table.getCellRect(var1.getRow(), var3, true);
                if (var2) {
                    var4.x += var4.width;
                }
            } else if (!var2) {
                var4.x += var4.width;
            }

            if (var4.x == 0) {
                var4.x = -1;
            } else {
                var4.x -= 2;
            }

            var4.width = 3;
            return var4;
        }
    }

    private int a(TableColumn var1) {
        TableColumnModel var2 = this.table.getColumnModel();

        for (int var3 = 0; var3 < var2.getColumnCount(); ++var3) {
            if (var2.getColumn(var3) == var1) {
                return var3;
            }
        }

        return -1;
    }

    private Rectangle a(Rectangle var1, boolean var2) {
        if (var1 == null) {
            return var1;
        } else {
            if (var2) {
                var1.x = 0;
                var1.width = this.table.getWidth();
            } else {
                var1.y = 0;
                if (this.table.getRowCount() != 0) {
                    Rectangle var3 = this.table.getCellRect(this.table.getRowCount() - 1, 0, true);
                    var1.height = var3.y + var3.height;
                } else {
                    var1.height = this.table.getHeight();
                }
            }

            return var1;
        }
    }

    public Dimension getPreferredSize(JComponent var1) {
        return this.wy().c(var1, super.getPreferredSize(var1));
    }

    public Dimension getMinimumSize(JComponent var1) {
        return this.wy().b(var1, super.getMinimumSize(var1));
    }

    public Dimension getMaximumSize(JComponent var1) {
        return this.wy().a(var1, super.getMaximumSize(var1));
    }

    public ComponentManager wy() {
        return this.Rp;
    }

    /**
     * TODO похлже это public pv wy()
     *
     * @return Графический элемент
     */
    @Override
    public aeK wz() {
        return this.Rp;
    }
}
