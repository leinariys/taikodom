package all;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Базовые элементы интерфейса
 * тег window JInternalFrame
 * class BaseUItegXML WindowCustom WindowCustomWrapper
 */
public class WindowCustomWrapper extends JInternalFrame implements IContainerCustomWrapper {
    protected List buZ;//Суда потомков надо добавлять
    /**
     * Указывает, должен ли этот диалог быть модальным.
     */
    private boolean gmA;
    private List gmB;
    private boolean gmC;
    private boolean gmD;
    /**
     * Устанавливает, должно ли это окно всегда находиться над другими окнами.
     */
    private boolean gmE;
    /**
     * Анимация
     */
    private aDX SZ;
    private boolean gmF;
    private boolean gmG;
    private lL_q gmH;
    private lL_q gmI;

    public WindowCustomWrapper() {
        this(true);
    }

    public WindowCustomWrapper(boolean var1) {

        this.gmA = false;
        this.buZ = new ArrayList();
        this.gmC = true;
        this.gmD = true;
        this.gmE = false;
        this.SZ = null;
        this.gmF = true;
        this.gmG = true;
        this.setFrameIcon((Icon) null);
        this.setBackground(new Color(0, 0, 0, 0));
        this.setContentPane(new ContentPane());
        this.setTransferHandler(new TransferHandler() {
            private static final long serialVersionUID = 1L;

            public boolean canImport(TransferSupport var1) {
                return false;
            }
        });
        this.fU(false);
        this.setDefaultCloseOperation(1);
        if (var1) {
            this.cpS();
        }
    }

    private void cpS() {
    }

    private synchronized void b(final GJ var1) {
        if (this.SZ != null) {
            this.SZ.kill();
        }

        if (this.gmI == null) {
            this.setAlpha(0);
            this.SZ = new aDX(this, "[0..255] dur 150", kk_q.asS, new GJ() {
                public void a(JComponent var1x) {
                    WindowCustomWrapper.this.fU(true);
                    WindowCustomWrapper.this.SZ = null;
                    if (var1 != null) {
                        var1.a(var1x);
                    }

                }
            });
        } else {
            this.gmI.iT();
            final GJ var2 = this.gmI.iW();
            this.SZ = new aDX(this, this.gmI.iV(), this.gmI.iU(), new GJ() {
                public void a(JComponent var1) {
                    WindowCustomWrapper.this.fU(true);
                    WindowCustomWrapper.this.SZ = null;
                    if (var2 != null) {
                        var2.a(var1);
                    }

                }
            });
        }

    }

    private void c(final GJ var1) {
        if (this.SZ != null) {
            this.SZ.kill();
        }

        if (this.gmH == null) {
            this.SZ = new aDX(this, "[255..0] dur 150", kk_q.asS, new GJ() {
                public void a(JComponent var1x) {
                    WindowCustomWrapper.this.fU(false);
                    WindowCustomWrapper.this.SZ = null;
                    if (var1 != null) {
                        var1.a(var1x);
                    }

                }
            });
        } else {
            this.gmH.iT();
            final GJ var2 = this.gmH.iW();
            this.SZ = new aDX(this, this.gmH.iV(), this.gmH.iU(), new GJ() {
                public void a(JComponent var1) {
                    WindowCustomWrapper.this.fU(true);
                    WindowCustomWrapper.this.SZ = null;
                    if (var2 != null) {
                        var2.a(var1);
                    }

                }
            });
        }

    }

    /**
     * Анимация?
     *
     * @return
     */
    public boolean isAnimating() {
        return this.SZ != null;
    }

    /**
     * Сделать Component видимым/скрытым
     *
     * @param var1 true, чтобы сделать компонент видимым; false для сделать его невидимым
     */
    private void fU(boolean var1) {
        super.setVisible(var1);
    }

    /**
     * Получить Система событий для этого графического Component
     *
     * @return class pv
     */
    public aeK la() {
        return ComponentManager.getCssHolder(this);
    }

    /**
     * Указывает, должен ли этот диалог быть модальным.
     *
     * @return
     */
    public boolean isModal() {
        return this.gmA;
    }

    /**
     * Указывает, должен ли этот диалог быть модальным.
     *
     * @param var1
     */
    public void setModal(boolean var1) {
        if (var1 != this.gmA) {
            if (this.isVisible()) {
                BaseUItegXML.thisClass.a(this, var1);
                this.requestFocus();//ПОлучить фокус ввода
            }
            this.gmF = !var1;
            this.gmG = !var1;
            this.gmA = var1;
        }
    }

    public void fV(boolean var1) {
        if (this.gmD && var1 != this.gmC) {
            JComponent var2 = ((InternalFrameUI) this.ui).getNorthPane();
            MouseListener var3;
            if (var1) {
                if (this.gmB != null && this.gmB.size() > 0) {
                    Iterator var4 = this.gmB.iterator();

                    while (var4.hasNext()) {
                        var3 = (MouseListener) var4.next();
                        var2.addMouseListener(var3);
                    }

                    this.gmB.clear();
                }
            } else {
                if (this.gmB == null) {
                    this.gmB = new ArrayList();
                } else {
                    this.gmB.clear();
                }

                MouseListener[] var6;
                int var5 = (var6 = var2.getMouseListeners()).length;

                for (int var7 = 0; var7 < var5; ++var7) {
                    var3 = var6[var7];
                    this.gmB.add(var3);
                    var2.removeMouseListener(var3);
                }
            }
            this.gmC = var1;
        }
    }

    public boolean cpT() {
        return this.gmC;
    }

    /**
     * Возвращает, является ли это окно всегда над другими окнами.
     *
     * @return
     */
    public boolean isAlwaysOnTop() {
        return this.gmE;
    }

    /**
     * Устанавливает, должно ли это окно всегда находиться над другими окнами.
     *
     * @param var1
     */
    public void setAlwaysOnTop(boolean var1) {
        if (var1 != this.gmE) {
            this.setLayer(var1 ? JLayeredPane.PALETTE_LAYER : JLayeredPane.DEFAULT_LAYER);
            this.gmE = var1;
        }
    }

    /**
     * Показывает или скрывает это окно
     *
     * @param var1
     */
    public void setVisible(boolean var1) {
        this.b(var1, (GJ) null);
    }

    /**
     * Показывает или скрывает это окно
     *
     * @param var1
     * @param var2
     */
    public void b(boolean var1, GJ var2) {
        if (var1 == this.isVisible()) {
            if (this.SZ == null) {
                if (var2 != null) {
                    var2.a(this);
                }

                return;
            }

            this.SZ.cAD();
        }

        if (var1) {
            if (this.gmF) {
                this.b(var2);
            } else {
                this.fU(true);
                if (var2 != null) {
                    var2.a(this);
                }
            }
        }

        if (this.gmA) {
            BaseUItegXML.thisClass.a(this, var1);
            if (var1) {
                this.processComponentEvent(new ComponentEvent(this, 102));
                this.requestFocus();
            } else {
                this.processComponentEvent(new ComponentEvent(this, 103));
            }
        }

        if (!var1) {
            if (this.gmG) {
                this.c(var2);
            } else {
                this.fU(false);
                if (var2 != null) {
                    var2.a(this);
                }
            }
        }
    }

    /**
     * Убить Анимация
     *
     * @param var1
     */
    public void fW(boolean var1) {
        if (this.SZ != null) {
            this.SZ.kill();
        }
        this.setAlpha(255);
        this.fU(var1);
    }

    public void center() {
        if (this.getParent() != null) {
            this.setLocation((this.getParent().getWidth() - this.getWidth()) / 2, (this.getParent().getHeight() - this.getHeight()) / 2);
        }
    }

    public void cpU() {
        this.gmD = false;
        ((InternalFrameUI) this.ui).setNorthPane((JComponent) null);
    }

    public void destroy() {
        this.dispose();
        if (this.getParent() != null) {
            this.getParent().remove(this);
        }

    }

    public JButton findJButton(String var1) {
        Component[] var5;
        int var4 = (var5 = this.getContentPane().getComponents()).length;

        for (int var3 = 0; var3 < var4; ++var3) {
            Component var2 = var5[var3];
            if (var2 instanceof JButton && var1.equals(var2.getName())) {
                return (JButton) var2;
            }

            if (var2 instanceof IContainerCustomWrapper) {
                JButton var6 = ((IContainerCustomWrapper) var2).findJButton(var1);
                if (var6 != null) {
                    return var6;
                }
            }
        }

        return null;
    }

    public ProgressBarCustomWrapper findJProgressBar(String var1) {
        Component[] var5;
        int var4 = (var5 = this.getContentPane().getComponents()).length;

        for (int var3 = 0; var3 < var4; ++var3) {
            Component var2 = var5[var3];
            if (var2 instanceof ProgressBarCustomWrapper && var1.equals(var2.getName())) {
                return (ProgressBarCustomWrapper) var2;
            }

            if (var2 instanceof IContainerCustomWrapper) {
                ProgressBarCustomWrapper var6 = ((IContainerCustomWrapper) var2).findJProgressBar(var1);
                if (var6 != null) {
                    return var6;
                }
            }
        }

        return null;
    }

    /**
     * Поиск графического элемента по имени атрибута name
     *
     * @param var1 Например versionLabel
     * @return
     */
    public JLabel findJLabel(String var1) {
        Component[] var5 = this.getContentPane().getComponents();
        int var4 = var5.length;
        for (int var3 = 0; var3 < var4; ++var3) {
            Component var2 = var5[var3];
            if (var2 instanceof JLabel && var1.equals(var2.getName())) {
                return (JLabel) var2;
            }

            if (var2 instanceof IContainerCustomWrapper) {
                JLabel var6 = ((IContainerCustomWrapper) var2).findJLabel(var1);
                if (var6 != null) {
                    return var6;
                }
            }
        }
        return null;
    }

    public TextfieldCustomWrapper findJTextField(String var1) {
        Component[] var5;
        int var4 = (var5 = this.getContentPane().getComponents()).length;

        for (int var3 = 0; var3 < var4; ++var3) {
            Component var2 = var5[var3];
            if (var2 instanceof TextfieldCustomWrapper && var1.equals(var2.getName())) {
                return (TextfieldCustomWrapper) var2;
            }

            if (var2 instanceof IContainerCustomWrapper) {
                TextfieldCustomWrapper var6 = ((IContainerCustomWrapper) var2).findJTextField(var1);
                if (var6 != null) {
                    return var6;
                }
            }
        }

        return null;
    }

    public Component findJComponent(String var1) {
        Iterator var3 = this.buZ.iterator();

        Component var2;
        while (var3.hasNext()) {
            var2 = (Component) var3.next();
            if (var1.equals(var2.getName())) {
                return var2;
            }
        }

        Component[] var5;
        int var4 = (var5 = this.getContentPane().getComponents()).length;

        for (int var7 = 0; var7 < var4; ++var7) {
            var2 = var5[var7];
            if (var1.equals(var2.getName())) {
                return var2;
            }

            if (var2 instanceof IContainerCustomWrapper) {
                Component var6 = ((IContainerCustomWrapper) var2).findJComponent(var1);
                if (var6 != null) {
                    return var6;
                }
            }
        }

        return null;
    }

    public IContainerCustomWrapper ce(String var1) {
        Component[] var5;
        int var4 = (var5 = this.getContentPane().getComponents()).length;

        for (int var3 = 0; var3 < var4; ++var3) {
            Component var2 = var5[var3];
            if (var2 instanceof IContainerCustomWrapper) {
                if (var1.equals(var2.getName())) {
                    return (IContainerCustomWrapper) var2;
                }

                IContainerCustomWrapper var6 = ((IContainerCustomWrapper) var2).ce(var1);
                if (var6 != null) {
                    return var6;
                }
            }
        }

        return null;
    }

    public RepeaterCustomWrapper ch(String var1) {
        if (var1 == null) {
            return null;
        } else {
            Iterator var3 = this.buZ.iterator();

            while (var3.hasNext()) {
                RepeaterCustomWrapper var2 = (RepeaterCustomWrapper) var3.next();
                if (var1.equals(var2.getName())) {
                    return var2;
                }
            }

            Component[] var5;
            int var4 = (var5 = this.getContentPane().getComponents()).length;

            for (int var8 = 0; var8 < var4; ++var8) {
                Component var7 = var5[var8];
                if (var7 instanceof IContainerCustomWrapper) {
                    RepeaterCustomWrapper var6 = ((IContainerCustomWrapper) var7).ch(var1);
                    if (var6 != null) {
                        return var6;
                    }
                }
            }

            return null;
        }
    }

    public void a(RepeaterCustomWrapper var1) {
        this.buZ.add(var1);
    }

    public JComboBox findJComboBox(String var1) {
        Component[] var5;
        int var4 = (var5 = this.getContentPane().getComponents()).length;

        for (int var3 = 0; var3 < var4; ++var3) {
            Component var2 = var5[var3];
            if (var2 instanceof JComboBox && var1.equals(var2.getName())) {
                return (JComboBox) var2;
            }

            if (var2 instanceof IContainerCustomWrapper) {
                JComboBox var6 = ((IContainerCustomWrapper) var2).findJComboBox(var1);
                if (var6 != null) {
                    return var6;
                }
            }
        }

        return null;
    }

    /**
     * Установка размера и  Проверяет этот контейнер
     */
    public void pack() {//getPreferredSize = java.awt.Dimension[width=200,height=16]
        this.setSize(this.getPreferredSize());//java.awt.Dimension
        this.validate();//Проверяет этот контейнер и все его подкомпоненты.
    }

    public void setEnabled(boolean var1) {
        Component[] var5;
        int var4 = (var5 = this.getContentPane().getComponents()).length;

        for (int var3 = 0; var3 < var4; ++var3) {
            Component var2 = var5[var3];
            var2.setEnabled(var1);
        }

        this.getContentPane().setEnabled(var1);
        super.setEnabled(var1);
    }

    public void setFocusable(boolean var1) {
        Component[] var5;
        int var4 = (var5 = this.getContentPane().getComponents()).length;

        for (int var3 = 0; var3 < var4; ++var3) {
            Component var2 = var5[var3];
            var2.setFocusable(var1);
        }

        this.getContentPane().setFocusable(var1);
        super.setFocusable(var1);
    }

    protected void e(Container var1) {
        synchronized (this.getTreeLock()) {
            int var3 = var1.getComponentCount();

            for (int var4 = 0; var4 < var3; ++var4) {
                Component var5 = var1.getComponent(var4);
                if (var5 instanceof Container) {
                    this.e((Container) var5);
                } else if (var5.isValid()) {
                    var5.invalidate();
                }
            }

            if (var1.isValid()) {
                var1.invalidate();
            }

        }
    }

    public String getElementName() {
        return "window";
    }

    public void close() {
        this.doDefaultCloseAction();
    }

    public void jD(String var1) {
        aeK var2 = ComponentManager.getCssHolder(this);
        var2.setAttribute("class", var1);
        var2.clear();
    }

    public int getAlpha() {
        PropertiesUiFromCss var1 = PropertiesUiFromCss.g(this);
        if (var1 != null) {
            Color var2 = var1.getColorMultiplier();
            if (var2 != null) {
                return var2.getAlpha();
            }
        }

        return 255;
    }

    public void setAlpha(int var1) {
        PropertiesUiFromCss var2 = PropertiesUiFromCss.g(this);
        if (var2 != null) {
            var2.setColorMultiplier(new Color(var1, var1, var1, var1));
        }

    }

    public void removeNotify() {
        if (this.getParent() != null) {
            this.getParent().repaint(this.getX(), this.getY(), this.getWidth(), this.getHeight());
        }

        super.removeNotify();
    }

    public wz cpV() {
        return (wz) this.getClientProperty("gui.TooltipProvider");
    }

    public void a(wz var1) {
        this.putClientProperty("gui.TooltipProvider", var1);
    }

    public void a(lL_q var1) {
        this.gmH = var1;
    }

    public void b(lL_q var1) {
        this.gmI = var1;
    }

    public void Kk() {
        ((IContainerCustomWrapper) this.getContentPane()).Kk();
    }

    public void Kl() {
        ((IContainerCustomWrapper) this.getContentPane()).Kl();
    }

    private final class ContentPane extends PanelCustomWrapper {

        public ContentPane() {
            this.setName("contentpane");
            this.setLayout(new BorderLayout());
        }

        public String getElementName() {
            return "contentpane";
        }
    }
}
