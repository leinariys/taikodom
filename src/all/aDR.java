package all;


import org.apache.commons.logging.Log;

import java.util.Iterator;
import java.util.WeakHashMap;
import java.util.concurrent.Semaphore;

public class aDR //implements aOm
{
    private static final Log logger = LogPrinter.setClass(aDR.class);
    arL hDb;
    boolean hge = false;
    boolean hDd = false;
    private all.b dyE;
    private Semaphore hDc = new Semaphore(1);
    private WeakHashMap hDe = new WeakHashMap();
    private a_q hDf;

    public aDR(all.b var1, arL var2) {
        this.hDf = a_q.iFl;
        this.dyE = var1;
        this.hDb = var2;
    }

    public all.b bgt() {
        return this.dyE;
    }

    public void o(all.b var1) {
        this.dyE = var1;
    }

    public arL cWq() {
        return this.hDb;
    }

    public void d(arL var1) {
        this.hDb = var1;
    }

    public boolean equals(Object var1) {
        if (var1 instanceof aDR) {
            aDR var2 = (aDR) var1;
            if (this == var2) {
                return true;
            } else {
                return this.hDb == var2.hDb && (this.dyE == var2.dyE || this.dyE.equals(var2.dyE));
            }
        } else {
            return false;
        }
    }

    public int hashCode() {
        return this.dyE.hashCode();
    }

    public boolean cST() {
        return this.hge;
    }

    public boolean e(arL var1) throws InterruptedException {
        this.hDc.acquire();

        try {
            if (this.hDd) {
                logger.debug("Trying to_q add " + var1.bFY() + " to_q proxy list of disconnected " + this.dyE);
                return false;
            }

            if (this.hDe.containsKey(var1)) {
                return false;
            }

            aem_q var2 = ((af) var1).a(this);
            this.hDe.put(var1, var2);
            var2.fnt = true;
        } finally {
            this.hDc.release();
        }

        return true;
    }

    /*
       public boolean s(aDJ var1) {
          try {
             this.hDc.acquire();
          } catch (InterruptedException var7) {
             throw new RuntimeException(var7);
          }

          boolean var3;
          try {
             var3 = this.hDe.containsKey(((Xf_q)var1).bFf());
          } finally {
             this.hDc.release();
          }

          return var3;
       }
    */
    public boolean f(arL var1) {
        try {
            this.hDc.acquire();
        } catch (InterruptedException var7) {
            throw new RuntimeException(var7);
        }

        boolean var3;
        try {
            var3 = this.hDe.containsKey(var1);
        } finally {
            this.hDc.release();
        }

        return var3;
    }

    public void g(arL var1) throws InterruptedException {
        this.hDc.acquire();

        try {
            this.hDe.remove(var1);
            if (var1 instanceof af) {
                ((af) var1).b(this);
            }
        } finally {
            this.hDc.release();
        }

    }

    public void cleanUp() throws InterruptedException {
        this.hDc.acquire();

        try {
            this.hDd = true;
            Iterator var2 = this.hDe.keySet().iterator();

            while (var2.hasNext()) {
                arL var1 = (arL) var2.next();
                if (var1 != null) {
                    ((af) var1).b(this);
                }
            }

            this.hDe.clear();
        } finally {
            this.hDc.release();
        }
    }

    public a_q cWr() {
        return this.hDf;
    }

    public void a(a_q var1) {
        this.hDf = var1;
    }

    public boolean cWs() {
        return this.hDf == a_q.iFm;
    }

    public aDR cWt() {
        return this;
    }

    static enum a_q {
        iFk,
        iFl,
        iFm;
    }
}
