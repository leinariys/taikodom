package all;

//package org.w3c.css.sac;
public interface SelectorFactory {
    IConditionalSelector createConditionalSelector(ISimpleSelector var1, Condition var2);

    ISimpleSelector createAnyNodeSelector();

    ISimpleSelector createRootNodeSelector();

    INegativeSelector createNegativeSelector(ISimpleSelector var1);

    IElementSelector createElementSelector(String var1, String var2);

    ICharacterDataSelector createTextNodeSelector(String var1);

    ICharacterDataSelector createCDataSectionSelector(String var1);

    IProcessingInstructionSelector createProcessingInstructionSelector(String var1, String var2);

    ICharacterDataSelector createCommentSelector(String var1);

    IElementSelector o(String var1, String var2);

    IDescendantSelector createDescendantSelector(ISelector var1, ISimpleSelector var2);

    IDescendantSelector createChildSelector(ISelector var1, ISimpleSelector var2);

    ISiblingSelector createDirectAdjacentSelector(short var1, ISelector var2, ISimpleSelector var3);
}
