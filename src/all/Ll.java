package all;

import org.w3c.dom.css.CSSValue;

import java.util.ArrayList;
import java.util.List;

public class Ll extends avI {
    List dtv;

    public Ll() {
        this.dtv = new ArrayList();
    }

    public Ll(avI... var1) {
        this.dtv = new ArrayList(var1.length);

        for (int var2 = 0; var2 < var1.length; ++var2) {
            this.b(var1[var2]);
        }

    }

    public void b(avI var1) {
        if (var1 != null) {
            this.dtv.add(var1);
        }

    }

    public CSSValue a(RC var1, String var2, boolean var3) {
        int var5 = this.dtv.size();

        CSSValue var4;
        do {
            --var5;
            if (var5 < 0) {
                return var3 && var1.Vm() != null ? this.a((RC) var1.Vm(), var2, var3) : null;
            }

            var4 = ((avI) this.dtv.get(var5)).a(var1, var2, false);
        } while (var4 == null);

        return var4;
    }

    public void clear() {
        this.dtv.clear();
    }

    public boolean a(RC var1, aKV_q var2, boolean var3) {
        int var4 = this.dtv.size();

        do {
            --var4;
            if (var4 < 0) {
                return true;
            }
        } while (((avI) this.dtv.get(var4)).a(var1, var2, var3));

        return false;
    }
}
