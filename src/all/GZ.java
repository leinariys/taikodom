package all;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class GZ extends Kd {
    public static GZ dah = new GZ();

    public static boolean c(ObjectOutput var0) {
        if (var0 instanceof aso_q) {
            boolean var1 = (((aso_q) var0).PN() & 1) != 0;
            return var1;
        } else {
            return true;
        }
    }

    public static boolean h(ObjectInput var0) {
        if (var0 instanceof aHN) {
            boolean var1 = (((aHN) var0).PN() & 1) != 0;
            return var1;
        } else {
            return true;
        }
    }

    public static boolean c(att var0) {
        if (var0 instanceof WB) {
            boolean var1 = (((WB) var0).PN() & 1) != 0;
            return var1;
        } else {
            return true;
        }
    }

    public static boolean b(Vm_q var0) {
        if (var0 instanceof adD) {
            boolean var1 = (((adD) var0).PN() & 1) != 0;
            return var1;
        } else {
            return true;
        }
    }

    public Object b(ObjectInput var1) throws IOException, ClassNotFoundException {
        if (var1 instanceof aHN) {
            aHN var2 = (aHN) var1;
            return var2.PL();
        } else {
            return var1.readObject();
        }
    }

    public void a(ObjectOutput var1, Object var2) throws IOException {
        if (var1 instanceof aso_q) {
            aso_q var3 = (aso_q) var1;
            var3.f((Xf_q) var2);
        } else {
            var1.writeObject(var2);
        }

    }

    public Object a(String var1, Vm_q var2) {
        if (var2 instanceof adD) {
            adD var3 = (adD) var2;
            return var3.ix(var1);
        } else {
            return super.a(var2);
        }
    }

    public Object a(Vm_q var1) {
        if (var1 instanceof adD) {
            adD var2 = (adD) var1;
            return var2.ix("value");
        } else {
            return super.a(var1);
        }
    }

    public void a(String var1, att var2, Object var3) {
        if (var2 instanceof aso_q) {
            WB var4 = (WB) var2;
            var4.a(var1, (Xf_q) var3);
        } else {
            super.a(var2, var3);
        }

    }

    public void a(att var1, Object var2) {
        if (var1 instanceof aso_q) {
            WB var3 = (WB) var1;
            var3.a("value", (Xf_q) var2);
        } else {
            super.a(var1, var2);
        }

    }
/*
   public static boolean CreateJComponent(att var0) {
      return var0 instanceof WB ? ((WB)var0).Xd() : true;
   }

   public static boolean CreateJComponent(Vm_q var0) {
      return var0 instanceof adD ? ((adD)var0).Xd() : true;
   }
   */
}
