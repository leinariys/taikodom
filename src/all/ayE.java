package all;

import java.io.Serializable;
import java.util.ArrayList;

public class ayE extends ArrayList implements SelectorList, Serializable {

    public int getLength() {
        return this.size();
    }

    public ISelector item(int var1) {
        return (ISelector) this.get(var1);
    }

    public String toString() {
        StringBuffer var1 = new StringBuffer();
        int var2 = this.getLength();

        for (int var3 = 0; var3 < var2; ++var3) {
            var1.append(this.item(var3).toString());
            if (var3 < var2 - 1) {
                var1.append(", ");
            }
        }

        return var1.toString();
    }
}
