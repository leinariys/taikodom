package all;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;

public final class He {
    public static int daD = Integer.MAX_VALUE;

    public static Object[] a(Collection var0, Class var1) {
        Object[] var2 = (Object[]) Array.newInstance(var1, var0.size());
        Iterator var3 = var0.iterator();

        for (int var4 = 0; var3.hasNext(); ++var4) {
            var2[var4] = var3.next();
        }

        return var2;
    }

    public static String m(Object[] var0) {
        if (var0 == null) {
            return "null";
        } else {
            StringBuffer var1 = new StringBuffer(var0.getClass().getComponentType().getName());
            var1.append("[").append(var0.length).append("]:{");
            if (var0.length >= 1) {
                var1.append(var0[0]);

                for (int var2 = 1; var2 < var0.length; ++var2) {
                    var1.append(", ").append(var0[var2]);
                }
            }

            var1.append("}");
            return var1.toString();
        }
    }

    public static Object[] b(Object[] var0, Object var1) {
        Object[] var2 = (Object[]) Array.newInstance(var1.getClass(), var0.length + 1);
        System.arraycopy(var0, 0, var2, 0, var0.length);
        var2[var0.length] = var1;
        return var2;
    }

    public static Object[] a(Class var0, Object[] var1, int var2) {
        Object[] var3 = (Object[]) Array.newInstance(var0, var2);
        System.arraycopy(var1, 0, var3, 0, Math.min(var1.length, var2));
        return var3;
    }

    public static Object[] c(Object[] var0, Object var1) {
        int var2;
        for (var2 = 0; var2 < var0.length && !var0[var2].equals(var1); ++var2) {
            ;
        }

        if (var2 < var0.length) {
            Object[] var3 = (Object[]) Array.newInstance(var1.getClass(), var0.length - 1);
            System.arraycopy(var0, 0, var3, 0, var2);
            System.arraycopy(var0, var2 + 1, var3, var2, var3.length - var2);
            return var3;
        } else {
            return var0;
        }
    }

    public static Object[] a(Object[] var0, Collection var1) {
        if (var1.size() == 0) {
            return var0;
        } else {
            Object[] var2 = (Object[]) Array.newInstance(var0.getClass().getComponentType(), var0.length - var1.size());
            int var3 = 0;
            int var4 = 0;

            int var5;
            for (var5 = 0; var3 < var0.length; ++var3) {
                if (var1.contains(var0[var3])) {
                    System.arraycopy(var0, var5, var2, var5 - var4, var3 - var5);
                    var5 = var3 + 1;
                    ++var4;
                }
            }

            System.arraycopy(var0, var5, var2, var5 - var4, var0.length - var5);
            return var2;
        }
    }

    /**
     * @deprecated
     */
    public static Object[] a(Object[] var0, int var1, int var2) {
        return b(var0, var1, var2);
    }

    public static Object[] b(Object[] var0, int var1, int var2) {
        int var3;
        if (var1 == Integer.MAX_VALUE) {
            var3 = 0;
        } else if (var1 < 0) {
            var3 = var0.length + var1;
        } else {
            var3 = var1;
        }

        int var4;
        if (var2 == Integer.MAX_VALUE) {
            var4 = var0.length;
        } else if (var2 < 0) {
            var4 = var0.length + var2;
        } else {
            var4 = var2;
        }

        if (var3 < 0) {
            var3 = 0;
        } else if (var3 > var0.length) {
            var3 = var0.length;
        }

        if (var4 < 0) {
            var4 = 0;
        } else if (var4 > var0.length) {
            var4 = var0.length;
        }

        int var5 = var4 - var3;
        if (var5 < 0) {
            var5 = 0;
        }

        Object[] var6 = (Object[]) Array.newInstance(var0.getClass().getComponentType(), var5);
        System.arraycopy(var0, var3, var6, 0, var5);
        return var6;
    }
}
