package all;

import taikodom.infra.comm.tcpnio.NIOBandwidthMeter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.channels.Channel;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;
import java.util.zip.Deflater;

public class LZ extends yV {
    private static final boolean dyB = false;
    private static final int dyC = 1024;
    private static AtomicInteger nextId = new AtomicInteger();
    private final LinkedBlockingQueue dyD;
    private final dm_q auD;
    private final all.b dyE;
    private final int dyF;
    private final TT_q dyG;
    private final aBC dyH;
    private final boolean auG;
    long dyK;
    long dyL;
    ReentrantLock dyM;
    ArrayList dyS;
    private Deflater dyI;
    private byte[] dyJ;
    private a_q dyN;
    private boolean dyO;
    private boolean dyP;
    private int id;
    private NIOBandwidthMeter dyQ;
    private AtomicBoolean dyR;
    private int pa;

    LZ(all.b var1, dm_q var2, SocketChannel var3, int var4) {
        this(var1, var2, var3, var4, true);
    }

    LZ(all.b var1, dm_q var2, SocketChannel var3, int var4, boolean var5) {
        super(var3);
        this.dyD = new LinkedBlockingQueue();
        this.dyI = new Deflater(-1, true);
        this.dyJ = new byte[262144];
        this.dyK = System.currentTimeMillis();
        this.dyL = System.currentTimeMillis();
        this.dyM = new ReentrantLock();
        this.dyN = a_q.eQn;
        this.dyO = true;
        this.id = nextId.incrementAndGet();
        this.dyR = new AtomicBoolean();
        this.dyS = new ArrayList();
        this.dyF = var4;
        this.auG = var5;
        this.pa = 67108863;
        if (var2.jL()) {
            this.dyG = new Bc(2048, this.pa);
            this.dyH = new bL(2048, this.pa);
        } else {
            this.dyG = new aud(2048, this.pa);
            this.dyH = new aUN(2048, this.pa);
        }

        this.dyE = var1;
        this.auD = var2;
        this.dyQ = new NIOBandwidthMeter(this);
    }

    public boolean bgo() {
        return this.auG;
    }

    int bgp() {
        return 5;
    }

    public a_q bgq() {
        return this.dyN;
    }

    public void a(a_q var1) {
        this.dyN = var1;
    }

    public void b(JB var1) {
        if (this.dyN != a_q.eQt) {
            if (this.dyN != a_q.eQs) {
                this.dyL = System.currentTimeMillis();
                if (this.dyD.offer(var1)) {
                    if (this.dyM.tryLock()) {
                        try {
                            if (!this.dyP && this.bgs()) {
                                this.bGd();
                            }
                        } finally {
                            this.dyM.unlock();
                        }
                    } else if (this.dyD.size() < 10) {
                        this.bGd();
                    }

                }
            }
        }
    }

    public void c(JB var1) throws IOException {
        if (this.dyN != a_q.eQt) {
            if (this.dyN != a_q.eQs) {
                if (this.auD.jL() && this.dyE.xQ() == b.a.j && var1.getLength() > 1024) {
                    byte[] var2 = this.d(var1);
                    JB var3 = new JB(var2);
                    var3.cT(true);
                    var3.le(var1.getLength());
                    var1 = var3;
                }

                this.dyL = System.currentTimeMillis();

                try {
                    this.e(var1);
                } catch (InterruptedException var4) {
                    throw new IOException(var4.getMessage());
                }
            }
        }
    }

    private byte[] d(JB var1) {
        this.dyI.setInput(var1.cJn(), var1.getOffset(), var1.getLength());
        this.dyI.finish();
        ByteArrayOutputStream var2 = new ByteArrayOutputStream();

        while (!this.dyI.finished()) {
            int var3 = this.dyI.deflate(this.dyJ);
            var2.write(this.dyJ, 0, var3);
        }

        this.dyI.reset();
        return var2.toByteArray();
    }

    private void e(JB var1) throws InterruptedException {
        this.dyD.put(var1);
        this.bGd();
    }

    private void f(JB var1) throws InterruptedException, IOException {
        if (this.dyD.offer(var1)) {
            if (this.dyM.tryLock()) {
                try {
                    if (!this.dyP) {
                        this.a(this.bGe(), false, true);
                        if (this.bgs()) {
                            this.bGd();
                        }
                    }
                } finally {
                    this.dyM.unlock();
                }
            } else if (this.dyD.size() < 10) {
                this.bGd();
            }
        } else {
            this.dyD.put(var1);
        }

    }

    public void bgr() {
        if (this.dyR.compareAndSet(false, true)) {
            this.dyN = a_q.eQt;
            this.dyS.clear();
            this.dyD.clear();
            if (this.dyO) {
                this.dyO = false;
                this.auD.e(this);
            }

            this.dyQ.cqZ();
            this.dyQ = null;
        }

    }

    public void b(SelectionKey var1) throws IOException, InterruptedException {
        if (!var1.isValid()) {
            if (this.dyO) {
                this.dyO = false;
                this.auD.e(this);
            }

        } else {
            if (var1.isReadable() && this.bgq() != a_q.eQs) {
                if (this.dyG.b(var1.channel()) == -1) {
                    try {
                        var1.channel().close();
                        var1.cancel();
                    } finally {
                        this.bgr();
                    }

                    return;
                }

                this.dyK = System.currentTimeMillis();
            }

            while (this.dyG.ayh()) {
                this.bgt().xS();
                JB var2 = this.dyG.ayg();
                var2.k(this.bgt());
                this.auD.a(this, var2);
            }

            this.a(var1);
        }
    }

    public void a(SelectionKey var1) throws IOException {
        if (this.dyM.tryLock()) {
            try {
                this.a(var1, true, (var1.interestOps() & 4) == 0 || var1.isWritable());
            } finally {
                this.dyM.unlock();
            }
        }

    }

    private void a(SelectionKey var1, boolean var2, boolean var3) throws IOException {
        if ((var3 || this.dyH.gE()) && this.dyH.gE()) {
            this.dyH.a((Channel) this.getChannel());
        }

        if (!this.dyH.gE()) {
            this.dyD.drainTo(this.dyS, 100 - this.dyS.size());
            if (this.dyS.size() > 0) {
                Iterator var4 = this.dyS.iterator();

                while (var4.hasNext()) {
                    JB var5 = (JB) var4.next();
                    if (!this.dyH.a(var5)) {
                        break;
                    }

                    var4.remove();
                }

                this.dyH.gF();
                if (this.dyH.gE() && var3) {
                    this.dyH.a((Channel) this.getChannel());
                }
            }
        }

        if (var2) {
            if (this.bgs()) {
                this.dyP = true;
                var1.interestOps(5);
            } else if (this.bgq() == a_q.eQs) {
                try {
                    this.getChannel().close();
                } finally {
                    this.bgr();
                }
            } else {
                this.dyP = false;
                var1.interestOps(1);
            }
        }

    }

    private boolean bgs() {
        return this.dyH.gE() || this.dyS.size() > 0 || this.dyD.size() > 0;
    }

    public all.b bgt() {
        return this.dyE;
    }

    public void bgu() throws IOException {
        this.auD.c(this);
    }

    public void bgv() {
        this.a(a_q.eQs);
    }

    public int gH() {
        return this.dyH.gH();
    }

    public int gI() {
        return this.dyH.gI();
    }

    public int gG() {
        return this.dyH.gG();
    }

    public int ayk() {
        return this.dyG.ayk();
    }

    public int ayl() {
        return this.dyG.ayl();
    }

    public int ayj() {
        return this.dyG.ayj();
    }

    public int gJ() {
        return this.dyH.gJ();
    }

    public int gK() {
        return this.dyH.gK();
    }

    public int getId() {
        return this.id;
    }

    public int bgw() {
        return this.dyF;
    }

    public long bgx() {
        return this.dyK;
    }

    public long bgy() {
        return this.dyL;
    }

    public int bgz() {
        return this.dyD.size();
    }

    public boolean bgA() {
        return this.dyE.xQ() == b.a.j;
    }

    public static enum a_q {
        eQn,
        eQo,
        eQp,
        eQq,
        eQr,
        eQs,
        eQt;
    }
}
