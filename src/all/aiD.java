package all;

import org.apache.commons.logging.Log;
import taikodom.game.main.ClientMain;

import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Обёрта для создания задач, очереди задач и передача задач на выполнение
 */
public class aiD implements Executor {

    /**
     * Логирование работы
     */
    private static final Log logger = LogPrinter.setClass(aiD.class);

    /**
     * Ссылка на ClientMain
     * Для доступа к пулу потока и
     * Блокировщику ресурсов
     */
    private final ClientMain clientMain;

    /**
     * Очередь задач дя потоков
     */
    LinkedBlockingQueue gQc = new LinkedBlockingQueue();

    /**
     * Создание задач для потоков
     *
     * @param var1 Ссылка на ClientMain
     */
    public aiD(ClientMain var1) {
        this.clientMain = var1;
    }

    /**
     * Выполняет задачу со временем.
     * Команда может выполняться в новом потоке,
     * в объединенном потоке или в вызывающем потоке по усмотрению реализации Executor.
     *
     * @param var1 Выполняемая задача
     */
    public void execute(final Runnable var1) {
        try {
            this.gQc.put(new Runnable() {
                public void run() {
                    synchronized (aiD.this.clientMain.engineGraphics.aee().getTreeLock()) {
                        Xn var2 = aiD.this.clientMain.dTs.cYc();

                        try {
                            var2.brK();
                            var1.run();
                        } finally {
                            var2.brL();
                        }

                    }
                }
            });
        } catch (InterruptedException var3) {
            var3.printStackTrace();
        } catch (RuntimeException var4) {
            var4.printStackTrace();
            throw var4;
        }

    }

    /**
     * Запустить бесконечный цикл
     * Из очереди задач отправить задачу на выполнение потокам
     */
    public void cCM() {
        while (true) {
            //Извлекает и удаляет головку этой очереди или возвращает null, если эта очередь пуста.
            final Runnable var1 = (Runnable) this.gQc.poll();
            if (var1 == null) {
                return;
            }

            this.clientMain.threadPool.submit(new Runnable() {
                public void run() {
                    try {
                        var1.run();
                    } catch (Throwable var2) {
                        aiD.logger.warn("Exception 005 while executing all task", var2);
                    }

                }
            });
        }
    }
}
