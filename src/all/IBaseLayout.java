package all;

import java.awt.*;

public interface IBaseLayout {
    LayoutManager createLayout(Container var1, String var2);

    Object createGridBagConstraints(String var1);
}
