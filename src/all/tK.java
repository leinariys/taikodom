package all;

public enum tK {
    bsA,
    bsB,
    bsC,
    bsD,
    bsE,
    bsF,
    bsG,
    bsH,
    bsI,
    bsJ,
    bsK,
    bsL,
    bsM,
    bsN,
    bsO,
    bsP,
    bsQ,
    bsR,
    bsS,
    bsT,
    bsU,
    bsV,
    bsW,
    bsX,
    bsY;

    private static tK[] bsZ = values();

    public static tK eM(int var0) {
        return bsZ[var0];
    }

    public boolean aiE() {
        return this.ordinal() < bsF.ordinal();
    }

    public boolean aiF() {
        return this.ordinal() < bsP.ordinal();
    }

    public boolean aiG() {
        return this.ordinal() > bsP.ordinal() && this.ordinal() < bsW.ordinal();
    }

    public boolean isCompound() {
        return this.ordinal() == bsX.ordinal();
    }

    public boolean isInfinite() {
        return this.ordinal() == bsV.ordinal();
    }
}
