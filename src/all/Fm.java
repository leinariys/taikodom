package all;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

public class Fm {
    private static final AtomicInteger cVj = new AtomicInteger();
    static LogPrinter logger = LogPrinter.K(Fm.class);
    ahw_q cVg;
    private ReentrantLock PL = new ReentrantLock();
    private TreeSet cVh = new TreeSet();
    private TreeSet cVi = new TreeSet();
    private Executor executor = new sC_q();

    public Fm(ahw_q var1) {
        this.cVg = var1;
    }

    public b a(a var1) {
        return new b(var1);
    }

    public void a(b var1, long var2, TimeUnit var4, long var5, TimeUnit var7) {
        this.PL.lock();

        try {
            this.cVh.remove(var1);
            boolean var8 = this.cVi.remove(var1);
            if (var1.izf > 0L) {
                var1.izf = Math.min(this.cVg.currentTimeMillis() + var4.toMillis(var2), Math.max(this.cVg.currentTimeMillis(), var1.izf));
            } else {
                var1.izf = this.cVg.currentTimeMillis() + var4.toMillis(var2);
            }

            if (var5 > 0L) {
                var1.ize = var7.toNanos(var5);
            } else {
                var1.ize = 0L;
            }

            var1.izi = false;
            if (!var1.aKE) {
                this.cVh.add(var1);
            }

            if (var8) {
                this.cVi.add(var1);
            }
        } finally {
            this.PL.unlock();
        }

    }

    public void gZ() {
        this.PL.lock();

        ArrayList var1;
        try {
            this.cVi.clear();
            var1 = new ArrayList(this.cVh);
            Iterator var2 = var1.iterator();

            while (true) {
                if (!var2.hasNext()) {
                    this.cVi.addAll(var1);
                    break;
                }

                if (var2.next() == null) {
                    var2.remove();
                }
            }
        } finally {
            this.PL.unlock();
        }

        while (true) {
            var1 = null;
            this.PL.lock();

            b var10;
            try {
                if (this.cVi.size() == 0 || this.cVh.size() == 0) {
                    break;
                }

                var10 = (b) this.cVi.first();
                if (this.cVg.currentTimeMillis() < var10.izf) {
                    break;
                }

                this.cVh.remove(var10);
                this.cVi.remove(var10);
                var10.aKE = true;
                var10.izg = var10.izf;
                if (var10.ize > 0L) {
                    var10.izf = var10.izf + var10.ize;
                } else {
                    var10.izf = 0L;
                    var10.izi = true;
                }
            } finally {
                this.PL.unlock();
            }

            if (var10 == null) {
                return;
            }

            this.executor.execute(var10.izk);
        }

    }

    public Executor getExecutor() {
        return this.executor;
    }

    public void setExecutor(Executor var1) {
        this.executor = var1;
    }

    public String aPn() {
        return this.cVh.toString();
    }

    public void c(Jz var1) {
        this.cVg = var1;
    }

    public interface a {
        void a(b var1);
    }

    public class b implements Comparable {
        final Runnable izk;
        public long izg;
        private int id;
        private long ize;
        private long izf;
        private long izh;
        private boolean aKE;
        private boolean izi;
        private a izj;

        public b(a var2) {
            this.id = Fm.cVj.incrementAndGet();
            this.izh = Fm.this.cVg.currentTimeMillis();
            this.aKE = false;
            this.izk = new Runnable() {
                public void run() {
                    b.this.dnw();
                }
            };
            this.izj = var2;
        }

        public void cancel() {
            Fm.this.PL.lock();

            try {
                Fm.this.cVh.remove(this);
                Fm.this.cVi.remove(this);
                this.izi = true;
            } finally {
                Fm.this.PL.unlock();
            }

        }

        private void dnw() {
            try {
                this.izj.a(this);
            } catch (Throwable var24) {
                var24.printStackTrace();
            } finally {
                this.izh = this.izg;
                Fm.this.PL.lock();

                try {
                    this.aKE = false;
                    if (!this.izi) {
                        Fm.this.cVh.remove(this);
                        Fm.this.cVh.add(this);
                    }
                } finally {
                    Fm.this.PL.unlock();
                }

            }

        }

        public int b(b var1) {
            if (var1 == this) {
                return 0;
            } else {
                long var2 = this.izf - var1.izf;
                return var2 < 0L ? -1 : (var2 > 0L ? 1 : this.id - var1.id);
            }
        }

        public long dnx() {
            return this.izg;
        }

        public long dny() {
            return this.izh;
        }

        public String toString() {
            return "task to_q " + this.izf + " " + this.izj;
        }

        public boolean isCancelled() {
            return this.izi;
        }

        @Override
        public int compareTo(Object o) {
            return 0;
        }
    }
}
