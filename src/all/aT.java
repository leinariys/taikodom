package all;

import javax.vecmath.Quat4f;

public class aT extends aoW {
    public OrientationBase a(float var1, float var2, float var3, float var4) {
        OrientationBase var5 = (OrientationBase) this.get();
        var5.set(var1, var2, var3, var4);
        return var5;
    }

    public OrientationBase b(Quat4f var1) {
        OrientationBase var2 = (OrientationBase) this.get();
        var2.set(var1);
        return var2;
    }

    protected OrientationBase fv() {
        return new OrientationBase();
    }

    protected void a(OrientationBase var1, OrientationBase var2) {
        var1.f(var2);
    }

    @Override
    protected Object create() {
        return null;
    }

    @Override
    protected void copy(Object var1, Object var2) {

    }
}
