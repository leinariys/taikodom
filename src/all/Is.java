package all;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.List;

public class Is extends aOv implements Externalizable {
    private static final long serialVersionUID = 1L;
    private int id;
    private aic dfz;
    private Gw dfA;
    private Xf_q Uk;

    public Is() {
    }

    public Is(Xf_q var1, int var2, Object var3, List var4) {
        this(var1, var2, var3, (Throwable) null, var4);
    }

    public Is(Xf_q var1, int var2, Object var3, Throwable var4, List var5) {
        this.id = var2;
        if (var4 != null) {
            this.dfA = new Gw(var4);
        } else {
            this.dfz = new aic(var3, var5);
        }

        this.Uk = var1;
    }

    public int getId() {
        return this.id;
    }

    public void readExternal(ObjectInput var1) throws IOException, ClassNotFoundException {
        this.id = var1.readInt();
        this.Uk = (Xf_q) GZ.dah.b(var1);
        if (var1.readBoolean()) {
            this.dfz = new aic();
            this.dfz.readExternal(var1);
        } else {
            this.dfA = new Gw();
            this.dfA.readExternal(var1);
        }

        super.readExternal(var1);
    }

    public void writeExternal(ObjectOutput var1) throws IOException {
        var1.writeInt(this.id);
        GZ.dah.a((ObjectOutput) var1, (Object) this.Uk);
        if (this.dfz != null) {
            var1.writeBoolean(true);
            this.dfz.writeExternal(var1);
        } else {
            var1.writeBoolean(false);
            this.dfA.writeExternal(var1);
        }

        super.writeExternal(var1);
    }

    public Xf_q yz() {
        return this.Uk;
    }

    public Object getResult() {
        return this.dfz != null ? this.dfz.getResult() : null;
    }

    public kU[] aWo() {
        return this.dfz != null ? this.dfz.aWo() : null;
    }

    public akO a(all.b var1, Jz var2) {
        ((se_q) this.Uk.bFf()).c(var1, this);
        return null;
    }

    public Throwable getException() {
        return this.dfA != null ? this.dfA.getCause() : null;
    }
}
