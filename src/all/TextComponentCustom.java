package all;

import javax.swing.text.JTextComponent;
import java.awt.*;

public abstract class TextComponentCustom extends BaseItem {
    protected static final LogPrinter logger = LogPrinter.setClass(TextComponentCustom.class);

    protected void a(MapKeyValue var1, JTextComponent var2, XmlNode var3) {
        super.applyAttribute(var1, (Component) var2, (XmlNode) var3);
        if ("false".equalsIgnoreCase(var3.getAttribute("editable"))) {
            var2.setEditable(false);
        }

        String var4 = var3.getAttribute("text");
        if (var4 != null) {
            var2.setText(var4);
        }

        XmlNode var5 = var3.c(0, "text", (String) null, (String) null);
        if (var5 != null) {
            var2.setText(var5.getText());
        }

        if (var2 instanceof ITextComponentCustomWrapper) {
            Integer var6 = checkValueAttribut((XmlNode) var3, (String) "max-char", (LogPrinter) logger);
            if (var6 != null) {
                ((ITextComponentCustomWrapper) var2).h(var6.intValue());
            }
        }

    }
}
