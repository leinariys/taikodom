package all;

import org.w3c.dom.css.CSSStyleDeclaration;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Загрузчик ресурсов аддона
 */
public class tj implements axa {
    /**
     *
     */
    private static Map bkU = new ConcurrentHashMap();
    /**
     * ссылка на Addon manager
     */
    protected final AddonManager bkT;
    /**
     * ссылка на Обёртка класса аддона логирование и локализация
     */
    private final pp bkV;
    /**
     * Список добавленых панелий в рут панель
     * пенели аддонов
     */
    private List bkS = new ArrayList();

    /**
     * Конструктор класса
     *
     * @param var1 ссылка на Addon manager
     * @param var2 ссылка на Обёртка класса аддона логирование и локализация
     */
    public tj(AddonManager var1, pp var2) {
        this.bkT = var1;
        this.bkV = var2;
    }

    /**
     * Создать Component на основе загрузить .xml файл
     *
     * @param var1 .xml файл  Пример login.xml
     * @return
     */
    public IComponentCustomWrapper CreatJComponentFromXML(String var1) {
        // this.bkV.bHy() = Пример taikodom.addon.login.LoginAddon
        return this.preparePath((Object) this.bkV.bHy(), (String) var1, (IContainerCustomWrapper) null);
    }

    /**
     * Создать Component на основе загрузить .xml файл
     *
     * @param var1
     * @param var2
     * @return
     */
    public IComponentCustomWrapper CreatJComponentFromXML(String var1, IContainerCustomWrapper var2) {
        return this.preparePath((Object) this.bkV.bHy(), (String) var1, (IContainerCustomWrapper) var2);
    }

    /**
     * Создать Component на основе загрузить .xml файл
     *
     * @param var1 Пример taikodomversion.xml
     * @return
     */
    public WindowCustomWrapper bN(String var1) {
        return (WindowCustomWrapper) this.preparePath((Object) this.bkV.bHy(), (String) var1, (IContainerCustomWrapper) null);
    }

    /**
     * Формирование адреса для загрузки  .xml ресурса аддона
     *
     * @param var1 ссылка на Загруженный класс аддона Пример taikodom.addon.login.LoginAddon
     * @param var2 Пример login.xml
     * @param var3
     * @return
     */
    protected IComponentCustomWrapper preparePath(Object var1, String var2, IContainerCustomWrapper var3) {
        URL var4;
        Class var5 = var1.getClass(); //var1.getClassBaseUi().getEnclosingClass() для LoginAddon это all.aCo
        if (var1 instanceof Class) {
            var4 = ((Class) var1).getResource(((Class) var1).getSimpleName() + ".class");

        } else if (var1 instanceof URL) {
            var4 = (URL) var1;
        } else if (var1 != null) {
            if (var5.getEnclosingClass() != null)
                var5 = var5.getEnclosingClass();

            var4 = var1.getClass().getResource(var5.getSimpleName() + ".class");//file:/C:/,,,/taikodom/addon/login/LoginAddon.class
        } else {
            var4 = null;
        }
        try {
            //file:/C:/,,,/taikodom/addon/login/LoginAddon.class, login.xml
            URL var7 = new URL(var4, var2);//file:/C:/,,,/taikodom/addon/login/login.xml
            return this.preparePath(var7, var3);
        } catch (MalformedURLException var6) {
            throw new RuntimeException(var6);
        }
    }

    /**
     * Загрузка .xml ресурса аддона
     *
     * @param var1 Пример file:/C:/,,,/taikodom/addon/login/login.xml
     * @param var2
     * @return
     */
    protected IComponentCustomWrapper preparePath(URL var1, IContainerCustomWrapper var2) {
        return this.a((new MapKeyValue((this.bkT.getBaseUItagXML()).cnl(), var1)), var1, var2);
    }

    /**
     * @param var1 Пример TranslationParseContext: class taikodom.addon.debugtools.taikodomversion.TaikodomVersionAddon
     * @param var2 Пример file:/C:/.../taikodom/addon/debugtools/taikodomversion/taikodomversion.xml
     * @param var3
     * @return Возвращает Базовые элементы интерфейса Пример, для class LoginAddon это class aCo extends JPanel
     */
    public IComponentCustomWrapper a(MapKeyValue var1, URL var2, IContainerCustomWrapper var3) {
        IComponentCustomWrapper var4 = (IComponentCustomWrapper) this.bkT.getBaseUItagXML().CreateJComponent(var1, var2);//получаем графический компонент на котором будет рисаваться аддон
        //addonManager AddonManager kj GK
        if (var3 != null) {
            ((Container) var3).add((JComponent) var4);
        } else {
            this.bkS.add(var4);
        }
        return var4;
    }


    public void dispose() {
        Iterator var2 = this.bkS.iterator();
        while (var2.hasNext()) {
            JComponent var1 = (JComponent) var2.next();
            try {
                Container var3 = var1.getParent();
                if (var3 != null) {
                    var3.remove(var1);
                }
            } catch (Exception var4) {
                var4.printStackTrace();
            }
        }
        this.bkS.clear();
    }

    /**
     * Размер окна java.awt.Dimension[width=1024,height=780]
     *
     * @return
     */
    public Dimension getScreenSize() {
        return new Dimension(this.getScreenWidth(), this.getScreenHeight());
    }

    /**
     * Высота окна
     *
     * @return
     */
    public int getScreenHeight() {
        return this.bkT.aVM().getScreenHeight();
    }

    /**
     * Ширина окна
     *
     * @return
     */
    public int getScreenWidth() {
        return this.bkT.aVM().getScreenWidth();
    }

    public Point getMousePosition() {
        return new Point(0, 0);
    }

    public CSSStyleDeclaration aY(String var1) {
        ParserCss var2 = ParserCss.getParserCss();
        CSSStyleDeclaration var3 = var2.getCSSStyleDeclaration(new InputSource(new StringReader("{" + var1 + "}")));
        if (var3 != null) {
            EQ.parseRule(var3);
        }

        return var3;
    }

    public void c(String var1, Class var2) {
        if (bkU.containsKey(var1)) {//Уже существует компонентный регистр для тега
            throw new IllegalArgumentException("There already is aQ component register for tag: " + var1);
        } else {
            bkU.put(var1, var2);
        }
    }

    public ILoaderImageInterface adz() {
        return this.bkV.getAddonManager().getBaseUItagXML().getLoaderImage();
    }

    public void bO(String var1) {
        bkU.remove(var1);
    }

    public avI a(Class var1, String var2) {
        avI var3;
        if (var2.startsWith("res://")) {
            var3 = (avI) this.bkT.eB(var2);
            if (var3 != null) {
                return var3;
            } else {
                var3 = this.bP(var2);
                this.bkT.n(var2, var3);
                return var3;
            }
        } else {
            var3 = (avI) this.bkT.eB(var1 + var2);
            if (var3 != null) {
                return var3;
            } else {
                var3 = this.bkV.getAddonManager().getBaseUItagXML().loadCSS((Object) var1, (String) var2);
                this.bkT.n(var1 + var2, var3);
                return var3;
            }
        }
    }

    public avI a(InputStream var1) {
        CssNode var2 = new CssNode();
        new EQ(var2);
        try {
            var2.loadFileCSS(var1);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return var2;
    }

    public avI a(Object var1, String var2) {
        if (var2.startsWith("res://")) {
            return this.a((Class) null, (String) var2);
        } else {
            return var1 instanceof Class ? this.bkV.getAddonManager().getBaseUItagXML().loadCSS(var1, var2) : this.a(var1.getClass(), var2);
        }
    }

    private avI bP(String var1) {
        if (var1.startsWith("res://")) {
            try {
                return this.a(this.bkV.hE(var1));
            } catch (IOException var3) {
                var3.printStackTrace();
                return null;
            }
        } else {
            return null;
        }
    }

    // public void setGreen(Object var1, String var2) { }


    public aVf adA() {
        return this.bkV;
    }

    /**
     * Добавить в рут панель содержимого новый компонент
     *
     * @param var1
     */
    public void addInRootPane(JComponent var1) {
        this.bkS.add(var1);
        JRootPane var2 = this.bkT.getBaseUItagXML().getRootPane();
        if (var1 instanceof JInternalFrame) {
            var2.getLayeredPane().add(var1);
        } else {
            var2.getContentPane().add(var1); //Добавить в панель содержимого
        }
    }

    // public void s(int var1, int var2) { }

    public boolean adB() {
        return false;
    }

    public void a(JComponent var1, avI var2, String var3) {
        aeK var4 = ComponentManager.getCssHolder(var1);
        var4.setCssNode(var2);
        var4.setAttribute("class", var3);
        var4.clear();
    }

    public Rectangle c(JComponent var1) {
        return SwingUtilities.convertRectangle(var1.getParent(), var1.getBounds(), this.bkT.getBaseUItagXML().getRootPane());
    }

   /*
   public WindowCustomWrapper CreateJComponent(Class var1, String var2)
   {
      return (WindowCustomWrapper)this.setGreen(var1, var2);
   }
   */

   /*
   public Sr setGreen(Class var1, String var2)
   {
      return this.loadFileXml((Class)var1, (String)var2, (il)null);
   }
*/

/* функционал повтарен выше
   public Sr loadFileXml(Class var1, String var2, il var3)
   {
      URL var4 = var1.getResource(var1.getSimpleName() + ".class");

      try
      {
         URL var5 = new URL(var4, var2);
         return this.preparePath(var5, var3);
      } catch (MalformedURLException var6) {
         throw new RuntimeException(var6);
      }
   }
*/

}
