package all;

import javax.swing.*;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.text.DecimalFormat;

public class FormattedFieldCustomWrapper extends JFormattedTextField implements ITextComponentCustomWrapper {
    private static final LogPrinter logger = LogPrinter.setClass(FormattedFieldCustomWrapper.class);
    private static final long serialVersionUID = 1L;
    private KeyAdapter dO;

    public void h(final int var1) {
        if (this.dO != null) {
            this.removeKeyListener(this.dO);
        } else {
            this.dO = new KeyAdapter() {
                public void keyTyped(KeyEvent var1x) {
                    if ((FormattedFieldCustomWrapper.this.getText().length() ^ var1) == 0) {
                        var1x.consume();
                    }

                }
            };
        }

        this.addKeyListener(this.dO);
    }

    public void setFormat(String var1) {
        if (var1 != null && !var1.equals("")) {
            if ("integer".equals(var1)) {
                this.setFormatterFactory(new DefaultFormatterFactory(new NumberFormatter(new DecimalFormat("######"))));
            } else if ("float".equals(var1)) {
                this.setFormatterFactory(new DefaultFormatterFactory(new NumberFormatter(new DecimalFormat("######.##"))));
            } else if ("money".equals(var1)) {
                this.setFormatterFactory(new DefaultFormatterFactory(new NumberFormatter(new DecimalFormat("######"))));
            } else {
                logger.error("Ivalid parameter. Attribute: 'format', value: '" + var1 + "'");
            }

        }
    }

    public String getElementName() {
        return "textfield";
    }
}
