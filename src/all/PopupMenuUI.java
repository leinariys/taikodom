package all;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicPopupMenuUI;
import java.awt.*;

/**
 * PopupMenuUI
 */
public class PopupMenuUI extends BasicPopupMenuUI implements aIh {
    private ComponentManager Rp;

    public PopupMenuUI(JPopupMenu var1) {
        var1.setLightWeightPopupEnabled(true);
        this.Rp = new ComponentManager("popupmenu", var1);
        var1.setOpaque(false);
    }

    public static ComponentUI createUI(JComponent var0) {
        return new PopupMenuUI((JPopupMenu) var0);
    }

    public Popup getPopup(JPopupMenu var1, int var2, int var3) {
        PopupFactory var4 = aKb.getSharedInstance();
        return var4.getPopup(var1.getInvoker(), var1, var2, var3);
    }

    public void update(Graphics var1, JComponent var2) {
        RM.a((aIh) this, (Graphics) var1, (JComponent) var2);
        this.paint(var1, var2);
    }

    public Dimension getPreferredSize(JComponent var1) {
        RM.a((aIh) this, (JComponent) var1);
        return this.wy().c(var1, super.getPreferredSize(var1));
    }

    public Dimension getMinimumSize(JComponent var1) {
        RM.a((aIh) this, (JComponent) var1);
        return this.wy().b(var1, super.getMinimumSize(var1));
    }

    public Dimension getMaximumSize(JComponent var1) {
        RM.a((aIh) this, (JComponent) var1);
        return this.wy().a(var1, super.getMaximumSize(var1));
    }

    public ComponentManager wy() {
        return this.Rp;
    }

    /**
     * TODO похлже это public pv wy()
     *
     * @return Графический элемент
     */
    @Override
    public aeK wz() {
        return this.Rp;
    }
}
