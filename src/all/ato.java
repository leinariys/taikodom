package all;

public class ato {
    private long swigCPtr;

    protected ato(long var1, boolean var3) {
        this.swigCPtr = var1;
    }

    protected ato() {
        this.swigCPtr = 0L;
    }

    protected static long i(ato var0) {
        return var0 == null ? 0L : var0.swigCPtr;
    }
}
