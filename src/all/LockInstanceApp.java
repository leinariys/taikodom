package all;

import java.io.File;
import java.io.FileFilter;
import java.io.RandomAccessFile;

//   JY

public class LockInstanceApp {
    /**
     * Путь к C:\Users\%UserName%\folderName
     */
    private static final String pathLock;

    /**
     * Файл - флаг показывающий, есть ли ещё одна копия клиента
     */
    private static File pathFile;
    /**
     * Работа с файлом чтение/запись
     */
    private static RandomAccessFile streamFile;

    static {
        pathLock = System.getProperty("user.home") + File.separator + "taikodom"; //C:\Users\%UserName%\taikodom
    }


    /**
     * Создание блокирующего файла, сигнализирующего о запуске приложения, с добавлением метки времени
     *
     * @param namePacket "com.hoplon.Taikodom"
     * @param pathLock   Адресс пользовательской папки "C:\Projects\Java\TaikodomGameLIVE"
     * @return
     */
    public static LockInstanceApp setMarkOriginalTime(String namePacket, String pathLock) {
        String markTime = "." + System.currentTimeMillis() + (long) (Math.random() * 1000.0D);
        return setMarkOriginal(namePacket + markTime, pathLock);//com.hoplon.Taikodom1537873325950841, C:\Projects\Java\TaikodomGameLIVE
    }

    /**
     * Создание файла, сигнализирующего о запуске клиента.
     *
     * @param nameFile C:\Projects\Java\TaikodomGameLIVE\lock
     * @param pathLock com.hoplon.Taikodom1537873712730922.lock
     * @return
     */
    public static LockInstanceApp setMarkOriginal(String nameFile, String pathLock) {
        pathFile = new File(pathLock + File.separator + "lock", nameFile + ".lock");
        return markOriginal();
    }


    /**
     * Проверка не запущенна ли ещё одна копия клиента
     *
     * @param var0 "com.hoplon.Taikodom"
     * @param var1 Адресс пользовательской папки "C:\Projects\Java\TaikodomGameLIVE"
     * @return true -lock есть и не можем удалить, false - блокировки нет
     */
    public static boolean isCopyApp(final String var0, String var1) {
        File var3 = new File(var1 + File.separator + "lock");//C:\Projects\Java\TaikodomGameLIVE\lock
        FileFilter var4 = new FileFilter() {
            public boolean accept(File var1) {
                return var1.getName().startsWith(var0);
            }
        };
        File[] var5 = var3.listFiles(var4);
        if (var5 == null) {
            return false;
        } else {
            File[] var9 = var5; //[0]-com.hoplon.Taikodom1537872164759791.lock
            int var8 = var5.length;
            for (int var7 = 0; var7 < var8; ++var7) {
                File var6 = var9[var7];
                if (var6.exists() && !var6.delete()) //есть ли файл и успешность его удаления
                {
                    return true;
                }
            }
            return false;
        }
    }

    /**
     * Пометить приложение как оригинал
     * Открытие файла на запись чтение
     *
     * @return
     */
    private static LockInstanceApp markOriginal() {
        if (pathFile.exists() && !pathFile.delete()) {
            return null;
        } else {
            try {
                pathFile.getParentFile().mkdirs();//Создание всех каталогов
                streamFile = new RandomAccessFile(pathFile, "rw");
                streamFile.write(10);
            } catch (Exception var2) {
                System.out.println("Can not write lock file: " + pathFile.getAbsolutePath());
                System.out.println(var2.getMessage());
                return new LockInstanceApp();
            }

            return new LockInstanceApp();
        }
    }
}
