package all;

import java.io.IOException;

public class cU_q extends IOException {

    private IOException xA;

    public cU_q(String var1) {
        super(var1);
    }

    public cU_q(IOException var1) {
        this.xA = var1;
    }

    public cU_q(String var1, IOException var2) {
        super(var1);
        this.xA = var2;
    }

    public Throwable getCause() {
        return this.xA;
    }
}
