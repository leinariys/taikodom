package all;

public class S {
    private float[] ek = new float[16];
    private int size;

    public void add(float var1) {
        if (this.size == this.ek.length) {
            this.expand();
        }

        this.ek[this.size++] = var1;
    }

    private void expand() {
        float[] var1 = new float[this.ek.length << 1];
        System.arraycopy(this.ek, 0, var1, 0, this.ek.length);
        this.ek = var1;
    }

    public float remove(int var1) {
        if (var1 >= this.size) {
            throw new IndexOutOfBoundsException();
        } else {
            float var2 = this.ek[var1];
            System.arraycopy(this.ek, var1 + 1, this.ek, var1, this.size - var1 - 1);
            --this.size;
            return var2;
        }
    }

    public float get(int var1) {
        if (var1 >= this.size) {
            throw new IndexOutOfBoundsException();
        } else {
            return this.ek[var1];
        }
    }

    public void set(int var1, float var2) {
        if (var1 >= this.size) {
            throw new IndexOutOfBoundsException();
        } else {
            this.ek[var1] = var2;
        }
    }

    public int size() {
        return this.size;
    }
}
