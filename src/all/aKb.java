package all;

import javax.swing.*;
import java.awt.*;

/**
 * Всплывающее окно
 */
public class aKb extends PopupFactory {
    private static PopupFactory ihP;
    private final PopupFactory aUu = new PopupFactory();
    LogPrinter logger = LogPrinter.K(aKb.class);

    public static PopupFactory getSharedInstance() {
        if (ihP == null) {
            ihP = new aKb();
            PopupFactory.setSharedInstance(ihP);
        }

        return ihP;
    }

    public Popup getPopup(Component var1, Component var2, int var3, int var4) {
        do {
            if (var1 instanceof JApplet) {
                Component var5 = ((JApplet) var1).getGlassPane();
                NQ var6 = new NQ((JComponent) var5);

                try {
                    var6.a(var5, var2, var3, var4);
                } catch (Exception var7) {
                    JLayeredPane var8 = ((JApplet) var1).getLayeredPane();
                    var6 = new NQ((JComponent) var8);
                    var6.a(var8, var2, var3, var4);
                }

                return var6;
            }

            var1 = ((Component) var1).getParent();
        } while (var1 != null);

        this.logger.warn("Popup creation failed, default popup created - JRootPane not found in component hierarchy of " + var1);
        return this.aUu.getPopup((Component) var1, var2, var3, var4);
    }
}
