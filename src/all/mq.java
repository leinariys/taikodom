package all;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * class InterfaceSFXAddon
 */
public class mq extends WA implements Externalizable {
    public static short bti = 0;
    public static short btj = 1;
    private short btk;
    private a btl;
    private boolean btm;

    public mq() {
    }

    public mq(short var1) {
        this(var1, (a) null);
    }

    public mq(short var1, a var2, boolean var3) {
        this.btk = var1;
        this.btl = var2;
        this.btm = var3;
    }

    public mq(short var1, a var2) {
        this(var1, var2, false);
    }

    public short aiH() {
        return this.btk;
    }

    public a aiI() {
        return this.btl;
    }

    public boolean aiJ() {
        return this.btm;
    }

    public void readExternal(ObjectInput var1) {
        throw new UnsupportedOperationException("Don't use this class serverside");
    }

    public void writeExternal(ObjectOutput var1) {
        throw new UnsupportedOperationException("Don't use this class serverside");
    }

    public interface a {
        void execute();
    }
}
