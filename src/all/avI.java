package all;

import org.w3c.dom.css.*;

import java.awt.*;
import java.io.StringReader;
import java.util.ArrayList;

public abstract class avI {
    public static Tu c(CSSValue var0) {
        if (var0 == null) {
            return Tu.fEC;
        } else if (var0.getCssValueType() != 1) {
            return Tu.fEE;
        } else {
            CSSPrimitiveValue var1 = (CSSPrimitiveValue) var0;
            if (var1.getPrimitiveType() == 2) {
                return new Tu(var1.getFloatValue((short) 2), Tu.a.ejc);
            } else {
                return var1.getPrimitiveType() == 3 ? new Tu(var1.getFloatValue((short) 3), Tu.a.ejb) : new Tu(var1.getFloatValue((short) 5), Tu.a.ejd);
            }
        }
    }

    public abstract CSSValue a(RC var1, String var2, boolean var3);

    public abstract boolean a(RC var1, aKV_q var2, boolean var3);

    public float a(RC var1, String var2, float var3, boolean var4) {
        CSSValue var5 = this.a(var1, var2, var4);
        if (var5 == null) {
            return -1.0F;
        } else if (var5.getCssValueType() != 1) {
            PI.p(var5 + " isn't all primitive value");
            return -1.0F;
        } else {
            CSSPrimitiveValue var6 = (CSSPrimitiveValue) var5;
            float var7;
            if (var6.getPrimitiveType() == 2) {
                var7 = var6.getFloatValue((short) 2) / 100.0F * var3;
                return var7;
            } else if (var6.getPrimitiveType() == 3) {
                var7 = var6.getFloatValue((short) 2) * var3;
                return var7;
            } else {
                return var6.getFloatValue((short) 5);
            }
        }
    }

    public float a(RC var1, String var2) {
        return this.b(var1, var2, true);
    }

    public float b(RC var1, String var2, boolean var3) {
        return this.a(var1, var2, var3, -1.0F);
    }

    public float a(RC var1, String var2, boolean var3, float var4) {
        CSSValue var5 = this.a(var1, var2, var3);
        if (var5 == null) {
            return var4;
        } else if (var5.getCssValueType() != 1) {
            PI.p(var5 + " isn't all primitive value");
            return var4;
        } else {
            CSSPrimitiveValue var6 = (CSSPrimitiveValue) var5;
            return var6.getFloatValue((short) 5);
        }
    }

    public Tu c(RC var1, String var2, boolean var3) {
        return c(this.a(var1, var2, var3));
    }

    public Tu getValue(RC var1, String var2, boolean var3, Tu var4) {
        Tu var5 = c(this.a(var1, var2, var3));
        return Tu.fEC.equals(var5) ? var4 : var5;
    }

    public boolean d(RC var1, String var2, boolean var3) {
        CSSValue var4 = this.a(var1, var2, var3);
        return var4 != null;
    }

    public boolean b(RC var1, String var2) {
        return this.d(var1, var2, true);
    }

    public String c(RC var1, String var2) {
        return this.e(var1, var2, true);
    }

    public String e(RC var1, String var2, boolean var3) {
        CSSValue var4 = this.a(var1, var2, var3);
        if (var4 == null) {
            return null;
        } else if (var4.getCssValueType() != 1) {
            PI.p(var4 + " isn't all primitive value");
            return null;
        } else {
            CSSPrimitiveValue var5 = (CSSPrimitiveValue) var4;
            return var5.getStringValue();
        }
    }

    public Point f(RC var1, String var2, boolean var3) {
        CSSValue var4 = this.a(var1, var2, var3);
        if (var4 == null) {
            return null;
        } else if (var4.getCssValueType() == 2) {
            CSSValueList var5 = (CSSValueList) var4;
            Point var6 = new Point();
            var6.setLocation((double) ((CSSPrimitiveValue) var5.item(0)).getFloatValue((short) 2), (double) ((CSSPrimitiveValue) var5.item(1)).getFloatValue((short) 2));
            return var6;
        } else {
            return null;
        }
    }

    public Image d(RC var1, String var2) {
        String var3 = this.e(var1, var2, false);
        if (var3 == null) {
            return null;
        } else if ("none".equals(var3)) {
            return null;
        } else {
            String var4 = "res://";
            if (var3.startsWith(var4)) {
                return this.getImage(var3.substring(var4.length()));
            } else {
                System.err.println("imagename must start with 'res://'. Wrong image name " + var3);
                return null;
            }
        }
    }

    public Image getImage(String var1) {
        return null;
    }

    Object v(Object... var1) {
        Object[] var5 = var1;
        int var4 = var1.length;

        for (int var3 = 0; var3 < var4; ++var3) {
            Object var2 = var5[var3];
            if (var2 != null) {
                return var2;
            }
        }

        return null;
    }

    public CSSStyleDeclaration b(RC var1) {
        Object var2 = null;
        String var3 = var1.getAttribute("style");
        if (var3 != null) {
            try {
                var2 = this.aY(var3);
            } /*catch (IOException var5)
         {
            var5.printStackTrace();
            var2 = new aHx((CSSRule)null);
         }*/ catch (CSSException var6) {
                (new CSSException((short) -1, "Error at Element: " + this.getClass().getName(), var6)).printStackTrace();
                var2 = new aHx((CSSRule) null);
            }
        }

        return (CSSStyleDeclaration) var2;
    }

    public axS c(RC var1) {
        return (axS) this.v(axS.kE(this.e(var1, "position", false)), axS.gRo);
    }

    public FontWrapper parseFont(RC var1) {
        String var2 = this.c(var1, "font-family");
        int var3 = 0;
        int var4 = (int) this.a(var1, "font-size");
        if (var2 != null && !"".equals(var2) && var4 >= 1) {
            String var5 = this.c(var1, "font-weight");
            String var6 = this.c(var1, "font-style");
            if ("bold".equals(var5)) {
                var3 |= 1;
            }

            if ("italic".equals(var6) || "oblique".equals(var6)) {
                var3 |= 2;
            }

            return new FontWrapper(var2, var3, var4);
        } else {
            return null;
        }
    }

    public CSSStyleDeclaration aY(String var1) {
        ParserCss var2 = ParserCss.getParserCss();
        CSSStyleDeclaration var3 = var2.getCSSStyleDeclaration(new InputSource(new StringReader("{" + var1 + "}")));
        if (var3 != null) {
            EQ.parseRule(var3);
        }

        return var3;
    }

    public String[] e(RC var1, String var2) {
        CSSValue var3 = this.a(var1, var2, true);
        if (var3 == null) {
            PI.p("not array found");
            return null;
        } else {
            ArrayList var4 = new ArrayList();
            if (var3.getCssValueType() == 2) {
                CSSValueList var5 = (CSSValueList) var3;

                for (int var6 = 0; var6 < var5.getLength(); ++var6) {
                    var4.add(((CSSPrimitiveValue) var5.item(var6)).getStringValue());
                }
            } else {
                var4.add(((CSSPrimitiveValue) var3).getStringValue());
            }

            return PI.a(var4);
        }
    }

    public Color e(RC var1) {
        CSSValue var2 = this.a(var1, "background-color", false);
        return var2 == null ? null : this.a(((CSSPrimitiveValue) var2).getRGBColorValue());
    }

    public Color f(RC var1) {
        CSSValue var2 = this.a(var1, "border-color", true);
        return var2 == null ? null : this.a(((CSSPrimitiveValue) var2).getRGBColorValue());
    }

    public Color g(RC var1) {
        return this.a(var1, true);
    }

    public Color a(RC var1, boolean var2) {
        return this.g(var1, "color", var2);
    }

    public Color g(RC var1, String var2, boolean var3) {
        CSSValue var4 = this.a(var1, var2, var3);
        if (var4 == null) {
            return null;
        } else {
            float var5 = this.b(var1, var2 + "-alpha", var3);
            if (var4.getCssValueType() == 1) {
                CSSPrimitiveValue var6 = (CSSPrimitiveValue) var4;
                if (var6.getPrimitiveType() == 25) {
                    ColorCSS var7 = (ColorCSS) var6.getRGBColorValue();
                    if (var5 == -1.0F) {
                        return new Color((int) var7.getRed().getFloatValue((short) 1), (int) var7.getGreen().getFloatValue((short) 1), (int) var7.getBlue().getFloatValue((short) 1), (int) var7.getAlpha().getFloatValue((short) 1));
                    }

                    return new Color((int) var7.getRed().getFloatValue((short) 1), (int) var7.getGreen().getFloatValue((short) 1), (int) var7.getBlue().getFloatValue((short) 1), (int) (var5 * 255.0F));
                }
            }

            return null;
        }
    }

    public Color d(CSSValue var1) {
        if (var1.getCssValueType() == 1) {
            CSSPrimitiveValue var2 = (CSSPrimitiveValue) var1;
            if (var2.getPrimitiveType() == 25) {
                ColorCSS var3 = (ColorCSS) var2.getRGBColorValue();
                return new Color((int) var3.getRed().getFloatValue((short) 1), (int) var3.getGreen().getFloatValue((short) 1), (int) var3.getBlue().getFloatValue((short) 1), (int) var3.getAlpha().getFloatValue((short) 1));
            }
        }

        return null;
    }

    private Color a(RGBColor var1) {
        return new Color(var1.getRed().getFloatValue((short) 1) / 255.0F, var1.getGreen().getFloatValue((short) 1) / 255.0F, var1.getBlue().getFloatValue((short) 1) / 255.0F);
    }

    public awy h(RC var1) {
        float var2 = this.a(var1, "border-top-width");
        float var3 = this.a(var1, "border-bottom-width");
        float var4 = this.a(var1, "border-left-width");
        float var5 = this.a(var1, "border-right-width");
        awy var6 = new awy();
        var6.top = (int) var2;
        var6.bottom = (int) var3;
        var6.left = (int) var4;
        var6.right = (int) var5;
        return var6;
    }

    /**
     * @deprecated
     */
    @Deprecated
    public awy i(RC var1) {
        float var2 = this.a(var1, "padding-top", false, 0.0F);
        float var3 = this.a(var1, "padding-bottom", false, 0.0F);
        float var4 = this.a(var1, "padding-left", false, 0.0F);
        float var5 = this.a(var1, "padding-right", false, 0.0F);
        awy var6 = new awy();
        var6.top = (int) var2;
        var6.bottom = (int) var3;
        var6.left = (int) var4;
        var6.right = (int) var5;
        return var6;
    }

    /**
     * @deprecated
     */
    @Deprecated
    public awy j(RC var1) {
        float var2 = this.a(var1, "margin-top", false, 0.0F);
        float var3 = this.a(var1, "margin-bottom", false, 0.0F);
        float var4 = this.a(var1, "margin-left", false, 0.0F);
        float var5 = this.a(var1, "margin-right", false, 0.0F);
        awy var6 = new awy();
        var6.top = (int) var2;
        var6.bottom = (int) var3;
        var6.left = (int) var4;
        var6.right = (int) var5;
        return var6;
    }

    public jM_q e(CSSValue var1) {
        if (var1 == null) {
            return jM_q.apd;
        } else if (var1.getCssValueType() == 2) {
            CSSValueList var2 = (CSSValueList) var1;
            return new jM_q(c(var2.item(0)), c(var2.item(1)));
        } else {
            return jM_q.apd;
        }
    }
}
