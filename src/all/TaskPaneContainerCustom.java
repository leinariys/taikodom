package all;

import javax.swing.*;
import java.awt.*;

public class TaskPaneContainerCustom extends BaseItemFactory {
    private static final LogPrinter logger = LogPrinter.setClass(TaskPaneContainerCustom.class);

    /**
     * Создание базового Component интерфейса
     * Вызывается из class BaseItem
     *
     * @param var1
     * @param var2
     * @param var3
     * @return
     */
    @Override
    protected JComponent CreatComponentCustom(MapKeyValue var1, Container var2, XmlNode var3) {
        return new TaskPaneContainerCustomWrapper();
    }

    protected void a(MapKeyValue var1, TaskPaneContainerCustomWrapper var2, XmlNode var3) {
        super.createLayout(var1, var2, var3);
        Integer var4 = checkValueAttribut((XmlNode) var3, (String) "divisorOrder", (LogPrinter) logger);
        if (var4 != null) {
            var2.nm(var4.intValue());
        }

    }
}
