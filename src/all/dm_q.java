package all;

import gnu.trove.THashMap;

import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.channels.SelectableChannel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantLock;

/**
 * aqx_q
 */
public class dm_q implements UV_q, aGP, ahG {
    private static final int yI = -2;
    private static final int yJ = -1;
    private static final boolean yK = "true".equalsIgnoreCase(System.getProperty("nio.savemessages"));
    static LogPrinter logger = LogPrinter.K(dm_q.class);
    // $FF: synthetic field
    private static int[] yY;
    protected final JB yT;//Первре сообщение
    protected final JB yU;//Второе сообщение
    private final ArrayBlockingQueue yL = new ArrayBlockingQueue(131072);
    private final b yP;
    protected agX yQ;
    protected String versionString;//Пример 1.0.4933.55
    protected InetAddress yX;//локальный адрес машины //после подключения удалённой taikodom.corpscorp.online/153.92.6.117
    private ReentrantLock yM = new ReentrantLock();
    private Map yN = new THashMap();
    private LZ[] yO = new LZ[0];
    private int yR;
    private int yS;
    private AtomicLong yV;
    private boolean yW;

    /**
     * подготовка 2-ух сообщений и полученияе локального адреса машины
     * при создании класса aqx_q
     *
     * @throws IOException
     */
    public dm_q() throws IOException {
        this.yP = mP.a(b.a.h, (InetAddress) null);
        this.yV = new AtomicLong();
        this.yW = true;
        try {
            this.yX = InetAddress.getLocalHost();//Получили локальный адесс Lein-LT/192.168.56.1
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        this.yQ = new agX();
        ala var1 = new ala();//Создание буфера сообщения  размером 16 byte
        var1.writeByte(0);//16 рисная кодировка
        var1.writeByte(0);
        this.yT = new JB(var1);
        this.yT.setSpecial(true);
        var1 = new ala();//Создаём 2-ое сообщение
        var1.writeByte(0);
        var1.writeByte(1);
        this.yU = new JB(var1);//Оборачиваем
        this.yU.setSpecial(true);//указываем что специальное /особое
    }

    // $FF: synthetic method
    static int[] jN() {
        int[] var10000 = yY;
        if (yY != null) {
            return var10000;
        } else {
            int[] var0 = new int[LZ.a_q.values().length];

            try {
                var0[LZ.a_q.eQq.ordinal()] = 4;
            } catch (NoSuchFieldError var7) {
                ;
            }

            try {
                var0[LZ.a_q.eQp.ordinal()] = 3;
            } catch (NoSuchFieldError var6) {
                ;
            }

            try {
                var0[LZ.a_q.eQt.ordinal()] = 7;
            } catch (NoSuchFieldError var5) {
                ;
            }

            try {
                var0[LZ.a_q.eQs.ordinal()] = 6;
            } catch (NoSuchFieldError var4) {
                ;
            }

            try {
                var0[LZ.a_q.eQn.ordinal()] = 1;
            } catch (NoSuchFieldError var3) {
                ;
            }

            try {
                var0[LZ.a_q.eQo.ordinal()] = 2;
            } catch (NoSuchFieldError var2) {
                ;
            }

            try {
                var0[LZ.a_q.eQr.ordinal()] = 5;
            } catch (NoSuchFieldError var1) {
                ;
            }

            yY = var0;
            return var0;
        }
    }

    private void a(LZ var1) {
        this.yM.lock();

        try {
            THashMap var2 = new THashMap(this.yN);
            var2.put(var1.bgt(), var1);
            this.yO = (LZ[]) var2.values().toArray(new LZ[this.yN.size()]);
            this.yN = var2;
        } finally {
            this.yM.unlock();
        }

    }

    private void b(LZ var1) {
        this.yM.lock();

        try {
            THashMap var2 = new THashMap(this.yN);
            var2.remove(var1.bgt());
            this.yO = (LZ[]) var2.values().toArray(new LZ[this.yN.size()]);
            this.yN = var2;
        } finally {
            this.yM.unlock();
        }

    }

    public int getNumberOfConnections() {
        return this.yO.length;
    }

    public void b(byte[] var1, int var2, int var3) throws IOException {
        LZ[] var4 = this.yO;
        JB var5 = new JB(var1, var2, var3);
        LZ[] var9 = var4;
        int var8 = var4.length;

        for (int var7 = 0; var7 < var8; ++var7) {
            LZ var6 = var9[var7];
            this.e(var6, var5);
        }

    }

    public void a(b var1, byte[] var2, int var3, int var4) throws IOException {
        if (var1 == this.yP) {
            try {
                this.yL.put(new aBB(var1, var2, var3, var4));
            } catch (InterruptedException var7) {
                throw new RuntimeException("Interrupted while trying to_q loopback", var7);
            }
        } else {
            ha var5 = (ha) var1;
            LZ var6 = (LZ) var5.xR();
            if (var6 == null) {
                var6 = (LZ) this.yN.get(var1);
                if (var6 != null) {
                    var5.x(var6);
                    this.e(var6, new JB(var2, var3, var4));
                } else {
                    System.err.println("address not found: " + var1);
                    if (var1 == null) {
                        throw new IllegalArgumentException("Null address");
                    }
                }
            } else {
                this.e(var6, new JB(var2, var3, var4));
            }
        }
    }

    public aBB jG() throws InterruptedException {
        aBB var1 = (aBB) this.yL.take();
        return var1;
    }

    public void close() {
        this.yQ.close();
    }

    public b jH() {
        return this.yP;
    }

    public Collection a(b.a var1) {
        LZ[] var2 = this.yO;
        ArrayList var3 = new ArrayList();
        LZ[] var7 = var2;
        int var6 = var2.length;

        for (int var5 = 0; var5 < var6; ++var5) {
            LZ var4 = var7[var5];
            if (var4.bgt().xQ() == var1) {
                var3.add(var4.bgt());
            }
        }

        return var3;
    }

    public boolean isUp() {
        return this.yQ.isRunning();
    }

    public void start() {
        this.yQ.start();
    }

    public void a(LZ var1, JB var2) throws InterruptedException, IOException {
        if (var2.isSpecial()) {
            byte[] var3 = var2.cJn();
            int var4 = var2.getOffset();
            if (var3[var4] == 0 && var3[var4 + 1] == 0) {
                this.e(var1, this.yU);
            } else if (var3[var4 + 0] != 0 || var3[var4 + 1] != 1) {
                throw new IllegalArgumentException("Invalid special message: " + var3[0] + "  " + var3[1]);
            }
        } else if (var1.bgq() == LZ.a_q.eQr) {
            this.yL.put(var2);
        } else {
            switch (jN()[var1.bgq().ordinal()]) {
                case 1:
                    this.b(var1, var2);
                    return;
                case 2:
                    this.c(var1, var2);
                    return;
                case 3:
                    this.d(var1, var2);
                    return;
                default:
            }
        }
    }

    void c(LZ var1) throws IOException {
        ala var2 = new ala();
        var2.writeInt(LZ.a_q.eQn.ordinal());
        var2.writeInt(this.yR);
        var2.writeUTF(this.versionString);
        byte[] var3 = var2.toByteArray();
        this.e(var1, new JB(var3));
    }

    protected JB a(LZ.a_q var1, int var2) {
        ala var3 = new ala();
        var3.writeInt(var1.ordinal());
        var3.writeInt(var2);
        byte[] var4 = var3.toByteArray();
        return new JB(var4);
    }

    protected int a(LZ.a_q var1, JB var2) throws EOFException {
        aMf var3 = new aMf(var2.cJn(), var2.getOffset(), var2.getLength());
        int var4 = var3.readInt();
        if (var4 != var1.ordinal()) {
            Thread.dumpStack();
            throw new IllegalStateException("Read state " + var4 + " expected " + var2);
        } else {
            return var3.readInt();
        }
    }

    protected void b(LZ var1, JB var2) throws IOException {
        byte[] var3 = var2.cJn();
        int var4 = var2.getOffset();
        int var5 = var2.getLength();
        aMf var6 = new aMf(var3, var4, var5);
        byte var7 = 0;
        int var8 = var6.readInt();
        if (var8 != LZ.a_q.eQn.ordinal()) {
            throw new IllegalStateException("Read state " + var8 + " expected " + LZ.a_q.eQn.ordinal());
        } else {
            if (var6.readInt() != this.yS) {
                var7 = -1;
            }

            String var9 = var6.readUTF();
            if (!this.versionString.equals(var9)) {
                this.R(var9);
                var7 = -2;
            }

            if (var7 != 0) {
                var1.a(LZ.a_q.eQs);
            } else {
                var1.a(LZ.a_q.eQo);
            }

            this.e(var1, this.a(LZ.a_q.eQo, var7));
        }
    }

    protected void R(String var1) {
    }

    protected void c(LZ var1, JB var2) throws IOException {
        int var3 = this.a(LZ.a_q.eQo, var2);
        if (var3 == 0) {
            var1.a(var1.bgo() ? LZ.a_q.eQp : LZ.a_q.eQr);
        } else {
            var1.bgv();
        }

    }

    protected void d(LZ var1, JB var2) throws EOFException, UTFDataFormatException, IOException {
        throw new jx();
    }

    protected void d(LZ var1) {
        this.a(var1);
    }

    protected void e(LZ var1) {
        logger.info("Disconnected " + var1.bgt());
        this.b(var1);
    }

    public UV_q jI() {
        return this;
    }

    public aGP jJ() {
        return this;
    }

    public boolean iX() {
        return this.yQ.isRunning();
    }

    public void all(om_q var1) {
        throw new jx();
    }

    public b jK() {
        return this.yP;
    }

    public InetAddress jh() {
        return this.yX;
    }

    public boolean iY() {
        return true;
    }

    public void c(b var1) {
        this.yM.lock();

        try {
            LZ var2 = (LZ) this.yN.get(var1);

            try {
                if (var2 != null) {
                    SelectableChannel var3 = var2.getChannel();
                    if (var3 != null) {
                        var3.close();
                    }
                }
            } catch (IOException var7) {
                logger.warn("Ignoring", var7);
            }
        } finally {
            this.yM.unlock();
        }

    }

    protected void e(LZ var1, JB var2) throws IOException {
        if (yK) {
            this.yV.incrementAndGet();
            (new File("messages")).mkdir();
            FileOutputStream var3 = new FileOutputStream("messages/" + this.getClass().getName() + "-" + String.format("%06d", this.yV.get()) + ".msg");
            var3.write(var2.cJn(), var2.getOffset(), var2.getLength());
            var3.flush();
            var3.close();
        }

        var1.c(var2);
    }

    /*
       public void all(adH var1) {
       }
    */
    public void a(int var1, int[] var2, int[] var3, boolean var4) throws IOException {
        throw new UnsupportedOperationException();
    }

    public void af(int var1) throws IOException {
        throw new UnsupportedOperationException();
    }

    public long ja() {
        throw new UnsupportedOperationException();
    }

    public long jf() {
        throw new UnsupportedOperationException();
    }

    public long jg() {
        throw new UnsupportedOperationException();
    }

    public long jc() {
        throw new UnsupportedOperationException();
    }

    public long jb() {
        throw new UnsupportedOperationException();
    }

    public long je() {
        throw new UnsupportedOperationException();
    }

    public long jd() {
        throw new UnsupportedOperationException();
    }

    public void setCompression(boolean var1) {
        this.yW = var1;
    }

    public boolean jL() {
        return this.yW;
    }

    public LZ[] jM() {
        return this.yO;
    }
}
