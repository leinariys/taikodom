package all;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.awt.*;

class aqV extends CellRendererPane {
    protected ComponentManager grc;

    public aqV(String var1) {
        this.grc = new ComponentManager(var1, this);
    }

    public aqV() {
        this("panel");
    }

    public Insets getInsets() {
        return BorderWrapper.getInstance().getBorderInsets(this);
    }

    public void paintComponent(Graphics var1, Component var2, Container var3, int var4, int var5, int var6, int var7, boolean var8) {
        RM.a(var1, this, this.grc.Vp(), new Rectangle(var4, var5, var6, var7));

        BorderWrapper.getInstance().paintBorder(this, var1, var4, var5, var6, var7);
        Insets var9 = BorderWrapper.getInstance().getBorderInsets(this);
        var4 += var9.left;
        var5 += var9.top;
        var6 -= var9.left + var9.right;
        var7 -= var9.top + var9.bottom;
        if (var2 == null) {
            if (var3 != null) {
                Color var17 = var1.getColor();
                var1.setColor(var3.getBackground());
                var1.fillRect(var4, var5, var6, var7);
                var1.setColor(var17);
            }

        } else {
            if (var2.getParent() != this) {
                this.add(var2);
            }

            var2.setBounds(var4, var5, var6, var7);
            if (var8) {
                if (var2 instanceof PanelCustomWrapper) {
                    synchronized (((PanelCustomWrapper) var2).getTreeLock()) {
                        ((PanelCustomWrapper) var2).validateTree();
                    }
                } else if (!(var2 instanceof JLabel) && !(var2 instanceof JTextComponent)) {
                    var2.validate();
                }
            }

            boolean var10 = false;
            if (var2 instanceof JComponent && ((JComponent) var2).isDoubleBuffered()) {
                var10 = true;
                ((JComponent) var2).setDoubleBuffered(false);
            }

            Graphics var11 = var1.create(var4, var5, var6, var7);

            try {
                var2.paint(var11);
            } finally {
                var11.dispose();
            }

            if (var10 && var2 instanceof JComponent) {
                ((JComponent) var2).setDoubleBuffered(true);
            }

            var2.setBounds(-var6, -var7, 0, 0);
        }
    }
}
