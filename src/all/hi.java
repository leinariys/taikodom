package all;

import java.io.FilterWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;

/**
 * Запись в файл статистики потоков
 */
public class hi extends FilterWriter {
    private String TM;
    private boolean TN = false;
    private ArrayList TO;

    public hi(Writer var1) {
        super(var1);
    }

    public String getIndent() {
        return this.TM;
    }

    public void az(String var1) {
        this.TM = var1;
    }

    public void aA(String var1) {
        this.push();
        this.TM = this.TM + var1;
    }

    public void push() {
        if (this.TO == null) {
            this.TO = new ArrayList();
        }

        this.TO.add(this.TM);
    }

    public void pop() {
        if (this.TO != null && this.TO.size() > 0) {
            this.TM = (String) this.TO.remove(this.TO.size() - 1);
        }

    }

    public void write(String var1, int var2, int var3) throws IOException {
        while (true) {
            if (var2 < var1.length()) {
                --var3;
                if (var3 >= 0) {
                    this.write(var1.charAt(var2));
                    ++var2;
                    continue;
                }
            }

            return;
        }
    }

    public void write(char[] var1, int var2, int var3) throws IOException {
        while (true) {
            if (var2 < var1.length) {
                --var3;
                if (var3 >= 0) {
                    this.write(var1[var2]);
                    ++var2;
                    continue;
                }
            }

            return;
        }
    }

    public void write(int var1) throws IOException {
        String var2;
        if (var1 == 10) {
            this.out.write(var1);
            var2 = this.getIndent();
            if (var2 != null) {
                this.out.write(var2);
            }

            this.TN = false;
        } else {
            if (this.TN) {
                this.out.write(10);
                var2 = this.getIndent();
                if (var2 != null) {
                    this.out.write(var2);
                }
            }

            if (var1 == 13) {
                this.TN = true;
            }

            this.out.write(var1);
        }

    }
}
