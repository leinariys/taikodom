package all;

import javax.swing.*;
import java.awt.*;

public class PictureCustom extends BaseItem {
    private static final LogPrinter logger = LogPrinter.setClass(PictureCustom.class);

    /**
     * Создание базового Component интерфейса
     * Вызывается из class BaseItem
     *
     * @param var1
     * @param var2
     * @param var3
     * @return
     */
    @Override
    protected JComponent CreatComponentCustom(MapKeyValue var1, Container var2, XmlNode var3) {
        return new PictureCustomWrapper(var1.getBaseUiTegXml().getLoaderImage());
    }

    protected void a(MapKeyValue var1, PictureCustomWrapper var2, XmlNode var3) {
        super.applyAttribute(var1, (Component) var2, (XmlNode) var3);
        Integer var4 = checkValueAttribut((XmlNode) var3, (String) "width", (LogPrinter) logger);
        var4 = var4 == null ? -1 : var4.intValue();
        Integer var5 = checkValueAttribut((XmlNode) var3, (String) "height", (LogPrinter) logger);
        var5 = var5 == null ? -1 : var5.intValue();
        var2.setImageSize(var4.intValue(), var5.intValue());
        String var6 = var3.getAttribute("src");
        if (var6 != null) {
            var2.setImage(var6);
        }

        var6 = var3.getAttribute("text");
        if (var6 != null) {
            var2.setText(var6);
        }

    }

}
