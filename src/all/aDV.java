package all;

public class aDV extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public aDV() {
    }

    public aDV(String var1) {
        super(var1);
    }

    public aDV(Throwable var1) {
        super(var1);
    }

    public aDV(String var1, Throwable var2) {
        super(var1, var2);
    }
}
