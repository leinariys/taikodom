package all;

/**
 * MouseMoveEvent
 * class CommandTranslatorAddon
 */
public final class aIU extends aOG {
    public static adl idz = new adl("MouseMoveEvent");
    private final int aCK;
    private final int aCL;
    private final int idx;
    private final int idy;
    private final float x;
    private final float y;
    private final float dx;
    private final float dy;
    private final int modifiers;

    public aIU(float var1, float var2, float var3, float var4, int var5, int var6, int var7, int var8, int var9) {
        this.x = var1;
        this.y = var2;
        this.dx = var3;
        this.dy = var4;
        this.aCK = var5;
        this.aCL = var6;
        this.idx = var7;
        this.idy = var8;
        this.modifiers = var9;
    }

    public final float getDx() {
        return this.dx;
    }

    public final float getDy() {
        return this.dy;
    }

    public final float getX() {
        return this.x;
    }

    public final float getY() {
        return this.y;
    }

    public final int dfi() {
        return this.idx;
    }

    public final int dfj() {
        return this.idy;
    }

    public final int getScreenX() {
        return this.aCK;
    }

    public final int getScreenY() {
        return this.aCL;
    }

    public adl Fp() {
        return idz;
    }

    public int getModifiers() {
        return this.modifiers;
    }
}
