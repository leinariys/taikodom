package all;

import javax.print.attribute.standard.MediaSize;
import javax.swing.*;
import javax.swing.UIDefaults.LazyInputMap;
import javax.swing.plaf.InsetsUIResource;
import javax.swing.plaf.basic.BasicLookAndFeel;
import java.awt.*;

/**
 * Элементы интерфейса
 */
public class BasicLookAndFeelSpaceEngine extends BasicLookAndFeel {

    private UIDefaultsUI uiDefaults;

    //определяются шрифты, цвета, горячие клавиши, бордеры и прочие мелочи
    @Override
    protected void initComponentDefaults(UIDefaults var1) {
        super.initComponentDefaults(var1);
        LazyInputMap var2 = new LazyInputMap(new Object[]{"ctrl C", "copy-to_q-clipboard", "ctrl V", "paste-from-clipboard", "ctrl X", "cut-to_q-clipboard", "COPY", "copy-to_q-clipboard", "PASTE", "paste-from-clipboard", "CUT", "cut-to_q-clipboard", "control INSERT", "copy-to_q-clipboard", "shift INSERT", "paste-from-clipboard", "shift DELETE", "cut-to_q-clipboard", "shift LEFT", "selection-backward", "shift KP_LEFT", "selection-backward", "shift RIGHT", "selection-forward", "shift KP_RIGHT", "selection-forward", "ctrl LEFT", "caret-previous-word", "ctrl KP_LEFT", "caret-previous-word", "ctrl RIGHT", "caret-next-word", "ctrl KP_RIGHT", "caret-next-word", "ctrl shift LEFT", "selection-previous-word", "ctrl shift KP_LEFT", "selection-previous-word", "ctrl shift RIGHT", "selection-next-word", "ctrl shift KP_RIGHT", "selection-next-word", "ctrl CreatAllComponentCustom", "select-all", "HOME", "caret-begin-line", "END", "caret-end-line", "shift HOME", "selection-begin-line", "shift END", "selection-end-line", "BACK_SPACE", "delete-previous", "shift BACK_SPACE", "delete-previous", "ctrl H", "delete-previous", "DELETE", "delete-next", "ctrl DELETE", "delete-next-word", "ctrl BACK_SPACE", "delete-previous-word", "RIGHT", "caret-forward", "LEFT", "caret-backward", "KP_RIGHT", "caret-forward", "KP_LEFT", "caret-backward", "ENTER", "notify-field-accept", "ctrl BACK_SLASH", "unselect", "control shift O", "toggle-componentOrientation"});
        LazyInputMap var3 = new LazyInputMap(new Object[]{"ctrl C", "copy-to_q-clipboard", "ctrl V", "paste-from-clipboard", "ctrl X", "cut-to_q-clipboard", "COPY", "copy-to_q-clipboard", "PASTE", "paste-from-clipboard", "CUT", "cut-to_q-clipboard", "control INSERT", "copy-to_q-clipboard", "shift INSERT", "paste-from-clipboard", "shift DELETE", "cut-to_q-clipboard", "shift LEFT", "selection-backward", "shift KP_LEFT", "selection-backward", "shift RIGHT", "selection-forward", "shift KP_RIGHT", "selection-forward", "ctrl LEFT", "caret-begin-line", "ctrl KP_LEFT", "caret-begin-line", "ctrl RIGHT", "caret-end-line", "ctrl KP_RIGHT", "caret-end-line", "ctrl shift LEFT", "selection-begin-line", "ctrl shift KP_LEFT", "selection-begin-line", "ctrl shift RIGHT", "selection-end-line", "ctrl shift KP_RIGHT", "selection-end-line", "ctrl CreatAllComponentCustom", "select-all", "HOME", "caret-begin-line", "END", "caret-end-line", "shift HOME", "selection-begin-line", "shift END", "selection-end-line", "BACK_SPACE", "delete-previous", "shift BACK_SPACE", "delete-previous", "ctrl H", "delete-previous", "DELETE", "delete-next", "RIGHT", "caret-forward", "LEFT", "caret-backward", "KP_RIGHT", "caret-forward", "KP_LEFT", "caret-backward", "ENTER", "notify-field-accept", "ctrl BACK_SLASH", "unselect", "control shift O", "toggle-componentOrientation"});
        LazyInputMap var4 = new LazyInputMap(new Object[]{"ctrl C", "copy-to_q-clipboard", "ctrl V", "paste-from-clipboard", "ctrl X", "cut-to_q-clipboard", "COPY", "copy-to_q-clipboard", "PASTE", "paste-from-clipboard", "CUT", "cut-to_q-clipboard", "control INSERT", "copy-to_q-clipboard", "shift INSERT", "paste-from-clipboard", "shift DELETE", "cut-to_q-clipboard", "shift LEFT", "selection-backward", "shift KP_LEFT", "selection-backward", "shift RIGHT", "selection-forward", "shift KP_RIGHT", "selection-forward", "ctrl LEFT", "caret-previous-word", "ctrl KP_LEFT", "caret-previous-word", "ctrl RIGHT", "caret-next-word", "ctrl KP_RIGHT", "caret-next-word", "ctrl shift LEFT", "selection-previous-word", "ctrl shift KP_LEFT", "selection-previous-word", "ctrl shift RIGHT", "selection-next-word", "ctrl shift KP_RIGHT", "selection-next-word", "ctrl CreatAllComponentCustom", "select-all", "HOME", "caret-begin-line", "END", "caret-end-line", "shift HOME", "selection-begin-line", "shift END", "selection-end-line", "UP", "caret-up", "KP_UP", "caret-up", "DOWN", "caret-down", "KP_DOWN", "caret-down", "PAGE_UP", "page-up", "PAGE_DOWN", "page-down", "shift PAGE_UP", "selection-page-up", "shift PAGE_DOWN", "selection-page-down", "ctrl shift PAGE_UP", "selection-page-left", "ctrl shift PAGE_DOWN", "selection-page-right", "shift UP", "selection-up", "shift KP_UP", "selection-up", "shift DOWN", "selection-down", "shift KP_DOWN", "selection-down", "ENTER", "insert-break", "BACK_SPACE", "delete-previous", "shift BACK_SPACE", "delete-previous", "ctrl H", "delete-previous", "DELETE", "delete-next", "ctrl DELETE", "delete-next-word", "ctrl BACK_SPACE", "delete-previous-word", "RIGHT", "caret-forward", "LEFT", "caret-backward", "KP_RIGHT", "caret-forward", "KP_LEFT", "caret-backward", "TAB", "insert-tab", "ctrl BACK_SLASH", "unselect", "ctrl HOME", "caret-begin", "ctrl END", "caret-end", "ctrl shift HOME", "selection-begin", "ctrl shift END", "selection-end", "ctrl T", "next-link-action", "ctrl shift T", "previous-link-action", "ctrl SPACE", "activate-link-action", "control shift O", "toggle-componentOrientation"});
        Color var5 = new Color(0, 0, 0, 0);
        BorderWrapper var6 = new BorderWrapper();
        Object[] var7 = new Object[]{"Button.background", var1.get("control"), "Button.border", var6, "Button.focusInputMap", new LazyInputMap(new Object[]{"SPACE", "pressed", "released SPACE", "released", "ENTER", "pressed", "released ENTER", "released"}), "Button.foreground", var1.get("controlText"), "Button.margin", new InsetsUIResource(0, 0, 0, 0), "Button.select", var1.get("controlLightShadow"), "CheckBox.border", var6, "CheckBoxMenuItem.border", var6, "CheckBoxMenuItem.borderPainted", Boolean.TRUE, "ComboBox.ancestorInputMap", new LazyInputMap(new Object[]{"ESCAPE", "hidePopup", "PAGE_UP", "pageUpPassThrough", "PAGE_DOWN", "pageDownPassThrough", "HOME", "homePassThrough", "END", "endPassThrough", "ENTER", "enterPressed"}), "ComboBox.background", var5, "ComboBox.buttonBackground", "", "ComboBox.buttonDarkShadow", "", "ComboBox.buttonHighlight", "", "ComboBox.buttonShadow", "", "ComboBox.disabledBackground", "", "ComboBox.disabledForeground", "", "ComboBox.font", "", "ComboBox.foreground", var5, "ComboBox.selectionBackground", "", "ComboBox.selectionForeground", "", "ComboBox.timeFactor", new Long(1000L), "DesktopIcon.border", var6, "EditorPane.border", var6, "EditorPane.focusInputMap", var4, "FormattedTextField.border", var6, "InternalFrame.border", var6, "InternalFrame.closeIcon", null, "InternalFrame.iconifyIcon", null, "InternalFrame.maximizeIcon", null, "InternalFrame.minimizeIcon", null, "Label.border", var6, "List.border", var6, "Menu.border", var6, "Menu.borderPainted", Boolean.TRUE, "MenuBar.border", var6, "MenuItem.border", var6, "MenuItem.borderPainted", Boolean.TRUE, "OptionPane.border", var6, "Panel.border", var6, "PasswordField.background", var5, "PasswordField.border", var6, "PasswordField.focusInputMap", var3, "PasswordField.foreground", var5, "PopupMenu.border", var6, "ProgressBar.border", var6, "RootPane.border", var6, "RadioButton.border", var6, "RadioButtonMenuItem.border", var6, "RadioButtonMenuItem.borderPainted", Boolean.TRUE, "ScrollBar.background", var5, "ScrollBar.border", var6, "ScrollPane.border", var6, "ScrollPane.viewportBorder", var6, "ScrollPane.borderPainted", Boolean.TRUE, "Slider.border", var6, "Slider.borderPainted", Boolean.TRUE, "Spinner.border", var6, "Spinner.borderPainted", Boolean.TRUE, "Spinner.editorBorderPainted", Boolean.TRUE, "SplitPane.border", var6, "SplitPane.borderPainted", Boolean.TRUE, "SplitPaneDivider.border", var6, "SplitPaneDivider.borderPainted", Boolean.TRUE, "Table.border", var6, "Table.scrollPaneBorder", var6, "TableHeader.cellBorder", var6, "TableHeader.focusCellBorder", var6, "TextArea.border", var6, "TextArea.focusInputMap", var4, "TextField.border", var6, "TextField.focusInputMap", var2, "TextPane.border", var6, "TextPane.focusInputMap", var4, "TitledBorder.border", var6, "ToggleButton.background", var1.get("control"), "ToggleButton.border", var6, "ToggleButton.borderPainted", Boolean.TRUE, "ToggleButton.focusInputMap", new LazyInputMap(new Object[]{"SPACE", "pressed", "released SPACE", "released", "ENTER", "pressed", "released ENTER", "released"}), "ToggleButton.foreground", var1.get("controlText"), "ToggleButton.margin", new InsetsUIResource(0, 0, 0, 0), "ToggleButton.select", var1.get("controlLightShadow"), "ToolBar.border", var6, "ToolTip.border", var6, "Tree.border", var6, "Tree.selectionBorderColor", var6, "Tree.dropLineColor", var5, "Tree.editorBorder", var6, "Table.focusSelectedCellHighlightBorder", var6, "Table.focusCellHighlightBorder", var6, "Table.focusCellForeground", var5, "Table.focusCellBackground", var5};
        var1.putDefaults(var7);
    }

    //для «подмены» UI класса
    @Override
    protected void initClassDefaults(UIDefaults var1) {
        super.initClassDefaults(var1);
        UIManager.getDefaults().put("InternalFrame.iconButtonToolTip", "");
        Object[] uiDefaults = new Object[]
                {
                        // Label
                        "LabelUI", LabelUI.class.getName(),
                        "ToolTipUI", ToolTipUI.class.getName(),

                        // Button
                        "ButtonUI", ButtonUI.class.getName(),
                        "ToggleButtonUI", ToggleButtonUI.class.getName(),
                        "CheckBoxUI", CheckBoxUI.class.getName(),
                        "RadioButtonUI", MediaSize.NA.class.getName(),

                        // Menu
                        "MenuBarUI", MenuBarUI.class.getName(),
                        "MenuUI", MenuUI.class.getName(),
                        "PopupMenuUI", PopupMenuUI.class.getName(),
                        //"MenuItemUI", ,
                        "CheckBoxMenuItemUI", CheckBoxMenuItemUI.class.getName(),
                        "RadioButtonMenuItemUI", RadioButtonMenuItemUI.class.getName(),
                        "PopupMenuSeparatorUI", PopupMenuSeparatorUI.class.getName(),

                        // Separator
                        "SeparatorUI", SeparatorUI.class.getName(),

                        // Scroll
                        "ScrollBarUI", ScrollBarUI.class.getName(),
                        "ScrollPaneUI", ScrollPaneUI.class.getName(),

                        // Text
                        "TextFieldUI", TextFieldUI.class.getName(),
                        "PasswordFieldUI", PasswordFieldUI.class.getName(),
                        "FormattedTextFieldUI", FormattedTextFieldUI.class.getName(),
                        "TextAreaUI", TextAreaUI.class.getName(),
                        "EditorPaneUI", EditorPaneUI.class.getName(),
                        "TextPaneUI", TextPaneUI.class.getName(),

                        // Toolbar
                        "ToolBarUI", ToolBarUI.class.getName(),
                        "ToolBarSeparatorUI", ToolBarSeparatorUI.class.getName(),

                        // Table
                        "TableUI", TableUI.class.getName(),
                        "TableHeaderUI", TableHeaderUI.class.getName(),
                        // Chooser
                        "ColorChooserUI", ColorChooserUI.class.getName(),
                        //"FileChooserUI", ,

                        // Container
                        "PanelUI", PanelUI.class.getName(),
                        "ViewportUI", ViewportUI.class.getName(),
                        "RootPaneUI", RootPaneUI.class.getName(),
                        "TabbedPaneUI", TabbedPaneUI.class.getName(),
                        "SplitPaneUI", SplitPaneUI.class.getName(),

                        // Complex components
                        "ProgressBarUI", ProgressBarUI.class.getName(),
                        "SliderUI", SliderUI.class.getName(),
                        "SpinnerUI", SpinnerUI.class.getName(),
                        "TreeUI", TreeUI.class.getName(),
                        "ListUI", ListUI.class.getName(),
                        "ComboBoxUI", ComboBoxUI.class.getName(),

                        // Desktop pane
                        "DesktopPaneUI", DesktopPaneUI.class.getName(),
                        "DesktopIconUI", DesktopIconUI.class.getName(),
                        "InternalFrameUI", InternalFrameUI.class.getName(),

                        // Option pane
                        "OptionPaneUI", OptionPaneUI.class.getName(),


                };
        var1.putDefaults(uiDefaults);
    }

    // Описание LaF
    public String getDescription() {
        return "SpaceEngine Look And Feel";
    }

    // Уникальный идентификатор LaF
    public String getID() {
        return "SpaceEngine";
    }

    // Уникальный идентификатор LaF
    public String getName() {
        return "SpaceEngine Look And Feel";
    }

    // Привязан ли данный LaF к текущей платформе (является ли его нативной имплементацией
    public boolean isNativeLookAndFeel() {
        return true;
    }

    // Поддерживается ли данный LaF на текущей платформе
    public boolean isSupportedLookAndFeel() {
        return true;
    }

    public UIDefaults getDefaults() {
        if (this.uiDefaults == null) {
            this.uiDefaults = new UIDefaultsUI(610, 0.75F);
            this.initClassDefaults(this.uiDefaults);//для «подмены» UI класса
            this.initSystemColorDefaults(this.uiDefaults); // стандартные цвета различных элементых
            this.initComponentDefaults(this.uiDefaults);//определяются шрифты, цвета, горячие клавиши, бордеры и прочие мелочи
        }

        return this.uiDefaults;
    }
}
