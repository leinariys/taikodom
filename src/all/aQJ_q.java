package all;

import org.w3c.dom.css.CSSRule;
import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.css.CSSStyleRule;
import org.w3c.dom.css.CSSStyleSheet;

import java.io.Serializable;
import java.io.StringReader;

public class aQJ_q implements Serializable, CSSStyleRule {
    //Тело всех сыы селекторов из файла
    private SS qq = null;
    private CSSRule qr = null;
    private SelectorList selectorName = null;
    //Один селектор разбитый только на правила , но ещё не разбит на правило-значение
    private CSSStyleDeclaration style = null;//aHx

    public aQJ_q(SS var1, CSSRule var2, SelectorList var3) {
        this.qq = var1;
        this.qr = var2;
        this.selectorName = var3;
    }

    public short getType() {
        return 1;
    }

    public String getCssText() {
        return this.getSelectorText() + " " + this.getStyle().toString();
    }

    public void setCssText(String var1) {
        if (this.qq != null && this.qq.isReadOnly()) {
            throw new qN((short) 7, 2);
        } else {
            try {
                InputSource var2 = new InputSource(new StringReader(var1));
                ParserCss var3 = ParserCss.getParserCss();
                CSSRule var4 = var3.f(var2);
                if (var4.getType() == 1) {
                    this.selectorName = ((aQJ_q) var4).selectorName;
                    this.style = ((aQJ_q) var4).style;
                } else {
                    throw new qN((short) 13, 4);
                }
            } catch (CSSException var5) {
                throw new qN((short) 12, 0, var5.getMessage());
            } /*catch (IOException var6) {
            throw new qN((short)12, 0, var6.getMessage());
         }*/
        }
    }

    public CSSStyleSheet getParentStyleSheet() {
        return this.qq;
    }

    public CSSRule getParentRule() {
        return this.qr;
    }

    public String getSelectorText() {
        return this.selectorName.toString();
    }

    public void setSelectorText(String var1) {
        if (this.qq != null && this.qq.isReadOnly()) {
            throw new qN((short) 7, 2);
        } else {
            try {
                InputSource var2 = new InputSource(new StringReader(var1));
                ParserCss var3 = ParserCss.getParserCss();
                this.selectorName = var3.g(var2);
            } catch (CSSException var4) {
                throw new qN((short) 12, 0, var4.getMessage());
            }/* catch (IOException var5) {
            throw new qN((short)12, 0, var5.getMessage());
         }*/
        }
    }

    public SelectorList dqd() {
        return this.selectorName;
    }

    public CSSStyleDeclaration getStyle() {
        return this.style;
    }

    public void a(aHx var1) {
        this.style = var1;
    }

    public String toString() {
        return this.getCssText();
    }
}
