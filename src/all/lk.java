package all;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

public class lk {
    private BufferedReader in;

    public lk(InputStream var1) {
        this.in = new BufferedReader(new InputStreamReader(var1));
    }

    public static void main(String[] var0) throws IOException {
        System.out.println("DEBUG2 -" + (new File("ob-client.map")).exists());
        (new lk(new FileInputStream("ob-client.map"))).getMap();
    }

    public Map getMap() throws IOException {
        HashMap var1 = new HashMap();

        String var2;
        while ((var2 = this.in.readLine()) != null) {
            if (!var2.startsWith(" ")) {
                StringTokenizer var3 = new StringTokenizer(var2, " ");
                String var4 = var3.nextToken();
                var3.nextToken();
                String var5 = var3.nextToken();
                var5 = var5.substring(0, var5.length() - 1);
                var1.put(var4, var5);
            }
        }

        return var1;
    }
}
