package all;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class amS_q extends ks_q {
    private static final int MIN_EXPONENT = -30;
    private static final int MAX_EXPONENT = 31;
    private static final int gbS = 16;
    private static final float MIN_VALUE = Float.intBitsToFloat(813694976);
    private static final float MAX_VALUE = Float.intBitsToFloat(1333772288);
    private static final int gbT = 8192;
    private static final float TOLERANCE = Float.intBitsToFloat(1065369599) - 1.0F;
    public static int gbL = -126;
    public static amS_q gbM = new amS_q();
    public static int gbN = 6;
    public static int gbO = 9;
    public static int gbP = 511;
    public static int gbQ = 63;
    public static int gbR = -96;

    public static int rb(int var0) {
        int var1 = 0;

        for (int var2 = 16; var2 > 0; var2 >>= 1) {
            if (var0 >>> var2 != 0) {
                var0 >>>= var2;
                var1 |= var2;
            }
        }

        return var1 + var0;
    }

    public int Kc() {
        return 16;
    }

    public int Ka() {
        return 31;
    }

    public int Kb() {
        return -30;
    }

    public int Ke() {
        return 6;
    }

    public int Kd() {
        return 9;
    }

    public float Kf() {
        return MAX_VALUE;
    }

    public float Kg() {
        return MIN_VALUE;
    }

    public int aT(float var1) {
        int var2 = Float.floatToIntBits(var1);
        int var3 = var2 & 8388607;
        int var4 = var2 >> 23 & 255;
        if (var4 != 255) {
            if ((var3 & 8192) != 0) {
                var2 += 8192;
                var4 = var2 >> 23 & 255;
                var3 = var2 & 8388607;
            }

            if (var4 != 0) {
                var4 -= 96;
                if (var4 <= 0 && var4 > -126) {
                    if (var4 > -9) {
                        var3 = (var3 | 8388608) >>> 1 - var4;
                    } else {
                        var3 = 0;
                    }

                    var4 = 0;
                } else if (var4 >= 63) {
                    var4 = 62;
                    var3 = 8388607;
                }
            } else {
                var3 = 0;
            }
        }

        int var5 = var2 >> 16 & '耀' | (var4 & 63) << 9 | var3 >> 14;
        return var5;
    }

    public float cF(int var1) {
        int var2 = var1 >> 9 & 63;
        int var3 = var1 & 511;
        int var4;
        if (var2 == 63) {
            var2 = 255;
        } else if (var2 != 0) {
            var2 += 96;
        } else if (var3 != 0) {
            var4 = 9 - rb(var3) + 1;
            var2 = -30 - var4 + 127;
            var3 = var3 << var4 & 511;
        }

        var4 = var1 << 16 & Integer.MIN_VALUE | var2 << 23 | var3 << 14;
        return Float.intBitsToFloat(var4);
    }

    public void a(ObjectOutput var1, float var2) throws IOException {
        var1.writeShort(this.aT(var2));
    }

    public float d(ObjectInput var1) throws IOException {
        return this.cF(var1.readUnsignedShort());
    }

    public float Kh() {
        return TOLERANCE;
    }

    public boolean Ki() {
        return true;
    }

    public boolean Kj() {
        return true;
    }
}
