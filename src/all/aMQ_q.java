package all;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 *
 */
public class aMQ_q {

    private ConcurrentHashMap hCq = new ConcurrentHashMap();

    /**
     * Добавить в список
     *
     * @param var1 Привер (ключ = значение) endVideo из аддона SplashScreen  и  ExclusiveVideoPlayer
     * @param var2 Обработчик события
     */
    public void put(Object var1, Object var2) {
        Collection var3 = (Collection) this.hCq.get(var1);
        if (var3 == null) {
            var3 = this.djH();
            Collection var4 = (Collection) this.hCq.putIfAbsent(var1, var3);
            if (var4 != null) {
                var3 = var4;
            }
        }

        var3.add(var2);
    }

    protected Collection djH() {
        return new CopyOnWriteArraySet();
    }

    public void aQ(Object var1) {
        Iterator var3 = this.hCq.values().iterator();

        while (var3.hasNext()) {
            Collection var2 = (Collection) var3.next();
            var2.remove(var1);
        }

    }

    public void k(Object var1, Object var2) {
        Collection var3 = (Collection) this.hCq.get(var1);
        if (var3 != null) {
            var3.remove(var2);
        }

    }

    public Collection aR(Object var1) {
        return (Collection) this.hCq.get(var1);
    }

    public void clear() {
        this.hCq.clear();
    }

    public Set entrySet() {
        return this.hCq.entrySet();
    }
}
