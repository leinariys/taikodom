package all;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicSplitPaneUI;
import java.awt.*;

/**
 * SplitPaneUI
 */
public class SplitPaneUI extends BasicSplitPaneUI implements aIh {
    private ComponentManager Rp;

    public SplitPaneUI(JSplitPane var1) {
        this.Rp = new ComponentManager("splitpane", var1);
        var1.setOpaque(false);
    }

    public static ComponentUI createUI(JComponent var0) {
        return new SplitPaneUI((JSplitPane) var0);
    }

    public void update(Graphics var1, JComponent var2) {
        RM.a((aIh) this, (Graphics) var1, (JComponent) var2);
        this.paint(var1, var2);
    }

    public Dimension getPreferredSize(JComponent var1) {
        RM.a((aIh) this, (JComponent) var1);
        return this.wy().c(var1, super.getPreferredSize(var1));
    }

    public Dimension getMinimumSize(JComponent var1) {
        RM.a((aIh) this, (JComponent) var1);
        return this.wy().b(var1, super.getMinimumSize(var1));
    }

    public Dimension getMaximumSize(JComponent var1) {
        RM.a((aIh) this, (JComponent) var1);
        return this.wy().a(var1, super.getMaximumSize(var1));
    }

    public ComponentManager wy() {
        return this.Rp;
    }

    /**
     * TODO похлже это public pv wy()
     *
     * @return Графический элемент
     */
    @Override
    public aeK wz() {
        return this.Rp;
    }
}
