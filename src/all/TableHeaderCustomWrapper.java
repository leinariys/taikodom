package all;

import javax.swing.*;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

public class TableHeaderCustomWrapper extends JTableHeader implements IComponentCustomWrapper {

    private static TableHeaderCustomWrapper ts;

    public TableHeaderCustomWrapper(TableColumnModel var1) {
        super(var1);
        ts = this;
        this.setBorder(new BorderWrapper());
        this.setBackground((Color) null);
    }

    protected TableCellRenderer createDefaultRenderer() {
        return new TableCellRendererCustomWrapper();
    }

    public String getElementName() {
        return "table-header";
    }

    public static class b extends JViewport implements IComponentCustomWrapper {

        private boolean isValid = false;

        public b() {
            this.setBackground(new Color(0, 0, 0, 0));
        }

        public void validate() {
            if (!this.isValid) {
                this.isValid = true;
                this.getParent().validate();
                this.isValid = false;
            }

        }

        public String getElementName() {
            return "background";
        }
    }

    public class TableCellRendererCustomWrapper implements TableCellRenderer {
        private static final long serialVersionUID = 1L;
        private Map yk = new HashMap();

        public Component getTableCellRendererComponent(JTable var1, Object var2, boolean var3, boolean var4, int var5, int var6) {
            JLabel var7 = (JLabel) this.yk.get(var2);
            if (var7 == null) {
                var7 = new JLabel((String) var2);
                TableHeaderCustomWrapper.ts.add(var7);
                this.yk.put(var2, var7);
            }

            return var7;
        }
    }
}
