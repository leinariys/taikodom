package all;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Свойства раздела настроек
 * Содержит список свойств
 */
public class ConfigManagerSection {

    /**
     * Содержит название раздела
     * И список свойств
     */
    private Map<String, ConfigManagerProperty> propertys = new HashMap<String, ConfigManagerProperty>();

    public ConfigManagerSection() {
    }

    public ConfigManagerSection(Map<String, ConfigManagerProperty> propertys) {
        this.propertys = propertys;
    }

    /**
     * Получить копию списка свойств
     *
     * @return
     */
    public ConfigManagerSection clone() {
        ConfigManagerSection newObj = new ConfigManagerSection();
        Iterator iterator = this.propertys.entrySet().iterator();

        while (iterator.hasNext()) {
            Entry item = (Entry) iterator.next();
            newObj.propertys.put((String) item.getKey(), ((ConfigManagerProperty) item.getValue()).clone());
        }
        return newObj;
    }

    /**
     * Получить список свойст
     *
     * @return
     */
    public Map getPropertys() {
        return this.propertys;
    }

    /**
     * Получит значение свойства
     *
     * @param keyProperty  имя свойства
     * @param typeProperty тип свойства
     * @return
     */
    private Object getValueProperty(String keyProperty, Class typeProperty) {
        ConfigManagerProperty itemProperty = this.propertys.get(keyProperty);
        if (itemProperty == null) {
            throw new InvalidSectionNameException(keyProperty);
        } else if (itemProperty.getTypeProperty() != typeProperty) {
            throw new ConfigManagerPropertyDifference(itemProperty.getTypeProperty(), typeProperty, keyProperty);
        } else {
            return itemProperty.getValueProperty();
        }
    }


    /**
     * Получит значение свойства или значение свойства по умолчанию
     *
     * @param keyProperty  имя свойства
     * @param typeProperty тип свойства
     * @param defaultObj   значение свойства по умолчанию
     * @return Если настройки нет, возвращает проверяемое значение. пример en
     */
    private Object getValueProperty(String keyProperty, Class typeProperty, Object defaultObj) {
        ConfigManagerProperty itemProperty = (ConfigManagerProperty) this.propertys.get(keyProperty);
        return itemProperty != null && itemProperty.getTypeProperty() == typeProperty
                ? itemProperty.getValueProperty()
                : defaultObj;
    }

    /**
     * Добавить Свойство
     *
     * @param keyProperty   имя свойства
     * @param typeProperty  тип свойства
     * @param valueProperty значение
     */
    private void setProperty(String keyProperty, Class typeProperty, Object valueProperty) {
        ConfigManagerProperty itemProperty = (ConfigManagerProperty) this.propertys.get(keyProperty);
        ConfigManagerProperty value = new ConfigManagerProperty(typeProperty, valueProperty, false);
        if (itemProperty == null || !itemProperty.equals(value)) {
            this.propertys.put(keyProperty, value);
        }
    }

    /**
     * Получить значение свойства
     *
     * @param keyProperty имя свойства
     * @return
     */
    public String getValuePropertyString(String keyProperty) {
        return (String) this.getValueProperty(keyProperty, String.class);
    }

    public String getValuePropertyString(setKeyValue var1) {
        return (String) this.getValueProperty(var1.getKey(), String.class);
    }

    /**
     * Получить значение свойства
     *
     * @param keyProperty   имя свойства
     * @param defaultString значение свойства по умолчанию
     * @return
     */
    public String getValuePropertyString(String keyProperty, String defaultString) {
        return (String) this.getValueProperty(keyProperty, String.class, defaultString);
    }

    public String getValuePropertyString(setKeyValue var1, String var2) {
        return (String) this.getValueProperty(var1.getKey(), String.class, var2);
    }

    /**
     * Добавить ключ и значение
     *
     * @param keyProperty   имя свойства
     * @param valueProperty значение свойства
     */
    public void setPropertyString(String keyProperty, String valueProperty) {
        this.setProperty(keyProperty, String.class, valueProperty);
    }

    public void setPropertyString(setKeyValue var1, String var2) {
        this.setProperty(var1.getKey(), String.class, var2);
    }

    /**
     * Добавить ключ и значение
     *
     * @param keyProperty имя свойства
     * @return
     */
    public Integer getValuePropertyInteger(String keyProperty) {
        return (Integer) this.getValueProperty(keyProperty, Integer.class);
    }


    public int getValuePropertyInteger(setKeyValue keyProperty) {
        return ((Integer) this.getValueProperty(keyProperty.getKey(), Integer.class)).intValue();
    }

    /**
     * Добавить ключ и значение
     *
     * @param keyProperty    имя свойства
     * @param defaultInteger значение свойства по умолчанию
     * @return
     */
    public Integer getValuePropertyInteger(String keyProperty, Integer defaultInteger) {
        return (Integer) this.getValueProperty(keyProperty, Integer.class, defaultInteger);
    }

    /**
     * Получение значения настройки из менеджере настроек
     *
     * @param var1 название настройки. пример language, username
     * @param var2 значение заглушка пример en
     * @return Если настройки нет, возвращает заглушку пример en
     */
    public Integer getValuePropertyInteger(setKeyValue var1, Integer var2) {
        return (Integer) this.getValueProperty(var1.getKey(), Integer.class, var2);
    }

    /**
     * Добавить ключ и значение
     *
     * @param keyProperty   имя свойства
     * @param valueProperty Значение
     */
    public void setPropertyInteger(String keyProperty, Integer valueProperty) {
        this.setProperty(keyProperty, Integer.class, valueProperty);
    }

    public void setPropertyInteger(setKeyValue var1, Integer var2) {
        this.setProperty(var1.getKey(), Integer.class, var2);
    }

    /**
     * Получить значение свойства
     *
     * @param keyProperty имя свойства
     * @return
     */
    public Float getValuePropertyFloat(String keyProperty) {
        return (Float) this.getValueProperty(keyProperty, Float.class);
    }

    /**
     * Получить значение свойства
     *
     * @param keyProperty    имя свойства
     * @param defaultInteger значение свойства по умолчанию
     * @return
     */
    public Float getValuePropertyFloat(String keyProperty, Float defaultInteger) {
        return (Float) this.getValueProperty(keyProperty, Float.class, defaultInteger);
    }


    public Float getValuePropertyFloat(setKeyValue var1) {
        return (Float) this.getValueProperty(var1.getKey(), Float.class);
    }

    public Float getValuePropertyFloat(setKeyValue var1, Float var2) {
        return (Float) this.getValueProperty(var1.getKey(), Float.class, var2);
    }

    /**
     * Добавить ключ и значение
     *
     * @param keyProperty   имя свойства
     * @param valueProperty Значение
     */
    public void setPropertyFloat(String keyProperty, Float valueProperty) {
        this.setProperty(keyProperty, Float.class, valueProperty);
    }

    public void setPropertyFloat(setKeyValue var1, Float var2) {
        this.setProperty(var1.getKey(), Float.class, var2);
    }


    /**
     * Получить значение свойства
     *
     * @param keyProperty имя свойства
     * @return
     */
    public Boolean getValuePropertyBoolean(String keyProperty) {
        return (Boolean) this.getValueProperty(keyProperty, Boolean.class);
    }

    /**
     * Получить значение свойства
     *
     * @param keyProperty    имя свойства
     * @param defaultInteger значение свойства по умолчанию
     * @return
     */
    public Boolean getValuePropertyBoolean(String keyProperty, Boolean defaultInteger) {
        return (Boolean) this.getValueProperty(keyProperty, Boolean.class, defaultInteger);
    }


    public Boolean getValuePropertyBoolean(setKeyValue var1) {
        return (Boolean) this.getValueProperty(var1.getKey(), Boolean.class);
    }


    public Boolean getValuePropertyBoolean(setKeyValue var1, Boolean var2) {
        return (Boolean) this.getValueProperty(var1.getKey(), Boolean.class, var2);
    }


    /**
     * Добавить ключ и значение
     *
     * @param keyProperty   имя свойства
     * @param valueProperty Значение
     */
    public void setPropertyBoolean(String keyProperty, Boolean valueProperty) {
        this.setProperty(keyProperty, Boolean.class, valueProperty);
    }

    public void setPropertyBoolean(setKeyValue var1, Boolean var2) {
        this.setProperty(var1.getKey(), Boolean.class, var2);
    }

    /**
     * Получить список с именими свойств
     *
     * @return
     */
    public Set getPropertysKeySet() {
        return this.propertys.keySet();
    }

    /**
     * Пометить все значения свойств как по умолчания
     */
    public void setMarkAsDefaultConfig() {
        Iterator var3 = this.propertys.entrySet().iterator();

        while (var3.hasNext()) {
            Entry property = (Entry) var3.next();
            ((ConfigManagerProperty) property.getValue()).setDefaultConfig(true);
        }

    }
}
