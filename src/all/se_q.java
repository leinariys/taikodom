package all;

import org.apache.commons.logging.Log;

import java.util.Collection;
import java.util.Date;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

public abstract class se_q implements aNl, arL {
    private static final Log logger = LogPrinter.setClass(se_q.class);
    private static ConcurrentHashMap hCq = new ConcurrentHashMap();
    protected Jz aGA;
    protected afk hCm;
    protected boolean disposed;
    volatile Fm.b hCo;
    ThreadLocal hCr = new ThreadLocal();
    private Xf_q hCl;
    private apO qT;
    private BH hCn;
    private long hCp = -1L;
    // private ch.all dqS;

    public static boolean bxL() {
        return aWq.dDA() != null;
    }

    public static String o(Xf_q var0) {
        return var0 != null ? var0.getClass().getName() + ":" + var0.bFf().hC().cqo() : "null";
    }

    public long cVe() {
        return this.hCp;
    }

    public void kv(long var1) {
        this.hCp = var1;
    }

    public int i(fm_q var1) {
        if (this.aGA.bGN() != null) {
            return 2;
        } else {
            int var2 = this.j(var1);
            return var2;
        }
    }

    private int j(fm_q var1) {
        if (this.bGZ()) {
            int var2 = var1.pA();
            if (var2 == 0) {
                var1.pP();
                return 0;
            } else {
                aWq var3 = aWq.dDA();
                if (var3 != null) {
                    if (var3.dDE()) {
                        ++var3.jgd;
                        if (var1.hm()) {
                            ++var3.enr;
                        }
                    } else {
                        ++var3.enx;
                        if (var1.hm()) {
                            ++var3.enr;
                            var3.k(var1);
                        }
                    }
                } else {
                    if (!this.aGA.bGo()) {
                        return 2;
                    }

                    if (var1.py()) {
                        return 2;
                    }
                }

                return var2;
            }
        } else {
            if (this.hCm.hF() && !var1.pL()) {/*
            if (this.yn().T().ho()) {
               return 0;
            }
*/
                if (this.disposed) {
                    return 0;
                }

                if (!this.ds()) {
                    this.disposed = true;
                    return 0;
                }
            }

            return this.bGY() ? var1.pY() : var1.pz();
        }
    }

    public Object all(Gr var1, ahM var2) {
        if (this.bGZ()) {
            throw new IllegalStateException("Invalid async call at server side: " + var1);
        } else {
            return null;
        }
    }

    public Object d(Gr var1) throws Throwable {
        sz var2 = this.aGA.bGN();
        if (var2 == null) {
            return this.PM().bGR().a(this, var1);
        } else {
            int var3 = this.j(var1.aQK());
            if (var3 == 0) {
                var2.b(this, var1.aQK());
                return var1.aQK().pN();
            } else {
                Xz var4 = var2.a((arL) this, (fm_q) var1.aQK());

                try {
                    Object var5;
                    switch (var3) {
                        case 1:
                            var5 = this.yn().a(var1);
                            break;
                        case 2:
                            var5 = this.PM().bGR().a(this, var1);
                            break;
                        case 3:
                            this.PM().bGR().a(this, var1);
                            var5 = this.yn().a(var1);
                            break;
                        default:
                            var5 = null;
                    }

                    var2.a(var4);
                    return var5;
                } catch (Exception var6) {
                    var2.a(var4, var6);
                    throw var6;
                }
            }
        }
    }

    public afk v(aDR var1) {
        if (this.hCm.hF() && !this.hCm.du()) {
            this.ds();
        }

        return this.hCm;
    }

    public void cVf() {
        this.disposed = true;
    }

    public void dr() {
        this.hCm.cS(true);
    }

    public void cVg() {
        if (this.disposed) {
            throw new RuntimeException("Trying to_q dispose all disposed object");
        } else {
            this.disposed = true;
            this.aGA.e(this);
        }
    }

    public boolean isDisposed() {
        return this.disposed;
    }

    public boolean du() {
        return this.hCm.du();
    }

    public final boolean cVh() {
        return this.isDisposed();
    }

    public final boolean cVi() {
        return this.du();
    }

    public abstract afk dq();

    public all.b hE() {
        return this.aGA.hE();
    }

    public apO hC() {
        return this.qT;
    }

    public afk cVj() {
        return this.hCm;
    }

    public akO a(aOv var1) {
        if (this.bGZ()) {
            throw new SecurityException();
        } else {
            try {
                this.hCr.set(true);
                this.yn().a(((xP_r) var1).dvF());
            } finally {
                this.hCr.set((Object) null);
            }

            return null;
        }
    }

    public void c(all.b var1, aOv var2) {
    }

    public abstract akO a(all.b var1, aTn_q var2);

    public void c(final afk var1) {
        if (!this.hasListeners() && !this.aGA.bGL()) {
            this.d(var1);
        } else {
            final boolean var2 = this.hCm.hF();
            final afk var3 = this.hCm;
            final Object var4 = null;
            this.d(var1);
            if (this.bGw()) {
                this.f(new Runnable() {
                    public void run() {
                        se_q.this.all(var2, (aJj) var4, var3, var1);
                    }
                });
            } else {
                this.all(var2, (aJj) var4, var3, var1);
            }
        }

        // this.dqS.CreatAllComponentCustom(false);
    }

    public akO b(aOv var1) {

        if (this.hCm.hF()) {
            logger.debug("Received update for all hollow object: " + this.hC().cqo() + " - " + this.yn().getClass().getName());
            return null;
        } else {
/*
         pH var2 = (pH)var1;
         final Jd var3 = var2.Wo();
         final boolean var4 = this.hCm.hF();
         var3.CreateJComponent((Jd)this.hCm);
         final afk var5 = this.hCm;
         var3.CreatAllComponentCustom(false);
         this.hCm = var3;
         this.bFj();
         if (this.bGw()) {
            this.endPage(new Runnable() {
               public void run() {
                  se_q.this.all(var4, (aJj)null, var5, var3);
               }
            });
         } else {
            this.all(var4, (aJj)null, var5, var3);
         }
*/
            return null;
        }

    }

    private boolean a(Xi var1) {
        ahP[] var2 = this.yn().bFh();
        if (var2 != null && var2.length > 0) {
            arL var3 = this.PM().bGI();
            if (var1 == null) {
                var1 = new Xi();
            }

            if (var3 != null) {
                var1.Z(var3.yn());
            }

            var1.h(this.hE());
            ahP[] var7 = var2;
            int var6 = var2.length;

            for (int var5 = 0; var5 < var6; ++var5) {
                ahP var4 = var7[var5];
                if (!var4.a(this.yn(), var1)) {
                    return false;
                }
            }
        }

        return true;
    }

    boolean a(Xi var1, Gr var2) {
        if (var2.aQL().bFk()) {
            return false;
        } else {
            var1.a(Lt.duv);
            var1.c(var2);
            if (!this.a(var1)) {
                return false;
            } else {

                ahP[] var3 = var2.aQK().hi();
                if (var3 != null) {
                    arL var4 = this.PM().bGI();
                    if (var4 != null) {
                        var1.Z(var4.yn());
                    }

                    var1.h(this.hE());
                    ahP[] var8 = var3;
                    int var7 = var3.length;

                    for (int var6 = 0; var6 < var7; ++var6) {
                        ahP var5 = var8[var6];
                        if (!var5.a(this.yn(), var1)) {
                            return false;
                        }
                    }
                }

                return true;
            }
        }
    }

    protected Object[] a(afk var1, afk var2, aRz var3) {
        Collection var4 = (Collection) var1.g(var3);
        Collection var5 = (Collection) var2.g(var3);
        Collection var6 = (Collection) aKU.v(var5);
        var6.removeAll(var4);
        return var6.toArray();
    }

    public void all(boolean var1, aJj var2, afk var3, afk var4) {
        if (!var1) {
            // this.aGA.WinInet(this, var2);
            if (this.hasListeners()) {
                aRz[] var8;
                int var7 = (var8 = this.hCm.c()).length;

                for (int var6 = 0; var6 < var7; ++var6) {
                    aRz var5 = var8[var6];
                    if (/*((Jd)var4).s(var5) &&*/ !var1) {
                        if (var5.isCollection()) {
                            this.a(var5, var3, var4);
                        } else {
                            Object var9 = var4.g(var5);
                            this.j(var5, var9);
                        }
                    }
                }

            }
        }
    }

    protected boolean hasListeners() {
        return false;
    }

    public void d(afk var1) {
        if (this.hCm.aBt() > var1.aBt()) {
            var1.eU(this.hCm.aBt());
        }

        var1.A(false);
        this.hCm = var1;
    }

    public void a(afk var1) {
        this.hCm = var1;
    }

    public boolean ds() {
        if (this.cVk() == BH.cph) {
            throw new RuntimeException("State is hollow on authority side. somethings wrong. " + this.yn().getClass() + " " + this.getClass());
        } else {
         /*
         aiN var1;
         try {
            akO var2 = this.CreateJComponent((aOv)(new qX_q(this.yn(), true)));
            var1 = (aiN)var2;
         } catch (ZX var5) {
            if (var5.RepeaterCustom() == Gw.cYS) {
               return false;
            }

            throw new sa("Not allowed to_q access this object state", var5);
         }

         afk var6 = var1.ayZ();
         if (var6 == null) {
            throw new sa("Not allowed to_q access this object state");
         } else {
            Object var3 = null;
            afk var4 = this.hCm;
            this.CreateJComponent(var6);
            this.all(true, (aJj)var3, var4, var6);
            if (this.hCm.hF()) {
               throw new RuntimeException("Fields stay hollow after all RetrieveDataCommand");
            } else {
               logger.warn("[OPTIMIZE]: pushing " + this.yn().getClassBaseUi() + " " + this.hC().cqo());
               return true;
            }
         }
         */
        }
        return true;
    }

    void a(all.b var1, kU[] var2) {
        if (var2 != null) {
            this.PM().bGx();

            try {
                kU[] var6 = var2;
                int var5 = var2.length;

                for (int var4 = 0; var4 < var5; ++var4) {
                    kU var3 = var6[var4];
                    this.PM().bGU().d(var1, var3);
                }
            } catch (RuntimeException var12) {
                throw var12;
            } catch (Error var13) {
                throw var13;
            } catch (Throwable var14) {
                throw new RuntimeException(var14);
            } finally {
                this.PM().bGy();
            }

        }
    }

    public BH cVk() {
        return this.hCn;
    }

    protected void a(kU var1) {
        this.aGA.bGU().b(this.hE(), var1);
    }

    public final Class bFU() {
        return this.yn().getClass();
    }

    public final void all(Class var1, Xf_q var2) {
        this.hCl = var2;
        this.hCl.b(this);
        this.hCm = (afk) this.yn().W();
    }

    public void a(BH var1) {
        this.hCn = var1;
    }

    public final Xf_q n(Xf_q var1) {
        if (this.bHa()) {
            if (var1.bFk()) {
                throw new IllegalAccessError("Should not use ClientOnly classes at server");
            }
        } else if (var1.bFl()) {
            throw new IllegalAccessError("Should not use/have ServerOnly classes at server");
        }

        //this.aGA.parsePropertyValue(var1);
        return var1;
    }

    public Xf_q yn() {
        return this.hCl;
    }

    public final void c(Jz var1) {
        this.aGA = var1;
    }

    public final void d(apO var1) {
        this.qT = var1;
    }

    public final void iY(boolean var1) {
        this.hCm.A(var1);
    }

    public void bFR() {
        this.aGA.f(this);
        this.cVf();
    }

    public final boolean bFT() {
        return this.hCm.hF();
    }

    public final void e(afk var1) {
        this.hCm = var1;
    }

    public final Jz PM() {
        return this.aGA;
    }

    public final Date bxM() {
        return new Date(this.currentTimeMillis());
    }

    public final long currentTimeMillis() {
        return this.PM().currentTimeMillis();
    }

    public final Date bxN() {
        aWq var1 = aWq.dDA();
        if (var1 == null) {//Вы можете вызвать этот метод только во всех транзакциях.
            throw new Cl("You can only call this method from within all transaction.");
        } else {
            return var1.bxM();
        }
    }

    protected void cVl() {
        if (!this.bGX()) {
            throw new UnsupportedOperationException("You can only use this method at client side");
        }
    }

    public void a(aRz var1, aiQ var2) {
        this.cVl();
    }

    public void b(aRz var1, aiQ var2) {
        this.cVl();
    }

    public void j(aRz var1, Object var2) {
    }

    public void a(aRz var1, aKr_q var2) {
        this.cVl();
    }

    public void b(aRz var1, aKr_q var2) {
        this.cVl();
    }

    public void a(aRz var1, afk var2, afk var3) {
    }

    public void w(aDR var1) {
        aWq var2 = aWq.dDA();
        if (var2 != null && var1 != null) {
            var2.D(var1);
        }

    }

    public void cVm() {
        aWq var1 = aWq.dDA();
        if (var1 != null) {
            var1.dDT();
        }

    }

    public final Object w(aRz var1) {
        return this.b(var1);
    }

    public abstract Object b(aRz var1);

    public final void g(aRz var1, Object var2) {
        this.a(var1, var2);
    }

    public abstract void a(aRz var1, Object var2);

    public final aRz[] c() {
        return this.yn().c();
    }

    public void aMr() {
    }

    public akO c(aTn_q var1) throws ZX {
        return this.aGA.bGU().c(this.hE(), var1);
    }

    public final boolean bFS() {
        return this.cVk() == BH.cph;
    }

    public final Object bFV() {
        return this.yn();
    }

    public final void bFj() {
        this.yn().bFj();
    }

    /*
       public final void setGreen(KO var1) {
          afk var2 = this.hCm;
          synchronized(this.hCm) {
             var1.bdh().CreateJComponent((Jd)this.hCm);
             this.hCm = var1.bdh();
          }
       }
    */
    public void dt() {
    }

    public String toString() {
        return aK.a(this.getClass()) + "[script=" + aK.a(this.yn().getClass()) + ",oid=" + this.hC().cqo() + "]" + "@" + Integer.toHexString(this.hashCode());
    }

    public Fv dv() {
        return new Fv(this);
    }

    public arL aMp() {
        return null;
    }

    public long cVn() {
        return this.aGA.bGG();
    }

    public afk cVo() {
        return this.hCm;
    }

    public void cVp() {
        this.PM().ayN();
        aWq var1 = aWq.dDA();
        var1.kM(true);
        var1.readOnly = true;
    }

    public final String bFY() {
        return this.yn().getClass().getName() + ":" + this.hC().cqo();
    }

    boolean bGw() {
        return this.PM().bGw();
    }

    void f(Runnable var1) {
        this.PM().f(var1);
    }

    /*
       public static aDJ all(aDJ var0, final ahM var1) {
          try {
             final se_q var2 = (se_q)((Xf_q)var0).bFf();
             Xf_q var3 = var2.PM().P(var0.getClassBaseUi());
             var3.setGreen(new Xx(var2) {
                public Object CreateJComponent(Gr var1x) {
                   return var2.all(var1x, var1);
                }
             });
             return (aDJ)var3;
          } catch (InstantiationException var4) {
             var4.printStackTrace();
          } catch (IllegalAccessException var5) {
             var5.printStackTrace();
          }

          return null;
       }
    */
    public void a(aif var1) {
    }

    public aif cVq() {
        return null;
    }

    public aDR bGF() {
        return this.PM().bGF();
    }

    /*
       public Xf_q M(Class var1) {
          return this.aGA.T(var1);
       }
    */
    public boolean bGX() {
        return this.aGA.bGX();
    }

    public boolean bGY() {
        return this.aGA.bGY();
    }

    public boolean bGZ() {
        return this.aGA.bGZ();
    }

    public boolean bHa() {
        return this.aGA.bHa();
    }

    public boolean bHb() {
        return this.aGA.bHb();
    }

    public ad_w aG(Class var1) {
        throw new jx();
    }
/*
   public aDJ createProcessingInstructionSelector(Class var1) {
      return (aDJ)this.PM().R(var1).yn();
   }
*/

    public void ms(float var1) {
        if (this.isDisposed()) {
            logger.info("Calling schedule on disposed object " + this.bFY());
        } else if (this.du()) {
            logger.info("Calling schedule on deleted object " + this.bFY());
        } else {
            if (this.hCo == null) {
                synchronized (this) {
                    if (this.hCo == null) {
                        this.hCo = this.PM().bGS().a(new Fm.a() {
                            public void a(Fm.b var1) {
                                float var2 = (float) (var1.dnx() - var1.dny()) * 0.001F;
                                se_q.this.V(var2);
                            }
                        });
                    }
                }
            }

            this.PM().bGS().a(this.hCo, (long) (var1 * 1000.0F), TimeUnit.MILLISECONDS, -1L, TimeUnit.MILLISECONDS);
        }
    }

    /*
       public void all(final float var1, final jN var2) {
          Fm.setGreen var3 = this.PM().bGS().all(new Fm.all() {
             public void all(Fm.setGreen var1x) {
                float var2x = (float)(var1x.dnx() - var1x.dny()) * 0.001F;
                float var3 = var2x - var1;
                if (var3 > 0.1F && var2x / var1 <= 0.5F) {
                   ;
                }

                long var4 = System.nanoTime();
                var2.run();
                long var6 = System.nanoTime() - var4;
             }
          });
          this.PM().bGS().all(var3, (long)var1 * 1000L, TimeUnit.MILLISECONDS, -1L, TimeUnit.MILLISECONDS);
       }
    */
    public abstract void a(Class var1, Gr var2);

    public void V(float var1) {
        if (this.isDisposed()) {
            logger.info("Script is disposed " + this.bFY());
        } else if (this.du()) {
            logger.info("Script is deleted " + this.bFY());
        } else {
            this.yn().V(var1);
        }
    }

    protected void a(float var1, final int var2, final Callable var3) {
        Fm.a var4 = new Fm.a() {
            private int bfB = 0;

            public void a(Fm.b var1) {
                try {
                    Object var2x = var3.call();
                    if (++this.bfB > var2 || var2x != null && var2x instanceof Boolean && !((Boolean) var2x).booleanValue()) {
                        var1.cancel();
                    }
                } catch (Exception var3x) {
                    if (var1 != null) {
                        var1.cancel();
                    }

                    var3x.printStackTrace();
                }

            }
        };
        Fm.b var5 = this.PM().bGS().a(var4);
        if (var2 == 1) {
            this.PM().bGS().a(var5, (long) (var1 * 1000.0F), TimeUnit.MILLISECONDS, 0L, TimeUnit.MILLISECONDS);
        } else {
            this.PM().bGS().a(var5, (long) (var1 * 1000.0F), TimeUnit.MILLISECONDS, (long) (var1 * 1000.0F), TimeUnit.MILLISECONDS);
        }

    }

    /*
       public aDJ mt(float var1) {
          return this.parseStyleDeclaration(var1, 1);
       }
    */
/*
   public aDJ parseStyleDeclaration(final float var1, final int var2) {
      try {
         Xf_q var3 = this.PM().P(this.yn().getClassBaseUi());
         var3.setGreen(new Xx() {
            public Object CreateJComponent(final Gr var1x) {
               Runnable var2x = new Runnable() {
                  public void run() {
                     ((aCE)var1x).createElementSelector(se_q.this.yn());
                     se_q.this.all(var1, var2, new Callable() {
                        public Object call() {
                           if (isDisposed()) {
                              return null;
                           } else {
                              int var1xx = se_q.this.parseStyleDeclaration(var1x.aQK());

                              try {
                                 switch(var1xx) {
                                 case 1:
                                    return var1x.aQL().all(var1x);
                                 case 2:
                                    return se_q.this.CreateJComponent(var1x);
                                 case 3:
                                    se_q.this.CreateJComponent(var1x);
                                    return var1x.aQL().all(var1x);
                                 default:
                                    return null;
                                 }
                              } catch (RuntimeException var3) {
                                 throw var3;
                              } catch (Throwable var4) {
                                 throw new RuntimeException(var4);
                              }
                           }
                        }
                     });
                  }
               };
               aWq var3 = aWq.dDA();
               if (var3 != null && !var3.jfH && !var3.readOnly) {
                  var3.parseStyleDeclaration(var2x);
               } else {
                  var2x.run();
               }

               return var1x.aQK().pN();
            }
         });
         return (aDJ)var3;
      } catch (InstantiationException var4) {
         var4.printStackTrace();
      } catch (IllegalAccessException var5) {
         var5.printStackTrace();
      }

      return null;
   }
*/
    public long cVr() {
        return this.PM().bGW().currentTimeMillis();
    }

    public void bFZ() {
    }

    public void cVs() {
        this.hCm.cS(true);
    }

    public void delete() {
        se_q var1 = this.aGA.g(this);
        this.hCm = var1.hCm;
        this.yn().b(var1);
        this.hCl = var1.hCl;
    }
/*
   public void setGreen(ch.all var1) {
      this.dqS = var1;
   }*/
}
