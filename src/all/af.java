package all;

import gnu.trove.THashMap;
import org.apache.commons.logging.Log;

import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public abstract class af extends se_q {
    private static final Log logger = LogPrinter.setClass(af.class);
    private static final aem_q[] hn = new aem_q[0];
    private static ConcurrentHashMap hu = new ConcurrentHashMap();
    private Map ho;
    private aem_q[] hp;
    private ReadWriteLock hq;
    private Fv hr;
    private long hs;
    private long ht;

    public af() {
        this.hp = hn;
        this.hq = new ReentrantReadWriteLock();
        this.hr = null;
    }

    public static Map dx() {
        HashMap var0 = new HashMap();
        Iterator var2 = hu.entrySet().iterator();

        while (var2.hasNext()) {
            Entry var1 = (Entry) var2.next();
            var0.put((Class) var1.getKey(), ((AtomicLong) var1.getValue()).longValue());
        }

        return Collections.unmodifiableMap(var0);
    }

    public long di() {
        return this.ht;
    }

    public void a(long var1) {
        this.ht = var1;
    }

    public aem_q a(aDR var1) {
        if (this.ho == null) {
            this.ho = Collections.synchronizedMap(new THashMap());
        }

        Map var2 = this.ho;
        synchronized (this.ho) {
            aem_q var3 = (aem_q) this.ho.get(var1.bgt());
            if (var3 != null) {
                return var3;
            } else {
                var3 = new aem_q(var1);
                this.ho.put(var1.bgt(), var3);
                if (this.ho.size() > 0) {
                    this.hp = (aem_q[]) this.ho.values().toArray(new aem_q[this.ho.size()]);
                } else {
                    this.hp = hn;
                }

                return var3;
            }
        }
    }

    public void b(aDR var1) {
        if (this.ho != null) {
            Map var2 = this.ho;
            synchronized (this.ho) {
                this.ho.remove(var1.bgt());
                Collection var3 = this.ho.values();
                this.hp = (aem_q[]) var3.toArray(new aem_q[var3.size()]);
            }
        }
    }

    public boolean a(all.b var1) {
        return this.ho == null ? false : this.ho.containsKey(var1);
    }

    public boolean c(aDR var1) {
        return this.ho.containsKey(var1.bgt());
    }

    public boolean dj() {
        if (!this.bGZ()) {
            throw new IllegalAccessError("Invalid at client");
        } else if (this.ho == null) {
            return false;
        } else {
            aDR var1 = this.PM().bGF();
            if (var1 == null) {
                throw new IllegalAccessError("Invalid outside client calls");
            } else {
                all.b var2 = var1.bgt();
                aem_q var3 = (aem_q) this.ho.get(var2);
                if (var3 != null && var3.fnt) {
                    return true;
                } else {
                    aWq var4 = aWq.dDA();
                    return var4 != null ? var4.a((se_q) this, (all.b) var2) : false;
                }
            }
        }
    }

    public boolean d(aDR var1) {
        if (this.ho == null) {
            return false;
        } else {
            aem_q var2 = (aem_q) this.ho.get(var1.bgt());
            return var2 == null ? false : var2.fnt;
        }
    }

    public void b(long var1) {
        this.PM().gd(var1);
    }

    public void push() {
        if (!this.bGZ()) {
            throw new IllegalAccessError();
        } else {
            aWq var1 = aWq.dDA();
            aDR var2;
            if (var1 != null && (var2 = var1.dDV()) != null) {
                this.e(var2);
            } else {
                this.e(this.PM().bGF());
            }

        }
    }

    public void e(aDR var1) {
        aWq var2 = aWq.dDA();
        if (var2 != null && var1 != null && !this.d(var1)) {
            var2.a(this, var1);
        }

    }

    /*
       public void all(aDJ var1) {

          af var2 = (af)((Xf_q)var1).bFf();
          aem_q[] var6;
          int var5 = (var6 = var2.dp()).length;

          for(int var4 = 0; var4 < var5; ++var4) {
             aem_q var3 = var6[var4];
             if (var3.fnt) {
                this.CreateJComponentAndAddInRootPane(var3.eIu);
             }
          }

       }
    */
    protected void a(kU var1, aDR var2) {
        aem_q[] var3 = this.dp();
        aem_q[] var7 = var3;
        int var6 = var3.length;

        for (int var5 = 0; var5 < var6; ++var5) {
            aem_q var4 = var7[var5];
            if (var2 == null || var4.eIu != var2) {
                this.aGA.bGU().b(var4.eIu.bgt(), var1);
            }
        }

    }

    protected void b(Gr var1) {
        this.a((Gr) var1, (aDR) null);
    }

    protected void a(Gr var1, aDR var2) {
        this.PM().bGR().a(var1, var2, (aWq) null, (aOv) null);
    }

    protected void a(aWq var1, Gr var2, aDR var3, aOv var4) {
        this.PM().bGR().a(var2, var3, var1, var4);
    }

    public boolean a(Xi var1, aDR var2, boolean var3) {
        if (var2.cST()) {
            return true;
        } else if (this.yn().bFl() && !var2.cWs()) {
            if (var3) {
                throw new RuntimeException("The requested object " + this.bFU().getName() + " id " + this.hC().cqo() + " is ServerOnly");
            } else {
                return false;
            }
        } else {
            ahP[] var4 = this.yn().bFh();
            if (var4 != null && var4.length > 0) {
                arL var5 = var2.cWq();
                if (var1 == null) {
                    var1 = new Xi();
                }

                if (var5 != null) {
                    var1.Z(var5.yn());
                }

                var1.h(var2.bgt());
                var1.j(var2);
                ahP[] var9 = var4;
                int var8 = var4.length;

                for (int var7 = 0; var7 < var8; ++var7) {
                    ahP var6 = var9[var7];
                    if (!var6.a(this.yn(), var1)) {
                        if (var3) {
                            throw new aOQ_q("Access to_q object " + this.bFU().getName() + " id " + this.hC().cqo() + " denied by replication rule: " + var6.Xf().name());
                        }

                        return false;
                    }
                }
            }

            return true;
        }
    }

    public void dk() {
        this.hq.writeLock().unlock();
    }

    public void dl() {
        this.hq.readLock().unlock();
    }

    public void dm() {
        this.hq.writeLock().lock();
    }

    public void dn() {
        this.hq.readLock().lock();
    }

    public boolean do_q() {
        return this.a((Xi) null, this.PM().bGF(), false);
    }

    public akO all(all.b var1, aTn_q var2) throws InterruptedException {
        if (this.PM().bGM()) {
            return null;
        } else {
            aDR var3 = this.PM().bGJ().f(var1);
            Gr var4 = var2.dvF();
            List var5 = null;
            Object var6 = null;
            fm_q var7 = var4.aQK();
            if (var7.pv()) {
                aWq var8 = null;
                if (aWq.dDA() == null) {
                    var8 = aWq.b(this.currentTimeMillis(), var3, var4);
                    var8.kM(true);
                }

                boolean var9 = false;

                try {
                    this.a(var4, var3);
                    var9 = true;
                } finally {
                    if (var8 != null) {
                        if (var9) {
                            var8.g(this.aGA);
                        }

                        var8.dispose();
                        var8 = null;
                    }

                }

                if (!var7.po()) {
                    return null;
                }
            }

            try {
                var6 = this.PM().bGR().a(var3, var4, var2.isBlocking());
            } catch (Exception var13) {
                if (var2 instanceof ek_q) {
                    ek_q var17 = (ek_q) var2;
                    this.aGA.bGU().b(var3.bgt(), (kU) (new Is(this.yn(), var17.getId(), (Object) null, var13, var5)));
                    return null;
                }

                throw var13;
            }

            if (var2.isBlocking()) {
                aW var15 = (aW) var6;
                var6 = var15.fC();
                var5 = var15.fB();
            }

            if (var2 instanceof ek_q) {
                ek_q var16 = (ek_q) var2;
                this.aGA.bGU().b(var3.bgt(), (kU) (new Is(this.yn(), var16.getId(), var6, var5)));
                return null;
            } else {
                return var2.isBlocking() ? new aic(var6, var5) : null;
            }
        }
    }

    public aem_q[] dp() {
        return this.hp;
    }

    public akO a(all.b var1, aOv var2) {
        try {
            if (this.hCm.hF() && !this.hCm.du()) {
                this.ds();
            }

            aDR var3 = this.PM().bGJ().f(var1);
            if (var3.hDb != null) {
                AtomicLong var4 = (AtomicLong) hu.get(this.yn().getClass());
                if (var4 == null) {
                    var4 = new AtomicLong();
                    AtomicLong var5 = (AtomicLong) hu.putIfAbsent(this.yn().getClass(), var4);
                    if (var5 != null) {
                        var4 = var5;
                    }
                }

                var4.incrementAndGet();
            }

            boolean var8 = ((qX_q) var2).Yg() && !var3.cST();
            this.aGA.bGr().hat.incrementAndGet();
            if (this.bGY() || this.a((Xi) null, var3, var8)) {
                try {
                    var3.e(this);
                } catch (InterruptedException var6) {
                    throw new RuntimeException(var6);
                }
                return null;
                //    return new aiN(this.hCm);
            }
        } catch (Exception var7) {
            throw new RuntimeException("Error on retrive data command of " + this.bFY(), var7);
        }

        return null;
    }

    /*
       public boolean all(aDR var1, Xi var2, Gr var3) {
          var2.all(Lt.duw);
          var2.CreateJComponent(var3);
          if (!this.all(var2, var1, false)) {
             return false;
          } else {
             ahP[] var4 = var3.aQK().hi();
             if (var4 != null) {
                arL var5 = var1.cWq();
                if (var5 != null) {
                   var2.Z(var5.yn());
                }

                var2.parseStyleSheet(var1.bgt());
                var2.parseRule(var1);

                try {
                   ahP[] var9 = var4;
                   int var8 = var4.length;

                   for(int var7 = 0; var7 < var8; ++var7) {
                      ahP var6 = var9[var7];
                      if (!var6.all(this.yn(), var2)) {
                         return false;
                      }
                   }
                } catch (Exception var10) {
                   logger.warn("Exception during replication check to_q " + var1.bgt() + " of object " + this.yn().getClassBaseUi() + ":" + this.hC().cqo() + " method " + var3.aQK().name(), var10);
                   return false;
                }
             }

             return true;
          }
       }
    */
    public final afk dq() {
        aWq var2 = aWq.dDA();
        afk var1;
        if (var2 != null) {
            if (var2.dDE()) {
                var1 = this.hCm;
            } else {
                var1 = var2.b(this);
            }
        } else {
            var1 = this.hCm;
        }

        if (var1.hF()) {
            this.ds();
            if (var2 != null) {
                if (var2.dDE()) {
                    var1 = this.hCm;
                } else {
                    var1 = var2.b(this);
                }
            } else {
                var1 = this.hCm;
            }
        }

        return var1;
    }

    public void dr() {
        if (!this.du()) {
            aWq var1 = aWq.dDA();
            if (var1 != null) {
                if (var1.dDE()) {
                    throw new IllegalStateException("Infra.infraDispose() cannot be called in safe context transaction");
                } else {
                    var1.a(this).cS(true);
                }
            } else {
                throw new IllegalStateException("Infra.infraDispose() cannot be called outside transaction");
            }
        }
    }

    public boolean ds() {
        if (!this.hCm.du()) {
            this.aGA.bGT().h(this);
        }

        return true;
    }

    public final Object b(aRz var1) {
        aWq var3 = aWq.dDA();
        afk var2;
        if (var3 != null) {
            if (var3.dDE()) {
                ++var3.jgb;
                var2 = this.hCm;
            } else {
                ++var3.env;
                var2 = var3.b(this);
            }
        } else {
            var2 = this.hCm;
        }

        if (var2.hF()) {
            this.ds();
            var2 = this.dq();
        }

        return var2.g(var1);
    }

    public final void a(aRz var1, Object var2) {
        aWq var3 = aWq.dDA();
        afk var4;
        if (var3 != null) {
            if (var3.readOnly) {
                throw new IllegalThreadStateException("All fields are read only and events cannot be fired from within all postponed infra call");
            }

            if (var3.dDE()) {
                ++var3.jgc;
                var4 = this.hCm;
            } else {
                ++var3.eny;
                var4 = var3.b(this);
            }
        } else {
            var4 = this.hCm;
        }

        if (var3 != null || !this.PM().bGo() && (this.bHa() || !this.yn().bFk())) {
            var4.e(var1, var2);
        } else {
            var4.d(var1, var2);
        }

    }

    public void dt() {
        if (!this.aGA.bGo()) {
            aWq var1 = aWq.dDA();
            if (var1 != null) {
                if (var1.dDE()) {
                    // throw new amT();
                }

                var1.a(this).cR(true);
            }

        }
    }

    public boolean du() {
        if (this.hCm.hF()) {
            this.ds();
        }

        return super.du();
    }

    /*
       public final void all(Class var1, final Gr var2) {
          aDJ var3 = (aDJ)this.yn();
          if (var2.aQK().pO().cpn() && var3.cWh()) {
             final aWq var4 = aWq.dDA();
             var4.parseStyleDeclaration(new Runnable() {
                public void run() {
                   af.this.all(var4, var2, (aDR)null, new xP_r(var2));
                }
             });
          }

          Collection var5 = var3.aJ(var1);
          if (var5 != null) {
             this.yn().all(new ArrayList(var5), var2);
          }

       }
    */
    public Fv dv() {
        if (this.hr == null) {
            this.hr = new Fv(this);
        }

        return this.hr;
    }

    public void a(afk var1) {
        if (var1.du()) {
            se_q var2 = this.aGA.g(this);
            this.hCm = var2.hCm;
            this.yn().b(var2);
        } else {
            super.a(var1);
        }

    }

    public void c(long var1) {
        this.hs = var1;
    }

    public long dw() {
        return this.hs;
    }

    public void a(all.b var1, long var2) {
        aem_q var4 = (aem_q) this.ho.get(var1);
        if (var4 == null) {
            logger.warn("Client is trying to_q enable the state update for an object it does not have (or at least test_GLCanvas longer has), from " + var1 + " object " + this.bFY());
        } else {
            if (var4.bUi()) {
                logger.warn("Client is trying to_q enable the state update for an object that is already sending the state update, from " + var1 + " object " + this.bFY());
            }

            afk var5 = this.cVj();
            if (var2 != var5.aBt()) {
                // this.aGA.bGU().setGreen(var1, (kU)(new pH((Jd)var5)));
            }

            var4.eg(true);
            if (logger.isDebugEnabled()) {
                logger.debug("enableStateUpdate: " + this.bFY() + " from " + var1);
            }

        }
    }

    public void b(all.b var1) {
        aem_q var2 = (aem_q) this.ho.get(var1);
        if (var2 == null) {
            logger.warn("Client is trying to_q disable the state update for an object it does not have (or at least test_GLCanvas longer has), from " + var1 + " object " + this.bFY());
        } else {
            if (!var2.bUi() && logger.isDebugEnabled()) {
                logger.warn("Client is trying to_q disable the state update for an object that is already NOT sending the state update, from " + var1 + " object " + this.bFY());
            }

            var2.eg(false);
            if (logger.isDebugEnabled()) {
                logger.debug("disableStateUpdate: " + this.bFY() + " from " + var1);
            }

        }
    }
}
