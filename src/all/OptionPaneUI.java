package all;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicOptionPaneUI;
import java.awt.*;

/**
 * OptionPaneUI
 */
public class OptionPaneUI extends BasicOptionPaneUI implements aIh {
    private ComponentManager Rp;

    public OptionPaneUI(JOptionPane var1) {
        this.Rp = new ComponentManager("dialog", var1);

        var1.setBorder(new BorderWrapper());
        var1.setBackground((Color) null);
        var1.setOpaque(false);
    }

    public static ComponentUI createUI(JComponent var0) {
        return new OptionPaneUI((JOptionPane) var0);
    }

    public ComponentManager wy() {
        return this.Rp;
    }

    public void installUI(JComponent var1) {
        super.installUI(var1);
        this.Rp.clear();
    }

    public Dimension getPreferredSize(JComponent var1) {
        return this.wy().c(var1, super.getPreferredSize(var1));
    }

    public Dimension getMinimumSize(JComponent var1) {
        return this.wy().b(var1, super.getMinimumSize(var1));
    }

    public Dimension getMaximumSize(JComponent var1) {
        return this.wy().a(var1, super.getMaximumSize(var1));
    }

    public void paint(Graphics var1, JComponent var2) {
        RM.a((aIh) this, (Graphics) var1, (JComponent) var2);
        super.paint(var1, var2);
        RM.a((ComponentUI) this, (Graphics) var1, (JComponent) var2);
    }

    /**
     * TODO похлже это public pv wy()
     *
     * @return Графический элемент
     */
    @Override
    public aeK wz() {
        return this.Rp;
    }
}
