package all;

import java.io.Serializable;

public class ElementSelector implements IElementSelector, Serializable {
    private String localName;

    public ElementSelector(String var1) {
        this.localName = var1;
    }

    public short getSelectorType() {
        return 4;
    }

    public String getNamespaceURI() {
        return null;
    }

    public String getLocalName() {
        return this.localName;
    }

    public String toString() {
        return this.getLocalName() != null ? this.getLocalName() : "*";
    }
}
