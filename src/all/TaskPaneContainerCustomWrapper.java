package all;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class TaskPaneContainerCustomWrapper extends PanelCustomWrapper {

    private ScrollableCustomWrapper efw = (ScrollableCustomWrapper) BaseUItegXML.thisClass.CreateJComponent(TaskPaneContainerCustomWrapper.class, "taskpane_container.xml");
    private JLayeredPane efx;
    private a efy;
    private b efz;
    private List efA = new ArrayList();
    private boolean efB;
    private boolean efC;
    private List efD = new ArrayList();
    private c efE;

    public TaskPaneContainerCustomWrapper() {
        super(new BorderLayout());
        this.efx = (JLayeredPane) this.efw.findJComponent("content");
        this.dt(true);
        super.addImpl(this.efw, "Center", -1);
        this.efy = new a((a) null);
        this.efB = false;
        this.efC = false;
    }

    public int l(Component var1) {
        if (this.efA != null && this.efx != null) {
            Integer var2 = this.efx.getComponentZOrder(var1);
            if (var2.intValue() < 0) {
                return -1;
            } else {
                if (var2.intValue() >= this.efA.size()) {
                    var2 = this.efA.size() - 1;
                }

                if (var2.intValue() < 0) {
                    return -1;
                } else {
                    var2 = (Integer) this.efA.get(var2.intValue());
                    return var2 == null ? -1 : var2.intValue();
                }
            }
        } else {
            return -1;
        }
    }

    private int buq() {
        return this.efD.size();
    }

    private int bur() {
        int var1 = 0;

        for (int var2 = 0; var2 < this.efD.size(); ++var2) {
            var1 += ((Container) this.efD.get(var2)).getHeight();
        }

        return var1;
    }

    public void dt(boolean var1) {
        if (var1) {
            if (this.efE == null) {
                this.efE = new c((c) null);
            }

            this.efx.setLayout(this.efE);
        } else {
            this.efx.setLayout((LayoutManager) null);
        }

        this.efw.revalidate();
    }

    public boolean bus() {
        return this.efx.getLayout() != null;
    }

    protected void addImpl(Component var1, Object var2, int var3) {
        if (var1 instanceof ITaskPaneCustomWrapper) {
            int var4 = -1;
            Component[] var8;
            int var7 = (var8 = this.efx.getComponents()).length;

            for (int var6 = 0; var6 < var7; ++var6) {
                Component var5 = var8[var6];
                if (this.l(var5) >= var3) {
                    var4 = this.efx.getComponentZOrder(var5);
                    break;
                }
            }

            if (var4 == -1 && this.buu() >= var3) {
                var4 = this.buv();
            }

            this.efx.add(var1, var2, var4);
            if (var1 instanceof TaskPaneCustomWrapper) {
                TaskPaneCustomWrapper var9 = (TaskPaneCustomWrapper) var1;
                if (var9.isCollapsed()) {
                    var9.collapse();
                }
            }

            var3 = var3 == -1 ? this.efx.getComponentCount() : var3;
            if (var4 == -1) {
                this.efA.add(var3);
            } else {
                this.efA.add(var4, var3);
            }

            if (var3 < 0) {
                this.efD.add((Container) var1);
            }
        } else {
            super.addImpl(var1, var2, var3);
        }

    }

    public void remove(int var1) {
        if (var1 >= 0 && var1 < this.efA.size()) {
            if (((Integer) this.efA.get(var1)).intValue() < 0) {
                this.efD.remove(this.efx.getComponent(var1));
            }

            this.efA.remove(var1);
            super.remove(var1);
        }
    }

    public void remove(Component var1) {
        int var2 = this.efx.getComponentZOrder(var1);
        if (var2 != -1) {
            if (((Integer) this.efA.get(var2)).intValue() < 0) {
                this.efD.remove(var1);
            }

            this.efA.remove(var2);
            super.remove(var1);
        }
    }

    public String getElementName() {
        return "taskpanecontainer";
    }

    public void b(TaskPaneCustomWrapper var1) {
        this.efy.b(var1);
    }

    public void c(TaskPaneCustomWrapper var1) {
        this.efy.c(var1);
    }

    public void du(boolean var1) {
        this.efB = var1;
    }

    public void a(TaskPaneCustomWrapper var1, boolean var2) {
        if (var1 != null) {
            int var3 = this.efx.getComponentZOrder(var1);
            if (var3 >= 0) {
                var1.setVisible(var2);
                this.validate();
            }
        }
    }

    private b but() {
        b var1 = new b((b) null);
        var1.setName("divisor");
        ComponentManager.getCssHolder(var1).setAttribute("name", "divisor");
        return var1;
    }

    public void nm(int var1) {
        if (this.efz == null) {
            this.efz = this.but();
            this.add(this.efz, var1);
        }

    }

    public int buu() {
        return this.l(this.efz);
    }

    public int buv() {
        return this.efx.getComponentZOrder(this.efz);
    }

    public Component[] buw() {
        return this.efx.getComponents();
    }

    public int bux() {
        return this.efx.getComponentCount();
    }

    public Component nn(int var1) {
        return this.efx.getComponent(var1);
    }

    public JScrollBar buy() {
        return this.efw.getVerticalScrollBar();
    }

    public JComponent EP() {
        return this.efz;
    }

    private class a {
        private int bGo;
        private int bGp;

        private a() {
            this.bGo = -1;
        }

        // $FF: synthetic method
        a(a var2) {
            this();
        }

        public void b(TaskPaneCustomWrapper var1) {
            Point var2 = TaskPaneContainerCustomWrapper.this.getMousePosition();
            if (!TaskPaneContainerCustomWrapper.this.efB && var2 != null) {
                if (this.bGo < 0) {
                    TaskPaneContainerCustomWrapper.this.efC = true;
                    this.bGo = TaskPaneContainerCustomWrapper.this.efw.getViewport().getViewPosition().y;
                    this.bGp = TaskPaneContainerCustomWrapper.this.efx.getComponentZOrder(var1);
                    aeK var3 = ComponentManager.getCssHolder(var1);
                    var3.setAlpha(0.5F);
                    BaseUItegXML.thisClass.cx(var3.Vr().aue());
                    TaskPaneContainerCustomWrapper.this.efx.setLayer(var1, JLayeredPane.DRAG_LAYER.intValue());
                }

                if (var2.y >= TaskPaneContainerCustomWrapper.this.bur()) {
                    var1.setLocation(0, var2.y + this.bGo - var1.djo().getHeight() / 2);
                }
            }
        }

        public void c(TaskPaneCustomWrapper var1) {
            aeK var2 = ComponentManager.getCssHolder(var1);
            Component var3 = null;
            Rectangle var4 = var1.getBounds();
            int var5 = 0;
            Component[] var9;
            int var8 = (var9 = TaskPaneContainerCustomWrapper.this.efx.getComponents()).length;

            int var7;
            for (var7 = 0; var7 < var8; ++var7) {
                Component var6 = var9[var7];
                if (!var1.equals(var6) && var6.isVisible()) {
                    var5 += var6.getHeight();
                    if (var4.intersects(var6.getBounds())) {
                        if (TaskPaneContainerCustomWrapper.this.l(var6) < 0) {
                            this.bGp = TaskPaneContainerCustomWrapper.this.efx.getComponentZOrder(var6);
                        } else {
                            var3 = var6;
                        }
                        break;
                    }
                }
            }

            TaskPaneContainerCustomWrapper.this.efx.setLayer(var1, JLayeredPane.DEFAULT_LAYER.intValue());
            this.bGo = -1;
            TaskPaneContainerCustomWrapper.this.efC = false;
            var2.Vr().setColorMultiplier(new Color(255, 255, 255, 255));
            if (var3 == null) {
                int var11 = var1.getY();
                if (var11 <= 0) {
                    TaskPaneContainerCustomWrapper.this.efx.setComponentZOrder(var1, TaskPaneContainerCustomWrapper.this.buq());
                } else if (var11 >= var5) {
                    var7 = TaskPaneContainerCustomWrapper.this.efx.getComponentCount() - 1;
                    TaskPaneContainerCustomWrapper.this.efx.setComponentZOrder(var1, var7);
                } else {
                    TaskPaneContainerCustomWrapper.this.efx.setComponentZOrder(var1, this.bGp);
                }

                this.a(var2);
                TaskPaneContainerCustomWrapper.this.validate();
            } else {
                Rectangle var10 = var3.getBounds();
                TaskPaneContainerCustomWrapper.this.efx.remove(var1);
                var7 = TaskPaneContainerCustomWrapper.this.efx.getComponentZOrder(var3) + (var4.intersection(var10).y < var10.y + var10.height / 2 ? 0 : 1);
                TaskPaneContainerCustomWrapper.this.efx.add(var1, var7);
                var1.setBounds(var4);
                this.a(var2);
                TaskPaneContainerCustomWrapper.this.validate();
            }
        }

        private void a(aeK var1) {
            BaseUItegXML.thisClass.cx(var1.Vr().auc());
        }
    }

    private class b extends PictureCustomWrapper implements ITaskPaneCustomWrapper {


        private b() {
        }

        // $FF: synthetic method
        b(b var2) {
            this();
        }

        public boolean isPinned() {
            return true;
        }

        public boolean aA() {
            return true;
        }
    }

    private class c implements LayoutManager2 {
        private c() {
        }

        // $FF: synthetic method
        c(c var2) {
            this();
        }

        public void addLayoutComponent(String var1, Component var2) {
        }

        public void layoutContainer(Container var1) {
            if (var1.getComponentCount() > 0 && !TaskPaneContainerCustomWrapper.this.efC) {
                Insets var2 = var1.getInsets();
                int var3 = var2.top;
                int var4 = var1.getWidth();
                Component[] var8;
                int var7 = (var8 = var1.getComponents()).length;

                for (int var6 = 0; var6 < var7; ++var6) {
                    Component var5 = var8[var6];
                    if (var5.isVisible()) {
                        int var9 = var5.getHeight();
                        var5.setBounds(var2.left, var3, var4, var9);
                        var3 += var9;
                    }
                }

            }
        }

        public Dimension minimumLayoutSize(Container var1) {
            Insets var2 = var1.getInsets();
            int var3 = var2.top;
            if (var1.getComponentCount() <= 0) {
                return new Dimension(var2.left + var2.right, var2.top + var2.bottom);
            } else {
                Component[] var7;
                int var6 = (var7 = var1.getComponents()).length;

                for (int var5 = 0; var5 < var6; ++var5) {
                    Component var4 = var7[var5];
                    if (var4.isVisible()) {
                        var3 += var4.getHeight();
                    }
                }

                int var10003 = var3 + var2.bottom;
                return new Dimension(var1.getMinimumSize().width, var10003);
            }
        }

        public Dimension preferredLayoutSize(Container var1) {
            Insets var2 = var1.getInsets();
            int var3 = var2.top;
            if (var1.getComponentCount() <= 0) {
                return new Dimension(var2.left + var2.right, var2.top + var2.bottom);
            } else {
                Component[] var7;
                int var6 = (var7 = var1.getComponents()).length;

                for (int var5 = 0; var5 < var6; ++var5) {
                    Component var4 = var7[var5];
                    if (var4.isVisible()) {
                        var3 += var4.getSize().height;
                    }
                }

                return new Dimension(0, var3 + var2.bottom);
            }
        }

        public void removeLayoutComponent(Component var1) {
        }

        public void addLayoutComponent(Component var1, Object var2) {
            var1.setSize(var1.getPreferredSize());
            var1.setMaximumSize(new Dimension(var1.getParent().getWidth(), var1.getMaximumSize().height));
        }

        public float getLayoutAlignmentX(Container var1) {
            return 0.0F;
        }

        public float getLayoutAlignmentY(Container var1) {
            return 0.0F;
        }

        public void invalidateLayout(Container var1) {
        }

        public Dimension maximumLayoutSize(Container var1) {
            Insets var2 = var1.getInsets();
            int var3 = var2.top;
            if (var1.getComponentCount() <= 0) {
                return new Dimension(var2.left + var2.right, var2.top + var2.bottom);
            } else {
                Component[] var7;
                int var6 = (var7 = var1.getComponents()).length;

                for (int var5 = 0; var5 < var6; ++var5) {
                    Component var4 = var7[var5];
                    if (var4.isVisible()) {
                        var3 += var4.getMaximumSize().height;
                    }
                }

                int var10003 = var3 + var2.bottom;
                return new Dimension(var1.getMaximumSize().width, var10003);
            }
        }
    }
}
