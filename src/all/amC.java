package all;

import javax.swing.*;
import java.applet.Applet;
import java.awt.*;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Менеджер перерисовки
 */
public class amC extends RepaintManager {
    private long fKP;
    private Map gbe = new IdentityHashMap();
    private Map gbf = new IdentityHashMap();

    public void addDirtyRegion(JComponent var1, int var2, int var3, int var4, int var5) {
        ++this.fKP;
        this.a((Container) var1, var2, var3, var4, var5);
    }

    private synchronized boolean a(Component var1, int var2, int var3, int var4, int var5) {
        Rectangle var6;
        synchronized (this) {
            Map var8 = this.gbe;
            synchronized (this.gbe) {
                var6 = (Rectangle) this.gbe.get(var1);
            }
        }

        if (var6 != null) {
            SwingUtilities.computeUnion(var2, var3, var4, var5, var6);
            return true;
        } else {
            return false;
        }
    }

    private void a(Container var1, int var2, int var3, int var4, int var5) {
        if (var4 > 0 && var5 > 0 && var1 != null) {
            if (var1.isShowing()) {
                if (var1.getWidth() > 0 && var1.getHeight() > 0) {
                    if (!this.a((Component) var1, var2, var3, var4, var5)) {
                        Container var6 = null;
                        Container var7 = var1;

                        while (var7 != null) {
                            if (var7.isVisible() && var7.getPeer() != null) {
                                if (!(var7 instanceof Window) && !(var7 instanceof Applet)) {
                                    var7 = var7.getParent();
                                    continue;
                                }

                                if (var7 instanceof Frame && (((Frame) var7).getExtendedState() & 1) == 1) {
                                    return;
                                }

                                var6 = var7;
                                break;
                            }

                            return;
                        }

                        if (var6 != null) {
                            synchronized (this) {
                                if (!this.a((Component) var1, var2, var3, var4, var5)) {
                                    Map var8 = this.gbe;
                                    synchronized (this.gbe) {
                                        this.gbe.put(var1, new Rectangle(var2, var3, var4, var5));
                                    }

                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public boolean isDoubleBufferingEnabled() {
        return false;
    }

    public void setDoubleBufferingEnabled(boolean var1) {
    }

    public boolean isCompletelyDirty(JComponent var1) {
        return false;
    }

    public void addDirtyRegion(Applet var1, int var2, int var3, int var4, int var5) {
        ++this.fKP;
        this.a((Container) var1, var2, var3, var4, var5);
    }

    public void addDirtyRegion(Window var1, int var2, int var3, int var4, int var5) {
        ++this.fKP;
        this.a((Container) var1, var2, var3, var4, var5);
    }

    public void addInvalidComponent(JComponent var1) {
        super.addInvalidComponent(var1);
        ++this.fKP;
    }

    public Rectangle getDirtyRegion(JComponent var1) {
        synchronized (this) {
            Map var3 = this.gbe;
            Rectangle var10000;
            synchronized (this.gbe) {
                var10000 = (Rectangle) this.gbe.get(var1);
            }

            return var10000;
        }
    }

    public Dimension getDoubleBufferMaximumSize() {
        return new Dimension(1, 1);
    }

    public void setDoubleBufferMaximumSize(Dimension var1) {
    }

    public void markCompletelyClean(JComponent var1) {
    }

    public Image getOffscreenBuffer(Component var1, int var2, int var3) {
        return null;
    }

    public void markCompletelyDirty(JComponent var1) {
        ++this.fKP;
        this.a((Container) var1, 0, 0, Integer.MAX_VALUE, Integer.MAX_VALUE);
    }

    public Image getVolatileOffscreenBuffer(Component var1, int var2, int var3) {
        return null;
    }

    public void removeInvalidComponent(JComponent var1) {
        super.removeInvalidComponent(var1);
    }

    public void paintDirtyRegions() {
    }

    public void validateInvalidComponents() {
        super.validateInvalidComponents();
    }

    public long cjJ() {
        return this.fKP;
    }

    public Rectangle p(Component var1) {
        boolean var2 = true;
        Rectangle var3 = new Rectangle();
        Rectangle var4 = new Rectangle();
        Map var5 = this.gbe;
        synchronized (this.gbe) {
            Map var6 = this.gbe;
            this.gbe = this.gbf;
            this.gbf = var6;
        }

        HashSet var14 = new HashSet();
        Iterator var7 = this.gbf.entrySet().iterator();

        while (var7.hasNext()) {
            Entry var15 = (Entry) var7.next();
            var14.add(new Km((Component) var15.getKey(), (Rectangle) var15.getValue()));
        }

        var7 = var14.iterator();

        while (true) {
            Object var8;
            Km var16;
            label81:
            do {
                while (var7.hasNext()) {
                    var16 = (Km) var7.next();
                    var4.setBounds((Rectangle) var16.bcA());
                    var8 = (Component) var16.first();
                    if (var8 != null) {
                        while (var8 != var1) {
                            Container var9 = ((Component) var8).getParent();
                            if (var9 == null) {
                                break;
                            }

                            SwingUtilities.computeIntersection(0, 0, ((Component) var8).getWidth(), ((Component) var8).getHeight(), var4);
                            var4.x += ((Component) var8).getX();
                            var4.y += ((Component) var8).getY();
                            var8 = var9;
                        }
                    }

                    if (var8 != var1) {
                        continue label81;
                    }

                    this.gbf.remove(var16.first());
                    SwingUtilities.computeIntersection(0, 0, var1.getWidth(), var1.getHeight(), var4);
                    if (var2) {
                        var3.setBounds(var4);
                        var2 = false;
                    } else {
                        SwingUtilities.computeUnion(var4.x, var4.y, var4.width, var4.height, var3);
                    }
                }

                synchronized (this) {
                    Map var17 = this.gbe;
                    synchronized (this.gbe) {
                        Iterator var19 = this.gbf.entrySet().iterator();

                        while (true) {
                            if (!var19.hasNext()) {
                                break;
                            }

                            Entry var18 = (Entry) var19.next();
                            Rectangle var10 = (Rectangle) var18.getValue();
                            this.a((Container) var18.getKey(), (int) var10.getX(), (int) var10.getY(), (int) var10.getWidth(), (int) var10.getHeight());
                        }
                    }
                }

                this.gbf.clear();
                return var3;
            } while (var8 != null && ((Component) var8).isShowing());

            this.gbf.remove(var16.first());
        }
    }

    public Rectangle a(JComponent var1, Graphics var2) {
        Graphics var4 = var2.create();
        Rectangle var3;
        synchronized (var1.getTreeLock()) {
            var3 = this.p(var1);
        }

        byte var5 = 8;
        Rectangle var6 = new Rectangle(var3.x - var5, var3.y - var5, var3.width + var5 * 2, var3.height + var5 * 2);
        SwingUtilities.computeIntersection(0, 0, var1.getWidth(), var1.getHeight(), var6);
        var4.setClip(var6);
        var1.paint(var4);
        return var3;
    }
}
