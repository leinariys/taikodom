package all;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

public class TaskPaneCustomWrapper extends PanelCustomWrapper implements ITaskPaneCustomWrapper {

    private TaskPaneContainerCustomWrapper ipb;
    private PanelCustomWrapper ipc;
    private JLabel ipd;
    private JButton ipe;
    private JToggleButton ipf;
    private PanelCustomWrapper fuf;
    private JButton aTM;
    private PanelCustomWrapper DV;
    private List ipg = new ArrayList();
    private List iph = new ArrayList();
    private List ipi = new ArrayList();
    private List ipj = new ArrayList();
    private List ipk = new ArrayList();
    private a ipl;
    private j ipm;
    private k ipn;
    private boolean eoS;
    private boolean ipo;
    private boolean ipp = true;
    private boolean ipq = false;
    private boolean resizable = true;

    public TaskPaneCustomWrapper() {
        super.setLayout(new GridLayout());
        this.DV = (PanelCustomWrapper) BaseUItegXML.thisClass.CreateJComponent(TaskPaneCustomWrapper.class, "taskpane.xml");
        this.iM();
        this.Fa();
        super.addImpl(this.DV, (Object) null, -1);
        this.pack();
        ComponentManager.getCssHolder(this.ipe).setAttribute("class", "expanded");
    }

    private void iM() {
        this.ipc = (PanelCustomWrapper) this.DV.ce("northPane");
        this.ipd = this.ipc.findJLabel("title");
        this.ipe = (JButton) this.ipc.findJComponent("collapse");
        this.ipf = (JToggleButton) this.ipc.findJComponent("pin");
        this.fuf = (PanelCustomWrapper) this.DV.ce("contentPane");
        this.aTM = this.DV.findJButton("resizeButton");
    }

    private void Fa() {
        g var1 = new g();
        this.ipd.addMouseListener(var1);
        this.ipd.addMouseMotionListener(var1);
        this.ipe.addMouseListener(var1);
        this.ipf.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent var1) {
                if (var1.getClickCount() > 1) {
                    ((JToggleButton) var1.getComponent()).setSelected(!((JToggleButton) var1.getComponent()).isSelected());
                } else {
                    TaskPaneCustomWrapper.this.aw(((JToggleButton) var1.getSource()).isSelected());
                }
            }
        });
        b var2 = new b((b) null);
        this.aTM.addMouseListener(var2);
        this.aTM.addMouseMotionListener(var2);
    }

    public String getTitle() {
        return this.ipd.getText();
    }

    public void setTitle(String var1) {
        this.ipd.setText(var1);
    }

    public JLabel djn() {
        return this.ipd;
    }

    public PanelCustomWrapper djo() {
        return this.ipc;
    }

    public PanelCustomWrapper djp() {
        return this.fuf;
    }

    public void q(Component var1) {
        if (this.fuf.getComponentCount() > 0) {
            this.fuf.removeAll();
        }

        this.addImpl(var1, (Object) null, -1);
    }

    public TaskPaneContainerCustomWrapper djq() {
        if (this.ipb == null) {
            Container var1;
            for (var1 = this.getParent(); var1 != null && !(var1 instanceof TaskPaneContainerCustomWrapper); var1 = var1.getParent()) {
                ;
            }

            if (var1 != null) {
                this.ipb = (TaskPaneContainerCustomWrapper) var1;
            }
        }

        return this.ipb;
    }

    public boolean isResizable() {
        return this.resizable;
    }

    public void setResizable(boolean var1) {
        this.resizable = var1;
        this.aTM.setVisible(var1);
    }

    protected void addImpl(Component var1, Object var2, int var3) {
        this.fuf.add(var1, var2, var3);
    }

    public void setLayout(LayoutManager var1) {
        if (this.fuf != null) {
            this.fuf.setLayout(var1);
        }
    }

    public String getElementName() {
        return "taskpane";
    }

    public void setVisible(boolean var1) {
        if (var1 && !this.djr()) {
            var1 = false;
        }

        super.setVisible(var1);
        if (this.fuf != null) {
            this.fuf.setVisible(var1);
        }

    }

    private boolean m(int var1, int var2) {
        return this.resizable && (!this.isPinned() || this.resizable && (this.ipn == null || this.ipn.m(var1, var2))) && var2 >= this.getMinimumSize().height && var2 <= this.getMaximumSize().height && var2 >= this.djo().getHeight();
    }

    public void expand() {
        this.fuf.setVisible(true);
        if (this.LW()) {
            this.ipf.setEnabled(true);
            if (this.ipq) {
                this.aw(true);
            }
        }

        ComponentManager.getCssHolder(this.ipe).setAttribute("class", "expanded");

        for (int var1 = 0; var1 < this.ipi.size(); ++var1) {
            ((h) this.ipi.get(var1)).collapse(false);
        }

    }

    public void collapse() {
        this.ipq = this.isPinned();
        this.aw(false);
        if (this.LW()) {
            this.ipf.setEnabled(false);
        }

        ComponentManager.getCssHolder(this.ipe).setAttribute("class", "collapsed");
        ComponentManager.getCssHolder(this.ipe).clear();
        Insets var1 = this.getInsets();
        this.setSize(this.getWidth(), this.ipc.getHeight() + var1.top + var1.bottom);
        this.ipo = true;

        for (int var2 = 0; var2 < this.ipi.size(); ++var2) {
            ((h) this.ipi.get(var2)).collapse(true);
        }

    }

    public boolean isCollapsed() {
        return this.ipo;
    }

    public void m(boolean var1) {
        if (this.eoS != var1) {
            this.eoS = var1;
            this.ipe.setVisible(!var1);
            if (this.ipp) {
                this.ipf.setVisible(!var1);
            }

            if (this.isResizable()) {
                this.aTM.setVisible(!var1);
            }

            for (int var2 = 0; var2 < this.ipg.size(); ++var2) {
                ((c) this.ipg.get(var2)).av(var1);
            }

        }
    }

    public void aw(boolean var1) {
        if (var1) {
            if (this.isCollapsed()) {
                return;
            }

            if (!this.LW()) {
                this.ipf.setSelected(false);
                return;
            }
        }

        if (var1 != this.ipf.isSelected()) {
            this.ipf.setSelected(var1);
        }

        for (int var2 = 0; var2 < this.iph.size(); ++var2) {
            ((e) this.iph.get(var2)).aw(var1);
        }

    }

    public boolean aA() {
        return false;
    }

    public boolean isPinned() {
        return this.ipf.isSelected();
    }

    public boolean LW() {
        return this.ipp && (this.ipm == null || this.ipm.LW());
    }

    public void jR(boolean var1) {
        this.ipp = var1;
        this.ipf.setVisible(var1);
    }

    public void a(c var1) {
        this.ipg.add(var1);
    }

    public void b(c var1) {
        this.ipg.remove(var1);
    }

    public void a(e var1) {
        this.iph.add(var1);
    }

    public void b(e var1) {
        this.iph.remove(var1);
    }

    public void a(h var1) {
        this.ipi.add(var1);
    }

    public void b(h var1) {
        this.ipi.remove(var1);
    }

    public void a(f var1) {
        this.ipj.add(var1);
    }

    public void b(f var1) {
        this.ipj.remove(var1);
    }

    public void a(d var1) {
        this.ipk.add(var1);
    }

    public void b(d var1) {
        this.ipk.remove(var1);
    }

    public void a(a var1) {
        this.ipl = var1;
    }

    private boolean djr() {
        return this.ipl != null ? this.ipl.isEnabled() : true;
    }

    public void a(j var1) {
        this.ipm = var1;
    }

    public void a(k var1) {
        this.ipn = var1;
    }

    public interface h {
        void collapse(boolean var1);
    }

    public interface a {
        boolean isEnabled();
    }

    public interface j {
        boolean LW();
    }

    public interface e {
        void aw(boolean var1);
    }

    public interface d {
        void cX(int var1);
    }

    public interface k {
        boolean m(int var1, int var2);
    }

    public interface f {
        void l(int var1, int var2);
    }

    public interface c {
        void av(boolean var1);
    }

    private class g extends MouseAdapter {
        private int time = 400;
        private boolean efC = false;
        private int ixW;
        private aDX SZ = null;
        private GJ ixX;
        private GJ ixY;
        private Cursor ixZ;
        private Cursor iya;
        private jL_q iyb;

        public g() {
            TaskPaneCustomWrapper.this.ipo = !TaskPaneCustomWrapper.this.fuf.isVisible();
            this.ixX = new GJ() {
                public void a(JComponent var1) {
                    TaskPaneContainerCustomWrapper var2 = TaskPaneCustomWrapper.this.djq();
                    if (var2 != null) {
                        var2.validate();
                        var2.du(false);
                    }

                    g.this.SZ = null;
                }
            };
            this.ixY = new GJ() {
                public void a(JComponent var1) {
                    TaskPaneCustomWrapper.this.fuf.setVisible(false);
                    TaskPaneContainerCustomWrapper var2 = TaskPaneCustomWrapper.this.djq();
                    if (var2 != null) {
                        var2.validate();
                        var2.du(false);
                    }

                    g.this.SZ = null;
                }
            };
            this.iyb = new jL_q() {
                public void a(Component var1, float var2) {
                    var1.setSize(var1.getWidth(), (int) var2);
                    TaskPaneCustomWrapper.this.djq().validate();
                }

                public float a(Component var1) {
                    return (float) var1.getHeight();
                }
            };
        }

        private void dmZ() {
            TaskPaneContainerCustomWrapper var1 = TaskPaneCustomWrapper.this.djq();
            if (!TaskPaneCustomWrapper.this.ipo) {
                TaskPaneCustomWrapper.this.ipo = true;
                int var2 = TaskPaneCustomWrapper.this.getHeight();
                Insets var3 = TaskPaneCustomWrapper.this.getInsets();
                if (this.SZ != null) {
                    this.SZ.kill();
                }

                var1.du(true);
                TaskPaneCustomWrapper.this.collapse();
                this.SZ = new aDX(TaskPaneCustomWrapper.this, "[" + var2 + ".." + (TaskPaneCustomWrapper.this.ipc.getHeight() + var3.top + var3.bottom) + "] dur " + this.time + " strong_out", this.iyb, this.ixY);
                BaseUItegXML.thisClass.cx(ComponentManager.getCssHolder(TaskPaneCustomWrapper.this).Vr().aui());
            } else {
                TaskPaneCustomWrapper.this.ipo = false;
                if (this.SZ != null) {
                    this.SZ.kill();
                }

                var1.du(true);
                TaskPaneCustomWrapper.this.expand();
                this.SZ = new aDX(TaskPaneCustomWrapper.this, "[" + TaskPaneCustomWrapper.this.getHeight() + ".." + TaskPaneCustomWrapper.this.getPreferredSize().height + "] dur " + this.time + " strong_out", this.iyb, this.ixX);
                BaseUItegXML.thisClass.cx(ComponentManager.getCssHolder(TaskPaneCustomWrapper.this).Vr().aug());
            }

        }

        public void mouseClicked(MouseEvent var1) {
            if (var1.getButton() == 1 && var1.getClickCount() <= 1 && !TaskPaneCustomWrapper.this.eoS) {
                this.dmZ();
            }
        }

        public void mouseReleased(MouseEvent var1) {
            if (var1.getButton() == 1 && var1.getClickCount() <= 1 && !TaskPaneCustomWrapper.this.eoS) {
                if (this.efC) {
                    this.efC = false;
                    var1.getComponent().setCursor(this.ixZ);
                    TaskPaneContainerCustomWrapper var2 = TaskPaneCustomWrapper.this.djq();
                    if (var2 != null) {
                        var2.c((TaskPaneCustomWrapper) TaskPaneCustomWrapper.this);

                        for (int var3 = 0; var3 < TaskPaneCustomWrapper.this.ipk.size(); ++var3) {
                            ((d) TaskPaneCustomWrapper.this.ipk.get(var3)).cX(var2.l(TaskPaneCustomWrapper.this));
                        }
                    }

                }
            }
        }

        public void mousePressed(MouseEvent var1) {
            this.ixW = var1.getY();
        }

        public void mouseDragged(MouseEvent var1) {
            if (!TaskPaneCustomWrapper.this.eoS) {
                this.iya = new Cursor(13);
                Component var2 = var1.getComponent();
                if (this.efC) {
                    TaskPaneContainerCustomWrapper var3 = TaskPaneCustomWrapper.this.djq();
                    if (var3 != null) {
                        var3.b((TaskPaneCustomWrapper) TaskPaneCustomWrapper.this);
                        if (var2.getCursor() != this.iya) {
                            var2.setCursor(this.iya);
                        }
                    }

                } else if (this.SZ == null && this.ixW + 3 <= Math.abs(var1.getY())) {
                    this.ixZ = var2.getCursor();
                    var2.setCursor(this.iya);
                    this.efC = true;
                }
            }
        }
    }

    private class b extends MouseAdapter {
        private int bFn;
        private int bFo;

        private b() {
            this.bFn = -1;
            this.bFo = 0;
        }

        // $FF: synthetic method
        b(b var2) {
            this();
        }

        public void mousePressed(MouseEvent var1) {
            this.bFn = var1.getYOnScreen();
            this.bFo = TaskPaneCustomWrapper.this.getHeight();
        }

        public void mouseReleased(MouseEvent var1) {
            this.bFn = -1;
            TaskPaneCustomWrapper.this.setPreferredSize(TaskPaneCustomWrapper.this.getSize());

            for (int var2 = 0; var2 < TaskPaneCustomWrapper.this.ipj.size(); ++var2) {
                ((f) TaskPaneCustomWrapper.this.ipj.get(var2)).l(this.bFo, TaskPaneCustomWrapper.this.getHeight());
            }

        }

        public void mouseDragged(MouseEvent var1) {
            if (this.bFn >= 0) {
                TaskPaneContainerCustomWrapper var2 = TaskPaneCustomWrapper.this.djq();
                if (var2 != null) {
                    int var3 = this.bFo + var1.getYOnScreen() - this.bFn;
                    if (TaskPaneCustomWrapper.this.m(this.bFo, var3)) {
                        TaskPaneCustomWrapper.this.setSize(new Dimension(TaskPaneCustomWrapper.this.getWidth(), var3));
                        var2.validate();
                    }
                }
            }
        }
    }
}
