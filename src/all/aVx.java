package all;

import com.hoplon.geometry.Vec3f;

public abstract class aVx extends aMa {
    protected final Vec3f cUp = new Vec3f(1.0F, 1.0F, 1.0F);
    protected final Vec3f cUq = new Vec3f(-1.0F, -1.0F, -1.0F);
    protected boolean cUr = false;

    public Vec3f localGetSupportingVertexWithoutMargin(Vec3f var1) {
        this.stack.bcH().push();

        try {
            Vec3f var3 = this.stack.bcH().h(0.0F, 0.0F, 0.0F);
            float var4 = -1.0E30F;
            Vec3f var5 = this.stack.bcH().ac(var1);
            float var6 = var5.lengthSquared();
            if (var6 < 1.0E-4F) {
                var5.set(1.0F, 0.0F, 0.0F);
            } else {
                float var7 = 1.0F / (float) Math.sqrt((double) var6);
                var5.scale(var7);
            }

            Vec3f var13 = (Vec3f) this.stack.bcH().get();

            for (int var2 = 0; var2 < this.getNumVertices(); ++var2) {
                this.b(var2, var13);
                float var8 = var5.dot(var13);
                if (var8 > var4) {
                    var4 = var8;
                    var3 = var13;
                }
            }

            Vec3f var10 = (Vec3f) this.stack.bcH().aq(var3);
            return var10;
        } finally {
            this.stack.bcH().pop();
        }
    }

    public void batchedUnitVectorGetSupportingVertexWithoutMargin(Vec3f[] var1, Vec3f[] var2, int var3) {
        this.stack.bcH().push();

        try {
            Vec3f var5 = (Vec3f) this.stack.bcH().get();
            float[] var7 = new float[var3];

            int var4;
            for (var4 = 0; var4 < var3; ++var4) {
                var7[var4] = -1.0E30F;
            }

            for (int var8 = 0; var8 < var3; ++var8) {
                Vec3f var9 = var1[var8];

                for (var4 = 0; var4 < this.getNumVertices(); ++var4) {
                    this.b(var4, var5);
                    float var6 = var9.dot(var5);
                    if (var6 > var7[var8]) {
                        var2[var8].set(var5);
                        var7[var8] = var6;
                    }
                }
            }
        } finally {
            this.stack.bcH().pop();
        }

    }

    public void calculateLocalInertia(float var1, Vec3f var2) {
        this.stack.bcF();

        try {
            float var3 = this.getMargin();
            xf_g var4 = (xf_g) this.stack.bcJ().get();
            var4.setIdentity();
            Vec3f var5 = (Vec3f) this.stack.bcH().get();
            Vec3f var6 = (Vec3f) this.stack.bcH().get();
            this.getAabb(var4, var5, var6);
            Vec3f var7 = (Vec3f) this.stack.bcH().get();
            var7.sub(var6, var5);
            var7.scale(0.5F);
            float var8 = 2.0F * (var7.x + var3);
            float var9 = 2.0F * (var7.y + var3);
            float var10 = 2.0F * (var7.z + var3);
            float var11 = var8 * var8;
            float var12 = var9 * var9;
            float var13 = var10 * var10;
            float var14 = var1 * 0.08333333F;
            var2.set(var12 + var13, var11 + var13, var11 + var12);
            var2.scale(var14);
        } finally {
            this.stack.bcG();
        }

    }

    private void a(xf_g var1, Vec3f var2, Vec3f var3, float var4) {
        this.stack.bcF();

        try {
            assert this.cUr;

            assert this.cUp.x <= this.cUq.x;

            assert this.cUp.y <= this.cUq.y;

            assert this.cUp.z <= this.cUq.z;

            Vec3f var5 = (Vec3f) this.stack.bcH().get();
            var5.sub(this.cUq, this.cUp);
            var5.scale(0.5F);
            Vec3f var6 = (Vec3f) this.stack.bcH().get();
            var6.add(this.cUq, this.cUp);
            var6.scale(0.5F);
            ajD var7 = this.stack.bcK().g(var1.bFF);
            rS.b(var7);
            Vec3f var8 = this.stack.bcH().ac(var6);
            var1.G(var8);
            Vec3f var9 = (Vec3f) this.stack.bcH().get();
            Vec3f var10 = (Vec3f) this.stack.bcH().get();
            var7.getRow(0, var10);
            var9.x = var10.dot(var5);
            var7.getRow(1, var10);
            var9.y = var10.dot(var5);
            var7.getRow(2, var10);
            var9.z = var10.dot(var5);
            var9.x += var4;
            var9.y += var4;
            var9.z += var4;
            var2.sub(var8, var9);
            var3.add(var8, var9);
        } finally {
            this.stack.bcG();
        }

    }

    public void getAabb(xf_g var1, Vec3f var2, Vec3f var3) {
        this.a(var1, var2, var3, this.getMargin());
    }

    protected final void a(xf_g var1, Vec3f var2, Vec3f var3) {
        this.a(var1, var2, var3, this.getMargin());
    }

    public void aPc() {
        this.stack.bcH().push();

        try {
            this.cUr = true;

            for (int var1 = 0; var1 < 3; ++var1) {
                Vec3f var2 = this.stack.bcH().h(0.0F, 0.0F, 0.0F);
                JL.a(var2, var1, 1.0F);
                Vec3f var3 = this.stack.bcH().ac(this.localGetSupportingVertex(var2));
                JL.a(this.cUq, var1, JL.b(var3, var1) + this.collisionMargin);
                JL.a(var2, var1, -1.0F);
                var3.set(this.localGetSupportingVertex(var2));
                JL.a(this.cUp, var1, JL.b(var3, var1) - this.collisionMargin);
            }
        } finally {
            this.stack.bcH().pop();
        }

    }

    public abstract int getNumVertices();

    public abstract int abC();

    public abstract void a(int var1, Vec3f var2, Vec3f var3);

    public abstract void b(int var1, Vec3f var2);

    public abstract int abB();

    public abstract void a(Vec3f var1, Vec3f var2, int var3);

    public abstract boolean b(Vec3f var1, float var2);
}
