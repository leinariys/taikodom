package all;

import org.w3c.dom.DOMException;

import java.util.Locale;
import java.util.ResourceBundle;

public class qN extends DOMException {
    public static int aWt = 0;
    public static int aWu = 1;
    public static int aWv = 2;
    public static int aWw = 3;
    public static int aWx = 4;
    public static int aWy = 5;
    public static int aWz = 6;
    public static int aWA = 7;
    public static int aWB = 8;
    public static int aWC = 9;
    public static int aWD = 10;
    public static int aWE = 11;
    public static int aWF = 12;
    public static int aWG = 13;
    public static int aWH = 14;
    public static int aWI = 15;
    public static int aWJ = 16;
    public static int aWK = 17;
    public static int aWL = 18;
    public static int aWM = 19;
    private static ResourceBundle aWN = ResourceBundle.getBundle(zU_q.class.getName(), Locale.getDefault());

    public qN(short var1, int var2) {
        super(var1, aWN.getString(ef(var2)));
    }

    public qN(int var1, int var2) {
        super((short) var1, aWN.getString(ef(var2)));
    }

    public qN(short var1, int var2, String var3) {
        super(var1, aWN.getString(ef(var2)));
    }

    private static String ef(int var0) {
        return "s" + String.valueOf(var0);
    }
}
