package all;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

public class de implements ds {
    private static Map yj = new HashMap();

    static {
        yj.put(Boolean.TYPE, new de() {
            public Object O(String var1) {
                if ("true".equals(var1)) {
                    return Boolean.TRUE;
                } else if ("1".equals(var1)) {
                    return Boolean.TRUE;
                } else {
                    return "yes".equals(var1) ? Boolean.TRUE : Boolean.FALSE;
                }
            }
        });
        yj.put(Integer.TYPE, new de() {
            public Object O(String var1) {
                return new Integer(var1);
            }
        });
        yj.put(Byte.TYPE, new de() {
            public Object O(String var1) {
                return new Byte((byte) Integer.parseInt(var1));
            }
        });
        yj.put(Short.TYPE, new de() {
            public Object O(String var1) {
                return new Short((short) Integer.parseInt(var1));
            }
        });
        yj.put(Long.TYPE, new de() {
            public Object O(String var1) {
                return new Long(var1);
            }
        });
        yj.put(Double.TYPE, new de() {
            public Object O(String var1) {
                return new Double(var1);
            }
        });
        yj.put(Float.TYPE, new de() {
            public Object O(String var1) {
                return new Float((float) Double.parseDouble(var1));
            }
        });
        yj.put(Boolean.class, (de) yj.get(Boolean.TYPE));
        yj.put(Integer.class, (de) yj.get(Integer.TYPE));
        yj.put(Byte.class, (de) yj.get(Byte.TYPE));
        yj.put(Short.class, (de) yj.get(Short.TYPE));
        yj.put(Long.class, (de) yj.get(Long.TYPE));
        yj.put(Double.class, (de) yj.get(Double.TYPE));
        yj.put(Float.class, (de) yj.get(Float.TYPE));
    }

    protected Constructor yi;
    protected Class clazz;

    protected de() {
    }

    public de(Class var1) throws NoSuchMethodException {
        this.clazz = var1;
        this.yi = var1.getConstructor(String.class);
    }

    public static void a(Class var0, de var1) {
        yj.put(var0, var1);
    }

    public static de c(Class var0) {
        de var1 = (de) yj.get(var0);
        return var1;
    }

    public Object O(String var1) throws IllegalAccessException, InvocationTargetException, InstantiationException {
        try {
            return this.yi.newInstance(var1);
        } catch (InvocationTargetException | InstantiationException | IllegalAccessException var5) {
            if (Number.class.isAssignableFrom(this.clazz) && var5.getCause() != null && var5.getCause() instanceof NumberFormatException) {
                double var3 = Double.parseDouble(var1);
                if (var3 % 1.0D == 0.0D) {
                    return this.yi.newInstance(String.valueOf((long) var3));
                }
            }

            throw var5;
        }
    }
}
