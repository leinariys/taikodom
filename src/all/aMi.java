package all;

import java.util.SortedMap;

//TODO На удаление
public interface aMi {
    float getTrans_rerunPerSecond();

    String getTransactionalReport();

    long getUpTimeInMillis();

    String getUpTime();

    long getTrans_averageTime();

    long getTrans_maxTime();

    long getTrans_minTime();

    double getTrans_stdDeviation();

    float getTrans_perSecond();

    float getWriteLocks_perSecond();

    float getPulls_perSecond();

    long getPullCount();

    float getDataReq_perSecond();

    float getCPU_usage();

    int getScriptObjectCount();

    SortedMap getTransactionalMethods();

    int getPendingRequestsCount();
}
