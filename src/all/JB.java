package all;

//обёртка сетевого сообщения
public final class JB extends aBB {
    private boolean special;
    private boolean compressed;
    private int dkw;

    public JB(byte[] var1, int var2, int var3) {
        super(var1, var2, var3);
    }

    public JB(b var1, byte[] var2) {
        super(var1, var2);
    }

    public JB(byte[] var1) {
        super(var1);
    }

    public JB(ala var1) {
        super(var1);
    }

    public boolean isSpecial() {
        return this.special;
    }

    //особое/специальное
    public void setSpecial(boolean var1) {
        this.special = var1;
    }

    public boolean isCompressed() {
        return this.compressed;
    }

    public void cT(boolean var1) {
        this.compressed = var1;
    }

    public void le(int var1) {
        this.dkw = var1;
    }

    public int aYP() {
        return this.dkw;
    }
}
