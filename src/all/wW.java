package all;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.ByteBuffer;

public class wW implements Serializable {
    public int bFf;
    public ByteBuffer bFg;
    public int bFh;
    public int numVertices;
    public ByteBuffer bFi;
    public int bFj;
    public aop bFk;

    private void writeObject(ObjectOutputStream var1) throws IOException {
        var1.writeInt(this.bFf);
        var1.writeInt(this.bFh);
        var1.writeInt(this.numVertices);
        var1.writeInt(this.bFj);
        var1.writeInt(this.bFg.array().length);
        var1.write(this.bFg.array());
        var1.writeInt(this.bFi.array().length);
        var1.write(this.bFi.array());
    }

    private void readObject(ObjectInputStream var1) throws IOException {
        this.bFk = aop.ghD;
        this.bFf = var1.readInt();
        this.bFh = var1.readInt();
        this.numVertices = var1.readInt();
        this.bFj = var1.readInt();
        byte[] var2 = new byte[var1.readInt()];
        var1.read(var2);
        this.bFg = ByteBuffer.allocate(var2.length);
        this.bFg.put(var2);
        byte[] var3 = new byte[var1.readInt()];
        var1.read(var3);
        this.bFi = ByteBuffer.allocate(var3.length);
        this.bFi.put(var3);
    }
}
