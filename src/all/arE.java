package all;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.ObjectStreamClass;
import java.util.Collection;
import java.util.Map;

public interface arE {
    int getMagicNumber(Class var1);

    Class tS(int var1);

    ObjectStreamClass getObjectStreamClassFromMagicNumber(int var1);

    int a(DataInput var1);

    void a(DataOutput var1, int var2);

    String jK(String var1);

    String jL(String var1);

    void at(Class var1);

    void jM(String var1);

    void B(Map var1);

    Collection getClasses();
}
