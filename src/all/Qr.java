package all;

import javax.management.InstanceAlreadyExistsException;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.io.EOFException;
import java.io.IOException;
import java.io.UTFDataFormatException;
import java.lang.management.ManagementFactory;
import java.net.InetSocketAddress;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.ServerSocketChannel;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

public class Qr extends dm_q implements aBE {
    private final ThreadPoolExecutor dVD;
    private int dVB;
    private b.a dVC;
    private om_q AB;
    private long dVE;
    private Map dVF;
    private TcpNioServerNetworkConsole dVG;

    public Qr() throws IOException {
        this(6667, (String) null);
    }

    public Qr(int var1, String var2) throws IOException {
        super();

        this.dVC = b.a.j;
        this.dVF = new HashMap();
        this.dVB = var1;
        this.versionString = var2;
        this.yQ.a((aBE) this);
        this.dVD = PoolThread.a(1, 1, 60L, TimeUnit.SECONDS, new LinkedBlockingQueue(), "Network Executer", false);
        this.aqT();
    }

    public kz a(b.a var1, ServerSocketChannel var2, int[] var3, int[] var4, boolean var5) throws ClosedChannelException {
        kz var6 = new kz(var2, b.a.j, this, var3, var4, var5);
        this.yQ.a((XF_w) var6);
        return var6;
    }

    public void a(int var1, int[] var2, int[] var3) throws IOException {
        this.a(var1, var2, var3, true);
    }

    public void a(int var1, int[] var2, int[] var3, boolean var4) throws IOException {
        ServerSocketChannel var5 = ServerSocketChannel.open();
        var5.configureBlocking(false);
        var5.socket().bind(new InetSocketAddress(var1));
        if (var2 != null) {
            if (var3 == null) {
                var3 = new int[]{255, 255, 255, 255};
            }
        } else {
            var3 = (int[]) null;
        }

        this.dVF.put(var1, this.a(this.dVC, var5, var2, var3, var4));
    }

    public void af(int var1) throws IOException {
        kz var2 = (kz) this.dVF.remove(var1);
        if (var2 != null) {
            var2.gLA.close();
        }

    }

    public Map bpx() {
        return this.dVF;
    }

    public void a(om_q var1) {
        this.AB = var1;

        try {
            this.a(this.dVB, (int[]) null, (int[]) null);
            this.start();
        } catch (IOException var3) {
            var3.printStackTrace();
        }

    }

    protected void d(final LZ var1, JB var2) throws EOFException, UTFDataFormatException {
        aMf var3 = new aMf(var2.cJn(), var2.getOffset(), var2.getLength());
        final String var4 = var3.readUTF();
        final String var5 = var3.readUTF();
        final boolean var6 = var3.readBoolean();
        var1.a(LZ.a_q.eQq);
        this.dVD.execute(new Runnable() {
            public void run() {
                try {
                    om_q.a_q var1x = om_q.a_q.aNI;
                    int var2 = -1;

                    Qr.this.AB.a(var1.bgt(), var4, var5, var6);
                    var1.a(LZ.a_q.eQr);
                    Qr.this.d(var1);
                    ala var3 = new ala();
                    var3.writeInt(LZ.a_q.eQp.ordinal());
                    var3.writeInt(om_q.a_q.aNF.ordinal());
                    var3.writeInt(var2);
                    byte[] var8 = var3.toByteArray();
                    Qr.this.e(var1, new JB(var8));
                } catch (Exception var7) {
                    Qr.logger.error("Severe error authenticating: ", var7);
                    var1.bgv();
                }

            }
        });
    }

    protected void e(final LZ var1) {
        try {
            super.e(var1);
        } finally {
            if (!this.dVD.isShutdown()) {
                try {
                    this.dVD.execute(new Runnable() {
                        public void run() {
                            Qr.this.AB.d(var1.bgt());
                        }
                    });
                } catch (RejectedExecutionException var7) {
                    logger.warn(var7.getMessage());
                }
            }

        }

    }

    public void close() {
        super.close();
        this.dVD.shutdown();
    }

    public void bpy() {
        if (System.currentTimeMillis() - this.dVE > 10000L) {
            this.dVE = System.currentTimeMillis();
            Iterator var2 = this.yQ.bXH().iterator();

            while (var2.hasNext()) {
                SelectionKey var1 = (SelectionKey) var2.next();

                try {
                    if (var1.attachment() instanceof LZ) {
                        LZ var3 = (LZ) var1.attachment();
                        long var4 = System.currentTimeMillis() - var3.bgx();
                        if ((double) var4 > 600000.0D) {
                            try {
                                var1.channel().close();
                            } finally {
                                var3.bgr();
                            }
                        } else if ((double) var4 > 60000.0D && (double) (System.currentTimeMillis() - var3.bgy()) > 50000.0D) {
                            var3.b(this.yT);
                        }
                    }
                } catch (IOException var10) {
                    var10.printStackTrace();
                }
            }
        }

    }

    public void a(LZ var1, JB var2) throws IOException, InterruptedException {
        if (var1.bgq() == LZ.a_q.eQr && this.dVG != null) {
            this.dVG.iSr.addAndGet((long) var2.getLength());
            this.dVG.iSs.incrementAndGet();
        }

        super.a(var1, var2);
    }

    protected void e(LZ var1, JB var2) throws IOException {
        if (this.dVG != null) {
            this.dVG.iSt.addAndGet((long) var2.getLength());
            this.dVG.iSu.incrementAndGet();
        }

        super.e(var1, var2);
    }

    private void aqT() {
        MBeanServer var1 = ManagementFactory.getPlatformMBeanServer();
        if (var1 != null) {
            this.dVG = new TcpNioServerNetworkConsole();

            try {
                String var2 = "Bitverse:name=NetworkTraffic";
                ObjectName var3 = new ObjectName(var2);
                var1.registerMBean(this.dVG, var3);
            } catch (InstanceAlreadyExistsException var4) {
                System.err.println(var4.getMessage());
            } catch (Exception var5) {
                var5.printStackTrace();
            }

        }
    }

    public interface TcpNioServerNetworkConsoleMBean {
        int getNumberOfConnections();

        float getOutgoingBytesPerSecond();

        float getIncomingBytesPerSecond();

        float getOutgoingMessagesPerSecond();

        float getIncomingMessagesPerSecond();

        float[] getServerOutgoingMessagesStats();
    }

    public class TcpNioServerNetworkConsole implements TcpNioServerNetworkConsoleMBean {
        public AtomicLong iSr = new AtomicLong();
        public AtomicLong iSs = new AtomicLong();
        public AtomicLong iSt = new AtomicLong();
        public AtomicLong iSu = new AtomicLong();
        long iSz = System.currentTimeMillis();
        private float iSv;
        private float iSw;
        private float iSx;
        private float iSy;

        public int getNumberOfConnections() {
            return Qr.this.getNumberOfConnections();
        }

        public float getIncomingBytesPerSecond() {
            this.dwk();
            return this.iSv;
        }

        public float getIncomingMessagesPerSecond() {
            this.dwk();
            return this.iSw;
        }

        public float getOutgoingBytesPerSecond() {
            this.dwk();
            return this.iSx;
        }

        public float getOutgoingMessagesPerSecond() {
            this.dwk();
            return this.iSy;
        }

        public float[] getServerOutgoingMessagesStats() {
            LZ[] var1 = Qr.this.jM();
            int var2 = Integer.MAX_VALUE;
            int var3 = 0;
            int var4 = 0;
            int var5 = 0;
            float var6 = 0.0F;
            float var7 = 0.0F;
            int var8 = 0;
            LZ[] var12 = var1;
            int var11 = var1.length;

            LZ var9;
            int var10;
            int var13;
            for (var10 = 0; var10 < var11; ++var10) {
                var9 = var12[var10];
                if (var9.bgA()) {
                    var13 = var9.bgz();
                    var4 += var13;
                    if (var13 > var3) {
                        var3 = var13;
                    }

                    if (var13 < var2) {
                        var2 = var13;
                    }

                    ++var8;
                }
            }

            if (var8 == 0) {
                var6 = 0.0F;
                var2 = 0;
                var7 = 0.0F;
            } else {
                var6 = (float) var4 / (float) var8;
                var12 = var1;
                var11 = var1.length;

                for (var10 = 0; var10 < var11; ++var10) {
                    var9 = var12[var10];
                    if (var9.bgA()) {
                        var13 = var9.bgz();
                        var5 = (int) ((float) var5 + ((float) var13 - var6) * ((float) var13 - var6));
                    }
                }

                var7 = (float) Math.sqrt((double) var5 / (double) var8);
            }

            return new float[]{(float) var8, var6, var7, (float) var2, (float) var3};
        }

        private void dwk() {
            long var1 = System.currentTimeMillis();
            long var3 = var1 - this.iSz;
            if (var3 >= 1000L) {
                this.iSv = this.a(this.iSr, var3);
                this.iSw = this.a(this.iSs, var3);
                this.iSx = this.a(this.iSt, var3);
                this.iSy = this.a(this.iSu, var3);
                this.iSz = var1;
            }
        }

        private float a(AtomicLong var1, long var2) {
            long var4 = var1.getAndSet(0L);
            return (float) var4 * 1000.0F / (float) var2;
        }
    }
}
