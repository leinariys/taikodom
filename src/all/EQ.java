package all;

import org.w3c.dom.css.*;

import java.io.Reader;
import java.io.StringReader;
import java.util.HashMap;

/**
 * конвектор значений CSS
 */
public class EQ {
    private static HashMap listColor = new HashMap();

    static {
        listColor.put("black", new Bj(new ColorCSS(0, 0, 0)));
        listColor.put("white", new Bj(new ColorCSS(255, 255, 255)));
        listColor.put("red", new Bj(new ColorCSS(255, 0, 0)));
        listColor.put("yellow", new Bj(new ColorCSS(255, 255, 0)));
        listColor.put("lime", new Bj(new ColorCSS(0, 255, 0)));
        listColor.put("aqua", new Bj(new ColorCSS(0, 255, 255)));
        listColor.put("blue", new Bj(new ColorCSS(0, 0, 255)));
        listColor.put("fuchsia", new Bj(new ColorCSS(255, 0, 255)));
        listColor.put("gray", new Bj(new ColorCSS(128, 128, 128)));
        listColor.put("silver", new Bj(new ColorCSS(192, 192, 192)));
        listColor.put("maroon", new Bj(new ColorCSS(128, 0, 0)));
        listColor.put("olive", new Bj(new ColorCSS(128, 128, 0)));
        listColor.put("green", new Bj(new ColorCSS(0, 128, 0)));
        listColor.put("teal", new Bj(new ColorCSS(0, 128, 128)));
        listColor.put("navy", new Bj(new ColorCSS(0, 0, 128)));
        listColor.put("purple", new Bj(new ColorCSS(128, 0, 128)));
    }

    CssNode currentCssNode;

    public EQ(CssNode currentCssNode) {
        this.currentCssNode = currentCssNode;
    }

    public static CSSStyleRule a(CSSStyleRule var0) {
        parseRule(var0.getStyle());
        return var0;
    }

    //парсинг правил селектора
    public static CSSStyleDeclaration parseRule(CSSStyleDeclaration var0) {
        for (int var1 = 0; var1 < var0.getLength(); ++var1) {
            String var2 = var0.item(var1);//названиесвойства
            if (var2.equals("color") || var2.equals("background-color") || var2.equals("border-color")) {
                CSSValue var3 = var0.getPropertyCSSValue(var2);//Значение свойства предварительное
                if (var3.getCssValueType() != 1 || ((CSSPrimitiveValue) var3).getPrimitiveType() != 25) {
                    String var4 = var0.getPropertyValue(var2);//Значение свойства действительное
                    Bj var5 = convert(var4);//Значение свойства нормализованное
                    if (var0 instanceof aHx) {
                        ((aHx) var0).setPropertyCSS(var2, var5);
                    } else {
                        var0.setProperty(var2, var5.toString(), (String) null);
                    }
                }
            }

            if (var2.equals("padding")) {
                a(var2, var0);
            }

            if (var2.equals("margin")) {
                a(var2, var0);
            }

            if (var2.equals("border-width")) {
                a("border-width", var0, "border", "-width");
            }

            if (var2.equals("background-position")) {
                b(var0);
            }
        }

        return var0;
    }

    public static void a(String var0, CSSStyleDeclaration var1) {
        a(var0, var1, var0, "");
    }

    public static void b(CSSStyleDeclaration var0) {
        CSSValue var1 = var0.getPropertyCSSValue("background-position");
        if (var1.getCssValueType() != 2) {
            if (var1.getCssValueType() == 1) {
                String var2 = var1.getCssText();
                if (var2.startsWith("top")) {
                    var0.setProperty("background-position", "50% 0%", (String) null);
                }

                if (var2.startsWith("bottom")) {
                    var0.setProperty("background-position", "50% 100%", (String) null);
                }

                if (var2.startsWith("left")) {
                    var0.setProperty("background-position", "0% 50%", (String) null);
                }

                if (var2.startsWith("right")) {
                    var0.setProperty("background-position", "100% 50%", (String) null);
                }

            }
        }
    }

    private static void a(String var0, CSSStyleDeclaration var1, String var2, String var3) {
        CSSValue var4 = var1.getPropertyCSSValue(var0);
        CSSValueList var5 = (CSSValueList) var4;
        Object var9 = null;
        Object var8 = null;
        Object var7 = null;
        Object var6 = null;
        if (var4.getCssValueType() == 2) {
            if (var5.getLength() == 2) {
                var6 = var5.item(0);
                var7 = var5.item(0);
                var8 = var5.item(1);
                var9 = var5.item(1);
            }

            if (var5.getLength() == 3) {
                var6 = var5.item(0);
                var8 = var5.item(1);
                var9 = var5.item(1);
                var7 = var5.item(2);
            }

            if (var5.getLength() == 4) {
                var6 = var5.item(0);
                var9 = var5.item(1);
                var7 = var5.item(2);
                var8 = var5.item(3);
            }
        } else {
            var6 = var5;
            var7 = var5;
            var8 = var5;
            var9 = var5;
        }

        if (var1 instanceof aHx) {
            aHx var10 = (aHx) var1;
            var10.setPropertyCSS(var2 + "-top" + var3, (Bj) var6);
            var10.setPropertyCSS(var2 + "-bottom" + var3, (Bj) var7);
            var10.setPropertyCSS(var2 + "-left" + var3, (Bj) var8);
            var10.setPropertyCSS(var2 + "-right" + var3, (Bj) var9);
        } else {
            var1.setProperty(var2 + "-top" + var3, ((CSSValue) var6).getCssText(), (String) null);
            var1.setProperty(var2 + "-bottom" + var3, ((CSSValue) var7).getCssText(), (String) null);
            var1.setProperty(var2 + "-left" + var3, ((CSSValue) var8).getCssText(), (String) null);
            var1.setProperty(var2 + "-right" + var3, ((CSSValue) var9).getCssText(), (String) null);
        }

    }

    //стринговое значение цвета на ргб
    public static Bj convert(String var0) {
        if (var0.indexOf("rgb") >= 0) {
            return null;
        } else {
            Bj var1 = (Bj) listColor.get(var0.toLowerCase());
            return var1;
        }
    }

    public void setCurrentCssNode(CssNode currentCssNode) {
        this.currentCssNode = currentCssNode;
    }

    /**
     * Выделение блакировцика ресурсов, чтение CSS стилей
     *
     * @param var1 адрес ресурса на чтение
     */
    public void parse(Reader var1) {
        ParserCss var2 = ParserCss.getParserCss();
        InputSource var3 = new InputSource(var1);
        CSSStyleSheet var4 = var2.parseCSS(var3);
        this.currentCssNode.add(var4);
        this.a(var4);
    }

    public void a(RC var1) {
        String var2 = var1.getAttribute("style");
        if (var2 != null && !this.currentCssNode.b((asi) var1)) {
            ParserCss var3 = ParserCss.getParserCss();
            CSSStyleSheet var4 = var3.parseCSS(new InputSource(new StringReader(var2)));
            this.currentCssNode.add(var4);
            this.currentCssNode.a((asi) var1);
            this.a(var4);
        }

        ahv var6 = var1.Vl();

        for (int var7 = 0; var7 < var6.getLength(); ++var7) {
            asi var5 = var6.qR(var7);
            if (var5.getNodeType() == 0) {
                this.a((RC) var5);
            }
        }
    }

    public void a(CSSStyleSheet var1) {
        CSSRuleList var2 = var1.getCssRules();
        ParserCss var3 = null;

        for (int var4 = 0; var4 < var2.getLength(); ++var4) {
            CSSRule var5 = var2.item(var4);//Достаёт из коллекции ролей селектор
            if (var5.getType() == 1) {
                CSSStyleRule var6 = (CSSStyleRule) var5;
                var6 = a(var6);
                lD var7 = new lD();
                var7.cssRule = var6;
                var7.cssStyleSheet = var1;
                if (var6 instanceof aQJ_q) {
                    var7.selectorList = ((aQJ_q) var6).dqd();
                } else {
                    String var8 = var6.getSelectorText();
                    if (var3 == null) {
                        var3 = ParserCss.getParserCss();
                    }

                    SelectorList var9 = var3.g(new InputSource(new StringReader(var8)));
                    var7.selectorList = var9;
                }

                var7.cssStyleDeclaration = var6.getStyle();
                this.currentCssNode.add(var7);
            }
        }
    }
}
