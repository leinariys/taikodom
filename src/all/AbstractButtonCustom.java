package all;

import gnu.trove.TObjectIntHashMap;

import javax.swing.*;
import java.awt.*;

public abstract class AbstractButtonCustom extends BaseItem {
    static TObjectIntHashMap biH = new TObjectIntHashMap();
    static TObjectIntHashMap biI = new TObjectIntHashMap();

    static {
        biH.put("left", 2);
        biH.put("center", 0);
        biH.put("right", 4);
        biH.put("leading", 10);
        biH.put("trailing", 11);
        biI.put("top", 1);
        biI.put("center", 0);
        biI.put("bottom", 3);
    }

    protected abstract AbstractButton f(MapKeyValue var1, Container var2, XmlNode var3);

    protected void a(MapKeyValue var1, AbstractButton var2, XmlNode var3) {
        super.applyAttribute(var1, (Component) var2, (XmlNode) var3);
        if ("true".equals(var3.getAttribute("selected"))) {
            var2.setSelected(true);
        }

        String var4 = var3.getAttribute("horizontalTextPosition");
        String var5;
        if (var4 != null) {
            var5 = var4.toLowerCase();
            if (biH.containsKey(var5)) {
                var2.setHorizontalTextPosition(biH.get(var5));
            }
        }

        var4 = var3.getAttribute("verticalTextPosition");
        if (var4 != null) {
            var5 = var4.toLowerCase();
            if (biI.containsKey(var5)) {
                var2.setVerticalTextPosition(biI.get(var5));
            }
        }

        var4 = var3.getAttribute("text");
        if (var4 != null) {
            var2.setText(var4);
        }

        XmlNode var6 = var3.c(0, "text", (String) null, (String) null);
        if (var6 != null) {
            var2.setText(var6.getText());
        }

        if (!"true".equals(var3.getAttribute("focusable"))) {
            var2.setFocusable(false);
        }

    }
}
