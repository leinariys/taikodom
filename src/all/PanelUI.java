package all;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicPanelUI;
import java.awt.*;

/**
 * PanelUI
 */
public class PanelUI extends BasicPanelUI implements aIh {
    protected ComponentManager Rp;

    protected PanelUI() {
    }

    public PanelUI(JPanel var1) {
        this.Rp = new ComponentManager("panel", var1);
        var1.setOpaque(false);
    }

    public static ComponentUI createUI(JComponent var0) {
        return new PanelUI((JPanel) var0);
    }

    public void update(Graphics var1, JComponent var2) {
        RM.a((aIh) this, (Graphics) var1, (JComponent) var2);
        super.paint(var1, var2);
    }

    public void installUI(JComponent var1) {
        super.installUI(var1);
        this.Rp.clear();
    }

    public Dimension getPreferredSize(JComponent var1) {
        RM.a((aIh) this, (JComponent) var1);
        Dimension var2 = super.getPreferredSize(var1);
        if (var2 == null && var1 instanceof BoxCustomWrapper) {
            Container var3 = var1.getParent();
            if (var3 != null) {
                var2 = ((BoxCustomWrapper) var1).getPreferredSize(var3.getWidth(), var3.getHeight());
            } else {
                var2 = ((BoxCustomWrapper) var1).getPreferredSize(0, 0);
            }
        }

        return this.wy().c(var1, var2);
    }

    public Dimension getMinimumSize(JComponent var1) {
        RM.a((aIh) this, (JComponent) var1);
        Dimension var2 = super.getMinimumSize(var1);
        if (var2 == null && var1 instanceof BoxCustomWrapper) {
            Container var3 = var1.getParent();
            if (var3 != null) {
                var2 = ((BoxCustomWrapper) var1).getMinimumSize(var3.getWidth(), var3.getHeight());
            } else {
                var2 = ((BoxCustomWrapper) var1).getMinimumSize(0, 0);
            }
        }

        return this.wy().b(var1, var2);
    }

    public Dimension getMaximumSize(JComponent var1) {
        RM.a((aIh) this, (JComponent) var1);
        return this.wy().a(var1, super.getMaximumSize(var1));
    }

    public ComponentManager wy() {
        return this.Rp;
    }

    public boolean contains(JComponent var1, int var2, int var3) {
        return !ComponentManager.getCssHolder(var1).Vs() ? false : super.contains(var1, var2, var3);
    }


    /**
     * TODO похлже это public pv wy()
     *
     * @return Графический элемент
     */
    @Override
    public aeK wz() {
        return this.Rp;
    }
}
