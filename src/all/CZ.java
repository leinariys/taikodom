package all;

/**
 * class InterfaceSFXAddon
 */
public class CZ {
    private final aeO cDc;
    private final boolean stop;

    public CZ(aeO var1) {
        this(var1, false);
    }

    public CZ(aeO var1, boolean var2) {
        this.cDc = var1;
        this.stop = var2;
    }

    public aeO aDI() {
        return this.cDc;
    }

    public boolean aDJ() {
        return this.stop;
    }
}
