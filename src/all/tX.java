package all;

import org.w3c.dom.css.CSSStyleDeclaration;

import java.io.StringReader;
import java.util.List;
import java.util.Locale;

class tX {
    /**
     * Список заголовков селекторов name и не только
     */
    List listTitleSelector;

    /**
     * @param var1 Список заголовков селекторов name и не только
     */
    public tX(List var1) {
        this.listTitleSelector = var1;
    }

    public static boolean matches(String var0, String var1) {
        if (var0 != null && var1 != null) {
            int var2 = var0.length();
            int var3 = var1.length();
            if (var2 < var3) {
                return false;
            } else {
                int var4 = 0;
                boolean var5 = true;

                for (int var6 = 0; var6 < var2; ++var6) {
                    if (var2 - var6 < var3 - var4) {
                        return false;
                    }

                    char var7 = var0.charAt(var6);
                    if (Character.isSpaceChar(var7)) {
                        if (var4 == var3) {
                            return true;
                        }

                        var5 = true;
                        var4 = 0;
                    } else if (var5) {
                        if (var4 < var3 && var1.charAt(var4) == var7) {
                            ++var4;
                        } else {
                            var5 = false;
                            var4 = 0;
                        }
                    }
                }

                return var4 == var3;
            }
        } else {
            return false;
        }
    }

    public CSSStyleDeclaration a(asi var1, String var2, boolean var3) {
        for (int var4 = this.listTitleSelector.size() - 1; var4 >= 0; --var4) {
            lD var5 = (lD) this.listTitleSelector.get(var4);

            for (int var6 = 0; var6 < var5.selectorList.getLength(); ++var6) {
                ISelector var7 = var5.selectorList.item(var6);
                if (this.a(var7, var1)) {
                    CSSStyleDeclaration var8 = var5.cssStyleDeclaration;
                    if (var8 != null && var8.getPropertyValue(var2) != null && !var8.getPropertyValue(var2).equals("")) {
                        return var8;
                    }
                }
            }
        }

        if (var3 && var1.Vm() != null) {
            return this.a(var1.Vm(), var2, var3);
        } else {
            return null;
        }
    }

    public boolean a(String var1, asi var2) {
        try {
            ParserCss var3 = ParserCss.getParserCss();
            SelectorList var4 = var3.g(new InputSource(new StringReader(var1)));

            for (int var5 = 0; var5 < var4.getLength(); ++var5) {
                ISelector var6 = var4.item(var5);
                if (this.a(var6, var2)) {
                    return true;
                }
            }
        } catch (Exception var7) {
            System.err.println(var7);
        }

        return false;
    }

    //распределитель селекторов Пример на  picture на .size4
    private boolean a(ISelector var1, asi var2) {
        switch (var1.getSelectorType()) {
            case 0:
                return this.a((IConditionalSelector) var1, var2);
            case 1:
            case 2:
            case 3:
            case 5:
            case 6:
            case 7:
            case 8:
            default:
                System.err.println("unrecognized selector type: " + var1 + " Element = " + var2.getClass());
                return false;
            case 4:
                return this.b((IElementSelector) var1, var2);
            case 9:
                return this.a((IElementSelector) var1, var2);
            case 10:
                return this.b((IDescendantSelector) var1, var2);
            case 11:
                return this.a((IDescendantSelector) var1, var2);
        }
    }

    private boolean a(IElementSelector var1, asi var2) {
        if (var1.getLocalName() == null) {
            return true;
        } else {
            return var2.ba(var1.getLocalName());
        }
    }

    private boolean b(IElementSelector var1, asi var2) {
        if (var1.getLocalName() == null) {
            return true;
        } else {
            return this.a(var2, var1.getLocalName());
        }
    }

    private boolean a(asi var1, String var2) {
        String var3 = var1.getNodeName();
        return var3.equalsIgnoreCase(var2);
    }

    private boolean a(IConditionalSelector var1, asi var2) {
        Condition var3 = var1.getCondition();
        return this.a((ISelector) var1.getSimpleSelector(), (asi) var2) && this.a(var3, var1, var2);
    }

    // распределитель
    private boolean a(Condition var1, IConditionalSelector var2, asi var3) {
        switch (var1.getConditionType()) {
            case 0:
                xr var6 = (xr) var1;
                if (this.a(var6.anF(), var2, var3) && this.a(var6.anG(), var2, var3)) {
                    return true;
                }

                return false;
            case 1:
            case 3:
            case 7:
            case 8:
            default:
                System.err.println("unrecognized condition type:  " + var1 + " " + var1.getConditionType() + " --> " + var1.getClass());
                return false;
            case 2:
                on var5 = (on) var1;
                return !this.a(var5.SW(), var2, var3);
            case 4:
                return this.a((akt_q) var1, var2.getSimpleSelector(), var3);
            case 5:
                return this.d((akt_q) var1, var2.getSimpleSelector(), var3);
            case 6:
                aIb var4 = (aIb) var1;
                return var4.getLang().equalsIgnoreCase(Locale.getDefault().getLanguage());
            case 9:
                return this.c((akt_q) var1, var2.getSimpleSelector(), var3);
            case 10:
                return this.b((akt_q) var1, var2.getSimpleSelector(), var3);
        }
    }

    private boolean a(akt_q var1, ISimpleSelector var2, asi var3) {
        return var1.getValue().equals(((RC) var3).getAttribute(var1.getLocalName()));
    }

    private boolean b(akt_q var1, ISimpleSelector var2, asi var3) {
        return var3 == null ? false : var3.ba(var1.getValue());
    }

    private boolean a(IDescendantSelector var1, asi var2) {
        ISimpleSelector var3 = var1.getSimpleSelector();
        ISelector var4 = var1.getAncestorSelector();
        if (this.a((ISelector) var3, (asi) var2)) {
            asi var5 = var2.Vm();
            if (var5 != null && this.a(var4, var5)) {
                return true;
            }
        }

        return false;
    }

    private boolean b(IDescendantSelector var1, asi var2) {
        ISimpleSelector var3 = var1.getSimpleSelector();
        ISelector var4 = var1.getAncestorSelector();
        if (!this.a((ISelector) var3, (asi) var2)) {
            return false;
        } else {
            RC var5 = (RC) var2;

            while (true) {
                RC var6 = (RC) var5.Vm();
                if (var6 == null) {
                    return false;
                }

                if (this.a((ISelector) var4, (asi) var6)) {
                    return true;
                }

                var5 = var6;
            }
        }
    }

    private boolean c(akt_q var1, ISimpleSelector var2, asi var3) {
        RC var4 = (RC) var3;
        if (!var4.hasAttribute("class")) {
            return false;
        } else {
            String var5 = var4.getAttribute("class");
            if (!matches(var5, var1.getValue())) {
                return false;
            } else {
                IElementSelector var6 = (IElementSelector) var2;
                if (var6.getLocalName() == null) {
                    return true;
                } else {
                    return this.a(var3, ((IElementSelector) var2).getLocalName());
                }
            }
        }
    }

    private boolean d(akt_q var1, ISimpleSelector var2, asi var3) {
        RC var4 = (RC) var3;
        if (!var4.hasAttribute("id")) {
            return false;
        } else if (!var4.getAttribute("id").equals(var1.getValue())) {
            return false;
        } else {
            IElementSelector var5 = (IElementSelector) var2;
            if (var5.getLocalName() == null) {
                return true;
            } else {
                return this.a(var3, ((IElementSelector) var2).getLocalName());
            }
        }
    }

    //проход по CSS файлу селекторов и разбивка селесторов
    public boolean a(asi var1, aKV_q var2, boolean var3) {
        for (int var4 = this.listTitleSelector.size() - 1; var4 >= 0; --var4) {
            lD var5 = (lD) this.listTitleSelector.get(var4);//достаём из списка полный заголовок селектора

            for (int var6 = 0; var6 < var5.selectorList.getLength(); ++var6) {
                ISelector var7 = var5.selectorList.item(var6);
                if (this.a(var7, var1)) {
                    CSSStyleDeclaration var8 = var5.cssStyleDeclaration;
                    if (!var2.c(var8)) {
                        return false;
                    }
                }
            }
        }

        if (var3) {
            asi var9 = var1.Vm();
            if (var9 != null) {
                return this.a(var9, var2, var3);
            }
        }

        return true;
    }
}
