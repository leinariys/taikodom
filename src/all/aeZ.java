package all;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class aeZ implements Comparable {
    private final Map buY = new HashMap();
    private final Md fsB;
    private int fsC;

    public aeZ(Md var1) {
        this.fsB = var1;
    }

    public int getCallCount() {
        return this.fsC;
    }

    public int a(aeZ var1) {
        int var2 = this.fsC - var1.fsC;
        return var2;
    }

    public void bUW() {
        ++this.fsC;
    }

    public Map bUX() {
        return this.buY;
    }

    public Md bUY() {
        return this.fsB;
    }

    public String toString() {
        return this.fsB.toString();
    }

    public void a(PrintWriter var1, int var2, String var3) {
        this.a(var1, var2, var3, var3);
    }

    public void a(PrintWriter var1, int var2, String var3, String var4) {
        if (this.fsB != null) {
            var1.print(var3);
            var1.print('[');
            var1.print(this.fsC);
            var1.print(", ");
            var1.print(this.fsC * 100 / var2);
            var1.print("%] ");
            var1.println(this.fsB.toString());
        }

        ArrayList var5 = new ArrayList();
        Iterator var7 = this.buY.values().iterator();

        while (var7.hasNext()) {
            aeZ var6 = (aeZ) var7.next();
            if (var6.fsC * 100 / var2 > 1) {
                var5.add(var6);
            }
        }

        int var10 = 0;
        int var11 = var5.size();
        Iterator var9 = var5.iterator();

        while (var9.hasNext()) {
            aeZ var8 = (aeZ) var9.next();
            ++var10;
            var8.a(var1, var2, var4 + (var10 != var11 ? "+" : "+"), var10 != var11 ? var4 + "|" : var4 + " ");
        }

    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }
}
