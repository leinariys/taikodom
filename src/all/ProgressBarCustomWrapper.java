package all;

import javax.swing.*;
import javax.swing.border.Border;

public class ProgressBarCustomWrapper extends JProgressBar implements IComponentCustomWrapper {

    private IBoundedRangeModelCustomWrapper nU;
    private transient float nV;
    private boolean nW;
    private int cellWidth;
    private int nX;
    private int nY;
    private boolean nZ;

    public ProgressBarCustomWrapper() {
        this.setBorder((Border) null);
        this.setStringPainted(true);
        this.nZ = false;
        this.cellWidth = 10;
        this.nX = 10;
        this.nY = 5;
        this.nW = true;
    }

    public void E(float var1) {
        super.setMaximum(Math.round(var1));
    }

    public void F(float var1) {
        super.setMinimum(Math.round(var1));
    }

    /**
     * Имя xml тега, список в class BaseUItegXML
     *
     * @return Пример progress
     */
    public String getElementName() {
        return "progress";
    }

    public String getString() {
        if (this.nU != null) {
            try {
                return this.nU.getText();
            } catch (RuntimeException var2) {
                var2.printStackTrace();
                return super.getString();
            }
        } else {
            return super.getString();
        }
    }

    public int getValue() {
        if (this.nU != null) {
            try {
                float var1 = this.nU.getValue();
                if (this.nV != var1) {
                    this.repaint(100L);
                    this.nV = var1;
                }

                return (int) this.nV;
            } catch (RuntimeException var2) {
                var2.printStackTrace();
                return super.getValue();
            }
        } else {
            return super.getValue();
        }
    }

    public void setValue(float var1) {
        super.setValue(Math.round(var1));
    }

    public void setValue(int var1) {
        this.setValue((float) var1);
    }

    public int getMinimum() {
        if (this.nU != null) {
            try {
                return (int) this.nU.getMinimum();
            } catch (RuntimeException var2) {
                var2.printStackTrace();
                return super.getMinimum();
            }
        } else {
            return super.getMinimum();
        }
    }

    public void setMinimum(int var1) {
        this.F((float) var1);
    }

    public int getMaximum() {
        if (this.nU != null) {
            try {
                return (int) this.nU.getMaximum();
            } catch (RuntimeException var2) {
                var2.printStackTrace();
                return super.getMaximum();
            }
        } else {
            return super.getMaximum();
        }
    }

    public void setMaximum(int var1) {
        this.E((float) var1);
    }

    public boolean gu() {
        return this.nZ;
    }

    public void w(boolean var1) {
        this.nZ = var1;
    }

    public void a(IBoundedRangeModelCustomWrapper var1) {
        this.nU = var1;
    }

    public boolean gv() {
        return this.nW;
    }

    public void x(boolean var1) {
        this.nW = var1;
    }

    public int gw() {
        return this.cellWidth;
    }

    public void U(int var1) {
        this.cellWidth = var1;
    }

    public int gx() {
        return this.nX;
    }

    public void V(int var1) {
        this.nX = var1;
    }

    public int gy() {
        return this.nY;
    }

    public void W(int var1) {
        this.nY = var1;
    }

    public interface IBoundedRangeModelCustomWrapper {
        float getValue();

        float getMinimum();

        float getMaximum();

        String getText();
    }
}
