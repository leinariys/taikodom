package all;

import javax.swing.*;
import javax.swing.text.Document;
import javax.swing.text.html.StyleSheet;
import java.net.URL;

public class HtmlCustomWrapper extends JEditorPane implements ITextComponentCustomWrapper {

    private av hJn;

    public HtmlCustomWrapper() {
        this.hJn = new av();
        this.setEditorKit(this.hJn);
        this.setContentType("text/html");
        this.e(HtmlCustomWrapper.class.getResource("default.css"));

        this.setEditable(false);
    }

    public void e(URL var1) {
        StyleSheet var2 = this.hJn.getStyleSheet();

        try {
            var2.importStyleSheet(var1);
        } catch (Exception var4) {
            System.out.println("Exception-" + var4);
            if (!var1.getFile().equals("default.css")) {
                this.e(HtmlCustomWrapper.class.getResource("default.css"));
            } else {
                var4.printStackTrace();
            }
        }

    }

    public StyleSheet getStyleSheet() {
        return this.hJn.getStyleSheet();
    }

    public void setStyleSheet(StyleSheet var1) {
        if (var1 == null) {
            this.hJn.setStyleSheet((StyleSheet) null);
        } else {
            Document var2 = this.getDocument();
            this.hJn = new av();
            this.hJn.setStyleSheet(var1);
            this.setEditorKit(this.hJn);
            this.setDocument(var2);
        }
    }

    public void setText(String var1) {
        if (!var1.startsWith("<html>")) {
            var1 = "<html><div>" + var1 + "</div></html>";
        }

        super.setText(var1);
    }

    public String getElementName() {
        return "html";
    }

    public void h(int var1) {
    }
}
