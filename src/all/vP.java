package all;

import com.sun.jna.Memory;
import com.sun.jna.Native;
import com.sun.jna.NativeLong;
import com.sun.jna.WString;
import com.sun.jna.ptr.NativeLongByReference;
import com.sun.jna.win32.StdCallLibrary;

/**
 * Используется в тестировании для выполнения http запроса
 */
public class vP {
    private static final String TYPE_PROTOCOL = "http://";

    public static yz l(String var0, String var1, String var2) {
        String var3 = TYPE_PROTOCOL;
        if (var3.startsWith(".")) {
            var3 = var3 + var0.substring(1);
        } else {
            var3 = var3 + var0;
        }

        var3 = var3 + var1;//http://www.googleadservices.com/pagead/conversion/1052789991/
        WString var4 = new WString(var3);//http://www.googleadservices.com/pagead/conversion/1052789991/
        WString var5 = new WString(var2);//Conversion
        Memory var6 = new Memory(1000L);//allocated@0x11c4328 (1000 bytes)
        NativeLongByReference var7 = new NativeLongByReference(new NativeLong(1000L));
        boolean var8 = WinInet.INSTANCE.InternetSetOptionW(var4, var5, var6, var7);
        if (!var8) {
            return null;
        } else {
            String var9 = var6.getString(0L, true);//ÀĜÀĜ
            var9 = var9.substring((var2 + "=").length());
            System.out.println("*********** " + var9);
            long var10 = 0L;
            return new yz(var0, var1, var2, var9, var10);
        }
    }

    public interface WinInet extends StdCallLibrary {
        WinInet INSTANCE = (WinInet) Native.loadLibrary("wininet", WinInet.class);

        /**
         * Устанавливает интернет-вариант.
         * Метод из библотеки wininet
         *
         * @param var1 Ручка для установки информации.
         * @param var2 Настройка Интернета. Это может быть один из значений флажка Option.
         * @param var3 Указатель на буфер, содержащий параметр.
         * @param var4 Размер буфера lpBuffer. Если lpBuffer содержит строку, размер находится в TCHAR. Если lpBuffer содержит ничего, кроме строки, размер находится в байтах.
         * @return Возвращает TRUE в случае успеха или FALSE в противном случае. Чтобы получить конкретное сообщение об ошибке, вызовите GetLastError.
         */
        boolean InternetSetOptionW(WString var1, WString var2, Memory var3, NativeLongByReference var4);
    }
}
