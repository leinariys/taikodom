package all;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * class MessageDialogAddon
 */
public class to_q extends WA implements Externalizable {

    private boolean bme;
    private a bmf;

    public to_q() {
        this(a.eLf);
    }

    public to_q(a var1) {
        this.bme = false;
        this.bmf = var1;
    }

    public void adC() {
        this.bme = true;
    }

    public boolean adD() {
        return this.bme;
    }

    public a adE() {
        return this.bmf;
    }

    public void readExternal(ObjectInput var1) {
        throw new UnsupportedOperationException("Do not use this class serverside");
    }

    public void writeExternal(ObjectOutput var1) {
        throw new UnsupportedOperationException("Do not use this class serverside");
    }

    public static enum a {
        eLf,
        eLg;
    }
}
