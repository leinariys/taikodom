package all;

import java.awt.*;

/**
 *
 */
public interface IComponentCustomWrapper {
    /**
     * Имя xml тега, список в class BaseUItegXML
     *
     * @return Пример progress
     */
    String getElementName();

    int getWidth();

    int getHeight();

    Dimension getSize();

    void setSize(Dimension var1);

    Dimension getPreferredSize();

    void setSize(int var1, int var2);

    int getX();

    int getY();

    Point getLocation();

    void setLocation(Point var1);

    void setLocation(int var1, int var2);

    Rectangle getBounds();

    void setBounds(Rectangle var1);

    void setBounds(int var1, int var2, int var3, int var4);

    boolean isVisible();

    void setVisible(boolean var1);

    boolean isEnabled();

    void setEnabled(boolean var1);

    Container getParent();

    void validate();

    void invalidate();
}
