package all;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.concurrent.atomic.AtomicInteger;

public class ek_q extends aTn_q {
    private static AtomicInteger nextId = new AtomicInteger();
    private int id;

    public ek_q() {
    }

    public ek_q(Gr var1) {
        super(var1);
        this.id = nextId.incrementAndGet();
    }

    public int getId() {
        return this.id;
    }

    public void readExternal(ObjectInput var1) throws IOException, ClassNotFoundException {
        this.id = var1.readInt();
        super.readExternal(var1);
    }

    public void writeExternal(ObjectOutput var1) throws IOException {
        var1.writeInt(this.id);
        super.writeExternal(var1);
    }
}
