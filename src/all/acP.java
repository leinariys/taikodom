package all;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class acP extends kU implements Externalizable {
    private static final long serialVersionUID = 1L;
    private List list;

    public acP() {
    }

    public acP(afk var1) {
        this.list = new ArrayList(1);
        this.list.add(var1);
    }

    public acP(List var1) {
        this.list = var1;
    }

    public void readExternal(ObjectInput var1) throws IOException, ClassNotFoundException {
        super.readExternal(var1);
        int var2 = var1.readInt();
        this.list = new ArrayList(var2);

        for (int var3 = 0; var3 < var2; ++var3) {
            Xf_q var4 = (Xf_q) GZ.dah.b(var1);
            // Jd var5 = (Jd)var4.W();
            // var5.all((ObjectInput)var1, (int)0);
            // this.list.add(var5);
        }

    }

    public void writeExternal(ObjectOutput var1) throws IOException {
        super.writeExternal(var1);
        var1.writeInt(this.list.size());
        Iterator var3 = this.list.iterator();

        while (var3.hasNext()) {
            afk var2 = (afk) var3.next();

            try {
                GZ.dah.a((ObjectOutput) var1, (Object) var2.yn());
                //((Jd)var2).setGreen((ObjectOutput)var1, 0);
            } catch (Exception var5) {
                throw new adk_q("Error serializing " + var2.yn().bFf().bFY(), var5);
            }
        }

    }

    public akO a(b var1, Jz var2) {
        Iterator var4 = this.list.iterator();

        while (var4.hasNext()) {
            afk var3 = (afk) var4.next();
            ((se_q) var3.yn().bFf()).c(var3);
        }

        return null;
    }
}
