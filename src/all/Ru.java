package all;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public class Ru {
    private Map hdm = new HashMap();

    public a k(ain var1) throws IOException {
        a var2 = (a) this.hdm.get(var1);
        if (var2 != null) {
            return var2;
        } else if (!var1.exists()) {
            return null;
        } else {
            var2 = new a();
            InputStream var4 = var1.getInputStream();

            XmlNode var5;
            try {
                var5 = (new axx()).a(var4, "UTF-8");
            } finally {
                try {
                    var4.close();
                } catch (Exception var16) {
                    ;
                }

            }

            XmlNode var6 = var5.mD("includes");
            XmlNode var7;
            if (var6 != null) {
                Iterator var8 = var6.getChildrenTag().iterator();

                while (var8.hasNext()) {
                    var7 = (XmlNode) var8.next();
                    String var9 = var7.getAttribute("file");
                    var2.gu(var9);
                }
            }

            var7 = var5.mD("trails");
            if (var7 != null) {
                Iterator var19 = var7.getChildrenTag().iterator();

                while (var19.hasNext()) {
                    XmlNode var18 = (XmlNode) var19.next();
                    String var10 = var18.getAttribute("attach_point");
                    String var11 = var18.getAttribute("asset");
                    b var12 = new b(var10, var11);
                    var2.a(var12);
                }
            }

            this.hdm.put(var1, var2);
            return var2;
        }
    }

    public class b {
        private String edT;
        private String edU;

        b(String var2, String var3) {
            this.edT = var2;
            this.edU = var3;
        }

        public String btt() {
            return this.edT;
        }

        public String btu() {
            return this.edU;
        }
    }

    public class a {
        private List eaf = new ArrayList();
        private List eag = new ArrayList();

        public void a(b var1) {
            this.eag.add(var1);
        }

        public void gu(String var1) {
            this.eaf.add(var1);
        }

        public List brr() {
            return this.eaf;
        }

        public List brs() {
            return this.eag;
        }
    }
}
