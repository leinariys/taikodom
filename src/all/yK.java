package all;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;

public abstract class yK {

    public static byte BOX = 0;
    public static byte SPHERE = 1;
    public static byte CYLINDER = 2;
    public static byte CAPSULE = 3;
    public static byte MESH = 4;
    public static byte TRIANGLE = 5;
    public static byte LINE = 6;
    public static byte COMPOUND = 7;
    public static byte CONVEX_HULL = 8;
    protected BoundingBox boundingBox = null;
    protected float volume;
    private boolean invisible;

    public abstract void getAabb(xf_g var1, Vec3f var2, Vec3f var3);

    public abstract Vec3f getLocalScaling();

    public abstract void setLocalScaling(Vec3f var1);

    public abstract void calculateLocalInertia(float var1, Vec3f var2);

    public abstract byte getShapeType();

    public BoundingBox getBoundingBox() {
        if (this.boundingBox == null) {
            this.boundingBox = new BoundingBox();
            this.growBoundingBox(new ajK(), this.boundingBox);
        }

        return this.boundingBox;
    }

    public float getVolume() {
        return this.volume;
    }

    public void setVolume(float var1) {
        this.volume = var1;
    }

    public void setTransparent(boolean var1) {
    }

    public void setDisableOnColision(boolean var1) {
    }

    protected abstract void growBoundingBox(ajK var1, BoundingBox var2);

    public float getOuterSphereRadius() {
        float var1 = this.getBoundingBox().dit();
        if (Float.isInfinite(var1)) {
            var1 = 10.0F;
        }

        return var1;
    }

    public void getFarthest(Vec3f var1, Vec3f var2) {
    }

    public RY toMesh() {
        throw new XU();
    }

    public void getCenter(Vec3f var1) {
    }

    public boolean isInvisible() {
        return this.invisible;
    }

    protected void setInvisible(boolean var1) {
        this.invisible = var1;
    }
}
