package all;

import org.w3c.dom.css.CSSStyleDeclaration;

import javax.swing.*;
import java.awt.*;
import java.util.Map;

/**
 * Система событий тега
 * class pv
 */
public interface aeK extends RC {
    String pressed = "pressed";
    String hover = "hover";
    String over = "over";
    String selected = "selected";
    String disabled = "disabled";
    String readOnly = "read-only";
    Object cssHolder = "css.holder";

    String getAttribute(String var1);

    void setAttribute(String var1, String var2);

    void setAttributes(Map var1);

    boolean hasAttribute(String var1);

    boolean ba(String var1);

    CSSStyleDeclaration Vh();

    avI getCssNode();

    void setCssNode(avI var1);

    PropertiesUiFromCss Vp();

    PropertiesUiFromCss Vr();

    Dimension c(JComponent var1, Dimension var2);

    Dimension b(JComponent var1, Dimension var2);

    Dimension a(JComponent var1, Dimension var2);

    /**
     * Установить стиль
     *
     * @param var1 Пример vertical-align:fill; text-align:fill
     */
    void setStyle(String var1);

    void clear();

    float getAlpha();

    void setAlpha(float var1);

    Color getColor();

    void setColor(Color var1);

    aeK Vo();

    Component getCurrentComponent();

    void Vq();

    void q(int var1, int var2);

    boolean Vs();

    void aO(boolean var1);

    boolean Vt();

    /**
     * Сигналить что указатель мыши над объектом
     *
     * @param var1 true-сигналить
     */
    void aP(boolean var1);

    void a(aWs var1);
}
