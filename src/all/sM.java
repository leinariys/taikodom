package all;

import com.hoplon.geometry.Vec3f;

public abstract class sM implements aTC_q {
    public final Vec3f biw = new Vec3f();
    public final Vec3f bix = new Vec3f();
    protected final Kt stack = Kt.bcE();
    public float r;

    public sM(Vec3f var1, Vec3f var2) {
        this.biw.set(var1);
        this.bix.set(var2);
        this.r = 1.0F;
    }

    public void a(Vec3f[] var1, int var2, int var3) {
        this.stack.bcH().push();

        try {
            Vec3f var4 = var1[0];
            Vec3f var5 = var1[1];
            Vec3f var6 = var1[2];
            Vec3f var7 = (Vec3f) this.stack.bcH().get();
            var7.sub(var5, var4);
            Vec3f var8 = (Vec3f) this.stack.bcH().get();
            var8.sub(var6, var4);
            Vec3f var9 = (Vec3f) this.stack.bcH().get();
            var9.cross(var7, var8);
            float var10 = var4.dot(var9);
            float var11 = var9.dot(this.biw);
            var11 -= var10;
            float var12 = var9.dot(this.bix);
            var12 -= var10;
            if (var11 * var12 < 0.0F) {
                float var13 = var11 - var12;
                float var14 = var11 / var13;
                if (var14 >= this.r) {
                    return;
                }

                float var15 = var9.lengthSquared();
                var15 *= -1.0E-4F;
                Vec3f var16 = new Vec3f();
                JL.a(var16, this.biw, this.bix, var14);
                Vec3f var17 = (Vec3f) this.stack.bcH().get();
                var17.sub(var4, var16);
                Vec3f var18 = (Vec3f) this.stack.bcH().get();
                var18.sub(var5, var16);
                Vec3f var19 = (Vec3f) this.stack.bcH().get();
                var19.cross(var17, var18);
                if (var19.dot(var9) >= var15) {
                    Vec3f var20 = (Vec3f) this.stack.bcH().get();
                    var20.sub(var6, var16);
                    Vec3f var21 = (Vec3f) this.stack.bcH().get();
                    var21.cross(var18, var20);
                    if (var21.dot(var9) >= var15) {
                        Vec3f var22 = (Vec3f) this.stack.bcH().get();
                        var22.cross(var20, var17);
                        if (var22.dot(var9) >= var15) {
                            if (var11 > 0.0F) {
                                this.r = this.a(var9, var14, var2, var3);
                            } else {
                                Vec3f var23 = (Vec3f) this.stack.bcH().get();
                                var23.negate(var9);
                                this.r = this.a(var23, var14, var2, var3);
                            }

                            return;
                        }

                        return;
                    }
                }

                return;
            }
        } finally {
            this.stack.bcH().pop();
        }

    }

    public abstract float a(Vec3f var1, float var2, int var3, int var4);
}
