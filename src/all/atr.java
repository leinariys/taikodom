package all;

public class atr {
    private long swigCPtr;

    protected atr(long var1, boolean var3) {
        this.swigCPtr = var1;
    }

    protected atr() {
        this.swigCPtr = 0L;
    }

    protected static long e(atr var0) {
        return var0 == null ? 0L : var0.swigCPtr;
    }
}
