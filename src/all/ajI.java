package all;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class ajI extends Kd {
    public static ajI fRF = new ajI();

    public Object b(ObjectInput var1) throws IOException, ClassNotFoundException {
        if (!(var1 instanceof aHN)) {
            return super.b(var1);
        } else {
            short var2 = var1.readShort();
            if (var2 == -1) {
                return null;
            } else {
                aHN var3 = (aHN) var1;
                int var9 = var2 & '\uffff';
                Class var4 = var3.PM().bxy().tS(var9);
                if (var4 == null) {
                    throw new RuntimeException("Could not find class for magic:" + var9);
                } else {
                    try {
                        Externalizable var5 = (Externalizable) var4.newInstance();
                        var5.readExternal(var1);
                        return var5;
                    } catch (InstantiationException var6) {
                        throw new adk_q("Error trying to_q read an " + var4, var6);
                    } catch (IllegalAccessException var7) {
                        throw new adk_q("Error trying to_q read an " + var4, var7);
                    } catch (Exception var8) {
                        throw new adk_q("Error trying to_q read an " + var4, var8);
                    }
                }
            }
        }
    }

    public void a(ObjectOutput var1, Object var2) throws IOException {
        if (var1 instanceof aso_q) {
            if (var2 == null) {
                var1.writeShort(-1);
            } else {
                aso_q var3 = (aso_q) var1;
                int var4 = var3.PM().bxy().getMagicNumber(var2.getClass());
                if (var4 == -1) {
                    throw new IllegalArgumentException(var2.getClass() + " must be precached!");
                }

                var1.writeShort(var4);
                ((Externalizable) var2).writeExternal(var1);
            }
        } else {
            super.a(var1, var2);
        }

    }
}
