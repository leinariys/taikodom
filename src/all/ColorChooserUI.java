package all;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicColorChooserUI;
import java.awt.*;

/**
 * ColorChooserUI
 */
public class ColorChooserUI extends BasicColorChooserUI implements aIh {
    private ComponentManager Rp;

    public ColorChooserUI(JColorChooser var1) {
        this.Rp = new ComponentManager("colorchooser", var1);
        var1.setOpaque(false);
    }

    public static ComponentUI createUI(JComponent var0) {
        return new ColorChooserUI((JColorChooser) var0);
    }

    public void update(Graphics var1, JComponent var2) {
        this.paint(var1, var2);
    }

    public void paint(Graphics var1, JComponent var2) {
        super.paint(var1, var2);
        RM.a((ComponentUI) this, (Graphics) var1, (JComponent) var2);
    }

    public Dimension getPreferredSize(JComponent var1) {
        return this.wy().c(var1, super.getPreferredSize(var1));
    }

    public Dimension getMinimumSize(JComponent var1) {
        return this.wy().b(var1, super.getMinimumSize(var1));
    }

    public Dimension getMaximumSize(JComponent var1) {
        return this.wy().a(var1, super.getMaximumSize(var1));
    }

    public ComponentManager wy() {
        return this.Rp;
    }

    /**
     * TODO похлже это public pv wy()
     *
     * @return Графический элемент
     */
    @Override
    public aeK wz() {
        return this.Rp;
    }
}
