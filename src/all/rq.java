package all;

import java.util.HashMap;
import java.util.Map;

/**
 * MouseButtonEvent
 * class CommandTranslatorAddon
 */
public class rq {
    public static Map Ow = new HashMap();
    public static Map Ox = new HashMap();
    public static int UNDEFINED = -1;
    public static int bax = 0;
    public static int bay = 1;
    public static int baz = 2;
    public static int baA = 3;
    public static int baB = 4;
    public static int baC = 16;

    static {
        Ow.put(Integer.valueOf(UNDEFINED), "");
        Ow.put(Integer.valueOf(bax), "MOUSE_LEFT");
        Ow.put(Integer.valueOf(bay), "MOUSE_MIDDLE");
        Ow.put(Integer.valueOf(baz), "MOUSE_RIGHT");
        Ow.put(Integer.valueOf(baA), "MOUSE_WHEELUP");
        Ow.put(Integer.valueOf(baB), "MOUSE_WHEELDOWN");
        Ox.put("", Integer.valueOf(UNDEFINED));
        Ox.put("MOUSE_LEFT", Integer.valueOf(bax));
        Ox.put("MOUSE_MIDDLE", Integer.valueOf(bay));
        Ox.put("MOUSE_RIGHT", Integer.valueOf(baz));
        Ox.put("MOUSE_WHEELUP", Integer.valueOf(baA));
        Ox.put("MOUSE_WHEELDOWN", Integer.valueOf(baB));
    }

    public static int ao(String var0) {
        Integer var1 = (Integer) Ox.get(var0);
        return var1 != null ? var1.intValue() : 0;
    }

    public static String bf(int var0) {
        String var1 = (String) Ow.get(var0);
        return var1 != null ? var1 : "UNDEFINED";
    }
}
