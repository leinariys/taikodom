package all;

public class avo {
    private long swigCPtr;

    protected avo(long var1, boolean var3) {
        this.swigCPtr = var1;
    }

    protected avo() {
        this.swigCPtr = 0L;
    }

    protected static long d(avo var0) {
        return var0 == null ? 0L : var0.swigCPtr;
    }
}
