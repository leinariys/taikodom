package all;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicEditorPaneUI;
import javax.swing.text.JTextComponent;
import java.awt.*;

/**
 * EditorPaneUI
 */
public class EditorPaneUI extends BasicEditorPaneUI implements aIh {
    private ComponentManager Rp;

    public EditorPaneUI(JEditorPane var1) {
        this.Rp = (ComponentManager) ComponentManager.getCssHolder(var1);
        if (this.Rp == null) {
            this.Rp = new aix("editor", var1);
        }

        var1.setOpaque(false);
    }

    public static ComponentUI createUI(JComponent var0) {
        return new EditorPaneUI((JEditorPane) var0);
    }

    public void update(Graphics var1, JComponent var2) {
        RM.a((aIh) this, (Graphics) var1, (JTextComponent) ((JTextComponent) var2));
        super.paint(var1, var2);
    }

    protected void paintBackground(Graphics var1) {
    }

    public Dimension getMinimumSize(JComponent var1) {
        RM.a((aIh) this, (JTextComponent) ((JTextComponent) var1));
        return this.wy().b(var1, super.getMinimumSize(var1));
    }

    public Dimension getMaximumSize(JComponent var1) {
        RM.a((aIh) this, (JTextComponent) ((JTextComponent) var1));
        return this.wy().a(var1, super.getMaximumSize(var1));
    }

    public Dimension getPreferredSize(JComponent var1) {
        RM.a((aIh) this, (JTextComponent) ((JTextComponent) var1));
        return this.Rp.c(var1, super.getPreferredSize(var1));
    }

    public ComponentManager wy() {
        return this.Rp;
    }

    /**
     * TODO похлже это public pv wy()
     *
     * @return Графический элемент
     */
    @Override
    public aeK wz() {
        return this.Rp;
    }
}
