package all;

import java.io.Serializable;

public class SiblingSelector implements ISiblingSelector, Serializable {
    private short gFB;
    private ISelector gFC;
    private ISimpleSelector gFD;

    public SiblingSelector(short var1, ISelector var2, ISimpleSelector var3) {
        this.gFB = var1;
        this.gFC = var2;
        this.gFD = var3;
    }

    public short getNodeType() {
        return this.gFB;
    }

    public short getSelectorType() {
        return 12;
    }

    public ISelector getSelector() {
        return this.gFC;
    }

    public ISimpleSelector getSiblingSelector() {
        return this.gFD;
    }

    public String toString() {
        return this.gFC.toString() + " + " + this.gFD.toString();
    }
}
