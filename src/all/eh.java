package all;

import com.hoplon.geometry.Vec3f;

import java.nio.ByteBuffer;

public class eh extends Bm {

    private Pw bZE = null;
    private boolean bZF;
    private boolean bZG;
    private QW bZH = Ks.D(a.class);

    public eh() {
        super((adJ) null);
        this.bZG = false;
    }

    public eh(adJ var1, boolean var2) {
        super(var1);
        this.bZF = var2;
        this.bZG = false;
        Vec3f var3 = new Vec3f();
        Vec3f var4 = new Vec3f();
        var1.x(var3, var4);
        this.bZE = new Pw();
        this.bZE.a(var1, var2, var3, var4);
        this.bZG = true;
        this.aPc();
        this.setVolume(this.getBoundingBox().dio() * this.getBoundingBox().dip() * this.getBoundingBox().diq());
    }

    public void a(sM var1, Vec3f var2, Vec3f var3) {
        a var4 = (a) this.bZH.get();
        var4.a(var1, this.BA);
        this.bZE.c(var4, var2, var3);
        this.bZH.release(var4);
    }

    public void a(aTrQ var1, Vec3f var2, Vec3f var3, Vec3f var4, Vec3f var5) {
        a var6 = (a) this.bZH.get();
        var6.a(var1, this.BA);
        this.bZE.a(var6, var2, var3, var4, var5);
        this.bZH.release(var6);
    }

    public void a(aTC_q var1, Vec3f var2, Vec3f var3) {
        this.stack = this.stack.bcD();
        if (this.bZH == null) {
            this.bZH = Ks.D(a.class);
        }

        a var4 = (a) this.bZH.get();
        var4.a(var1, this.BA);
        this.bZE.a((aLG) var4, var2, var3);
        this.bZH.release(var4);
    }

    public void arW() {
        this.bZE.a(this.BA);
        this.aPc();
    }

    public void h(Vec3f var1, Vec3f var2) {
        this.bZE.a(this.BA, var1, var2);
        JL.o(this.cUp, var1);
        JL.p(this.cUq, var2);
    }

    public void setLocalScaling(Vec3f var1) {
        this.stack.bcH().push();

        try {
            Vec3f var2 = (Vec3f) this.stack.bcH().get();
            var2.sub(this.getLocalScaling(), var1);
            if (var2.lengthSquared() > 1.1920929E-7F) {
                super.setLocalScaling(var1);
                this.bZE = new Pw();
                this.bZE.a(this.BA, this.bZF, this.cUp, this.cUq);
            }
        } finally {
            this.stack.bcH().pop();
        }

    }

    public Pw arX() {
        return this.bZE;
    }

    public void a(Pw var1) {
        assert this.bZE == null;

        assert !this.bZG;

        this.bZE = var1;
        this.bZG = false;
    }

    public boolean arY() {
        return this.bZF;
    }

    public byte getShapeType() {
        return 4;
    }

    protected static class a implements aLG {
        public adJ BA;
        public aTC_q BB;
        private Vec3f[] BC = new Vec3f[]{new Vec3f(), new Vec3f(), new Vec3f()};
        private aUv BD = new aUv();

        public void a(aTC_q var1, adJ var2) {
            this.BA = var2;
            this.BB = var1;
        }

        public void f(int var1, int var2) {
            this.BA.b(this.BD, var1);
            ByteBuffer var3 = this.BD.iXl;
            int var4 = var2 * this.BD.iXm;

            assert this.BD.iXo == aop.ghD || this.BD.iXo == aop.ghE;

            Vec3f var5 = this.BA.getScaling();

            for (int var6 = 2; var6 >= 0; --var6) {
                int var7;
                if (this.BD.iXo == aop.ghE) {
                    var7 = var3.getShort(var4 + var6 * 2) & '\uffff';
                } else {
                    var7 = var3.getInt(var4 + var6 * 4);
                }

                ByteBuffer var8 = this.BD.iXi;
                int var9 = var7 * this.BD.stride;
                this.BC[var6].set(var8.getFloat(var9 + 0) * var5.x, var8.getFloat(var9 + 4) * var5.y, var8.getFloat(var9 + 8) * var5.z);
            }

            this.BB.a(this.BC, var1, var2);
            this.BA.qh(var1);
            this.BD.dAi();
        }
    }
}
