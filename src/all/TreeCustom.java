package all;

import gnu.trove.TObjectIntHashMap;

import javax.swing.*;
import javax.swing.tree.DefaultTreeSelectionModel;
import java.awt.*;

public class TreeCustom extends BaseItem {
    private static TObjectIntHashMap selectionModel = new TObjectIntHashMap();

    static {
        selectionModel.put("SINGLE_TREE_SELECTION", 1);
        selectionModel.put("CONTIGUOUS_TREE_SELECTION", 2);
        selectionModel.put("DISCONTIGUOUS_TREE_SELECTION", 4);
    }

    public JTree D(MapKeyValue var1, Container var2, XmlNode var3) {
        JTree var4 = (JTree) super.CreateUI(var1, var2, var3);
        this.CreatChildren(var1, (JComponent) var4, var3);
        return var4;
    }

    protected JTree E(MapKeyValue var1, Container var2, XmlNode var3) {
        acz var4 = new acz();
        return var4;
    }

    protected void a(MapKeyValue var1, JTree var2, XmlNode var3) {
        super.applyAttribute(var1, (Component) var2, (XmlNode) var3);
        String var4 = var3.getAttribute("toggleClickCount");
        if (var4 != null) {
            var2.setToggleClickCount(Integer.parseInt(var4));
        }

        var4 = var3.getAttribute("rowHeight");
        if (var4 != null) {
            var2.setRowHeight(Integer.parseInt(var4));
        }

        var2.setRootVisible(!"false".equals(var3.getAttribute("rootVisible")));
        var4 = var3.getAttribute("selectionModel");
        if (var4 != null) {
            DefaultTreeSelectionModel var5 = new DefaultTreeSelectionModel();
            var5.setSelectionMode(selectionModel.get(var4.replace('-', '_').toUpperCase()));
            var2.setSelectionModel(var5);
        }

    }

    /**
     * /** Создание базового Component интерфейса
     * Вызывается из class BaseItem
     *
     * @param var1
     * @param var2
     * @param var3
     * @return
     */
    //  @Override
    //  protected Component CreateJComponent
    @Override
    protected JComponent CreatComponentCustom(MapKeyValue var1, Container var2, XmlNode var3) {
        return null;
    }
}
