package all;

import java.io.IOException;
import java.nio.channels.SelectableChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

public abstract class awd extends XF_w {
    ServerSocketChannel gLA;

    public awd(ServerSocketChannel var1) {
        this.gLA = var1;
    }

    public void b(SelectionKey var1) throws IOException {
        if (var1.isAcceptable()) {
            this.KC();
        }

    }

    protected void KC() throws IOException {
        SocketChannel var1 = this.gLA.accept();
        var1.configureBlocking(false);
        this.yQ.a((XF_w) this.a(var1));
    }

    protected abstract yV a(SocketChannel var1);

    SelectableChannel getChannel() {
        return this.gLA;
    }
}
