package all;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.awt.event.MouseEvent;

public class TableCustomWrapper extends JTable implements IComponentCustomWrapper {
    private static final long serialVersionUID = 1L;
    private boolean cuC = false;
    private a cuD;

    public TableCustomWrapper(MapKeyValue var1, XmlNode var2) {
        this.setBackground((Color) null);
        this.cuD = new a();
        this.setModel(this.cuD);
        this.setDefaultRenderer(PictureCustomWrapper.class, new b((b) null));
        this.setDefaultRenderer(JLabel.class, new c((c) null));
    }

    public void d(Object... var1) {
        this.cuD.setColumnIdentifiers(var1);
    }

    public void addRow(Object... var1) {
        this.cuD.addRow(var1);
        this.getParent().validate();
    }

    public void clear() {
        this.cuD.VB();
    }

    protected JTableHeader createDefaultTableHeader() {
        return new TableHeaderCustomWrapper(this.columnModel);
    }

    public String getElementName() {
        return "table";
    }

    public void aBh() {
        this.cuC = false;
    }

    public void aBi() {
        this.cuC = true;
    }

    public void changeSelection(int var1, int var2, boolean var3, boolean var4) {
        if (!this.cuC) {
            super.changeSelection(var1, var2, var3, var4);
        }
    }

    public String getToolTipText(MouseEvent var1) {
        if (wz.d(this) != null) {
            int var2 = this.rowAtPoint(var1.getPoint());
            if (var2 != -1) {
                Object var3 = this.getValueAt(var2, 0);
                if (var3 != null) {
                    return var3.toString();
                }
            }
        }

        return super.getToolTipText(var1);
    }

    private final class b implements TableCellRenderer {
        private b() {
        }

        // $FF: synthetic method
        b(b var2) {
            this();
        }

        public Component getTableCellRendererComponent(JTable var1, Object var2, boolean var3, boolean var4, int var5, int var6) {
            return var2 instanceof PictureCustomWrapper ? (PictureCustomWrapper) var2 : null;
        }
    }

    private final class c implements TableCellRenderer {
        private c() {
        }

        // $FF: synthetic method
        c(c var2) {
            this();
        }

        public Component getTableCellRendererComponent(JTable var1, Object var2, boolean var3, boolean var4, int var5, int var6) {
            return var2 instanceof JLabel ? (JLabel) var2 : null;
        }
    }

    private final class a extends DefaultTableModel {
        private static final long serialVersionUID = 1L;

        public Class getColumnClass(int var1) {
            return this.getValueAt(0, var1).getClass();
        }

        public boolean isCellEditable(int var1, int var2) {
            return this.getColumnClass(var2).equals(Boolean.class);
        }

        public void VB() {
            int var1 = this.getRowCount();
            if (var1 > 0) {
                TableCustomWrapper.this.removeRowSelectionInterval(0, var1 - 1);
            }

        }
    }
}
