package all;

import org.apache.commons.logging.Log;
import taikodom.addon.login.LoginAddon;
import taikodom.addon.messagedialog.MessageDialogAddon;
import taikodom.infra.script.I18NString;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * запросы аддона к другим компонентам LoginAddon
 * class LoginAddon
 */
@awK("taikodom.addon.login")
public class akH {
    private static final String urlBaseDefault = "http://www.corpscorp.online";
    private static String urlBaseCurrent = "";
    private final Log logger = LogPrinter.setClass(LoginAddon.class);
    private final String urlSignup;
    private final String urlAuthForgotten;
    private final String urlSupport;
    public String urlCreateUser;
    private String urlEmailActivation;
    private String urlEmailFromUser;
    private String location;
    private GK kj;
    private String fUl;

    public akH(vW var1) {
        this.kj = (GK) var1;
        this.location = I18NString.getCurrentLocation();
        if (!this.location.equalsIgnoreCase("pt") && !this.location.equalsIgnoreCase("en")) {
            this.location = "en";
        }

        String urlBase = this.getUrlSite();
        this.urlSignup = urlBase + "/signup";
        this.urlAuthForgotten = urlBase + "/auth/forgotten";
        this.urlSupport = urlBase + "/support/";
        this.urlEmailActivation = urlBase + "/users/game_resend_activation_email";
        this.urlEmailFromUser = urlBase + "/users/game_get_email_from_user";
        this.urlCreateUser = urlBase + "/users/game_create_user";
    }

    public void jf(String var1) {
        ConfigManager var2 = this.kj.getEngineGame().getConfigManager();
        ConfigManagerSection var3 = var2.getSection(setKeyValue.user_username.getValue());
        if (var3 == null) {
            var3 = new ConfigManagerSection();
            var2.sectionOptions.put(setKeyValue.user_username.getValue(), var3);
        }

        var3.setPropertyString(setKeyValue.user_username, var1);
        var3 = var2.getSection(setKeyValue.user_lastRunVersion.getValue());
        if (var3 == null) {
            var3 = new ConfigManagerSection();
            var2.sectionOptions.put(setKeyValue.user_lastRunVersion.getValue(), var3);
        }

        var3.setPropertyString(setKeyValue.user_lastRunVersion, "1.0.4933.55");

        try {
            var2.loadConfigIni("config/user.ini");
        } catch (IOException var4) {
            this.logger.error("User config options could not be saved", var4);
        }

    }

    /**
     * Достаём значение из настроек раздел [user] параметр username
     *
     * @return
     */
    public String getUserUsername() {
        ConfigManager var1 = this.kj.getEngineGame().getConfigManager();//Менеджер настроек
        ConfigManagerSection var2 = var1.getSection(setKeyValue.user_username.getValue());//Параметры раздела настроек
        return var2 == null ? "" : var2.getValuePropertyString(setKeyValue.user_username, "");
    }

    /**
     * @return
     */
    public String getUserLastRunVersion() {
        ConfigManager var1 = this.kj.getEngineGame().getConfigManager();
        ConfigManagerSection var2 = var1.getSection(setKeyValue.user_lastRunVersion.getValue());
        return var2 == null ? "" : var2.getValuePropertyString(setKeyValue.user_lastRunVersion, "");
    }

    public String getUrlSite() {
        if (urlBaseCurrent.length() == 0) {
            try {
                ConfigManager var1 = this.kj.getEngineGame().getConfigManager();
                ConfigManagerSection var2 = var1.getSection(setKeyValue.url_site.getValue());
                urlBaseCurrent = var2.getValuePropertyString(setKeyValue.url_site, urlBaseDefault);
            } catch (InvalidSectionNameException var3) {
                urlBaseCurrent = urlBaseDefault;
            }
        }

        return urlBaseCurrent;
    }

    public void setExit() {
        this.kj.getEngineGame().setExit();
    }

    public void cgC() {
        try {
            this.gg(this.urlSignup);
        } catch (Exception var2) {
            this.logger.error("Problems opening Taikodom site (Register)", var2);
        }

    }

    public void cgD() {
        try {
            this.gg(this.urlAuthForgotten);
        } catch (Exception var2) {
            this.logger.error("Problems opening Taikodom site (Forgot Password)", var2);
        }

    }

    public void cgE() {
        try {
            this.gg(this.urlSupport);
        } catch (Exception var2) {
            this.logger.error("Problems opening Taikodom site (Support)", var2);
        }

    }

    public void cgF() {
        try {
            this.gg(this.getUrlSite());
        } catch (Exception var2) {
            this.logger.error("Problems opening Taikodom site (Home)", var2);
        }

    }

    public void cgG() {
        this.kj.aVU().h(new agZ());
    }

    public void cgH() {
        this.kj.aVU().h(new aKv());
    }

    public String loadServerStatusTxt() {
        String var1 = this.getUrlSite() + "/server_status_" + this.location + ".txt";
        return this.jg(var1);
    }

    public String loadServerAlertTxt() {
        String var1 = this.getUrlSite() + "/server_alert_" + this.location + ".txt";
        return this.jg(var1);
    }

    public String jg(String url) {
        String var2 = "";
        try {
            HttpURLConnection.setFollowRedirects(false);
            HttpURLConnection var3 = (HttpURLConnection) (new URL(url)).openConnection();
            if (var3.getResponseCode() != 200) {
                throw new IOException();
            }
            var2 = Hm.a(var3.getInputStream(), Hm.utf8);
            if (var2 == null) {
                return "";
            }
            var2 = var2.trim();
        } catch (MalformedURLException var4) {
            this.logger.error("MalformedURLException", var4);
        } catch (IOException var5) {
            this.logger.error("Could not download the server status or alert update. URL tried: " + url);
        }
        return var2;
    }

    public String cgK() {
        String var1 = "";

        try {
            var1 = this.jh("res/client/notActivated" + (this.location.equalsIgnoreCase("pt") ? "" : "_" + this.location) + ".html");
        } catch (Exception var3) {
            this.logger.error("Could not open not activated notification file.", var3);
        }

        return var1;
    }

    public String cgL() {
        String var1 = "";

        try {
            var1 = this.jh("res/client/license" + (this.location.equalsIgnoreCase("pt") ? "" : "_" + this.location) + ".txt");
        } catch (Exception var3) {
            this.logger.error("Could not open license file.", var3);
        }

        return var1;
    }

    public String releaseNotesTxt() {
        String url = this.getUrlSite() + "/release_notes_" + this.location + ".txt";
        return this.jg(url);
    }

    private String jh(String var1) throws IOException {
        return Hm.a(this.kj.getAddonManager().aVY().concat(var1).getInputStream(), Hm.utf8);
    }

    private void gg(String var1) throws IOException {
        amg.openURL(var1);
    }

    private void b(Nn.atitle var1, String var2) {
        ((MessageDialogAddon) this.kj.U(MessageDialogAddon.class)).a(var1, var2, this.kj.translate("OK"));
    }

    public boolean cgN() {
        this.cgQ();
        int var1 = this.cgP().length();
        return var1 > 0 && var1 < 500;
    }

    public void cgO() {
        if (this.cgN()) {
            this.kj.aVU().h(new aUU((short) 1));
            this.b(Nn.atitle.error, this.cgP());
        }

    }

    public String cgP() {
        return this.fUl;
    }

    private void cgQ() {
        this.ji(this.loadServerAlertTxt());
    }

    public void ji(String var1) {
        this.fUl = var1;
    }

    public String getLocation() {
        return this.location;
    }

    public boolean m(String var1, String var2, String var3) {
        String var4 = VN.execute(this.urlEmailActivation, new Object[]{"locale", this.location, "external_agent", "true", "name", var1, "password", var2, "email", var3});
        boolean var5 = false;
        if (var4.length() == 0) {
            this.b(Nn.atitle.error, this.kj.translate("Server is offline. Please try again later."));
        } else if (var4.equalsIgnoreCase("OK")) {
            this.b(Nn.atitle.info, this.kj.translate("Email sent. Check your inbox."));
            var5 = true;
        } else {
            this.b(Nn.atitle.warning, var4);
        }

        return var5;
    }

    public String ar(String var1, String var2) {
        String var3 = "";
        var3 = VN.h(this.urlEmailFromUser, new Object[]{"locale", this.location, "external_agent", "true", "name", var1, "password", var2});
        return var3.trim();
    }
}
