package all;

public class jM_q {
    public static jM_q apd;
    public static jM_q ape;
    public static jM_q apf;

    static {
        apd = new jM_q(Tu.fEC, Tu.fEC);
        ape = new jM_q(Tu.fEE, Tu.fEE);
        apf = new jM_q(Tu.fEF, Tu.fEF);
    }

    final Tu horizon;
    final Tu vertical;

    public jM_q(Tu var1, Tu var2) {
        this.horizon = var1;
        this.vertical = var2;
    }

    public Tu getHorizon() {
        return this.horizon;
    }

    public Tu getVertical() {
        return this.vertical;
    }

    public boolean equals(Object var1) {
        if (var1 == this) {
            return true;
        } else if (!(var1 instanceof jM_q)) {
            return super.equals(var1);
        } else {
            jM_q var2 = (jM_q) var1;
            return (this.horizon == var2.horizon || this.horizon != null && this.horizon.equals(var2.vertical)) && (this.vertical == var2.vertical || this.vertical != null && this.vertical.equals(var2.vertical));
        }
    }

    public int hashCode() {
        int var1 = this.horizon != null ? this.horizon.hashCode() : 0;
        return var1 * 31 + (this.vertical != null ? this.vertical.hashCode() : 0);
    }

    public String toString() {
        return this.horizon + " " + this.vertical;
    }
}
