package all;

import gnu.trove.TObjectIntHashMap;
import org.w3c.dom.css.CSSStyleDeclaration;

import java.awt.*;
import java.io.StringReader;

public class FlowLayoutCustom extends BaseLayoutCssValue {
    private static final TObjectIntHashMap alignment = new TObjectIntHashMap();
    private static LogPrinter logger = LogPrinter.K(FlowLayoutCustom.class);

    static {
        alignment.put("CENTER", 1);
        alignment.put("TRAILING", 4);
        alignment.put("LEADING", 3);
        alignment.put("LEFT", 0);
        alignment.put("RIGHT", 2);
    }

    public LayoutManager createLayout(Container var1, String var2) {
        FlowLayout var3 = new FlowLayout();
        if (var2 != null && !(var2 = var2.trim()).isEmpty()) {
            try {
                CSSStyleDeclaration var4 = ParserCss.getParserCss().getCSSStyleDeclaration(new InputSource(new StringReader(var2)));
                var3.setAlignment(this.getValueCss(var4, alignment, "alignment", var3.getAlignment()));
                var3.setHgap(this.getValueCss(var4, "hgap", var3.getHgap()));
                var3.setAlignOnBaseline("true".equals(var4.getPropertyCSSValue("alignOnBaseline")));
                var3.setVgap(this.getValueCss(var4, "vgap", var3.getVgap()));
            } catch (Exception var5) {
                logger.warn(var5);
            }
        }
        return var3;
    }
}
