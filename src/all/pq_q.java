package all;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Загрузчик шрифтов
 */
public final class pq_q implements age_q {
    private final GUIModule iZu;
    ThreadLocal iZt = new ThreadLocal() {
        @Override
        protected a initialValue() {
            return new a();
        }
    };
    private Map map = new HashMap();
    private Map iZv = new HashMap();

    pq_q(GUIModule var1) {
        this.iZu = var1;
    }

    public Font m(String var1, int var2, int var3) {
        return this.b(var1, var2, var3, false);
    }

    /**
     * Загрузка шрифта
     *
     * @param var1 Имя шрифта, tahoma
     * @param var2 размер шрифта  11
     * @param var3 стиль шрифта 0
     * @param var4 false
     * @return
     */
    public Font b(String var1, int var2, int var3, boolean var4) {
        a var5 = (a) this.iZt.get();
        var5.a(var1, var2, var3, var4);
        Font var6 = (Font) this.map.get(var5);
        if (var6 != null) {
            return var6;
        } else {
            String var7 = "";
            if ((var3 & 1) > 0) {
                var7 = var7 + "-Bold";
            }

            if ((var3 & 2) > 0) {
                var7 = var7 + "-Italic";
            }

            String var8 = this.iZu.rootPathRender + "/data/fonts/" + var1 + var7 + ".ttf";
            Font var9 = (Font) this.iZv.get(var8);
            if (var9 == null) {
                File var10 = new File(var8);

                try {
                    if (var10.exists()) {
                        var9 = Font.createFont(0, var10);
                    } else {
                        var10 = new File(this.iZu.rootPathRender + "/data/fonts/" + var1 + ".ttf");
                        if (var10.exists()) {
                            var9 = Font.createFont(0, var10);
                        } else {
                            var10 = new File(this.iZu.rootPathRender + "/data/fonts/" + var1.replaceAll("-.*$", "") + ".ttf");
                            if (var10.exists()) {
                                var9 = Font.createFont(0, var10);
                            } else {
                                var9 = new Font(var1, var3, var2);
                            }
                        }
                    }
                } catch (FontFormatException var12) {
                    var12.printStackTrace();
                } catch (IOException var13) {
                    var13.printStackTrace();
                }
            }

            var6 = var9.deriveFont(var3, (float) var2);
            acN.put(var6, "antialiased", var4);
            this.map.put(var5.UZ(), var6);
            return var6;
        }
    }

    static class a {
        String name;
        int height;
        int style;
        boolean aRD;

        public boolean equals(Object var1) {
            a var2 = (a) var1;
            return this.name.equals(var2.name) && this.height == var2.height && this.style == var2.style && this.aRD == var2.aRD;
        }

        public void a(String var1, int var2, int var3, boolean var4) {
            this.name = var1;
            this.height = var2;
            this.style = var3;
            this.aRD = var4;
        }

        public a UZ() {
            a var1 = new a();
            var1.a(this.name, this.height, this.style, this.aRD);
            return var1;
        }

        public int hashCode() {
            return this.name.hashCode() * 31 + this.height * 31 + this.style ^ 255 + (this.aRD ? 1 : 0);
        }
    }
}
