package all;

import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import com.sun.opengl.util.Screenshot;
import org.lwjgl.LWJGLException;
import org.lwjgl.input.Controllers;
import taikodom.render.*;
import taikodom.render.camera.Camera;
import taikodom.render.gl.GLSupportedCaps;
import taikodom.render.gl.GLWrapper;
import taikodom.render.gui.GBillboard;
import taikodom.render.gui.GuiScene;
import taikodom.render.helpers.RenderTask;
import taikodom.render.impostor.ImpostorManager;
import taikodom.render.loader.FileSceneLoader;
import taikodom.render.loader.RenderAsset;
import taikodom.render.loader.provider.BitmapFontFactory;
import taikodom.render.loader.provider.ImageLoader;
import taikodom.render.postProcessing.GlowFX;
import taikodom.render.postProcessing.PostProcessingFX;
import taikodom.render.scene.*;
import taikodom.render.textures.Material;
import taikodom.render.textures.RenderTarget;

import javax.imageio.ImageIO;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLContext;
import javax.media.opengl.GLEventListener;
import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.*;
import java.util.concurrent.Semaphore;

/**
 * Графический движок JR.java
 */
public class EngineGraphics {
    private static final LogPrinter logger;
    private static Semaphore idM;

    static {
        //System.loadLibrary("mss32");// нужен для воспроизведения звука
        //System.loadLibrary("granny2"); // библиотека, обеспечивающая корректную работу игрового приложения
        //System.loadLibrary("binkw32"); //библиотеку видео кодека для игр, разработанного RAD Game Tools
        logger = LogPrinter.K(EngineGraphics.class);
        idM = new Semaphore(1); //синхронизация Семафор.
    }

    private final StepContext stepContext = new StepContext();
    /**
     * Эксклюзивный видеопроигрыватель
     */
    private final ExclusiveVideoPlayer ieq = new ExclusiveVideoPlayer();
    /**
     * Закрыто ли окно
     */
    protected boolean iee;
    /**
     * Размер окна полноэкранного режима
     */
    DisplayMode ier = null;
    /**
     * Локализация
     */
    private String idN = Locale.getDefault().getLanguage().toLowerCase();
    /**
     * GLCanvas и GLJPanel или JGLDesktop(своя реализация) - это два основных класса графического интерфейс openGL
     * Всякий раз, когда вы сталкиваетесь с проблемами с GLJCanvas , вы должны использовать класс GLJPanel .
     */
    private JGLDesktop ddf;
    /**
     * список настроек раздела [render]
     */
    private ConfigManagerSection configManagerSection;
    /**
     * Настройки отрисовки заполняются из раздела [render]
     * idP
     */
    private RenderConfig ren_config = new RenderConfig();
    private b idQ = new b();
    private List ghr = new ArrayList();
    private List idR = new ArrayList();
    private List idS = new ArrayList();
    private List idT = new ArrayList();
    /**
     * Список иконок окна
     */
    private List idU;
    private List idV = Collections.synchronizedList(new LinkedList());
    /**
     * Корневой путь. Каталог с кодом отрисовки графики
     */
    private ain rootpath;
    /**
     * Загрузчик ресурсов файлов с расширением .pro
     */
    private FileSceneLoader loader;
    /**
     * Возвращает экран GraphicsDevice по умолчанию.
     */
    private GraphicsDevice idW;
    /**
     * Возвращает текущий режим отображения этого GraphicsDevice.
     * Настройки экрана, разрешение, глубина цвета, частота
     */
    private DisplayMode idX;
    /**
     * Списка допустимые параметры экрана для игры. Разрешение экрана и частота
     */
    private ArrayList idY;
    /**
     * Окно клиента
     */
    private Frame idZ;
    /**
     * Ширина окна
     */
    private int fr_width;
    /**
     * ВЫсота экрана
     */
    private int fr_height;
    /**
     * Частота экрана
     */
    private int refreshRate;
    /**
     * Глубина цвета 32-bit
     */
    private int bpp;
    private boolean fullscreen;
    /**
     * совпадает ли размер окна и разрешение экрана
     */
    private boolean iec;
    /**
     * взять контроль клавиатуры и мыши
     */
    private Robot ied;
    private GLContext glContext;
    private boolean ief;
    private boolean ieg;
    private SceneView ieh;
    private SceneView iei;
    /**
     * EventManager Блокировщик чего-то или выполнитель задачь в главном потоке для графического движка
     */
    private ajJ_q eventManager;
    private RenderView renderView;
    private List iej;
    private boolean iek = false;
    private GuiScene iel = new GuiScene("GuiScene");
    private RenderGui renderGui;
    private SoundObject iem;
    private SoundObject ien;
    private ImpostorManager impostorManager;
    private GBillboard ieo;
    private boolean iep;

    /**
     * EventManager Блокировщик чего-то или выполнитель задачь в главном потоке для графического движка
     *
     * @param var1
     */
    public EngineGraphics(ajJ_q var1) {
        this.eventManager = var1;
    }

    public SceneView aea() {
        return this.ieh;
    }

    public Scene adZ() {
        return this.ieh.getScene();
    }

    public Camera adY() {
        return this.ieh.getCamera();
    }

    /**
     * Применяем настройки раздела [render]
     *
     * @param var1 список настроек раздела [render]
     * @param var2 Каталог с кодом отрисовки графики
     */
    public void setConfigSection(ConfigManagerSection var1, ain var2) {
        this.configManagerSection = var1;
        this.ren_config.setScreenWidth(this.configManagerSection.getValuePropertyInteger(setKeyValue.render_xres, 1024));
        this.ren_config.setScreenHeight(this.configManagerSection.getValuePropertyInteger(setKeyValue.render_yres, 768));
        this.ren_config.setBpp(this.configManagerSection.getValuePropertyInteger(setKeyValue.render_bpp, 32));
        this.ren_config.setTextureQuality(this.configManagerSection.getValuePropertyInteger(setKeyValue.render_textureQuality, 512));
        this.ren_config.setTextureFilter(this.configManagerSection.getValuePropertyInteger(setKeyValue.render_textureFilter, 0));
        this.ren_config.setAntiAliasing(this.configManagerSection.getValuePropertyInteger(setKeyValue.render_antiAliasing, 0));
        this.ren_config.setLodQuality(this.configManagerSection.getValuePropertyFloat(setKeyValue.render_lodQuality, 0.0F));
        this.ren_config.setEnvFxQuality(this.configManagerSection.getValuePropertyInteger(setKeyValue.render_environmentFxQuality, 0));
        this.ren_config.setShaderQuality(this.configManagerSection.getValuePropertyInteger(setKeyValue.render_shaderQuality, 0));
        this.ren_config.setFullscreen(this.configManagerSection.getValuePropertyBoolean(setKeyValue.render_fullscreen, false));
        this.ren_config.setUseVbo(this.configManagerSection.getValuePropertyBoolean(setKeyValue.render_useVbo, false));
        this.ren_config.setAnisotropicFilter(this.configManagerSection.getValuePropertyFloat(setKeyValue.render_anisotropicFilter, 0.0F));
        this.ren_config.setPostProcessingFX(this.configManagerSection.getValuePropertyBoolean(setKeyValue.render_postProcessingFx, true));
        this.ren_config.setGuiContrastLevel(this.configManagerSection.getValuePropertyFloat(setKeyValue.render_guiContrastBlocker, 0.0F));
        this.idQ.egF = this.configManagerSection.getValuePropertyFloat(setKeyValue.render_musicVolume, 1.0F);
        this.idQ.egG = this.configManagerSection.getValuePropertyFloat(setKeyValue.render_ambienceVolume, 1.0F);
        this.idQ.egH = this.configManagerSection.getValuePropertyFloat(setKeyValue.render_guiVolume, 1.0F);
        this.idQ.egI = this.configManagerSection.getValuePropertyFloat(setKeyValue.render_engineVolume, 1.0F);
        this.idQ.egJ = this.configManagerSection.getValuePropertyFloat(setKeyValue.render_fxVolume, 1.0F);

        this.rootpath = var2;//Каталог с кодом отрисовки графики
        logger.info("Render root path: " + this.rootpath);

        BitmapFontFactory.setRootPath(this.rootpath.getAbsoluteFile().getPath());//Передача  Корневой путь. Каталог с кодом отрисовки графики
        this.loader = new FileSceneLoader(this.rootpath);//Создание загрузчика объектов сцены
        ImageLoader.setMaxMipMapSize(this.ren_config.getTextureQuality());

        this.setupJOGL();
        this.setupSound();
        this.setupControllers();
        this.setupGLEventListener();//Сохранение настроек

        this.renderView = new RenderView(this.ddf);//JGLDesktop интерфейс openGL
        GLWrapper var3 = new GLWrapper(this.ddf.getGL());
        if (!GLSupportedCaps.isPbuffer() || !GLSupportedCaps.isDds()) {
            if (this.aei().equals("convert")) {//Необходимый ресурс не найден. Обновите свой видеодрайвер to_q последней версии и повторите попытку.
                JOptionPane.showMessageDialog(this.idZ, "CreatAllComponentCustom required resource was not found. Update your video driver to_q the lastest version and try again.", "Error", 0);
            } else {
                JOptionPane.showMessageDialog(this.idZ, "Um recurso requerido não foi encontrado. Atualize seu driver de video para uma versão mais recente CreateJComponentAndAddInRootPane tente novamente.", "Erro", 0);
            }
            System.exit(1);
        }
        //Доступная ускоренная память
        logger.info("Available accelerated memory: " + this.idW.getAvailableAcceleratedMemory());
        this.ren_config.setUseVbo(this.ren_config.isUseVbo() && GLSupportedCaps.isVbo());
        this.ddf.setGL(var3);
        this.renderView.setGL(this.ddf.getGL());
        this.renderView.getDrawContext().setCurrentFrame(1000L);
        this.renderView.getDrawContext().setRenderView(this.renderView);
        this.renderView.getDrawContext().setUseVbo(this.ren_config.isUseVbo());

        this.renderGui = new RenderGui();
        this.renderGui.getGuiContext().setDc(this.renderView.getDrawContext());
        this.renderGui.resize(0, 0, this.fr_width, this.fr_height);

        this.ieh = new SceneView();
        this.ieh.setPriority(100);
        this.ieh.getScene().setName("Default Scene");
        this.ieh.getScene().createPartitioningStructure();
        this.ieh.setViewport(0, 0, this.fr_width, this.fr_height);
        this.ieh.setAllowImpostors(true);
        this.impostorManager = new ImpostorManager(this.ddf.getContext());
        this.renderView.getRenderContext().setImpostorManager(this.impostorManager);
        GlowFX var4 = new GlowFX();
        this.ieh.addPostProcessingFX(var4);
        MilesSoundListener var5 = new MilesSoundListener();
        this.ieh.getScene().addChild(var5);
        this.a(this.ieh);

        this.iei = new SceneView();
        this.iei.setClearColorBuffer(false);
        this.iei.setClearDepthBuffer(false);
        this.iei.setClearStencilBuffer(false);
        this.iei.setEnabled(true);
        this.a(this.iei);

        this.ieo = new GBillboard();
        Material var6 = new Material();
        var6.setShader(this.loader.getDefaultVertexColorShader());
        this.ieo.setMaterial(var6);
        this.ieo.setPrimitiveColor(new Color(0.0F, 0.0F, 0.0F, this.ren_config.getGuiContrastLevel()));
        this.ieo.setSize(new aQKa((float) this.ren_config.getScreenWidth(), (float) this.ren_config.getScreenHeight()));
        this.ieo.setPosition(new aQKa((float) (this.ren_config.getScreenWidth() / 2), (float) (this.ren_config.getScreenHeight() / 2)));
        this.ieo.setName("Screen Contrast Billboard");//Экран Contrast Billboard
        this.ieo.setRender(true);
        this.iel.addChild(this.ieo);

        logger.info("Render module initialized");//Модуль Render инициализирован
    }

    private void setupSound() {
        SoundMiles.getInstance().init("");
    }

    /**
     * GLCapabilities указывает неизменный набор возможностей OpenGL.
     */
    private void setupJOGL() {
        GLCapabilities var1 = new GLCapabilities();//Определяет набор возможностей OpenGL.
        var1.setDoubleBuffered(true);//Включает или отключает двойную буферизацию.
        var1.setHardwareAccelerated(true);//Включает или отключает аппаратное ускорение.
        if (this.ren_config.getAntiAliasing() > 0)//Сглаживание
        {
            var1.setSampleBuffers(true);//Указывает, должны ли быть выделены буферы выборки для полного сглаживания сцены (FSAA) для этой возможности.
            var1.setNumSamples(this.ren_config.getAntiAliasing());//Если буферы выборки включены, указывается количество выделенных буферов.
        }

        if (this.ren_config.getBpp() == 32) {
            var1.setDepthBits(24);//Устанавливает количество бит, запрошенных для буфера глубины.
            var1.setStencilBits(8);//Устанавливает количество бит, запрошенных для буфера трафарета.
            var1.setRedBits(8);//Устанавливает количество бит, запрошенных для красного компонента цветового буфера.
            var1.setBlueBits(8);//Устанавливает количество бит, запрошенных для синего компонента цветового буфера.
            var1.setGreenBits(8);//Устанавливает количество бит, запрошенных для зеленого компонента цветового буфера.
            var1.setAlphaBits(8);//Устанавливает количество бит, запрошенных для альфа-компонента цветового буфера.
            this.idW = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();//Возвращает экран GraphicsDevice по умолчанию.
            this.idX = this.idW.getDisplayMode();//Возвращает настройки экрана, разрешение, глубина цвета, частота
            DisplayMode[] var2 = this.idW.getDisplayModes();//Получить список возможных настроек экрана разрешение/частота /цвет
            this.setListSettingsDisplay(var2);
            // isFullScreenSupported Возвращает true, если GraphicsDevice поддерживает полноэкранный эксклюзивный режим.
            if (this.idW.isFullScreenSupported() && this.ren_config.isFullscreen()) {//Создать размер полноэканного режима
                this.ier = this.setFullScreenDisplay(var2, this.ren_config.getScreenWidth(), this.ren_config.getScreenHeight(), this.ren_config.getRefreshRate());
            }

            if (this.ren_config.isFullscreen()) {
                if (this.ier == null) {//Полноэкранный режим не поддерживается, вместо этого выполняется в оконном режиме
                    logger.warn("Full-screen mode not supported, running in windowed mode instead.");
                } else {//Разрешение полноэкранного режима
                    logger.info("Client resolution: " + this.ier.getWidth() + "x" + this.ier.getHeight() + " bpp:" + this.ier.getBitDepth() + " refresh:" + this.ier.getRefreshRate());
                }
            }
            //размеры окна из настроек
            this.fr_width = this.ren_config.getScreenWidth();
            this.fr_height = this.ren_config.getScreenHeight();
            this.refreshRate = this.ren_config.getRefreshRate();//частота экрана
            //совпадает ли размер окна и разрешение экрана
            if (this.fr_width == this.idX.getWidth() && this.fr_height == this.idX.getHeight()) {
                this.iec = true;
            }

            this.idZ = new Frame("Taikodom");//Заголовок окна
            if (this.idU != null) {
                this.idZ.setIconImage((Image) this.idU.get(1));
            }

            this.fullscreen = false;

            this.idZ.setResizable(false);//Устанавливает, изменяется ли этот кадр пользователем.
            this.idZ.setSize(this.fr_width, this.fr_height);//Задать размер окна
            if (this.ier != null)//Если есть настройки Размер окна полноэкранного режима
            {
                this.idZ.setUndecorated(true);//разрешает декорации для этого фрейма. // вызвать только в том случае, если кадр не отображается.
                this.fullscreen = true;
            } else if (this.iec) {
                this.idZ.setUndecorated(true);
            }

            if (!this.fullscreen) {
                Insets var3 = this.idZ.getInsets();
                int var4 = this.idX.getWidth() - var3.left - var3.right;
                int var5 = this.idX.getHeight() - var3.top - var3.bottom;
                this.fr_width = this.fr_width > var4 ? var4 : this.fr_width;
                this.fr_height = this.fr_height > var5 ? var5 : this.fr_height;
            }

            this.bpp = this.ren_config.getBpp();//Глубина цвета 32-bit
            this.idZ.setVisible(true);
            this.idZ.setFocusable(false);
            this.idZ.enableInputMethods(false);//Отключает поддержку метода ввода для этого компонента.

            try {
                this.ied = new Robot();//взять контроль клавиатуры и мыши
            } catch (AWTException e) {
                e.printStackTrace();
            }
            //this.ied.mouseMove(0, 0);
            this.ied.setAutoDelay(0);
            this.ied.setAutoWaitForIdle(false);

            this.ddf = new JGLDesktop(var1, null, null, this.idW)//создаём объект openGL
            {
                public boolean isFocusCycleRoot() {
                    return true;
                }

                public void paint(Graphics var1) {
                }
            };
            this.ddf.setSize(this.fr_width, this.fr_height);//Задаём размер JGLDesktop
            if (this.iec) {
                this.ddf.getRootPane().setSize(this.fr_width, this.fr_height);
            }

            this.ddf.setAutoSwapBufferMode(false);//отключает автоматическую свопинг буфера для этой возможности.
            this.ddf.getContext().setSynchronized(true);
            this.ddf.enableInputMethods(true);//Включает поддержку метода ввода для этого компонента.
            this.glContext = this.ddf.getContext();
            if (this.fullscreen) {
                this.idZ.add(this.ddf);//Добавляем  openGL в окно
            } else {
                this.idZ.add(this.ddf);
            }

            this.idZ.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent var1) {
                    EngineGraphics.this.iee = true;
                }

                @Override
                public void windowClosed(WindowEvent var1) {
                    EngineGraphics.this.iee = true;
                }

                @Override
                public void windowOpened(WindowEvent var1) {
                    super.windowOpened(var1);
                    EngineGraphics.this.idZ.invalidate();//флаг - требует обновления макета
                }
            });

            if (this.idW.isFullScreenSupported() && this.ier != null) {//Полноэкранный режим задан
                this.idW.setFullScreenWindow(this.idZ);//Войдите в полноэкранный режим
                if (this.idW.isDisplayChangeSupported())//доступны низкоуровневые изменения
                {
                    this.idW.setDisplayMode(this.ier);//разрешения экрана
                    this.fullscreen = true;
                } else {
                    this.aF(this.fr_width, this.fr_height);
                    //Невозможно изменить режим отображения to_q, полноэкранный режим отключен
                    logger.warn("Unable to_q change display mode, full-screen disabled.");
                }
            } else //Выбран оконный режим
            {
                this.aF(this.fr_width, this.fr_height);
            }

            if (this.fullscreen) {
                this.idZ.addWindowListener(new WindowAdapter() {
                    public void windowActivated(WindowEvent var1) {
                        Thread var2 = new Thread("FullScreen") {
                            public void run() {
                                try {
                                    Thread.sleep(2000L);
                                    EngineGraphics.this.idW.setDisplayMode(EngineGraphics.this.idX);
                                    Thread.sleep(2000L);
                                    EngineGraphics.this.idW.setDisplayMode(EngineGraphics.this.ier);
                                } catch (InterruptedException var2) {
                                    logger.error(var2);
                                }
                            }
                        };
                        var2.start();
                    }

                    public void windowDeactivated(WindowEvent var1) {
                        if (EngineGraphics.this.idW.isDisplayChangeSupported()) {
                            EngineGraphics.this.idW.setDisplayMode(EngineGraphics.this.idX);
                        }
                    }
                });
            }

            int var6 = this.ddf.getContext().makeCurrent();
            if (var6 == 0) {//Невозможно to_q сделать GL-контекст активным
                throw new RuntimeException("Unable to_q make GL context active");
            } else {
                this.ddf.requestFocus();
                this.ddf.getGL().glClear(16384);
            }
        } else {
            throw new RuntimeException("Invalid bpp: " + this.ren_config.getBpp() + ". Taikodom needs 32 bpp.");
        }
    }

    /**
     * Задать полноэкранный режим
     *
     * @param var1 Списка допустимые параметры экрана для игры. Разрешение экрана и частота
     * @param var2 Ширина
     * @param var3 Высота
     * @param var4 Частота
     * @return
     */
    private DisplayMode setFullScreenDisplay(DisplayMode[] var1, int var2, int var3, int var4) {
        DisplayMode var5 = null;
        DisplayMode[] var9 = var1;

        DisplayMode var6;
        int var7;
        for (var7 = 0; var7 < var1.length; ++var7) {
            var6 = var9[var7];
            if (var6.getBitDepth() == 32 && var6.getWidth() == var2 && var6.getHeight() == var3 && var6.getRefreshRate() >= var4 && (var5 == null || var5.getRefreshRate() < var4)) {
                var5 = var6;
            }
        }

        if (var5 != null) {
            return var5;
        } else {
            var9 = var1;

            for (var7 = 0; var7 < var1.length; ++var7) {
                var6 = var9[var7];
                if (var6.getBitDepth() == 32 && var6.getWidth() >= var2 && var6.getHeight() >= var3 && var6.getRefreshRate() >= 60 && (var5 == null || var5.getRefreshRate() < var4)) {
                    var5 = var6;
                }
            }
            return var5;
        }
    }

    /**
     * Заполнения списка допустимые параметры экрана для игры
     *
     * @param var1 Cписок возможных настроек экрана разрешение/частота /цвет
     */
    private void setListSettingsDisplay(DisplayMode[] var1) {
        this.idY = new ArrayList();
        for (int var3 = 0; var3 < var1.length; ++var3) {
            DisplayMode var2 = var1[var3];
            if (var2.getWidth() >= 1024 && var2.getBitDepth() == 32 && var2.getRefreshRate() >= 60) {//Разрешение экрана и частота
                Vec3f var6 = new Vec3f((float) var2.getWidth(), (float) var2.getHeight(), (float) var2.getRefreshRate());
                boolean var7 = false;
                Iterator var9 = this.idY.iterator();

                while (var9.hasNext())//ПРоверка, что такого параметра нет ещё в списке
                {
                    Vec3f var8 = (Vec3f) var9.next();
                    if (var8.equals(var6)) {
                        var7 = true;
                        break;
                    }
                }

                if (!var7) {
                    this.idY.add(var6);//Добавить в список
                }
            }
        }
    }

    /**
     * Установить иконки окна
     *
     * @param var1 Массив иконок
     */
    public void j(List var1) {
        this.idU = new ArrayList(var1);
        if (this.idZ != null)//создано ли окно
        {
            this.idZ.setIconImage((Image) this.idU.get(1));//Задаём окну иконку
        }
    }

    /**
     * Задать размер окна и GL компонента и его положение
     *
     * @param var1 Ширина
     * @param var2 Высота
     */
    private void aF(int var1, int var2) {
        final Frame var3 = this.idZ;
        final JGLDesktop var4 = this.ddf;
        // final DisplayMode var99 = this.idX;

        final int var5 = var1;
        final int var6 = var2;
        try {
            EventQueue.invokeAndWait(new Runnable() {
                public void run() {
                    Insets var1 = var3.getInsets();//размер рамок
                    // var3.setLocation((var99.getWidth()/2)-var5/2, var99.getHeight()/2-var6/2);//Сделать окно по центру
                    var3.setSize(var5 + var1.left + var1.right, var6 + var1.top + var1.bottom);

                    var4.setLocation(var1.left, var1.top);
                    var4.setSize(var5, var6);
                    if (EngineGraphics.this.iec) {
                        var4.getRootPane().setSize(var5, var6);
                    }

                }
            });
        } catch (Exception var8) {
            var8.printStackTrace();
        }

    }

    public int getAntiAliasing() {
        return this.ren_config.getAntiAliasing();
    }

    public void setAntiAliasing(int var1) {
        this.ren_config.setAntiAliasing(var1);
    }

    public int aes() {
        return this.ddf.getWidth();
    }

    public int aet() {
        return this.ddf.getHeight();
    }

    public float aeg() {
        return this.idQ.egH;
    }

    public void dp(float var1) {
        this.idQ.egH = var1;
    }

    public float aed() {
        return this.idQ.egJ;
    }

    public void do_q(float var1) {
        this.idQ.egJ = var1;
    }

    public void setAnisotropicFilter(float var1) {
        this.ren_config.setAnisotropicFilter(var1);
    }

    public float bb() {
        return this.ren_config.getAnisotropicFilter().floatValue();
    }

    /**
     * Закрыто ли окно
     *
     * @return
     */
    public boolean aeu() {
        return this.iee;
    }

    public void close() {
        if (this.fullscreen) {
            this.idW.setDisplayMode(this.idX);
            this.idW.setFullScreenWindow((Window) null);
        }

        this.ddf.getContext().release();
        this.idZ.setVisible(false);
        this.idZ.dispose();
        this.ddf = null;
        this.idZ = null;
        SoundMiles.getInstance().shutdown();
    }

    public String aei() {
        return this.idN;
    }

    public void aez() {
        this.ief = true;
    }

    public void aeA() {
        this.ieg = true;
    }

    /**
     * Сохранить снимок экрана
     */
    public void swapBuffers() {
        try {
            this.ddf.getDrawable().swapBuffers();
            if (this.ief) {
                this.ief = false;
                FileControl var1 = new FileControl("data/screenshots");
                var1.getFile().mkdirs();
                Calendar var2 = Calendar.getInstance();
                String var3 = "" + var2.get(1);
                var3 = var3 + "_" + var2.get(2);
                var3 = var3 + "_" + var2.get(5);
                var3 = var3 + "_" + var2.get(14);
                var1 = new FileControl("data/screenshots/screenshot_" + var3 + "." + "jpg");
                FileOutputStream var4 = new FileOutputStream(var1.getFile());
                BufferedImage var5 = Screenshot.readToBufferedImage(this.fr_width, this.fr_height);
                ImageIO.write(var5, "jpg", var4);
                var4.close();
            }

            if (this.ieg) {
                try {
                    this.ieg = false;
                    RenderTarget var15 = new RenderTarget();//Создание поверхности рисования для снимков HighRes
                    logger.info("Creating draw surface for HighRes screenshot");
                    short var16 = 2048;
                    var15.createAsPBO(var16, var16, this.renderView.getDrawContext());
                    var15.bind(this.renderView.getDrawContext());
                    RenderTarget var17 = this.ieh.getRenderTarget();
                    this.ieh.setRenderTarget((RenderTarget) null);
                    ArrayList var18 = new ArrayList(this.ieh.getPostProcessingFXs());
                    this.ieh.getPostProcessingFXs().clear();
                    Viewport var19 = new Viewport(this.ieh.getViewport());
                    this.ieh.setViewport(0, 0, var16, var16);
                    this.ieh.getRenderInfo().setClearColor(1.0F, 0.0F, 0.0F, 1.0F);
                    float var6 = this.ieh.getCamera().getAspect();
                    this.ieh.getCamera().setAspect(1.0F);
                    this.getGL().glDisable(3089);
                    logger.info("Rendering to_q screenshot surface");
                    this.renderView.render(this.ieh, 0.0D, 0.0F);
                    this.ieh.setRenderTarget(var17);
                    this.ieh.setViewport(var19);
                    this.ieh.getCamera().setAspect(var6);
                    this.ieh.getPostProcessingFXs().addAll(var18);
                    FileControl var7 = new FileControl("data/screenshots");
                    var7.getFile().mkdirs();
                    Calendar calendar = Calendar.getInstance();
                    String date = "" + calendar.get(1);
                    date = date + "_" + calendar.get(2);
                    date = date + "_" + calendar.get(5);
                    date = date + "_" + calendar.get(14);
                    String var10 = "png";
                    logger.info("Creating file screenshot_high_" + date + "." + var10);
                    var7 = new FileControl("data/screenshots/screenshot_high_" + date + "." + var10);
                    FileOutputStream var11 = new FileOutputStream(var7.getFile());
                    BufferedImage var12 = Screenshot.readToBufferedImage(var16, var16);
                    logger.info("Saving screenshot");
                    ImageIO.write(var12, var10, var11);
                    var11.close();
                    var15.unbind(this.renderView.getDrawContext());
                    var15.releaseReferences(this.getGL());
                    logger.info("Screenshot saved");
                } catch (Exception var13) {
                    logger.error(var13);
                }
            }
        } catch (Exception var14) {
            logger.error(var14);
        }

    }

    /**
     * Проверяет, существует ли файл или каталог
     *
     * @param var1
     * @return
     */
    private boolean kw(String var1) {
        return this.rootpath.concat(var1).exists();
    }

    /**
     * Загрузка адресов ресурсов из файлов .pro
     *
     * @param var1 Пример data/audio/interface/interface_sfx.pro
     */
    public void bV(String var1) {
        if (var1 == null) {
            logger.debug("trying to_q loadStockDefinitions from null file!");
        } else {
            if (this.aei() != null) {
                String var2 = var1.replaceAll("([.][^.]+)$", "_" + this.aei() + "$1");
                if (this.kw(var2)) {
                    var1 = var2;
                }
            }

            try {
                idM.acquire();

                label125:
                {
                    try {
                        if (this.loader.getStockFile(var1) == null) {
                            break label125;
                        }
                    } finally {
                        idM.release();
                    }

                    return;
                }

                logger.debug("loadStockDefinitions " + var1);
                idM.acquire();

                try {
                    this.loader.loadFile(var1);
                    this.dfm();
                } catch (FileNotFoundException var12) {
                    System.err.println("StockDefinitions: Failed to_q load file: '" + var1 + "'");
                    var12.printStackTrace();
                } finally {
                    idM.release();
                }
            } catch (Exception var15) {
                var15.printStackTrace();
            }

        }
    }

    /**
     * Попытка загрузка локализованных текстур из includes
     *
     * @throws IOException
     */
    private void dfm() throws IOException {
        String var1;
        for (; this.loader.getMissingIncludes().size() > 0; this.loader.loadFile(var1))//Загрузка ресурса
        {
            var1 = (String) this.loader.getMissingIncludes().iterator().next();
            if (this.aei() != null) {
                String var2 = var1.replaceAll("([.][^.]+)$", "_" + this.aei() + "$1");
                if (this.kw(var2))//Проверяет, существует ли файл или каталог
                {
                    var1 = var2;
                }
            }
        }
    }

    public RenderAsset bT(String var1) {
        if (var1 == null) {
            logger.debug("null object name");
            return null;
        } else {
            if (this.aei() != null) {
                String var2 = var1 + "_" + this.aei();
                RenderAsset var3 = this.loader.instantiateAsset(var2);
                if (var3 != null) {
                    return var3;
                }
            }

            RenderAsset var4 = this.loader.instantiateAsset(var1);
            return var4;
        }
    }

    /**
     * Переместить указатель мыши в указанные координаты
     *
     * @param var1
     * @param var2
     * @param var3
     */
    public void a(int var1, int var2, boolean var3) {
        Insets var4 = this.idZ.getInsets();
        Rectangle var5 = this.idZ.getBounds();
        int var6 = var1 + var4.left + var5.x;
        int var7 = var2 + var4.top + var5.y;
        this.ied.mouseMove(var6, var7);
    }

    public void a(akI.a var1) {
        if (var1 == null) {
            if (logger.isDebugEnabled()) {
                throw new IllegalArgumentException("Null steppable added to_q RenderModule.");
            }
        } else {
            this.idS.add(var1);
        }
    }


    public void b(akI.a var1) {
        this.idT.add(var1);
    }

    public boolean step(float var1) {
        long var2 = atM.currentTimeMillis();
        this.stepContext.reset();
        this.stepContext.setDeltaTime(var1);
        List var5 = this.idV;
        ArrayList var4;
        synchronized (this.idV) {
            var4 = new ArrayList(this.idV);
        }

        Iterator var6 = var4.iterator();

        while (var6.hasNext()) {
            SceneView var10 = (SceneView) var6.next();
            if (var10.isEnabled()) {
                this.stepContext.setCamera(var10.getCamera());
                var10.getScene().step(this.stepContext);
                var10.getCamera().step(this.stepContext);
            }
        }

        this.iel.step(this.stepContext);
        this.idR.removeAll(this.idT);
        this.idR.addAll(this.idS);
        this.idS.clear();
        this.idT.clear();
        var6 = this.idR.iterator();

        while (var6.hasNext()) {
            akI.a var11 = (akI.a) var6.next();

            try {
                var11.a(var1, var2);
            } catch (Exception var9) {
                this.idR.remove(var11);
                System.err.println(var9);
                var9.printStackTrace();
                break;
            }
        }

        return true;
    }

    public void i(float var1, float var2) {
        this.stepContext.setDeltaTime(var2);
        this.aey();
        List var4 = this.idV;
        ArrayList var3;
        synchronized (this.idV) {
            var3 = new ArrayList(this.idV);
        }

        this.renderView.getDrawContext().reset();
        Iterator var5 = var3.iterator();

        while (var5.hasNext()) {
            SceneView var8 = (SceneView) var5.next();
            if (var8.isEnabled()) {
                this.stepContext.setCamera(var8.getCamera());
                var8.getCamera().step(this.stepContext);
                this.renderView.render(var8, (double) var1, var2);
            }
        }

        try {
            this.renderGui.render(this.iel);
        } catch (Exception var6) {
            logger.warn("Exception while rendering HUD scene");
            var6.printStackTrace();
        }

    }

    public List aep() {
        return this.idV;
    }

    public void a(SceneView var1) {
        int var2 = 0;
        synchronized (this.idV) {
            Iterator var5 = this.idV.iterator();

            while (true) {
                if (var5.hasNext()) {
                    SceneView var4 = (SceneView) var5.next();
                    if (var4.getPriority() >= var1.getPriority()) {
                        ++var2;
                        continue;
                    }
                }
                this.idV.add(var2, var1);
                break;
            }
        }
        this.dfn();
    }

    public void b(SceneView var1) {
        this.idV.remove(var1);
    }

    private void dfn() {
        ArrayList var1;
        synchronized (this.idV) {
            var1 = new ArrayList(this.idV);
        }

        Iterator var3 = var1.iterator();

        while (var3.hasNext()) {
            SceneView var7 = (SceneView) var3.next();
            var7.setSceneViewQuality(this.ren_config.getSceneViewQuality());
            Iterator var5 = var7.getPostProcessingFXs().iterator();

            while (var5.hasNext()) {
                PostProcessingFX var4 = (PostProcessingFX) var5.next();
                var4.setEnabled(this.ren_config.isPostProcessingFX());
            }
        }
    }

    public ain aen() {
        return this.rootpath;
    }

    public int getShaderQuality() {
        return this.ren_config.getShaderQuality();
    }

    public void setShaderQuality(int var1) {
        this.ren_config.setShaderQuality(var1);
        this.dfn();
    }

    public FileSceneLoader aej() {
        return this.loader;
    }

    public JGLDesktop aee() {
        return this.ddf;
    }

    public RenderAsset bU(String var1) {
        return this.bT(var1);
    }

    public boolean getUseVBO() {
        return this.ren_config.isUseVbo();
    }

    public float getLodQuality() {
        return this.ren_config.getLodQuality();
    }

    public void setLodQuality(float var1) {
        this.ren_config.setLodQuality(var1);
        this.dfn();
    }

    public void eJ(int var1) {
        this.ren_config.setEnvFxQuality(var1);
        //SZ.Al(var1);
        this.dfn();
    }

    public int aec() {
        return this.ren_config.getEnvFxQuality();
    }

    public int ael() {
        return this.idY.size();
    }

    public Vec3f eH(int var1) {
        return (Vec3f) this.idY.get(var1);
    }

    public float adX() {
        return this.idQ.egG;
    }

    public void dm(float var1) {
        this.idQ.egG = var1;
        if (this.iem != null) {
            this.iem.setGain(var1);
        }
    }

    public float aeb() {
        return this.idQ.egI;
    }

    public void dn(float var1) {
        this.idQ.egI = var1;
        this.dfo();
    }

    private void dfo() {
        Scene var1 = this.adZ();
        synchronized (var1.getChildren()) {
            Iterator var4 = var1.getChildren().iterator();
            while (var4.hasNext()) {
                SceneObject var3 = (SceneObject) var4.next();
                if (var3 instanceof SPitchedSet) {
                    SPitchedSet var5 = (SPitchedSet) var3;
                    var5.setGain(this.idQ.egI);
                }
            }
        }
    }

    public void dq(float var1) {
        this.idQ.egF = var1;
        if (this.ien != null) {
            this.ien.setGain(var1);
        }
    }

    public float aek() {
        return this.idQ.egF;
    }

    public void adV() {
        this.ieh.getScene().removeAllChildren();
        this.aeh().removeAllChildren();
        this.aeh().addChild(this.ieo);
        this.impostorManager.clear();
        MilesSoundListener var1 = new MilesSoundListener();
        this.adZ().addChild(var1);
    }

    public void dr(float var1) {
        if (this.iem != null) {
            this.iem.fadeOut(var1);
            this.iem = null;
        }
    }

    public void ds(float var1) {
        if (this.ien != null) {
            this.ien.fadeOut(var1);
            this.ien = null;
        }

    }

    public void eI(int var1) {
        //Звуковые мины получают экземпляр для голодания
        SoundMiles.getInstance().prepareForStarving(var1);
    }

    public void aex() {
        SoundMiles.getInstance().restoreFromStarving();
    }

    public GLContext aef() {
        return this.glContext;
    }

    public javax.media.opengl.GL getGL() {
        return this.glContext.getGL();
    }

    public void setPostProcessingFX(boolean var1) {
        this.ren_config.setPostProcessingFX(var1);
        this.dfn();
    }

    public boolean aem() {
        return this.ren_config.isPostProcessingFX();
    }

    public void aev() {
        throw new RuntimeException("Must implement this");
    }

    public void bW(String var1) {
        this.b(var1, 0.0F);
    }

    public void b(String var1, float var2) {
        logger.debug("playAmbienceMusic " + var1);
        SoundObject var3 = (SoundObject) this.bT(var1);
        if (var3 != null) {
            if (var3 == this.iem) {
                return;
            }

            this.iei.getScene().addChild(var3);
        }

        if (this.iem != null) {
            SoundObject var4 = this.iem;
            this.ds(2.0F);
            this.iei.getScene().addChild(var4);
        }

        this.iem = var3;
        if (this.iem != null) {
            this.iem.setGain(this.adX());
            this.iem.play();
            if (var2 > 0.0F) {
                this.iem.fadeIn(var2);
            }
        }

    }

    public void bX(String var1) {
        this.c(var1, 0.0F);
    }

    public void c(String var1, float var2) {
        logger.debug("playMultiLayerMusic " + var1);
        SoundObject var3 = (SoundObject) this.bT(var1);
        if (var3 != null) {
            if (var3 == this.ien) {
                return;
            }

            this.iei.getScene().addChild(var3);
        }

        if (this.ien != null) {
            SoundObject var4 = this.ien;
            this.ds(2.0F);
            this.iei.getScene().addChild(var4);
        }

        this.ien = var3;
        if (this.ien != null) {
            this.ien.setGain(this.aek());
            this.ien.play();
            if (var2 > 0.0F) {
                this.ien.fadeIn(var2);
            }
        }

    }

    public List aeo() {
        if (this.iej == null) {
            this.iej = new ArrayList();
        }

        if (this.getRenderView() != null && this.getRenderView().getDrawContext().eventList != null) {
            this.iej.addAll(this.getRenderView().getDrawContext().eventList);
            this.getRenderView().getDrawContext().eventList.clear();
        }

        return this.iej;
    }

    public void adW() {
        this.glContext.getGL().glFlush();
    }

    public void aeE() {
        this.glContext.getGL().glFinish();
    }

    private void setupGLEventListener() {
        if (this.eventManager == null) {//Предупреждение: нет EventManager. Слушатели не будут созданы
            System.out.println("Warning: no EventManager present. Listeners wont be created");
        } else {
            /**GLEventListener - Чтобы ваша программа могла использовать графический API JOGL
             */
            GLEventListener var1 = new GLEventListener() {
                /**Он вызывается объектом интерфейса GLAutoDrawable во время первой перерисовки после изменения размера компонента.
                 * Он также вызывается всякий раз, когда положение компонента в окне изменяется.
                 * @param glAutoDrawable Поверхность для рисования для команд OpenGL.
                 * @param x
                 * @param y
                 * @param width
                 * @param height
                 */
                public void reshape(GLAutoDrawable glAutoDrawable, int x, int y, int width, int height) {
                    EngineGraphics.this.ren_config.setScreenWidth(width);
                    EngineGraphics.this.ren_config.setScreenHeight(width);
                    EngineGraphics.this.ren_config.setBpp(32);
                    EngineGraphics.this.ren_config.setFullscreen(false);
                    EngineGraphics.this.fr_width = width;
                    EngineGraphics.this.fr_height = height;
                    EngineGraphics.this.bpp = 32;
                    EngineGraphics.this.fullscreen = false;
                }

                /**
                 *этот метод содержит логику, используемую для рисования графических элементов с использованием OpenGL API.
                 * @param var1 Поверхность для рисования OpenGL и примитивы рисования(линии треугольники).
                 */
                public void display(GLAutoDrawable var1) {
                }

                /**
                 *
                 * @param glAutoDrawable Поверхность для рисования для команд OpenGL.
                 * @param var2
                 * @param var3
                 */
                public void displayChanged(GLAutoDrawable glAutoDrawable, boolean var2, boolean var3) {
                }

                /**
                 * Он вызывается объектом интерфейса GLAutoDrawable сразу после инициализации GLContext OpenGL.
                 * @param var1  Поверхность для рисования OpenGL и примитивы рисования(линии треугольники).
                 */
                public void init(GLAutoDrawable var1) {
                }
            };

            /**GLEventListener - Чтобы ваша программа могла использовать графический API JOGL
             */
            this.ddf.addGLEventListener(var1);
            this.ddf.setFocusTraversalKeysEnabled(false);
        }
    }

    private void setupControllers() {
        try {
            Controllers.create();
        } catch (LWJGLException var4) {//Невозможно инициализировать контроллеры to_q!
            logger.debug("Unable to_q initialize controllers!");
        } finally {
            this.iek = true;
        }
    }

    public void aew() {
        if (this.iek) {
            Controllers.clearEvents();
            Controllers.poll();
            this.dfr();
        }

    }

    /**
     * Обработчик нажатий кнопок джостика
     */
    private void dfr() {
        while (Controllers.next()) {
            if (Controllers.isEventButton()) {//Джостик кнопка
                this.h(new OA(Controllers.getEventControlIndex(), Controllers.getEventSource().isButtonPressed(Controllers.getEventControlIndex())));
            } else if (Controllers.isEventAxis()) {//Джостик оси
                int var1 = Controllers.getEventControlIndex();
                byte var2;
                if (Controllers.isEventXAxis()) {
                    var2 = 0;
                } else if (Controllers.isEventYAxis()) {
                    var2 = 1;
                } else {
                    switch (var1) {
                        case 0:
                        case 3:
                            var2 = 3;
                            break;
                        case 1:
                        case 2:
                        default:
                            var2 = 2;
                    }
                }

                this.h(new aeh(var2, Controllers.getEventSource().getAxisValue(Controllers.getEventControlIndex())));
            } else if (Controllers.isEventPovX() || Controllers.isEventPovY()) {
                this.h(new Et_g(QK.n(Controllers.getEventSource().getPovX(), Controllers.getEventSource().getPovY()), true));
            }
        }
    }

    private void h(Object var1) {
        this.eventManager.h(var1);
    }

    public GuiScene aeh() {
        return this.iel;
    }

    public RenderView getRenderView() {
        return this.renderView;
    }

    public StepContext aer() {
        return this.stepContext;
    }

    public float aeq() {
        return (float) this.fr_width / (float) this.fr_height;
    }

    public void a(RenderTask var1) {
        synchronized (this.ghr) {
            if (!this.ghr.contains(var1)) {
                this.ghr.add(var1);
            }
        }
    }

    public void aey() {
        ArrayList var1;
        synchronized (this.ghr) {
            var1 = new ArrayList(this.ghr);
            this.ghr.clear();
        }
        Iterator var3 = var1.iterator();
        while (var3.hasNext()) {
            RenderTask var7 = (RenderTask) var3.next();
            try {
                var7.run(this.renderView);
            } catch (Exception var5) {
                logger.error("Ignoring erroneous render task: " + var7.getClass(), var5);
            }
        }
    }

    public void setTextureQuality(int var1) {
        this.ren_config.setTextureQuality(var1);
        ImageLoader.setMaxMipMapSize(this.ren_config.getTextureQuality());
    }

    public SceneView bY(String var1) {
        Iterator var3 = this.idV.iterator();
        SceneView var2;
        do {
            if (!var3.hasNext()) {
                return null;
            }
            var2 = (SceneView) var3.next();
        } while (var2.getHandle() == null || !var2.getHandle().equals(var1));
        return var2;
    }

    /**
     * Установить видео для проигрывания в Эксклюзивный видеопроигрыватель
     *
     * @param var1
     */
    public void a(Video var1) {
        this.ieq.playVideo(var1);
    }

    public void stopVideo() {
        this.ieq.stopVideo();
    }

    /**
     * Проигрывается ли видео
     *
     * @return
     */
    public boolean aeB() {
        return this.ieq.isPlaying();
    }

    /**
     * Начать проигрование видео заставки
     */
    public void aeC() {
        this.ieq.renderFrame(this.renderView.getDrawContext());
    }

    public int getRefreshRate() {
        return this.refreshRate;
    }

    /**
     * Получить окно клиента
     *
     * @return
     */
    public Frame dfs() {
        return this.idZ;
    }

    private class b {
        public float egF;
        public float egG;
        public float egH;
        public float egI;
        public float egJ;
    }
}
