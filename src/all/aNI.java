package all;

import org.w3c.dom.css.CSSMediaRule;
import org.w3c.dom.css.CSSRule;
import org.w3c.dom.css.CSSRuleList;
import org.w3c.dom.css.CSSStyleSheet;
import org.w3c.dom.stylesheets.MediaList;

import java.io.Serializable;
import java.io.StringReader;

public class aNI implements Serializable, CSSMediaRule {
    private SS qq = null;
    private CSSRule qr = null;
    private MediaList qt = null;
    private CSSRuleList iwD = null;

    public aNI(SS var1, CSSRule var2, MediaList var3) {
        this.qq = var1;
        this.qr = var2;
        this.qt = var3;
    }

    public short getType() {
        return 4;
    }

    public String getCssText() {
        StringBuffer var1 = new StringBuffer("@media ");
        var1.append(this.getMedia().toString()).append(" {");

        for (int var2 = 0; var2 < this.getCssRules().getLength(); ++var2) {
            CSSRule var3 = this.getCssRules().item(var2);
            var1.append(var3.getCssText()).append(" ");
        }

        var1.append("}");
        return var1.toString();
    }

    public void setCssText(String var1) {
        if (this.qq != null && this.qq.isReadOnly()) {
            throw new qN((short) 7, 2);
        } else {
            try {
                InputSource var2 = new InputSource(new StringReader(var1));
                ParserCss var3 = ParserCss.getParserCss();
                CSSRule var4 = var3.f(var2);
                if (var4.getType() == 4) {
                    this.qt = ((aNI) var4).qt;
                    this.iwD = ((aNI) var4).iwD;
                } else {
                    throw new qN((short) 13, 7);
                }
            } catch (CSSException var5) {
                throw new qN((short) 12, 0, var5.getMessage());
            } /*catch (IOException var6) {
            throw new qN((short)12, 0, var6.getMessage());
         }*/
        }
    }

    public CSSStyleSheet getParentStyleSheet() {
        return this.qq;
    }

    public CSSRule getParentRule() {
        return this.qr;
    }

    public MediaList getMedia() {
        return this.qt;
    }

    public CSSRuleList getCssRules() {
        return this.iwD;
    }

    public int insertRule(String var1, int var2) {
        if (this.qq != null && this.qq.isReadOnly()) {
            throw new qN((short) 7, 2);
        } else {
            try {
                InputSource var3 = new InputSource(new StringReader(var1));
                ParserCss var4 = ParserCss.getParserCss();
                var4.a(this.qq);
                var4.b(this.qr);
                CSSRule var5 = var4.f(var3);
                ((RK) this.getCssRules()).add(var5, var2);
                return var2;
            } catch (ArrayIndexOutOfBoundsException var6) {
                throw new qN((short) 1, 1, var6.getMessage());
            } catch (CSSException var7) {
                throw new qN((short) 12, 0, var7.getMessage());
            }/* catch (IOException var8) {
            throw new qN((short)12, 0, var8.getMessage());
         }*/
        }
    }

    public void deleteRule(int var1) {
        if (this.qq != null && this.qq.isReadOnly()) {
            throw new qN((short) 7, 2);
        } else {
            try {
                ((RK) this.getCssRules()).delete(var1);
            } catch (ArrayIndexOutOfBoundsException var3) {
                throw new qN((short) 1, 1, var3.getMessage());
            }
        }
    }

    public void a(RK var1) {
        this.iwD = var1;
    }

    public String toString() {
        return this.getCssText();
    }
}
