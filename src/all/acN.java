package all;

import java.util.EnumSet;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class acN {
    private static ComponentGraphicsResource ffa;

    static {
        ffa = new ComponentGraphicsResource(16, 0.75F, 16, ComponentGraphicsResource.i.eeK, ComponentGraphicsResource.i.eeJ, EnumSet.of(ComponentGraphicsResource.h.dRd));
    }

    public static Object get(Object var0, Object var1) {
        Map var2 = (Map) ffa.get(var0);
        return var2 == null ? null : var2.get(var1);
    }

    public static Map ak(Object var0) {
        return (Map) ffa.get(var0);
    }

    public static Object get(Object var0, Object var1, Object var2) {
        Map var3 = (Map) ffa.get(var0);
        if (var3 == null) {
            return var2;
        } else {
            Object var4 = var3.get(var1);
            return var4 == null ? var2 : var4;
        }
    }

    public static Object put(Object var0, Object var1, Object var2) {
        Object var3 = (Map) ffa.get(var0);
        if (var3 == null) {
            Class var4 = acN.class;
            synchronized (acN.class) {
                var3 = (Map) ffa.get(var0);
                if (var3 == null) {
                    var3 = new ConcurrentHashMap();
                    ffa.put(var0, var3);
                }
            }
        }

        Object var6 = ((Map) var3).get(var1);
        if (var2 != null) {
            if (var6 != null) {
                ((Map) var3).remove(var1);
                ((Map) var3).put(var1, var2);
            } else {
                ((Map) var3).put(var1, var2);
            }
        } else if (var6 != null) {
            ((Map) var3).remove(var1);
        }

        return var6;
    }
}
