package all;

import javax.swing.*;
import java.awt.*;

/**
 * JScrollBar
 */
public class ScrollBarCustom extends BaseItem {
    private static final LogPrinter logger = LogPrinter.setClass(SliderCustom.class);

    /**
     * Создание базового Component интерфейса
     * Вызывается из class BaseItem
     *
     * @param var1
     * @param var2
     * @param var3
     * @return
     */
    @Override
    protected JComponent CreatComponentCustom(MapKeyValue var1, Container var2, XmlNode var3) {
        return new JScrollBar("vertical".equals(var3.getAttribute("orientation")) ? 1 : 0);
    }

    protected void a(MapKeyValue var1, JScrollBar var2, XmlNode var3) {
        super.applyAttribute(var1, (Component) var2, (XmlNode) var3);
        Integer var4 = checkValueAttribut((XmlNode) var3, (String) "min", (LogPrinter) logger);
        if (var4 != null) {
            var2.setMinimum(var4.intValue());
        }

        var4 = checkValueAttribut((XmlNode) var3, (String) "max", (LogPrinter) logger);
        if (var4 != null) {
            var2.setMaximum(var4.intValue());
        }

        var4 = checkValueAttribut((XmlNode) var3, (String) "value", (LogPrinter) logger);
        if (var4 != null) {
            var2.setValue(var4.intValue());
        }

        var4 = checkValueAttribut((XmlNode) var3, (String) "major-ticks", (LogPrinter) logger);
        if (var4 != null) {
            var2.setBlockIncrement(var4.intValue());
        }

        var4 = checkValueAttribut((XmlNode) var3, (String) "minor-ticks", (LogPrinter) logger);
        if (var4 != null) {
            var2.setUnitIncrement(var4.intValue());
        }
    }
}
