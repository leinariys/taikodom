package all;

import javax.swing.*;
import java.awt.*;

public class SplitCustomWrapper extends JSplitPane implements IContainerCustomWrapper {

    private a bJg = new a(this);

    public void destroy() {
        if (this.getParent() != null) {
            this.getParent().remove(this);
        }

    }

    public JButton findJButton(String var1) {
        Component var2 = this.getLeftComponent();
        if (var2 instanceof JButton && var1.equals(var2.getName())) {
            return (JButton) var2;
        } else {
            JButton var3;
            if (var2 instanceof IContainerCustomWrapper) {
                var3 = ((IContainerCustomWrapper) var2).findJButton(var1);
                if (var3 != null) {
                    return var3;
                }
            }

            var2 = this.getRightComponent();
            if (var2 instanceof JButton && var1.equals(var2.getName())) {
                return (JButton) var2;
            } else {
                if (var2 instanceof IContainerCustomWrapper) {
                    var3 = ((IContainerCustomWrapper) var2).findJButton(var1);
                    if (var3 != null) {
                        return var3;
                    }
                }

                return null;
            }
        }
    }

    public JComboBox findJComboBox(String var1) {
        Component var2 = this.getLeftComponent();
        if (var2 instanceof JComboBox && var1.equals(var2.getName())) {
            return (JComboBox) var2;
        } else {
            JComboBox var3;
            if (var2 instanceof IContainerCustomWrapper) {
                var3 = ((IContainerCustomWrapper) var2).findJComboBox(var1);
                if (var3 != null) {
                    return var3;
                }
            }

            var2 = this.getRightComponent();
            if (var2 instanceof JComboBox && var1.equals(var2.getName())) {
                return (JComboBox) var2;
            } else {
                if (var2 instanceof IContainerCustomWrapper) {
                    var3 = ((IContainerCustomWrapper) var2).findJComboBox(var1);
                    if (var3 != null) {
                        return var3;
                    }
                }

                return null;
            }
        }
    }

    public Component findJComponent(String var1) {
        Component var2 = this.getLeftComponent();
        if (var1.equals(var2.getName())) {
            return var2;
        } else {
            Component var3;
            if (var2 instanceof IContainerCustomWrapper) {
                var3 = ((IContainerCustomWrapper) var2).findJComponent(var1);
                if (var3 != null) {
                    return var3;
                }
            }

            var2 = this.getRightComponent();
            if (var1.equals(var2.getName())) {
                return var2;
            } else {
                if (var2 instanceof IContainerCustomWrapper) {
                    var3 = ((IContainerCustomWrapper) var2).findJComponent(var1);
                    if (var3 != null) {
                        return var3;
                    }
                }

                return null;
            }
        }
    }

    public IContainerCustomWrapper ce(String var1) {
        Component var2 = this.getLeftComponent();
        if (var2 instanceof IContainerCustomWrapper && var1.equals(var2.getName())) {
            return (IContainerCustomWrapper) var2;
        } else {
            IContainerCustomWrapper var3;
            if (var2 instanceof IContainerCustomWrapper) {
                var3 = ((IContainerCustomWrapper) var2).ce(var1);
                if (var3 != null) {
                    return var3;
                }
            }

            var2 = this.getRightComponent();
            if (var2 instanceof IContainerCustomWrapper && var1.equals(var2.getName())) {
                return (IContainerCustomWrapper) var2;
            } else {
                if (var2 instanceof IContainerCustomWrapper) {
                    var3 = ((IContainerCustomWrapper) var2).ce(var1);
                    if (var3 != null) {
                        return var3;
                    }
                }

                return null;
            }
        }
    }

    public ProgressBarCustomWrapper findJProgressBar(String var1) {
        Component[] var5;
        int var4 = (var5 = this.getComponents()).length;

        for (int var3 = 0; var3 < var4; ++var3) {
            Component var2 = var5[var3];
            if (var2 instanceof ProgressBarCustomWrapper && var1.equals(var2.getName())) {
                return (ProgressBarCustomWrapper) var2;
            }

            if (var2 instanceof IContainerCustomWrapper) {
                ProgressBarCustomWrapper var6 = ((IContainerCustomWrapper) var2).findJProgressBar(var1);
                if (var6 != null) {
                    return var6;
                }
            }
        }

        return null;
    }

    public JLabel findJLabel(String var1) {
        Component var2 = this.getLeftComponent();
        if (var2 instanceof JLabel && var1.equals(var2.getName())) {
            return (JLabel) var2;
        } else {
            JLabel var3;
            if (var2 instanceof IContainerCustomWrapper) {
                var3 = ((IContainerCustomWrapper) var2).findJLabel(var1);
                if (var3 != null) {
                    return var3;
                }
            }

            var2 = this.getRightComponent();
            if (var2 instanceof JLabel && var1.equals(var2.getName())) {
                return (JLabel) var2;
            } else {
                if (var2 instanceof IContainerCustomWrapper) {
                    var3 = ((IContainerCustomWrapper) var2).findJLabel(var1);
                    if (var3 != null) {
                        return var3;
                    }
                }

                return null;
            }
        }
    }

    public RepeaterCustomWrapper ch(String var1) {
        Component[] var5;
        int var4 = (var5 = this.getComponents()).length;

        for (int var3 = 0; var3 < var4; ++var3) {
            Component var2 = var5[var3];
            if (var2 instanceof IContainerCustomWrapper) {
                RepeaterCustomWrapper var6 = ((IContainerCustomWrapper) var2).ch(var1);
                if (var6 != null) {
                    return var6;
                }
            }
        }

        return null;
    }

    public TextfieldCustomWrapper findJTextField(String var1) {
        Component var2 = this.getLeftComponent();
        if (var2 instanceof TextfieldCustomWrapper && var1.equals(var2.getName())) {
            return (TextfieldCustomWrapper) var2;
        } else {
            TextfieldCustomWrapper var3;
            if (var2 instanceof IContainerCustomWrapper) {
                var3 = ((IContainerCustomWrapper) var2).findJTextField(var1);
                if (var3 != null) {
                    return var3;
                }
            }

            var2 = this.getRightComponent();
            if (var2 instanceof TextfieldCustomWrapper && var1.equals(var2.getName())) {
                return (TextfieldCustomWrapper) var2;
            } else {
                if (var2 instanceof IContainerCustomWrapper) {
                    var3 = ((IContainerCustomWrapper) var2).findJTextField(var1);
                    if (var3 != null) {
                        return var3;
                    }
                }

                return null;
            }
        }
    }

    public void pack() {
        this.setSize(this.getPreferredSize());
        this.validate();
    }

    public String getElementName() {
        return "splitpane";
    }

    public void setEnabled(boolean var1) {
        Component[] var5;
        int var4 = (var5 = this.getComponents()).length;

        for (int var3 = 0; var3 < var4; ++var3) {
            Component var2 = var5[var3];
            var2.setEnabled(var1);
        }

        super.setEnabled(var1);
    }

    public void setFocusable(boolean var1) {
        Component[] var5;
        int var4 = (var5 = this.getComponents()).length;

        for (int var3 = 0; var3 < var4; ++var3) {
            Component var2 = var5[var3];
            var2.setFocusable(var1);
        }

        super.setFocusable(var1);
    }

    public void Kk() {
        this.bJg.Kk();
    }

    public void Kl() {
        this.bJg.Kl();
    }
}
