package all;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Co {
    private static Comparator fLj = new Comparator() {
        public int compare(Object var1, Object var2) {
            int var3 = var1 instanceof a ? ((a) var1).value : ((float[]) var1).length;
            int var4 = var2 instanceof a ? ((a) var2).value : ((float[]) var2).length;
            return var3 > var4 ? 1 : (var3 < var4 ? -1 : 0);
        }
    };
    private static Comparator fLk = new Comparator() {
        public int compare(Object var1, Object var2) {
            int var3 = var1 instanceof a ? ((a) var1).value : ((Object[]) var1).length;
            int var4 = var2 instanceof a ? ((a) var2).value : ((Object[]) var2).length;
            return var3 > var4 ? 1 : (var3 < var4 ? -1 : 0);
        }
    };
    private Class fLh;
    private ArrayList list = new ArrayList();
    private Comparator comparator;
    private a fLi = new a((a) null);

    public Co(Class var1) {
        this.fLh = var1;
        if (var1 == Float.TYPE) {
            this.comparator = fLj;
        } else {
            if (var1.isPrimitive()) {
                throw new UnsupportedOperationException("unsupported type " + var1);
            }

            this.comparator = fLk;
        }

    }

    private Object create(int var1) {
        return Array.newInstance(this.fLh, var1);
    }

    public Object qW(int var1) {
        this.fLi.value = var1;
        int var2 = Collections.binarySearch(this.list, this.fLi, this.comparator);
        return var2 < 0 ? this.create(var1) : this.list.remove(var2);
    }

    public Object qX(int var1) {
        this.fLi.value = var1;
        int var2 = Collections.binarySearch(this.list, this.fLi, this.comparator);
        if (var2 < 0) {
            var2 = -var2 - 1;
            return var2 < this.list.size() ? this.list.remove(var2) : this.create(var1);
        } else {
            return this.list.remove(var2);
        }
    }

    public void release(Object var1) {
        int var2 = Collections.binarySearch(this.list, var1, this.comparator);
        if (var2 < 0) {
            var2 = -var2 - 1;
        }

        this.list.add(var2, var1);
        if (this.comparator == fLk) {
            Object[] var3 = (Object[]) var1;

            for (int var4 = 0; var4 < var3.length; ++var4) {
                var3[var4] = null;
            }
        }

    }

    private static class a {
        public int value;

        private a() {
        }

        // $FF: synthetic method
        a(a var1) {
            this();
        }
    }
}
