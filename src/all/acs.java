package all;

/**
 * Блакировщик потока доступ к графическому движку
 * class Ix
 */
public class acs {
    private static ThreadLocal fbD;
    private static adE fbE = null;
    private static boolean fbF = false;

    public static adE ald() {
        return fbD == null ? fbE : (adE) fbD.get();
    }

    public static void eb(boolean var0) {
        fbF = var0;
    }

    /**
     * @param var0 ClientMain this.vS
     */
    public static void a(adE var0) {
        if (!(var0 instanceof aGf) && fbF) {
            if (fbD == null) {
                fbD = new ThreadLocal();
            }

            fbD.set(var0);
            fbE = var0;
        } else {
            fbE = var0;
        }

    }

    /**
     * @deprecated
     */
    @Deprecated
    public static void am(String var0, String var1) {
        ald().getEventManager().h(new aeq(var0, var1));
    }
}
