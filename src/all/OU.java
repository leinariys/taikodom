package all;

import java.io.Serializable;

public class OU implements aIb, Serializable {
    private String _lang;

    public OU(String var1) {
        this._lang = var1;
    }

    public short getConditionType() {
        return 6;
    }

    public String getLang() {
        return this._lang;
    }

    public String toString() {
        return ":lang(" + this.getLang() + ")";
    }
}
