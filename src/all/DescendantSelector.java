package all;

import java.io.Serializable;

public class DescendantSelector implements IDescendantSelector, Serializable {
    private ISelector clG;
    private ISimpleSelector bek;

    public DescendantSelector(ISelector var1, ISimpleSelector var2) {
        this.clG = var1;
        this.bek = var2;
    }

    public short getSelectorType() {
        return 10;
    }

    public ISelector getAncestorSelector() {
        return this.clG;
    }

    public ISimpleSelector getSimpleSelector() {
        return this.bek;
    }

    public String toString() {
        return this.getAncestorSelector().toString() + " " + this.getSimpleSelector().toString();
    }
}
