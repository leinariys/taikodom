package all;

import java.util.HashMap;
import java.util.Map;

public enum axS {
    gRn,
    gRo,
    gRp,
    gRq,
    gRr;

    static final Map gRs = new HashMap();

    static {
        gRs.put("inherit", gRn);
        gRs.put("static", gRo);
        gRs.put("absolute", gRp);
        gRs.put("fixed", gRq);
        gRs.put("relative", gRr);
    }

    public static axS kE(String var0) {
        return (axS) gRs.get(var0);
    }
}
