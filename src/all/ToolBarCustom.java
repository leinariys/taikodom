package all;

import javax.swing.*;
import java.awt.*;

public class ToolBarCustom extends BaseItem {

    /**
     * Создание базового Component интерфейса
     * Вызывается из class BaseItem
     *
     * @param var1
     * @param var2
     * @param var3
     * @return
     */
    @Override
    protected JComponent CreatComponentCustom(MapKeyValue var1, Container var2, XmlNode var3) {
        return new JToolBar();
    }

    protected void a(MapKeyValue var1, JToolBar var2, XmlNode var3) {
        super.applyAttribute(var1, (Component) var2, (XmlNode) var3);
        if ("vertical".equals(var3.getAttribute("orientation"))) {
            var2.setOrientation(1);
        }

    }

    public static class a extends BaseItem {
        /**
         * Создание базового Component интерфейса
         * Вызывается из class BaseItem
         *
         * @param var1
         * @param var2
         * @param var3
         * @return
         */
        @Override
        protected JComponent CreatComponentCustom(MapKeyValue var1, Container var2, XmlNode var3) {
            return new JLabel();
        }

    }
}
