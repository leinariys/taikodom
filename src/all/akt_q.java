package all;

public interface akt_q extends Condition {
    String getNamespaceURI();

    String getLocalName();

    boolean getSpecified();

    String getValue();
}
