package all;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicTableHeaderUI;
import javax.swing.table.JTableHeader;
import java.awt.*;

/**
 * TableHeaderUI
 */
public class TableHeaderUI extends BasicTableHeaderUI implements aIh {
    private ComponentManager Rp;

    private TableHeaderUI(JTableHeader var1) {
        this.Rp = new ComponentManager("table-header", var1);
        var1.setOpaque(false);
        var1.setBackground(new Color(0, 0, 0, 0));
    }

    public static ComponentUI createUI(JComponent var0) {
        var0.setBorder(new BorderWrapper());
        var0.setBackground((Color) null);
        return new TableHeaderUI((JTableHeader) var0);
    }

    public void installUI(JComponent var1) {
        this.header = (JTableHeader) var1;
        this.rendererPane = new aqV("cell");
        this.header.add(this.rendererPane);
        this.installListeners();
        this.installKeyboardActions();
    }

    /**
     * TODO похлже это public pv wy()
     *
     * @return Графический элемент
     */
    @Override
    public aeK wz() {
        return this.Rp;
    }

    public Dimension getPreferredSize(JComponent var1) {
        RM.a((aIh) this, (JComponent) var1);
        return this.wz().c(var1, super.getPreferredSize(var1));
    }

    public Dimension getMinimumSize(JComponent var1) {
        RM.a((aIh) this, (JComponent) var1);
        return this.wz().b(var1, super.getMinimumSize(var1));
    }

    public Dimension getMaximumSize(JComponent var1) {
        RM.a((aIh) this, (JComponent) var1);
        return this.wz().a(var1, super.getMaximumSize(var1));
    }

    public void update(Graphics var1, JComponent var2) {
        RM.a((aIh) this, (Graphics) var1, (JComponent) var2);
        this.paint(var1, var2);
    }
}
