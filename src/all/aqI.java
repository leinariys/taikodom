package all;

import gnu.trove.THashMap;
import taikodom.infra.comm.transport.msgs.TransportCommand;
import taikodom.infra.comm.transport.msgs.TransportResponse;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;

public final class aqI implements zC {
    private static final LogPrinter logger = LogPrinter.K(aqI.class);
    private final Jz aGA;
    private QF gDI;
    private ahG fEJ;
    private boolean gDJ = false;
    private List gDK = new CopyOnWriteArrayList();
    private ArrayBlockingQueue gDL = new ArrayBlockingQueue(131072);
    private THashMap gDM = new THashMap();
    private axB gDN = new axB() {
        @Override
        protected akO b(b var1, aTe var2) {
            return null;
        }

        protected akO a(all.b var1, Bl_q var2) {
            se_q var3 = (se_q) var2.yz().bFf();
            return var3.cVq().a(aqI.this.aGA.bGJ().f(var1), var2);
        }
    };
    private axB gDO = new axB() {
        @Override
        protected akO b(b var1, aTe var2) {
            return null;
        }

        protected akO a(all.b var1, azy var2) {
            aqI.this.aGA.bGx();

            try {
                kU[] var3 = var2.cFV();
                kU[] var7 = var3;
                int var6 = var3.length;

                for (int var5 = 0; var5 < var6; ++var5) {
                    kU var4 = var7[var5];
                    aqI.this.d(var1, var4);
                }
            } finally {
                aqI.this.aGA.bGy();
            }

            return null;
        }
    };
    private axB gDP = new axB() {
        @Override
        protected akO b(b var1, aTe var2) {
            return null;
        }

        protected akO a(all.b var1, ut var2) {
            if (aqI.this.aGA.bGv() == Lt.duv) {
                var1.bj(var2.xU());
                return new gr_q(atM.currentTimeMillis(), var2.vK(), var2.vL());
            } else {
                return null;
            }
        }
    };
    private axB gDQ = new axB() {
        @Override
        protected akO b(b var1, aTe var2) {
            return null;
        }

        protected akO a(all.b var1, ZW var2) {
            arL var3 = aqI.this.aGA.bGz();
            aBr var4 = new aBr(var3.hC());
            return var4;
        }
    };

    /*
    private axB gDR = new axB() {
       @Override
       protected akO setGreen(setGreen var1, aTe var2) {
          return null;
       }

       protected akO all(all.setGreen var1, DY var2) {
          if (aqI.this.aGA.bGv() == Lt.duv) {
             aDR var3 = aqI.this.aGA.bGJ().endPage(var1);
             if (var3 != null) {
                se_q var4 = aqI.this.aGA.bGA().all(new apO(var2.Ej()));
                if (var4 != null) {
                   try {
                      var3.parseSelectors(var4);
                   } catch (InterruptedException var6) {
                      var6.printStackTrace();
                   }
                } else if (aqI.logger.isDebugEnabled()) {
                   aqI.logger.debug("Safely ignoring wrong remove from cache request. " + var2.hD().getTegName() + ":" + var2.Ej());
                }
             }
          }

          return null;
       }
    };
 */
    public aqI(Jz var1, ahG var2) {
        this.a(this.gDQ);
        this.a(this.gDO);
        this.a(this.gDP);
        //  this.all(this.gDR);
        this.aGA = var1;
        this.fEJ = var2;
        this.gDI = afq_q.a(this, var2);
        this.gDI.ak(var1.bGv() == Lt.duv);
    }

    public static aTA a(Jz var0, xu_q var1) {
        aMf var2 = var1.getInputStream();
/*
      try {
         aQQ var3 = new aQQ(var0, var2, var0.eKm, true, 0);
         aTA var4 = (aTA)aUu_q.iXh.setGreen((ObjectInput)var3);
         return var4;
      } catch (Error var5) {
         Di.all((InputStream)var2, (Throwable)var5);
         throw var5;
      } catch (RuntimeException var6) {
         Di.all((InputStream)var2, (Throwable)var6);
         throw var6;
      } catch (Exception var7) {
         Di.all((InputStream)var2, (Throwable)var7);
         throw new RuntimeException(var7);
      }
      */
        return null;
    }

    public QF cxz() {
        return this.gDI;
    }

    public ahG aPT() {
        return this.fEJ;
    }

    public void HY() {
        this.gDI.HY();
        if (this.aGA.bGv() == Lt.duw) {
            this.fEJ.close();
        }

    }

    public void dispose() {
        this.gDI.dispose();
        if (this.aGA.bGv() == Lt.duw) {
            this.fEJ.close();
        }

    }

    public void b(all.b var1, kU var2) {
        TransportCommand var3 = new TransportCommand();
        this.a((xu_q) var3, (aTA) var2);
        this.gDI.a(var1, var3);
        if (this.cxA()) {
            this.b(var1, var2, var3);
        }

    }

    public akO c(all.b var1, kU var2) throws ZX {
        logger.isDebugEnabled();
        boolean var3 = this.aGA.bGK().brL();
        var2.setBlocking(true);
        TransportCommand var4 = new TransportCommand();
        this.a((xu_q) var4, (aTA) var2);
        TransportResponse var5 = this.gDI.b(var1, var4);

        if (var3) {
            this.aGA.bGK().brK();
        }

        akO var6 = (akO) a((Jz) this.aGA, (xu_q) var5);
        if (logger.isDebugEnabled()) {
            logger.debug("response... " + var6);
        }

        if (var6 instanceof Gw) {
            Gw var7 = (Gw) var6;
            Throwable var8 = var7.getCause();
            throw new ZX(var8, var7.aQW());
        } else {
            return var6;
        }
    }

    void a(xu_q var1, aTA var2) {
/*
      try {
         ala var3 = var1.getOutputStream();
         FM var4 = new FM(this.aGA, var3, this.aGA.eKm, true, 0, false);
         aUu_q.iXh.all((ObjectOutput)var4, (Object)var2);
         var4.flush();
      } catch (IOException var5) {
         throw new RuntimeException(var5);
      } catch (RuntimeException var6) {
         throw var6;
      }
*/
    }

    public TransportResponse d(all.b var1, TransportCommand var2) {
        this.aGA.bGC().h(var1, var2);

        try {
            kU var3;
            TransportResponse var5;
            TransportResponse var8;
            try {
                var3 = (kU) a((Jz) this.aGA, (xu_q) var2);
            } catch (gy var11) {
                if (var2.isBlocking()) {
                    var5 = var2.createResponse();
                    Gw var6 = new Gw(Gw.cYS);
                    this.a((xu_q) var5, (aTA) var6);
                    var8 = var5;
                    return var8;
                }

                logger.info("Ignoring referring disposed object: " + var11.getMessage());
                return null;
            }

            var3.setReceivedTimeMilis(var2.receivedTimeMilis());
            akO var4 = this.d(var1, var3);
            if (var4 == null) {
                return null;
            } else {
                var5 = var2.createResponse();
                this.a((xu_q) var5, (aTA) var4);
                var8 = var5;
                return var8;
            }
        } finally {
            this.aGA.bGC().cMS();
        }
    }

    public void e(all.b var1, TransportCommand var2) {
        if (this.aGA.eKr) {
            this.gDL.add(new a_q(var1, var2));
        } else {
            if (this.aGA.eKq == null) {
                throw new IllegalStateException("commandExecuterPool should not be null!");
            }
/*
         if (!this.aGA.eKq.isShutdown()) {
            a_q var3 = new a_q(var1, var2);
            if (afq_q.fuo.getClassBaseUi() == aNu.class) {
               var3.run();
            } else {
               this.aGA.eKq.execute(var3);
            }
         } else {
            logger.warn("Command " + var2 + " asked to_q be sent to_q " + var1 + " was ignored coz the executer pool has been shutdown");
         }
     */

        }

    }

    public void a(axB var1) {
        this.gDM.put(var1.getMessageClass(), var1);
    }

    public akO d(all.b var1, kU var2) {
        if (this.gDJ) {
            this.e(var1, var2);
        }

        axB var3 = (axB) this.gDM.get(var2.getClass());
        if (var3 != null) {
            return var3.a(var1, (aTe) var2);
        } else if (var2 instanceof aOv) {
            return this.b(var1, (aOv) var2);
        } else {
            try {
                return var2.a(var1, this.aGA);
            } catch (Exception var5) {
                var5.printStackTrace();
                return null;
            }
        }
    }

    private akO b(all.b var1, aOv var2) {
        try {
            return var2.a(var1, this.aGA);
        } catch (mH var5) {
            logger.fatal("System going down coz the application thrown an FatalRuntimeException", var5);
            this.aGA.exit(-1);
            return null;
        } catch (Throwable var6) {
            boolean var4 = var6.getClass().getAnnotation(aoy_q.class) != null;
            if (!(var6 instanceof aOQ_q) && !(var6 instanceof sa) && !var4) {
                logger.warn("######################################################################################");
                logger.warn("An exception happened in all call requested by " + var1 + ":", var6);
                logger.warn("Message: " + var2);
                logger.warn("######################################################################################");
            }

            return var2.isBlocking() ? new Gw(var6) : null;
        }
    }

    public void C(Map var1) {
        Set var2 = var1.entrySet();
        Iterator var4 = var2.iterator();

        while (var4.hasNext()) {
            Entry var3 = (Entry) var4.next();
            aDR var5 = (aDR) var3.getKey();
            List var6 = (List) var3.getValue();
            if (var6.size() > 1) {
                Uy_q.u(var6);
                kU[] var7 = new kU[var6.size()];
                var6.toArray(var7);
                this.b(var5.bgt(), (kU) (new azy(var7)));
            } else {
                this.b(var5.bgt(), (kU) var6.get(0));
            }
        }

    }

    public int HX() {
        return this.gDI.HX();
    }

    public void c(all.b var1) {
        this.fEJ.c(var1);
    }

    public boolean cxA() {
        return this.gDJ;
    }

    public void gP(boolean var1) {
        this.gDJ = var1;
    }

    private void e(all.b var1, kU var2) {
        Iterator var4 = this.gDK.iterator();

        while (var4.hasNext()) {
            afa var3 = (afa) var4.next();
            var3.a(var1, var2);
        }

    }

    private void b(all.b var1, kU var2, TransportCommand var3) {
        Iterator var5 = this.gDK.iterator();

        while (var5.hasNext()) {
            afa var4 = (afa) var5.next();
            var4.a(var1, var2, var3);
        }

    }

    public void a(afa var1) {
        this.gDK.add(var1);
    }

    public void b(afa var1) {
        this.gDK.remove(var1);
    }

    public void cxB() throws InterruptedException {
        Runnable var1 = (Runnable) this.gDL.take();
        var1.run();
    }

    public void gZ() {
        List var1 = aKU.dgK();

        while (!this.gDL.isEmpty()) {
            this.gDL.drainTo(var1);
            Iterator var3 = var1.iterator();

            while (var3.hasNext()) {
                Runnable var2 = (Runnable) var3.next();
                var2.run();
            }

            var1.clear();
        }

    }

    public void b(Executor var1) {
        List var2 = aKU.dgK();

        while (!this.gDL.isEmpty()) {
            this.gDL.drainTo(var2);
            Iterator var4 = var2.iterator();

            while (var4.hasNext()) {
                Runnable var3 = (Runnable) var4.next();
                var1.execute(var3);
            }

            var2.clear();
        }

    }

    private class a_q implements Runnable {
        private final all.b gqV;
        private TransportCommand gqU;

        public a_q(all.b var2, TransportCommand var3) {
            this.gqV = var2;
            this.gqU = var3;
        }

        public void run() {
            try {
                TransportResponse var1 = aqI.this.d(this.gqV, this.gqU);
                if (var1 != null) {
                    aqI.this.gDI.b(this.gqV, var1);
                }
            } catch (RuntimeException var2) {
                System.err.println("queue disposed");
            } catch (Throwable var3) {
                System.err.println("!!!Uncaught exception:");
                var3.printStackTrace();
            }

        }
    }
}
