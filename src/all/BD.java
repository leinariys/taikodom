package all;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * class test_Penel
 */
public class BD extends JDialog implements ActionListener {
    private static BD cnO;
    private static String value = "";
    private JList list;

    /**
     * @param var1
     * @param var2
     * @param var3
     * @param var4
     * @param var5
     * @param var6
     * @param var7
     */
    private BD(Frame var1, Component var2, String var3, String var4, Object[] var5, String var6, String var7) {
        super(var1, var4, true);
        JButton var8 = new JButton("Cancel");
        var8.addActionListener(this);
        final JButton var9 = new JButton("Set");
        var9.setActionCommand("Set");
        var9.addActionListener(this);
        this.getRootPane().setDefaultButton(var9);
        this.list = new JList(var5) {
            public int getScrollableUnitIncrement(Rectangle var1, int var2, int var3) {
                int var4;
                if (var2 == 1 && var3 < 0 && (var4 = this.getFirstVisibleIndex()) != -1) {
                    Rectangle var5 = this.getCellBounds(var4, var4);
                    if (var5.y == var1.y && var4 != 0) {
                        Point var6 = var5.getLocation();
                        --var6.y;
                        int var7 = this.locationToIndex(var6);
                        Rectangle var8 = this.getCellBounds(var7, var7);
                        if (var8 != null && var8.y < var5.y) {
                            return var8.height;
                        }

                        return 0;
                    }
                }

                return super.getScrollableUnitIncrement(var1, var2, var3);
            }
        };
        this.list.setSelectionMode(1);
        if (var7 != null) {
            this.list.setPrototypeCellValue(var7);
        }

        this.list.setLayoutOrientation(2);
        this.list.setVisibleRowCount(-1);
        this.list.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent var1) {
                if (var1.getClickCount() == 2) {
                    var9.doClick();
                }

            }
        });
        JScrollPane var10 = new JScrollPane(this.list);
        var10.setPreferredSize(new Dimension(250, 80));
        var10.setAlignmentX(0.0F);
        JPanel var11 = new JPanel();
        var11.setLayout(new BoxLayout(var11, 3));
        JLabel var12 = new JLabel(var3);
        var12.setLabelFor(this.list);
        var11.add(var12);
        var11.add(Box.createRigidArea(new Dimension(0, 5)));
        var11.add(var10);
        var11.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        JPanel var13 = new JPanel();
        var13.setLayout(new BoxLayout(var13, 2));
        var13.setBorder(BorderFactory.createEmptyBorder(0, 10, 10, 10));
        var13.add(Box.createHorizontalGlue());
        var13.add(var8);
        var13.add(Box.createRigidArea(new Dimension(10, 0)));
        var13.add(var9);
        Container var14 = this.getContentPane();
        var14.add(var11, "Center");
        var14.add(var13, "Last");
        this.setValue(var6);
        this.pack();
        this.setLocationRelativeTo(var2);
    }

    /**
     * @param var0
     * @param var1
     * @param var2
     * @param var3
     * @param var4
     * @param var5
     * @param var6
     * @return
     */
    public static String a(Component var0, Component var1, String var2, String var3, String[] var4, String var5, String var6) {
        Frame var7 = JOptionPane.getFrameForComponent(var0);
        cnO = new BD(var7, var1, var2, var3, var4, var5, var6);
        cnO.setVisible(true);
        return value;
    }

    /**
     * @param var1
     */
    private void setValue(String var1) {
        value = var1;
        this.list.setSelectedValue(value, true);
    }

    /**
     * @param var1
     */
    public void actionPerformed(ActionEvent var1) {
        if ("Set".equals(var1.getActionCommand())) {
            value = (String) this.list.getSelectedValue();
        }

        cnO.setVisible(false);
    }
}
