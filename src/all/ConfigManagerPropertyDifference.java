package all;

public class ConfigManagerPropertyDifference extends RuntimeException {

    private String keyProperty;
    private Class typePropertyCurrent;
    private Class typePropertyRequested;

    public ConfigManagerPropertyDifference(Class typePropertyRequested, Class typePropertyCurrent, String keyProperty) {
        this.typePropertyRequested = typePropertyRequested;
        this.typePropertyCurrent = typePropertyCurrent;
        this.keyProperty = keyProperty;
    }

    public final String getKeyProperty() {
        return this.keyProperty;
    }

    public final Class getTypePropertyCurrent() {
        return this.typePropertyCurrent;
    }

    public final Class getTypePropertyRequested() {
        return this.typePropertyRequested;
    }
}
