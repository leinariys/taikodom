package all;

import javax.swing.*;
import java.awt.*;

/**
 *
 */
public class ItemIconCustom extends BaseItem {

    /**
     * Создание базового Component интерфейса
     * Вызывается из class BaseItem
     *
     * @param var1
     * @param var2
     * @param var3
     * @return
     */
    @Override
    protected JComponent CreatComponentCustom(MapKeyValue var1, Container var2, XmlNode var3) {
        return new ItemIconCustomWrapper();
    }

    protected void a(MapKeyValue var1, ItemIconCustomWrapper var2, XmlNode var3) {
        super.applyAttribute(var1, (Component) var2, (XmlNode) var3);
        String var4 = var3.getAttribute("src");
        if (var4 != null) {
            var2.aw(var4);
        }

    }

}
