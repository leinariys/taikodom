package all;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class Gw extends akO {

    public static int cYS = 1;
    private Throwable cause;
    private int cYR;

    public Gw() {
    }

    public Gw(int var1) {
        this.cYR = var1;
    }

    public Gw(Throwable var1) {
        this.cause = var1;
    }

    public Throwable getCause() {
        return this.cause;
    }

    public void readExternal(ObjectInput var1) throws IOException, ClassNotFoundException {
        this.cYR = var1.readInt();
        if (var1.readBoolean()) {
            this.cause = (Throwable) var1.readObject();
        }

    }

    public void writeExternal(ObjectOutput var1) throws IOException {
        var1.writeInt(this.cYR);
        if (this.cause != null) {
            var1.writeBoolean(true);
            var1.writeObject(this.cause);
        } else {
            var1.writeBoolean(false);
        }

    }

    public int aQW() {
        return this.cYR;
    }

    public void hq(int var1) {
        this.cYR = var1;
    }
}
