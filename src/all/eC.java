package all;

public class eC {
    private static final LogPrinter logger = LogPrinter.K(eC.class);
    private static final long Dp = 5000L;
    protected volatile long Dq = this.mh();
    protected volatile long Dr = this.mg();
    protected volatile long Ds;
    protected volatile long Dt;

    public long currentTimeMillis() {
        long var1 = this.mh();
        long var3 = var1 - this.Ds;
        long var5;
        if (Math.abs(var3) > 5000L) {
            var1 = this.mh();
            var5 = this.mg();
            synchronized (this) {
                long var8 = (var5 - this.Dr) / 1000000L;
                long var10 = var1 - this.Dq + this.Dt;
                var3 = var8 - var10;
                if (Math.abs(var3) > 20000L) {
                    this.Dt += var3;
                    if (this.isVerbose()) {
                        logger.warn("Adjusting time offset: " + var3 + " ms");
                    }
                }

                this.Dr = (long) ((double) this.Dr + (double) var8 * 1000000.0D);
                this.Dq += var8;
            }

            var1 = this.mh();
        }

        this.Ds = var1;
        var5 = var1 + this.Dt;
        return var5;
    }

    protected boolean isVerbose() {
        return true;
    }

    protected long mg() {
        return System.nanoTime();
    }

    protected long mh() {
        return System.currentTimeMillis();
    }

    protected long mi() {
        return 0L;
    }
}
