package all;

import com.hoplon.geometry.Vec3f;

import javax.vecmath.Quat4f;
import javax.vecmath.Tuple3f;

public class OrientationBase extends Quat4f implements afA_q {
    private static final double PI_180 = 0.017453292519943295D;

    public static float ANGLE_PRECISION = 5.0E-5F;
    public static float TRACE_PRECISION = 1.0E-5F;

    public OrientationBase() {
        this.w = 1.0F;
        this.x = this.y = this.z = 0.0F;
    }

    public OrientationBase(OrientationBase var1) {
        super(var1);
    }

    public OrientationBase(float[] var1) {
        super(var1);
    }

    public OrientationBase(float var1, float var2, float var3, float var4) {
        super(var1, var2, var3, var4);
    }

    protected OrientationBase(Vec3f var1, double var2) {
        var2 /= 2.0D;
        double var4 = Math.sin(var2);
        double var6 = Math.cos(var2);
        var1 = new Vec3f(var1);
        var1.normalize();
        this.w = (float) var6;
        this.x = (float) ((double) var1.x * var4);
        this.y = (float) ((double) var1.y * var4);
        this.z = (float) ((double) var1.z * var4);
    }

    public OrientationBase(double var1, double var3, double var5) {
        this.h(var1, var3, var5);
    }

    public OrientationBase(ajK var1) {
        this.set(var1);
    }

    public OrientationBase(ajD var1) {
        this.e(var1);
    }

    public static OrientationBase c(Vec3f var0, double var1) {
        return new OrientationBase(var0, var1);
    }

    public static OrientationBase d(Vec3f var0, double var1) {
        return new OrientationBase(var0, var1 * PI_180);
    }

    public static OrientationBase d(double var0, double var2, double var4) {
        return new OrientationBase(var0, var2, var4);
    }

    public static OrientationBase c(double var0, double var2, double var4) {
        return new OrientationBase(var0 * PI_180, var2 * PI_180, var4 * PI_180);
    }

    public static OrientationBase a(Quat4f var0, Quat4f var1) {
        return (new OrientationBase()).b(var0, var1);
    }

    public static void a(OrientationBase var0, Vec3f var1, Vec3f var2) {
        float var3 = var1.x;
        float var4 = var1.y;
        float var5 = var1.z;
        if (isZero(var3) && isZero(var4) && isZero(var5)) {
            var2.x = var3;
            var2.y = var4;
            var2.z = var5;
        }

        float var6 = var0.x;
        float var7 = var0.z;
        float var8 = var0.y;
        float var9 = var0.w;
        float var10 = -var6 * var3 - var8 * var4 - var7 * var5;
        float var11 = var9 * var3 + var8 * var5 - var7 * var4;
        float var12 = var9 * var4 + var7 * var3 - var6 * var5;
        float var13 = var9 * var5 + var6 * var4 - var8 * var3;
        float var14 = 1.0F / (var9 * var9 + var6 * var6 + var8 * var8 + var7 * var7);
        if ((double) Math.abs((var10 * var9 + var11 * var6 + var12 * var8 + var13 * var7) * var14) > 0.1D) {
            var2.x = var3;
            var2.y = var4;
            var2.z = var5;
        } else {
            var2.x = (var11 * var9 - var10 * var6 - var12 * var7 + var13 * var8) * var14;
            var2.y = (var12 * var9 - var10 * var8 - var13 * var6 + var11 * var7) * var14;
            var2.z = (var13 * var9 - var10 * var7 - var11 * var8 + var12 * var6) * var14;
        }

    }

    public static void b(OrientationBase var0, Vector3dOperations var1, Vector3dOperations var2) {
        double var3 = var1.x;
        double var5 = var1.y;
        double var7 = var1.z;
        if (T(var3) && T(var5) && T(var7)) {
            var2.x = var3;
            var2.y = var5;
            var2.z = var7;
        }

        float var9 = var0.x;
        float var10 = var0.z;
        float var11 = var0.y;
        float var12 = var0.w;
        double var13 = (double) (-var9) * var3 - (double) var11 * var5 - (double) var10 * var7;
        double var15 = (double) var12 * var3 + (double) var11 * var7 - (double) var10 * var5;
        double var17 = (double) var12 * var5 + (double) var10 * var3 - (double) var9 * var7;
        double var19 = (double) var12 * var7 + (double) var9 * var5 - (double) var11 * var3;
        float var21 = 1.0F / (var12 * var12 + var9 * var9 + var11 * var11 + var10 * var10);
        if (Math.abs((var13 * (double) var12 + var15 * (double) var9 + var17 * (double) var11 + var19 * (double) var10) * (double) var21) > 0.1D) {
            var2.x = var3;
            var2.y = var5;
            var2.z = var7;
        } else {
            var2.x = (var15 * (double) var12 - var13 * (double) var9 - var17 * (double) var10 + var19 * (double) var11) * (double) var21;
            var2.y = (var17 * (double) var12 - var13 * (double) var11 - var19 * (double) var9 + var15 * (double) var10) * (double) var21;
            var2.z = (var19 * (double) var12 - var13 * (double) var10 - var15 * (double) var11 + var17 * (double) var9) * (double) var21;
        }

    }

    private static void b(OrientationBase var0, Vec3f var1, Vec3f var2) {
        float var3 = var1.x;
        float var4 = var1.y;
        float var5 = var1.z;
        if (isZero(var3) && isZero(var4) && isZero(var5)) {
            var2.x = var3;
            var2.y = var4;
            var2.z = var5;
        }

        float var6 = var0.w;
        float var7 = -var0.x;
        float var8 = -var0.y;
        float var9 = -var0.z;
        float var10 = -var7 * var3 - var8 * var4 - var9 * var5;
        float var11 = var6 * var3 + var8 * var5 - var9 * var4;
        float var12 = var6 * var4 + var9 * var3 - var7 * var5;
        float var13 = var6 * var5 + var7 * var4 - var8 * var3;
        float var14 = 1.0F / (var6 * var6 + var7 * var7 + var8 * var8 + var9 * var9);
        if ((double) Math.abs((var10 * var6 + var11 * var7 + var12 * var8 + var13 * var9) * var14) > 0.1D) {
            var2.x = var3;
            var2.y = var4;
            var2.z = var5;
        } else {
            var2.x = (var11 * var6 - var10 * var7 - var12 * var9 + var13 * var8) * var14;
            var2.y = (var12 * var6 - var10 * var8 - var13 * var7 + var11 * var9) * var14;
            var2.z = (var13 * var6 - var10 * var9 - var11 * var8 + var12 * var7) * var14;
        }

    }

    public static OrientationBase a(OrientationBase var0, OrientationBase var1, float var2, OrientationBase var3) {
        var0 = var0.zb();
        var1 = var1.zb();
        double var4 = (double) (var0.w * var1.w + var0.x * var1.x + var0.y * var1.y + var0.z * var1.z);
        if (var4 < 0.0D) {
            var1 = new OrientationBase(-var1.x, -var1.y, -var1.z, -var1.w);
            var4 = -var4;
        }

        float var6;
        float var7;
        if (var4 > 0.9998999834060669D) {
            var6 = 1.0F - var2;
            var7 = var2;
        } else {
            float var9 = (float) Math.sqrt(1.0D - var4 * var4);
            float var10 = (float) Math.atan2((double) var9, var4);
            float var11 = 1.0F / var9;
            var6 = (float) (Math.sin((double) ((1.0F - var2) * var10)) * (double) var11);
            var7 = (float) (Math.sin((double) (var2 * var10)) * (double) var11);
        }

        var3.set(var0.x * var6 + var1.x * var7, var0.y * var6 + var1.y * var7, var0.z * var6 + var1.z * var7, var0.w * var6 + var1.w * var7);
        var3.normalize();
        return var3;
    }

    public static boolean isZero(float var0) {
        return var0 < TRACE_PRECISION && var0 > -TRACE_PRECISION;
    }

    static boolean T(double var0) {
        return var0 < 9.999999747378752E-6D && var0 > -9.999999747378752E-6D;
    }

    public void h(double var1, double var3, double var5) {
        double var7 = Math.sin(var3 / 2.0D);
        double var9 = Math.cos(var3 / 2.0D);
        double var11 = Math.sin(var5 / 2.0D);
        double var13 = Math.cos(var5 / 2.0D);
        double var15 = Math.sin(var1 / 2.0D);
        double var17 = Math.cos(var1 / 2.0D);
        double var19 = var9 * var13;
        double var21 = var7 * var11;
        this.x = (float) (var15 * var19 - var17 * var21);
        this.y = (float) (var17 * var7 * var13 + var15 * var9 * var11);
        this.z = (float) (var17 * var9 * var11 - var15 * var7 * var13);
        this.w = (float) (var17 * var19 + var15 * var21);
    }

    public void i(double var1, double var3, double var5) {
        this.h(var1 * PI_180, var3 * PI_180, var5 * PI_180);
    }

    public void set(ajK var1) {
        float var2 = 1.0F + var1.m00 + var1.m11 + var1.m22;
        float var3;
        if (var2 > TRACE_PRECISION) {
            var3 = (float) Math.sqrt((double) var2) * 2.0F;
            this.x = (var1.m21 - var1.m12) / var3;
            this.y = (var1.m02 - var1.m20) / var3;
            this.z = (var1.m10 - var1.m01) / var3;
            this.w = 0.25F * var3;
        } else if (var1.m00 > var1.m11 && var1.m00 > var1.m22) {
            var3 = (float) Math.sqrt(1.0D + (double) var1.m00 - (double) var1.m11 - (double) var1.m22) * 2.0F;
            this.x = 0.25F * var3;
            this.y = (var1.m10 + var1.m01) / var3;
            this.z = (var1.m02 + var1.m20) / var3;
            this.w = (var1.m12 - var1.m21) / var3;
        } else if (var1.m11 > var1.m22) {
            var3 = (float) Math.sqrt(1.0D + (double) var1.m11 - (double) var1.m00 - (double) var1.m22) * 2.0F;
            this.x = (var1.m10 + var1.m01) / var3;
            this.y = 0.25F * var3;
            this.z = (var1.m21 + var1.m12) / var3;
            this.w = (var1.m02 - var1.m20) / var3;
        } else {
            var3 = (float) Math.sqrt(1.0D + (double) var1.m22 - (double) var1.m00 - (double) var1.m11) * 2.0F;
            this.x = (var1.m02 + var1.m20) / var3;
            this.y = (var1.m21 + var1.m12) / var3;
            this.z = 0.25F * var3;
            this.w = (var1.m01 - var1.m10) / var3;
        }

        double var4 = this.bhp();
        if (!isZero(1.0F - (float) var4)) {
            double var6 = Math.sqrt(var4);
            this.w = (float) ((double) this.w / var6);
            this.x = (float) ((double) this.x / var6);
            this.y = (float) ((double) this.y / var6);
            this.z = (float) ((double) this.z / var6);
        }

    }

    public void e(ajD var1) {
        float var2 = 1.0F + var1.m00 + var1.m11 + var1.m22;
        float var3;
        if (var2 > TRACE_PRECISION) {
            var3 = (float) Math.sqrt((double) var2) * 2.0F;
            this.x = (var1.m21 - var1.m12) / var3;
            this.y = (var1.m02 - var1.m20) / var3;
            this.z = (var1.m10 - var1.m01) / var3;
            this.w = 0.25F * var3;
        } else if (var1.m00 > var1.m11 && var1.m00 > var1.m22) {
            var3 = (float) Math.sqrt(1.0D + (double) var1.m00 - (double) var1.m11 - (double) var1.m22) * 2.0F;
            this.x = 0.25F * var3;
            this.y = (var1.m10 + var1.m01) / var3;
            this.z = (var1.m02 + var1.m20) / var3;
            this.w = (var1.m12 - var1.m21) / var3;
        } else if (var1.m11 > var1.m22) {
            var3 = (float) Math.sqrt(1.0D + (double) var1.m11 - (double) var1.m00 - (double) var1.m22) * 2.0F;
            this.x = (var1.m10 + var1.m01) / var3;
            this.y = 0.25F * var3;
            this.z = (var1.m21 + var1.m12) / var3;
            this.w = (var1.m02 - var1.m20) / var3;
        } else {
            var3 = (float) Math.sqrt(1.0D + (double) var1.m22 - (double) var1.m00 - (double) var1.m11) * 2.0F;
            this.x = (var1.m02 + var1.m20) / var3;
            this.y = (var1.m21 + var1.m12) / var3;
            this.z = 0.25F * var3;
            this.w = (var1.m01 - var1.m10) / var3;
        }

        double var4 = this.bhp();
        if (!isZero(1.0F - (float) var4)) {
            double var6 = Math.sqrt(var4);
            this.w = (float) ((double) this.w / var6);
            this.x = (float) ((double) this.x / var6);
            this.y = (float) ((double) this.y / var6);
            this.z = (float) ((double) this.z / var6);
        }

    }

    public void q(Vec3f var1, Vec3f var2, Vec3f var3) {
        float var4 = 1.0F + var1.x + var2.y + var3.z;
        float var5;
        if (var4 > TRACE_PRECISION) {
            var5 = (float) Math.sqrt((double) var4) * 2.0F;
            this.x = (var2.z - var3.y) / var5;
            this.y = (var3.x - var1.z) / var5;
            this.z = (var1.y - var2.x) / var5;
            this.w = 0.25F * var5;
        } else if (var1.x > var2.y && var1.x > var3.z) {
            var5 = (float) Math.sqrt(1.0D + (double) var1.x - (double) var2.y - (double) var3.z) * 2.0F;
            this.x = 0.25F * var5;
            this.y = (var1.y + var2.x) / var5;
            this.z = (var3.x + var1.z) / var5;
            this.w = (var3.y - var2.z) / var5;
        } else if (var2.y > var3.z) {
            var5 = (float) Math.sqrt(1.0D + (double) var2.y - (double) var1.x - (double) var3.z) * 2.0F;
            this.x = (var1.y + var2.x) / var5;
            this.y = 0.25F * var5;
            this.z = (var2.z + var3.y) / var5;
            this.w = (var3.x - var1.z) / var5;
        } else {
            var5 = (float) Math.sqrt(1.0D + (double) var3.z - (double) var1.x - (double) var2.y) * 2.0F;
            this.x = (var3.x + var1.z) / var5;
            this.y = (var2.z + var3.y) / var5;
            this.z = 0.25F * var5;
            this.w = (var2.x - var1.y) / var5;
        }

        double var6 = this.bhp();
        if (!isZero(1.0F - (float) var6)) {
            double var8 = Math.sqrt(var6);
            this.w = (float) ((double) this.w / var8);
            this.x = (float) ((double) this.x / var8);
            this.y = (float) ((double) this.y / var8);
            this.z = (float) ((double) this.z / var8);
        }

    }

    public double bho() {
        return Math.sqrt((double) (this.w * this.w + this.x * this.x + this.y * this.y + this.z * this.z));
    }

    public double bhp() {
        return (double) (this.w * this.w + this.x * this.x + this.y * this.y + this.z * this.z);
    }

    public OrientationBase zd() {
        return new OrientationBase(-this.x, -this.y, -this.z, this.w);
    }

    public OrientationBase coq() {
        this.conjugate();
        return this;
    }

    public OrientationBase zc() {
        double var1 = this.bho();
        return new OrientationBase(-((float) ((double) this.x / var1)), -((float) ((double) this.y / var1)), -((float) ((double) this.z / var1)), (float) ((double) this.w / var1));
    }

    public OrientationBase cor() {
        double var1 = this.bho();
        this.set(-((float) ((double) this.x / var1)), -((float) ((double) this.y / var1)), -((float) ((double) this.z / var1)), (float) ((double) this.w / var1));
        return this;
    }

    public OrientationBase zb() {
        return (new OrientationBase(this)).cos();
    }

    public OrientationBase cos() {
        this.normalize();
        return this;
    }

    public boolean isNormalized() {
        double var1 = this.bhp();
        return isZero(1.0F - (float) var1);
    }

    public OrientationBase d(Quat4f var1) {
        return (new OrientationBase()).b(this, var1);
    }

    public OrientationBase b(Quat4f var1, Quat4f var2) {
        this.mul(var1, var2);
        return this;
    }

    public OrientationBase e(Quat4f var1) {
        this.mul(var1);
        return this;
    }

    public float bht() {
        float var1 = this.x * this.x + this.y * this.y + this.z * this.z + this.w * this.w;
        float var2;
        if (var1 > 0.0F) {
            var1 = 1.0F / (float) Math.sqrt((double) var1);
            var2 = this.w * var1;
        } else {
            var2 = 0.0F;
        }

        return (float) Math.acos((double) var2) * 2.0F;
    }

    public float bhu() {
        return (float) ((double) this.bht() / PI_180);
    }

    public Vec3f cot() {
        Vec3f var1 = new Vec3f();
        float var2 = this.x * this.x + this.y * this.y + this.z * this.z + this.w * this.w;
        if (var2 > 0.0F) {
            var2 = 1.0F / (float) Math.sqrt((double) var2);
            float var6 = this.x * var2;
            float var5 = this.y * var2;
            float var4 = this.z * var2;
            float var3 = this.w * var2;
            float var7 = (float) Math.sqrt((double) (1.0F - var3 * var3));
            if (Math.abs(var7) < ANGLE_PRECISION) {
                var1.x = 1.0F;
                var1.y = var1.z = 0.0F;
                return var1;
            } else {
                var1.x = var6 / var7;
                var1.y = var5 / var7;
                var1.z = var4 / var7;
                return var1;
            }
        } else {
            return var1;
        }
    }

    public float[] bhw() {
        float[] var1 = new float[3];
        double var2 = (double) (this.x * this.y + this.z * this.w);
        if (var2 > 0.4999500000012631D) {
            var1[1] = (float) (2.0D * Math.atan2((double) this.x, (double) this.w));
            var1[2] = 1.5707964F;
            var1[0] = 0.0F;
            return var1;
        } else if (var2 < -0.4999500000012631D) {
            var1[1] = (float) (-2.0D * Math.atan2((double) this.x, (double) this.w));
            var1[2] = -1.5707964F;
            var1[0] = 0.0F;
            return var1;
        } else {
            double var4 = (double) (this.x * this.x);
            double var6 = (double) (this.y * this.y);
            double var8 = (double) (this.z * this.z);
            var1[0] = (float) Math.atan2((double) (2.0F * this.x * this.w - 2.0F * this.y * this.z), 1.0D - 2.0D * var4 - 2.0D * var8);
            var1[1] = (float) Math.atan2((double) (2.0F * this.y * this.w - 2.0F * this.x * this.z), 1.0D - 2.0D * var6 - 2.0D * var8);
            var1[2] = (float) Math.asin(2.0D * var2);
            return var1;
        }
    }

    public void e(Tuple3f var1) {
        double var2 = (double) (this.x * this.y + this.z * this.w);
        if (var2 > 0.4999500000012631D) {
            var1.y = (float) (2.0D * Math.atan2((double) this.x, (double) this.w));
            var1.z = 1.5707964F;
            var1.x = 0.0F;
        } else if (var2 < -0.4999500000012631D) {
            var1.y = (float) (-2.0D * Math.atan2((double) this.x, (double) this.w));
            var1.z = -1.5707964F;
            var1.x = 0.0F;
        } else {
            double var4 = (double) (this.x * this.x);
            double var6 = (double) (this.y * this.y);
            double var8 = (double) (this.z * this.z);
            var1.x = (float) Math.atan2((double) (2.0F * this.x * this.w - 2.0F * this.y * this.z), 1.0D - 2.0D * var4 - 2.0D * var8);
            var1.y = (float) Math.atan2((double) (2.0F * this.y * this.w - 2.0F * this.x * this.z), 1.0D - 2.0D * var6 - 2.0D * var8);
            var1.z = (float) Math.asin(2.0D * var2);
        }
    }

    public float[] bhx() {
        float[] var1 = this.bhw();
        var1[0] = (float) ((double) var1[0] / PI_180);
        var1[1] = (float) ((double) var1[1] / PI_180);
        var1[2] = (float) ((double) var1[2] / PI_180);
        return var1;
    }

    public OrientationBase d(OrientationBase var1) {
        return new OrientationBase(this.x + var1.x, this.y + var1.y, this.z + var1.z, this.w + var1.w);
    }

    public OrientationBase cou() {
        return new OrientationBase(-this.x, -this.y, -this.z, -this.w);
    }

    public OrientationBase cov() {
        this.negate();
        return this;
    }

    public boolean e(OrientationBase var1) {
        OrientationBase var2 = this.zb();
        OrientationBase var3 = var1.zb();
        return var2.equals(var3);
    }

    public ajK cow() {
        ajK var1 = new ajK();
        OrientationBase var2 = this.zb();
        float var3 = var2.x * var2.x;
        float var4 = var2.x * var2.y;
        float var5 = var2.x * var2.z;
        float var6 = var2.w * var2.x;
        float var7 = var2.y * var2.y;
        float var8 = var2.y * var2.z;
        float var9 = var2.y * var2.w;
        float var10 = var2.z * var2.z;
        float var11 = var2.z * var2.w;
        var1.m00 = 1.0F - 2.0F * (var7 + var10);
        var1.m01 = 2.0F * (var4 - var11);
        var1.m02 = 2.0F * (var5 + var9);
        var1.m10 = 2.0F * (var4 + var11);
        var1.m11 = 1.0F - 2.0F * (var3 + var10);
        var1.m12 = 2.0F * (var8 - var6);
        var1.m20 = 2.0F * (var5 - var9);
        var1.m21 = 2.0F * (var8 + var6);
        var1.m22 = 1.0F - 2.0F * (var3 + var7);
        var1.m03 = var1.m13 = var1.m23 = var1.m30 = var1.m31 = var1.m32 = 0.0F;
        var1.m33 = 1.0F;
        return var1;
    }

    public void d(ajK var1) {
        float var2 = this.w;
        float var3 = this.x;
        float var4 = this.y;
        float var5 = this.z;
        double var6 = (double) (var2 * var2 + var3 * var3 + var4 * var4 + var5 * var5);
        if (!isZero(1.0F - (float) var6)) {
            double var8 = 1.0D / Math.sqrt(var6);
            var2 = (float) ((double) var2 * var8);
            var3 = (float) ((double) var3 * var8);
            var4 = (float) ((double) var4 * var8);
            var5 = (float) ((double) var5 * var8);
        }

        float var17 = var3 * var3;
        float var9 = var3 * var4;
        float var10 = var3 * var5;
        float var11 = var2 * var3;
        float var12 = var4 * var4;
        float var13 = var4 * var5;
        float var14 = var4 * var2;
        float var15 = var5 * var5;
        float var16 = var5 * var2;
        var1.m00 = 1.0F - 2.0F * (var12 + var15);
        var1.m01 = 2.0F * (var9 - var16);
        var1.m02 = 2.0F * (var10 + var14);
        var1.m10 = 2.0F * (var9 + var16);
        var1.m11 = 1.0F - 2.0F * (var17 + var15);
        var1.m12 = 2.0F * (var13 - var11);
        var1.m20 = 2.0F * (var10 - var14);
        var1.m21 = 2.0F * (var13 + var11);
        var1.m22 = 1.0F - 2.0F * (var17 + var12);
        var1.m03 = var1.m13 = var1.m23 = var1.m30 = var1.m31 = var1.m32 = 0.0F;
        var1.m33 = 1.0F;
    }

    public void e(ajK var1) {
        float var2 = this.w;
        float var3 = this.x;
        float var4 = this.y;
        float var5 = this.z;
        double var6 = (double) (var2 * var2 + var3 * var3 + var4 * var4 + var5 * var5);
        if (!isZero(1.0F - (float) var6)) {
            double var8 = 1.0D / Math.sqrt(var6);
            var2 = (float) ((double) var2 * var8);
            var3 = (float) ((double) var3 * var8);
            var4 = (float) ((double) var4 * var8);
            var5 = (float) ((double) var5 * var8);
        }

        float var17 = var3 * var3;
        float var9 = var3 * var4;
        float var10 = var3 * var5;
        float var11 = var2 * var3;
        float var12 = var4 * var4;
        float var13 = var4 * var5;
        float var14 = var4 * var2;
        float var15 = var5 * var5;
        float var16 = var5 * var2;
        var1.m00 = 1.0F - 2.0F * (var12 + var15);
        var1.m01 = 2.0F * (var9 - var16);
        var1.m02 = 2.0F * (var10 + var14);
        var1.m10 = 2.0F * (var9 + var16);
        var1.m11 = 1.0F - 2.0F * (var17 + var15);
        var1.m12 = 2.0F * (var13 - var11);
        var1.m20 = 2.0F * (var10 - var14);
        var1.m21 = 2.0F * (var13 + var11);
        var1.m22 = 1.0F - 2.0F * (var17 + var12);
    }

    public Vec3f aT(Vec3f var1) {
        Vec3f var2 = new Vec3f();
        a(this, var1, var2);
        return var2;
    }

    public Vec3f aU(Vec3f var1) {
        Vec3f var2 = new Vec3f();
        b(this, var1, var2);
        return var2;
    }

    public final void E(Vec3f var1, Vec3f var2) {
        b(this, var1, var2);
    }

    public void F(Vec3f var1, Vec3f var2) {
        a(this, var1, var2);
    }

    public Vec3f aV(Vec3f var1) {
        Vec3f var2 = new Vec3f();
        this.G(var1, var2);
        return var2;
    }

    public void G(Vec3f var1, Vec3f var2) {
        float var3 = this.w;
        float var4 = this.x;
        float var5 = this.y;
        float var6 = this.z;
        float var7 = var3 * var4;
        float var8 = var3 * var5;
        float var9 = var3 * var6;
        float var10 = -var4 * var4;
        float var11 = var4 * var5;
        float var12 = var4 * var6;
        float var13 = -var5 * var5;
        float var14 = var5 * var6;
        float var15 = -var6 * var6;
        float var16 = var1.x;
        float var17 = var1.y;
        float var18 = var1.z;
        float var19 = 2.0F * ((var13 + var14) * var16 + (var9 + var11) * var17 + (var12 - var8) * var18) + var16;
        float var20 = 2.0F * ((var11 - var9) * var16 + (var10 + var15) * var17 + (var7 + var14) * var18) + var17;
        float var21 = 2.0F * ((var8 + var12) * var16 + (var14 - var7) * var17 + (var10 + var13) * var18) + var18;
        var2.x = var19;
        var2.y = var20;
        var2.z = var21;
    }

    public void a(OrientationBase var1, float var2) {
        a(this, var1, var2, this);
    }

    public void a(OrientationBase var1, OrientationBase var2, float var3) {
        float var4 = var2.w;
        float var5 = var2.x;
        float var6 = var2.y;
        float var7 = var2.z;
        float var8 = var1.w;
        float var9 = var1.x;
        float var10 = var1.y;
        float var11 = var1.z;
        double var12 = (double) (var8 * var4 + var9 * var5 + var10 * var6 + var11 * var7);
        if (var12 < 0.0D) {
            var4 = -var4;
            var5 = -var5;
            var6 = -var6;
            var7 = -var7;
            var12 = -var12;
        }

        float var14;
        float var15;
        if (var12 > 0.9998999834060669D) {
            var14 = 1.0F - var3;
            var15 = var3;
        } else {
            float var17 = (float) Math.sqrt(1.0D - var12 * var12);
            float var18 = (float) Math.atan2((double) var17, var12);
            float var19 = 1.0F / var17;
            var14 = (float) (Math.sin((double) ((1.0F - var3) * var18)) * (double) var19);
            var15 = (float) (Math.sin((double) (var3 * var18)) * (double) var19);
        }

        this.w = var8 * var14 + var4 * var15;
        this.x = var9 * var14 + var5 * var15;
        this.y = var10 * var14 + var6 * var15;
        this.z = var11 * var14 + var7 * var15;
    }

    public void cox() {
        double var1 = this.bhp();
        if (!isZero(1.0F - (float) var1)) {
            double var3 = Math.sqrt(var1);
            this.w = (float) ((double) this.w / var3);
            this.x = (float) ((double) this.x / var3);
            this.y = (float) ((double) this.y / var3);
            this.z = (float) ((double) this.z / var3);
        }

    }

    public boolean equals(Object var1) {
        if (var1 instanceof OrientationBase) {
            OrientationBase var2 = (OrientationBase) var1;
            return isZero(this.w - var2.w) && isZero(this.x - var2.x) && isZero(this.y - var2.y) && isZero(this.z - var2.z);
        } else {
            return false;
        }
    }

    public String toString() {
        return "[ (" + this.w + ") + (" + this.x + ")*parseStyleDeclaration + (" + this.y + ")*parseRule + (" + this.z + ")*WinInet ]";
    }

    public String bhA() {
        String var1 = "0.0";
        String var2;
        if (isZero(this.w)) {
            var2 = var1;
        } else {
            var2 = String.valueOf(this.w);
        }

        String var3;
        if (isZero(this.x)) {
            var3 = var1;
        } else {
            var3 = String.valueOf(this.x);
        }

        String var4;
        if (isZero(this.y)) {
            var4 = var1;
        } else {
            var4 = String.valueOf(this.y);
        }

        String var5;
        if (isZero(this.z)) {
            var5 = var1;
        } else {
            var5 = String.valueOf(this.z);
        }

        return "[ (" + var2 + ") + (" + var3 + ")*parseStyleDeclaration + (" + var4 + ")*parseRule + (" + var5 + ")*WinInet ]";
    }

    public String bhB() {
        return "[ Axis: " + this.cot() + " Angle: " + this.bhu() + "° ]";
    }

    public String bhC() {
        float[] var1 = this.bhx();
        return "[ Roll: " + var1[2] + " Pitch: " + var1[0] + " Yaw: " + var1[1] + " ]";
    }

    public int hashCode() {
        return (int) (((this.w * 31.0F + this.x) * 31.0F + this.y) * 31.0F + this.z);
    }

    public boolean anA() {
        float var1 = this.w + this.x + this.y + this.z;
        return !Float.isNaN(var1) && !Float.isInfinite(var1);
    }

    public void f(OrientationBase var1) {
        this.x = var1.x;
        this.y = var1.y;
        this.z = var1.z;
        this.w = var1.w;
    }

    public OrientationBase p(float var1, float var2, float var3, float var4) {
        this.x = var1;
        this.y = var2;
        this.z = var3;
        this.w = var4;
        return this;
    }

    public boolean a(OrientationBase var1, double var2) {
        return (double) Math.abs(var1.x - this.x) <= var2 && (double) Math.abs(var1.y - this.y) <= var2 && (double) Math.abs(var1.z - this.z) <= var2 && (double) Math.abs(var1.w - this.w) <= var2;
    }

    public void g(Vector3dOperations var1, Vector3dOperations var2) {
        Vec3f var3 = var1.aB(var2);
        var3.normalize();
        Vec3f var4 = new Vec3f(0.0F, 1.0F, 0.0F);
        this.F(var4, var4);
        ajK var5 = new ajK();
        float var6 = var4.dot(var3);
        Vec3f var7;
        if (Math.abs(var6) >= 0.9F) {
            var4.set(1.0F, 0.0F, 0.0F);
            this.F(var4, var4);
            var7 = var3.b(var4);
            var7.normalize();
            var4.cross(var7, var3);
            var4.normalize();
            var5.aD(var4);
            var5.aE(var7);
            var5.aF(var3);
        } else {
            var7 = var4.b(var3);
            var7.normalize();
            var4.cross(var3, var7);
            var4.normalize();
            var5.aD(var7);
            var5.aE(var4);
            var5.aF(var3);
        }

        this.set(var5);
    }

    public OrientationBase f(ajD var1) {
        this.e(var1);
        return this;
    }

    public OrientationBase kb(float var1) {
        OrientationBase var2 = new OrientationBase(this);
        var2.scale(var1);
        return var2;
    }

    public OrientationBase kc(float var1) {
        this.scale(var1);
        return this;
    }

    public OrientationBase coy() {
        return new OrientationBase(this);
    }

    public void readExternal(Vm_q var1) {
        this.x = var1.gZ("x");
        this.y = var1.gZ("y");
        this.z = var1.gZ("z");
        this.w = var1.gZ("w");
    }

    public void writeExternal(att var1) {
        var1.a("x", this.x);
        var1.a("y", this.y);
        var1.a("z", this.z);
        var1.a("w", this.w);
    }
}
