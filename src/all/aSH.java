package all;

import com.hoplon.geometry.Vec3f;

import javax.vecmath.Vector3f;

public class aSH {
    protected Vec3f p0;
    protected Vec3f p1;
    protected Vec3f p2;
    protected Vec3f Oj;
    protected Vec3f iNl;

    public aSH() {
        this.p0 = new Vec3f();
        this.p1 = new Vec3f();
        this.p2 = new Vec3f();
    }

    public aSH(Vec3f var1, Vec3f var2, Vec3f var3) {
        this.p0 = var1;
        this.p1 = var2;
        this.p2 = var3;
    }

    public Vec3f duL() {
        return this.p0;
    }

    public void bS(Vec3f var1) {
        this.p0 = var1;
        this.Oj = null;
        this.iNl = null;
    }

    public void I(float var1, float var2, float var3) {
        this.p0.set(var1, var2, var3);
        this.Oj = null;
        this.iNl = null;
    }

    public Vec3f duM() {
        return this.p1;
    }

    public void bT(Vec3f var1) {
        this.p1 = var1;
        this.Oj = null;
        this.iNl = null;
    }

    public void J(float var1, float var2, float var3) {
        this.p1.set(var1, var2, var3);
        this.Oj = null;
        this.iNl = null;
    }

    public Vec3f duN() {
        return this.p2;
    }

    public void bU(Vec3f var1) {
        this.p2 = var1;
        this.Oj = null;
        this.iNl = null;
    }

    public void K(float var1, float var2, float var3) {
        this.p2.set(var1, var2, var3);
        this.Oj = null;
        this.iNl = null;
    }

    public void q(Vec3f var1, Vec3f var2, Vec3f var3) {
        this.p0 = var1;
        this.p1 = var2;
        this.p2 = var3;
        this.Oj = null;
        this.iNl = null;
    }

    public String toString() {
        return String.format("%s %s %s", this.p0, this.p1, this.p2);
    }

    public boolean equals(Object var1) {
        if (!(var1 instanceof aSH)) {
            return false;
        } else {
            aSH var2 = (aSH) var1;
            return var2.p0.equals(this.p0) && var2.p1.equals(this.p1) && var2.p2.equals(this.p2);
        }
    }

    public gu_g rayIntersects(Vec3f var1, Vec3f var2) {
        gu_g var3 = new gu_g(var1, var2);
        return this.rayIntersects(var3);
    }

    public gu_g rayIntersects(gu_g var1) {
        Vec3f var2 = this.p1.j(this.p0);
        Vec3f var3 = this.p2.j(this.p1);
        Vec3f var4 = var2.b(var3);
        float var5 = var4.dot(var1.vR());
        if (var5 >= 0.0F) {
            return var1;
        } else {
            float var6 = var4.dot(this.p0);
            float var7 = var6 - var4.dot(var1.vP());
            if (var7 > 0.0F) {
                return var1;
            } else if (var7 < var5) {
                return var1;
            } else {
                var7 /= var5;
                Vec3f var8 = var1.vP().i(var1.vR().mS(var7));
                float var9;
                float var10;
                float var11;
                float var12;
                float var13;
                float var14;
                if (Math.abs(var4.x) > Math.abs(var4.y)) {
                    if (Math.abs(var4.x) > Math.abs(var4.z)) {
                        var9 = var8.y - this.p0.y;
                        var10 = this.p1.y - this.p0.y;
                        var11 = this.p2.y - this.p0.y;
                        var12 = var8.z - this.p0.z;
                        var13 = this.p1.z - this.p0.z;
                        var14 = this.p2.z - this.p0.z;
                    } else {
                        var9 = var8.x - this.p0.x;
                        var10 = this.p1.x - this.p0.x;
                        var11 = this.p2.x - this.p0.x;
                        var12 = var8.y - this.p0.y;
                        var13 = this.p1.y - this.p0.y;
                        var14 = this.p2.y - this.p0.y;
                    }
                } else if (Math.abs(var4.y) > Math.abs(var4.z)) {
                    var9 = var8.x - this.p0.x;
                    var10 = this.p1.x - this.p0.x;
                    var11 = this.p2.x - this.p0.x;
                    var12 = var8.z - this.p0.z;
                    var13 = this.p1.z - this.p0.z;
                    var14 = this.p2.z - this.p0.z;
                } else {
                    var9 = var8.x - this.p0.x;
                    var10 = this.p1.x - this.p0.x;
                    var11 = this.p2.x - this.p0.x;
                    var12 = var8.y - this.p0.y;
                    var13 = this.p1.y - this.p0.y;
                    var14 = this.p2.y - this.p0.y;
                }

                float var15 = var10 * var14 - var13 * var11;
                if (var15 == 0.0F) {
                    return var1;
                } else {
                    var15 = 1.0F / var15;
                    float var16 = (var9 * var14 - var12 * var11) * var15;
                    if (var16 < 0.0F) {
                        return var1;
                    } else {
                        float var17 = (var10 * var12 - var13 * var9) * var15;
                        if (var17 < 0.0F) {
                            return var1;
                        } else {
                            float var18 = 1.0F - var16 - var17;
                            if (var18 < 0.0F) {
                                return var1;
                            } else {
                                var1.setIntersected(true);
                                var1.ac(var7);
                                var1.l(var4.dfO());
                                return var1;
                            }
                        }
                    }
                }
            }
        }
    }

    public Vec3f Jh() {
        if (this.Oj == null) {
            this.Oj = this.p0.j(this.p1).d((Vector3f) this.p0.j(this.p2)).dfP();
        }

        return this.Oj;
    }

    public Vec3f duO() {
        if (this.iNl == null) {
            float var1 = (this.p0.x + this.p1.x + this.p2.x) / 3.0F;
            float var2 = (this.p0.y + this.p1.y + this.p2.y) / 3.0F;
            float var3 = (this.p0.z + this.p1.z + this.p2.z) / 3.0F;
            this.iNl = new Vec3f(var1, var2, var3);
        }

        return this.iNl;
    }
}
