package all;

import java.util.Vector;

public class asq implements SACMediaList {
    private Vector guO = new Vector(10, 10);

    public int getLength() {
        return this.guO.size();
    }

    public String item(int var1) {
        return (String) this.guO.elementAt(var1);
    }

    public void add(String var1) {
        this.guO.addElement(var1);
    }

    public String toString() {
        StringBuffer var1 = new StringBuffer();
        int var2 = this.getLength();

        for (int var3 = 0; var3 < var2; ++var3) {
            var1.append(this.item(var3));
            if (var3 < var2 - 1) {
                var1.append(", ");
            }
        }

        return var1.toString();
    }
}
