package all;

import java.text.MessageFormat;

public class adg {
    public static String format(String var0, Object... var1) {
        StringBuffer var2 = new StringBuffer();
        char[] var6;
        int var5 = (var6 = var0.toCharArray()).length;

        for (int var4 = 0; var4 < var5; ++var4) {
            char var3 = var6[var4];
            var2.append(var3);
            if (var3 == '\'') {
                var2.append('\'');
            }
        }

        return MessageFormat.format(var2.toString(), var1);
    }
}
