package all;

import com.hoplon.geometry.Vec3f;

public class ayY {
    public static int gXE = 1;
    public static int gXF = 2;
    public static int gXG = 3;
    public static int gXH = 4;
    public static int gXI = 5;
    protected final Kt stack = Kt.bcE();
    protected final xf_g gXJ = new xf_g();
    protected final Vec3f gXK = new Vec3f();
    protected final Vec3f gXL = new Vec3f();
    protected xf_g deL = new xf_g();
    protected LL_w gXM;
    protected Lc fgD;
    protected int gXN = 1;
    protected int gXO = -1;
    protected int gXP = -1;
    protected int gXQ = 1;
    protected float gXR;
    protected float dMd = 0.5F;
    protected float dMe;
    protected Object gXS;
    protected Object gXT;
    protected float r = 1.0F;
    protected float gXU;
    protected float gXV;
    protected boolean gXW;

    public boolean d(ayY var1) {
        return true;
    }

    public boolean cEU() {
        return (this.gXN & 7) == 0;
    }

    public boolean cEV() {
        return (this.gXN & 1) != 0;
    }

    public boolean cEW() {
        return (this.gXN & 2) != 0;
    }

    public boolean cEX() {
        return (this.gXN & 3) != 0;
    }

    public boolean cEY() {
        return (this.gXN & 4) == 0;
    }

    public Lc cEZ() {
        return this.fgD;
    }

    public void a(Lc var1) {
        this.fgD = var1;
    }

    public int cFa() {
        return this.gXQ;
    }

    public void wv(int var1) {
        if (this.gXQ != 4 && this.gXQ != 5) {
            this.gXQ = var1;
        }

    }

    public float cFb() {
        return this.gXR;
    }

    public void la(float var1) {
        this.gXR = var1;
    }

    public void ww(int var1) {
        this.gXQ = var1;
    }

    public void activate() {
        this.hx(false);
    }

    public void hx(boolean var1) {
        if (var1 || (this.gXN & 3) == 0) {
            this.wv(1);
            this.gXR = 0.0F;
        }

    }

    public boolean isActive() {
        return this.cFa() != 2 && this.cFa() != 5;
    }

    public float cFc() {
        return this.dMe;
    }

    public void lb(float var1) {
        this.dMe = var1;
    }

    public float cFd() {
        return this.dMd;
    }

    public void lc(float var1) {
        this.dMd = var1;
    }

    public Object cFe() {
        return this.gXT;
    }

    public xf_g cFf() {
        return this.deL;
    }

    public void f(xf_g var1) {
        this.deL.a(var1);
    }

    public LL_w cFg() {
        return this.gXM;
    }

    public void c(LL_w var1) {
        this.gXM = var1;
    }

    public xf_g cFh() {
        return this.gXJ;
    }

    public void j(xf_g var1) {
        this.gXJ.a(var1);
    }

    public Vec3f cFi() {
        return this.gXK;
    }

    public Vec3f cFj() {
        return this.gXL;
    }

    public int cFk() {
        return this.gXO;
    }

    public void wx(int var1) {
        this.gXO = var1;
    }

    public int cFl() {
        return this.gXP;
    }

    public void wy(int var1) {
        this.gXP = var1;
    }

    public float cFm() {
        return this.r;
    }

    public void ld(float var1) {
        this.r = var1;
    }

    public int cFn() {
        return this.gXN;
    }

    public void wz(int var1) {
        this.gXN = var1;
    }

    public float cFo() {
        return this.gXU;
    }

    public void le(float var1) {
        this.gXU = var1;
    }

    public float cFp() {
        return this.gXV;
    }

    public void lf(float var1) {
        this.gXV = var1;
    }

    public Object cFq() {
        return this.gXS;
    }

    public void aA(Object var1) {
        this.gXS = var1;
    }

    public boolean e(ayY var1) {
        return this.gXW ? this.d(var1) : true;
    }
}
