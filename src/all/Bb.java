package all;

import com.hoplon.geometry.Vec3f;

import java.util.*;
import java.util.Map.Entry;

public class Bb {
    private static final List dMV = new ArrayList();
    private static final Map dMW = new HashMap();
    public static boolean DEBUG = true;
    public static boolean dMs = false;
    public static float dMt = 0.04F;
    public static float dMu = 1.1920929E-7F;
    public static float dMv = 1.1920929E-7F;
    public static float dMw = 6.2831855F;
    public static float dMx = 3.1415927F;
    public static float dMy = 1.5707964F;
    public static float dMz = 0.017453292F;
    public static float dMA = 57.295776F;
    public static float dMB = Float.MAX_VALUE;
    public static aaf dMC;
    public static ahd_q dMD;
    public static float dME = 0.02F;
    public static float dMF = 2.0F;
    public static boolean dMG = false;
    public static int dMH;
    public static int dMI = 0;
    public static int dMJ = 0;
    public static int dMK;
    public static int dML;
    public static int dMM;
    public static int dMN = 0;
    public static Vec3f dMO = new Vec3f();
    public static float dMP = 0.0F;
    public static int dMQ = 0;
    public static int dMR = 0;
    public static int dMS = 0;
    public static int dMT = 0;
    public static Vec3f dMU = new Vec3f(0.0F, 0.0F, 0.0F);
    public static long dMX;
    public static long dMY;

    public static void gf(String var0) {
    }

    public static void blS() {
    }

    public static void blT() {
        ArrayList var0 = new ArrayList((Collection) dMW.entrySet());
        Collections.sort(var0, new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                return a((Entry) o1, (Entry) o2);
            }

            public int a(Entry var1, Entry var2) {
                return ((Long) var1.getValue()).compareTo((Long) var2.getValue());
            }
        });
        Iterator var2 = var0.iterator();

        while (var2.hasNext()) {
            Entry var1 = (Entry) var2.next();
            System.out.println((String) var1.getKey() + " = " + var1.getValue() + " ms");
        }

    }

    private static class b {
        public String name;
        public long startTime;
    }
}
