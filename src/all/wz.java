package all;

import javax.swing.*;
import java.awt.*;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public abstract class wz {
    private static final Map bDV = new ConcurrentHashMap();
    public static String bDU = "gui.TooltipProvider";

    public static void a(String var0, wz var1) {
        bDV.put(var0, var1);
    }

    public static wz d(JComponent var0) {
        Object var1 = var0.getClientProperty("gui.TooltipProvider");
        if (var1 != null) {
            if (var1 instanceof String) {
                return (wz) bDV.get(var1);
            }

            if (var1 instanceof wz) {
                return (wz) var1;
            }
        }

        return null;
    }

    public static void a(JComponent var0, String var1) {
        var0.putClientProperty("gui.TooltipProvider", var1);
    }

    public static void a(JComponent var0, wz var1) {
        var0.putClientProperty("gui.TooltipProvider", var1);
    }

    public abstract Component b(Component var1);
}
