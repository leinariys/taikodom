package all;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicSliderUI;
import java.awt.*;

/**
 * SliderUI
 */
public class SliderUI extends BasicSliderUI implements aIh {
    private ComponentManager Rp;

    public SliderUI(JSlider var1) {
        super(var1);
        this.Rp = new ComponentManager("slider", var1);
        var1.setBackground((Color) null);
    }

    public static ComponentUI createUI(JComponent var0) {
        return new SliderUI((JSlider) var0);
    }

    /**
     * TODO похлже это public pv wy()
     *
     * @return Графический элемент
     */
    @Override
    public aeK wz() {
        return this.Rp;
    }

    protected Dimension getThumbSize() {
        return super.getThumbSize();
    }

    protected void calculateTrackRect() {
        super.calculateTrackRect();
    }

    public void paint(Graphics var1, JComponent var2) {
        RM.a((aIh) this, (Graphics) var1, (JComponent) var2);
        super.paint(var1, var2);
    }

    public void paintTrack(Graphics var1) {
        super.paintTrack(var1);
    }

    public void paintThumb(Graphics var1) {
        super.paintThumb(var1);
    }

    public Dimension getPreferredSize(JComponent var1) {
        return this.Rp.c(var1, super.getPreferredSize(var1));
    }

    public Dimension getMinimumSize(JComponent var1) {
        return this.Rp.b(var1, super.getMinimumSize(var1));
    }

    public Dimension getMaximumSize(JComponent var1) {
        return this.Rp.a(var1, super.getMaximumSize(var1));
    }
}
