package all;

import sun.swing.SwingUtilities2;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicTabbedPaneUI;
import javax.swing.text.View;
import java.awt.*;

/**
 * TabbedPaneUI
 */
public class TabbedPaneUI extends BasicTabbedPaneUI implements aIh {
    private aeK BM;
    private JLabel jLabel;
    private PropertiesUiFromCss fue;
    private PanelCustomWrapper fuf;

    public TabbedPaneUI(final TabpaneCustomWrapper var1) {
        this.BM = new ComponentManager("tabpane", var1);
        this.jLabel = new JLabel() {
            public Container getParent() {
                return var1;
            }
        };
        this.fuf = new PanelCustomWrapper() {
            public Container getParent() {
                return var1;
            }
        };
    }

    public static ComponentUI createUI(JComponent var0) {
        return new TabbedPaneUI((TabpaneCustomWrapper) var0);
    }

    /**
     * TODO похлже это public pv wy()
     *
     * @return Графический элемент
     */
    @Override
    public aeK wz() {
        return this.BM;
    }

    public Dimension getPreferredSize(JComponent var1) {
        return this.wz().c(var1, super.getPreferredSize(var1));
    }

    public Dimension getMinimumSize(JComponent var1) {
        return this.wz().b(var1, super.getMinimumSize(var1));
    }

    public Dimension getMaximumSize(JComponent var1) {
        return this.wz().a(var1, super.getMaximumSize(var1));
    }

    private void bVu() {
        if (!this.tabPane.isValid()) {
            this.tabPane.validate();
        }

        if (!this.tabPane.isValid()) {
            TabbedPaneLayout var1 = (TabbedPaneLayout) this.tabPane.getLayout();
            var1.calculateLayoutInfo();
        }

    }

    public void paint(Graphics var1, JComponent var2) {
        int var3 = this.tabPane.getSelectedIndex();
        int var4 = this.tabPane.getTabPlacement();
        this.bVu();
        this.paintContentBorder(var1, var4, var3);
        this.paintTabArea(var1, var4, var3);
    }

    protected void paintContentBorder(Graphics var1, int var2, int var3) {
        int var4 = this.tabPane.getWidth();
        int var5 = this.tabPane.getHeight();
        Insets var6 = this.tabPane.getInsets();
        int var7 = var6.left;
        int var8 = var6.top;
        int var9 = var4 - var6.right - var6.left;
        int var10 = var5 - var6.top - var6.bottom;
        switch (var2) {
            case 1:
            default:
                var8 += this.calculateTabAreaHeight(var2, this.runCount, this.maxTabHeight);
                var10 -= var8 - var6.top;
                break;
            case 2:
                var7 += this.calculateTabAreaWidth(var2, this.runCount, this.maxTabWidth);
                var9 -= var7 - var6.left;
                break;
            case 3:
                var10 -= this.calculateTabAreaHeight(var2, this.runCount, this.maxTabHeight);
                break;
            case 4:
                var9 -= this.calculateTabAreaWidth(var2, this.runCount, this.maxTabWidth);
        }

        if (this.tabPane.getTabCount() > 0 && this.tabPane.isOpaque()) {
            Color var11 = UIManager.getColor("TabbedPane.contentAreaColor");
            if (var11 != null) {
                var1.setColor(var11);
            } else if (var3 == -1) {
                var1.setColor(this.tabPane.getBackground());
            } else {
                var1.setColor(Color.RED);
            }

            var1.fillRect(var7, var8, var9, var10);
        }

        if (this.tabPane.getSelectedIndex() < 0) {
            this.fuf.getBorder().paintBorder(this.fuf, var1, var7, var8, var9, var10);
        } else {
            Rectangle var13 = this.rects[this.tabPane.getSelectedIndex()];
            if (var13 == null) {
                this.fuf.getBorder().paintBorder(this.fuf, var1, var7, var8, var9, var10);
            } else {
                int var12 = var7 + var13.x;
                this.fuf.getBorder().paintBorder(this.fuf, var1, var7, var8, var12, var5);
                this.fuf.getBorder().paintBorder(this.fuf, var1, var12 + var13.width, var8, var9 - var12, var10);
            }
        }
    }

    protected void paintTabArea(Graphics var1, int var2, int var3) {
        int var4 = this.tabPane.getTabCount();
        Rectangle var5 = new Rectangle();
        Rectangle var6 = new Rectangle();
        Rectangle var7 = var1.getClipBounds();

        for (int var8 = this.runCount - 1; var8 >= 0; --var8) {
            int var9 = this.tabRuns[var8];
            int var10 = this.tabRuns[var8 == this.runCount - 1 ? 0 : var8 + 1];
            int var11 = var10 != 0 ? var10 - 1 : var4 - 1;

            for (int var12 = var9; var12 <= var11; ++var12) {
                if (var12 != var3 && this.rects[var12].intersects(var7)) {
                    this.paintTab(var1, var2, this.rects, var12, var5, var6);
                }
            }
        }

        if (this.rects.length > 0 && var3 >= 0 && this.rects[var3].intersects(var7)) {
            this.paintTab(var1, var2, this.rects, var3, var5, var6);
        }

    }

    protected void paintTab(Graphics var1, int var2, Rectangle[] var3, int var4, Rectangle var5, Rectangle var6) {
        Rectangle var7 = var3[var4];
        boolean var8 = this.tabPane.getSelectedIndex() == var4;
        ComponentManager.getCssHolder(this.jLabel).q(2, var8 ? 2 : 0);
        this.jLabel.setBounds(var7);
        RM.a((Graphics) var1.create(var7.x, var7.y, var7.width, var7.height), (Component) this.jLabel, (PropertiesUiFromCss) PropertiesUiFromCss.g(this.jLabel));
        BorderWrapper var9 = (BorderWrapper) this.jLabel.getBorder();
        if (var9 != null) {
            var9.paintBorder(this.jLabel, var1, var7.x, var7.y, var7.width, var7.height);
        }

        String var10 = this.tabPane.getTitleAt(var4);
        this.fue = ComponentManager.getCssHolder(this.jLabel).Vp();
        Font var11 = this.fue.getFont();
        if (var11 != null) {
            FontMetrics var12 = SwingUtilities2.getFontMetrics(this.tabPane, var1, var11);
            this.layoutLabel(var2, var12, var4, var10, (Icon) null, var7, var5, var6, var8);
            this.paintText(var1, var2, this.fue.getFont(), var12, var4, var10, var6, var8);
        }
    }

    protected void paintText(Graphics var1, int var2, Font var3, FontMetrics var4, int var5, String var6, Rectangle var7, boolean var8) {
        var1.setFont(var3);
        View var9 = this.getTextViewForTab(var5);
        if (var9 != null) {
            var9.paint(var1, var7);
        } else {
            int var10 = this.tabPane.getDisplayedMnemonicIndexAt(var5);
            Color var11 = this.fue.getColor();
            if (var11 != null) {
                var1.setColor(var11);
            }

            if (this.tabPane.isEnabled() && this.tabPane.isEnabledAt(var5)) {
                SwingUtilities2.drawStringUnderlineCharAt(this.tabPane, var1, var6, var10, var7.x, var7.y + var4.getAscent());
            } else {
                SwingUtilities2.drawStringUnderlineCharAt(this.tabPane, var1, var6, var10, var7.x, var7.y + var4.getAscent());
            }
        }

    }

    protected int getTabLabelShiftY(int var1, int var2, boolean var3) {
        return 0;
    }

    protected int getTabLabelShiftX(int var1, int var2, boolean var3) {
        return 0;
    }

    protected Insets getSelectedTabPadInsets(int var1) {
        return new Insets(0, 0, 0, 0);
    }
}
