package all;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.nio.channels.Channel;

public class bL extends aBC {
    private final int pa;
    private int oZ = 48151623;
    private ByteBuffer buffer = ByteBuffer.allocate(1024);
    private int pb;
    private int pd;
    private int pe;
    private boolean pf;
    private int pg;
    private int ph;

    public bL(int var1, int var2) {
        this.pa = var2;
        if (var2 >= 536870911) {
            throw new IllegalArgumentException("Max block sise too big " + var2);
        }
    }

    public bL() {
        this.pa = 8388608;
    }

    public boolean gE() {
        return this.pf;
    }

    public boolean a(JB var1) {
        int var2 = var1.getOffset();
        int var3 = var1.getLength();
        byte[] var4 = var1.cJn();
        this.pd += var1.isCompressed() ? var1.aYP() : var3;
        ++this.pe;
        if (!this.Y(this.buffer.position() + var3 + 6)) {
            return false;
        } else {
            int var5 = this.buffer.position();
            this.b(var1.isCompressed(), var3, var1.isSpecial());
            this.buffer.put(var4, var2, var3);
            this.a(this.buffer.array(), var5, this.buffer.position());
            return true;
        }
    }

    public void gF() {
        if (this.buffer.position() > 0) {
            this.buffer.flip();
            this.pf = true;
        }

    }

    private boolean Y(int var1) {
        if (this.buffer.capacity() < var1) {
            if (this.buffer.capacity() > 1048576 && this.buffer.position() > 0) {
                return false;
            }

            ByteBuffer var2 = ByteBuffer.allocate(Math.max(var1, this.buffer.capacity() << 1));
            this.buffer.flip();
            var2.put(this.buffer);
            this.buffer = var2;
        }

        return true;
    }

    private void a(byte[] var1, int var2, int var3) {
        for (int var4 = var2; var4 < var3; ++var4) {
            byte var5 = var1[var4];
            var1[var4] = (byte) ((var5 & 255 ^ this.oZ & 255) & 255);
            this.oZ = aFj.aC(this.oZ, var5);
        }

    }

    private void a(boolean var1, int var2, boolean var3) {
        if (var2 <= 31) {
            this.buffer.put(3, (byte) (var2 | (var1 ? 128 : 0)));
            this.buffer.position(3);
        } else if (var2 <= 8191) {
            this.buffer.putShort(2, (short) (var2 | (var1 ? 'ꀀ' : 8192)));
            this.buffer.position(2);
        } else if (var2 <= 2097151) {
            this.buffer.put(1, (byte) (var2 >> 16 | (var1 ? 192 : 64)));
            this.buffer.putShort(2, (short) (var2 & '\uffff'));
            this.buffer.position(1);
        } else {
            this.buffer.putInt(0, var2 | (var1 ? -536870912 : 1610612736));
            this.buffer.position(0);
        }

        if (var3) {
            if (this.buffer.position() == 0) {
                throw new IllegalArgumentException("Invalid special message size: " + var2);
            }

            this.buffer.position(this.buffer.position() - 1);
            this.buffer.put(this.buffer.position(), (byte) 0);
        }

    }

    private void b(boolean var1, int var2, boolean var3) {
        if (var3) {
            this.buffer.put((byte) 0);
        }

        if (var2 <= 31) {
            this.buffer.put((byte) (var2 | (var1 ? 128 : 0)));
        } else if (var2 <= 8191) {
            this.buffer.putShort((short) (var2 | (var1 ? 'ꀀ' : 8192)));
        } else if (var2 <= 2097151) {
            this.buffer.put((byte) (var2 >> 16 | (var1 ? 192 : 64)));
            this.buffer.putShort((short) (var2 & '\uffff'));
        } else {
            this.buffer.putInt(var2 | (var1 ? -536870912 : 1610612736));
        }

    }

    public boolean a(Channel var1) throws IOException {
        int var2 = ((ByteChannel) var1).write(this.buffer);
        if (var2 > 0) {
            this.pb += var2;
            ++this.ph;
        } else {
            ++this.pg;
        }

        if (!this.buffer.hasRemaining()) {
            this.buffer.clear();
            this.pf = false;
        }

        return var2 > 0;
    }

    public int gG() {
        return this.pb;
    }

    public int gH() {
        return this.pe;
    }

    public int gI() {
        return this.pd;
    }

    public int gJ() {
        return this.pg;
    }

    public int gK() {
        return this.ph;
    }
}
