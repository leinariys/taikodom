package all;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * EventManager Блокировщик чего-то или выполнитель задачь в главном потоке для графического движка
 */
public class EventManager extends Zu implements ajJ_q {
    private aMQ_q oT = new aMQ_q();
    private BlockingQueue oU = new LinkedBlockingQueue();

    public void publish(Object var1) {
        this.a("", var1);
    }

    public void a(String var1, Object var2) {
        this.oT.put(var1, var2);
        this.d(var1, var2);
    }

    public void b(String var1, Object var2) {
        this.oT.k(var1, var2);
        this.m(var1, var2);
    }

    public void g(Object var1) {
        this.b("", var1);
    }

    public void h(Object var1) {
        this.d("", var1);
    }

    public void i(Object var1) {
        this.c("", var1);
    }

    public void c(String var1, Object var2) {
        this.oU.add(new Km(var1, var2));
    }

    public void d(String var1, Object var2) {
        this.l(var1, var2);
    }

    public Collection a(String var1, Class var2) {
        ArrayList var3 = null;
        Collection var4 = this.oT.aR(var1);
        if (var4 == null) {
            return Collections.EMPTY_LIST;
        } else {
            Iterator var6 = var4.iterator();

            while (var6.hasNext()) {
                Object var5 = var6.next();
                if (var5.getClass() == var2) {
                    if (var3 == null) {
                        var3 = new ArrayList();
                    }

                    var3.add(var5);
                }
            }

            return (Collection) (var3 != null ? var3 : Collections.EMPTY_LIST);
        }
    }

    public Collection b(Class var1) {
        return this.a("", var1);
    }

    public void gz() {
        try {
            Km var1 = (Km) this.oU.take();
            this.d((String) var1.first(), var1.bcA());
        } catch (InterruptedException var2) {
            var2.printStackTrace();
        }

    }

    public int gA() {
        return this.oU.size();
    }

    public Collection j(Object var1) {
        return this.e("", var1);
    }

    public Collection e(String var1, Object var2) {
        ArrayList var3 = null;
        Collection var4 = this.oT.aR(var1);
        Iterator var6 = var4.iterator();

        while (var6.hasNext()) {
            Object var5 = var6.next();
            if (var5.equals(var2)) {
                if (var3 == null) {
                    var3 = new ArrayList();
                }

                var3.add(var5);
            }
        }

        return (Collection) (var3 != null ? var3 : Collections.EMPTY_LIST);
    }


}
