package all;

import com.hoplon.geometry.Vec3f;

public class aTw {
    private static final float iPG = 0.001F;
    private Vec3f Oj = new Vec3f();
    private float distance = 0.0F;

    public void normalize() {
        float var1 = this.Oj.lengthSquared();
        if (var1 > 0.0F) {
            float var2 = 1.0F / (float) Math.sqrt((double) var1);
            this.Oj.x *= var2;
            this.Oj.y *= var2;
            this.Oj.z *= var2;
            this.distance *= var2;
        }

    }

    public a bV(Vec3f var1) {
        float var2 = var1.x * this.Oj.x + var1.y * this.Oj.y + var1.z * this.Oj.z + this.distance;
        if (var2 > 0.001F) {
            return a.jaW;
        } else {
            return var2 < -0.001F ? a.jaX : a.jaV;
        }
    }

    public void t(Vec3f var1, Vec3f var2, Vec3f var3) {
        float var4 = var2.x - var1.x;
        float var5 = var2.z - var1.z;
        float var6 = var2.z - var1.z;
        float var7 = var3.x - var1.x;
        float var8 = var3.z - var1.z;
        float var9 = var3.z - var1.z;
        this.Oj.x = var5 * var9 - var6 * var8;
        this.Oj.y = var6 * var7 - var4 * var9;
        this.Oj.z = var4 * var8 - var5 * var7;
        this.Oj.normalize();
        this.distance = -(this.Oj.x * var1.x + this.Oj.y * var1.y + this.Oj.z * var1.z);
    }

    public void b(aTw var1) {
        this.Oj.set(var1.Oj);
        this.distance = var1.distance;
    }

    public void a(BM_q var1) {
        this.Oj.set(var1.getNormal().getVec3f());
        this.distance = (float) var1.dnz();
    }

    public void b(float var1, float var2, float var3) {
        this.Oj.set(var1, var2, var3);
    }

    public void l(Vec3f var1) {
        this.Oj.set(var1);
    }

    public Vec3f vO() {
        return this.Oj;
    }

    public float getDistance() {
        return this.distance;
    }

    public void setDistance(float var1) {
        this.distance = var1;
    }

    public void x(float var1, float var2, float var3, float var4) {
        this.Oj.x = var1;
        this.Oj.y = var2;
        this.Oj.z = var3;
        this.distance = var4;
    }

    public float bW(Vec3f var1) {
        return var1.x * this.Oj.x + var1.y * this.Oj.y + var1.z * this.Oj.z + this.distance;
    }

    public static enum a {
        jaV,
        jaW,
        jaX;
    }
}
