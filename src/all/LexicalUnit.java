package all;
//package org.w3c.css.sac;
//LexicalUnit

import java.io.Serializable;

public class LexicalUnit implements Serializable {
    short SAC_OPERATOR_COMMA = 0;
    short SAC_OPERATOR_PLUS = 1;
    short SAC_OPERATOR_MINUS = 2;
    short SAC_OPERATOR_MULTIPLY = 3;
    short SAC_OPERATOR_SLASH = 4;
    short SAC_OPERATOR_MOD = 5;
    short SAC_OPERATOR_EXP = 6;
    short SAC_OPERATOR_LT = 7;
    short SAC_OPERATOR_GT = 8;
    short SAC_OPERATOR_LE = 9;
    short SAC_OPERATOR_GE = 10;
    short SAC_OPERATOR_TILDE = 11;
    short SAC_INHERIT = 12;
    short SAC_INTEGER = 13;
    short SAC_REAL = 14;
    short SAC_EM = 15;
    short SAC_EX = 16;
    short SAC_PIXEL = 17;
    short SAC_INCH = 18;
    short SAC_CENTIMETER = 19;
    short SAC_MILLIMETER = 20;
    short SAC_POINT = 21;
    short SAC_PICA = 22;
    short SAC_PERCENTAGE = 23;
    short SAC_URI = 24;
    short SAC_COUNTER_FUNCTION = 25;
    short SAC_COUNTERS_FUNCTION = 26;
    short SAC_RGBCOLOR = 27;
    short SAC_DEGREE = 28;
    short SAC_GRADIAN = 29;
    short SAC_RADIAN = 30;
    short SAC_MILLISECOND = 31;
    short SAC_SECOND = 32;
    short SAC_HERTZ = 33;
    short SAC_KILOHERTZ = 34;
    short SAC_IDENT = 35;
    short SAC_STRING_VALUE = 36;
    short SAC_ATTR = 37;
    short SAC_RECT_FUNCTION = 38;
    short SAC_UNICODERANGE = 39;
    short SAC_SUB_EXPRESSION = 40;
    short SAC_FUNCTION = 41;
    short SAC_DIMENSION = 42;


    private short lexicalUnitType;
    private LexicalUnit nextLexicalUnit;
    private LexicalUnit previousLexicalUnit;
    private float floatValue;
    private String CH;
    private String functionName;
    private LexicalUnit subValues;
    private String stringValue;

    protected LexicalUnit(LexicalUnit var1, short var2) {
        this.lexicalUnitType = var2;
        this.previousLexicalUnit = var1;
        if (this.previousLexicalUnit != null) {
            ((LexicalUnit) this.previousLexicalUnit).nextLexicalUnit = this;
        }

    }

    protected LexicalUnit(LexicalUnit var1, int var2) {
        this(var1, (short) 13);
        this.floatValue = (float) var2;
    }

    protected LexicalUnit(LexicalUnit var1, short var2, float var3) {
        this(var1, var2);
        this.floatValue = var3;
    }

    protected LexicalUnit(LexicalUnit var1, short var2, String var3, float var4) {
        this(var1, var2);
        this.CH = var3 != null ? var3.intern() : null;
        this.floatValue = var4;
    }

    protected LexicalUnit(LexicalUnit var1, short var2, String var3) {
        this(var1, var2);
        this.stringValue = var3 != null ? var3.intern() : null;
    }

    protected LexicalUnit(LexicalUnit var1, short var2, String var3, LexicalUnit var4) {
        this(var1, var2);
        this.functionName = var3 != null ? var3.intern() : null;
        this.subValues = var4;
    }

    private static float a(char var0, String var1) {
        return (float) (var0 == '-' ? -1 : 1) * Float.valueOf(var1).floatValue();
    }

    public static LexicalUnit perseValueColor(LexicalUnit var0, float var1) {
        return var1 > (float) ((int) var1) ? new LexicalUnit(var0, (short) 14, var1) : new LexicalUnit(var0, (int) var1);
    }

    public static LexicalUnit b(LexicalUnit var0, float var1) {
        return new LexicalUnit(var0, (short) 23, var1);
    }

    public static LexicalUnit c(LexicalUnit var0, float var1) {
        return new LexicalUnit(var0, (short) 17, var1);
    }

    public static LexicalUnit d(LexicalUnit var0, float var1) {
        return new LexicalUnit(var0, (short) 19, var1);
    }

    public static LexicalUnit e(LexicalUnit var0, float var1) {
        return new LexicalUnit(var0, (short) 20, var1);
    }

    public static LexicalUnit f(LexicalUnit var0, float var1) {
        return new LexicalUnit(var0, (short) 18, var1);
    }

    public static LexicalUnit g(LexicalUnit var0, float var1) {
        return new LexicalUnit(var0, (short) 21, var1);
    }

    public static LexicalUnit h(LexicalUnit var0, float var1) {
        return new LexicalUnit(var0, (short) 22, var1);
    }

    public static LexicalUnit i(LexicalUnit var0, float var1) {
        return new LexicalUnit(var0, (short) 15, var1);
    }

    public static LexicalUnit j(LexicalUnit var0, float var1) {
        return new LexicalUnit(var0, (short) 16, var1);
    }

    public static LexicalUnit k(LexicalUnit var0, float var1) {
        return new LexicalUnit(var0, (short) 28, var1);
    }

    public static LexicalUnit l(LexicalUnit var0, float var1) {
        return new LexicalUnit(var0, (short) 30, var1);
    }

    public static LexicalUnit m(LexicalUnit var0, float var1) {
        return new LexicalUnit(var0, (short) 29, var1);
    }

    public static LexicalUnit n(LexicalUnit var0, float var1) {
        return new LexicalUnit(var0, (short) 31, var1);
    }

    public static LexicalUnit o(LexicalUnit var0, float var1) {
        return new LexicalUnit(var0, (short) 32, var1);
    }

    public static LexicalUnit p(LexicalUnit var0, float var1) {
        return new LexicalUnit(var0, (short) 33, var1);
    }

    public static LexicalUnit a(LexicalUnit var0, float var1, String var2) {
        return new LexicalUnit(var0, (short) 42, var2, var1);
    }

    public static LexicalUnit q(LexicalUnit var0, float var1) {
        return new LexicalUnit(var0, (short) 34, var1);
    }

    public static LexicalUnit a(LexicalUnit var0, LexicalUnit var1) {
        return new LexicalUnit(var0, (short) 25, "counter", var1);
    }

    public static LexicalUnit b(LexicalUnit var0, LexicalUnit var1) {
        return new LexicalUnit(var0, (short) 26, "counters", var1);
    }

    public static LexicalUnit c(LexicalUnit var0, LexicalUnit var1) {
        return new LexicalUnit(var0, (short) 37, "attr", var1);
    }

    public static LexicalUnit d(LexicalUnit var0, LexicalUnit var1) {
        return new LexicalUnit(var0, (short) 38, "rect", var1);
    }

    public static LexicalUnit e(LexicalUnit var0, LexicalUnit var1) {
        return new LexicalUnit(var0, (short) 27, "rgb", var1);
    }

    public static LexicalUnit a(LexicalUnit var0, String var1, LexicalUnit var2) {
        return new LexicalUnit(var0, (short) 41, var1, var2);
    }

    public static LexicalUnit a(LexicalUnit var0, String var1) {
        return new LexicalUnit(var0, (short) 36, var1);
    }

    public static LexicalUnit b(LexicalUnit var0, String var1) {
        return new LexicalUnit(var0, (short) 35, var1);
    }

    public static LexicalUnit c(LexicalUnit var0, String var1) {
        return new LexicalUnit(var0, (short) 24, var1);
    }

    public static LexicalUnit a(LexicalUnit var0) {
        return new LexicalUnit(var0, (short) 0);
    }

    public short getLexicalUnitType() {
        return this.lexicalUnitType;
    }

    public LexicalUnit getNextLexicalUnit() {
        return this.nextLexicalUnit;
    }

    public LexicalUnit getPreviousLexicalUnit() {
        return this.previousLexicalUnit;
    }

    public int getIntegerValue() {
        return (int) this.floatValue;
    }

    public float getFloatValue() {
        return this.floatValue;
    }

    public String getDimensionUnitText() {
        switch (this.lexicalUnitType) {
            case 15:
                return "em";
            case 16:
                return "ex";
            case 17:
                return "px";
            case 18:
                return "in";
            case 19:
                return "cm";
            case 20:
                return "mm";
            case 21:
                return "pt";
            case 22:
                return "pc";
            case 23:
                return "%";
            case 24:
            case 25:
            case 26:
            case 27:
            case 35:
            case 36:
            case 37:
            case 38:
            case 39:
            case 40:
            case 41:
            default:
                return "";
            case 28:
                return "deg";
            case 29:
                return "grad";
            case 30:
                return "rad";
            case 31:
                return "ms";
            case 32:
                return "s";
            case 33:
                return "Hz";
            case 34:
                return "kHz";
            case 42:
                return this.CH;
        }
    }

    public String getFunctionName() {
        return this.functionName;
    }

    public LexicalUnit getParameters() {
        return this.subValues;
    }

    public String getStringValue() {
        return this.stringValue;
    }

    public LexicalUnit getSubValues() {
        return this.subValues;
    }

    public String toString() {
        StringBuffer var1 = new StringBuffer();
        switch (this.lexicalUnitType) {
            case 0:
                var1.append(",");
                break;
            case 1:
                var1.append("+");
                break;
            case 2:
                var1.append("-");
                break;
            case 3:
                var1.append("*");
                break;
            case 4:
                var1.append("/");
                break;
            case 5:
                var1.append("%");
                break;
            case 6:
                var1.append("^");
                break;
            case 7:
                var1.append("<");
                break;
            case 8:
                var1.append(">");
                break;
            case 9:
                var1.append("<=");
                break;
            case 10:
                var1.append(">=");
                break;
            case 11:
                var1.append("~");
                break;
            case 12:
                var1.append("inherit");
                break;
            case 13:
                var1.append(String.valueOf(this.getIntegerValue()));
                break;
            case 14:
                var1.append(this.O(this.getFloatValue()));
                break;
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 28:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
            case 42:
                var1.append(this.O(this.getFloatValue())).append(this.getDimensionUnitText());
                break;
            case 24:
                var1.append("url(").append(this.getStringValue()).append(")");
                break;
            case 25:
                var1.append("counter(");
                this.a(var1, this.subValues);
                var1.append(")");
                break;
            case 26:
                var1.append("counters(");
                this.a(var1, this.subValues);
                var1.append(")");
                break;
            case 27:
                var1.append("rgb(");
                this.a(var1, this.subValues);
                var1.append(")");
                break;
            case 35:
                var1.append(this.getStringValue());
                break;
            case 36:
                var1.append("\"").append(this.getStringValue()).append("\"");
                break;
            case 37:
                var1.append("attr(");
                this.a(var1, this.subValues);
                var1.append(")");
                break;
            case 38:
                var1.append("rect(");
                this.a(var1, this.subValues);
                var1.append(")");
                break;
            case 39:
                var1.append(this.getStringValue());
                break;
            case 40:
                var1.append(this.getStringValue());
                break;
            case 41:
                var1.append(this.getFunctionName());
                this.a(var1, this.subValues);
                var1.append(")");
        }

        return var1.toString();
    }

    public String toDebugString() {
        StringBuffer var1 = new StringBuffer();
        switch (this.lexicalUnitType) {
            case 0:
                var1.append("SAC_OPERATOR_COMMA");
                break;
            case 1:
                var1.append("SAC_OPERATOR_PLUS");
                break;
            case 2:
                var1.append("SAC_OPERATOR_MINUS");
                break;
            case 3:
                var1.append("SAC_OPERATOR_MULTIPLY");
                break;
            case 4:
                var1.append("SAC_OPERATOR_SLASH");
                break;
            case 5:
                var1.append("SAC_OPERATOR_MOD");
                break;
            case 6:
                var1.append("SAC_OPERATOR_EXP");
                break;
            case 7:
                var1.append("SAC_OPERATOR_LT");
                break;
            case 8:
                var1.append("SAC_OPERATOR_GT");
                break;
            case 9:
                var1.append("SAC_OPERATOR_LE");
                break;
            case 10:
                var1.append("SAC_OPERATOR_GE");
                break;
            case 11:
                var1.append("SAC_OPERATOR_TILDE");
                break;
            case 12:
                var1.append("SAC_INHERIT");
                break;
            case 13:
                var1.append("SAC_INTEGER(").append(String.valueOf(this.getIntegerValue())).append(")");
                break;
            case 14:
                var1.append("SAC_REAL(").append(this.O(this.getFloatValue())).append(")");
                break;
            case 15:
                var1.append("SAC_EM(").append(this.O(this.getFloatValue())).append(this.getDimensionUnitText()).append(")");
                break;
            case 16:
                var1.append("SAC_EX(").append(this.O(this.getFloatValue())).append(this.getDimensionUnitText()).append(")");
                break;
            case 17:
                var1.append("SAC_PIXEL(").append(this.O(this.getFloatValue())).append(this.getDimensionUnitText()).append(")");
                break;
            case 18:
                var1.append("SAC_INCH(").append(this.O(this.getFloatValue())).append(this.getDimensionUnitText()).append(")");
                break;
            case 19:
                var1.append("SAC_CENTIMETER(").append(this.O(this.getFloatValue())).append(this.getDimensionUnitText()).append(")");
                break;
            case 20:
                var1.append("SAC_MILLIMETER(").append(this.O(this.getFloatValue())).append(this.getDimensionUnitText()).append(")");
                break;
            case 21:
                var1.append("SAC_POINT(").append(this.O(this.getFloatValue())).append(this.getDimensionUnitText()).append(")");
                break;
            case 22:
                var1.append("SAC_PICA(").append(this.O(this.getFloatValue())).append(this.getDimensionUnitText()).append(")");
                break;
            case 23:
                var1.append("SAC_PERCENTAGE(").append(this.O(this.getFloatValue())).append(this.getDimensionUnitText()).append(")");
                break;
            case 24:
                var1.append("SAC_URI(url(").append(this.getStringValue()).append("))");
                break;
            case 25:
                var1.append("SAC_COUNTER_FUNCTION(counter(");
                this.a(var1, this.subValues);
                var1.append("))");
                break;
            case 26:
                var1.append("SAC_COUNTERS_FUNCTION(counters(");
                this.a(var1, this.subValues);
                var1.append("))");
                break;
            case 27:
                var1.append("SAC_RGBCOLOR(rgb(");
                this.a(var1, this.subValues);
                var1.append("))");
                break;
            case 28:
                var1.append("SAC_DEGREE(").append(this.O(this.getFloatValue())).append(this.getDimensionUnitText()).append(")");
                break;
            case 29:
                var1.append("SAC_GRADIAN(").append(this.O(this.getFloatValue())).append(this.getDimensionUnitText()).append(")");
                break;
            case 30:
                var1.append("SAC_RADIAN(").append(this.O(this.getFloatValue())).append(this.getDimensionUnitText()).append(")");
                break;
            case 31:
                var1.append("SAC_MILLISECOND(").append(this.O(this.getFloatValue())).append(this.getDimensionUnitText()).append(")");
                break;
            case 32:
                var1.append("SAC_SECOND(").append(this.O(this.getFloatValue())).append(this.getDimensionUnitText()).append(")");
                break;
            case 33:
                var1.append("SAC_HERTZ(").append(this.O(this.getFloatValue())).append(this.getDimensionUnitText()).append(")");
                break;
            case 34:
                var1.append("SAC_KILOHERTZ(").append(this.O(this.getFloatValue())).append(this.getDimensionUnitText()).append(")");
                break;
            case 35:
                var1.append("SAC_IDENT(").append(this.getStringValue()).append(")");
                break;
            case 36:
                var1.append("SAC_STRING_VALUE(\"").append(this.getStringValue()).append("\")");
                break;
            case 37:
                var1.append("SAC_ATTR(attr(");
                this.a(var1, this.subValues);
                var1.append("))");
                break;
            case 38:
                var1.append("SAC_RECT_FUNCTION(rect(");
                this.a(var1, this.subValues);
                var1.append("))");
                break;
            case 39:
                var1.append("SAC_UNICODERANGE(").append(this.getStringValue()).append(")");
                break;
            case 40:
                var1.append("SAC_SUB_EXPRESSION(").append(this.getStringValue()).append(")");
                break;
            case 41:
                var1.append("SAC_FUNCTION(").append(this.getFunctionName()).append("(");
                this.a(var1, this.subValues);
                var1.append("))");
                break;
            case 42:
                var1.append("SAC_DIMENSION(").append(this.O(this.getFloatValue())).append(this.getDimensionUnitText()).append(")");
        }

        return var1.toString();
    }

    private void a(StringBuffer var1, LexicalUnit var2) {
        for (LexicalUnit var3 = var2; var3 != null; var3 = var3.getNextLexicalUnit()) {
            var1.append(var3.toString());
        }

    }

    private String O(float var1) {
        String var2 = String.valueOf(this.getFloatValue());
        return var1 - (float) ((int) var1) != 0.0F ? var2 : var2.substring(0, var2.length() - 2);
    }
}
