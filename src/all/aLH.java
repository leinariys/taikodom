package all;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;

import javax.vecmath.Tuple3d;
import java.io.Serializable;

public class aLH implements afA_q, Serializable {
    static final Vector3dOperations cRz = new Vector3dOperations();
    static final Vector3dOperations cRA = new Vector3dOperations();
    static final Vector3dOperations cRB = new Vector3dOperations();
    static final Vector3dOperations imm = new Vector3dOperations();
    static final Vector3dOperations imn = new Vector3dOperations();
    static final Vector3dOperations imo = new Vector3dOperations();
    static final Vector3dOperations imp = new Vector3dOperations();
    static final Vector3dOperations imq = new Vector3dOperations();
    private Vector3dOperations imk;
    private Vector3dOperations iml;


    public aLH() {
        this.imk = new Vector3dOperations();
        this.iml = new Vector3dOperations();
        this.imk.set(Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY);
        this.iml.set(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);
    }

    public aLH(Vector3dOperations var1, Vector3dOperations var2) {
        this.imk = new Vector3dOperations();
        this.iml = new Vector3dOperations();
        this.imk.aA(var2);
        this.iml.aA(var1);
    }

    public aLH(double var1, double var3, double var5, double var7, double var9, double var11) {
        this(new Vector3dOperations(var1, var3, var5), new Vector3dOperations(var7, var9, var11));
    }

    public aLH(double[] var1) {
        this.imk = new Vector3dOperations();
        this.iml = new Vector3dOperations();
        this.iml.set(var1[0], var1[1], var1[2]);
        this.imk.set(var1[3], var1[4], var1[5]);
    }

    public boolean equals(Object var1) {
        if (var1 == this) {
            return true;
        } else if (var1 == null) {
            return false;
        } else if (var1.getClass() != this.getClass()) {
            return false;
        } else {
            aLH var2 = (aLH) var1;
            return this.imk.equals(var2.imk) && this.iml.equals(var2.iml);
        }
    }

    public void aD(Vector3dOperations var1) {
        this.iml.aA(var1);
    }

    public Vector3dOperations dim() {
        return this.iml;
    }

    public void aE(Vector3dOperations var1) {
        this.imk.aA(var1);
    }

    public Vector3dOperations din() {
        return this.imk;
    }

    public float dio() {
        float var1 = (float) (this.imk.x - this.iml.x);
        return (double) var1 > 1.0E26D ? 0.0F : Math.max(0.0F, var1);
    }

    public float dip() {
        float var1 = (float) (this.imk.y - this.iml.y);
        return (double) var1 > 1.0E26D ? 0.0F : Math.max(0.0F, var1);
    }

    public float diq() {
        float var1 = (float) (this.imk.z - this.iml.z);
        return (double) var1 > 1.0E26D ? 0.0F : Math.max(0.0F, var1);
    }

    public float dir() {
        float var1 = this.dio();
        float var2 = this.dip();
        return (float) Math.sqrt((double) (var1 * var1 + var2 * var2));
    }

    public float dis() {
        float var1 = this.dip();
        float var2 = this.diq();
        return (float) Math.sqrt((double) (var2 * var2 + var1 * var1));
    }

    public float clX() {
        float var1 = this.dio();
        float var2 = this.dip();
        float var3 = this.diq();
        return var1 * var1 + var2 * var2 + var3 * var3;
    }

    public float length() {
        return (float) Math.sqrt((double) this.lengthSquared());
    }

    public float lengthSquared() {
        float var1 = this.dio();
        float var2 = this.dip();
        float var3 = this.diq();
        return var1 * var1 + var2 * var2 + var3 * var3;
    }

    public float dit() {
        return (float) Math.max(this.iml.length(), this.imk.length());
    }

    public float diu() {
        return (float) Math.max(this.iml.lengthSquared(), this.imk.lengthSquared());
    }

    public boolean a(Pt var1) {
        float var2 = 0.0F;
        float var3;
        if ((double) var1.bny().x < this.iml.x) {
            var3 = (float) ((double) var1.bny().x - this.iml.x);
            var2 = var3 * var3;
        }

        if ((double) var1.bny().y < this.iml.y) {
            var3 = (float) ((double) var1.bny().y - this.iml.y);
            var2 += var3 * var3;
        }

        if ((double) var1.bny().z < this.iml.z) {
            var3 = (float) ((double) var1.bny().z - this.iml.z);
            var2 += var3 * var3;
        }

        if ((double) var1.bny().x > this.imk.x) {
            var3 = (float) ((double) var1.bny().x - this.imk.x);
            var2 += var3 * var3;
        }

        if ((double) var1.bny().y > this.imk.y) {
            var3 = (float) ((double) var1.bny().y - this.imk.y);
            var2 += var3 * var3;
        }

        if ((double) var1.bny().z > this.imk.z) {
            var3 = (float) ((double) var1.bny().z - this.imk.z);
            var2 += var3 * var3;
        }

        return var2 <= var1.getRadius() * var1.getRadius();
    }

    public boolean a(aVS var1) {
        double var2 = 0.0D;
        double var4;
        if (var1.zO().x < this.iml.x) {
            var4 = var1.zO().x - this.iml.x;
            var2 = var4 * var4;
        }

        if (var1.zO().y < this.iml.y) {
            var4 = var1.zO().y - this.iml.y;
            var2 += var4 * var4;
        }

        if (var1.zO().z < this.iml.z) {
            var4 = var1.zO().z - this.iml.z;
            var2 += var4 * var4;
        }

        if (var1.zO().x > this.imk.x) {
            var4 = var1.zO().x - this.imk.x;
            var2 += var4 * var4;
        }

        if (var1.zO().y > this.imk.y) {
            var4 = var1.zO().y - this.imk.y;
            var2 += var4 * var4;
        }

        if (var1.zO().z > this.imk.z) {
            var4 = var1.zO().z - this.imk.z;
            var2 += var4 * var4;
        }

        return var2 <= var1.dCJ() * var1.dCJ();
    }

    public boolean a(aLH var1) {
        return this.e(var1);
    }

    public boolean e(aLH var1) {
        Vector3dOperations var2 = var1.din();
        Vector3dOperations var3 = this.iml;
        if (var3.x > var2.x) {
            return false;
        } else {
            Vector3dOperations var4 = var1.dim();
            Vector3dOperations var5 = this.imk;
            if (var5.x < var4.x) {
                return false;
            } else if (var3.y > var2.y) {
                return false;
            } else if (var5.y < var4.y) {
                return false;
            } else if (var3.z > var2.z) {
                return false;
            } else {
                return var5.z >= var4.z;
            }
        }
    }

    public aDM a(Vector3dOperations var1, Vector3dOperations var2, boolean var3) {
        aDM var4 = new aDM(var1, var2);
        boolean var5 = true;
        double var8 = 0.0D;
        double var6;
        if (var1.x < this.iml.x) {
            var6 = this.iml.x - var1.x;
            if (var6 > var4.getRayDelta().x) {
                return var4;
            }

            var6 /= var4.getRayDelta().x;
            var5 = false;
            var8 = -1.0D;
        } else if (var1.x > this.imk.x) {
            var6 = this.imk.x - var1.x;
            if (var6 < var4.getRayDelta().x) {
                return var4;
            }

            var6 /= var4.getRayDelta().x;
            var5 = false;
            var8 = 1.0D;
        } else {
            var6 = -1.0D;
        }

        double var12 = 0.0D;
        double var10;
        if (var1.y < this.iml.y) {
            var10 = this.iml.y - var1.y;
            if (var10 > var4.getRayDelta().y) {
                return var4;
            }

            var10 /= var4.getRayDelta().y;
            var5 = false;
            var12 = -1.0D;
        } else if (var1.y > this.imk.y) {
            var10 = this.imk.y - var1.y;
            if (var10 < var4.getRayDelta().y) {
                return var4;
            }

            var10 /= var4.getRayDelta().y;
            var5 = false;
            var12 = 1.0D;
        } else {
            var10 = -1.0D;
        }

        double var16 = 0.0D;
        double var14;
        if (var1.z < this.iml.z) {
            var14 = this.iml.z - var1.z;
            if (var14 > var4.getRayDelta().z) {
                return var4;
            }

            var14 /= var4.getRayDelta().z;
            var5 = false;
            var16 = -1.0D;
        } else if (var1.z > this.imk.z) {
            var14 = this.imk.z - var1.z;
            if (var14 < var4.getRayDelta().z) {
                return var4;
            }

            var14 /= var4.getRayDelta().z;
            var5 = false;
            var16 = 1.0D;
        } else {
            var14 = -1.0D;
        }

        if (var5) {
            if (var3) {
                var4.setNormal(var4.getRayDelta().dfZ().dfX());
                var4.setParamIntersection(0.0D);
                var4.setIntersected(true);
                return var4;
            } else {
                return var4;
            }
        } else {
            byte var18 = 0;
            double var19 = var6;
            if (var10 > var6) {
                var18 = 1;
                var19 = var10;
            }

            if (var14 > var19) {
                var18 = 2;
                var19 = var14;
            }

            double var21;
            double var23;
            switch (var18) {
                case 0:
                    var21 = var1.y + var4.getRayDelta().y * var19;
                    if (var21 < this.iml.y || var21 > this.imk.y) {
                        return var4;
                    }

                    var23 = var1.z + var4.getRayDelta().z * var19;
                    if (var23 < this.iml.z || var23 > this.imk.z) {
                        return var4;
                    }

                    var4.getNormal().set(var8, 0.0D, 0.0D);
                    break;
                case 1:
                    var21 = var1.x + var4.getRayDelta().x * var19;
                    if (var21 >= this.iml.x && var21 <= this.imk.x) {
                        var23 = var1.z + var4.getRayDelta().z * var19;
                        if (var23 < this.iml.z || var23 > this.imk.z) {
                            return var4;
                        }

                        var4.getNormal().set(0.0D, var12, 0.0D);
                        break;
                    }

                    return var4;
                case 2:
                    var21 = var1.x + var4.getRayDelta().x * var19;
                    if (var21 < this.iml.x || var21 > this.imk.x) {
                        return var4;
                    }

                    var23 = var1.y + var4.getRayDelta().y * var19;
                    if (var23 < this.iml.y || var23 > this.imk.y) {
                        return var4;
                    }

                    var4.getNormal().set(0.0D, 0.0D, var16);
            }

            var4.setParamIntersection(var19);
            var4.setIntersected(true);
            return var4;
        }
    }

    public void reset() {
        this.iml.x = this.iml.y = this.iml.z = Double.POSITIVE_INFINITY;
        this.imk.x = this.imk.y = this.imk.z = Double.NEGATIVE_INFINITY;
    }

    public void aF(Vector3dOperations var1) {
        this.imk.x = this.iml.x = var1.x;
        this.imk.y = this.iml.y = var1.y;
        this.imk.z = this.iml.z = var1.z;
    }

    public void aG(Vector3dOperations var1) {
        if (var1.x < this.iml.x) {
            this.iml.x = var1.x;
        }

        if (var1.y < this.iml.y) {
            this.iml.y = var1.y;
        }

        if (var1.z < this.iml.z) {
            this.iml.z = var1.z;
        }

        if (var1.x > this.imk.x) {
            this.imk.x = var1.x;
        }

        if (var1.y > this.imk.y) {
            this.imk.y = var1.y;
        }

        if (var1.z > this.imk.z) {
            this.imk.z = var1.z;
        }

    }

    public void addPoint(Vec3f var1) {
        if ((double) var1.x < this.iml.x) {
            this.iml.x = (double) var1.x;
        }

        if ((double) var1.y < this.iml.y) {
            this.iml.y = (double) var1.y;
        }

        if ((double) var1.z < this.iml.z) {
            this.iml.z = (double) var1.z;
        }

        if ((double) var1.x > this.imk.x) {
            this.imk.x = (double) var1.x;
        }

        if ((double) var1.y > this.imk.y) {
            this.imk.y = (double) var1.y;
        }

        if ((double) var1.z > this.imk.z) {
            this.imk.z = (double) var1.z;
        }

    }

    public void a(ajK var1, aLH var2) {
        Vector3dOperations var4 = new Vector3dOperations(this.imk.x, this.imk.y, this.imk.z);
        Vector3dOperations var5 = new Vector3dOperations(this.imk.x, this.imk.y, this.iml.z);
        Vector3dOperations var6 = new Vector3dOperations(this.imk.x, this.iml.y, this.iml.z);
        Vector3dOperations var7 = new Vector3dOperations(this.imk.x, this.iml.y, this.imk.z);
        Vector3dOperations var8 = new Vector3dOperations(this.iml.x, this.imk.y, this.imk.z);
        Vector3dOperations var9 = new Vector3dOperations(this.iml.x, this.imk.y, this.iml.z);
        Vector3dOperations var10 = new Vector3dOperations(this.iml.x, this.iml.y, this.iml.z);
        Vector3dOperations var11 = new Vector3dOperations(this.iml.x, this.iml.y, this.imk.z);
        var2.reset();
        Vector3dOperations var3 = var1.a((Tuple3d) var4);
        var2.aG(var3);
        var3 = var1.a((Tuple3d) var5);
        var2.aG(var3);
        var3 = var1.a((Tuple3d) var6);
        var2.aG(var3);
        var3 = var1.a((Tuple3d) var7);
        var2.aG(var3);
        var3 = var1.a((Tuple3d) var8);
        var2.aG(var3);
        var3 = var1.a((Tuple3d) var9);
        var2.aG(var3);
        var3 = var1.a((Tuple3d) var10);
        var2.aG(var3);
        var3 = var1.a((Tuple3d) var11);
        var2.aG(var3);
    }

    public void b(ajK var1, aLH var2) {
        float var3 = Math.abs(var1.m00);
        float var4 = Math.abs(var1.m01);
        float var5 = Math.abs(var1.m02);
        float var6 = Math.abs(var1.m10);
        float var7 = Math.abs(var1.m11);
        float var8 = Math.abs(var1.m12);
        float var9 = Math.abs(var1.m20);
        float var10 = Math.abs(var1.m21);
        float var11 = Math.abs(var1.m22);
        float var12 = this.dio();
        float var13 = this.dip();
        float var14 = this.diq();
        float var15 = (var12 * var3 + var13 * var4 + var14 * var5) / 2.0F;
        float var16 = (var12 * var6 + var13 * var7 + var14 * var8) / 2.0F;
        float var17 = (var12 * var9 + var13 * var10 + var14 * var11) / 2.0F;
        float var18 = var1.m03;
        float var19 = var1.m13;
        float var20 = var1.m23;
        var2.imk.x = (double) (var18 + var15);
        var2.imk.y = (double) (var19 + var16);
        var2.imk.z = (double) (var20 + var17);
        var2.iml.x = (double) (var18 - var15);
        var2.iml.y = (double) (var19 - var16);
        var2.iml.z = (double) (var20 - var17);
    }

    public void a(OrientationBase var1, Vector3dOperations var2, aLH var3) {
        var3.reset();
        float var4 = var1.x;
        float var5 = var1.y;
        float var6 = var1.z;
        float var7 = var1.w;
        float var8 = var7 * var7 + var4 * var4 + var5 * var5 + var6 * var6;
        float var9;
        if (!OrientationBase.isZero(1.0F - var8)) {
            var9 = (float) Math.sqrt((double) var8);
            var4 /= var9;
            var5 /= var9;
            var6 /= var9;
            var7 /= var9;
        }

        var9 = var4 * var4;
        float var10 = var4 * var5;
        float var11 = var4 * var6;
        float var12 = var7 * var4;
        float var13 = var5 * var5;
        float var14 = var5 * var6;
        float var15 = var5 * var7;
        float var16 = var6 * var6;
        float var17 = var6 * var7;
        float var18 = Math.abs(1.0F - 2.0F * (var13 + var16));
        float var19 = Math.abs(2.0F * (var10 - var17));
        float var20 = Math.abs(2.0F * (var11 + var15));
        float var21 = Math.abs(2.0F * (var10 + var17));
        float var22 = Math.abs(1.0F - 2.0F * (var9 + var16));
        float var23 = Math.abs(2.0F * (var14 - var12));
        float var24 = Math.abs(2.0F * (var11 - var15));
        float var25 = Math.abs(2.0F * (var14 + var12));
        float var26 = Math.abs(1.0F - 2.0F * (var9 + var13));
        float var27 = this.dio();
        float var28 = this.dip();
        float var29 = this.diq();
        float var30 = (var27 * var18 + var28 * var19 + var29 * var20) / 2.0F;
        float var31 = (var27 * var21 + var28 * var22 + var29 * var23) / 2.0F;
        float var32 = (var27 * var24 + var28 * var25 + var29 * var26) / 2.0F;
        double var33 = var2.x;
        double var35 = var2.y;
        double var37 = var2.z;
        var3.imk.x = var33 + (double) var30;
        var3.imk.y = var35 + (double) var31;
        var3.imk.z = var37 + (double) var32;
        var3.iml.x = var33 - (double) var30;
        var3.iml.y = var35 - (double) var31;
        var3.iml.z = var37 - (double) var32;
    }

    public String toString() {
        return "[(" + this.iml.x + ", " + this.iml.y + ", " + this.iml.z + ") -> (" + this.imk.x + ", " + this.imk.y + ", " + this.imk.z + ")]";
    }

    public int hashCode() {
        return this.iml.hashCode() * this.imk.hashCode();
    }

    public aLH div() {
        return new aLH(this.iml, this.imk);
    }

    public Vector3dOperations zO() {
        new Vector3dOperations();
        Vector3dOperations var1 = this.imk.d((Tuple3d) this.iml);
        var1.scale(0.5D);
        return var1;
    }

    public void aH(Vector3dOperations var1) {
        var1.add(this.imk, this.iml);
        var1.scale(0.5D);
    }

    public void A(double var1, double var3, double var5) {
        this.imk.x = var1;
        this.imk.y = var3;
        this.imk.z = var5;
    }

    public void B(double var1, double var3, double var5) {
        this.iml.x = var1;
        this.iml.y = var3;
        this.iml.z = var5;
    }

    public void f(aLH var1) {
        Vector3dOperations var2 = var1.din();
        Vector3dOperations var3 = var1.dim();
        this.A(var2.x, var2.y, var2.z);
        this.B(var3.x, var3.y, var3.z);
    }

    public void a(BoundingBox var1, Vector3dOperations var2) {
        Vec3f var3 = var1.dqQ();
        Vec3f var4 = var1.dqP();
        this.imk.x = (double) var3.x + var2.x;
        this.imk.y = (double) var3.y + var2.y;
        this.imk.z = (double) var3.z + var2.z;
        this.iml.x = (double) var4.x + var2.x;
        this.iml.y = (double) var4.y + var2.y;
        this.iml.z = (double) var4.z + var2.z;
    }

    public void aI(Vector3dOperations var1) {
        this.iml.x += var1.x;
        this.iml.y += var1.y;
        this.iml.z += var1.z;
        this.imk.x += var1.x;
        this.imk.y += var1.y;
        this.imk.z += var1.z;
    }

    public void b(ti var1) {
        this.iml.x += var1.adw();
        this.iml.y += var1.adx();
        this.iml.z += var1.ady();
        this.imk.x += var1.adw();
        this.imk.y += var1.adx();
        this.imk.z += var1.ady();
    }

    public void g(aLH var1) {
        this.imk.aA(var1.din());
        this.iml.aA(var1.dim());
    }

    public void c(BoundingBox var1) {
        this.iml.setVec3f(var1.dqP());
        this.imk.setVec3f(var1.dqQ());
    }

    public void h(aLH var1) {
        this.aG(var1.imk);
        this.aG(var1.iml);
    }

    public void a(bc_q var1, aLH var2) {
        cRz.set(this.imk.x, this.imk.y, this.imk.z);
        cRA.set(this.imk.x, this.imk.y, this.iml.z);
        cRB.set(this.imk.x, this.iml.y, this.iml.z);
        imm.set(this.imk.x, this.iml.y, this.imk.z);
        imn.set(this.iml.x, this.imk.y, this.imk.z);
        imo.set(this.iml.x, this.imk.y, this.iml.z);
        imp.set(this.iml.x, this.iml.y, this.iml.z);
        imq.set(this.iml.x, this.iml.y, this.imk.z);
        var2.reset();
        var1.a(cRz);
        var2.aG(cRz);
        var1.a(cRA);
        var2.aG(cRA);
        var1.a(cRB);
        var2.aG(cRB);
        var1.a(imm);
        var2.aG(imm);
        var1.a(imn);
        var2.aG(imn);
        var1.a(imo);
        var2.aG(imo);
        var1.a(imp);
        var2.aG(imp);
        var1.a(imq);
        var2.aG(imq);
    }

    public void g(float var1, float var2, float var3, float var4, float var5, float var6) {
        this.iml.set((double) var1, (double) var2, (double) var3);
        this.imk.set((double) var4, (double) var5, (double) var6);
    }

    public void C(double var1, double var3, double var5) {
        if (var1 < this.iml.x) {
            this.iml.x = var1;
        }

        if (var3 < this.iml.y) {
            this.iml.y = var3;
        }

        if (var5 < this.iml.z) {
            this.iml.z = var5;
        }

        if (var1 > this.imk.x) {
            this.imk.x = var1;
        }

        if (var3 > this.imk.y) {
            this.imk.y = var3;
        }

        if (var5 > this.imk.z) {
            this.imk.z = var5;
        }

    }

    public void aJ(Vector3dOperations var1) {
        this.iml.x -= var1.x;
        this.iml.y -= var1.y;
        this.iml.z -= var1.z;
        this.imk.x -= var1.x;
        this.imk.y -= var1.y;
        this.imk.z -= var1.z;
    }

    public void aK(Vector3dOperations var1) {
        this.iml.add(var1);
        this.imk.add(var1);
    }

    public void D(double var1, double var3, double var5) {
        this.iml.x(var1, var3, var5);
        this.imk.x(var1, var3, var5);
    }

    public void E(double var1, double var3, double var5) {
        this.iml.x -= var1;
        this.iml.y -= var3;
        this.iml.z -= var5;
        this.imk.x += var1;
        this.imk.y += var3;
        this.imk.z += var5;
    }

    public void F(double var1, double var3, double var5) {
        this.iml.x *= var1;
        this.iml.y *= var3;
        this.iml.z *= var5;
        this.imk.x *= var1;
        this.imk.y *= var3;
        this.imk.z *= var5;
    }

    public void scale(double var1) {
        this.F(var1, var1, var1);
    }

    public void readExternal(Vm_q var1) {
        this.iml = (Vector3dOperations) var1.he("mins");
        this.imk = (Vector3dOperations) var1.he("maxes");
    }

    public void writeExternal(att var1) {
        var1.a("mins", (afA_q) this.iml);
        var1.a("maxes", (afA_q) this.imk);
    }

    public void G(double var1, double var3, double var5) {
        this.iml.o(var1, var3, var5);
        this.imk.z(var1, var3, var5);
    }
}
