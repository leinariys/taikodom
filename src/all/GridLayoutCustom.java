package all;

import org.w3c.dom.css.CSSStyleDeclaration;

import java.awt.*;
import java.io.StringReader;

public class GridLayoutCustom extends BaseLayoutCssValue {
    public static LogPrinter logger = LogPrinter.K(GridLayoutCustom.class);

    public Object createGridBagConstraints(String var1) {
        return var1;
    }

    public LayoutManager createLayout(Container var1, String var2) {
        GridLayout var3 = new GridLayout();
        if (var2 != null && !(var2 = var2.trim()).isEmpty()) {
            try {
                CSSStyleDeclaration var4 = ParserCss.getParserCss().getCSSStyleDeclaration(new InputSource(new StringReader(var2)));
                var3.setHgap((int) this.getValueCss(var4, "hgap", (float) var3.getHgap()));
                var3.setVgap((int) this.getValueCss(var4, "vgap", (float) var3.getVgap()));
                var3.setColumns((int) this.getValueCss(var4, "columns", (float) var3.getColumns()));
                var3.setRows((int) this.getValueCss(var4, "rows", (float) var3.getRows()));
            } catch (Exception var6) {
                logger.warn(var6);
            }
        }

        return var3;
    }
}
