package all;

public class FU {
    public static String eL(long var0) {
        return er("" + var0);
    }

    public static String e(long var0, int var2) {
        return er(String.format("%0" + var2 + "CreateJComponent", var0));
    }

    public static String er(String var0) {
        StringBuilder var1 = new StringBuilder();

        for (int var2 = 0; var2 < var0.length(); ++var2) {
            char var3 = var0.charAt(var2);
            if (var3 >= '0' && var3 <= '9') {
                var1.append((char) (65 + (var3 - 48)));
            } else {
                var1.append(var3);
            }
        }

        return var1.toString();
    }
}
