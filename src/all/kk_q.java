package all;

import javax.swing.*;
import java.awt.*;

public class kk_q {
    public static jL_q asO = new jL_q() {
        public void a(Component var1, float var2) {
            var1.setLocation((int) var2, var1.getY());
        }

        public float a(Component var1) {
            return (float) var1.getX();
        }
    };
    public static jL_q asP = new jL_q() {
        public void a(Component var1, float var2) {
            var1.setLocation(var1.getX(), (int) var2);
        }

        public float a(Component var1) {
            return (float) var1.getY();
        }
    };
    public static jL_q asQ = new jL_q() {
        public void a(Component var1, float var2) {
            var1.setSize(var1.getWidth(), (int) var2);
        }

        public float a(Component var1) {
            return (float) var1.getHeight();
        }
    };
    public static jL_q asR = new jL_q() {
        public void a(Component var1, float var2) {
            var1.setSize((int) var2, var1.getHeight());
        }

        public float a(Component var1) {
            return (float) var1.getWidth();
        }
    };
    public static jL_q asS = new jL_q() {
        public void a(Component var1, float var2) {
            ComponentManager var3 = (ComponentManager) ComponentManager.getCssHolder(var1);
            PropertiesUiFromCss var4 = var3.Vr();
            if (var4 != null) {
                int var5 = (int) var2;
                var4.setColorMultiplier(new Color(var5, var5, var5, var5));
                var1.repaint();
            }

        }

        public float a(Component var1) {
            ComponentManager var2 = (ComponentManager) ComponentManager.getCssHolder(var1);
            PropertiesUiFromCss var3 = var2.Vr();
            if (var3 != null) {
                Color var4 = var3.getColorMultiplier();
                if (var4 != null) {
                    return (float) var4.getAlpha();
                }
            }

            return 255.0F;
        }
    };
    public static jL_q asT = new jL_q() {
        private float bZy = 0.55F;
        private long bZz = 0L;

        @Override
        public void a(Component var1, float var2) {

        }

        @Override
        public float a(Component var1) {
            return c((JLabel) var1);
        }

        public void a(JLabel var1, float var2) {
            ((LabelUI) var1.getUI()).uk(Math.round(var2));
            double var3 = Math.random();
            if (var3 <= (double) this.bZy && this.bZz == 0L) {
                this.bZz = System.currentTimeMillis();
                String var5 = ComponentManager.getCssHolder(var1).Vr().auk();
                if (var5 != null) {
                    BaseUItegXML.thisClass.cx(var5);
                }
            }

            if (this.bZz != 0L && System.currentTimeMillis() > this.bZz + 75L) {
                this.bZz = 0L;
            }

            var1.repaint();
        }

        public float c(JLabel var1) {
            return (float) ((LabelUI) var1.getUI()).ctz();
        }
    };
    public static jL_q asU = new jL_q() {
        @Override
        public void a(Component var1, float var2) {
            a((ProgressBarCustomWrapper) var1, (float) var2);
        }

        @Override
        public float a(Component var1) {
            return a((ProgressBarCustomWrapper) var1);
        }

        public void a(ProgressBarCustomWrapper var1, float var2) {
            var1.setValue(var2);
        }

        public float a(ProgressBarCustomWrapper var1) {
            return (float) var1.getValue();
        }
    };
    public static jL_q asV = new jL_q() {
        @Override
        public void a(Component var1, float var2) {
            a((PictureCustomWrapper) var1, (float) var2);
        }

        @Override
        public float a(Component var1) {
            return a((PictureCustomWrapper) var1);
        }

        public void a(PictureCustomWrapper var1, float var2) {
            var1.kU(0.017453292F * var2);
            var1.repaint();
        }

        public float a(PictureCustomWrapper var1) {
            return var1.cCz();
        }
    };

    public static class a implements jL_q {
        private final Color enP;
        private final Color enQ;
        private float aSB;

        public a(Color var1, Color var2) {
            this.enP = var1;
            this.enQ = var2;
            this.aSB = 0.0F;
        }

        public void a(Component var1, float var2) {
            PropertiesUiFromCss var3 = ComponentManager.getCssHolder(var1).Vp();
            this.aSB = var2;
            float var4 = this.aSB / 255.0F;
            Color var5 = new Color(this.a(this.enP.getRed(), this.enQ.getRed(), var4), this.a(this.enP.getGreen(), this.enQ.getGreen(), var4), this.a(this.enP.getBlue(), this.enQ.getBlue(), var4));
            var3.setColorMultiplier(var5);
            var1.repaint();
        }

        private int a(int var1, int var2, float var3) {
            return (int) Math.ceil((double) ((float) var1 * var3 + (float) var2 * (1.0F - var3))) & 255;
        }

        public float a(Component var1) {
            return this.aSB;
        }
    }
}
