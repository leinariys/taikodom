package all;

import javax.vecmath.Tuple3d;

public class aDM {
    private final Vector3dOperations startPos;
    private final Vector3dOperations endPos;
    private final Vector3dOperations rayDelta;
    private boolean intersected = false;
    private double paramIntersection = Double.POSITIVE_INFINITY;
    private BM_q plane = new BM_q();

    public aDM(Vector3dOperations var1, Vector3dOperations var2) {
        this.startPos = new Vector3dOperations(var1);
        this.endPos = new Vector3dOperations(var2);
        this.rayDelta = var2.f((Tuple3d) var1);
    }

    public void set(aDM var1) {
        this.setIntersected(var1.intersected);
        this.setParamIntersection(var1.paramIntersection);
        this.setPlane(var1.plane);
    }

    public void set(gu_g var1) {
        this.setIntersected(var1.isIntersected());
        this.setParamIntersection((double) var1.vM());
        this.plane.b(var1.vN());
    }

    public final boolean isIntersected() {
        return this.intersected;
    }

    public final void setIntersected(boolean var1) {
        this.intersected = var1;
    }

    public final double getParamIntersection() {
        return this.paramIntersection;
    }

    public final void setParamIntersection(double var1) {
        this.paramIntersection = var1;
    }

    public final BM_q getPlane() {
        return this.plane;
    }

    public final void setPlane(BM_q var1) {
        this.plane.a(var1);
    }

    public void setNormal(double var1, double var3, double var5) {
        this.plane.setNormal(var1, var3, var5);
    }

    public Vector3dOperations getNormal() {
        return this.plane.getNormal();
    }

    public void setNormal(Vector3dOperations var1) {
        this.plane.setNormal(var1);
    }

    public final Vector3dOperations getStartPos() {
        return this.startPos;
    }

    public final Vector3dOperations getEndPos() {
        return this.endPos;
    }

    public final Vector3dOperations getRayDelta() {
        return this.rayDelta;
    }

    public void updateRay(Vector3dOperations var1, Vector3dOperations var2) {
        this.startPos.aA(var1);
        this.endPos.aA(var2);
        this.rayDelta.aA(var2.f((Tuple3d) var1));
    }
}
