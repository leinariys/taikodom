package all;

public class VZ extends aoW {
    public Vector3dOperations l(float var1, float var2, float var3) {
        Vector3dOperations var4 = (Vector3dOperations) this.get();
        var4.set((double) var1, (double) var2, (double) var3);
        return var4;
    }

    public Vector3dOperations N(Vector3dOperations var1) {
        Vector3dOperations var2 = (Vector3dOperations) this.get();
        var2.aA(var1);
        return var2;
    }

    protected Vector3dOperations bDn() {
        return new Vector3dOperations();
    }

    protected void d(Vector3dOperations var1, Vector3dOperations var2) {
        var1.aA(var2);
    }

    @Override
    protected Object create() {
        return null;
    }

    @Override
    protected void copy(Object var1, Object var2) {

    }
}
