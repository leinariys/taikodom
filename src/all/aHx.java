package all;

import org.w3c.dom.css.CSSRule;
import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.css.CSSValue;

import java.io.Serializable;
import java.io.StringReader;
import java.util.ArrayList;

public class aHx implements Serializable, CSSStyleDeclaration {
    //Тело селектора
    private CSSRule qr;//
    //Список правил селектора
    private ArrayList listPropertyCss = new ArrayList();

    public aHx(CSSRule var1) {
        this.qr = var1;
    }

    public String getCssText() {
        StringBuffer var1 = new StringBuffer();
        var1.append("{");

        for (int var2 = 0; var2 < this.listPropertyCss.size(); ++var2) {
            PropertyCss var3 = (PropertyCss) this.listPropertyCss.get(var2);
            if (var3 != null) {
                var1.append(var3.toString());
            }

            if (var2 < this.listPropertyCss.size() - 1) {
                var1.append("; ");
            }
        }

        var1.append("}");
        return var1.toString();
    }

    public void setCssText(String var1) {
        try {
            InputSource var2 = new InputSource(new StringReader(var1));
            ParserCss var3 = ParserCss.getParserCss();
            this.listPropertyCss.clear();
            var3.a((CSSStyleDeclaration) this, (InputSource) var2);
        } catch (Exception var4) {
            throw new qN((short) 12, 0, var4.getMessage());
        }
    }

    public String getPropertyValue(String var1) {
        PropertyCss var2 = this.mK(var1);
        return var2 != null ? var2.getCssValue().toString() : "";
    }

    public CSSValue getPropertyCSSValue(String var1) {
        PropertyCss var2 = this.mK(var1);
        return var2 != null ? var2.getCssValue() : null;
    }

    public String removeProperty(String var1) {
        for (int var2 = 0; var2 < this.listPropertyCss.size(); ++var2) {
            PropertyCss var3 = (PropertyCss) this.listPropertyCss.get(var2);
            if (var3.getName().equalsIgnoreCase(var1)) {
                this.listPropertyCss.remove(var2);
                return var3.getCssValue().toString();
            }
        }

        return "";
    }

    public String getPropertyPriority(String var1) {
        PropertyCss var2 = this.mK(var1);
        if (var2 != null) {
            return var2.getImportant() ? "important" : "";
        } else {
            return "";
        }
    }

    public void setProperty(String var1, String var2, String var3) {
        try {
            InputSource var4 = new InputSource(new StringReader(var2));
            ParserCss var5 = ParserCss.getParserCss();
            CSSValue var6 = var5.e(var4);
            PropertyCss var7 = this.mK(var1);
            boolean var8 = var3 != null ? var3.equalsIgnoreCase("important") : false;
            if (var7 == null) {
                var7 = new PropertyCss(var1, var6, var8);
                this.add(var7);
            } else {
                var7.setCssValue(var6);
                var7.setImportant(var8);
            }

        } catch (Exception var9) {
            throw new qN((short) 12, 0, var9.getMessage());
        }
    }

    //давбавление название правила и значения
    public void setPropertyCSS(String var1, Bj var2) {
        PropertyCss var3 = this.mK(var1);
        if (var3 == null) {
            var3 = new PropertyCss(var1, var2, false);
            this.add(var3);
        } else {
            var3.setCssValue(var2);
            var3.setImportant(false);
        }

    }

    public int getLength() {
        return this.listPropertyCss.size();
    }

    public String item(int var1) {
        PropertyCss var2 = (PropertyCss) this.listPropertyCss.get(var1);
        return var2 != null ? var2.getName() : "";
    }

    public PropertyCss yh(int var1) {
        PropertyCss var2 = (PropertyCss) this.listPropertyCss.get(var1);
        return var2;
    }

    public CSSRule getParentRule() {
        return this.qr;
    }

    public void add(PropertyCss var1) {
        this.listPropertyCss.add(var1);
    }

    private PropertyCss mK(String var1) {
        for (int var2 = 0; var2 < this.listPropertyCss.size(); ++var2) {
            PropertyCss var3 = (PropertyCss) this.listPropertyCss.get(var2);
            if (var3.getName().equalsIgnoreCase(var1)) {
                return var3;
            }
        }

        return null;
    }

    public String toString() {
        return this.getCssText();
    }
}
