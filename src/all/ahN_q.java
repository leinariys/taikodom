package all;

public class ahN_q {
    private long fLE;
    private long fLF;
    private long last;

    public ahN_q(float var1) {
        this.setFps(var1);
    }

    void setFps(float var1) {
        this.fLF = (long) (1000.0F / var1);
    }

    long caT() {
        this.fLE = System.currentTimeMillis();
        long var1 = this.fLE - this.last - this.fLF;
        this.last = this.fLE;
        long var3 = this.fLF - var1;
        if (var3 > 0L) {
            PoolThread.sleep(var3);
        }

        long var5 = var1 > -this.fLF ? var1 + this.fLF : 0L;
        return var5;
    }
}
