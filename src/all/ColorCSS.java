package all;

import org.w3c.dom.css.CSSPrimitiveValue;
import org.w3c.dom.css.RGBColor;

import java.io.Serializable;

public class ColorCSS implements Serializable, RGBColor {
    private CSSPrimitiveValue red = null;
    private CSSPrimitiveValue green = null;
    private CSSPrimitiveValue blue = null;
    private CSSPrimitiveValue alpha = null;

    protected ColorCSS(LexicalUnit var1) {
        this.red = new Bj(var1, true);
        LexicalUnit var2 = var1.getNextLexicalUnit();
        var2 = var2.getNextLexicalUnit();
        this.green = new Bj(var2, true);
        var2 = var2.getNextLexicalUnit();
        var2 = var2.getNextLexicalUnit();
        this.blue = new Bj(var2, true);
        var2 = var2.getNextLexicalUnit();
        if (var2 != null) {
            var2 = var2.getNextLexicalUnit();
            if (var2 != null) {
                this.alpha = new Bj(var2, true);
            } else {
                this.alpha = new Bj(LexicalUnit.perseValueColor((LexicalUnit) null, 255.0F), true);
            }
        } else {
            this.alpha = new Bj(LexicalUnit.perseValueColor((LexicalUnit) null, 255.0F), true);
        }

    }

    public ColorCSS() {
    }

    public ColorCSS(int red, int green, int blue) {
        this.red = new Bj(LexicalUnit.perseValueColor((LexicalUnit) null, (float) red));
        this.green = new Bj(LexicalUnit.perseValueColor((LexicalUnit) null, (float) green));
        this.blue = new Bj(LexicalUnit.perseValueColor((LexicalUnit) null, (float) blue));
        this.alpha = new Bj(LexicalUnit.perseValueColor((LexicalUnit) null, 255.0F));
    }

    public CSSPrimitiveValue getRed() {
        return this.red;
    }

    public void setRed(CSSPrimitiveValue var1) {
        this.red = var1;
    }

    public CSSPrimitiveValue getGreen() {
        return this.green;
    }

    public void setGreen(CSSPrimitiveValue var1) {
        this.green = var1;
    }

    public CSSPrimitiveValue getBlue() {
        return this.blue;
    }

    public void setBlue(CSSPrimitiveValue var1) {
        this.blue = var1;
    }

    public CSSPrimitiveValue getAlpha() {
        return this.alpha;
    }

    public void setAlpha(CSSPrimitiveValue var1) {
        this.alpha = var1;
    }


    public String toString() {
        return "rgb(" + this.red.toString() + ", " + this.green.toString() + ", " + this.blue.toString() + "," + this.alpha.toString() + ")";
    }
}
