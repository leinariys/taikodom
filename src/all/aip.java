package all;

public class aip {
    public static float fNb = 0.017453292F;

    public static float clamp(float var0, float var1, float var2) {
        if (var2 < var0) {
            return var0;
        } else {
            return var2 > var1 ? var1 : var2;
        }
    }

    public static long c(long var0, long var2, long var4) {
        if (var4 < var0) {
            return var0;
        } else {
            return var4 > var2 ? var2 : var4;
        }
    }

    public static int clamp(int var0, int var1, int var2) {
        if (var2 < var0) {
            return var0;
        } else {
            return var2 > var1 ? var1 : var2;
        }
    }

    public static float cbI() {
        return (float) (Math.random() * 2.0D - 1.0D);
    }

    public static float cbJ() {
        return (float) Math.random();
    }

    public static float L(double var0) {
        if (var0 >= 0.0D) {
            return (float) ((long) var0);
        } else {
            double var2 = (double) ((long) var0);
            return var2 != var0 ? (float) (var2 - 1.0D) : (float) var2;
        }
    }

    public static float ju(float var0) {
        if (var0 >= 0.0F) {
            return (float) ((long) var0);
        } else {
            float var1 = (float) ((long) var0);
            return var1 != var0 ? var1 - 1.0F : var1;
        }
    }

    public static double floor(double var0) {
        if (var0 >= 0.0D) {
            return (double) ((long) var0);
        } else {
            double var2 = (double) ((long) var0);
            return var2 != var0 ? var2 - 1.0D : var2;
        }
    }

    public static int jv(float var0) {
        if (var0 >= 0.0F) {
            return (int) var0;
        } else {
            float var1 = (float) ((long) var0);
            return var1 != var0 ? (int) (var1 - 1.0F) : (int) var1;
        }
    }

    public static float s(float var0, float var1) {
        return (float) Math.sqrt((double) (var0 * var0 + var1 * var1));
    }

    public static double ceil(double var0) {
        if (var0 >= 0.0D) {
            return (double) ((long) var0);
        } else {
            double var2 = (double) ((long) var0);
            return var2 != var0 ? var2 - 1.0D : var2;
        }
    }

    public static double jw(float var0) {
        if (var0 <= 0.0F) {
            return (double) ((long) var0);
        } else {
            float var1 = (float) ((long) var0);
            return var1 != var0 ? (double) (var1 + 1.0F) : (double) var1;
        }
    }

    public static float jx(float var0) {
        if (var0 <= 0.0F) {
            return (float) ((long) var0);
        } else {
            float var1 = (float) ((long) var0);
            return var1 != var0 ? var1 + 1.0F : var1;
        }
    }

    public static double M(double var0) {
        return Math.cos(var0 * 0.01745329238474369D);
    }

    public static double N(double var0) {
        return Math.sin(var0 * 0.01745329238474369D);
    }

    public static double O(double var0) {
        return Math.tan(var0 * 0.01745329238474369D);
    }

    public static int ra(int var0) {
        int var1;
        for (var1 = 1; var1 < var0; var1 *= 2) {
            ;
        }

        return var1;
    }

    public static int rb(int var0) {
        int var1 = 0;

        for (int var2 = 1; var0 > var2; var2 *= 2) {
            ++var1;
        }

        return var1;
    }
}
