package all;

import java.io.*;
import java.nio.ByteBuffer;

/**
 * Работа с файлами File
 * class FileControl avU GB iF sU
 */
public interface ain {
    /**
     * Создает новый экземпляр файла из родительского абстрактного пути и строки дочернего пути.
     *
     * @param var1 Строка имени дочернего пути (имя файла)
     */
    ain concat(String var1);

    String getName();

    boolean isDir();

    /**
     * метод проверяет существование файла или каталога, определенного этим абстрактным именем пути.
     *
     * @return
     */
    boolean exists();

    ain[] getChildren();

    ain[] a(FY var1);

    ain getParentFile();

    void mkdirs();

    InputStream getInputStream() throws FileNotFoundException;

    PrintWriter getPrintWriter() throws FileNotFoundException;

    OutputStream getOutputStream() throws FileNotFoundException;

    boolean delete();

    String getPath();

    boolean renameTo(ain var1);

    ain getAbsoluteFile();

    long lastModified();

    File getFile();

    /**
     * Получить массив byte
     *
     * @return
     */
    byte[] getByte();

    /**
     * Записать массив byte
     *
     * @param var1 массив byte
     */
    void setByte(byte[] var1);

    ByteBuffer BI();

    long length();
}
