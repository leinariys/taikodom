package all;

import org.w3c.dom.stylesheets.MediaList;

import java.io.Serializable;
import java.util.Vector;

public class zw_q implements Serializable, MediaList {
    private Vector bZx = new Vector();

    public zw_q(SACMediaList var1) {
        for (int var2 = 0; var2 < var1.getLength(); ++var2) {
            this.bZx.addElement(var1.item(var2));
        }

    }

    public String getMediaText() {
        StringBuffer var1 = new StringBuffer("");

        for (int var2 = 0; var2 < this.bZx.size(); ++var2) {
            var1.append(this.bZx.elementAt(var2).toString());
            if (var2 < this.bZx.size() - 1) {
                var1.append(", ");
            }
        }

        return var1.toString();
    }

    public void setMediaText(String var1) {
    }

    public int getLength() {
        return this.bZx.size();
    }

    public String item(int var1) {
        return var1 < this.bZx.size() ? (String) this.bZx.elementAt(var1) : null;
    }

    public void deleteMedium(String var1) {
        for (int var2 = 0; var2 < this.bZx.size(); ++var2) {
            String var3 = (String) this.bZx.elementAt(var2);
            if (var3.equalsIgnoreCase(var1)) {
                this.bZx.removeElementAt(var2);
                return;
            }
        }

        throw new qN((short) 8, 18);
    }

    public void appendMedium(String var1) {
        this.bZx.addElement(var1);
    }

    public String toString() {
        return this.getMediaText();
    }
}
