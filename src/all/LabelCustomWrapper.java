package all;

import taikodom.render.graphics2d.RGraphics2;

import javax.swing.*;
import java.awt.*;

public class LabelCustomWrapper extends JLabel {
    private static final long serialVersionUID = 1L;
    private boolean izI;

    public LabelCustomWrapper() {
        this.setFocusable(false);
    }

    public void jV(boolean var1) {
        this.izI = var1;
    }

    public boolean dnK() {
        return this.izI;
    }

    protected void firePropertyChange(String var1, Object var2, Object var3) {
        if (this.dnK()) {
            if (var1 == "text" || (var1 == "font" || var1 == "foreground") && var2 != var3 && this.getClientProperty("html") != null) {
                super.firePropertyChange(var1, var2, var3);
            }
        } else {
            super.firePropertyChange(var1, var2, var3);
        }

    }

    public void paint(Graphics var1) {
        if (var1 instanceof RGraphics2) {
            PropertiesUiFromCss var2 = PropertiesUiFromCss.g(this);
            if (var2 != null) {
                Color var3 = var2.getColorMultiplier();
                if (var3 != null) {
                    ((RGraphics2) var1).setColorMultiplier(var3);
                }
            }
        }

        super.paint(var1);
    }
}
