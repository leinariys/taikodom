package all;

import javax.swing.*;
import java.awt.*;

public final class ComboBoxCustom extends BaseItem {
    /**
     * Создание базового Component интерфейса
     * Вызывается из class BaseItem
     *
     * @param var1
     * @param var2
     * @param var3
     * @return
     */
    @Override
    protected JComponent CreatComponentCustom(MapKeyValue var1, Container var2, XmlNode var3) {
        return new JComboBox();
    }

    public JComboBox W(MapKeyValue var1, Container var2, XmlNode var3) {
        JComboBox var4 = (JComboBox) super.CreateUI(var1, var2, var3);
        this.CreatChildren(var1, var4, var3);
        return var4;
    }

}
