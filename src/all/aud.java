package all;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.nio.channels.Channel;
import java.util.LinkedList;

public class aud extends TT_q {
    private final int pa;
    private ByteBuffer buffer = ByteBuffer.allocate(1024);
    private int oZ = 48151623;
    private int ckD = -1;
    private int ckE = -1;
    private LinkedList ckF = new LinkedList();
    private byte[] ckH = new byte[1024];
    private boolean special;
    private int ckI;
    private int pd;
    private int ckJ;

    public aud(int var1, int var2) {
        this.pa = var2;
        if (var2 >= 536870911) {
            throw new IllegalArgumentException("Max block sise too big " + var2);
        }
    }

    public aud() {
        this.pa = 8388608;
    }

    public JB ayg() {
        return (JB) this.ckF.removeFirst();
    }

    public boolean ayh() {
        return this.ckF.size() > 0;
    }

    public int b(Channel var1) throws IOException {
        int var2 = ((ByteChannel) var1).read(this.buffer);
        if (var2 == -1) {
            return -1;
        } else {
            this.ckI += var2;
            if (this.buffer.position() >= this.buffer.capacity() - 1) {
                ByteBuffer var3 = ByteBuffer.allocate(this.buffer.capacity() << 1);
                this.buffer.flip();
                var3.put(this.buffer);
                this.buffer = var3;
                var2 += ((ByteChannel) var1).read(this.buffer);
            }

            if (var2 > 0) {
                this.gx(var2);

                do {
                    if (this.ckE == -1) {
                        this.readHeader();
                    }

                    if (this.ckE == -1) {
                        break;
                    }

                    if (this.ckE > this.pa) {
                        throw new IllegalArgumentException("Message too big: " + this.ckE);
                    }

                    if (this.buffer.position() < this.ckE + this.ckD) {
                        break;
                    }

                    this.ayi();
                } while (this.buffer.position() > 0);
            }

            return var2;
        }
    }

    private void ayi() {
        byte[] var1 = this.buffer.array();
        byte[] var2 = new byte[this.ckE];
        System.arraycopy(var1, this.ckD, var2, 0, this.ckE);
        JB var3 = new JB(var2, 0, var2.length);
        var3.setSpecial(this.special);
        this.ckF.add(var3);
        if (this.buffer.position() > this.ckE + this.ckD) {
            System.arraycopy(var1, this.ckE + this.ckD, var1, 0, this.buffer.position() - (this.ckE + this.ckD));
            this.buffer.position(this.buffer.position() - (this.ckE + this.ckD));
        } else {
            this.buffer.clear();
        }

        ++this.ckJ;
        this.pd += var2.length;
        this.ckE = -1;
        this.ckD = -1;
    }

    private void readHeader() {
        byte var1 = this.buffer.get(0);
        byte var2 = 0;
        if (var1 == 0) {
            if (this.buffer.position() < 2) {
                return;
            }

            var1 = this.buffer.get(1);
            var2 = 1;
            this.special = true;
        } else {
            this.special = false;
        }

        int var3;
        int var4;
        switch ((var1 & 96) >> 5) {
            case 0:
                this.ckD = var2 + 1;
                this.ckE = var1 & 31;
                return;
            case 1:
                this.ckD = var2 + 2;
                if (this.buffer.position() > 1) {
                    var3 = this.buffer.get(var2 + 1) & 255;
                    this.ckE = (var1 & 31) << 8 | var3;
                }

                return;
            case 2:
                this.ckD = var2 + 3;
                if (this.buffer.position() > 2) {
                    var3 = this.buffer.get(var2 + 1) & 255;
                    var4 = this.buffer.get(var2 + 2) & 255;
                    this.ckE = (var1 & 31) << 16 | var3 << 8 | var4;
                }

                return;
            case 3:
                this.ckD = var2 + 4;
                if (this.buffer.position() > 4) {
                    var3 = this.buffer.get(var2 + 1) & 255;
                    var4 = this.buffer.get(var2 + 2) & 255;
                    int var5 = this.buffer.get(var2 + 3) & 255;
                    this.ckE = (var1 & 31) << 24 | var3 << 16 | var4 << 8 | var5;
                }

                return;
            default:
        }
    }

    private void gx(int var1) {
        byte[] var2 = this.buffer.array();
        int var3 = this.buffer.position();

        for (int var4 = var3 - var1; var4 < var3; ++var4) {
            this.oZ = aFj.aC(this.oZ, var2[var4] = (byte) ((var2[var4] & 255 ^ this.oZ & 255) & 255));
        }

    }

    public int ayj() {
        return this.ckI;
    }

    public int ayk() {
        return this.ckJ;
    }

    public int ayl() {
        return this.pd;
    }
}
