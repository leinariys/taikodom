package all;

import javax.swing.text.Element;
import javax.swing.text.StyleConstants;
import javax.swing.text.View;
import javax.swing.text.ViewFactory;
import javax.swing.text.html.HTML.Tag;
import javax.swing.text.html.HTMLEditorKit;

public class av extends HTMLEditorKit {
    private static final long serialVersionUID = 1L;

    public ViewFactory getViewFactory() {
        return new a();
    }

    public static class a extends HTMLFactory implements ViewFactory {
        public View create(Element var1) {
            Object var2 = var1.getAttributes().getAttribute(StyleConstants.NameAttribute);
            if (var2 instanceof Tag) {
                Tag var3 = (Tag) var2;
                if (var3 == Tag.IMG) {
                    return new Ve(var1);
                }
            }

            return super.create(var1);
        }
    }
}
