package all;

import java.io.IOException;
import java.nio.channels.SelectableChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

public abstract class XF_w {
    protected agX yQ;
    private SelectionKey eJw;

    protected final void bGd() {
        this.yQ.d(this.eJw);
    }

    protected final void a(SelectionKey var1, agX var2) {
        this.eJw = var1;
        this.yQ = var2;
    }

    int bgp() {
        if (this.getChannel() instanceof SocketChannel) {
            return 5;
        } else if (this.getChannel() instanceof ServerSocketChannel) {
            return 16;
        } else {
            throw new IllegalStateException();
        }
    }

    protected abstract void b(SelectionKey var1) throws IOException, InterruptedException;

    abstract SelectableChannel getChannel();

    public void bgu() throws IOException {
    }

    public void bgr() {
    }

    public void c(SelectionKey var1) {
        this.eJw = var1;
    }

    public SelectionKey bGe() {
        return this.eJw;
    }
}
