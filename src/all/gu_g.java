package all;

import com.hoplon.geometry.Vec3f;

public class gu_g {
    private final Vec3f Qn;
    private final Vec3f Qo;
    private final Vec3f Qp;
    private boolean intersected = false;
    private float Qq = Float.POSITIVE_INFINITY;
    private aTw Qr = new aTw();

    public gu_g(Vec3f var1, Vec3f var2) {
        this.Qn = new Vec3f(var1);
        this.Qo = new Vec3f(var2);
        this.Qp = var2.j(var1);
    }

    public void set(gu_g var1) {
        this.setIntersected(var1.intersected);
        this.ac(var1.Qq);
        this.a(var1.Qr);
    }

    public void set(aDM var1) {
        this.setIntersected(var1.isIntersected());
        this.ac((float) var1.getParamIntersection());
        this.Qr.a(var1.getPlane());
    }

    public final boolean isIntersected() {
        return this.intersected;
    }

    public final void setIntersected(boolean var1) {
        this.intersected = var1;
    }

    public final float vM() {
        return this.Qq;
    }

    public final void ac(float var1) {
        this.Qq = var1;
    }

    public final aTw vN() {
        return this.Qr;
    }

    public final void a(aTw var1) {
        this.Qr.b(var1);
    }

    public void b(float var1, float var2, float var3) {
        this.Qr.b(var1, var2, var3);
    }

    public void l(Vec3f var1) {
        this.Qr.l(var1);
    }

    public Vec3f vO() {
        return this.Qr.vO();
    }

    public final Vec3f vP() {
        return this.Qn;
    }

    public final Vec3f vQ() {
        return this.Qo;
    }

    public final Vec3f vR() {
        return this.Qp;
    }

    public void c(Vec3f var1, Vec3f var2) {
        this.Qn.set(var1);
        this.Qo.set(var2);
        this.Qp.set(var2.j(var1));
    }
}
