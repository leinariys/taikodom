package all;

import java.io.Serializable;

public final class Md implements Serializable, Comparable {
    private String dyV;
    private String methodName;
    private String fileName;

    public Md(StackTraceElement var1) {
        this.a(var1);
    }

    public Md() {
    }

    private static boolean eq(Object var0, Object var1) {
        return var0 == var1 || var0 != null && var0.equals(var1);
    }

    public void a(StackTraceElement var1) {
        this.dyV = var1.getClassName();
        this.methodName = var1.getMethodName();
        this.fileName = var1.getFileName();
    }

    public String toString() {
        String var1 = this.dyV + "." + this.methodName;
        return var1 + (this.fileName != null ? "(" + this.fileName + ")" : "(Unknown Source)");
    }

    public boolean equals(Object var1) {
        if (var1 == this) {
            return true;
        } else if (!(var1 instanceof Md)) {
            return false;
        } else {
            Md var2 = (Md) var1;
            return var2.dyV.equals(this.dyV) && eq(this.methodName, var2.methodName) && eq(this.fileName, var2.fileName);
        }
    }

    public int hashCode() {
        int var1 = 31 * this.dyV.hashCode() + this.methodName.hashCode();
        var1 = 31 * var1 + (this.fileName == null ? 0 : this.fileName.hashCode());
        return var1;
    }

    public String getDeclaringClass() {
        return this.dyV;
    }

    public String getMethodName() {
        return this.methodName;
    }

    public String getFileName() {
        return this.fileName;
    }

    public String getFullName() {
        return this.dyV + "." + this.methodName;
    }


    public int b(Md var1) {
        int var2 = this.getDeclaringClass().compareTo(var1.getDeclaringClass());
        if (var2 == 0) {
            var2 = this.methodName.compareTo(var1.methodName);
        }

        return var2;
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }
}
