package all;

import java.io.Serializable;

public class ElementSelectorUri implements IElementSelector, Serializable {
    private String localName;

    public ElementSelectorUri(String var1) {
        this.localName = var1;
    }

    public short getSelectorType() {
        return 9;
    }

    public String getNamespaceURI() {
        return null;
    }

    public String getLocalName() {
        return this.localName;
    }
}
