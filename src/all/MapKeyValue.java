package all;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Создание списка ключ - значение для Базовые элементы интерфейса class BaseUItegXML
 */
public class MapKeyValue // implements alC
{
    /**
     *
     */
    protected MapKeyValue listTagAsClass;
    /**
     * Пример file:/C:/.../taikodom/addon/debugtools/taikodomversion/taikodomversion.xml
     */
    private URL pathFile;
    /**
     * Список Базовые элементы интерфейса
     * Класс BaseUItegXML
     */
    private Map listBaseUiXML;
    /**
     *
     */
    private BaseUItegXML baseUiTegXml;
    /**
     * Список Базовые элементы интерфейса Layout
     * Класс BaseUItegXML
     */
    private Map listBaseLayoutUiXML;

    public MapKeyValue(BaseUItegXML var1) {
        this.baseUiTegXml = var1;
    }

    public MapKeyValue(BaseUItegXML var1, URL pathFile) {
        this(var1);
        this.pathFile = pathFile;
    }

    /**
     * @param listTagAsClass
     * @param pathFile       Пример file:/C:/,,,/taikodom/addon/login/login.xml
     */
    public MapKeyValue(MapKeyValue listTagAsClass, URL pathFile) {
        this.listTagAsClass = listTagAsClass;
        this.pathFile = pathFile;
    }

    /**
     * Добавление ключ значения Базовые элементы интерфейса
     *
     * @param key   TegName
     * @param value class
     */
    public void add(String key, BaseItem value) {
        if (this.listBaseUiXML == null) {
            this.listBaseUiXML = new HashMap();
        }

        this.listBaseUiXML.put(key, value);
    }

    /**
     * Добавление ключ значения Layout Базовые элементы интерфейса
     *
     * @param key   TegName
     * @param value class
     */
    public void add(String key, IBaseLayout value) {
        if (this.listBaseLayoutUiXML == null) {
            this.listBaseLayoutUiXML = new HashMap();
        }
        this.listBaseLayoutUiXML.put(key, value);
    }

    public URL getPathFile() {
        if (this.pathFile != null) {
            return this.pathFile;
        } else {
            return this.listTagAsClass != null ? this.listTagAsClass.getPathFile() : null;
        }
    }

    /**
     * Получить класс по имени тега в xml (Базовые элементы интерфейса)
     *
     * @param tegName Пример window
     * @return Пример all.WindowCustom
     */
    public BaseItem getClassBaseUi(String tegName) {//2
        if (this.listBaseUiXML != null) {
            BaseItem var2 = (BaseItem) this.listBaseUiXML.get(tegName);//класс  соответствующий имени тега в xml
            if (var2 != null) {
                return var2;
            }
        }

        return this.listTagAsClass != null ? this.listTagAsClass.getClassBaseUi(tegName) : null;
    }

    public IBaseLayout getClassLayoutUi(String var1) {
        if (this.listBaseLayoutUiXML != null) {
            IBaseLayout var2 = (IBaseLayout) this.listBaseLayoutUiXML.get(var1);
            if (var2 != null) {
                return var2;
            }
        }
        return this.listTagAsClass != null ? this.listTagAsClass.getClassLayoutUi(var1) : null;
    }

    public BaseUItegXML getBaseUiTegXml() {
        if (this.baseUiTegXml != null) {
            return this.baseUiTegXml;
        } else {
            return this.listTagAsClass != null ? this.listTagAsClass.getBaseUiTegXml() : null;
        }
    }
}
