package all;

import java.net.InetAddress;

public class ha implements b {
    private static final int Ti = 28;
    private static final long serialVersionUID = 1L;
    private final int id;
    private final InetAddress Tn;
    private transient String str;
    private transient a Tj;
    private transient Object Tk;
    private transient long Tl;
    private String info;
    private transient long Tm;

    public ha(a var1, int var2, InetAddress var3) {
        this.Tn = var3;
        this.id = var2 & 268435455 | var1.ordinal() << 28;
        this.Tj = var1;
    }

    public int hashCode() {
        return this.id;
    }

    public boolean equals(Object var1) {
        if (this == var1) {
            return true;
        } else if (var1 == null) {
            return false;
        } else if (this.getClass() != var1.getClass()) {
            return false;
        } else {
            ha var2 = (ha) var1;
            return this.id == var2.id;
        }
    }

    public String toString() {
        return this.str != null ? this.str : (this.str = Integer.toString(this.id) + (this.info != null ? " " + this.info : ""));
    }

    public a xQ() {
        if (this.Tj != null) {
            return this.Tj;
        } else {
            this.Tj = a.values()[this.id >> 28];
            return null;
        }
    }

    public Object xR() {
        return this.Tk;
    }

    public void x(Object var1) {
        this.Tk = var1;
    }

    public void xS() {
        this.Tl = System.currentTimeMillis();
    }

    public long xT() {
        return this.Tl;
    }

    public void setInfo(String var1) {
        this.info = var1;
    }

    public long xU() {
        return this.Tm;
    }

    public void bj(long var1) {
        this.Tm = var1;
    }

    public long xV() {
        return this.Tm;
    }

    public InetAddress getInetAddress() {
        return this.Tn;
    }

    public int getId() {
        return this.id;
    }
}
