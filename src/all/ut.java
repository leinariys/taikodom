package all;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class ut extends kU implements Externalizable {
    private static final long serialVersionUID = 1L;
    private long Tm = 0L;
    private long PZ = 0L;
    private long Qa = 0L;

    public ut() {
    }

    public ut(long var1, long var3, long var5) {
        this.Tm = var1;
        this.PZ = var3;
        this.Qa = var5;
    }

    public long xU() {
        return this.Tm;
    }

    public void readExternal(ObjectInput var1) throws IOException, ClassNotFoundException {
        super.readExternal(var1);
        this.Tm = var1.readLong();
        this.PZ = var1.readLong();
        this.Qa = var1.readLong();
    }

    public void writeExternal(ObjectOutput var1) throws IOException {
        super.writeExternal(var1);
        var1.writeLong(this.Tm);
        var1.writeLong(this.PZ);
        var1.writeLong(this.Qa);
    }

    public long vK() {
        return this.PZ;
    }

    public long vL() {
        return this.Qa;
    }
}
