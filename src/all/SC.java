package all;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;

import javax.vecmath.Tuple3f;

public class SC extends ET {

    protected Vec3f[] ebY;
    private ye_q eeH;
    private a eeI = new a((a) null);

    public SC() {
    }

    public SC(Vec3f[] var1) {
        this.ebY = var1;
        this.aPc();
        this.setVolume(this.getBoundingBox().dio() * this.getBoundingBox().dip() * this.getBoundingBox().diq());
        this.eeH = new ye_q(this.getBoundingBox());
    }

    public byte getShapeType() {
        return 8;
    }

    protected void growBoundingBox(ajK var1, BoundingBox var2) {
        Vec3f var3 = new Vec3f();
        Vec3f[] var7 = this.ebY;
        int var6 = this.ebY.length;

        for (int var5 = 0; var5 < var6; ++var5) {
            Vec3f var4 = var7[var5];
            var1.b((Tuple3f) var4, (Tuple3f) var3);
            var2.addPoint(var3);
        }

    }

    public void getFarthest(Vec3f var1, Vec3f var2) {
        Vec3f var3 = null;
        float var4 = Float.NEGATIVE_INFINITY;
        Vec3f[] var8 = this.ebY;
        int var7 = this.ebY.length;

        for (int var6 = 0; var6 < var7; ++var6) {
            Vec3f var5 = var8[var6];
            float var9 = var1.dot(var5);
            if (var9 > var4) {
                var4 = var9;
                var3 = var5;
            }
        }

        if (var3 != null) {
            var2.set(var3);
        }

    }

    public Vec3f localGetSupportingVertexWithoutMargin(Vec3f var1) {
        this.stack = this.stack.bcD();
        this.stack.bcH().push();

        Vec3f var11;
        try {
            Vec3f var2 = this.stack.bcH().h(0.0F, 0.0F, 0.0F);
            float var4 = -1.0E30F;
            Vec3f var5 = this.stack.bcH().ac(var1);
            float var6 = var5.lengthSquared();
            if (var6 < 1.0E-4F) {
                var5.set(1.0F, 0.0F, 0.0F);
            } else {
                float var7 = 1.0F / (float) Math.sqrt((double) var6);
                var5.scale(var7);
            }

            Vec3f var14 = (Vec3f) this.stack.bcH().get();
            boolean var8 = false;
            if (this.localScaling.length() != 1.0F) {
                for (int var9 = 0; var9 < this.ebY.length; ++var9) {
                    if (var8) {
                        JL.g(var14, this.ebY[var9], this.localScaling);
                    } else {
                        var14 = this.ebY[var9];
                    }

                    float var3 = var5.dot(var14);
                    if (var3 > var4) {
                        var4 = var3;
                        var2.set(var14);
                    }
                }

                var11 = (Vec3f) this.stack.bcH().aq(var2);
                return var11;
            }

            this.eeI.v(var5);
            this.eeH.a(this.eeI);
            var11 = this.eeI.dtf();
        } finally {
            this.stack.bcH().pop();
        }

        return var11;
    }

    public void batchedUnitVectorGetSupportingVertexWithoutMargin(Vec3f[] var1, Vec3f[] var2, int var3) {
        this.stack.bcH().push();

        try {
            float[] var5 = new float[var3];

            for (int var6 = 0; var6 < var3; ++var6) {
                var5[var6] = -1.0E30F;
            }

            Vec3f var13 = (Vec3f) this.stack.bcH().get();

            for (int var7 = 0; var7 < this.ebY.length; ++var7) {
                JL.g(var13, this.ebY[var7], this.localScaling);

                for (int var8 = 0; var8 < var3; ++var8) {
                    Vec3f var9 = var1[var8];
                    float var4 = var9.dot(var13);
                    if (var4 > var5[var8]) {
                        var2[var8].set(var13);
                        var5[var8] = var4;
                    }
                }
            }
        } finally {
            this.stack.bcH().pop();
        }

    }

    public Vec3f localGetSupportingVertex(Vec3f var1) {
        this.stack.bcH().push();

        Vec3f var5;
        try {
            Vec3f var2 = this.stack.bcH().ac(this.localGetSupportingVertexWithoutMargin(var1));
            if (this.getMargin() != 0.0F) {
                Vec3f var3 = this.stack.bcH().ac(var1);
                if (var3.lengthSquared() < 1.4210855E-14F) {
                    var3.set(-1.0F, -1.0F, -1.0F);
                }

                var3.normalize();
                var2.scaleAdd(this.getMargin(), var3, var2);
            }

            var5 = (Vec3f) this.stack.bcH().aq(var2);
        } finally {
            this.stack.bcH().pop();
        }

        return var5;
    }

    public int getNumVertices() {
        return this.ebY.length;
    }

    public int abC() {
        return this.ebY.length;
    }

    public void a(int var1, Vec3f var2, Vec3f var3) {
        int var4 = var1 % this.ebY.length;
        int var5 = (var1 + 1) % this.ebY.length;
        JL.g(var2, this.ebY[var4], this.localScaling);
        JL.g(var3, this.ebY[var5], this.localScaling);
    }

    public void b(int var1, Vec3f var2) {
        JL.g(var2, this.ebY[var1], this.localScaling);
    }

    public int abB() {
        return 0;
    }

    public void a(Vec3f var1, Vec3f var2, int var3) {
        assert false;

    }

    public boolean b(Vec3f var1, float var2) {
        return false;
    }

    public Vec3f[] bsr() {
        return this.ebY;
    }

    public void setLocalScaling(Vec3f var1) {
        super.setLocalScaling(var1);
    }

    private class a implements nA {
        boolean iJd;
        boolean iJe;
        boolean iJf;
        Vec3f iJg;
        float iJh;
        private Vec3f dir;

        private a() {
            this.iJg = new Vec3f(0.0F, 0.0F, 0.0F);
        }

        // $FF: synthetic method
        a(a var2) {
            this();
        }

        public void v(Vec3f var1) {
            this.iJd = var1.x >= 0.0F;
            this.iJe = var1.y >= 0.0F;
            this.iJf = var1.z >= 0.0F;
            this.iJh = Float.NEGATIVE_INFINITY;
            this.dir = var1;
        }

        public boolean a(BoundingBox var1) {
            if (this.iJh == Float.NEGATIVE_INFINITY) {
                return true;
            } else {
                float var2 = this.iJd ? var1.dqQ().x : var1.dqP().x;
                float var3 = this.iJe ? var1.dqQ().y : var1.dqP().y;
                float var4 = this.iJf ? var1.dqQ().z : var1.dqP().z;
                float var5 = var2 * this.dir.x + var3 * this.dir.y + var4 * this.dir.z;
                return var5 > this.iJh;
            }
        }

        @Override
        public boolean a(BoundingBox var1, Object var2) {
            return false;
        }

        public boolean a(BoundingBox var1, Vec3f var2) {
            float var3 = var2.dot(this.dir);
            if (var3 > this.iJh) {
                this.iJg = var2;
                this.iJh = var3;
            }

            return true;
        }

        protected Vec3f dtf() {
            return this.iJg;
        }
    }
}
