package all;

public class aUM implements aWb {
    private final EngineGame engineGame;
    private AddonManager addonManager;

    public aUM(EngineGame var1) {
        this.engineGame = var1;
    }
/*
   public boolean cnn() {
      return this.engineGame.cnn();
   }
*/

    public Object getRoot() {
        return this.engineGame.getRoot();
    }

    /**
     * Высота окна
     *
     * @return
     */
    public int getScreenHeight() {
        return this.engineGame.getScreenHeight();
    }

    /**
     * Ширина окна
     *
     * @return
     */
    public int getScreenWidth() {
        return this.engineGame.getScreenWidth();
    }

    public ajJ_q getEventManager() {
        return this.engineGame.getEventManager();
    }

    public void setAddonManager(AddonManager var1) {
        this.addonManager = var1;
    }

    /**
     * Получить обёртку аддона с логирование и локализацией
     *
     * @param var1 ссылка на Addon manager
     * @param var2 ссылка на Загрузчик addons\taikodom.xml
     * @param var3 ссылка на Загруженный класс аддона
     * @return
     */
    public aVf a(AddonManager var1, aVW var2, aMS var3) {
        GK var4 = new GK(this.engineGame, var1, var2, (fo) var3);//логирование и локализация
        var3.InitAddon(var4);//Передаём в аддон логирование и локализацию
        return var4;
    }

    /**
     * Каталог с орсисовкой графики
     */
    public ain getRootPathRender() {
        return this.engineGame.getRootPathRender();
    }

    /**
     * Главный каталог
     */
    public ain getRootPath() {
        return this.engineGame.getRootPath();
    }
}
