package all;

import java.io.IOException;
import java.nio.channels.SelectableChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;

public abstract class yV extends XF_w {
    private final SocketChannel bMW;

    public yV(SocketChannel var1) {
        this.bMW = var1;
    }

    SelectableChannel getChannel() {
        return this.bMW;
    }

    public abstract void a(SelectionKey var1) throws IOException;
}
