package all;

import com.hoplon.geometry.Vec3f;

import javax.vecmath.Quat4f;

public class xf_g {
    public final ajD bFF = new ajD();
    public final Vec3f bFG = new Vec3f();
    protected Kt stack;

    public xf_g() {
    }

    public xf_g(ajD var1) {
        this.bFF.set(var1);
    }

    public xf_g(xf_g var1) {
        this.a(var1);
    }

    public void a(xf_g var1) {
        this.bFF.set(var1.bFF);
        this.bFG.set(var1.bFG);
    }

    public void G(Vec3f var1) {
        this.bFF.transform(var1);
        var1.add(this.bFG);
    }

    public void setIdentity() {
        this.bFF.setIdentity();
        this.bFG.set(0.0F, 0.0F, 0.0F);
    }

    public void inverse() {
        this.bFF.transpose();
        this.bFG.scale(-1.0F);
        this.bFF.transform(this.bFG);
    }

    public void b(xf_g var1) {
        this.a(var1);
        this.inverse();
    }

    public void c(xf_g var1) {
        if (this.stack == null) {
            this.stack = Kt.bcE();
        }

        this.stack.bcH().push();

        try {
            Vec3f var2 = this.stack.bcH().ac(var1.bFG);
            this.G(var2);
            this.bFF.mul(var1.bFF);
            this.bFG.set(var2);
        } finally {
            this.stack.bcH().pop();
        }

    }

    public void a(xf_g var1, xf_g var2) {
        this.a(var1);
        this.c(var2);
    }

    public void f(Vec3f var1, Vec3f var2) {
        if (this.stack == null) {
            this.stack = Kt.bcE();
        }

        this.stack.bcK().push();

        try {
            var2.sub(var1, this.bFG);
            ajD var3 = this.stack.bcK().g(this.bFF);
            var3.transpose();
            var3.transform(var2);
        } finally {
            this.stack.bcK().pop();
        }

    }

    public Quat4f anz() {
        if (this.stack == null) {
            this.stack = Kt.bcE();
        }

        this.stack.bcN().push();

        Quat4f var3;
        try {
            OrientationBase var1 = (OrientationBase) this.stack.bcN().get();
            rS.b(this.bFF, (Quat4f) var1);
            var3 = (Quat4f) this.stack.bcN().aq(var1);
        } finally {
            this.stack.bcN().pop();
        }

        return var3;
    }

    public void setRotation(Quat4f var1) {
        rS.a(this.bFF, var1);
    }

    public void a(float[] var1) {
        rS.a(this.bFF, var1);
        this.bFG.set(var1[12], var1[13], var1[14]);
    }

    public void b(float[] var1) {
        rS.b(this.bFF, var1);
        var1[12] = this.bFG.x;
        var1[13] = this.bFG.y;
        var1[14] = this.bFG.z;
        var1[15] = 1.0F;
    }

    public void set(ajK var1) {
        ajD var2 = this.bFF;
        var2.m00 = var1.m00;
        var2.m01 = var1.m01;
        var2.m02 = var1.m02;
        var2.m10 = var1.m10;
        var2.m11 = var1.m11;
        var2.m12 = var1.m12;
        var2.m20 = var1.m20;
        var2.m21 = var1.m21;
        var2.m22 = var1.m22;
        Vec3f var3 = this.bFG;
        var3.x = var1.m03;
        var3.y = var1.m13;
        var3.z = var1.m23;
    }

    public boolean anA() {
        return this.bFG.anA() && this.bFF.anA();
    }
}
