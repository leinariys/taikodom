package all;

import gnu.trove.THashMap;

import java.util.ArrayList;
import java.util.Map;

public class aMq //implements aPm
{
    protected Map ipr;
    protected int ips;

    public aMq() {
        this(128);
    }

    public aMq(int var1) {
        this.ipr = new THashMap();
        this.ips = var1;
    }

    public void put(Class var1, Object var2) {
        ArrayList var3 = (ArrayList) this.ipr.get(var1);
        if (var3 == null) {
            var3 = new ArrayList();
            this.ipr.put(var1, var3);
        }

        if (var3.size() < this.ips) {
            var3.add(var2);
        }

    }

    public int getMaximumCacheSize() {
        return this.ips;
    }

    public void setMaximumCacheSize(int var1) {
        this.ips = var1;
    }

    public Object aN(Class var1) {
        ArrayList var2 = (ArrayList) this.ipr.get(var1);
        if (var2 == null) {
            return null;
        } else {
            int var3 = var2.size();
            return var3 == 0 ? null : var2.remove(var3 - 1);
        }
    }

    public boolean isEmpty() {
        return this.ipr.isEmpty();
    }

    public int aO(Class var1) {
        ArrayList var2 = (ArrayList) this.ipr.get(var1);
        return var2 == null ? 0 : var2.size();
    }
}
