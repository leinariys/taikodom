package all;

import gnu.trove.THashSet;

import java.io.*;
import java.lang.ref.WeakReference;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Чтение настроек аддонов
 */
public class pp implements aVf {
    /**
     *
     */
    private static String pathConfigAddons = "./config/addons/";

    /**
     *
     */
    protected List eLV = new ArrayList();

    /**
     *
     */
    protected Properties eLW = new Properties();

    /**
     *
     */
    protected Map eLX = new ConcurrentHashMap();

    /**
     * Загрузчик ресурсов аддона
     */
    protected tj eLY;

    /**
     * ссылка на Addon manager
     */
    protected AddonManager addonManager;

    /**
     *
     */
    protected PrintStream out;

    /**
     *
     */
    protected List dkr;

    /**
     *
     */
    protected aBg dej;

    /**
     *
     */
    protected THashSet eLZ;

    /**
     * ссылка на Загруженный класс аддона
     */
    protected aMS bCI;

    /**
     *
     */
    protected Ih eMa;

    /**
     * ссылка на Загрузчик addons\taikodom.xml
     */
    protected aVW eMb;

    protected pp() {
        this.out = System.out;
        this.dkr = new ArrayList();
        this.eLZ = new THashSet();
    }

    public pp(AddonManager var1, aVW var2, aMS var3) {
        this.out = System.out;
        this.dkr = new ArrayList();
        this.eLZ = new THashSet();

        this.addonManager = var1;
        this.eMb = var2;
        this.bCI = var3;
        this.eLY = new tj(var1, this);
        this.dej = this.addonManager.alf();
        this.eMa = new Ih(var1.aVU());
    }

    public Object U(Class var1) {
        return this.addonManager.y(var1);
    }

    /**
     * Остановить работу аддона
     */
    public void dispose() {
        this.eLY.dispose();
        this.eMa.dispose();
        Iterator var2 = this.eLZ.iterator();

        while (var2.hasNext()) {
            WeakReference var1 = (WeakReference) var2.next();
            c var3 = (c) var1.get();
            if (var3 != null) {
                var3.setZl((ahM) null);
            }
        }

        this.eLZ.clear();
        var2 = this.dkr.iterator();

        while (var2.hasNext()) {
            b var4 = (b) var2.next();
            var4.canceled(true);
        }

        var2 = this.eLV.iterator();

        while (var2.hasNext()) {
            Runnable var5 = (Runnable) var2.next();
            var5.run();
        }
    }

    public InputStream hE(String var1) throws FileNotFoundException {
        return this.addonManager.aVX().concat(var1.replaceAll("^res://", "")).getInputStream();
    }

    public AddonManager getAddonManager() {
        return this.addonManager;
    }

    /**
     * Загрузчик ресурсов аддона
     *
     * @return
     */
    public tj bHv() {
        return this.eLY;
    }

    public void c(String var1, String... var2) {
        List var3 = (List) this.eLX.get(var1);
        if (var3 != null) {
            Iterator var5 = var3.iterator();

            while (var5.hasNext()) {
                akT var4 = (akT) var5.next();
                var4.j(var2);
            }
        }
    }

    public String getLineConfig(String var1, String var2) {
        String var3 = pathConfigAddons + var2 + ".ini";
        ain var4 = this.addonManager.aVY().concat(var3);
        if (!var4.exists()) {
            return null;
        } else {
            try {
                InputStream var5 = var4.getInputStream();
                this.eLW.load(var5);
                String var6 = (String) this.eLW.get(var1);
                var5.close();
                return var6;
            } catch (FileNotFoundException var7) {
                var7.printStackTrace();
            } catch (IOException var8) {
                var8.printStackTrace();
            }
            return null;
        }
    }

    public void a(String var1, akT var2) {
        Object var3 = (List) this.eLX.get(var1);
        if (var3 == null) {
            var3 = new CopyOnWriteArrayList();
            this.eLX.put(var1, var3);
        }
        ((List) var3).add(var2);
    }

    public PrintStream bHw() {
        return this.out;
    }

    public aPE a(long var1, aPE var3) {
        b var4 = new b(var3);
        this.dkr.add(var4);
        this.dej.b("Addon timer - " + var1, var4, var1);
        return var4.dsq;
    }

    public void P(Object var1) {
    }

    public void Q(Object var1) {
    }

    public Object bHx() {
        throw new jx();
    }

    public void e(String var1, String var2, String var3) {
        ain var4 = this.addonManager.aVY().concat(pathConfigAddons);
        if (!var4.exists()) {
            var4.mkdirs();
        }

        try {
            OutputStream var5 = var4.concat(var3 + ".ini").getOutputStream();
            this.eLW.put(var1, var2);
            this.eLW.store(var5, "");
            var5.close();
        } catch (FileNotFoundException var7) {
            var7.printStackTrace();
        } catch (IOException var8) {
            var8.printStackTrace();
        }
    }

    public aPE b(long var1, aPE var3) {
        b var4 = new b(var3);
        this.dej.a("Addon timer - " + var1, var4, var1);
        return var4.dsq;
    }

    public ahM b(ahM var1) {
        c var2 = new c();
        var2.setZl(var1);
        this.eLZ.add(new WeakReference(var2) {
            public boolean enqueue() {
                pp.this.eLZ.remove(this);
                return super.enqueue();
            }
        });
        return var2;
    }

    /**
     * ссылка на Загруженный класс аддона
     *
     * @return
     */
    public aMS bHy() {
        return this.bCI;
    }

    /**
     * Открытие файла на чтение, Пример nls.taikodom.addon.debugtools.taikodomversion-en.properties
     *
     * @param var1 Класс аддона
     * @param var2 имя файла Пример nls.taikodom.addon.debugtools.taikodomversion-en.properties
     * @return
     * @throws FileNotFoundException
     */
    public InputStream f(Object var1, String var2) throws FileNotFoundException {
        if (var2.startsWith("res://")) {
            return this.hE(var2);
        } else {
            Class var3;
            if (var1 == null) {
                var3 = this.bHy().getClass();
            } else if (var1 instanceof Class) {
                var3 = (Class) var1;
            } else {
                var3 = var1.getClass();
            }

            InputStream var4 = var3.getResourceAsStream(var2);
            return var4;
        }
    }

    public final String translate(String var1) {
        return this.a((Class) null, (String) null, var1, (Object[]) null);
    }

    public final String translate(String var1, Object... var2) {
        return this.a((Class) null, (String) null, var1, var2);
    }

    public final String V(String var1, String var2) {
        return this.a((Class) null, var1, var2, (Object[]) null);
    }

    public final String a(String var1, String var2, Object... var3) {
        return this.a((Class) null, var1, var2, var3);
    }

    public final String a(Class var1, String var2, Object... var3) {
        return this.a(var1, (String) null, var2, var3);
    }

    public String a(Class var1, String var2, String var3, Object... var4) {
        return String.format(var3, var4);
    }

    public ajJ_q aVU() {
        return this.eMa;
    }

    public void setAVU(Ih ggg) {
        this.eMa = ggg;
    }

    public aVW bHz() {
        return this.eMb;
    }

    public void restart() {
        this.eMb.a(this.bCI);
    }

    private final class c implements ahM {
        private ahM Zl;

        public void setZl(ahM var1) {
            this.Zl = var1;
        }

        public void n(Object var1) {
            if (this.Zl != null) {
                this.Zl.n(var1);
            }
        }

        public void a(Throwable var1) {
            if (this.Zl != null) {
                this.Zl.a(var1);
            }
        }
    }

    class b extends aen {
        private final aPE dsq;
        private long last;

        b(aPE var2) {
            this.dsq = var2;
            this.last = System.currentTimeMillis();
        }

        public void run() {
            long var1 = System.currentTimeMillis();
            if (!this.dsq.fg(var1 - this.last)) {
                this.canceled(true);
            }
            this.last = var1;
        }
    }
}
