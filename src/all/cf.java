package all;

import org.w3c.dom.css.CSSImportRule;
import org.w3c.dom.css.CSSRule;
import org.w3c.dom.css.CSSStyleSheet;
import org.w3c.dom.stylesheets.MediaList;

import java.io.Serializable;
import java.io.StringReader;

public class cf implements Serializable, CSSImportRule {
    SS qq = null;
    CSSRule qr = null;
    String qs = null;
    MediaList qt = null;

    public cf(SS var1, CSSRule var2, String var3, MediaList var4) {
        this.qq = var1;
        this.qr = var2;
        this.qs = var3;
        this.qt = var4;
    }

    public short getType() {
        return 3;
    }

    public String getCssText() {
        StringBuffer var1 = new StringBuffer();
        var1.append("@import url(").append(this.getHref()).append(")");
        if (this.getMedia().getLength() > 0) {
            var1.append(" ").append(this.getMedia().toString());
        }

        var1.append(";");
        return var1.toString();
    }

    public void setCssText(String var1) {
        if (this.qq != null && this.qq.isReadOnly()) {
            throw new qN((short) 7, 2);
        } else {
            try {
                InputSource var2 = new InputSource(new StringReader(var1));
                ParserCss var3 = ParserCss.getParserCss();
                CSSRule var4 = var3.f(var2);
                if (var4.getType() == 3) {
                    this.qs = ((cf) var4).qs;
                    this.qt = ((cf) var4).qt;
                } else {
                    throw new qN((short) 13, 6);
                }
            } catch (CSSException var5) {
                throw new qN((short) 12, 0, var5.getMessage());
            } /*catch (IOException var6) {
            throw new qN((short)12, 0, var6.getMessage());
         }*/
        }
    }

    public CSSStyleSheet getParentStyleSheet() {
        return this.qq;
    }

    public CSSRule getParentRule() {
        return this.qr;
    }

    public String getHref() {
        return this.qs;
    }

    public MediaList getMedia() {
        return this.qt;
    }

    public CSSStyleSheet getStyleSheet() {
        return null;
    }

    public String toString() {
        return this.getCssText();
    }
}
