package all;

/**
 * Список звуков интерфейса
 * class InterfaceSFXAddon
 */
public enum tV_q implements aeO {
    btR("sfx_level_up"),
    btS("sfx_xp_won"),
    btT("sfx_reward_won"),
    btU("sfx_mode_change"),
    btV("sfx_message_type"),
    btW("sfx_npc_chat_loop"),
    btX("sfx_error_message"),
    btY("sfx_mouse_click"),
    btZ("sfx_scroll_click"),
    bua("sfx_radio_over"),
    bub("sfx_window_open"),
    buc("sfx_equip_amplifier"),
    bud("sfx_equip_energy"),
    bue("sfx_equip_projectile"),
    bug("sfx_equip_launcher"),
    buh("sfx_equip_reammo"),
    bui("sfx_equip_change"),
    buj("sfx_equip_empty_a"),
    buk("sfx_equip_empty_b"),
    bul("sfx_chat_send"),
    bum("sfx_scroll_loop"),
    bun("sfx_mouse_click_login"),
    buo("sfx_welcome_aboard"),
    bup("sfx_gate_enter"),
    buq("sfx_gate_exit"),
    bur("sfx_shield_empty"),
    bus("sfx_shield_full"),
    but("sfx_missile_target_lock"),
    buu("sfx_missile_search_near"),
    buv("sfx_missile_pursuit_loop"),
    buw("sfx_special_hazard_loop"),
    bux("sfx_special_boost_full"),
    buy("sfx_special_boost_empty"),
    buz("sfx_fighter_click"),
    buA("sfx_bomber_click"),
    buB("sfx_explorer_click"),
    buC("sfx_freighter_click");

    public static String buD = "data/audio/interface/interface_sfx.pro";
    private String handle;
    private String file;

    private tV_q(String var3) {
        this.handle = var3;
        this.file = "data/audio/interface/interface_sfx.pro";
    }

    private tV_q(String var3, String var4) {
        this.handle = var3;
        this.file = var4;
    }

    public String getHandle() {
        return this.handle;
    }

    public String getFile() {
        return this.file;
    }
}
