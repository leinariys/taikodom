package all;

/**
 * Список возможных Параметров раздела настроек
 */
public enum setKeyValue {
    user_language("language", "user"), //Якык пользователя
    user_lastRunVersion("lastRunVersion", "user"),
    user_username("username", "user"), //Логин для автоматического заполнения
    user_lastPlayedCharacter("lastPlayedCharacter", "user"),
    render_xres("xres", "render"),
    render_yres("yres", "render"),
    render_refresh("refresh", "render"),
    render_bpp("bpp", "render"),//Глубина цвета 32-bit
    render_fullscreen("fullscreen", "render"),
    render_textureQuality("textureQuality", "render"),
    render_textureFilter("textureFilter", "render"),
    render_antiAliasing("antiAliasing", "render"),
    render_postProcessingFx("postProcessingFx", "render"),
    render_shaderQuality("shaderQuality", "render"),
    render_anisotropicFilter("anisotropicFilter", "render"),
    render_lodQuality("lodQuality", "render"),
    render_environmentFxQuality("environmentFxQuality", "render"),
    render_musicVolume("musicVolume", "render"),
    render_ambienceVolume("ambienceVolume", "render"),
    render_fxVolume("fxVolume", "render"),
    render_guiVolume("guiVolume", "render"),
    render_engineVolume("engineVolume", "render"),
    render_guiContrastBlocker("guiContrastBlocker", "render"),
    render_useVbo("useVbo", "render"),
    render_usePixelLight("usePixelLight", "render"),
    render_wireframeOnly("wireframeOnly", "render"),
    render_dynamicTextureRate("dynamicTextureRate", "render"),
    render_glowFeedback("glowFeedback", "render"),
    render_showSelectionOutline("showSelectionOutline", "render"),
    url_site("site", "url"),
    network_server("server", "network"),//Может быть перечислен через , без пробелов
    network_tcpPort("tcpPort", "network"),
    console_verbose("verbose", "console");

    private final String key;
    private final String value;

    private setKeyValue(String var3) {
        this.key = var3;
        this.value = "";
    }

    private setKeyValue(String var3, String var4) {
        this.key = var3;
        this.value = var4;
    }

    /**
     * Получить ключ
     *
     * @return
     */
    public String getKey() {
        return this.key;
    }

    /**
     * Получить ключ
     *
     * @return
     */
    public String toString() {
        return this.key;
    }

    /**
     * Получить значение
     *
     * @return
     */
    public String getValue() {
        return this.value;
    }
}
