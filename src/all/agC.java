package all;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Вид стрелки курсора
 */
public class agC implements HC {
    /**
     * GUIModule
     */
    private static HC fyE;
    private final GUIModule fyI;
    private Cursor fyF;
    private Cursor fyG;
    private Cursor fyH;

    public agC(GUIModule var1) {
        this.fyI = var1;
    }

    public static HC bWV() {
        return fyE;
    }

    public static void a(HC var0) {
        fyE = var0;
    }

    /**
     * SelectionCursor _enabled
     *
     * @return
     */
    public Cursor aRO() {
        if (this.fyF == null) {
            Image var1 = this.fyI.adz().getImage("res://data/gui/imageset/imageset_core/mouse_enabled");
            this.fyF = Toolkit.getDefaultToolkit().createCustomCursor(var1, new Point(0, 0), "SelectionCursor");
        }

        return this.fyF;
    }

    /**
     * SelectionCursor _combat
     *
     * @return
     */
    public Cursor aRP() {
        if (this.fyG == null) {
            Image var1 = this.fyI.adz().getImage("res://data/gui/imageset/imageset_core/mouse_combat");
            this.fyG = Toolkit.getDefaultToolkit().createCustomCursor(var1, new Point(0, 0), "SelectionCursor");
        }

        return this.fyG;
    }

    /**
     * HiddenCursor
     *
     * @return
     */
    public Cursor aRQ() {
        if (this.fyH == null) {
            this.fyH = Toolkit.getDefaultToolkit().createCustomCursor(new BufferedImage(1, 1, 2), new Point(0, 0), "HiddenCursor");
        }

        return this.fyH;
    }

    /**
     * SelectionCursor _enabled
     */
    public void aRR() {
        this.fyI.ddH().setCursor(this.aRO());
    }

    /**
     * HiddenCursor
     */
    public void aRS() {
        this.fyI.ddH().setCursor(this.aRQ());
    }

    /**
     * SelectionCursor _combat
     */
    public void aRT() {
        this.fyI.ddH().setCursor(this.aRP());
    }
}
