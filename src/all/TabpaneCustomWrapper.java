package all;

import javax.swing.*;
import java.awt.*;

public class TabpaneCustomWrapper extends JTabbedPane implements IContainerCustomWrapper {

    private IContainerCustomWrapper.a bJg = new IContainerCustomWrapper.a(this);

    public TabpaneCustomWrapper() {
        this.setBorder(new BorderWrapper());
        this.setBackground((Color) null);
    }

    public Dimension getMinimumSize() {
        return new Dimension(0, 0);
    }

    public Dimension getMaximumSize() {
        return new Dimension(800, 800);
    }

    public Dimension getPreferredSize() {
        return new Dimension(200, 200);
    }

    public void destroy() {
        if (this.getParent() != null) {
            this.getParent().remove(this);
        }

    }

    public JButton findJButton(String var1) {
        Component[] var5;
        int var4 = (var5 = this.getComponents()).length;

        for (int var3 = 0; var3 < var4; ++var3) {
            Component var2 = var5[var3];
            if (var2 instanceof JButton && var1.equals(var2.getName())) {
                return (JButton) var2;
            }

            if (var2 instanceof IContainerCustomWrapper) {
                JButton var6 = ((IContainerCustomWrapper) var2).findJButton(var1);
                if (var6 != null) {
                    return var6;
                }
            }
        }

        return null;
    }

    public Component findJComponent(String var1) {
        Component[] var5;
        int var4 = (var5 = this.getComponents()).length;

        for (int var3 = 0; var3 < var4; ++var3) {
            Component var2 = var5[var3];
            if (var1.equals(var2.getName())) {
                return var2;
            }

            if (var2 instanceof IContainerCustomWrapper) {
                Component var6 = ((IContainerCustomWrapper) var2).findJComponent(var1);
                if (var6 != null) {
                    return var6;
                }
            }
        }

        return null;
    }

    public JLabel findJLabel(String var1) {
        Component[] var5;
        int var4 = (var5 = this.getComponents()).length;

        for (int var3 = 0; var3 < var4; ++var3) {
            Component var2 = var5[var3];
            if (var2 instanceof JLabel && var1.equals(var2.getName())) {
                return (JLabel) var2;
            }

            if (var2 instanceof IContainerCustomWrapper) {
                JLabel var6 = ((IContainerCustomWrapper) var2).findJLabel(var1);
                if (var6 != null) {
                    return var6;
                }
            }
        }

        return null;
    }

    public ProgressBarCustomWrapper findJProgressBar(String var1) {
        Component[] var5;
        int var4 = (var5 = this.getComponents()).length;

        for (int var3 = 0; var3 < var4; ++var3) {
            Component var2 = var5[var3];
            if (var2 instanceof ProgressBarCustomWrapper && var1.equals(var2.getName())) {
                return (ProgressBarCustomWrapper) var2;
            }

            if (var2 instanceof IContainerCustomWrapper) {
                ProgressBarCustomWrapper var6 = ((IContainerCustomWrapper) var2).findJProgressBar(var1);
                if (var6 != null) {
                    return var6;
                }
            }
        }

        return null;
    }

    public TextfieldCustomWrapper findJTextField(String var1) {
        Component[] var5;
        int var4 = (var5 = this.getComponents()).length;

        for (int var3 = 0; var3 < var4; ++var3) {
            Component var2 = var5[var3];
            if (var2 instanceof TextfieldCustomWrapper && var1.equals(var2.getName())) {
                return (TextfieldCustomWrapper) var2;
            }

            if (var2 instanceof IContainerCustomWrapper) {
                TextfieldCustomWrapper var6 = ((IContainerCustomWrapper) var2).findJTextField(var1);
                if (var6 != null) {
                    return var6;
                }
            }
        }

        return null;
    }

    public IContainerCustomWrapper ce(String var1) {
        Component[] var5;
        int var4 = (var5 = this.getComponents()).length;

        for (int var3 = 0; var3 < var4; ++var3) {
            Component var2 = var5[var3];
            if (var2 instanceof IContainerCustomWrapper) {
                if (var1.equals(var2.getName())) {
                    return (IContainerCustomWrapper) var2;
                }

                IContainerCustomWrapper var6 = ((IContainerCustomWrapper) var2).ce(var1);
                if (var6 != null) {
                    return var6;
                }
            }
        }

        return null;
    }

    public RepeaterCustomWrapper ch(String var1) {
        Component[] var5;
        int var4 = (var5 = this.getComponents()).length;

        for (int var3 = 0; var3 < var4; ++var3) {
            Component var2 = var5[var3];
            if (var2 instanceof IContainerCustomWrapper) {
                RepeaterCustomWrapper var6 = ((IContainerCustomWrapper) var2).ch(var1);
                if (var6 != null) {
                    return var6;
                }
            }
        }

        return null;
    }

    public JComboBox findJComboBox(String var1) {
        Component[] var5;
        int var4 = (var5 = this.getComponents()).length;

        for (int var3 = 0; var3 < var4; ++var3) {
            Component var2 = var5[var3];
            if (var2 instanceof JComboBox && var1.equals(var2.getName())) {
                return (JComboBox) var2;
            }

            if (var2 instanceof IContainerCustomWrapper) {
                JComboBox var6 = ((IContainerCustomWrapper) var2).findJComboBox(var1);
                if (var6 != null) {
                    return var6;
                }
            }
        }

        return null;
    }

    public void pack() {
        this.setSize(this.getPreferredSize());
        this.validate();
    }

    public void setEnabled(boolean var1) {
        Component[] var5;
        int var4 = (var5 = this.getComponents()).length;

        for (int var3 = 0; var3 < var4; ++var3) {
            Component var2 = var5[var3];
            var2.setEnabled(var1);
        }

        super.setEnabled(var1);
    }

    public void setFocusable(boolean var1) {
        Component[] var5;
        int var4 = (var5 = this.getComponents()).length;

        for (int var3 = 0; var3 < var4; ++var3) {
            Component var2 = var5[var3];
            var2.setFocusable(var1);
        }

        super.setFocusable(var1);
    }

    public String getElementName() {
        return "tabpane";
    }

    public void Kk() {
        this.bJg.Kk();
    }

    public void Kl() {
        this.bJg.Kl();
    }

    public static class a extends PanelCustomWrapper {
        private static final long serialVersionUID = 1L;

        public a() {
            this.setLayout(new GridLayout());
            this.setBorder(new BorderWrapper());
        }

        public String getElementName() {
            return "tab";
        }
    }
}
