package all;

import taikodom.infra.script.I18NString;

import javax.swing.*;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Properties;

/**
 * Обёртка класса аддона логирование и локализация
 */
public class GK extends pp implements vW {
    private EngineGame engineGame;
    /**
     * Логирование класса аддона
     */
    private LogPrinter logPrinter;
    /**
     * Таблица локализации ключ - значение
     */
    private Properties cZz;

    /**
     * Обёртка класса аддона логирование и локализация
     *
     * @param var1
     * @param var2 ссылка на Addon manager
     * @param var3 ссылка на Загрузчик addons\taikodom.xml
     * @param var4 ссылка на Загруженный класс аддона
     */
    public GK(EngineGame var1, AddonManager var2, aVW var3, fo var4) {
        this.addonManager = var2;
        this.bCI = var4;
        this.eMb = var3;
        this.eLY = new dW(var2, this);
        this.dej = this.addonManager.alf();//class aBg
        this.eMa = new Ih(var2.aVU());
        this.engineGame = var1;
        this.logPrinter = LogPrinter.K(var4.getClass());//подписываем класс на логирование пример taikodom.addon.debugtools.taikodomversion.TaikodomVersionAddon
        this.cZz = this.g(var4.getClass(), I18NString.getCurrentLocation());//Загрузка локализации  аддона
    }
/*
   public aDJ aQ(aDJ var1, ahM var2) {
      ahM var3 = this.setGreen(var2);
      return se.aQ(var1, var3);
   }
*/
/*
   public DN aRt() {
      return (DN)this.engineGame.getRoot();
   }
*/
/*
   public DN ala() {
      return (DN)this.engineGame.getRoot();
   }
*/

    /**
     * Остановить работу аддона
     */
    public void dispose() {
        try {
            this.bHy().stop();
        } finally {
            super.dispose();
        }

    }
/*
   public void aQ(aDJ var1, aRz var2, final Action var3) {
      if (var2.isCollection()) {
         var1.aQ(var2, new aKr() {
            public void setGreen(aDJ var1, aRz var2, Object[] var3x) {
               var3.actionPerformed((ActionEvent)null);
            }

            public void aQ(aDJ var1, aRz var2, Object[] var3x) {
               var3.actionPerformed((ActionEvent)null);
            }
         });
      } else {
         var1.aQ(var2, new aiQ() {
            public void aQ(aDJ var1, aRz var2, Object var3x) {
               var3.actionPerformed((ActionEvent)null);
            }
         });
      }

   }
*/

    public String aQ(Class var1, String var2, String var3, Object... var4) {
        // I18NString[] var8 = Vf.gNs;
        int var7 = /*Vf.gNs.length*/0;

        for (int var6 = 0; var6 < var7; ++var6) {
            // I18NString var5 = var8[var6];
            // if (var5.get("convert").equalsIgnoreCase(var3))
            {
                //   return var5.get();
            }
        }

        if (var1 == null) {
            var1 = this.bHy().getClass();
        } else {
            Class var9 = var1.getEnclosingClass();
            if (var9 != null) {
                var1 = this.bHy().getClass();
            }
        }

        if (/*this.ala() != null*/ false) {
            // if (this.ala().aIY() != null)
            {
                //  return this.ala().aIY().aQ(var1, var2, var3, var4);
                // } else {
                return var4 != null && var4.length > 0 ? String.format(var3, var4) : var3;
            }
        } else if (this.cZz != null && this.cZz.containsKey(var3)) {
            return this.cZz.getProperty(var3);
        } else {
            Properties var10 = this.g(var1, I18NString.getCurrentLocation());
            return var10 != null && var10.containsKey(var3) ? var10.getProperty(var3) : "$" + var3;
        }
    }

    /*
       public akU dL() {
          return this.ala() == null ? null : this.ala().aPC();
       }
    */
/*
   public t alb() {
      return this.engineGame.alb();
   }
*/
    public LogPrinter alc() {
        return this.logPrinter;
    }

    public EngineGraphics getEngineGraphics() {
        return this.engineGame.getEngineGraphics();
    }

    public aBg alf() {
        return this.engineGame.alf();
    }

    public EngineGame getEngineGame() {
        return this.engineGame;
    }

    public ILoaderTrail alg() {
        return this.engineGame.getLoaderTrail();
    }

    public void aQ(String var1, final Action var2) {
        this.eMa.a(var1, aHk.class, new ags() {
            @Override
            public boolean WinInet(Object var1) {
                var2.actionPerformed((aHk) var1);
                return true;
            }
        });
    }

    public String af(String var1) {
        Collection var2 = this.aVU().a(var1 + "_KEY", String.class);
        return var2 != null && var2.size() > 0 ? (String) var2.iterator().next() : null;
    }
/*
   public void P(Object var1) {
      lo.aQ((ajJ_q)this.eMa, var1);
   }
*/
/*
   public void ignorableAtRule(Object var1) {
      lo.aQ(this.eMa, var1);
   }
*/

    /**
     * Загрузка локализации  аддона, получение таблицы ключ - значение
     *
     * @param var1 Имя класса аддона Пример taikodom.addon.debugtools.taikodomversion
     * @param var2 Язук локализации Пример convert
     * @return Таблица ключ - значение или null
     */
    private Properties g(Class var1, String var2) {
        awK var3 = (awK) var1.getAnnotation(awK.class);
        if (var3 == null) {//Класс не имеет перевода
            this.logPrinter.error("Calling class " + var1.getName() + " has test_GLCanvas @TranslationScope");
            return null;
        } else {
            InputStream var4 = null;
            try {
                var4 = this.f(var1, "nls." + var3.value() + "-" + var2 + ".properties"); //nls.taikodom.addon.debugtools.taikodomversion-en.properties
                if (var4 != null)//Файл открыт на чтение
                {
                    Properties var5 = new Properties();
                    var5.load(var4);
                    Properties var7 = var5;
                    return var7;
                }
            } catch (IOException var15) {
                this.logPrinter.error("Could not load local NLS properties file for language '" + var2 + "'", var15);
            } finally {
                if (var4 != null) {
                    try {
                        var4.close();
                    } catch (IOException var14) {
                        var14.printStackTrace();
                    }
                }
            }
            return null;
        }
    }
}
