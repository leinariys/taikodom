package all;

//package org.w3c.css.sac;
public interface IConditionalSelector extends ISimpleSelector {
    ISimpleSelector getSimpleSelector();

    Condition getCondition();
}
