package all;

import sun.swing.SwingUtilities2;

import javax.swing.*;
import javax.swing.plaf.basic.BasicInternalFrameTitlePane;
import java.awt.*;
import java.beans.PropertyChangeListener;

public class BasicInternalFrameTitlePaneCustomWrapper extends BasicInternalFrameTitlePane implements IComponentCustomWrapper {

    protected Icon paletteCloseIcon;
    protected int paletteTitleHeight;
    protected JLabel titleLabel = new JLabel();
    protected JPanel BL;
    private aeK BM = new ComponentManager("window-title", this);

    public BasicInternalFrameTitlePaneCustomWrapper(JInternalFrame var1) {
        super(var1);
        this.titleLabel.setName("title");
        ComponentManager.getCssHolder(this.titleLabel).setAttribute("name", "title");
        this.add(this.titleLabel);
        this.setBorder(new BorderWrapper());
    }

    // $FF: synthetic method
    static JInternalFrame getFrame(BasicInternalFrameTitlePaneCustomWrapper var0) {
        return var0.frame;
    }

    // $FF: synthetic method
    static JButton getCloseButton(BasicInternalFrameTitlePaneCustomWrapper var0) {
        return var0.closeButton;
    }

    // $FF: synthetic method
    static JButton getIconButton(BasicInternalFrameTitlePaneCustomWrapper var0) {
        return var0.iconButton;
    }

    // $FF: synthetic method
    static JButton getMaxButton(BasicInternalFrameTitlePaneCustomWrapper var0) {
        return var0.maxButton;
    }

    protected void installDefaults() {
        super.installDefaults();
        this.setFont(UIManager.getFont("InternalFrame.titleFont"));
        this.paletteTitleHeight = UIManager.getInt("InternalFrame.paletteTitleHeight");
        this.paletteCloseIcon = UIManager.getIcon("InternalFrame.paletteCloseIcon");
    }

    protected void createButtons() {
        this.BL = new JPanel();
        this.BL.setLayout(new FlowLayout(2, 0, 0));
        this.BL.setAlignmentY(1.0F);
        this.BL.setName("button-panel");
        ComponentManager.getCssHolder(this.BL).setAttribute("name", "button-panel");
        this.iconButton = new JButton();
        this.iconButton.addActionListener(this.iconifyAction);
        this.iconButton.setFocusable(false);
        this.iconButton.setAlignmentY(1.0F);
        ComponentManager.getCssHolder(this.iconButton).setAttribute("name", "iconify");
        this.iconButton.setName("iconify");
        this.maxButton = new JButton();
        this.maxButton.setFocusable(false);
        this.maxButton.addActionListener(this.maximizeAction);
        this.maxButton.setAlignmentY(1.0F);
        this.closeButton = new JButton();
        this.closeButton.setFocusable(false);
        this.closeButton.setAlignmentY(1.0F);
        this.closeButton.addActionListener(this.closeAction);
        ComponentManager.getCssHolder(this.closeButton).setAttribute("name", "close");
        this.closeButton.setName("close");
        this.setButtonIcons();
    }

    protected void addSubComponents() {
        this.add(this.BL);
        this.BL.add(this.iconButton);
        this.BL.add(this.closeButton);
    }

    protected void assembleSystemMenu() {
    }

    protected void addSystemMenuItems(JMenu var1) {
    }

    protected void showSystemMenu() {
    }

    protected PropertyChangeListener createPropertyChangeListener() {
        return null;
    }

    protected LayoutManager createLayout() {
        return new TitlePaneLayoutCustomWrapper();
    }

    public void paintComponent(Graphics var1) {
        PropertiesUiFromCss var2 = this.BM.Vp();
        this.titleLabel.setText(this.frame.getTitle());
        RM.a((Graphics) var1, (Component) this, (PropertiesUiFromCss) var2);
    }

    public aeK la() {
        return this.BM;
    }

    public String getElementName() {
        return "window-title";
    }

    class TitlePaneLayoutCustomWrapper extends TitlePaneLayout {
        TitlePaneLayoutCustomWrapper() {
            super();
        }

        public void addLayoutComponent(String var1, Component var2) {
        }

        public void removeLayoutComponent(Component var1) {
        }

        public Dimension preferredLayoutSize(Container var1) {
            return this.minimumLayoutSize(var1);
        }

        public Dimension minimumLayoutSize(Container var1) {
            int var2 = 30;
            if (BasicInternalFrameTitlePaneCustomWrapper.getFrame(BasicInternalFrameTitlePaneCustomWrapper.this).isClosable() && BasicInternalFrameTitlePaneCustomWrapper.getCloseButton(BasicInternalFrameTitlePaneCustomWrapper.this) != null) {
                var2 += BasicInternalFrameTitlePaneCustomWrapper.getCloseButton(BasicInternalFrameTitlePaneCustomWrapper.this).getPreferredSize().width;
            }

            FontMetrics var3 = BasicInternalFrameTitlePaneCustomWrapper.getFrame(BasicInternalFrameTitlePaneCustomWrapper.this).getFontMetrics(BasicInternalFrameTitlePaneCustomWrapper.this.getFont());
            String var4 = BasicInternalFrameTitlePaneCustomWrapper.getFrame(BasicInternalFrameTitlePaneCustomWrapper.this).getTitle();
            int var5 = var4 != null ? SwingUtilities2.stringWidth(BasicInternalFrameTitlePaneCustomWrapper.getFrame(BasicInternalFrameTitlePaneCustomWrapper.this), var3, var4) : 0;
            int var6 = var4 != null ? var4.length() : 0;
            int var7;
            if (var6 > 2) {
                var7 = SwingUtilities2.stringWidth(BasicInternalFrameTitlePaneCustomWrapper.getFrame(BasicInternalFrameTitlePaneCustomWrapper.this), var3, BasicInternalFrameTitlePaneCustomWrapper.getFrame(BasicInternalFrameTitlePaneCustomWrapper.this).getTitle().substring(0, 2) + "...");
                var2 += var5 < var7 ? var5 : var7;
            } else {
                var2 += var5;
            }

            boolean var17 = false;
            int var8 = var3.getHeight();
            byte var9 = -1;
            PropertiesUiFromCss var10 = BasicInternalFrameTitlePaneCustomWrapper.this.BM.Vp();
            if (var10 != null) {
                var8 = (int) ((float) var8 + var10.atP());
            }

            var7 = Math.max(Math.max(var8, var9), BasicInternalFrameTitlePaneCustomWrapper.getCloseButton(BasicInternalFrameTitlePaneCustomWrapper.this).getHeight());
            int var11 = var1.getWidth();
            int var12 = var1.getHeight();
            if (var10 != null) {
                int var13 = var10.atK().jp((float) var11);
                int var14 = var10.atL().jp((float) var11);
                int var15 = var10.atM().jp((float) var12);
                int var16 = var10.atJ().jp((float) var12);
                var2 += var13 + var14;
                var7 += var15 + var16;
                return new Dimension(var2, var7);
            } else {
                return new Dimension(0, 0);
            }
        }

        public void layoutContainer(Container var1) {
            int var2 = BasicInternalFrameTitlePaneCustomWrapper.this.getWidth();
            int var3 = BasicInternalFrameTitlePaneCustomWrapper.this.getHeight();
            BasicInternalFrameTitlePaneCustomWrapper.getCloseButton(BasicInternalFrameTitlePaneCustomWrapper.this).setVisible(BasicInternalFrameTitlePaneCustomWrapper.getFrame(BasicInternalFrameTitlePaneCustomWrapper.this).isClosable());
            BasicInternalFrameTitlePaneCustomWrapper.getIconButton(BasicInternalFrameTitlePaneCustomWrapper.this).setVisible(BasicInternalFrameTitlePaneCustomWrapper.getFrame(BasicInternalFrameTitlePaneCustomWrapper.this).isIconifiable());
            BasicInternalFrameTitlePaneCustomWrapper.getMaxButton(BasicInternalFrameTitlePaneCustomWrapper.this).setVisible(BasicInternalFrameTitlePaneCustomWrapper.getFrame(BasicInternalFrameTitlePaneCustomWrapper.this).isMaximizable());
            int var4 = BasicInternalFrameTitlePaneCustomWrapper.this.BL.getPreferredSize().width;
            BasicInternalFrameTitlePaneCustomWrapper.this.BL.setBounds(var2 - var4, 0, var4, var3);
            BasicInternalFrameTitlePaneCustomWrapper.this.titleLabel.setBounds(0, 0, var2, var3);
        }
    }
}
