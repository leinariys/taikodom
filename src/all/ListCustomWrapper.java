package all;

import javax.swing.*;
import javax.swing.event.ListSelectionListener;
import java.awt.event.MouseEvent;

public class ListCustomWrapper extends JList {

    public static int hde = -10;
    private MouseEvent hdf;

    public ListCustomWrapper(DefaultListModel var1) {
        super(var1);
    }

    public String getToolTipText(MouseEvent var1) {
        this.hdf = var1;
        if (wz.d(this) != null) {
            int var2 = this.locationToIndex(var1.getPoint());
            if (var2 != -1) {
                if (!this.getCellBounds(var2, var2 + 1).contains(var1.getPoint())) {
                    return null;
                }

                Object var3 = this.getModel().getElementAt(var2);
                if (var3 != null) {
                    return var3.toString();
                }
            }
        }

        return super.getToolTipText(var1);
    }

    public MouseEvent cHt() {
        return this.hdf;
    }

    public void c(MouseEvent var1) {
        this.hdf = var1;
    }

    public void setSelectionMode(int var1) {
        switch (var1) {
            case -10:
                this.setSelectionModel(new a((a) null));
                break;
            default:
                this.setSelectionModel(new DefaultListSelectionModel());
                super.setSelectionMode(var1);
        }

    }

    private class a implements ListSelectionModel {
        private a() {
        }

        // $FF: synthetic method
        a(a var2) {
            this();
        }

        public void addListSelectionListener(ListSelectionListener var1) {
        }

        public void addSelectionInterval(int var1, int var2) {
        }

        public void clearSelection() {
        }

        public int getAnchorSelectionIndex() {
            return -1;
        }

        public void setAnchorSelectionIndex(int var1) {
        }

        public int getLeadSelectionIndex() {
            return -1;
        }

        public void setLeadSelectionIndex(int var1) {
        }

        public int getMaxSelectionIndex() {
            return -1;
        }

        public int getMinSelectionIndex() {
            return -1;
        }

        public int getSelectionMode() {
            return -1;
        }

        public void setSelectionMode(int var1) {
        }

        public boolean getValueIsAdjusting() {
            return false;
        }

        public void setValueIsAdjusting(boolean var1) {
        }

        public void insertIndexInterval(int var1, int var2, boolean var3) {
        }

        public boolean isSelectedIndex(int var1) {
            return false;
        }

        public boolean isSelectionEmpty() {
            return false;
        }

        public void removeIndexInterval(int var1, int var2) {
        }

        public void removeListSelectionListener(ListSelectionListener var1) {
        }

        public void removeSelectionInterval(int var1, int var2) {
        }

        public void setSelectionInterval(int var1, int var2) {
        }
    }
}
