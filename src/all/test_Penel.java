package all;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * создаёт окно кнопки для вызова окна с выбором имени
 */
public class test_Penel {
    static JFrame BE;
    static String[] names = new String[]{"Arlo", "Cosmo", "Elmo", "Hugo", "Jethro", "Laszlo", "Milo", "Nemo", "Otto", "Ringo", "Rocco", "Rollo"};

    /**
     * @return
     */
    public static JPanel kW() {
        JLabel var0 = new JLabel("The chosen name:");
        final JLabel var1 = new JLabel(names[1]);
        var0.setLabelFor(var1);
        var1.setFont(kX());
        final JButton var2 = new JButton("Pick loadFileXml new name...");
        var2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent var1x) {//открытие контекстного окна
                String var2x = BD.a(test_Penel.BE, var2, "Baby names ending in O:", "Name Chooser", test_Penel.names, var1.getText(), "Cosmo  ");
                var1.setText(var2x);
            }
        });
        JPanel var3 = new JPanel();
        var3.setLayout(new BoxLayout(var3, 3));
        var3.setBorder(BorderFactory.createEmptyBorder(20, 20, 10, 20));
        var0.setAlignmentX(0.5F);
        var1.setAlignmentX(0.5F);
        var2.setAlignmentX(0.5F);
        var3.add(var0);
        var3.add(Box.createVerticalStrut(5));
        var3.add(var1);
        var3.add(Box.createRigidArea(new Dimension(150, 10)));
        var3.add(var2);
        return var3;
    }

    /**
     * @return
     */
    protected static Font kX() {
        String[] var0 = new String[]{"French Script", "FrenchScript", "Script"};
        String[] var1 = (String[]) null;
        String var2 = null;
        GraphicsEnvironment var3 = GraphicsEnvironment.getLocalGraphicsEnvironment();
        if (var3 != null) {
            var1 = var3.getAvailableFontFamilyNames();
        }

        if (var1 != null && var0 != null) {
            for (int var4 = 0; var2 == null && var4 < var0.length; ++var4) {
                for (int var5 = 0; var2 == null && var5 < var1.length; ++var5) {
                    if (var1[var5].startsWith(var0[var4])) {
                        Font var6 = new Font(var1[var5], 0, 1);
                        if (var6.canDisplay('A')) {
                            var2 = var1[var5];
                            System.out.println("Using font: " + var2);
                        }
                    }
                }
            }
        }

        return var2 != null ? new Font(var2, 0, 36) : new Font("Serif", 2, 36);
    }

    /**
     *
     */
    private static void kY() {
        BE = new JFrame("Name That Baby");//создали окно
        BE.setDefaultCloseOperation(3);
        JPanel var0 = kW();//Создали панель
        var0.setOpaque(true);
        var0.add(kW());
        BE.setContentPane(var0);//установили панедь в окно
        BE.pack();//Принять размер компонентов
        BE.setVisible(true);
    }

    public static void main(String[] var0) {
        test_Penel.kY();
    /*  SwingUtilities.invokeLater(new Runnable()
      {
         public void run() {
            test_Penel.kY();
         }
      });*/
    }
}
