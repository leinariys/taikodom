package all;

//package org.w3c.css.sac;
public interface ISiblingSelector extends ISelector {
    short ANY_NODE = 201;

    short getNodeType();

    ISelector getSelector();

    ISimpleSelector getSiblingSelector();
}
