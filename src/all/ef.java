package all;

public class ef {
    private long swigCPtr;

    protected ef(long var1, boolean var3) {
        this.swigCPtr = var1;
    }

    protected ef() {
        this.swigCPtr = 0L;
    }

    protected static long c(ef var0) {
        return var0 == null ? 0L : var0.swigCPtr;
    }
}
