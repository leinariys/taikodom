package all;

import taikodom.render.DrawContext;
import taikodom.render.graphics2d.DrawNode;
import taikodom.render.graphics2d.RGuiScene;

public class nu extends DrawNode {
    protected int bufferPosition;
    protected int count;
    protected int elementType;

    public nu(int var1, int var2, int var3) {
        this.bufferPosition = var2;
        this.count = var3;
        this.elementType = var1;
    }

    public void draw(DrawContext var1, RGuiScene var2) {
        var2.getIndexes().buffer().position(this.bufferPosition);
        var1.getGL().glDrawElements(this.elementType, this.count, 5125, var2.getIndexes().buffer());
    }
}
