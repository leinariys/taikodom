package all;

import org.apache.commons.logging.Log;
import taikodom.infra.comm.transport.msgs.TransportCommand;
import taikodom.infra.comm.transport.msgs.TransportResponse;
import taikodom.infra.enviroment.jmx.EnvironmentConsole;

import javax.management.InstanceAlreadyExistsException;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.io.*;
import java.lang.management.ManagementFactory;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class Jz implements Uh_q, ahw_q {
    private static final Log logger = LogPrinter.setClass(Jz.class);
    public static boolean eKb = false;

    static {
        String var0 = System.getProperty("Environment.DISABLE_PERSISTENCE");
        eKb = var0 != null && var0.equalsIgnoreCase("true");
    }

    public final long eKo;
    protected final FP eKw;
    private final Lt eJT;
    private final AtomicLong cZh;
    protected arE eKt;
    protected boolean disposed;
    protected Collection eKz;
    int eKc;
    Vv_q eKe;
    boolean eKl;
    boolean eKm;
    ThreadPoolExecutor eKq;
    boolean eKr;
    ThreadLocal eKD;
    Object[] eKI;
    private lj eJU;
    private arL eJV;
    private boolean eJW;
    private boolean eJX;
    private Uy_q eJY;
    private ComponentGraphicsResource eJZ;
    private Object eKa;
    private EnvironmentConsole eKd;
    private ArrayList eKf;
    private sz eKg;
    private Fm eKh;
    private boolean eKi;
    private ThreadLocal eKj;
    private ReadWriteLock eKk;
    private azC eKn;
    private kw_w eKp;
    private arL eKs;
    private aqc eKu;
    private Class eKv;
    private aqI eKx;
    private Xn eKy;
    private boolean initialized;
    private ain eKA;
    private vU eKB;
    private Thread eKC;
    private all.b qU;
    private ajs eKE;
    private ahw_q eKF;
    private Hr eKG;
    private LinkedBlockingQueue eKH;
    private boolean eKJ;
    private boolean eKK;

    public Jz(FP var1) {
        this(var1, true);
    }

    public Jz(FP var1, boolean var2) {
        this.cZh = new AtomicLong(1L);
        this.eJW = true;
        this.eJX = false;
        this.eJY = new Uy_q(this);
        this.eJZ = new ComponentGraphicsResource();
        this.eKc = 0;
        this.eKe = new Vv_q();
        this.eKf = null;
        this.eKh = new Fm(this);
        this.eKi = true;
        this.eKj = new ThreadLocal();
        this.eKk = new ReentrantReadWriteLock(true);
        this.eKl = true;
        this.eKn = new azC();
        this.disposed = false;
        this.eKy = new Xn() {
            public void brK() {
            }

            public boolean brL() {
                return false;
            }
        };
        this.initialized = false;
        this.eKz = new CopyOnWriteArrayList();
        this.eKD = new ThreadLocal();
        //this.eKF = atM.cVg;
        this.eKG = new Hr();
        this.eKI = new Object[1];
        this.eKJ = true;
        this.eKK = true;
        this.eKw = var1;
        this.eJT = var1.aPU();
        this.eKo = System.currentTimeMillis();
        this.eKA = var1.aQe();
        if (this.eKA == null) {
            this.eKA = new FileControl("data");
        }

        if (var2) {
            this.init();
        }

    }

    public static void aa(Object var0) {
    }

    public ain aQe() {
        return this.eKA;
    }

    public void init() {
        if (this.initialized) {
            throw new IllegalStateException("Environment already initialized");
        } else {
            this.initialized = true;
            logger.info("Initing Environment.");
            ahG var1 = this.eKw.aPT();
            Class var2 = this.eKw.aPV();
            boolean var3 = this.eKw.aPY();
            Object var4 = this.eKw.aPW();
            boolean var5 = this.eKw.aPX();
            this.eKi = this.eKw.aPZ();
            this.eKa = var4;
            this.eKv = var2;
            // this.bxz();
            this.eKm = this.bGv() == Lt.duv | !this.getClass().getName().startsWith("all");
            if (this.eJT == Lt.duv) {
                // this.eJU = new ch(this);
                this.aqT();
                if (this.eKw.atf()) {
                    logger.info("Ensure loaded");
                    this.bGH().aiL();
                    logger.info("Ensure done");
                }
            } else {
                // this.eJU = new ch(this);
                if (this.eKw.atf()) {
                    logger.info("Ensure loaded");
                    this.bGH().aiL();
                    logger.info("Ensure done");
                }

                this.cZh.set(9223372036853727232L);
            }

            try {
                File var6 = new File(System.getProperty("taikodom.obmap", "ob-client.map"));
                if (var6.exists()) {
                    logger.info("Using obfuscation map: " + var6.getAbsolutePath());
                    this.eKt.B((new lk(new FileInputStream(var6))).getMap());
                } else if (this.eJT == Lt.duv) {
                    logger.warn("Obfuscation map not found, use -Dtaikodom.obmap=ob_map_file");
                }
            } catch (FileNotFoundException var11) {
                logger.warn("Error reading obfuscation map", var11);
            } catch (IOException var12) {
                logger.warn("Error reading obfuscation map", var12);
            }
            //Все имена классов с 2-ух файлов
            Iterator var7 = this.bGH().aXV().iterator();

            while (var7.hasNext()) {
                String var13 = (String) var7.next();//имя класса
                this.eKt.jM(var13);
                this.eKt.jM("[EditorCustomWrapper" + var13 + ";");
                this.eKt.jM("[[EditorCustomWrapper" + var13 + ";");
            }

            this.eKr = this.bGv() == Lt.duw;
            int var14 = Math.max(5, Runtime.getRuntime().availableProcessors() * 2);
            this.eKH = new LinkedBlockingQueue(var14 * 20000);
            this.eKq = PoolThread.a(var14, var14 * 10, 60L, TimeUnit.SECONDS, this.eKH, "Command Executer", !this.eKr);
            logger.info("Setup transport manager.");
            if (this.eKw.aQd()) {
                this.a(var1);
            }

            if (this.eJT == Lt.duv && var1.iY()) {
                if (this.eKw.aPZ() || this.eKw.aPY()) {
                    if ((this.eKu = this.eKw.aQh()) == null) {
                        //  this.eKu = new RX(this, "Transaction log flusher");
                    }

                    this.eKu.f(this.eKA);
                    this.eKu.a(this);

                    this.eKu.init();
                }

                try {
                    this.a(var2, var3);
                } catch (Exception var9) {
                    throw new RuntimeException(var9);
                }

                if (var5) {
                    this.bGp();
                    return;
                }

                //this.eKp = new amB(this);
                //this.eKp.start();
            } else {
                logger.info("Resolving rt.");
                this.eJV = this.bxA().bFf();
                logger.info("Pull.");
                if (this.eJT == Lt.duv) {
                    // ((TD)this.eJV).ds();
                }

                logger.info("Rt resolved.");
            }

            if (this.eJT == Lt.duw) {
                this.bxB();
            } else {
                if (this.eKw.aQd()) {
                    // this.bGm();
                }

                this.eJW = false;
                if (this.eJV != null) {
                    this.aMr();
                }
            }

            this.eJW = false;
            if (this.eJT == Lt.duv && this.eKw.aPZ()) {
                this.eKu.start();
            }

        }
    }

    protected void aMr() {
    }

    /*
       private void bGm() {
          this.bGU().all(new aL() {
             public Class getMessageClass() {
                return all.class;
             }

             public akO all(all.setGreen var1, aTA var2) {
                Jz.this.parseStyleSheet(Jz.this.eKe.endPage(var1));
                return new aNk();
             }
          });
       }
    */
    protected void bxB() {
    }

    protected void a(ahG var1) {
        this.eKx = new aqI(this, var1);
    }

    /*
       protected void bxz() {
          this.eKt = new ij();
          this.eKt.at(this.eKv);
       }
    */
    public boolean bGn() {
        return this.eKr;
    }

    public void dI(boolean var1) {
        this.eKr = var1;
    }

    public final boolean bGo() {
        return this.eJW;
    }

    private void a(Class var1, boolean var2) {
        if (this.bGv() != Lt.duv) {
            throw new RuntimeException("Only the server can resurrect data");
        } else {
            if (var2) {
                try {
                    this.O(var1);
                } catch (Exception var11) {
                    throw new RuntimeException("Problems reading persisted data", var11);
                }
            }

            if (this.eJV == null && this.eKw.aQc()) {
                Xf_q var3;
                try {
                    // var3 = this.P(var1);
                } catch (Exception var10) {
                    throw new RuntimeException(var10);
                }

                // this.eJV = this.all(var3, 1L);
                long var4 = System.nanoTime();
                this.ayN();

                try {
                    this.eJV.yn().bFg();
                    this.ge(System.nanoTime() - var4);
                    if (this.aQh() != null) {
                        this.aQh().i((se_q) this.eJV);
                        this.aQh().bOc();
                    }
                } catch (Exception var9) {
                    try {
                        this.gd(System.nanoTime() - var4);
                    } catch (Exception var8) {
                        logger.error(var8);
                        logger.error(var9);
                    }

                    throw new adk_q("Error creating root", var9);
                }
            }

        }
    }

    private void O(Class var1) {
        //this.eJV = this.CreateJComponent(var1, 1L);
        if (!this.eKu.bOd()) {
            this.eJV.yn().bFg();
            if (this.eKu != null) {
                this.eKu.i((se_q) this.eJV);
            }

        } else {
            if (this.eKw.aQa()) {
                try {
                    this.eKu.bOe();
                } catch (Throwable var4) {
                    throw new RuntimeException("Error reading transaction log.", var4);
                }
            }

            System.gc();
            System.gc();
            logger.info("Checking repository sanity... ");
            Iterator var2 = this.eJU.iterator();

            while (var2.hasNext()) {
                se_q var3 = (se_q) var2.next();
                if (var3.bFT() && System.getProperty("fill-hollow-hack") != null) {
                    var3.cVj().A(false);
                    logger.error("Filling up hollow object " + var3.bFY());
                }
            }

        }
    }

    public void ayN() {
        aWq var1 = aWq.dDA();
        if (var1 != null) {
            var1.dDR();
        } else {
            this.bGs();

            aWq.b(this.currentTimeMillis(), (aDR) null, (Gr) null);
        }
    }

    public void gd(long var1) {
        aWq var3 = aWq.dDA();
        if (var3 != null) {
            if (var3.dDS() >= 0) {
                return;
            }

            var3.dispose();
        }

        this.gf(var1);
    }

    public void ge(long var1) {
        aWq var3 = aWq.dDA();
        if (var3 == null || var3.dDS() < 0) {
            try {
                if (var3.g(this) == -1) {
                    new RuntimeException();
                }

                if (var3.dDF() != null && var3.dDF().size() > 0) {
                    this.bGU().C(var3.dDF());
                }
            } catch (aoO var9) {
                var3.dDO();
                throw var9;
            } catch (InterruptedException var10) {
                var3.dDO();
                throw new RuntimeException(var10);
            } finally {
                var3.dispose();
                this.gf(var1);
            }

        }
    }

    public void bGp() {
        logger.info("Persist is beginning");
        if (this.bGv() != Lt.duv) {
            throw new RuntimeException("Only the server can persist data");
        } else {
            try {
                this.bGt();
                this.bGq();
                return;
            } finally {
                this.bGu();
            }

        }
    }

    private boolean bGq() {
        return this.eKu.bOc();
    }

    public azC bGr() {
        return this.eKn;
    }

    public void bGs() {
        if (this.eKw.aPZ()) {
            azC var1 = this.eKn;
            var1.han.incrementAndGet();
            var1.haq.incrementAndGet();
            this.eKk.readLock().lock();
        }

    }

    public void gf(long var1) {
        if (this.eKw.aPZ()) {
            this.eKk.readLock().unlock();
            azC var3 = this.eKn;
            if (this.bHd()) {
                var3.ham.addAndGet(var1);
                var3.har.addAndGet(var1);
                var3.has.addAndGet(var1 * var1);
                if (var1 < var3.hau.get()) {
                    var3.hau.set(var1);
                }

                if (var1 > var3.hav.get()) {
                    var3.hav.set(var1);
                }
            }
        }

    }

    public void bGt() {
        this.eKk.writeLock().lock();
    }

    public void bGu() {
        this.eKk.writeLock().unlock();
    }

    public final Lt bGv() {
        return this.eJT;
    }

    public boolean bGw() {
        return this.bGv() == Lt.duw && this.eKj.get() != null;
    }

    public void f(Runnable var1) {
        ((List) this.eKj.get()).add(var1);
    }

    public void bGx() {
        this.eKj.set(new ArrayList());
    }

    public void bGy() {
        List var1 = (List) this.eKj.get();
        if (var1 != null) {
            try {
                Iterator var3 = var1.iterator();

                while (var3.hasNext()) {
                    Runnable var2 = (Runnable) var3.next();

                    try {
                        var2.run();
                    } catch (Exception var5) {
                        logger.warn("Exception caught while executing an app queued call", var5);
                    }
                }
            } catch (Exception var6) {
                logger.warn("Exception caught while executing an app queued calls", var6);
            }

            this.eKj.set((Object) null);
        } else {
            logger.warn("should not call endQueueApplicationCalls without first calling beginQueueApplicationCalls");
        }

    }

    void exit(int var1) {
        this.HY();
        System.exit(var1);
    }

    public se_q a(apO var1) {
        se_q var2 = this.eJU.a(var1);
        if (var2 == null) {
            throw new RuntimeException("Unknown object id: " + var1.cqo());
        } else {
            return var2;
        }
    }

    public se_q c(apO var1) {
        se_q var2 = this.eJU.a(var1);
        return var2;
    }

    public Xf_q c(int var1, long var2) {
        lj var5 = this.eJU;
        se_q var4;
        synchronized (this.eJU) {
            apO var6 = new apO(var2);
            var4 = this.eJU.a(var6);
            if (var4 == null) {
                Class var7 = this.eKt.tS(var1);
                if (var7 == null) {
                    throw new IllegalArgumentException("Invalid class magic number " + var1);
                }

                var4 = this.a(var7, var6, this.bGv() == Lt.duv ? BH.cph : BH.cpi);
            }
        }

        return var4.yn();
    }

    public Xf_q c(Class var1, long var2) {
        lj var5 = this.eJU;
        se_q var4;
        synchronized (this.eJU) {
            apO var6 = new apO(var2);
            var4 = this.eJU.a(var6);
            if (var4 == null) {
                var4 = this.a(var1, var6, this.bGv() == Lt.duv ? BH.cph : BH.cpi);
                var4.iY(true);
            }
        }

        return var4.yn();
    }

    public se_q j(Xf_q var1) {
        return (se_q) var1.bFf();
    }

    public se_q a(Class var1, apO var2, BH var3) {
        Object var4 = /*this.bGZ() ?*/ new af() {
            @Override
            public ad_w aG(Class var1) {
                return null;
            }

            @Override
            public akO a(b var1, aTn_q var2) {
                return null;
            }

            @Override
            public Object i(Xf_q var1) {
                return null;
            }

            @Override
            public Xf_q yn() {
                return null;
            }

            @Override
            public void a(Class var1, Gr var2) {

            }

            @Override
            public Xf_q M(Class var1) {
                return null;
            }
        } /*: new TD()*/;

        try {
            ((se_q) var4).c(this);
            ((se_q) var4).all(var1, this.P(var1));
            ((se_q) var4).d(var2);
            ((se_q) var4).a(var3);
            this.eJU.a((se_q) var4);
            ((se_q) var4).dt();
            return (se_q) var4;
        } catch (Exception var6) {
            throw new RuntimeException("Error creating " + var1 + ":" + var2.cqo(), var6);
        }
    }

    public void e(se_q var1) {
        this.eJU.b(var1);
    }

    public void a(Class var1, se_q var2) {
        this.eJU.a(var2);
    }

    protected Xf_q bxA() {
        aBr var1;
        ZW var3 = new ZW();
        var3.setBlocking(true);
        TransportCommand var4 = new TransportCommand();
        this.eKx.a((xu_q) var4, (aTA) var3);
        TransportResponse var2 = this.bGU().cxz().b((b) null, (TransportCommand) var4);
        var1 = (aBr) aqI.a((Jz) this, (xu_q) var2);

        se_q var6 = this.a(this.eKv, var1.cJg(), this.bGv() == Lt.duv ? BH.cpj : BH.cpi);
        return var6.yn();
    }

    public Object a(aEc var1) {
        if (var1 instanceof Fv) {
            Fv var2 = (Fv) var1;

            try {
                return this.c(var2.aPv(), var2.Ej());
            } catch (Exception var4) {
                throw new gy(var4, "Error deserializing script: " + var2.Ej());
            }
        } else {
            throw new RuntimeException("Dont know how to_q handle " + var1);
        }
    }

    public Object all(amr var1) {
        if (var1 instanceof Xf_q) {
            arL var2 = ((Xf_q) var1).bFf();
            if (this.eJT == Lt.duw && var2.bFS()) {
                throw new IllegalStateException("You are trying to_q send all " + var2.bFU().getName() + " id=" + var2.hC().cqo() + " instanced on client side, to_q the server.");
            } else if (var2 == null) {
                logger.error("SERIOUS ERROR!!! null infra!");
                logger.error("null infra for " + var1.getClass());
                return var1;
            } else {
                return var2.dv();
            }
        } else {
            throw new RuntimeException("Dont know how to_q handle " + var1);
        }
    }
/*
   public se_q WinInet(Xf_q var1) {
      return this.WinInet(var1, this.cZh.incrementAndGet());
   }
*/

    public se_q d(Class var1, long var2) {
        return this.all(this.P(var1), var2);
    }


    public Xf_q P(Class var1) {
        try {
            Constructor var2 = var1.getDeclaredConstructor(aNg.class);
            var2.setAccessible(true);
            return (Xf_q) var2.newInstance(this.eKI);
        } catch (Exception var4) {
            String var3 = "Class to_q be instantiated " + var1;
            throw new RuntimeException(var3, var4);
        }
    }

/*
   public se_q ignorableAtRule(Class var1) {
      return this.WinInet(this.P(var1));
   }
*/

    public se_q all(Xf_q var1, long var2) {
        apO var4 = new apO(var2);
        Object var5 = /*this.bGZ() ?*/ new af() {
            @Override
            public akO a(b var1, aTn_q var2) {
                return null;
            }

            @Override
            public Object i(Xf_q var1) {
                return null;
            }

            @Override
            public void a(Class var1, Gr var2) {

            }

            @Override
            public Xf_q M(Class var1) {
                return null;
            }
        } /*: new TD()*/;
        ((se_q) var5).c(this);
        ((se_q) var5).all(var1.getClass(), var1);
        ((se_q) var5).d(var4);
        ((se_q) var5).iY(false);
        if (!this.eJU.c((se_q) var5)) {
            throw new RuntimeException("Given OID is already in use: " + var2);
        } else {
            this.gg(var2);
            ((se_q) var5).dt();
            // ((se_q)var5).all(getByte.cph);
            return (se_q) var5;
        }
    }

    private void gg(long var1) {
        if (this.eJT != Lt.duw) {
            boolean var3;
            do {
                long var4 = this.cZh.get();
                long var6 = Math.max(var1 + 1L, var4);
                var3 = this.cZh.compareAndSet(var4, var6);
            } while (!var3);

        }
    }

    /*
       public se_q R(Class var1) {
          return this.CreateJComponentAndAddInRootPane(var1, this.cZh.incrementAndGet());
       }*/
/*
   public aDJ S(Class var1) {
      return (aDJ)this.CreateJComponentAndAddInRootPane(var1, this.cZh.incrementAndGet()).yn();
   }
*/
/*
   public Xf_q T(Class var1) {
      if (this.bGv() == Lt.duw && var1.getAnnotation(aOg.class) != null && var1.getAnnotation(SAC_NOT_SUPPORTED_ERR.class) == null && var1.getAnnotation(NL.class) == null) {
         throw new IllegalAccessError("Clients can only create script objects annotated with @ClientOnly");
      } else if (this.bGv() == Lt.duv && var1.getAnnotation(SAC_NOT_SUPPORTED_ERR.class) != null) {
         throw new IllegalAccessError("Server can not create script objects annotated with @ClientOnly");
      } else {
         try {
            se_q var2 = this.CreateJComponent(var1, this.cZh.incrementAndGet());
            Xf_q var3 = var2.yn();
            return var3;
         } catch (SecurityException var4) {
            throw new RuntimeException(var4);
         } catch (IllegalArgumentException var5) {
            throw new RuntimeException(var5);
         } catch (InstantiationException var6) {
            throw new RuntimeException(var6);
         } catch (IllegalAccessException var7) {
            throw new RuntimeException(var7);
         }
      }
   }
*/
/*
   public se_q CreateJComponentAndAddInRootPane(Class var1, long var2) {
      if (this.bGv() == Lt.duw && var1.getAnnotation(SAC_NOT_SUPPORTED_ERR.class) == null) {
         throw new IllegalAccessError("Clients can only create script objects annotated with @ClientOnly");
      } else if (this.bGv() == Lt.duv && var1.getAnnotation(SAC_NOT_SUPPORTED_ERR.class) != null) {
         throw new IllegalAccessError("Server can not create script objects annotated with @ClientOnly");
      } else {
         try {
            se_q var4 = this.CreateJComponent(var1, var2);
            Xf_q var5 = var4.yn();
            var5.bFg();
            return var4;
         } catch (SecurityException var6) {
            throw new RuntimeException(var6);
         } catch (IllegalArgumentException var7) {
            throw new RuntimeException(var7);
         } catch (InstantiationException var8) {
            throw new RuntimeException(var8);
         } catch (IllegalAccessException var9) {
            throw new RuntimeException(var9);
         }
      }
   }
*/
    public arL bGz() {
        return this.eJV;
    }

    public lj bGA() {
        return this.eJU;
    }

    public void step(float var1) {
        this.eKx.gZ();
    }

    public void b(float var1, Executor var2) {
        this.eKx.b(var2);
    }

    public aDR a(aDR var1, Xf_q var2, boolean var3) {
        se_q var4 = this.j(var2);
        return this.eKe.a(var1, var4, var3);
    }

    public void i(all.b var1) {
        try {
            aDR var2 = this.eKe.g(var1);
            if (var2 != null && var2.cWs()) {
                this.i(var2);
            }
        } catch (InterruptedException var3) {
            var3.printStackTrace();
        }

    }

    public Hr bGB() {
        return this.eKG;
    }

    public void a(Hr var1) {
        this.eKG = var1;
    }

    aCh bGC() {
        if (this.eKD.get() == null) {
            this.eKD.set(new aCh());
        }

        return (aCh) this.eKD.get();
    }

    public all.b bGD() {
        return this.bGC().hmQ;
    }

    public arL bGE() {
        aDR var1 = this.eKe.f(this.bGD());
        return var1 == null ? null : var1.hDb;
    }

    public aDR bGF() {
        return this.eKe.f(this.bGD());
    }

    public long bGG() {
        return this.bGC() != null && this.bGC().gqU != null ? this.bGC().gqU.receivedTimeMilis() : 0L;
    }

    public ajs bGH() {
        if (this.eKE == null) {
            this.eKE = this.eKw.ate();
        }

        return this.eKE;
    }

    public void HY() {
        this.disposed = true;
        this.eKq.shutdown();
        if (this.eKx != null) {
            this.eKx.HY();
        }

        if (this.eKp != null) {
            this.eKp.HY();
        }

        this.eJY.dispose();
        this.bGA().dispose();
        if (this.bGv() == Lt.duv && this.eKu != null) {
            this.eKu.HY();
        }

        if (this.eKC != null) {
            this.eKC.interrupt();
        }

        this.eKC = null;
    }

    public se_q ab(Object var1) {
        return this.j((Xf_q) var1);
    }

    public final void a(Yw var1) {
        if (this.bGv() != Lt.duv) {
            throw new RuntimeException("Only the server can add transaction log");
        } else if (this.disposed) {
            logger.error("Environment is disposed. Can't push transactions");
        } else {
            this.eKn.haw.addAndGet((long) (var1.bHF() + var1.bHE()));
            if (!eKb) {
                if (this.eKi) {
                    this.eKu.c(var1);
                }
            }
        }
    }

    public arL bGI() {
        if (this.bGv() != Lt.duw) {
            throw new IllegalAccessError("Client only method!");
        } else {
            return this.eKs;
        }
    }

    public Vv_q bGJ() {
        return this.eKe;
    }

    public Xn bGK() {
        return this.eKy;
    }

    public void b(Xn var1) {
        this.eKy = var1;
    }
/*
   public void all(createCommentSelector var1) {
      if (this.eKf == null) {
         this.eKf = new ArrayList();
      }

      ArrayList var2 = this.eKf;
      synchronized(this.eKf) {
         this.eKf.add(var1);
      }
   }
*/
/*
   public void setGreen(createCommentSelector var1) {
      if (this.eKf != null) {
         ArrayList var2 = this.eKf;
         synchronized(this.eKf) {
            this.eKf.remove(var1);
         }
      }
   }
*/

    public void all(se_q var1, aJj var2) {
        if (this.eKf != null) {
            ArrayList var4 = this.eKf;
            List var3;
            synchronized (this.eKf) {
                var3 = (List) this.eKf.clone();
            }

            Iterator var5 = var3.iterator();
/*
         while(var5.hasNext()) {
            createCommentSelector var7 = (createCommentSelector)var5.next();
            var7.all(var1.yn(), var2);
         }*/
        }

    }

    public boolean bGL() {
        return this.eKf != null && this.eKf.size() > 0;
    }

    public final boolean bGM() {
        return this.eJX;
    }

    public void dJ(boolean var1) {
        this.eJX = var1;
    }

    public sz bGN() {
        return this.eKg;
    }

    public void a(sz var1) {
        this.eKg = var1;
    }

    public void f(se_q var1) {
        this.e(var1);
    }

    public void bGO() {
        long var3;
        for (Iterator var1 = this.eJU.iterator(); var1.hasNext(); this.ge(System.nanoTime() - var3)) {
            se_q var2 = (se_q) var1.next();
            var3 = System.nanoTime();
            this.ayN();

            try {
                // aDJ var5 = (aDJ)var2.yn();
                // this.createProcessingInstructionSelector(var5);
            } catch (RuntimeException var6) {
                var6.printStackTrace();
                this.gd(var3);
                throw var6;
            }
        }

    }

    /*
       private void createProcessingInstructionSelector(aDJ var1) {
          var1.__ressurectCalled = false;

          try {
             var1.aH();
             if (!var1.__ressurectCalled) {
                logger.warn("onRessurect call did not arrive to_q ScriptObject, please check the class hierarquy of " + var1.getClassBaseUi().getTegName() + ", the onRessurect method must call super.onRessurect");
             }
          } catch (Exception var3) {
             logger.warn("Problems running onRessurect", var3);
          }

       }
    */
    public arE bxy() {
        return this.eKt;
    }

    public final long currentTimeMillis() {
        return this.eKG.currentTimeMillis();
    }

    private void aqT() {
        MBeanServer var1 = ManagementFactory.getPlatformMBeanServer();
        if (var1 != null) {
            this.eKd = new EnvironmentConsole(this);

            try {
                String var2 = "Bitverse:name=Transactions";
                ObjectName var3 = new ObjectName(var2);
                var1.registerMBean(this.eKd, var3);
            } catch (InstanceAlreadyExistsException var4) {
                System.err.println(var4.getMessage());
            } catch (Exception var5) {
                var5.printStackTrace();
            }

        }
    }

    public Object aPW() {
        return this.eKa;
    }

    public void ac(Object var1) {
        this.eKa = var1;
    }

    public int getScriptObjectCount() {
        return this.eJU.size();
    }

    public EnvironmentConsole bGP() {
        return this.eKd;
    }

    public void bGQ() {
        this.eKu.bOf();
    }

    public Uy_q bGR() {
        return this.eJY;
    }

    /*
       public void all(ch.all var1) {
          if (this.bGv() == Lt.duw && var1.hD().getAnnotation(NL.class) == null && !var1.hF()) {
             DY var2 = new DY(var1.hC().cqo(), var1.hD());
             this.eKx.setGreen(this.eJV.hE(), (kU)var2);
          }

       }
    */
    public Fm bGS() {
        return this.eKh;
    }

    public void d(Fm var1) {
        this.eKh = var1;
    }

    public void dK(boolean var1) {
        this.eKi = var1;
    }

    /*
       public ajs ate() {
          return this.eKw.ate();
       }
    */
    public long getNextId() {
        return this.cZh.get();
    }

    public void a(aqc var1) {
        this.eKu = var1;
    }

    public aqc bGT() {
        return this.eKu;
    }

    public aqI bGU() {
        return this.eKx;
    }

    public void b(Thread var1) {
        this.eKC = var1;
    }

    public void bGV() {
   /*
      try {
         this.bGU().CreateJComponent((all.setGreen)null, new all());
      } catch (axI var2) {
         throw new RuntimeException(var2);
      } catch (ZX var3) {
         throw new RuntimeException(var3);
      }
     */
    }

    /*
       public Collection parsePriority(Xf_q var1) {
          return var1 instanceof aim ? ((aim)var1).aae() : this.eKz;
       }
    */
    public ahw_q bGW() {
        return (ahw_q) (this.eJT != Lt.duv ? this.eKF : this.eKG);
    }

    public void b(ahw_q var1) {
        this.eKF = var1;
    }

    /**
     * @deprecated
     */
    @Deprecated
    public void eP(long var1) {
        this.eKG.eP(var1);
    }

    /**
     * @deprecated
     */
    @Deprecated
    public void dL(boolean var1) {
        this.eKG.cN(var1);
    }

    public boolean bGX() {
        return this.bGv() == Lt.duw && !this.eKw.aQb();
    }

    public boolean bGY() {
        return this.bGv() == Lt.duw && this.eKw.aQb();
    }

    public boolean bGZ() {
        return this.bGv() == Lt.duv;
    }

    public boolean bHa() {
        return this.bGZ();
    }

    public boolean bHb() {
        return this.bGv() == Lt.duw;
    }

    public all.b hE() {
        return this.qU;
    }

    public void j(all.b var1) {
        this.qU = var1;
    }

    public void c(arL var1) {
        this.eKs = var1;
    }

    public void a(aqI var1) {
        this.eKx = var1;
    }

    protected void i(aDR var1) {
        this.eKz.remove(var1);
    }

    protected void h(aDR var1) {
        var1.a(aDR.a_q.iFm);
        this.eKz.clear();
        this.eKz.add(var1);
    }

    public vU bHc() {
        return this.eKB;
    }

    public boolean bHd() {
        return this.eKK;
    }

    public boolean bHe() {
        return this.eKJ;
    }

    public void dM(boolean var1) {
        this.eKK = var1;
    }

    public void dN(boolean var1) {
        this.eKJ = var1;
    }

    public Object b(Class var1, long var2) {
        lj var5 = this.eJU;
        se_q var4;
        synchronized (this.eJU) {
            apO var6 = new apO(var2);
            var4 = this.eJU.a(var6);
            if (var4 == null) {
                Object var11 = /*this.bGZ() ?*/ new af() {
                    @Override
                    public ad_w aG(Class var1) {
                        return null;
                    }

                    @Override
                    public akO a(b var1, aTn_q var2) {
                        return null;
                    }

                    @Override
                    public Object i(Xf_q var1) {
                        return null;
                    }

                    @Override
                    public Xf_q yn() {
                        return null;
                    }

                    @Override
                    public void a(Class var1, Gr var2) {

                    }

                    @Override
                    public Xf_q M(Class var1) {
                        return null;
                    }
                }/* : new TD()*/;

                Xf_q var10000;
                ((se_q) var11).c(this);
                //((se_q)var11).all(var1, this.P(var1));
                ((se_q) var11).d(var6);
                ((se_q) var11).a(this.bGv() == Lt.duv ? BH.cph : BH.cpi);
                this.eJU.a((se_q) var11);
                ((se_q) var11).iY(true);
                var10000 = ((se_q) var11).yn();

                return var10000;
            }
        }

        return var4.yn();
    }

    public Xf_q bxx() {
        return this.bGz().yn();
    }

    public void fL(long var1) {
        this.cZh.set(var1);
    }

    public aqc aQh() {
        return this.eKu;
    }

    public se_q g(se_q var1) {
        Class var2 = var1.yn().getClass();
        Object var3 = (se_q) this.eJZ.get(var2);
        if (var3 == null) {
            Xf_q var4 = (Xf_q) this.b(var2, this.cZh.incrementAndGet());
            var3 = (af) var4.bFf();
            ((se_q) var3).cVs();
            ((se_q) var3).hCm.A(false);
            ((se_q) var3).hCm.cS(true);
            se_q var5 = (se_q) this.eJZ.putIfAbsent(var2, var3);
            if (var5 != null) {
                var3 = var5;
            }
        }

        return (se_q) var3;
    }

    public kw_w bHf() {
        return this.eKp;
    }

    public int bHg() {
        return this.eKH.size();
    }

    public static class a_q extends kU implements Externalizable {

    }
}
