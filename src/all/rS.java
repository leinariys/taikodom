package all;

import com.hoplon.geometry.Vec3f;

import javax.vecmath.Quat4f;

public class rS {
    public static void a(ajD var0, ajD var1, Vec3f var2) {
        var0.m00 = var1.m00 * var2.x;
        var0.m01 = var1.m01 * var2.y;
        var0.m02 = var1.m02 * var2.z;
        var0.m10 = var1.m10 * var2.x;
        var0.m11 = var1.m11 * var2.y;
        var0.m12 = var1.m12 * var2.z;
        var0.m20 = var1.m20 * var2.x;
        var0.m21 = var1.m21 * var2.y;
        var0.m22 = var1.m22 * var2.z;
    }

    public static void b(ajD var0) {
        var0.m00 = Math.abs(var0.m00);
        var0.m01 = Math.abs(var0.m01);
        var0.m02 = Math.abs(var0.m02);
        var0.m10 = Math.abs(var0.m10);
        var0.m11 = Math.abs(var0.m11);
        var0.m12 = Math.abs(var0.m12);
        var0.m20 = Math.abs(var0.m20);
        var0.m21 = Math.abs(var0.m21);
        var0.m22 = Math.abs(var0.m22);
    }

    public static void a(ajD var0, float[] var1) {
        var0.m00 = var1[0];
        var0.m01 = var1[4];
        var0.m02 = var1[8];
        var0.m10 = var1[1];
        var0.m11 = var1[5];
        var0.m12 = var1[9];
        var0.m20 = var1[2];
        var0.m21 = var1[6];
        var0.m22 = var1[10];
    }

    public static void b(ajD var0, float[] var1) {
        var1[0] = var0.m00;
        var1[1] = var0.m10;
        var1[2] = var0.m20;
        var1[3] = 0.0F;
        var1[4] = var0.m01;
        var1[5] = var0.m11;
        var1[6] = var0.m21;
        var1[7] = 0.0F;
        var1[8] = var0.m02;
        var1[9] = var0.m12;
        var1[10] = var0.m22;
        var1[11] = 0.0F;
    }

    public static void a(ajD var0, float var1, float var2, float var3) {
        float var4 = (float) Math.cos((double) var1);
        float var5 = (float) Math.cos((double) var2);
        float var6 = (float) Math.cos((double) var3);
        float var7 = (float) Math.sin((double) var1);
        float var8 = (float) Math.sin((double) var2);
        float var9 = (float) Math.sin((double) var3);
        float var10 = var4 * var6;
        float var11 = var4 * var9;
        float var12 = var7 * var6;
        float var13 = var7 * var9;
        var0.setRow(0, var5 * var6, var8 * var12 - var11, var8 * var10 + var13);
        var0.setRow(1, var5 * var9, var8 * var13 + var10, var8 * var11 - var12);
        var0.setRow(2, -var8, var5 * var7, var5 * var4);
    }

    private static float b(ajD var0, Vec3f var1) {
        return var0.m00 * var1.x + var0.m10 * var1.y + var0.m20 * var1.z;
    }

    private static float c(ajD var0, Vec3f var1) {
        return var0.m01 * var1.x + var0.m11 * var1.y + var0.m21 * var1.z;
    }

    private static float d(ajD var0, Vec3f var1) {
        return var0.m02 * var1.x + var0.m12 * var1.y + var0.m22 * var1.z;
    }

    public static void a(Vec3f var0, Vec3f var1, ajD var2) {
        float var3 = b(var2, var1);
        float var4 = c(var2, var1);
        float var5 = d(var2, var1);
        var0.x = var3;
        var0.y = var4;
        var0.z = var5;
    }

    public static void a(ajD var0, Quat4f var1) {
        float var2 = var1.x * var1.x + var1.y * var1.y + var1.z * var1.z + var1.w * var1.w;

        assert var2 != 0.0F;

        float var3 = 2.0F / var2;
        float var4 = var1.x * var3;
        float var5 = var1.y * var3;
        float var6 = var1.z * var3;
        float var7 = var1.w * var4;
        float var8 = var1.w * var5;
        float var9 = var1.w * var6;
        float var10 = var1.x * var4;
        float var11 = var1.x * var5;
        float var12 = var1.x * var6;
        float var13 = var1.y * var5;
        float var14 = var1.y * var6;
        float var15 = var1.z * var6;
        var0.m00 = 1.0F - (var13 + var15);
        var0.m01 = var11 - var9;
        var0.m02 = var12 + var8;
        var0.m10 = var11 + var9;
        var0.m11 = 1.0F - (var10 + var15);
        var0.m12 = var14 - var7;
        var0.m20 = var12 - var8;
        var0.m21 = var14 + var7;
        var0.m22 = 1.0F - (var10 + var13);
    }

    public static void b(ajD var0, Quat4f var1) {
        Kt var2 = Kt.bcE();
        float var3 = var0.m00 + var0.m11 + var0.m22;
        float[] var4 = (float[]) var2.bcO().qW(4);
        if (var3 > 0.0F) {
            float var5 = (float) Math.sqrt((double) (var3 + 1.0F));
            var4[3] = var5 * 0.5F;
            var5 = 0.5F / var5;
            var4[0] = (var0.m21 - var0.m12) * var5;
            var4[1] = (var0.m02 - var0.m20) * var5;
            var4[2] = (var0.m10 - var0.m01) * var5;
        } else {
            int var9 = var0.m00 < var0.m11 ? (var0.m11 < var0.m22 ? 2 : 1) : (var0.m00 < var0.m22 ? 2 : 0);
            int var6 = (var9 + 1) % 3;
            int var7 = (var9 + 2) % 3;
            float var8 = (float) Math.sqrt((double) (var0.getElement(var9, var9) - var0.getElement(var6, var6) - var0.getElement(var7, var7) + 1.0F));
            var4[var9] = var8 * 0.5F;
            var8 = 0.5F / var8;
            var4[3] = (var0.getElement(var7, var6) - var0.getElement(var6, var7)) * var8;
            var4[var6] = (var0.getElement(var6, var9) + var0.getElement(var9, var6)) * var8;
            var4[var7] = (var0.getElement(var7, var9) + var0.getElement(var9, var7)) * var8;
        }

        var1.set(var4[0], var4[1], var4[2], var4[3]);
        var2.bcO().release(var4);
    }

    private static float a(ajD var0, int var1, int var2, int var3, int var4) {
        return var0.getElement(var1, var2) * var0.getElement(var3, var4) - var0.getElement(var1, var4) * var0.getElement(var3, var2);
    }

    public static void c(ajD var0) {
        float var1 = a(var0, 1, 1, 2, 2);
        float var2 = a(var0, 1, 2, 2, 0);
        float var3 = a(var0, 1, 0, 2, 1);
        float var4 = var0.m00 * var1 + var0.m01 * var2 + var0.m02 * var3;

        assert var4 != 0.0F;

        float var5 = 1.0F / var4;
        float var6 = var1 * var5;
        float var7 = a(var0, 0, 2, 2, 1) * var5;
        float var8 = a(var0, 0, 1, 1, 2) * var5;
        float var9 = var2 * var5;
        float var10 = a(var0, 0, 0, 2, 2) * var5;
        float var11 = a(var0, 0, 2, 1, 0) * var5;
        float var12 = var3 * var5;
        float var13 = a(var0, 0, 1, 2, 0) * var5;
        float var14 = a(var0, 0, 0, 1, 1) * var5;
        var0.m00 = var6;
        var0.m01 = var7;
        var0.m02 = var8;
        var0.m10 = var9;
        var0.m11 = var10;
        var0.m12 = var11;
        var0.m20 = var12;
        var0.m21 = var13;
        var0.m22 = var14;
    }
}
