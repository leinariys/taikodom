package all;

public class ahD {
    private long swigCPtr;

    protected ahD(long var1, boolean var3) {
        this.swigCPtr = var1;
    }

    protected ahD() {
        this.swigCPtr = 0L;
    }

    protected static long i(ahD var0) {
        return var0 == null ? 0L : var0.swigCPtr;
    }
}
