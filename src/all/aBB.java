package all;

//Обёртка сетевого сообщения
public class aBB {
    private final byte[] hhy;
    private final int offset;
    private final int length;
    private int messageId;
    private transient b hhw;
    private transient b hhx;

    public aBB(b var1, byte[] var2) {
        this.hhw = var1;
        this.hhy = var2;
        this.offset = 0;
        this.length = var2.length;
    }

    public aBB(byte[] var1) {
        this.hhy = var1;
        this.offset = 0;
        this.length = var1.length;
    }

    public aBB(ala var1) {
        this(var1.chG(), 0, var1.getCount());
    }

    public aBB(byte[] var1, int var2, int var3) {
        this.hhy = var1;//Сообщение
        this.offset = var2;//смещение
        this.length = var3;//Размер сообщения
    }

    public aBB(b var1, byte[] var2, int var3, int var4) {
        this.hhw = var1;
        this.hhy = var2;
        this.offset = var3;
        this.length = var4;
    }

    public String toString() {
        return aK.b(this.hhy, this.offset, this.length, " ", 16);
    }

    public final int getMessageId() {
        return this.messageId;
    }

    public final void setMessageId(int var1) {
        this.messageId = var1;
    }

    public final b cJl() {
        return this.hhw;
    }

    public final b cJm() {
        return this.hhx;
    }

    public final void k(b var1) {
        this.hhw = var1;
    }

    public final void l(b var1) {
        this.hhx = var1;
    }

    public final byte[] cJn() {
        return this.hhy;
    }

    public final int getOffset() {
        return this.offset;
    }

    public final int getLength() {
        return this.length;
    }
}
