package all;

/**
 *
 */
public class aeh extends aOG {
    public static adl fnq = new adl("JoystickAxisEvent");
    private int fnp;
    private float value;

    public aeh(int var1, float var2) {
        this.fnp = var1;
        this.value = var2;
    }

    public final int getAxis() {
        return this.fnp;
    }

    public final float getValue() {
        return this.value;
    }

    public adl Fp() {
        return fnq;
    }
}
