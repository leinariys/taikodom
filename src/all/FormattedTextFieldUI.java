package all;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicTextFieldUI;
import javax.swing.text.JTextComponent;
import java.awt.*;

/**
 * FormattedTextFieldUI
 */
public class FormattedTextFieldUI extends BasicTextFieldUI implements aIh {
    private final JFormattedTextField fNp;
    private final Ek bHX;
    private ComponentManager Rp;

    public FormattedTextFieldUI(JFormattedTextField var1) {
        this.fNp = var1;
        this.Rp = new aix("textfield", var1);
        var1.setSelectionColor(new Color(12, 92, 111));
        this.bHX = new Ek(this.Rp);
    }

    public static ComponentUI createUI(JComponent var0) {
        return new FormattedTextFieldUI((JFormattedTextField) var0);
    }

    public void update(Graphics var1, JComponent var2) {
        RM.a((aIh) this, (Graphics) var1, (JTextComponent) ((JTextComponent) var2));
        super.paint(var1, var2);
    }

    protected void installListeners() {
        super.installListeners();
        this.fNp.addKeyListener(this.bHX);
        this.fNp.addMouseListener(this.bHX);
    }

    protected void uninstallListeners() {
        super.uninstallListeners();
        this.fNp.removeKeyListener(this.bHX);
        this.fNp.removeMouseListener(this.bHX);
    }

    protected void paintBackground(Graphics var1) {
    }

    public Dimension getMinimumSize(JComponent var1) {
        RM.a((aIh) this, (JTextComponent) ((JTextComponent) var1));
        return this.wy().b(var1, super.getMinimumSize(var1));
    }

    public Dimension getMaximumSize(JComponent var1) {
        RM.a((aIh) this, (JTextComponent) ((JTextComponent) var1));
        return this.wy().a(var1, super.getMaximumSize(var1));
    }

    public Dimension getPreferredSize(JComponent var1) {
        RM.a((aIh) this, (JTextComponent) ((JTextComponent) var1));
        return this.Rp.c(var1, super.getPreferredSize(var1));
    }

    public ComponentManager wy() {
        return this.Rp;
    }

    /**
     * TODO похлже это public pv wy()
     *
     * @return Графический элемент
     */
    @Override
    public aeK wz() {
        return this.Rp;
    }
}
