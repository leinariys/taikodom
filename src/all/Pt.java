package all;

import com.hoplon.geometry.Vec3f;

public class Pt implements aqA {
    protected final Vec3f dQv = new Vec3f();
    protected float dQw = 0.0F;

    public Vec3f bny() {
        return this.dQv;
    }

    public float getRadius() {
        return this.dQw;
    }

    public void setRadius(float var1) {
        this.dQw = var1;
    }

    public void ad(Vec3f var1) {
        this.dQv.set(var1);
    }

    public void a(ajD var1, Vector3dOperations var2, Vector3dOperations var3) {
        float var4 = this.dQw;
        Vec3f var5 = this.dQv;
        double var6 = (double) (var1.m00 * var5.x + var1.m01 * var5.y + var1.m02 * var5.z);
        double var8 = (double) (var1.m10 * var5.y + var1.m11 * var5.y + var1.m12 * var5.z);
        double var10 = (double) (var1.m20 * var5.z + var1.m21 * var5.y + var1.m22 * var5.z);
        var2.x = var6 + (double) var4;
        var2.y = var8 + (double) var4;
        var2.z = var10 + (double) var4;
        var3.x = var6 - (double) var4;
        var3.y = var8 - (double) var4;
        var3.z = var10 - (double) var4;
    }

    public void i(float var1, float var2, float var3) {
        this.dQv.set(var1, var2, var3);
    }
}
