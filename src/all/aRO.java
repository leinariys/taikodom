package all;

import com.hoplon.geometry.Vec3f;

public class aRO {
    public static int N(Vec3f var0, Vec3f var1) {
        return (var0.x < -var1.x ? 1 : 0) | (var0.x > var1.x ? 8 : 0) | (var0.y < -var1.y ? 2 : 0) | (var0.y > var1.y ? 16 : 0) | (var0.z < -var1.z ? 4 : 0) | (var0.z > var1.z ? 32 : 0);
    }

    public static boolean a(Vec3f var0, Vec3f var1, Vec3f var2, Vec3f var3, float[] var4, Vec3f var5) {
        Kt var6 = Kt.bcE();
        var6.bcH().push();

        try {
            Vec3f var7 = (Vec3f) var6.bcH().get();
            Vec3f var8 = (Vec3f) var6.bcH().get();
            Vec3f var9 = (Vec3f) var6.bcH().get();
            Vec3f var10 = (Vec3f) var6.bcH().get();
            Vec3f var11 = (Vec3f) var6.bcH().get();
            Vec3f var12 = (Vec3f) var6.bcH().get();
            var7.sub(var3, var2);
            var7.scale(0.5F);
            var8.add(var3, var2);
            var8.scale(0.5F);
            var9.sub(var0, var8);
            var10.sub(var1, var8);
            int var13 = N(var9, var7);
            int var14 = N(var10, var7);
            if ((var13 & var14) == 0) {
                float var15 = 0.0F;
                float var16 = var4[0];
                var11.sub(var10, var9);
                float var17 = 1.0F;
                var12.set(0.0F, 0.0F, 0.0F);
                int var18 = 1;
                int var19 = 0;

                while (true) {
                    if (var19 >= 2) {
                        if (var15 <= var16) {
                            var4[0] = var15;
                            var5.set(var12);
                            return true;
                        }
                        break;
                    }

                    for (int var20 = 0; var20 != 3; ++var20) {
                        float var21;
                        if ((var13 & var18) != 0) {
                            var21 = (-JL.b(var9, var20) - JL.b(var7, var20) * var17) / JL.b(var11, var20);
                            if (var15 <= var21) {
                                var15 = var21;
                                var12.set(0.0F, 0.0F, 0.0F);
                                JL.a(var12, var20, var17);
                            }
                        } else if ((var14 & var18) != 0) {
                            var21 = (-JL.b(var9, var20) - JL.b(var7, var20) * var17) / JL.b(var11, var20);
                            var16 = Math.min(var16, var21);
                        }

                        var18 <<= 1;
                    }

                    var17 = -1.0F;
                    ++var19;
                }
            }
        } finally {
            var6.bcH().pop();
        }

        return false;
    }

    public static boolean j(Vec3f var0, Vec3f var1, Vec3f var2, Vec3f var3) {
        boolean var4 = true;
        var4 = var0.x <= var3.x && var1.x >= var2.x ? var4 : false;
        var4 = var0.z <= var3.z && var1.z >= var2.z ? var4 : false;
        var4 = var0.y <= var3.y && var1.y >= var2.y ? var4 : false;
        return var4;
    }

    public static boolean a(Vec3f[] var0, Vec3f var1, Vec3f var2) {
        Vec3f var3 = var0[0];
        Vec3f var4 = var0[1];
        Vec3f var5 = var0[2];
        if (Math.min(Math.min(var3.x, var4.x), var5.x) > var2.x) {
            return false;
        } else if (Math.max(Math.max(var3.x, var4.x), var5.x) < var1.x) {
            return false;
        } else if (Math.min(Math.min(var3.z, var4.z), var5.z) > var2.z) {
            return false;
        } else if (Math.max(Math.max(var3.z, var4.z), var5.z) < var1.z) {
            return false;
        } else if (Math.min(Math.min(var3.y, var4.y), var5.y) > var2.y) {
            return false;
        } else {
            return Math.max(Math.max(var3.y, var4.y), var5.y) >= var1.y;
        }
    }
}
