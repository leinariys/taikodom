package all;

import javax.swing.*;
import java.awt.*;

/**
 * Базовые элементы интерфейса
 * тег desktoppane JDesktopPane
 * class BaseUItegXML DesktoppaneCustom  BaseItemFactory
 */
public class DesktoppaneCustom extends BaseItemFactory {
    /**
     * Создание базового Component интерфейса
     * Вызывается из class BaseItem
     *
     * @param var1
     * @param var2
     * @param var3
     * @return
     */
    @Override
    protected JComponent CreatComponentCustom(MapKeyValue var1, Container var2, XmlNode var3) {
        return new JDesktopPane();
    }

}
