package all;

import java.util.HashMap;

public class uu {
    HashMap bvW = new HashMap();
    private boolean bvX = true;

    public static void main(String[] var0) {
        uu var1 = new uu();
        StringBuilder var2 = new StringBuilder();
        var2.append(var1.c("method", "sttt", "tran. time", 123456789L, "avg tran. time", 5.1234568E7F, "perc of up time", 2.342134E7F, "exec. count", Integer.valueOf(10))).append("\n");
        var2.append(var1.c("method", "sttt", "tran. time", 10L, "avg tran. time", 5.0F, "perc of up time", 0.1F, "exec. count", Integer.valueOf(10)));
        System.out.println(var2.toString());
    }

    public String c(Object... var1) {
        StringBuilder var2 = this.bvX ? new StringBuilder() : null;
        StringBuilder var3 = new StringBuilder();

        for (int var4 = 0; var4 < var1.length - 1; var4 += 2) {
            String var5 = (String) var1[var4];
            Object var6 = var1[var4 + 1];
            a var7 = (a) this.bvW.get(var4);
            if (var7 == null) {
                var7 = new a();
                var7.title = var5;
                var7.width = var5.length();
                if (var6 instanceof Integer) {
                    var7.width = Math.max(9, var7.width);
                    var7.format = "%," + var7.width + "CreateJComponent ";
                } else if (var6 instanceof String) {
                    var7.width = Math.max(80, Math.max(((String) var6).length(), var7.width));
                    var7.format = "%" + var7.width + "s ";
                } else if (var6 instanceof Long) {
                    var7.width = Math.max(20, var7.width);
                    var7.format = "%," + var7.width + "CreateJComponent ";
                } else if (var6 instanceof Float) {
                    var7.width = Math.max(20, var7.width);
                    var7.format = "%," + var7.width + "endPage ";
                }

                var7.fSK = "%" + var7.width + "s ";
                this.bvW.put(var4, var7);
                var2.append(String.format(var7.fSK, var5));
            }

            var3.append(String.format(var7.format, var6));
        }

        if (this.bvX) {
            this.bvX = false;
            return var2.toString() + "\n" + var3.toString();
        } else {
            return var3.toString();
        }
    }

    static class a {
        public int width;
        String title;
        String format;
        String fSK;
    }
}
