package all;

import javax.swing.*;
import java.awt.*;

public class PictureCustomWrapper extends JLabel implements IComponentCustomWrapper {
    private static final String PREFIX = "res://";
    private ILoaderImageInterface loaderImage;
    private Dimension imageSize;
    private float gPx;

    public PictureCustomWrapper() {
        this.gPx = 0.0F;
        this.setVerticalTextPosition(3);
        this.setHorizontalTextPosition(0);
        this.imageSize = new Dimension(-1, -1);
    }

    public PictureCustomWrapper(ILoaderImageInterface var1) {
        this();
        this.loaderImage = var1;
    }

    public static void setSizeImage(PictureCustomWrapper var0, Dimension var1) {
        Dimension var2 = var0.getImageSize();
        float var3 = (float) var1.height / (float) var2.height;
        float var4 = (float) var1.width / (float) var2.width;
        if (var3 > var4) {
            var0.setImageSize((int) Math.ceil((double) ((float) var2.width * var3)), var1.height);
        } else {
            var0.setImageSize(var1.width, (int) Math.ceil((double) ((float) var2.height * var4)));
        }
    }

    public Dimension getImageSize() {
        Icon var1 = this.getIcon();
        return var1 != null && this.imageSize.height < 0 && this.imageSize.width < 0 ? new Dimension(var1.getIconWidth(), var1.getIconHeight()) : this.imageSize;
    }

    public void setImageSize(int var1, int var2) {
        this.imageSize.setSize(var1, var2);
        if (this.getIcon() instanceof ImageIcon) {
            ImageIcon var3 = (ImageIcon) this.getIcon();
            this.d(var3.getImage());
            this.b(var3.getImage());

        }
    }

    public void setImage(String var1) {
        if (var1.startsWith(PREFIX)) {
            var1 = var1.substring(PREFIX.length());
        }
        this.setImage(this.loaderImage.getImage(var1));
    }

    public void setImage(Image var1) {
        if (var1 != null) {
            if (this.imageSize != null && this.imageSize.width >= 0 && this.imageSize.height >= 0) {
                var1 = this.d(var1);
            }
            this.b(var1);
        }
    }

    protected void b(Image var1) {
        this.setIcon(new ImageIcon(var1));
    }

    private Image d(Image var1) {
        return var1.getScaledInstance(this.imageSize.width, this.imageSize.height, 2);
    }

    public void a(ImagesetGui var1, String var2) {
        this.setImage(this.loaderImage.getImage(var1.getPath() + var2));
    }

    public Icon cCx() {
        return this.getIcon();
    }

    public void cCy() {
        this.setIcon((Icon) null);
    }

    public String getElementName() {
        return "picture";
    }

    public void paint(Graphics var1) {
        if (this.gPx != 0.0F) {
            Graphics2D var2 = (Graphics2D) var1.create();
            var2.rotate((double) this.gPx, (double) (this.getWidth() / 2), (double) (this.getHeight() / 2));
            super.paint(var2);
        } else {
            super.paint(var1);
        }
    }

    public float cCz() {
        return this.gPx;
    }

    public void kU(float var1) {
        this.gPx = var1;
    }
}
