package all;

import javax.swing.*;
import java.awt.*;

public interface IContainerCustomWrapper extends IComponentCustomWrapper {
    Component findJComponent(String var1);

    /**
     * Поиск элемента по атрибуту name
     *
     * @param var1 aCo
     * @return
     */
    JLabel findJLabel(String var1);

    ProgressBarCustomWrapper findJProgressBar(String var1);

    JButton findJButton(String var1);

    TextfieldCustomWrapper findJTextField(String var1);

    IContainerCustomWrapper ce(String var1);

    RepeaterCustomWrapper ch(String var1);

    JComboBox findJComboBox(String var1);

    void destroy();

    void pack();

    void Kk();

    void Kl();

    Component[] getComponents();

    Component getComponent(int var1);

    int getComponentCount();

    Component add(Component var1);

    Component add(String var1, Component var2);

    Component add(Component var1, int var2);

    void add(Component var1, Object var2);

    void add(Component var1, Object var2, int var3);

    LayoutManager getLayout();

    void setEnabled(boolean var1);

    void setFocusable(boolean var1);

    /**
     *
     */
    public static class a {
        private boolean atq;
        private b atr = new b();
        private Container ats;
        private PictureCustomWrapper att;
        private aDX SZ;
        private GJ atu;

        /**
         * @param var1
         */
        public a(Container var1) {
            this.ats = var1;
            this.atr.setVisible(false);
            this.att = new PictureCustomWrapper();
            ComponentManager.getCssHolder(this.att).setAttribute("class", "loading");
            this.att.setSize(50, 50);
        }

        /**
         *
         */
        public void Kk() {
            this.atq = true;
            this.atr.setVisible(true);
            this.att.setVisible(true);
            ComponentManager.getCssHolder(this.ats).aO(false);
            this.ats.repaint();
            this.atu = new GJ() {
                public void a(JComponent var1) {
                    if (a.this.ats.isShowing() && var1.isVisible()) {
                        a.this.SZ = new aDX(a.this.att, "[0..360] dur 3000", new jL_q() {
                            @Override
                            public void a(Component var1, float var2) {
                                a((PictureCustomWrapper) var1, (float) var2);
                            }

                            @Override
                            public float a(Component var1) {
                                return a((PictureCustomWrapper) var1);
                            }

                            public void a(PictureCustomWrapper var1, float var2) {
                                var1.kU(0.017453292F * var2);
                                a.this.ats.repaint();
                            }

                            public float a(PictureCustomWrapper var1) {
                                return var1.cCz();
                            }
                        }, a.this.atu);
                    }

                }
            };

            this.SZ = new aDX(this.att, "[0..360] dur 3000", new jL_q() {
                @Override
                public void a(Component var1, float var2) {
                    a((PictureCustomWrapper) var1, (float) var2);
                }

                @Override
                public float a(Component var1) {
                    return a((PictureCustomWrapper) var1);
                }

                public void a(PictureCustomWrapper var1, float var2) {
                    var1.kU(0.017453292F * var2);
                    a.this.ats.repaint();
                }

                public float a(PictureCustomWrapper var1) {
                    return var1.cCz();
                }
            }, this.atu);
        }

        /**
         *
         */
        public void Kl() {
            this.atq = false;
            this.atr.setVisible(false);
            this.att.setVisible(false);
            if (this.SZ != null) {
                this.SZ.kill();
                ComponentManager.getCssHolder(this.ats).aO(true);
            }

            this.ats.repaint();
        }

        /**
         * @param var1
         */
        public void paint(Graphics var1) {
            if (this.atq) {
                this.atr.setBounds(this.ats.getBounds());
                this.atr.paint(var1);
                var1.translate((this.atr.getWidth() - this.att.getWidth()) / 2, (this.atr.getHeight() - this.att.getHeight()) / 2);
                this.att.paint(var1);
            }

        }
    }

    /**
     *
     */
    public static class b extends JPanel implements IComponentCustomWrapper {

        public String getElementName() {
            return "overlay";
        }
    }
}
