package all;

import javax.swing.text.JTextComponent;

public final class aix extends ComponentManager {
    public aix(String var1, JTextComponent var2) {
        super(var1, var2);
    }

    public int Vi() {
        int var1 = super.Vi();
        if (!((JTextComponent) this.getCurrentComponent()).isEditable()) {
            var1 |= 64;
        }

        return var1;
    }
}
