package all;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicProgressBarUI;
import java.awt.*;

/**
 * ProgressBarUI
 */
public class ProgressBarUI extends BasicProgressBarUI implements aIh {
    private ComponentManager Rp;

    public ProgressBarUI(JProgressBar var1) {
        var1.setBorder((Border) null);
        var1.setBackground(new Color(0, 0, 0, 0));
        var1.setStringPainted(true);
        this.Rp = new ComponentManager("progress", var1);
    }

    public static ComponentUI createUI(JComponent var0) {
        return new ProgressBarUI((JProgressBar) var0);
    }

    public void paint(Graphics var1, JComponent var2) {
        RM.a((aIh) this, (JComponent) var2);
        ProgressBarCustomWrapper var3 = (ProgressBarCustomWrapper) var2;
        PropertiesUiFromCss var4 = this.Rp.Vp();
        if (var4 != null) {
            if (var3.gu()) {
                this.a(var1, var3, var4);
            } else {
                Image var5 = var4.ato();
                Image var6 = var4.fT(0);
                Image var7 = var4.atq();
                if (var5 == null || var6 == null || var7 == null) {
                    return;
                }

                Rectangle var8 = var3.getBounds();
                int var9 = var5.getWidth(var2);
                int var10 = var7.getWidth(var2);
                int var11 = var8.width - var9 - var10;
                int var12 = var5.getHeight(var2);
                int var13 = var8.height;
                var1.drawImage(var5, 0, 0, var9, var13, 0, 0, var9, var12, var2);
                var1.drawImage(var6, var9, 0, var9 + var11, var13, 0, 0, var6.getWidth(var2), var12, var2);
                var1.drawImage(var7, var9 + var11, 0, var9 + var11 + var10, var13, 0, 0, var10, var12, var2);
                int var14 = var3.getValue();
                int var15 = var3.getMaximum();
                int var16 = var15 == 0 ? 0 : var14 * var11 / var15;
                if (var14 > 0) {
                    Image var17 = var4.atA();
                    Image var18 = var4.atN();
                    Image var19 = var4.atg();
                    if (var17 == null || var18 == null || var19 == null) {
                        return;
                    }

                    var1.drawImage(var17, 0, 0, var9, var13, 0, 0, var9, var12, var2);
                    var1.drawImage(var19, var9, 0, var9 + var16, var13, 0, 0, var6.getWidth(var2), var12, var2);
                    var1.drawImage(var18, var9 + var16, 0, var9 + var16 + var10, var13, 0, 0, var10, var12, var2);
                }

                if (this.progressBar.isStringPainted()) {
                    this.paintString(var1, 0, 0, var8.width, var8.height, var16 + var10, var2.getInsets());
                }
            }

        }
    }

    public void a(Graphics var1, ProgressBarCustomWrapper var2, PropertiesUiFromCss var3) {
        Insets var4 = var2.getInsets();
        int var5 = var2.getWidth() - (var4.right + var4.left);
        int var6 = var2.getHeight() - (var4.top + var4.bottom);
        if (var5 > 0 && var6 > 0) {
            int var7;
            int var8;
            if (var2.gv()) {
                if (var2.gw() <= 0) {
                    return;
                }

                var7 = (int) Math.ceil((double) ((float) var5 / (float) var2.gw()));
                var8 = (int) Math.ceil((double) ((float) var5 / (float) var7));
            } else {
                if (var2.gx() <= 0) {
                    return;
                }

                var8 = (int) Math.ceil((double) ((float) var5 / (float) var2.gx()));
                var7 = (int) Math.ceil((double) ((float) var5 / (float) var8));
            }

            int var9 = var2.gy();
            var1.translate(-var9, 0);
            int var10 = 0;
            int var11 = var8 - var9;
            Image var12;
            Image var13;
            Image var14;
            int var15;
            int var16;
            int var17;
            int var18;
            if (var2.getValue() > 0) {
                var12 = var3.atg();
                var13 = var3.atA();
                var14 = var3.atN();
                if (var12 == null || var13 == null || var14 == null) {
                    return;
                }

                var15 = var2.getValue();
                var16 = var2.getMaximum();

                for (var17 = Math.round((float) var7 * ((float) var15 / (float) var16)); var10 < var17; ++var10) {
                    var18 = var8 * var10 + var9;
                    int var19 = var18 + var13.getWidth(var2);
                    int var20 = var19 + var11;
                    int var21 = var14.getWidth(var2);
                    int var22 = var12.getHeight(var2);
                    var1.drawImage(var13, var18, 0, var19, var6, 0, 0, var13.getWidth(var2), var22, var2);
                    var1.drawImage(var12, var19, 0, var20, var6, 0, 0, var12.getWidth(var2), var22, var2);
                    var1.drawImage(var14, var20, 0, var20 + var21, var6, 0, 0, var21, var22, var2);
                }
            }

            var12 = var3.fT(0);
            var13 = var3.ato();
            var14 = var3.atq();
            if (var12 != null && var13 != null && var14 != null) {
                while (var10 < var7) {
                    var15 = var8 * var10 + var9;
                    var16 = var15 + var13.getWidth(var2);
                    var17 = var16 + var11;
                    var18 = var14.getWidth(var2);
                    var13.getHeight(var2);
                    var1.drawImage(var13, var15, 0, var16, var6, 0, 0, var13.getWidth(var2), var13.getHeight(var2), var2);
                    var1.drawImage(var12, var16, 0, var17, var6, 0, 0, var12.getWidth(var2), var12.getHeight(var2), var2);
                    var1.drawImage(var14, var17, 0, var17 + var18, var6, 0, 0, var18, var14.getHeight(var2), var2);
                    ++var10;
                }

            }
        }
    }

    protected Color getSelectionBackground() {
        return this.Rp.Vp().fS(0);
    }

    protected Color getSelectionForeground() {
        return this.Rp.Vp().getColor();
    }

    public Dimension getPreferredSize(JComponent var1) {
        return this.Rp.c(var1, super.getPreferredSize(var1));
    }

    public Dimension getMinimumSize(JComponent var1) {
        PropertiesUiFromCss var2 = this.Rp.Vp();
        if (var2 != null) {
            Dimension var3 = var2.getMinimumSize();
            if (var3 == null) {
                Image var4 = var2.ato();
                Image var5 = var2.fT(0);
                Image var6 = var2.atq();
                int var7 = 0;
                int var8 = 0;
                if (var4 != null) {
                    var7 += var4.getWidth(var1);
                    var8 += var4.getHeight(var1);
                }

                if (var6 != null) {
                    var7 += var6.getWidth(var1);
                }

                if (var5 != null) {
                    var7 += var5.getWidth(var1);
                }

                var3 = new Dimension(var7, var8);
            }

            return var3;
        } else {
            return super.getMinimumSize(var1);
        }
    }

    /**
     * TODO похлже это public pv wy()
     *
     * @return Графический элемент
     */
    @Override
    public aeK wz() {
        return this.Rp;
    }
}
