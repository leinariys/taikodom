package all;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public abstract class Jm_f implements Gd, ajs {
    private static final LogPrinter logger = LogPrinter.K(Jm_f.class);
    /**
     * Список классов all.aDZ, all.aDZ$endPage
     */
    protected List djo = new ArrayList();
    int djs = 0;
    private String signature;
    private List djn = new ArrayList();
    private boolean djp;
    private atZ djq;
    private boolean djr;

    public Jm_f(atZ var1) {
        this.djq = var1;
        this.djr = true;
    }

    public static any C(Class var0) {
        try {
            Field var1 = var0.getDeclaredField("___iScriptClass");
            var1.setAccessible(true);
            return (any) var1.get((Object) null);
        } catch (SecurityException var2) {
            var2.printStackTrace();
        } catch (NoSuchFieldException var3) {
            var3.printStackTrace();
        } catch (IllegalArgumentException var4) {
            var4.printStackTrace();
        } catch (IllegalAccessException var5) {
            var5.printStackTrace();
        }

        System.err.println("No IScriptClass found for " + var0);
        return null;
    }

    public List A(Class var1) {
        this.aiL();
        List var2 = aKU.dgK();
        Iterator var4 = this.djn.iterator();

        while (true) {
            Class var3;
            do {
                if (!var4.hasNext()) {
                    return var2;
                }

                var3 = (Class) var4.next();
            } while (var3.getSuperclass() != var1 && !this.d(var3.getInterfaces(), var1));

            var2.add(var3);
        }
    }

    private boolean d(Object[] var1, Object var2) {
        Object[] var6 = var1;
        int var5 = var1.length;

        for (int var4 = 0; var4 < var5; ++var4) {
            Object var3 = var6[var4];
            if (var3 == var2) {
                return true;
            }
        }

        return false;
    }

    public synchronized void aiL() {
        if (!this.djp) {
            for (int var1 = 0; var1 < this.djo.size(); ++var1) {
                Class var2 = var1 < this.djn.size() ? (Class) this.djn.get(var1) : null;
                if (var2 == null) {
                    try {
                        var2 = Class.forName((String) this.djo.get(var1));
                    } catch (ClassNotFoundException var4) {
                        logger.error(var4);
                    } catch (Error var5) {
                        System.err.println("Problems reading class " + (String) this.djo.get(var1) + ":");
                        var5.printStackTrace();
                        throw var5;
                    }

                    this.djn.set(var1, var2);
                }

                if (this.djq != null) {
                    this.djq.ag(var1, this.djo.size());
                }
            }

            this.djp = true;
        }
    }

    public String getSignature() {
        this.aiL();
        if (this.signature != null) {
            return this.signature;
        } else {
            StringBuilder var1 = new StringBuilder();
            Iterator var3 = this.djn.iterator();

            while (var3.hasNext()) {
                Class var2 = (Class) var3.next();
                afW var4 = (afW) var2.getAnnotation(afW.class);
                var1.append(var4.aul());
            }

            return this.signature = aK.w(var1.toString());
        }
    }

    public List aXU() {
        this.aiL();
        return this.djn;
    }

    /**
     * Возвращает список классов
     *
     * @return
     */
    public List aXV() {
        return this.djo;
    }

    public void es(String var1) {
        if (this.djo.contains(var1)) {
            ++this.djs;
            if (this.djq != null) {
                this.djq.ag(this.djs, this.djo.size());
            }
        }

    }

    /**
     * Приходит название класса
     *
     * @param var1 Пример all.aDZ, all.aDZ$endPage
     */
    protected void eD(String var1) {
        this.djo.add(var1);
    }

    public any B(Class var1) {
        return C(var1);
    }
}
