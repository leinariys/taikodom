package all;

public class Ch extends vv {
    private final Vector3dOperations cameraOffset = new Vector3dOperations();
    private final bc_q cuU = new bc_q();
    private final aLH cuV = new aLH();
    private final Vector3dOperations cuW = new Vector3dOperations();
    private final aVS cuX = new aVS();

    public void a(bc_q var1, ajK var2) {
        this.cameraOffset.aA(var1.position);
        this.cameraOffset.negate();
        this.cuU.b(var1);
        this.cuU.position.set(0.0D, 0.0D, 0.0D);
        super.a(this.cuU, var2);
    }

    public boolean a(aLH var1) {
        this.cuV.g(var1);
        this.cuV.aK(this.cameraOffset);
        return super.a(this.cuV);
    }

    public boolean a(Pt var1) {
        this.cuX.center.setVec3f(var1.bny());
        this.cuX.center.add(this.cameraOffset);
        this.cuX.jcB = (double) var1.getRadius();
        return super.a(this.cuX);
    }

    public boolean a(aVS var1) {
        this.cuX.b(var1);
        this.cuX.center.add(this.cameraOffset);
        return super.a(var1);
    }

    public boolean v(Vector3dOperations var1) {
        this.cuW.aA(var1);
        this.cuW.add(this.cameraOffset);
        return super.v(var1);
    }

    public int b(aLH var1) {
        this.cuV.g(var1);
        this.cuV.aK(this.cameraOffset);
        return super.b(this.cuV);
    }
}
