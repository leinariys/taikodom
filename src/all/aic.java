package all;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.List;

public class aic extends Vd implements Externalizable {

    private Object bgK;
    private kU[] fMF;

    public aic() {
    }

    public aic(Object var1, List var2) {
        this.bgK = var1;
        if (var2 != null) {
            this.fMF = (kU[]) var2.toArray(new kU[var2.size()]);
        }

    }

    public Object getResult() {
        return this.bgK;
    }

    public kU[] aWo() {
        return this.fMF;
    }

    public void readExternal(ObjectInput var1) throws IOException, ClassNotFoundException {
        if (!var1.readBoolean()) {
            this.fMF = null;
        } else {
            int var2 = var1.readInt();
            kU[] var3 = this.fMF = new kU[var2];

            for (int var4 = 0; var4 < var2; ++var4) {
                var3[var4] = (kU) aUu_q.iXh.b(var1);
            }
        }

        this.bgK = var1.readObject();
    }

    public void writeExternal(ObjectOutput var1) throws IOException {
        if (this.fMF == null) {
            var1.writeBoolean(false);
        } else {
            var1.writeBoolean(true);
            kU[] var2 = this.fMF;
            int var3 = var2.length;
            var1.writeInt(var3);

            for (int var4 = 0; var4 < var3; ++var4) {
                aUu_q.iXh.a((ObjectOutput) var1, (Object) var2[var4]);
            }
        }

        var1.writeObject(this.bgK);
    }
}
