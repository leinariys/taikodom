package all;

import gnu.trove.TObjectIntHashMap;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.awt.*;

public final class TextfieldCustom extends TextComponentCustom {
    private static final LogPrinter logger = LogPrinter.setClass(TextfieldCustom.class);
    private static TObjectIntHashMap ihO = new TObjectIntHashMap();

    static {
        ihO.put("LEFT", 2);
        ihO.put("CENTER", 0);
        ihO.put("RIGHT", 4);
        ihO.put("LEADING", 10);
        ihO.put("TRAILING", 11);
    }

    /**
     * Создание базового Component интерфейса
     * Вызывается из class BaseItem
     *
     * @param var1
     * @param var2
     * @param var3
     * @return
     */
    @Override
    protected JComponent CreatComponentCustom(MapKeyValue var1, Container var2, XmlNode var3) {
        return new TextfieldCustomWrapper();
    }

    protected void a(MapKeyValue var1, TextfieldCustomWrapper var2, XmlNode var3) {
        super.a(var1, (JTextComponent) var2, var3);
        Integer var4 = checkValueAttribut((XmlNode) var3, (String) "cols", (LogPrinter) logger);
        if (var4 != null) {
            var2.setColumns(var4.intValue());
        }

        String var5 = var3.getAttribute("horizontalAlignment");
        if (var5 != null) {
            var2.setHorizontalAlignment(ihO.get(var5.toUpperCase()));
        }

    }

}
