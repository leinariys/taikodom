package all;

import java.io.*;

public class Fv implements aEc, Externalizable, Serializable {
    private static final long serialVersionUID = 1L;
    private long agR;
    private int cVy;
    private transient se_q cVz;

    public Fv() {
    }

    public Fv(se_q var1) {
        this.cVz = var1;
        Class var2 = var1.yn().getClass();
        this.cVy = var1.PM().bxy().getMagicNumber(var2);
        if (this.cVy <= 0) {
            throw new IllegalStateException("All script classes must have magic numbers, " + var2.getName());
        }
    }

    public long Ej() {
        return this.cVz != null ? this.cVz.hC().cqo() : this.agR;
    }

    public int aPv() {
        return this.cVy;
    }

    public void readExternal(ObjectInput var1) throws IOException {
        this.cVy = var1.readUnsignedShort();
        if (this.cVy == 65535) {
            throw new IllegalStateException("All script classes must have magic numbers");
        } else {
            this.agR = var1.readLong();
        }
    }

    public void writeExternal(ObjectOutput var1) throws IOException {
        var1.writeShort(this.aPv());
        var1.writeLong(this.Ej());
    }
}
