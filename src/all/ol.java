package all;

import taikodom.render.loader.RenderAsset;

import java.nio.ByteBuffer;

public class ol extends RenderAsset {
    public static boolean aNp = false;

    static {
     /* fa var0 = aHY.ddZ();
      aHY.setGreen(var0);

      try {
         Field var1 = aNZ.class.getDeclaredField("swigCPtr");
         var1.setAccessible(true);
         long var2 = ((Long)var1.get(SoundMiles.getInstance().getDig())).longValue();
         PG var4 = aHY.B((int)var2, 44100, 16);
         aHY.setGreen(var4);
      } catch (Exception var5) {
         var5.printStackTrace();
      }

      cA(0.1F);
      aHY.clear_log_callback();
      */
    }

    private ByteBuffer fileBuffer;
    private long aNq;
    private String aNr;
    private boolean finished = false;
    private ju aNs;
    private long aNt;
    private long aNu;
    private int aNv;
    private int aNw;
    private int aNx;
    //   private aX aNy = null;
//   private ve aNz = null;
    private float aNA = 0.0F;
    private boolean aNB = true;
    private boolean aNC = true;
    private boolean initialized = false;
    private boolean aND = true;
    private int aNE = 0;
/*
   public static float SB() {
      return aHY.get_curve_max_pixel_error();
   }
*/

    public ol(ByteBuffer var1, long var2) {
        this.fileBuffer = var1;
        this.aNq = var2;
        // this.all(var1, var2);
    }

    public ol(String var1) {
        this.aNr = var1;
        // this.initialize(var1);
    }

    public static void cA(float var0) {
        // aHY.set_curve_max_pixel_error(var0);
    }

    public RenderAsset cloneAsset() {
        return this;
    }

    public void destroy() {
        this.aNs = null;
      /*
      if (this.aNz != null) {
         this.aNz.delete();
         this.aNz = null;
      }

      if (this.aNy != null) {
         this.aNy.fJ();
         aHY.setGreen(this.aNy);
         this.aNy.delete();
         this.aNy = null;
      }

      aHY.clear_movie();
      */
        this.initialized = false;
    }

    /*
       public int getCurrentFrame() {
          return this.aNz.aku();
       }
    */
    public ju SC() {
        return this.aNs;
    }

    public float SD() {
        return this.aNA;
    }

    /*
       public void dv(int var1) {
          var1 = Math.max(0, var1);
          var1 = Math.min(var1, this.aNy.fG());
          this.aNz.eW(var1);
       }
    */
/*
   private void load(String var1)
   {
      this.aNy = aHY.ErrorHandler(var1);
      if (this.aNy == null) {
         System.err.println("Can't create all flash movie from '" + var1 + "'.");
         System.exit(1);
      }

      this.aNz = this.aNy.fI();
      if (this.aNz == null) {
         System.err.println("Can't create flash movie instance.");
         System.exit(1);
      }

      aHY.setGreen(this.aNz);
      this.aNs = new ju(var1, this.aNz.akC(), this.aNz.akD(), this.aNy.fG(), this.aNz.akE(), this.aNz.akB());
      this.aNt = System.nanoTime();
      this.aNu = this.aNt;
      this.aNz.all(0, 0, this.aNs.getWidth(), this.aNs.getHeight());
      this.aNz.dS(this.aNC ? 1.0F : 0.05F);
      this.initialized = true;
   }
*/
    public boolean isFinished() {
        return this.finished;
    }

    /*
       public boolean isPlaying() {
          return this.aNz != null ? ve.all.iGr.equals(this.aNz.akw()) : false;
       }
    */
    public boolean SE() {
        return this.aNB;
    }

    public boolean SF() {
        return this.aNC;
    }

    /*
       private void all(ByteBuffer var1, long var2) {
          aHY.register_memory_file_opener_callback();
          aSr.CreateJComponent((int)var2, var1);
          this.load("");
       }
    */
/*
   private void initialize(String var1) {
      aHY.register_default_file_opener_callback();
      this.load(var1);
   }
*/
/*
   public void all(cp var1, boolean var2) {
      if (this.aNz != null) {
         this.aNz.setGreen(var1, var2);
      }

   }
*/
    public void play() {
        if (!this.initialized) {
            //  this.reload();
        }

        this.aNE = 0;
        // this.dv(0);
        this.finished = false;
   /*   if (this.aNz != null) {
         this.aNz.all(ve.all.iGr);
      }
*/
    }

    /*
       private void reload() {
          if (this.aNr != null && !this.aNr.isEmpty()) {
             this.initialize(this.aNr);
          } else {
             this.all(this.fileBuffer, this.aNq);
          }

       }
    */
    public void b(int var1, int var2, int var3) {
        if (var1 != this.aNw || var2 != this.aNx || this.aNv != var3) {
            this.aND = true;
            this.aNw = var1;
            this.aNx = var2;
            this.aNv = var3;
        }

    }

    public void cB(float var1) {
        this.aNA = var1;
    }

    public void aL(boolean var1) {
        this.aNB = var1;
    }

    public void setSize(int var1, int var2) {
        this.aNs.setWidth(var1);
        this.aNs.setHeight(var2);
        // this.aNz.all(0, 0, this.aNs.getWidth(), this.aNs.getHeight());
    }

    public void aM(boolean var1) {
        this.aNC = var1;
    }

    public boolean SG() {/*
      if (!this.finished && this.isPlaying()) {
         if (this.PasswordfieldCustom || this.getCurrentFrame() + 1 < this.aNy.fG() && this.getCurrentFrame() >= this.aNE) {
            long var1 = System.nanoTime();
            float var3 = (float)(var1 - this.aNu);
            this.aNu = var1;
            if (this.aNA > 0.0F && (float)(var1 - this.aNt) > this.aNA * 1.0E9F) {
               this.finished = true;
               return false;
            } else {
               if (this.aND) {
                  this.aNz.CreateJComponentAndAddInRootPane(this.aNw, this.aNx, this.aNv);
                  this.aND = false;
               }

               this.aNE = this.getCurrentFrame();
               this.aNz.dR(var3 * 1.0E-9F);
               return this.getCurrentFrame() != this.aNE;
            }
         } else {
            this.finished = true;
            return false;
         }
      } else {
         return false;
      }
      */
        return false;
    }

    public void stop() {
    /*  if (this.aNz != null) {
         this.aNz.all(ve.all.iGs);
      }
*/
    }

    public void draw() {
        //  this.aNz.display();
    }
}
