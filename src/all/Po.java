package all;

import com.hoplon.geometry.Vec3f;

public class Po {
    public final Vec3f dPX = new Vec3f();
    public final Vec3f dPY = new Vec3f();
    public int dPZ;
    public int dQa;
    public int dQb;

    public void a(Po var1) {
        this.dPX.set(var1.dPX);
        this.dPY.set(var1.dPY);
        this.dPZ = var1.dPZ;
        this.dQa = var1.dQa;
        this.dQb = var1.dQb;
    }
}
