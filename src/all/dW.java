package all;

import javax.swing.*;
import java.awt.*;
import java.net.URL;
import java.util.Iterator;

/**
 * Создание дополнительных атрибутов локализации в класса обёртке xml файла
 */
public class dW extends tj {
    private static final String[] eJf = new String[]{"title", "text", "tooltip"};
    /**
     * ссылка на Обёртка класса аддона логирование и локализация
     */
    private final vW eJg;
    /**
     * Пример class taikodom.addon.debugtools.taikodomversion.TaikodomVersionAddon
     */
    private Class eJh;

    /**
     * Конструктор класса
     *
     * @param var1 ссылка на Addon manager
     * @param var2 ссылка на Обёртка класса аддона логирование и локализация
     */
    public dW(AddonManager var1, pp var2) {
        super(var1, var2);
        this.eJg = (vW) var2;
    }

    public String toString() {
        return "CTX: " + this.eJg + ", SCOPE: " + this.eJh;
    }

    /**
     * Загрузка .xml ресурса аддона, проверка класса
     *
     * @param var1 ссылка на Загруженный класс аддона
     * @param var2 Пример taikodomversion.xml
     * @param var3
     * @return
     */
    @Override
    protected IComponentCustomWrapper preparePath(Object var1, String var2, IContainerCustomWrapper var3) {
        Object var4;
        if (var1 != null && !(var1 instanceof URL)) {
            var4 = var1;
        }//Object LoginAddon
        else {
            var4 = this.eJg.bHy();
        }

        this.eJh = (Class) (var4 == null ? null : (var4 instanceof Class ? var4 : var4.getClass()));//Class LoginAddon
        return super.preparePath(var1, var2, var3);
    }

    /**
     * @param var1 Пример file:/C:/.../taikodom/addon/debugtools/taikodomversion/taikodomversion.xml
     * @param var2
     * @return
     */
    protected IComponentCustomWrapper preparePath(URL var1, IContainerCustomWrapper var2) {//oE
        ((EngineGame) this.eJg.getEngineGame()).cno().a(var1, new ahq_q() {
            public void a(URL var1) {
                dW.this.adA().restart();
            }
        });
        BaseUItegXML var3 = (BaseUItegXML) this.bkT.getBaseUItagXML();
        a var4 = new a(new MapKeyValue(var3.cnl(), var1));
        return super.a(var4, (URL) var1, (IContainerCustomWrapper) var2);
    }

    /**
     * Работа с атрибутами "title", "text", "tooltip" в xml файле и его потомками
     * Создание дополнительных атрибутов локализации
     *
     * @param var1 содержание xml файла
     */
    void e(XmlNode var1) {
        if (!var1.getFlagBool()) {
            String var2 = var1.getAttribute("ihint");
            String[] var6 = eJf;
            int var5 = eJf.length;

            for (int var4 = 0; var4 < var5; ++var4) {
                String var3 = var6[var4];
                String var7 = var1.getAttribute(var3);
                if (var7 != null) {
                    if (var2 != null) {
                        var1.addAttribute(var3, this.eJg.a(this.eJh, var2, var7, new Object[0]));
                    } else {
                        var1.addAttribute(var3, this.eJg.a(this.eJh, var7, new Object[0]));
                    }
                }
            }
            //Каждого потомка тега отправляем в этот метод
            Iterator var9 = var1.getChildrenTag().iterator();
            while (var9.hasNext()) {
                XmlNode var8 = (XmlNode) var9.next();
                this.e(var8);
            }

            var1.setFlagBool(true);
        }
    }

    class a extends MapKeyValue {
        public a(MapKeyValue var2) {
            super(var2, var2.getPathFile());
        }

        /**
         * Получить класс по имени тега в xml (Базовые элементы интерфейса)
         * Создать графический Component
         *
         * @param var1 Пример window
         * @return
         */
        public BaseItem getClassBaseUi(String var1) {
            BaseItem var2 = super.getClassBaseUi(var1);//класс  соответствующий имени тега в xml
//1
            return var2 != null ? var2 : new BaseItem()  //var2 == null ? null : new BaseItem() изменил, заглушка если не найдено соответствующий класс  соответствующий имени тега
            {
                @Override
                public JComponent CreateUI(MapKeyValue var1, Container var2x, XmlNode var3) {
                    dW.this.e(var3);
                    return (JComponent) var2.CreateUI(var1, var2x, var3);
                }

                @Override
                protected JComponent CreatComponentCustom(MapKeyValue var1, Container var2, XmlNode var3) {
                    return null;
                }
            };
        }

        //Контекст анализа переводов
        public String toString() {
            return "TranslationParseContext: " + dW.this.eJh;
        }
    }
}
