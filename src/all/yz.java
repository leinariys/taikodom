package all;

/**
 * Контейнер точки HTTP запроса
 */
public class yz {
    private String host;
    private String path;
    private String name;
    private String value;
    private long bLR;

    public yz(String var1, String var2, String var3, String var4, String var5) {
        this(var1, var2, var3, var4, 0L);

        long var6;
        try {
            var6 = Long.parseLong(var5);
        } catch (NumberFormatException var8) {
            var6 = 0L;
        }

        this.ds(var6);
    }

    public yz(String var1, String var2, String var3, String var4, long var5) {
        this.host = var1;
        this.path = var2;
        this.name = var3;
        this.value = var4;
        this.bLR = var5;
    }

    public String getHost() {
        return this.host;
    }

    public void setHost(String var1) {
        this.host = var1;
    }

    public String getPath() {
        return this.path;
    }

    public void setPath(String var1) {
        this.path = var1;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String var1) {
        this.name = var1;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String var1) {
        this.value = var1;
    }

    private void ds(long var1) {
        this.bLR = var1;
    }

    public long apY() {
        return this.bLR;
    }

    public String toString() {
        return "Host: " + this.host + "\n" + "path: " + this.path + "\n" + "name: " + this.name + "\n" + "value: " + this.value + "\n" + "expiry: " + this.bLR;
    }
}
