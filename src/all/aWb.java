package all;

/**
 * class aUM  aWb
 */
public interface aWb {
    void setAddonManager(AddonManager var1);

    // Object getRoot();

    // boolean cnn();

    /**
     * Ширина окна
     *
     * @return
     */
    int getScreenWidth();

    /**
     * Высота окна
     *
     * @return
     */
    int getScreenHeight();

    /**
     * Получить обёртку аддона с логирование и локализацией
     *
     * @param var1 ссылка на Addon manager
     * @param var2 ссылка на Загрузчик addons\taikodom.xml
     * @param var3 ссылка на Загруженный класс аддона
     * @return
     */
    aVf a(AddonManager var1, aVW var2, aMS var3);

    ajJ_q getEventManager();

    ain getRootPathRender();

    ain getRootPath();
}
