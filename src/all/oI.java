package all;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * class MusicAddon
 */
public class oI extends WA implements Externalizable {

    private a dB;

    public oI() {
    }

    public oI(a var1) {
        this.dB = var1;
    }

    public String toString() {
        return this.dB == null ? "<NO TRANSITION>" : this.dB.toString();
    }

    public a Us() {
        return this.dB;
    }

    public void readExternal(ObjectInput var1) throws IOException {
        this.dB = a.values()[var1.readInt()];
    }

    public void writeExternal(ObjectOutput var1) throws IOException {
        var1.writeInt(this.dB.ordinal());
    }

    public static enum a {
        iOV,
        iOW,
        iOX,
        iOY,
        iOZ,
        iPa,
        iPb,
        iPc,
        iPd,
        iPe,
        iPf,
        iPg,
        iPh,
        iPi,
        iPj,
        iPk,
        iPl,
        iPm,
        iPn,
        iPo,
        iPp,
        iPq,
        iPr,
        iPs,
        iPt,
        iPu,
        iPv,
        iPw,
        iPx,
        iPy;
    }
}
