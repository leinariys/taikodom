package all;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * class CommandTranslatorAddon
 */
public class aSd extends WA implements Externalizable {

    private String iLU;
    private boolean aBE;
    private jq iLV;

    public aSd() {
    }

    public aSd(String var1, boolean var2) {
        this.iLU = var1;
        this.aBE = var2;
    }

    public aSd(String var1, jq var2, boolean var3) {
        this.iLU = var1;
        this.iLV = var2;
        this.aBE = var3;
    }

    public boolean isPressed() {
        return this.aBE;
    }

    public String duc() {
        return this.iLU;
    }

    public void readExternal(ObjectInput var1) throws IOException, ClassNotFoundException {
        this.iLU = (String) var1.readObject();
        this.aBE = var1.readBoolean();
    }

    public void writeExternal(ObjectOutput var1) throws IOException {
        var1.writeObject(this.iLU);
        var1.writeBoolean(this.aBE);
    }

    public String toString() {
        return "Command:  " + this.iLU + " " + this.aBE;
    }

    public jq dud() {
        return this.iLV;
    }
}
