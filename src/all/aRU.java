package all;

public enum aRU {
    iLf(2),
    iLg(4) {
        public int t(int var1, int var2) {
            return var1 - var2;
        }
    },
    iLh(0) {
        public int t(int var1, int var2) {
            return (var1 - var2) / 2;
        }
    },
    iLi(0) {
        public int u(int var1, int var2) {
            return var1;
        }
    };

    private final int bzK;

    private aRU(int var3) {
        this.bzK = var3;
    }

    // $FF: synthetic method
    aRU(int var3, aRU var4) {
        this(var3);
    }

    public int akW() {
        return this.bzK;
    }

    public int t(int var1, int var2) {
        return 0;
    }

    public int u(int var1, int var2) {
        return var2;
    }
}
