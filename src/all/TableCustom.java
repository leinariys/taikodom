package all;

import gnu.trove.TObjectIntHashMap;

import javax.swing.*;
import javax.swing.table.JTableHeader;
import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;

public class TableCustom extends BaseItemFactory {
    private static TObjectIntHashMap modes = new TObjectIntHashMap();

    static {
        modes.put("ALL_COLUMNS", 4);
        modes.put("LAST_COLUMN", 3);
        modes.put("NEXT_COLUMN", 1);
        modes.put("OFF", 0);
        modes.put("SUBSEQUENT_COLUMNS", 2);
    }

    /**
     * Создание базового Component интерфейса
     * Вызывается из class BaseItem
     *
     * @param var1
     * @param var2
     * @param var3
     * @return
     */
    @Override
    protected JComponent CreatComponentCustom(MapKeyValue var1, Container var2, XmlNode var3) {
        return new TableCustomWrapper(var1, var3);
    }

    protected void a(MapKeyValue var1, TableCustomWrapper var2, XmlNode var3) {
        this.a(var1, var2, var3);
        boolean var4 = false;
        Iterator var6 = var3.getChildrenTag().iterator();

        while (var6.hasNext()) {
            XmlNode var5 = (XmlNode) var6.next();
            if (!var4 && "header".equals(var5.getTegName())) {
                var2.d(this.b(var5));
                var4 = true;
            }
        }

        if (!var4) {
            var2.setTableHeader((JTableHeader) null);
        }

    }

    private Object[] b(XmlNode var1) {
        ArrayList var2 = new ArrayList();
        Iterator var4 = var1.getChildrenTag().iterator();

        while (var4.hasNext()) {
            XmlNode var3 = (XmlNode) var4.next();
            if ("title".equals(var3.getTegName())) {
                var2.add(var3.getAttribute("text"));
            }
        }

        return var2.toArray();
    }

    protected void b(MapKeyValue var1, TableCustomWrapper var2, XmlNode var3) {
        super.createLayout(var1, var2, var3);
        String var4 = var3.getAttribute("rowHeight");
        if (var4 != null) {
            var2.setRowHeight(Integer.parseInt(var4));
        }

        String var5 = var3.getAttribute("autoResizeMode");
        if (var5 != null) {
            var2.setAutoResizeMode(modes.get(var5.replace('-', '_').toUpperCase()));
        }

        if ("true".equals(var3.getAttribute("singleLine"))) {
            var2.setSelectionMode(0);
            var2.setRowSelectionAllowed(true);
            var2.setColumnSelectionAllowed(false);
        }

    }


    public static class a extends BaseItem {
        /**
         * Создание базового Component интерфейса
         * Вызывается из class BaseItem
         *
         * @param var1
         * @param var2
         * @param var3
         * @return
         */
        @Override
        protected JComponent CreatComponentCustom(MapKeyValue var1, Container var2, XmlNode var3) {
            return new JLabel();
        }
    }
}
