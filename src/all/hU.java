package all;

import com.hoplon.geometry.Vec3f;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.*;

/**
 * Работа с каталогом физики игры
 */
public class hU {
    /**
     * Кеш
     */
    private static final Map cache = Collections.synchronizedMap(new HashMap());
    /**
     * Каталог кода физики игры
     */
    private static ain rootPathResource = new FileControl(System.getProperty("physics.resources.rootpath", "."));

    /**
     * Получить каталог кода физики игры
     *
     * @return
     */
    public static ain getRootPathResource() {
        return rootPathResource;
    }

    public static void setRootPathResource(ain rootPathResource) {
        hU.rootPathResource = rootPathResource;
    }

    public static yK b(String var0, boolean var1) {
        String var2 = var0.replace(".pro", ".msh").replace("data/models", "res/shapes");
        String var3 = var2 + "/" + var1;
        yK var4 = (yK) cache.get(var3);
        if (var4 != null) {
            return var4;
        } else {
            XmlNode var5 = null;
            InputStream var6 = null;

            try {
                var6 = rootPathResource.concat(var2).getInputStream();
                var5 = (new axx()).a(var6, "UTF-8");
            } catch (IOException var30) {
                throw new RuntimeException("PHYSICS RESOURCE " + var2 + " NOT FOUND.");
            } finally {
                try {
                    if (var6 != null) {
                        var6.close();
                    }
                } catch (Exception var29) {
                    ;
                }

            }

            String var7 = var1 ? "concave" : "convex";
            List var8 = var5.q("shape", "type", var7);
            if (var8.size() == 0) {
                var8 = var5.q("shape", "type", "spherical");
                float var32 = Float.parseFloat(((XmlNode) var8.get(0)).mD("sphere").getAttribute("radius"));
                return new alI(var32);
            } else if (var8.size() > 1) {
                throw new RuntimeException("PHYSICS RESOUCE " + var2 + " CONTAINS MORE THAN ONE SHAPE FOR THE SAME ASSET.");
            } else {
                List var9;
                if (!var1) {
                    var9 = ((XmlNode) var8.get(0)).q("point", (String) null, (String) null);
                    Vec3f[] var33 = new Vec3f[var9.size()];

                    for (int var34 = 0; var34 < var9.size(); ++var34) {
                        Vec3f var36 = new Vec3f(Float.parseFloat(((XmlNode) var9.get(var34)).getAttribute("x")), Float.parseFloat(((XmlNode) var9.get(var34)).getAttribute("y")), Float.parseFloat(((XmlNode) var9.get(var34)).getAttribute("z")));
                        var33[var34] = var36;
                    }

                    SC var35 = new SC(var33);
                    cache.put(var3, var35);
                    return var35;
                } else {
                    var9 = ((XmlNode) var8.get(0)).q("mesh", (String) null, (String) null);
                    aRF var12 = new aRF();
                    Iterator var14 = var9.iterator();

                    while (var14.hasNext()) {
                        XmlNode var13 = (XmlNode) var14.next();
                        XmlNode var15 = var13.mD("indexes");
                        XmlNode var16 = var13.mD("vertices");
                        String var10 = var15.getAttribute("value");
                        String var11 = var16.getAttribute("value");
                        StringTokenizer var17 = new StringTokenizer(var10, " ");
                        int var18 = var17.countTokens();
                        ByteBuffer var19 = ByteBuffer.allocate(var18 * 4);
                        int var20 = 0;

                        while (var17.hasMoreTokens()) {
                            ++var20;
                            var19.putInt(Integer.parseInt(var17.nextToken()));
                        }

                        var17 = new StringTokenizer(var11, " ");
                        int var21 = var17.countTokens();
                        ByteBuffer var22 = ByteBuffer.allocate(var21 * 4);
                        int var23 = 0;

                        while (var17.hasMoreTokens()) {
                            ++var23;
                            var22.putFloat(Float.parseFloat(var17.nextToken()));
                        }

                        wW var24 = new wW();
                        var24.bFf = var18 / 3;
                        var24.bFg = var19;
                        var24.bFh = 12;
                        var24.numVertices = var21 / 3;
                        var24.bFi = var22;
                        var24.bFj = 12;
                        var12.a(var24);
                    }

                    eh var37 = new eh(var12, true);
                    cache.put(var3, var37);
                    return var37;
                }
            }
        }
    }

}
