package all;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * interface tN
 */
public abstract class acG_q implements Serializable {
    protected static final long serialVersionUID = 1L;
    private static final char dBz = '"';
    private static final String dBA = "\\\"";
    private static final char dBC = ',';
    private static final String feD = "App";
    private static final String[][] feE = new String[][]{{"LoginLog", "UserLoginLog"}, {"LogoutLog", "UserLogoutLog"}};
    private static final SimpleDateFormat dBH = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    public static String feB = "Log";
    public static String feC = "yyyy-MM-dd HH:mm:ss.SSS";
    public static char dBB = ';';
    private Date feF;
    private long feG;
    private String feH;

    protected acG_q() {
    }

    public static String W(Class var0) {
        return "App" + X(var0);
    }

    public static String X(Class var0) {
        String var1 = var0.getSimpleName();
        if (var1.endsWith("Log")) {
            var1 = var1.substring(0, var1.length() - 3);
        }

        return var1;
    }

    public static List Y(Class var0) {
        ArrayList var1 = new ArrayList();
        Field[] var2 = acG_q.class.getDeclaredFields();
        Field[] var6 = var2;
        int var5 = var2.length;

        for (int var4 = 0; var4 < var5; ++var4) {
            Field var3 = var6[var4];
            if ((var3.getModifiers() & 24) == 0) {
                var1.add(var3);
            }
        }

        Field[] var8 = var0.getDeclaredFields();
        Field[] var7 = var8;
        int var10 = var8.length;

        for (var5 = 0; var5 < var10; ++var5) {
            Field var9 = var7[var5];
            if ((var9.getModifiers() & 24) == 0) {
                var1.add(var9);
            }
        }

        return var1;
    }

    public static String Z(Class var0) {
        StringBuffer var1 = new StringBuffer();
        String var2 = X(var0);
        var1.append(var2);
        List var3 = Y(var0);
        Iterator var5 = var3.iterator();

        while (var5.hasNext()) {
            Field var4 = (Field) var5.next();
            var1.append(';');
            var1.append(var4.getName());
        }

        return var1.toString();
    }

    public static String aa(Class var0) {
        StringBuffer var1 = new StringBuffer();
        String var2 = W(var0);
        var1.append("CREATE TABLE ");
        var1.append(var2);
        var1.append(" (");
        List var3 = Y(var0);
        boolean var4 = true;
        Iterator var6 = var3.iterator();

        while (true) {
            while (var6.hasNext()) {
                Field var5 = (Field) var6.next();
                if (!var4) {
                    var1.append(", ");
                } else {
                    var4 = false;
                }

                var1.append(var5.getName());
                Class var7 = var5.getType();
                if (var7 == String.class) {
                    var1.append(" VARCHAR(256)");
                } else if (var7 == Float.class || var7.isPrimitive() && var7 == Float.TYPE) {
                    var1.append(" DECIMAL(16, 8)");
                } else if (var7 == Integer.class || var7.isPrimitive() && var7 == Integer.TYPE) {
                    var1.append(" INT");
                } else if (var7 != Long.class && (!var7.isPrimitive() || var7 != Long.TYPE)) {
                    if (var7 == Date.class) {
                        var1.append(" DATETIME");
                    } else {
                        var1.append(" <<<UNSUPPORTED TYPE!>>>");
                    }
                } else {
                    var1.append(" BIGINT");
                }
            }

            var1.append(");");
            return var1.toString();
        }
    }

    public static String ab(Class var0) {
        StringBuffer var1 = new StringBuffer();
        String var2 = W(var0);
        var1.append("DROP TABLE ");
        var1.append(var2);
        var1.append(";");
        return var1.toString();
    }

    public static List ac(Class var0) {
        ArrayList var1 = new ArrayList();
        String var2 = W(var0);
        var1.add(var2);
        List var3 = Y(var0);
        Iterator var5 = var3.iterator();

        while (var5.hasNext()) {
            Field var4 = (Field) var5.next();
            var1.add(var4.getName());
        }

        return var1;
    }

    private void writeObject(ObjectOutputStream var1) throws IOException {
        var1.writeObject(this.feF);
        var1.writeObject(this.feH);
        var1.writeLong(this.feG);
    }

    private void readObject(ObjectInputStream var1) throws IOException, ClassNotFoundException {
        this.feF = (Date) var1.readObject();
        this.feH = (String) var1.readObject();
        this.feG = var1.readLong();
    }

    /*
       protected acG_q(long var1, akU var3)
       {
          this.feF = new Date(var1);
          if (var3 != null) {
             this.feH = var3.getTegName();
             this.feG = ((Xf)var3).bFf().hC().cqo();
          } else {
             this.feH = null;
             this.feG = -1L;
          }

       }
    */
    public void gC(long var1) {
        this.feF = new Date(var1);
    }

    public String bPE() {
        String var1 = this.getClass().getSimpleName();
        if (var1.endsWith("Log")) {
            var1 = var1.substring(0, var1.length() - 3);
        }

        return var1;
    }

    private String encodeText(String var1) {
        StringBuilder var2 = new StringBuilder(34);

        for (int var3 = 0; var3 < var1.length(); ++var3) {
            char var4 = var1.charAt(var3);
            switch (var4) {
                case '"':
                    var2.append("\\\"");
                    break;
                case ';':
                    var2.append(',');
                    break;
                default:
                    var2.append(var4);
            }
        }

        return var2.toString();
    }

    private String aj(Object var1) {
        if (var1 == null) {
            return "";
        } else {
            String var2 = null;
            Class var3 = var1.getClass();
        /* if (var3 == akU.class)
         {
            var2 = ((akU)var1).getTegName();
         } else*/
            if (var3 == Date.class) {
                var2 = dBH.format((Date) var1);
            } else {
                var2 = var1.toString();
            }

            return this.encodeText(var2);
        }
    }

    public String bPF() {
        StringBuilder var1 = new StringBuilder();

        try {
            Class var2 = this.getClass();
            List var3 = Y(var2);
            var1.append(this.bPE());
            Iterator var5 = var3.iterator();

            while (var5.hasNext()) {
                Field var4 = (Field) var5.next();
                var4.setAccessible(true);
                var1.append(';');
                var1.append(this.encodeText(this.aj(var4.get(this))));
            }
        } catch (IllegalArgumentException var6) {
            System.out.println("Exception-" + var6);
        } catch (IllegalAccessException var7) {
            System.out.println("Exception-" + var7);
        }

        return var1.toString();
    }

    private String is(String var1) {
        String var2 = "";

        for (int var3 = 0; var3 < var1.length(); ++var3) {
            var2 = var2 + var1.charAt(var3);
            if (var1.charAt(var3) == '\'') {
                var2 = var2 + '\'';
            }
        }

        return var2;
    }

    public String bPG() throws IllegalAccessException {
        StringBuffer var1 = new StringBuffer();
        Class var2 = this.getClass();
        List var3 = Y(var2);
        String var4 = W(var2);
        var1.append("INSERT INTO ");
        var1.append(var4);
        var1.append(" (");
        boolean var5 = true;

        Field var6;
        Iterator var7;
        for (var7 = var3.iterator(); var7.hasNext(); var1.append(var6.getName())) {
            var6 = (Field) var7.next();
            if (!var5) {
                var1.append(", ");
            } else {
                var5 = false;
            }
        }

        var1.append(") values (");
        var5 = true;
        var7 = var3.iterator();

        while (var7.hasNext()) {
            var6 = (Field) var7.next();
            var6.setAccessible(true);
            if (!var5) {
                var1.append(", ");
            } else {
                var5 = false;
            }

            Object var8 = var6.get(this);
            if (var8 == null) {
                var1.append("null");
            } else if (var6.getType() == String.class) {
                var1.append("'");
                String var9 = (String) var8;
                if (var9.length() > 256) {
                    var9 = var9.substring(0, 256);
                }

                var1.append(this.is(var9));
                var1.append("'");
            } else if (var6.getType() == Date.class) {
                var1.append("'");
                var1.append(dBH.format(var8));
                var1.append("'");
            } else {
                var1.append(var8.toString());
            }
        }

        var1.append(");");
        return var1.toString();
    }

    public String bPH() throws IllegalAccessException {
        StringBuffer var1 = new StringBuffer();
        Class var2 = this.getClass();
        List var3 = Y(var2);
        String var4 = W(var2);
        var1.append("<" + var4 + ">");

        Field var5;
        for (Iterator var6 = var3.iterator(); var6.hasNext(); var1.append("</" + var5.getName() + ">")) {
            var5 = (Field) var6.next();
            var5.setAccessible(true);
            var1.append("<" + var5.getName() + ">");
            Object var7 = var5.get(this);
            if (var7 == null) {
                var1.append("NULL");
            } else if (var5.getType() == String.class) {
                var1.append("'");
                String var8 = (String) var7;
                if (var8.length() > 256) {
                    var8 = var8.substring(0, 256);
                }

                var1.append(this.is(var8));
                var1.append("'");
            } else if (var5.getType() == Date.class) {
                var1.append("'");
                var1.append(dBH.format(var7));
                var1.append("'");
            } else {
                var1.append(var7.toString());
            }
        }

        var1.append("</" + var4 + ">");
        return var1.toString();
    }

    public Date getTimestamp() {
        return this.feF;
    }
}
