package all;

import org.w3c.dom.css.Counter;

import java.io.Serializable;

public class akw implements Serializable, Counter {
    private String fTD;
    private String fTE;
    private String fTF;

    public akw(boolean var1, LexicalUnit var2) {
        this.fTD = var2.getStringValue();
        LexicalUnit var3 = var2.getNextLexicalUnit();
        if (var1 && var3 != null) {
            var3 = var3.getNextLexicalUnit();
            this.fTF = var3.getStringValue();
            var3 = var3.getNextLexicalUnit();
        }

        if (var3 != null) {
            this.fTE = var3.getStringValue();
            var3 = var3.getNextLexicalUnit();
        }

    }

    public String getIdentifier() {
        return this.fTD;
    }

    public String getListStyle() {
        return this.fTE;
    }

    public String getSeparator() {
        return this.fTF;
    }

    public String toString() {
        StringBuffer var1 = new StringBuffer();
        if (this.fTF == null) {
            var1.append("counter(");
        } else {
            var1.append("counters(");
        }

        var1.append(this.fTD);
        if (this.fTF != null) {
            var1.append(", \"").append(this.fTF).append("\"");
        }

        if (this.fTE != null) {
            var1.append(", ").append(this.fTE);
        }

        var1.append(")");
        return var1.toString();
    }
}
