package all;

public class aVS implements aqA {
    protected final Vector3dOperations center = new Vector3dOperations();
    protected double jcB;

    public Vector3dOperations zO() {
        return this.center;
    }

    public double dCJ() {
        return this.jcB;
    }

    public void h(Vector3dOperations var1) {
        this.center.aA(var1);
    }

    public void ag(double var1) {
        this.jcB = var1;
    }

    public void a(ajD var1, Vector3dOperations var2, Vector3dOperations var3) {
        double var4 = this.jcB;
        Vector3dOperations var6 = this.center;
        double var7 = (double) var1.m00 * var6.x + (double) var1.m01 * var6.y + (double) var1.m02 * var6.z;
        double var9 = (double) var1.m10 * var6.y + (double) var1.m11 * var6.y + (double) var1.m12 * var6.z;
        double var11 = (double) var1.m20 * var6.z + (double) var1.m21 * var6.y + (double) var1.m22 * var6.z;
        var2.x = var7 + var4;
        var2.y = var9 + var4;
        var2.z = var11 + var4;
        var3.x = var7 - var4;
        var3.y = var9 - var4;
        var3.z = var11 - var4;
    }

    public void H(double var1, double var3, double var5) {
        this.center.set(var1, var3, var5);
    }

    public void b(aVS var1) {
        this.center.aA(var1.center);
        this.jcB = var1.jcB;
    }
}
