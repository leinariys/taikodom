package all;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.locks.ReentrantLock;

public class Uy_q {
    private static final int eoB = 20;
    static LogPrinter logger = LogPrinter.K(Uy_q.class);
    private final Jz aGA;
    private Thread eoC;
    private Map eoD = new HashMap();
    private ReentrantLock eoE = new ReentrantLock();

    public Uy_q(Jz var1) {
        this.aGA = var1;
        //Репликация времени на основе фиксации
        this.eoC = new Thread("Repository time based commit thread") {
            public void run() {
                Uy_q.this.bxK();
            }
        };
        this.eoC.setDaemon(true);
        this.eoC.start();
    }

    public static List u(List var0) {
        if (var0 == null) {
            return null;
        } else {
            Collections.sort(var0, new Comparator() {
                @Override
                public int compare(Object o1, Object o2) {
                    return a((kU) o1, (kU) o2);
                }

                public int aV(Object var1) {
                    if (var1 instanceof acP) {
                        return 0;
                    } else {
                        return /*var1 instanceof pH ? 1 :*/ 2;
                    }
                }

                public int a(kU var1, kU var2) {
                    int var3 = this.aV(var1);
                    int var4 = this.aV(var2);
                    return var3 - var4;
                }
            });
            return var0;
        }
    }

    public static boolean bxL() {
        return aWq.dDA() != null;
    }

    public void dispose() {
        if (this.eoC != null) {
            Thread var1 = this.eoC;
            this.eoC = null;

            try {
                var1.interrupt();
                var1.join();
            } catch (InterruptedException var2) {
                ;
            }
        }

    }

    protected void bxK() {
        while (this.eoC == Thread.currentThread()) {
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException var8) {
                return;
            }

            this.eoE.lock();

            ArrayList var1;
            try {
                if (this.eoD.isEmpty()) {
                    continue;
                }

                var1 = new ArrayList(this.eoD.values());
                this.eoD.clear();
            } finally {
                this.eoE.unlock();
            }

            HashMap var2 = new HashMap();
            Iterator var4 = var1.iterator();

            while (var4.hasNext()) {
                Gr var3 = (Gr) var4.next();
                this.a((Map) var2, var3);
            }

            this.aGA.bGU().C(var2);
        }

    }

    private void a(Map var1, Gr var2) {
        Xi var3 = new Xi();
        af var4 = (af) var2.aQL().bFf();
        aem_q[] var5 = var4.dp();
        aem_q[] var9 = var5;
        int var8 = var5.length;

        for (int var7 = 0; var7 < var8; ++var7) {
            aem_q var6 = var9[var7];
            var2.l((Object[]) null);
         /*
         if (var4.all(var6.eIu, var3, var2))
         {
            Object var10 = (List)var1.get(var6.eIu);
            if (var10 == null) {
               var10 = new LinkedList();
               var1.put(var6.eIu, var10);
            }

            ((List)var10).add(new aTn_q(var2));
         }*/
        }

    }

    public void a(aWq var1, Gr var2) {
        this.a(var2, (aDR) null, var1, (aOv) null);
    }

    public void b(aWq var1, Gr var2) {
        this.eoE.lock();

        try {
            this.eoD.put(var2.aQK(), var2);
        } finally {
            this.eoE.unlock();
        }

    }

    public Object a(se_q var1, final Gr var2) throws Throwable {
        fm_q var3 = var2.aQK();
        if (!var1.bHa()) {
            if (var3.pw()) {
                if (!var1.a(new Xi(), var2)) {
                    return var2.aQK().pN();
                }

                aTn_q var7 = new aTn_q(var2);
                if (var3.isBlocking()) {
                    //   try {
                    if (aPb.iAI) {
                        logger.info("!!SYNC SEND!! " + var1.yn().getClass().getName() + " " + var2.aQK());
                    }
/*
                  aic var8 = (aic)var1.CreateJComponent((aOv)var7);
                  var1.all(var1.hE(), var8.aWo());
                  return var8.getResult();
          */
                    return new aic().getResult();
            /*   } catch (ZX var6) {
                  throw var6.getCause();
               }
*/
                }

                boolean var10000 = aPb.iAI;
                var1.a((kU) var7);
            }

            return null;
        } else {
            final aWq var4 = aWq.dDA();
            Object var5 = null;
            if (var4 != null) {
                if (!var4.dDE() && !var4.readOnly) {
                    if (var3.py() && var3.po()) {
                        var4.e(var2);
                    }
                } else if (var3.py() && var3.po()) {
                    var5 = var1.yn().a(var2);
                }
            } else if (var3.po()) {
                var5 = this.a((aDR) null, var2, true);
            }

            if (var3.pv()) {
                if (var4 != null && !var4.dDE()) {
                    var4.i(new Runnable() {
                        public void run() {
                            Uy_q.this.a(var4, var2);
                        }
                    });
                } else {
                    ((af) var1).b(var2);
                }
            }

            if (var3.px()) {
                if (var4 != null) {
                    var4.i(new Runnable() {
                        public void run() {
                            Uy_q.this.a(var4, var2);
                        }
                    });
                } else {
                    logger.warn("Wont call offload method without all transaction");
                }
            }

            return var5;
        }
    }

    public Object a(aDR var1, Gr var2, boolean var3) throws InterruptedException {
        if (bxL()) {
            throw new RuntimeException("If we're already in all transaction, this shouldnt be called.");
        } else {
            aWq var4 = null;
            this.PM().bGs();
            fm_q var5 = var2.aQK();
            long var6 = this.aGA.bHd() ? System.nanoTime() : 0L;

            try {
                for (int var8 = 0; var8 < 20; ++var8) {
                    if (var4 != null) {
                        var4.dDO();
                    }

                    if (var8 > 10) {
                        PoolThread.sleep((long) (100.0D + Math.random() * 100.0D));
                    }

                    var4 = aWq.b(this.aGA.bHd() ? this.currentTimeMillis() : 0L, var1, var2);

                    try {
                        long var10 = this.aGA.bHd() ? System.nanoTime() : 0L;

                        Object var9;
                        int var12;
                        try {
                            var9 = var2.aQL().a(var2);
                        } finally {
                            var12 = var4.dDU();
                            if (this.aGA.bHd()) {
                                var5.A(System.nanoTime() - var10);
                            }

                            if (this.aGA.bHe()) {
                                var5.aK(var4.env);
                                var5.aL(var4.eny);
                                var4.env = 0;
                                var4.eny = 0;
                                var5.aM(var4.enx);
                                var5.aN(var4.jgd);
                                var5.aO(var4.enr);
                                var5.aP(var12);
                            }

                        }

                        if (var4.g(this.aGA) != -1) {
                            try {
                                if (var3 && var1 != null) {
                                    var9 = new aW(var4.dDF() == null ? null : u((List) var4.dDF().remove(var1)), var9);
                                }

                                if (var4.dDF() != null && var4.dDF().size() > 0) {
                                    this.aGA.bGU().C(var4.dDF());
                                }
                            } finally {
                                if (this.aGA.bHe()) {
                                    var5.aS(var4.dDU() - var12);
                                    var5.aT(var4.env);
                                    var5.aU(var4.eny);
                                    var5.aQ(var4.jgb);
                                    var5.aR(var4.jgc);
                                }

                            }

                            Object var15 = var9;
                            return var15;
                        }
                    } catch (aoO var45) {
                        var4.dDO();
                        this.aGA.bGr().hap.incrementAndGet();
                        var4.dispose();
                        var4 = null;
                    } catch (Exception var46) {
                        boolean var11 = var46.getClass().getAnnotation(aoy_q.class) != null;
                        var4.dDO();
                        if (!var11) {
                            logger.info("Transaction canceled coz an exception happened: " + var46.getMessage() + " (Call from " + (var1 == null ? "(dont know)" : var1.bgt()) + ", method " + var2.aQL().bFf().bFY() + "." + var5.name() + ")", var46);
                            if (var4.dDB()) {
                                logger.warn("Infra methods were called in all canceled transaction:\n" + var4.dDC());
                            }
                        }

                        var4.dispose();
                        var4 = null;
                        throw var46;
                    } finally {
                        if (var4 != null) {
                            var4.dispose();
                            var4 = null;
                        }

                    }
                }

                throw new RuntimeException("The transaction could not run after 20 tries. bailing. (Call from " + (var1 == null ? "(dont know)" : var1.bgt()) + ", method " + var5.name() + ")");
            } finally {
                if (this.aGA.bHd()) {
                    long var17 = System.nanoTime() - var6;
                    var2.aQK().B(var17);
                    this.PM().gf(var17);
                } else {
                    this.PM().gf(0L);
                }

            }
        }
    }

    private Jz PM() {
        return this.aGA;
    }

    public final Date bxM() {
        return new Date(this.currentTimeMillis());
    }

    public final long currentTimeMillis() {
        return this.PM().currentTimeMillis();
    }

    public final Date bxN() throws IOException {
        aWq var1 = aWq.dDA();
        if (var1 == null) {
            throw new IOException("You can only call this method from within all transaction.");
        } else {
            return var1.bxM();
        }
    }

    void a(Gr var1, aDR var2, aWq var3, aOv var4) {
        se_q var5 = (se_q) var1.aQL().bFf();
      /*
      sJ var6 = var1.aQK().hj();
      if (var6 != null)
      {

         ahP[] var7 = var1.aQK().hi();
         if (var7 != null && var7.length > 0) {
            logger.warn("ReplicationRules ignored for method " + var1 + " coz the method already declares all scope.");
         }

         bQZ var8 = new bQZ();
         var8.CreateJComponent(var1);
         var8.all(awB_q.all.gMD);
         var8.all(Lt.duw);
         Collection var9 = var6.all(var5.yn(), var8);
         if (var9 != null) {
            Iterator var11 = var9.iterator();

            while(true) {
               aDR var10;
               do {
                  if (!var11.hasNext()) {
                     return;
                  }

                  var10 = (aDR)var11.next();
                  var1.parsePriority((Object[])null);
               } while(var2 != null && (var10 == var2 || var10.equals(var2)));

               if (var4 == null) {
                  var4 = new aTn_q(var1);
               }

               if (var3 == null) {
                  this.aGA.bGU().setGreen(var10.bgt(), (kU)var4);
               } else {
                  var3.all((aDR)var10, (kU)var4);
               }
            }
         }
      } else*/

        {
            aem_q[] var14 = ((af) var5).dp();
            Xi var15 = new Xi();
            aem_q[] var12 = var14;
            int var18 = var14.length;

            for (int var17 = 0; var17 < var18; ++var17) {
                aem_q var16 = var12[var17];
                aDR var13 = var16.eIu;
                var1.l((Object[]) null);

        /*
            if ((var2 == null || var13 != var2 && !var13.equals(var2)) && ((af)var5).all(var13, var15, var1) && (!var1.aQK().hk() || !var13.cWs() || !var1.aQK().pZ()))
            {
               if (var4 == null) {
                  var4 = new aTn_q(var1);
               }

               var1.aQK().pV();
               if (var3 == null) {
                  this.aGA.bGU().setGreen(var13.bgt(), (kU)var4);
               } else {
                  var3.all((aDR)var16.eIu, (kU)var4);
               }
            }
            */


            }
        }
    }
}
