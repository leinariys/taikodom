package all;

public class wv {
    public static int bDt = 1;
    public static int bDu = 2;
    public static int bDv = 3;
    public static int bDw = 4;
    public static int bDx = 5;
    public static int bDy = 6;
    public static int bDz = 7;
    public static int bDA = 8;
    public static int bDB = 9;
    public static int bDC = 20;
    public static int bDD = 10;
    public static int bDE = 11;
    public static int bDF = 12;
    public static int bDG = 13;
    public static int bDH = 14;
    private int bDq;
    private String msg;
    private float bDr;
    private float bDs;

    public wv(int var1) {
        this(var1, (String) null, 0.0F, 0.0F);
    }

    public wv(int var1, String var2) {
        this(var1, var2, 0.0F, 0.0F);
    }

    public wv(int var1, String var2, float var3, float var4) {
        this.bDq = var1;
        this.msg = var2;
        this.bDr = var3;
        this.bDs = var4;
    }

    public int amy() {
        return this.bDq;
    }

    public String amz() {
        return this.msg;
    }

    public float amA() {
        return this.bDr;
    }

    public float amB() {
        return this.bDs;
    }
}
