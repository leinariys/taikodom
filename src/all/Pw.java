package all;

import com.hoplon.geometry.Vec3f;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

public class Pw {
    private static final boolean hbI = false;
    public static int hbM = 2048;
    public static int hbN = 10;
    private static int hbJ = 0;
    private static int hbK = 0;
    private static int hbL = 0;
    protected final List hbY = new ArrayList();
    private final List hbO = new ArrayList();
    private final List hbP = new ArrayList();
    private final Vec3f hbU = new Vec3f();
    private final Vec3f hbV = new Vec3f();
    private final Vec3f hbW = new Vec3f();
    protected Kt stack = Kt.bcE();
    protected Vk_q hbX;
    protected int hbZ;
    private aEr hbQ = new aEr();
    private aEr hbR = new aEr();
    private int hbS;
    private boolean hbT;
    private aUv BD = new aUv();

    public void c(int var1, Vec3f var2) {
        if (this.hbT) {
            this.hbR.i(var1, this.bd(var2));
        } else {
            ((Po) this.hbP.get(var1)).dPX.set(var2);
        }

    }

    public void d(int var1, Vec3f var2) {
        if (this.hbT) {
            this.hbR.j(var1, this.bd(var2));
        } else {
            ((Po) this.hbP.get(var1)).dPY.set(var2);
        }

    }

    public Vec3f wH(int var1) {
        if (this.hbT) {
            Vec3f var2 = new Vec3f();
            this.a(var2, this.hbQ.xE(var1));
            return var2;
        } else {
            return ((Po) this.hbO.get(var1)).dPX;
        }
    }

    public Vec3f wI(int var1) {
        if (this.hbT) {
            Vec3f var2 = new Vec3f();
            this.a(var2, this.hbQ.xF(var1));
            return var2;
        } else {
            return ((Po) this.hbO.get(var1)).dPY;
        }
    }

    public void I(Vec3f var1, Vec3f var2) {
        this.e(var1, var2, 1.0F);
    }

    public void e(Vec3f var1, Vec3f var2, float var3) {
        this.stack = this.stack.bcD();
        this.stack.bcH().push();

        try {
            Vec3f var4 = this.stack.bcH().h(var3, var3, var3);
            this.hbU.sub(var1, var4);
            this.hbV.add(var2, var4);
            Vec3f var5 = (Vec3f) this.stack.bcH().get();
            var5.sub(this.hbV, this.hbU);
            this.hbW.set(65535.0F, 65535.0F, 65535.0F);
            JL.h(this.hbW, this.hbW, var5);
        } finally {
            this.stack.bcH().pop();
        }

    }

    public void ao(int var1, int var2) {
        if (this.hbT) {
            this.hbR.aB(var1, -var2);
        } else {
            ((Po) this.hbP.get(var1)).dPZ = var2;
        }

    }

    public void c(int var1, Vec3f var2, Vec3f var3) {
        if (this.hbT) {
            long var4 = this.bd(var2);
            long var6 = this.bd(var3);

            for (int var8 = 0; var8 < 3; ++var8) {
                if (this.hbR.az(var1, var8) > aEr.i(var4, var8)) {
                    this.hbR.z(var1, var8, aEr.i(var4, var8));
                }

                if (this.hbR.aA(var1, var8) < aEr.i(var6, var8)) {
                    this.hbR.A(var1, var8, aEr.i(var6, var8));
                }
            }
        } else {
            JL.o(((Po) this.hbP.get(var1)).dPX, var2);
            JL.p(((Po) this.hbP.get(var1)).dPY, var3);
        }

    }

    public void ap(int var1, int var2) {
        if (this.hbT) {
            this.hbQ.swap(var1, var2);
        } else {
            Po var3 = (Po) this.hbO.get(var1);
            this.hbO.set(var1, (Po) this.hbO.get(var2));
            this.hbO.set(var2, var3);
        }

    }

    public void aq(int var1, int var2) {
        if (this.hbT) {
            this.hbR.a(var1, this.hbQ, var2);
        } else {
            ((Po) this.hbP.get(var1)).a((Po) this.hbO.get(var2));
        }

    }

    public void a(adJ var1, boolean var2, Vec3f var3, Vec3f var4) {
        this.stack = this.stack.bcD();
        this.stack.bcH().push();

        try {
            this.hbT = var2;
            boolean var5 = false;
            int var12;
            if (this.hbT) {
                this.I(var3, var4);
                b var6 = new b(this.hbQ, this);
                var1.a(var6, this.hbU, this.hbV);
                var12 = this.hbQ.size();
                this.hbR.resize(2 * var12);
            } else {
                a var13 = new a(this.hbO);
                Vec3f var7 = this.stack.bcH().h(-1.0E30F, -1.0E30F, -1.0E30F);
                Vec3f var8 = this.stack.bcH().h(1.0E30F, 1.0E30F, 1.0E30F);
                var1.a(var13, var7, var8);
                var12 = this.hbO.size();
                tt.a(this.hbP, 2 * var12, Po.class);
            }

            this.hbS = 0;
            this.ar(0, var12);
            if (this.hbT && this.hbY.size() == 0) {
                aVM var14 = new aVM();
                this.hbY.add(var14);
                var14.a(this.hbR, 0);
                var14.jbF = 0;
                var14.jbG = this.hbR.xH(0) ? 1 : this.hbR.xI(0);
            }

            this.hbZ = this.hbY.size();
            this.hbQ.clear();
            this.hbO.clear();
        } finally {
            this.stack.bcH().pop();
        }

    }

    public void a(adJ var1) {
        this.stack = this.stack.bcD();
        this.stack.bcH().push();

        try {
            if (this.hbT) {
                Vec3f var2 = (Vec3f) this.stack.bcH().get();
                Vec3f var3 = (Vec3f) this.stack.bcH().get();
                var1.x(var2, var3);
                this.I(var2, var3);
                this.a(var1, 0, this.hbS, 0);

                for (int var4 = 0; var4 < this.hbY.size(); ++var4) {
                    aVM var5 = (aVM) this.hbY.get(var4);
                    var5.a(this.hbR, var5.jbF);
                }
            } else {
                this.a(var1, false, (Vec3f) null, (Vec3f) null);
            }
        } finally {
            this.stack.bcH().pop();
        }

    }

    public void a(adJ var1, Vec3f var2, Vec3f var3) {
        throw new UnsupportedOperationException();
    }

    public void a(adJ var1, int var2, int var3, int var4) {
        assert this.hbT;

        this.stack = this.stack.bcD();
        this.stack.bcH().push();

        try {
            byte var5 = -1;
            Vec3f[] var6 = new Vec3f[]{(Vec3f) this.stack.bcH().get(), (Vec3f) this.stack.bcH().get(), (Vec3f) this.stack.bcH().get()};
            Vec3f var7 = (Vec3f) this.stack.bcH().get();
            Vec3f var8 = (Vec3f) this.stack.bcH().get();
            Vec3f var9 = var1.getScaling();

            for (int var10 = var3 - 1; var10 >= var2; --var10) {
                aEr var11 = this.hbR;
                int var12 = var10;
                int var13;
                int var14;
                if (!var11.xH(var10)) {
                    var13 = var10 + 1;
                    var14 = this.hbR.xH(var13) ? var10 + 2 : var10 + 1 + this.hbR.xI(var13);

                    for (int var24 = 0; var24 < 3; ++var24) {
                        var11.z(var12, var24, this.hbR.az(var13, var24));
                        if (var11.az(var12, var24) > this.hbR.az(var14, var24)) {
                            var11.z(var12, var24, this.hbR.az(var14, var24));
                        }

                        var11.A(var12, var24, this.hbR.aA(var13, var24));
                        if (var11.aA(var12, var24) < this.hbR.aA(var14, var24)) {
                            var11.A(var12, var24, this.hbR.aA(var14, var24));
                        }
                    }
                } else {
                    var13 = var11.xK(var10);
                    var14 = var11.xJ(var10);
                    if (var13 != var5) {
                        if (var5 >= 0) {
                            var1.qh(var5);
                        }

                        var1.b(this.BD, var13);

                        assert this.BD.iXo == aop.ghD || this.BD.iXo == aop.ghE;
                    }

                    ByteBuffer var15 = this.BD.iXl;
                    int var16 = var14 * this.BD.iXm;

                    for (int var17 = 2; var17 >= 0; --var17) {
                        int var18;
                        if (this.BD.iXo == aop.ghE) {
                            var18 = var15.getShort(var16 + var17 * 2) & '\uffff';
                        } else {
                            var18 = var15.getInt(var16 + var17 * 4);
                        }

                        ByteBuffer var19 = this.BD.iXi;
                        int var20 = var18 * this.BD.stride;
                        var6[var17].set(var19.getFloat(var20 + 0) * var9.x, var19.getFloat(var20 + 4) * var9.y, var19.getFloat(var20 + 8) * var9.z);
                    }

                    var7.set(1.0E30F, 1.0E30F, 1.0E30F);
                    var8.set(-1.0E30F, -1.0E30F, -1.0E30F);
                    JL.o(var7, var6[0]);
                    JL.p(var8, var6[0]);
                    JL.o(var7, var6[1]);
                    JL.p(var8, var6[1]);
                    JL.o(var7, var6[2]);
                    JL.p(var8, var6[2]);
                    var11.i(var10, this.bd(var7));
                    var11.j(var10, this.bd(var8));
                }
            }

            if (var5 >= 0) {
                var1.qh(var5);
            }

            this.BD.dAi();
        } finally {
            this.stack.bcH().pop();
        }

    }

    protected void ar(int var1, int var2) {
        this.stack = this.stack.bcD();
        this.stack.bcH().push();

        try {
            int var6 = var2 - var1;
            int var7 = this.hbS;

            assert var6 > 0;

            if (var6 == 1) {
                this.aq(this.hbS, var1);
                ++this.hbS;
                return;
            }

            int var3 = this.at(var1, var2);
            int var4 = this.y(var1, var2, var3);
            int var8 = this.hbS;
            this.d(this.hbS, this.stack.bcH().h(-1.0E30F, -1.0E30F, -1.0E30F));
            this.c(this.hbS, this.stack.bcH().h(1.0E30F, 1.0E30F, 1.0E30F));

            for (int var5 = var1; var5 < var2; ++var5) {
                this.c(this.hbS, this.wH(var5), this.wI(var5));
            }

            ++this.hbS;
            int var9 = this.hbS;
            this.ar(var1, var4);
            int var10 = this.hbS;
            this.ar(var4, var2);
            int var11 = this.hbS - var7;
            if (this.hbT) {
                int var12 = aEr.cXz();
                int var13 = var11 * var12;
                if (var13 > 2048) {
                    this.as(var9, var10);
                }
            }

            this.ao(var8, var11);
        } finally {
            this.stack.bcH().pop();
        }

    }

    protected boolean a(long var1, long var3, long var5, long var7) {
        int var9 = aEr.i(var1, 0);
        int var10 = aEr.i(var1, 1);
        int var11 = aEr.i(var1, 2);
        int var12 = aEr.i(var3, 0);
        int var13 = aEr.i(var3, 1);
        int var14 = aEr.i(var3, 2);
        int var15 = aEr.i(var5, 0);
        int var16 = aEr.i(var5, 1);
        int var17 = aEr.i(var5, 2);
        int var18 = aEr.i(var7, 0);
        int var19 = aEr.i(var7, 1);
        int var20 = aEr.i(var7, 2);
        boolean var21 = true;
        var21 = var9 <= var18 && var12 >= var15 ? var21 : false;
        var21 = var11 <= var20 && var14 >= var17 ? var21 : false;
        var21 = var10 <= var19 && var13 >= var16 ? var21 : false;
        return var21;
    }

    protected void as(int var1, int var2) {
        assert this.hbT;

        int var3 = this.hbR.xH(var1) ? 1 : this.hbR.xI(var1);
        int var4 = var3 * aEr.cXz();
        int var5 = this.hbR.xH(var2) ? 1 : this.hbR.xI(var2);
        int var6 = var5 * aEr.cXz();
        aVM var7;
        if (var4 <= 2048) {
            var7 = new aVM();
            this.hbY.add(var7);
            var7.a(this.hbR, var1);
            var7.jbF = var1;
            var7.jbG = var3;
        }

        if (var6 <= 2048) {
            var7 = new aVM();
            this.hbY.add(var7);
            var7.a(this.hbR, var2);
            var7.jbF = var2;
            var7.jbG = var5;
        }

        this.hbZ = this.hbY.size();
    }

    protected int y(int var1, int var2, int var3) {
        this.stack = this.stack.bcD();
        this.stack.bcH().push();

        int var14;
        try {
            int var5 = var1;
            int var6 = var2 - var1;
            Vec3f var8 = this.stack.bcH().h(0.0F, 0.0F, 0.0F);
            Vec3f var9 = (Vec3f) this.stack.bcH().get();

            int var4;
            for (var4 = var1; var4 < var2; ++var4) {
                var9.add(this.wI(var4), this.wH(var4));
                var9.scale(0.5F);
                var8.add(var9);
            }

            var8.scale(1.0F / (float) var6);
            float var7 = JL.b(var8, var3);

            for (var4 = var1; var4 < var2; ++var4) {
                var9.add(this.wI(var4), this.wH(var4));
                var9.scale(0.5F);
                if (JL.b(var9, var3) > var7) {
                    this.ap(var4, var5);
                    ++var5;
                }
            }

            int var10 = var6 / 3;
            boolean var11 = var5 <= var1 + var10 || var5 >= var2 - 1 - var10;
            if (var11) {
                var5 = var1 + (var6 >> 1);
            }

            boolean var12 = var5 == var1 || var5 == var2;

            assert !var12;

            var14 = var5;
        } finally {
            this.stack.bcH().pop();
        }

        return var14;
    }

    protected int at(int var1, int var2) {
        this.stack.bcH().push();

        try {
            Vec3f var4 = this.stack.bcH().h(0.0F, 0.0F, 0.0F);
            Vec3f var5 = this.stack.bcH().h(0.0F, 0.0F, 0.0F);
            int var6 = var2 - var1;
            Vec3f var7 = (Vec3f) this.stack.bcH().get();

            int var3;
            for (var3 = var1; var3 < var2; ++var3) {
                var7.add(this.wI(var3), this.wH(var3));
                var7.scale(0.5F);
                var4.add(var7);
            }

            var4.scale(1.0F / (float) var6);
            Vec3f var8 = (Vec3f) this.stack.bcH().get();

            for (var3 = var1; var3 < var2; ++var3) {
                var7.add(this.wI(var3), this.wH(var3));
                var7.scale(0.5F);
                var8.sub(var7, var4);
                JL.g(var8, var8, var8);
                var5.add(var8);
            }

            var5.scale(1.0F / ((float) var6 - 1.0F));
            int var10 = JL.W(var5);
            return var10;
        } finally {
            this.stack.bcH().pop();
        }
    }

    public void a(aLG var1, Vec3f var2, Vec3f var3) {
        if (this.hbT) {
            long var4 = this.bd(var2);
            long var6 = this.bd(var3);
            this.a(this.hbR, 0, var1, var4, var6);
        } else {
            this.b(var1, var2, var3);
        }

    }

    protected void b(aLG var1, Vec3f var2, Vec3f var3) {
        assert !this.hbT;

        Po var4 = null;
        int var5 = 0;
        int var7 = 0;
        int var8 = 0;

        while (true) {
            while (var7 < this.hbS) {
                assert var8 < this.hbS;

                ++var8;
                var4 = (Po) this.hbP.get(var5);
                boolean var10 = aRO.j(var2, var3, var4.dPX, var4.dPY);
                boolean var9 = var4.dPZ == -1;
                if (var9 && var10) {
                    var1.f(var4.dQa, var4.dQb);
                }

                var4 = null;
                if (!var10 && !var9) {
                    int var6 = ((Po) this.hbP.get(var5)).dPZ;
                    var5 += var6;
                    var7 += var6;
                } else {
                    ++var5;
                    ++var7;
                }
            }

            if (hbL < var8) {
                hbL = var8;
            }

            return;
        }
    }

    protected void a(aEr var1, int var2, aLG var3, long var4, long var6) {
        assert this.hbT;

        boolean var9 = this.a(var4, var6, var1.xE(var2), var1.xF(var2));
        boolean var8 = var1.xH(var2);
        if (var9) {
            if (var8) {
                var3.f(var1.xK(var2), var1.xJ(var2));
            } else {
                int var10 = var2 + 1;
                this.a(var1, var10, var3, var4, var6);
                int var11 = var1.xH(var10) ? var10 + 1 : var10 + var1.xI(var10);
                this.a(var1, var11, var3, var4, var6);
            }
        }

    }

    public void c(aLG var1, Vec3f var2, Vec3f var3) {
        this.stack = this.stack.bcD();
        this.stack.bcH().push();

        try {
            boolean var4 = this.hbT && this.hbX == Vk_q.esn;
            if (var4) {
                throw new UnsupportedOperationException();
            }

            Vec3f var5 = this.stack.bcH().ac(var2);
            Vec3f var6 = this.stack.bcH().ac(var2);
            JL.o(var5, var3);
            JL.p(var6, var3);
            this.a(var1, var5, var6);
        } finally {
            this.stack.bcH().pop();
        }

    }

    public void a(aLG var1, Vec3f var2, Vec3f var3, Vec3f var4, Vec3f var5) {
        this.stack = this.stack.bcD();
        this.stack.bcH().push();

        try {
            boolean var6 = this.hbT && this.hbX == Vk_q.esn;
            if (var6) {
                throw new UnsupportedOperationException();
            }

            Vec3f var7 = this.stack.bcH().ac(var2);
            Vec3f var8 = this.stack.bcH().ac(var2);
            JL.o(var7, var3);
            JL.p(var8, var3);
            var7.add(var4);
            var8.add(var5);
            this.a(var1, var7, var8);
        } finally {
            this.stack.bcH().pop();
        }

    }

    public long bd(Vec3f var1) {
        assert this.hbT;

        this.stack = this.stack.bcD();
        this.stack.bcH().push();

        long var8;
        try {
            Vec3f var2 = this.stack.bcH().ac(var1);
            JL.p(var2, this.hbU);
            JL.o(var2, this.hbV);
            Vec3f var3 = (Vec3f) this.stack.bcH().get();
            var3.sub(var2, this.hbU);
            JL.g(var3, var3, this.hbW);
            int var4 = (int) (var3.x + 0.5F) & '\uffff';
            int var5 = (int) (var3.y + 0.5F) & '\uffff';
            int var6 = (int) (var3.z + 0.5F) & '\uffff';
            var8 = (long) var4 | (long) var5 << 16 | (long) var6 << 32;
        } finally {
            this.stack.bcH().pop();
        }

        return var8;
    }

    public void a(Vec3f var1, long var2) {
        int var4 = (int) (var2 & 65535L);
        int var5 = (int) ((var2 & 4294901760L) >>> 16);
        int var6 = (int) ((var2 & 281470681743360L) >>> 32);
        var1.x = (float) var4 / this.hbW.x;
        var1.y = (float) var5 / this.hbW.y;
        var1.z = (float) var6 / this.hbW.z;
        var1.add(this.hbU);
    }

    private static class a implements SP {
        private final Vec3f cam = new Vec3f();
        private final Vec3f can = new Vec3f();
        public List dQC;

        public a(List var1) {
            this.dQC = var1;
        }

        public void b(Vec3f[] var1, int var2, int var3) {
            Po var4 = new Po();
            this.cam.set(1.0E30F, 1.0E30F, 1.0E30F);
            this.can.set(-1.0E30F, -1.0E30F, -1.0E30F);
            JL.o(this.cam, var1[0]);
            JL.p(this.can, var1[0]);
            JL.o(this.cam, var1[1]);
            JL.p(this.can, var1[1]);
            JL.o(this.cam, var1[2]);
            JL.p(this.can, var1[2]);
            var4.dPX.set(this.cam);
            var4.dPY.set(this.can);
            var4.dPZ = -1;
            var4.dQa = var2;
            var4.dQb = var3;
            this.dQC.add(var4);
        }
    }

    private static class b implements SP {
        public aEr hDg;
        public Pw hDh;
        protected Kt stack = Kt.bcE();

        public b(aEr var1, Pw var2) {
            this.hDg = var1;
            this.hDh = var2;
        }

        public void b(Vec3f[] var1, int var2, int var3) {
            assert var2 < 1024;

            assert var3 < 2097152;

            assert var3 >= 0;

            this.stack = this.stack.bcD();
            this.stack.bcH().push();

            try {
                int var4 = this.hDg.cXy();
                Vec3f var5 = (Vec3f) this.stack.bcH().get();
                Vec3f var6 = (Vec3f) this.stack.bcH().get();
                var5.set(1.0E30F, 1.0E30F, 1.0E30F);
                var6.set(-1.0E30F, -1.0E30F, -1.0E30F);
                JL.o(var5, var1[0]);
                JL.p(var6, var1[0]);
                JL.o(var5, var1[1]);
                JL.p(var6, var1[1]);
                JL.o(var5, var1[2]);
                JL.p(var6, var1[2]);
                if (var6.x - var5.x < 0.002F) {
                    var6.x += 0.001F;
                    var5.x -= 0.001F;
                }

                if (var6.y - var5.y < 0.002F) {
                    var6.y += 0.001F;
                    var5.y -= 0.001F;
                }

                if (var6.z - var5.z < 0.002F) {
                    var6.z += 0.001F;
                    var5.z -= 0.001F;
                }

                this.hDg.i(var4, this.hDh.bd(var5));
                this.hDg.j(var4, this.hDh.bd(var6));
                this.hDg.aB(var4, var2 << 21 | var3);
            } finally {
                this.stack.bcH().pop();
            }

        }
    }
}
