package all;


public class ThreadWrapper extends Thread {
    private Object context;

    public ThreadWrapper(Runnable var1) {
        super(var1);
    }

    public ThreadWrapper(Runnable var1, String var2) {
        super(var1, var2);
    }

    public ThreadWrapper(String var1) {
        super(var1);
    }

    public Object getContext() {
        return this.context;
    }

    public void setContext(Object var1) {
        this.context = var1;
    }
}
