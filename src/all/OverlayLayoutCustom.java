package all;

import javax.swing.*;
import java.awt.*;

public class OverlayLayoutCustom extends BaseLayoutCssValue {
    public LayoutManager createLayout(Container var1, String var2) {
        return new OverlayLayout(var1);
    }
}
