package all;

public class Ta {
    static LogPrinter logger = LogPrinter.K(Ta.class);
    private final String name;
    private long egN;
    private int counter;

    public Ta() {
        this((String) null);
    }

    public Ta(String var1) {
        this.name = var1;
        this.reset();
    }

    public void reset() {
        this.egN = System.currentTimeMillis();
        this.counter = 0;
        logger.info("Checkpoint " + (this.name != null ? this.name : "") + "#0");
    }

    public void buE() {
        long var1 = System.currentTimeMillis() - this.egN;
        this.egN += var1;
        logger.info("Checkpoint " + (this.name != null ? this.name : "") + "#" + ++this.counter + " reached after " + var1 + " millis");
    }
}
