package all;

public class ConditionFactory implements IConditionFactory {
    public xr a(Condition var1, Condition var2) {
        return new To(var1, var2);
    }

    public xr b(Condition var1, Condition var2) {
        throw new CSSException(CSSException.SAC_NOT_SUPPORTED_ERR);
    }

    public on a(Condition var1) {
        throw new CSSException(CSSException.SAC_NOT_SUPPORTED_ERR);
    }

    public XQ_q a(int var1, boolean var2, boolean var3) {
        throw new CSSException(CSSException.SAC_NOT_SUPPORTED_ERR);
    }

    public akt_q a(String var1, String var2, boolean var3, String var4) {
        return new aWn(var1, var4);
    }

    public akt_q eL(String var1) {
        return new fE(var1);
    }

    public aIb eM(String var1) {
        return new OU(var1);
    }

    public akt_q b(String var1, String var2, boolean var3, String var4) {
        return new awC(var1, var4);
    }

    public akt_q c(String var1, String var2, boolean var3, String var4) {
        return new awu(var1, var4);
    }

    public akt_q D(String var1, String var2) {
        return new rF(var2);
    }

    public akt_q E(String var1, String var2) {
        return new UC(var2);
    }

    public Condition bif() {
        throw new CSSException(CSSException.SAC_NOT_SUPPORTED_ERR);
    }

    public Condition big() {
        throw new CSSException(CSSException.SAC_NOT_SUPPORTED_ERR);
    }

    public Sl eN(String var1) {
        throw new CSSException(CSSException.SAC_NOT_SUPPORTED_ERR);
    }
}
