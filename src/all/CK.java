package all;

import java.io.PrintWriter;
import java.lang.Thread.State;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeSet;

public class CK {
    private final aeZ cAa = new aeZ((Md) null);
    private final Thread thread;
    private final Md cAb = new Md();
    public int cAd;
    public int[] cAe = new int[State.values().length];
    public int cAf;
    private Map cAc = new HashMap();

    public CK(Thread var1) {
        this.thread = var1;
    }

    public aeZ aDv() {
        return this.cAa;
    }

    public Thread getThread() {
        return this.thread;
    }

    public aBY a(Md var1) {
        return (aBY) this.cAc.get(var1);
    }

    public void a(StackTraceElement[] var1) {
        ++this.cAf;
        ++this.cAd;
        ++this.cAe[this.thread.getState().ordinal()];
        aeZ var2 = this.cAa;
        int var3 = var1.length;

        while (true) {
            --var3;
            if (var3 < 0) {
                return;
            }

            StackTraceElement var4 = var1[var3];
            this.cAb.a(var4);
            aBY var5 = this.a(this.cAb);
            Md var6;
            if (var5 == null) {
                var6 = new Md(var4);
                this.cAc.put(var6, var5 = new aBY(var6));
            }

            if (var5.hmH != this.cAf) {
                var5.hmH = this.cAf;
                var5.b(var4);
            }

            var6 = var5.bUY();
            Map var7 = var2.bUX();
            aeZ var8 = (aeZ) var7.get(var6);
            if (var8 == null) {
                var7.put(var6, var8 = new aeZ(var6));
            }

            var8.bUW();
            var2 = var8;
        }
    }

    public int aDw() {
        return this.cAd;
    }

    public void dump(PrintWriter var1) {
        var1.println(this.thread);

        for (int var2 = 0; var2 < this.cAe.length; ++var2) {
            if (this.cAe[var2] > 0) {
                var1.print(" " + (int) ((float) this.cAe[var2] * 100.0F / (float) this.cAd) + "% " + State.values()[var2]);
            }
        }

        var1.println();
        TreeSet var6 = new TreeSet(this.cAc.values());
        int var3 = 0;
        Iterator var5 = var6.iterator();

        while (var5.hasNext()) {
            aBY var4 = (aBY) var5.next();
            if (var4.cMQ() * 100L / (long) this.cAd > 1L) {
                ++var3;
                if (var3 < 100) {
                    var1.print(" *[");
                    var1.print(var4.cMQ());
                    var1.print(", ");
                    var1.print(var4.cMQ() * 100L / (long) this.cAd);
                    var1.print("%] ");
                    var1.println(var4.bUY().toString());
                }
            }
        }

        var1.println();
        this.cAa.a(var1, this.cAd, " ");
    }
}
