package all;

import java.io.EOFException;
import java.io.InputStream;
import java.io.UTFDataFormatException;

public class aMf extends InputStream //implements Ie  //TODO На удаление
{
    int ioy = 0;
    int fVi = 0;
    private byte[] buf;
    private int count;
    private int pos;
    private int mark = 0;
    private char[] ioz = null;

    private aMf() {
    }

    public aMf(byte[] var1) {
        this.buf = var1;
        this.pos = 0;
        this.count = var1.length;
    }

    public aMf(byte[] var1, int var2, int var3) {
        this.buf = var1;
        this.pos = var2;
        this.mark = var2;
        this.count = Math.min(var2 + var3, var1.length);
    }

    public int a(int var1) throws EOFException {
        int var2;
        for (var2 = 0; var1 > 0; this.fVi = 0) {
            if (this.fVi == 0) {
                this.ioy = this.readUnsignedByte();
                this.fVi = 8;
            }

            if (var1 <= this.fVi) {
                if (var1 == this.fVi) {
                    var2 |= this.ioy & ala.emM[var1];
                    this.fVi -= var1;
                    return var2;
                }

                var2 |= this.ioy >> this.fVi - var1 & ala.emM[var1];
                this.fVi -= var1;
                return var2;
            }

            var2 |= (this.ioy & ala.emM[this.fVi]) << var1 - this.fVi;
            var1 -= this.fVi;
        }

        return var2;
    }

    public int read() {
        return this.pos < this.count ? this.buf[this.pos++] & 255 : -1;
    }

    private void ensure(int var1) throws EOFException {
        if (var1 > this.count) {
            throw new EOFException();
        }
    }

    public int read(byte[] var1, int var2, int var3) {
        if (this.pos >= this.count) {
            return -1;
        } else {
            if (this.pos + var3 > this.count) {
                var3 = this.count - this.pos;
            }

            if (var3 <= 0) {
                return 0;
            } else {
                System.arraycopy(this.buf, this.pos, var1, var2, var3);
                this.pos += var3;
                return var3;
            }
        }
    }

    public long skip(long var1) {
        if ((long) this.pos + var1 > (long) this.count) {
            var1 = (long) (this.count - this.pos);
        }

        if (var1 < 0L) {
            return 0L;
        } else {
            this.pos = (int) ((long) this.pos + var1);
            return var1;
        }
    }

    public int available() {
        return this.count - this.pos;
    }

    public boolean markSupported() {
        return true;
    }

    public void mark(int var1) {
        this.mark = this.pos;
    }

    public void reset() {
        this.pos = this.mark;
        this.ioy = 0;
        this.fVi = 0;
    }

    public void seek(int var1) {
        this.pos = var1;
    }

    public aMf diU() {
        aMf var1 = new aMf();
        var1.buf = new byte[this.buf.length];
        System.arraycopy(this.buf, 0, var1.buf, 0, this.buf.length);
        var1.count = this.count;
        var1.pos = this.pos;
        var1.mark = this.mark;
        var1.ioy = this.ioy;
        var1.fVi = this.fVi;
        return var1;
    }

    public void close() {
    }

    public void readFully(byte[] var1) throws EOFException {
        this.readFully(var1, 0, var1.length);
    }

    public void readFully(byte[] var1, int var2, int var3) throws EOFException {
        int var4 = this.read(var1, var2, var3);
        if (var4 < 0) {
            throw new EOFException();
        }
    }

    public int skipBytes(int var1) {
        return (int) this.skip((long) var1);
    }

    public boolean readBoolean() throws EOFException {
        return this.a(1) != 0;
    }

    public byte readByte() throws EOFException {
        this.ensure(this.pos + 1);
        return this.buf[this.pos++];
    }

    public int readUnsignedByte() throws EOFException {
        this.ensure(this.pos + 1);
        return this.buf[this.pos++] & 255;
    }

    public short readShort() throws EOFException {
        if (this.a(1) == 0) {
            boolean var1 = this.a(1) == 1;
            short var2 = (short) (this.read() & 255);
            return var1 ? (short) (-var2) : var2;
        } else {
            return this.cjB();
        }
    }

    public short cjB() throws EOFException {
        this.ensure(this.pos + 2);
        int var1 = this.buf[this.pos++] & 255;
        int var2 = this.buf[this.pos++] & 255;
        return (short) ((var1 << 8) + (var2 << 0));
    }

    public int readUnsignedShort() throws EOFException {
        return this.readShort() & '\uffff';
    }

    public int cjC() throws EOFException {
        this.ensure(this.pos + 2);
        int var1 = this.buf[this.pos++] & 255;
        int var2 = this.buf[this.pos++] & 255;
        return (var1 << 8) + (var2 << 0);
    }

    public char readChar() throws EOFException {
        return this.a(1) == 1 ? (char) this.cjB() : (char) this.read();
    }

    public int readInt() throws EOFException {
        int var1 = this.a(2);
        int var2;
        boolean var3;
        switch (var1) {
            case 0:
                var3 = this.a(1) == 1;
                var2 = this.read() & 255;
                return var3 ? -var2 : var2;
            case 1:
                var3 = this.a(1) == 1;
                var2 = this.cjC();
                return var3 ? -var2 : var2;
            case 2:
                var3 = this.a(1) == 1;
                var2 = this.cjF();
                return var3 ? -var2 : var2;
            case 3:
            default:
                return this.cjE();
        }
    }

    public int cjD() throws EOFException {
        int var1;
        switch (this.a(2)) {
            case 0:
                var1 = this.read() & 255;
                break;
            case 1:
                var1 = this.cjC();
                break;
            case 2:
                var1 = this.cjF();
                break;
            default:
                var1 = this.cjE();
        }

        return var1;
    }

    public int cjE() throws EOFException {
        this.ensure(this.pos + 4);
        int var1 = this.buf[this.pos++] & 255;
        int var2 = this.buf[this.pos++] & 255;
        int var3 = this.buf[this.pos++] & 255;
        int var4 = this.buf[this.pos++] & 255;
        return (var1 << 24) + (var2 << 16) + (var3 << 8) + (var4 << 0);
    }

    public int cjF() throws EOFException {
        this.ensure(this.pos + 3);
        int var1 = this.buf[this.pos++] & 255;
        int var2 = this.buf[this.pos++] & 255;
        int var3 = this.buf[this.pos++] & 255;
        return (var1 << 16) + (var2 << 8) + (var3 << 0);
    }

    public long readLong() throws EOFException {
        long var1;
        boolean var3;
        switch (this.a(3)) {
            case 0:
                var3 = this.a(1) == 1;
                var1 = (long) (this.read() & 255);
                return var3 ? -var1 : var1;
            case 1:
                var3 = this.a(1) == 1;
                var1 = (long) this.cjC();
                return var3 ? -var1 : var1;
            case 2:
                var3 = this.a(1) == 1;
                var1 = (long) this.cjF();
                return var3 ? -var1 : var1;
            case 3:
                var3 = this.a(1) == 1;
                var1 = (long) this.cjE() & 4294967295L;
                return var3 ? -var1 : var1;
            case 4:
                var3 = this.a(1) == 1;
                var1 = this.diX();
                return var3 ? -var1 : var1;
            case 5:
                var3 = this.a(1) == 1;
                var1 = this.diW();
                return var3 ? -var1 : var1;
            case 6:
                var3 = this.a(1) == 1;
                var1 = this.diV();
                return var3 ? -var1 : var1;
            case 7:
            default:
                return this.cjG();
        }
    }

    public long cjG() throws EOFException {
        this.ensure(this.pos + 8);
        int var1 = this.buf[this.pos++] & 255;
        int var2 = this.buf[this.pos++] & 255;
        int var3 = this.buf[this.pos++] & 255;
        int var4 = this.buf[this.pos++] & 255;
        int var5 = this.buf[this.pos++] & 255;
        int var6 = this.buf[this.pos++] & 255;
        int var7 = this.buf[this.pos++] & 255;
        int var8 = this.buf[this.pos++] & 255;
        return ((long) var1 << 56) + ((long) (var2 & 255) << 48) + ((long) (var3 & 255) << 40) + ((long) (var4 & 255) << 32) + ((long) (var5 & 255) << 24) + (long) ((var6 & 255) << 16) + (long) ((var7 & 255) << 8) + (long) ((var8 & 255) << 0);
    }

    private long diV() throws EOFException {
        this.ensure(this.pos + 7);
        int var1 = this.buf[this.pos++] & 255;
        int var2 = this.buf[this.pos++] & 255;
        int var3 = this.buf[this.pos++] & 255;
        int var4 = this.buf[this.pos++] & 255;
        int var5 = this.buf[this.pos++] & 255;
        int var6 = this.buf[this.pos++] & 255;
        int var7 = this.buf[this.pos++] & 255;
        return ((long) (var1 & 255) << 48) + ((long) (var2 & 255) << 40) + ((long) (var3 & 255) << 32) + ((long) (var4 & 255) << 24) + (long) ((var5 & 255) << 16) + (long) ((var6 & 255) << 8) + (long) ((var7 & 255) << 0);
    }

    private long diW() throws EOFException {
        this.ensure(this.pos + 6);
        int var1 = this.buf[this.pos++] & 255;
        int var2 = this.buf[this.pos++] & 255;
        int var3 = this.buf[this.pos++] & 255;
        int var4 = this.buf[this.pos++] & 255;
        int var5 = this.buf[this.pos++] & 255;
        int var6 = this.buf[this.pos++] & 255;
        return ((long) (var1 & 255) << 40) + ((long) (var2 & 255) << 32) + ((long) (var3 & 255) << 24) + (long) ((var4 & 255) << 16) + (long) ((var5 & 255) << 8) + (long) ((var6 & 255) << 0);
    }

    private long diX() throws EOFException {
        this.ensure(this.pos + 5);
        int var1 = this.buf[this.pos++] & 255;
        int var2 = this.buf[this.pos++] & 255;
        int var3 = this.buf[this.pos++] & 255;
        int var4 = this.buf[this.pos++] & 255;
        int var5 = this.buf[this.pos++] & 255;
        return ((long) (var1 & 255) << 32) + ((long) (var2 & 255) << 24) + (long) ((var3 & 255) << 16) + (long) ((var4 & 255) << 8) + (long) ((var5 & 255) << 0);
    }

    public float readFloat() throws EOFException {
        if (this.a(1) == 0) {
            return Float.intBitsToFloat(this.cjE());
        } else {
            switch (this.a(2)) {
                case 0:
                    return (float) this.readInt();
                case 1:
                    return ala.fVk.cF(this.readByte());
                case 2:
                    return ala.fVj.cF(this.cjB());
                case 3:
                    return ala.fVl.cF(this.cjF());
                default:
                    return 0.0F;
            }
        }
    }

    public double readDouble() throws EOFException {
        return this.a(1) == 1 ? (double) this.readFloat() : Double.longBitsToDouble(this.cjG());
    }

    public String readLine() {
        if (this.ioz == null) {
            this.ioz = new char[128];
        }

        int var2 = this.ioz.length;
        char[] var3 = this.ioz;
        int var4 = 0;

        while (true) {
            int var1;
            switch (var1 = this.read()) {
                case -1:
                    if (var4 == 0) {
                        return null;
                    }

                    return String.copyValueOf(this.ioz, 0, var4);
                case 10:
                    return String.copyValueOf(this.ioz, 0, var4);
                case 13:
                    int var5 = this.read();
                    if (var5 != 10 && var5 != -1) {
                        --this.pos;
                    }

                    return String.copyValueOf(this.ioz, 0, var4);
                default:
                    --var2;
                    if (var2 < 0) {
                        this.ioz = new char[var4 + 128];
                        var2 = this.ioz.length - var4 - 1;
                        System.arraycopy(var3, 0, this.ioz, 0, var4);
                        var3 = this.ioz;
                    }

                    var3[var4++] = (char) var1;
            }
        }
    }

    public String readUTF() throws EOFException, UTFDataFormatException {
        int var1 = this.readUnsignedShort();
        this.ensure(this.pos + var1);
        if (this.ioz == null || this.ioz.length < var1) {
            this.ioz = new char[var1 * 2];
        }

        int var5 = 0;
        int var6 = 0;
        int var7 = this.pos;
        byte[] var8 = this.buf;

        int var2;
        for (this.pos += var1; var5 < var1; this.ioz[var6++] = (char) var2) {
            var2 = var8[var7 + var5] & 255;
            if (var2 > 127) {
                break;
            }

            ++var5;
        }

        while (var5 < var1) {
            var2 = var8[var7 + var5] & 255;
            byte var3;
            switch (var2 >> 4) {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                    ++var5;
                    this.ioz[var6++] = (char) var2;
                    break;
                case 8:
                case 9:
                case 10:
                case 11:
                default:
                    throw new UTFDataFormatException("malformed input around byte " + var5);
                case 12:
                case 13:
                    var5 += 2;
                    if (var5 > var1) {
                        throw new UTFDataFormatException("malformed input: partial character at end");
                    }

                    var3 = var8[var7 + var5 - 1];
                    if ((var3 & 192) != 128) {
                        throw new UTFDataFormatException("malformed input around byte " + var5);
                    }

                    this.ioz[var6++] = (char) ((var2 & 31) << 6 | var3 & 63);
                    break;
                case 14:
                    var5 += 3;
                    if (var5 > var1) {
                        throw new UTFDataFormatException("malformed input: partial character at end");
                    }

                    var3 = var8[var7 + var5 - 2];
                    byte var4 = var8[var7 + var5 - 1];
                    if ((var3 & 192) != 128 || (var4 & 192) != 128) {
                        throw new UTFDataFormatException("malformed input around byte " + (var5 - 1));
                    }

                    this.ioz[var6++] = (char) ((var2 & 15) << 12 | (var3 & 63) << 6 | (var4 & 63) << 0);
            }
        }

        return new String(this.ioz, 0, var6);
    }

    public String toString() {
        return aK.b(this.buf, 0, this.buf.length, " ", 16);
    }
}
