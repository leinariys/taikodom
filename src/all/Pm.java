package all;

import org.apache.commons.logging.Log;
import taikodom.render.loader.RenderAsset;

public class Pm implements aOT {
    private static final Log logger = LogPrinter.K(LoaderTrail.class);
    private EngineGraphics engineGraphics;

    public Pm(EngineGraphics var1) {
        this.engineGraphics = var1;
    }

    public void a(acj var1) {
        this.engineGraphics.bV(var1.fileName);
    }

    public void b(acj var1) {
        RenderAsset var2 = null;

        try {
            var2 = this.engineGraphics.bT(var1.hTN);
        } catch (Exception var5) {
            logger.error(var5.getMessage(), var5);
        }

        try {
            if (var2 != null) {
                var1.c(var2);
            } else {
                logger.error("Object not found " + var1.hTN);
            }
        } catch (RuntimeException var4) {
            logger.error(var4.getMessage(), var4);
        }
    }

    @Override
    public void b(aGU var1) {

    }

    @Override
    public void c(aGU var1) {

    }
}
