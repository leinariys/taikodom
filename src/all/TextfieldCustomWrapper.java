package all;

import javax.swing.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class TextfieldCustomWrapper extends JTextField implements ITextComponentCustomWrapper {
    private KeyAdapter dO;

    public void h(final int var1) {
        if (this.dO != null) {
            this.removeKeyListener(this.dO);
        } else {
            this.dO = new KeyAdapter() {
                public void keyTyped(KeyEvent var1x) {
                    if ((TextfieldCustomWrapper.this.getText().length() ^ var1) == 0) {
                        var1x.consume();
                    }
                }
            };
        }
        this.addKeyListener(this.dO);
    }

    public String getElementName() {
        return "textfield";
    }
}
