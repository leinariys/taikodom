package all;

import org.w3c.dom.css.CSSStyleDeclaration;

import javax.swing.*;
import java.awt.*;
import java.io.InputStream;

/**
 * class tj Загрузчик ресурсов аддона
 */
public interface axa {
    // void setGreen(Object var1, String var2);


    /**
     * Загрузить .xml файл
     *
     * @param var1 Пример login.xml
     * @return
     */
    IComponentCustomWrapper CreatJComponentFromXML(String var1);

    IComponentCustomWrapper CreatJComponentFromXML(String var1, IContainerCustomWrapper var2);


    /**
     * Загрузка .xml ресурса аддона
     *
     * @param var1 Пример taikodomversion.xml
     * @return
     */
    WindowCustomWrapper bN(String var1);

    void dispose();

    int getScreenHeight();

    /**
     * Размер окна java.awt.Dimension[width=1024,height=780]
     *
     * @return
     */
    Dimension getScreenSize();

    int getScreenWidth();

    Point getMousePosition();

    // void s(int var1, int var2);

    boolean adB();

    avI a(Class var1, String var2);

    avI a(InputStream var1);

    avI a(Object var1, String var2);

    CSSStyleDeclaration aY(String var1);

    void c(String var1, Class var2);

    void bO(String var1);

    aVf adA();

    ILoaderImageInterface adz();

    /**
     * Добавить в панель содержимого новый компонент
     *
     * @param var1
     */
    void addInRootPane(JComponent var1);

    void a(JComponent var1, avI var2, String var3);

    Rectangle c(JComponent var1);

    // Sr setGreen(Class var1, String var2);

    //  Sr loadFileXml(Class var1, String var2, il var3);

    //WindowCustomWrapper CreateJComponent(Class var1, String var2);


}
