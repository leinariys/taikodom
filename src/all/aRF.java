package all;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

public class aRF extends adJ {
    private List iKX = new ArrayList();

    public aRF() {
    }

    public aRF(int var1, ByteBuffer var2, int var3, int var4, ByteBuffer var5, int var6) {
        wW var7 = new wW();
        var7.bFf = var1;
        var7.bFg = var2;
        var7.bFh = var3;
        var7.numVertices = var4;
        var7.bFi = var5;
        var7.bFj = var6;
        this.a(var7);
    }

    public void a(wW var1) {
        this.a(var1, aop.ghD);
    }

    public void a(wW var1, aop var2) {
        this.iKX.add(var1);
        ((wW) this.iKX.get(this.iKX.size() - 1)).bFk = var2;
    }

    public void a(aUv var1, int var2) {
        assert var2 < this.bUc();

        wW var3 = (wW) this.iKX.get(var2);
        var1.iXj = var3.numVertices;
        var1.iXi = var3.bFi;
        var1.iXk = aop.ghB;
        var1.stride = var3.bFj;
        var1.iXn = var3.bFf;
        var1.iXl = var3.bFg;
        var1.iXm = var3.bFh;
        var1.iXo = var3.bFk;
    }

    public void b(aUv var1, int var2) {
        wW var3 = (wW) this.iKX.get(var2);
        var1.iXj = var3.numVertices;
        var1.iXi = var3.bFi;
        var1.iXk = aop.ghB;
        var1.stride = var3.bFj;
        var1.iXn = var3.bFf;
        var1.iXl = var3.bFg;
        var1.iXm = var3.bFh;
        var1.iXo = var3.bFk;
    }

    public void qg(int var1) {
    }

    public void qh(int var1) {
    }

    public int bUc() {
        return this.iKX.size();
    }

    public List dtP() {
        return this.iKX;
    }

    public void qi(int var1) {
    }

    public void qj(int var1) {
    }
}
