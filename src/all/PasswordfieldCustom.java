package all;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.awt.*;

public class PasswordfieldCustom extends TextComponentCustom {
    private static final LogPrinter logger = LogPrinter.setClass(FormattedFieldCustomWrapper.class);

    /**
     * Создание базового Component интерфейса
     * Вызывается из class BaseItem
     *
     * @param var1
     * @param var2
     * @param var3
     * @return
     */
    @Override
    protected JComponent CreatComponentCustom(MapKeyValue var1, Container var2, XmlNode var3) {
        return new PasswordFieldCustomWrapper();
    }

    protected void a(MapKeyValue var1, PasswordFieldCustomWrapper var2, XmlNode var3) {
        super.a(var1, (JTextComponent) var2, var3);
        String var4 = var3.getAttribute("echo-char");
        var2.setEchoChar(var4 != null && !var4.equals("") ? var4.charAt(0) : '*');
        Integer var5 = checkValueAttribut((XmlNode) var3, (String) "cols", (LogPrinter) logger);
        if (var5 != null) {
            var2.setColumns(var5.intValue());
        }

    }
}
