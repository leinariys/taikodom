package all;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * Загрузчик addons\taikodom.xml
 */
public class wm implements aVW {
    static LogPrinter logger = LogPrinter.K(wm.class);
    /**
     * Путь к addons\taikodom.xml
     */
    private final File file;
    private final AddonManager jcC;
    private b jcD = null;
    private XmlNode hcG;
    /**
     * Список аддонов с waitPlayer(ждать игрока) = true
     */
    private List jcE = new ArrayList();
    /**
     * Список аддонов с waitPlayer(ждать игрока) = false
     */
    private List jcF = new ArrayList();
    private String hcF;
    private long Ne = 0L;

    public wm(AddonManager var1, File var2) {
        this.jcC = var1;
        this.file = var2;
        if (var2.getName().endsWith(".xml")) {
            try {
                this.hcG = (new axx()).a((InputStream) (new FileInputStream(var2)), (String) "UTF-8");
            } catch (IOException var4) {
                throw new IllegalStateException("Cannot loadFileCSS descriptor", var4);
            }
        }

    }

    private void load() {
        logger.debug("Loading addon '" + this.file.getName() + "' ...");
        this.Ne = this.file.lastModified();

        try {
            long var1 = System.currentTimeMillis();
            this.start();//Старт закрузки списка аддонов
            logger.debug(String.format("'%s' loaded in %.3fs", this.file.getName(), (float) (System.currentTimeMillis() - var1) / 1000.0F));
        } catch (Exception var3) {
            logger.error("Problems loading addon: " + this.file, var3);
            this.dispose();
        }

    }

    /**
     * Старт закрузки списка аддонов
     */
    public void start() {
        if (this.file.getName().endsWith(".xml")) {
            this.dCK();
        }

    }

    /**
     * Загрузка списка аддонов
     */
    private void dCK() {
        try {
            this.hcG = (new axx()).a((InputStream) (new FileInputStream(this.file)), (String) "UTF-8");
        } catch (IOException var14) {
            throw new IllegalStateException("Cannot loadFileCSS descriptor", var14);
        }

        ArrayList var1 = new ArrayList();
        XmlNode var2 = this.hcG.mC("classpath");//Достаём данные из раздела classpath
        XmlNode var4;
        if (var2 != null) {
            List var3 = var2.getChildrenTag();
            Iterator var5 = var3.iterator();

            while (var5.hasNext()) {//Каждую строчку
                var4 = (XmlNode) var5.next();
                String var6;
                if ("dir".equals(var4.getTegName())) {
                    var6 = var4.getAttribute("path");
                    if (!var6.endsWith("/")) {
                        var6 = var6 + "/";
                    }

                    try {
                        var1.add((new File(this.file.getParent(), var6)).toURL());
                    } catch (MalformedURLException var13) {
                        throw new IllegalStateException("Cannot add classpath: " + var6, var13);
                    }
                } else if ("jar".equals(var4.getTegName())) {
                    var6 = var4.getAttribute("path");

                    try {
                        var1.add((new File(this.file.getParent(), var6)).toURL());
                    } catch (MalformedURLException var12) {
                        throw new IllegalStateException("Cannot add classpath: " + var6, var12);
                    }
                }
            }
        }
        URL[] var15 = new URL[var1.size()];
        var1.toArray(var15);
        this.jcD = new b(var15, this.getClass().getClassLoader());

        var4 = this.hcG.mC("libraries");//Достаём данные из раздела libraries
        if (var4 != null) {
            Iterator var18 = var4.getChildrenTag().iterator();
            while (var18.hasNext()) {//Каждую строчку
                XmlNode var16 = (XmlNode) var18.next();
                if ("dir".equals(var16.getTegName())) {
                    String var7 = var16.getAttribute("path");
                    if (!var7.endsWith("/")) {
                        var7 = var7 + "/";
                    }

                    this.jcD.hZ((new File(this.file.getParent(), var7)).getAbsolutePath());
                }
            }
        }

        List var17 = this.hcG.mF("addon"); //Достаём данные из раздела addon
        Iterator var19 = var17.iterator();
        while (var19.hasNext()) {//Каждую строчку
            XmlNode var20 = (XmlNode) var19.next();
            String var8 = var20.getAttribute("class"); //Пример taikodom.addon.splashscreen.SplashScreen
            String var9 = var20.getAttribute("waitPlayer"); // Параметр
            boolean var10 = var9 == null ? true : !"false".equals(var9);
            a var11 = new a();//Создание класса аддона
            var11.className = var8;
            if (var10) {
                this.jcE.add(var11);
            }//Добавляем аддон в списки
            else {
                this.jcF.add(var11);
            }
        }

        this.dCL();
        this.aVV();
    }

    /**
     * Создание аддонов с waitPlayer(ждать игрока) = false
     */
    private void dCL() {
        Iterator var2 = this.jcF.iterator();
        while (var2.hasNext()) {
            a var1 = (a) var2.next();
            logger.debug("Starting addon " + var1.className);
            try {
                if (var1.amq()) //Создаём класс аддона
                {
                    var1.bkV = this.jcC.aVM().a(this.jcC, this, var1.bCI);
                }
            } catch (Error var4) {
                logger.error("Problems starting addon", var4);
            } catch (Exception var5) {
                logger.error("Problems starting addon", var5);
            }
        }

    }

    /**
     * Создание аддонов с waitPlayer(ждать игрока) = true
     */
    public void aVV() {
        //if (this.jcC.aVT())
        {
            Iterator var1 = this.jcE.iterator();
            while (var1.hasNext()) {
                a var2 = (a) var1.next();
                logger.debug("Starting addon " + var2.className);
                try {
                    if (var2.amq()) //Создаём класс аддона
                    {
                        var2.bkV = this.jcC.aVM().a(this.jcC, this, var2.bCI);
                    }
                } catch (Exception var4) {
                    logger.error("Problems starting addon", var4);
                } catch (Error var5) {
                    logger.error("Problems starting addon", var5);
                }

                this.jcF.add(var2);
                var1.remove();
            }
        }
    }

    public String cHn() {
        return this.hcF;
    }

    public void stop() {
        Iterator var2 = this.jcE.iterator();

        a var1;
        while (var2.hasNext()) {
            var1 = (a) var2.next();
            logger.debug("Stopping " + var1.getClass().getName());

            try {
                if (var1.bkV != null) {
                    var1.bkV.dispose();
                }
            } catch (Exception var5) {
                logger.error("Problems stopping addon", var5);
            }
        }

        this.jcE.clear();
        var2 = this.jcF.iterator();

        while (var2.hasNext()) {
            var1 = (a) var2.next();
            logger.debug("Stopping " + var1.getClass().getName());

            try {
                if (var1.bkV != null) {
                    var1.bkV.dispose();
                }
            } catch (Exception var4) {
                logger.error("Problems stopping addon", var4);
            }
        }

        this.jcF.clear();
        this.jcD = null;
    }

    public void dispose() {
        if (this.jcD != null) {
            logger.debug("Unloading addon '" + this.file.getName() + "'");
            this.stop();
        }
    }

    /**
     * Загрузка списка аддонов
     */
    public void dCM() {
        if (this.Ne != this.file.lastModified()) {
            this.dispose();
            this.load();//Начать чтение файла с аддонами и загрузить их
        }

    }

    public File getFile() {
        return this.file;
    }

    public void b(String var1, String[] var2) {
        Iterator var4 = this.jcF.iterator();

        while (var4.hasNext()) {
            a var3 = (a) var4.next();
            if (var3.bkV != null) {
                var3.bkV.c(var1, var2);
            }
        }

    }

    public Object aR(Class var1) {
        Iterator var3 = this.jcF.iterator();

        while (var3.hasNext()) {
            a var2 = (a) var3.next();
            if (var1.isInstance(var2.bCI)) {
                return var2.bCI;
            }
        }

        return null;
    }

    public List aS(Class var1) {
        ArrayList var2 = new ArrayList();
        Iterator var4 = this.jcF.iterator();

        while (var4.hasNext()) {
            a var3 = (a) var4.next();
            if (var1.isInstance(var3.bCI)) {
                System.out.println();
                var2.add(var3.bCI);
            }
        }

        return var2;
    }

    public void a(aMS var1) {
        if (var1 != null) {
            Iterator var3 = this.jcF.iterator();

            while (var3.hasNext()) {
                a var2 = (a) var3.next();
                if (var2.bCI != null && var2.bCI.getClass() != null && var2.bCI.getClass().equals(var1.getClass())) {
                    if (var2.bkV != null) {
                        var2.bkV.dispose();
                    }

                    var2.bCI = null;
                    var2.bkV = null;

                    try {
                        if (var2.amq()) {
                            var2.bkV = this.jcC.aVM().a(this.jcC, this, var2.bCI);
                        }
                        break;
                    } catch (Exception var5) {
                        throw new RuntimeException(var5);
                    }
                }
            }
        }
    }

    public Collection dCN() {
        ArrayList var1 = new ArrayList();
        Iterator var3 = this.jcF.iterator();

        while (var3.hasNext()) {
            a var2 = (a) var3.next();
            if (var2.bCI != null) {
                var1.add(var2.bCI);
            }
        }

        return var1;
    }

    static class b extends URLClassLoader {
        private List eSu = new ArrayList();

        public b(URL[] var1, ClassLoader var2) {
            super(var1, var2);
        }

        public void hZ(String var1) {
            this.eSu.add(var1);
        }

        protected String findLibrary(String var1) {
            String var2 = super.findLibrary(var1);
            if (var2 == null) {
                Iterator var4 = this.eSu.iterator();

                while (var4.hasNext()) {
                    String var3 = (String) var4.next();
                    File var5 = new File(var3, System.mapLibraryName(var1));
                    if (var5.exists()) {
                        return var5.getAbsolutePath();
                    }
                }
            }

            return var2;
        }
    }

    /**
     * Класс данных строки аддона
     */
    private class a {
        /**
         * class GK Обёртка класса аддона логирование и локализация
         */
        aVf bkV;
        /**
         * Загруженный класс аддона
         */
        aMS bCI;
        /**
         * Название Аддона
         * Пример taikodom.addon.splashscreen.SplashScreen
         */
        String className;

        /**
         * Создание экземпляра класса аддона
         *
         * @return true-успех или уже загружен. false-ошибка
         * @throws IllegalAccessException
         * @throws InstantiationException
         * @throws ClassNotFoundException
         */
        public boolean amq() throws IllegalAccessException, InstantiationException, ClassNotFoundException {
            if (this.bCI == null) {
                Class var1 = wm.this.jcD.loadClass(this.className);
                if (!aMS.class.isAssignableFrom(var1)) {
                    return false;
                } else {
                    this.bCI = (aMS) var1.newInstance();
                    return true;
                }
            } else {
                return true;
            }
        }
    }
}
