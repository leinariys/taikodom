package all;

import java.net.URLStreamHandler;
import java.net.URLStreamHandlerFactory;
import java.util.HashMap;
import java.util.Map;

/**
 * URL-адрес приложения для Java Virtual Machine.
 * URLStreamHandlerFactory - Этот интерфейс определяет фабрику для обработчиков протоколов потока URL.
 */
public class hP implements URLStreamHandlerFactory {
    /**
     * Ссылка на фабрику
     */
    private static hP VR;
    /**
     * Таблица хеш значений
     */
    protected Map map = new HashMap();

    public static hP ze() {
        if (VR == null) {
            VR = new hP();
        }

        return VR;
    }

    /**
     * Создает новый экземпляр URLStreamHandler с указанным протоколом.
     *
     * @param var1 протокол («ftp», «http», «nntp» и т. д.).
     * @return URLStreamHandler для конкретного протокола.
     */
    public URLStreamHandler createURLStreamHandler(String var1) {
        return (URLStreamHandler) this.map.get(var1.toLowerCase());
    }

    /**
     * Связывает указанное значение с указанным ключом на этой карте.
     *
     * @param var1 ключ, с которым должно быть связано указанное значение
     * @param var2 значение, которое должно быть связано с указанным ключом
     */
    public void a(String var1, URLStreamHandler var2) {
        this.map.put(var1.toLowerCase(), var2);
    }
}
