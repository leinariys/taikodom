package all;

import javax.swing.*;
import java.awt.*;

public class LabeledIconCustomWrapper extends PanelCustomWrapper {

    // $FF: synthetic field
    private static int[] eLR;
    private PictureCustomWrapper eLK;
    private PictureCustomWrapper eLL;
    private PictureCustomWrapper eLM;
    private PictureCustomWrapper eLN;
    private IconViewerCustomWrapper eLO;
    private IContainerCustomWrapper eLP;
    private IContainerCustomWrapper eLQ;

    public LabeledIconCustomWrapper() {
        this.setLayout(new GridLayout());
        this.eLQ = (PanelCustomWrapper) BaseUItegXML.thisClass.CreateJComponent(LabeledIconCustomWrapper.class, "labeledIcon.xml");
        this.eLK = (PictureCustomWrapper) this.eLQ.findJComponent("upper-label-w");
        this.eLL = (PictureCustomWrapper) this.eLQ.findJComponent("upper-label-CreateJComponentAndAddInRootPane");
        this.eLM = (PictureCustomWrapper) this.eLQ.findJComponent("lower-label-w");
        this.eLN = (PictureCustomWrapper) this.eLQ.findJComponent("lower-label-CreateJComponentAndAddInRootPane");
        this.eLO = (IconViewerCustomWrapper) this.eLQ.findJComponent("image-label");
        this.eLP = this.eLQ.ce("overlay");
        this.add((PanelCustomWrapper) this.eLQ);
    }

    // $FF: synthetic method
    static int[] bHs() {
        int[] var10000 = eLR;
        if (eLR != null) {
            return var10000;
        } else {
            int[] var0 = new int[a.values().length];

            try {
                var0[a.ddD.ordinal()] = 2;
            } catch (NoSuchFieldError var4) {
                ;
            }

            try {
                var0[a.ddC.ordinal()] = 1;
            } catch (NoSuchFieldError var3) {
                ;
            }

            try {
                var0[a.ddF.ordinal()] = 4;
            } catch (NoSuchFieldError var2) {
                ;
            }

            try {
                var0[a.ddE.ordinal()] = 3;
            } catch (NoSuchFieldError var1) {
                ;
            }

            eLR = var0;
            return var0;
        }
    }

    public PictureCustomWrapper a(a var1) {
        switch (bHs()[var1.ordinal()]) {
            case 1:
                return this.eLK;
            case 2:
                return this.eLL;
            case 3:
                return this.eLM;
            case 4:
                return this.eLN;
            default:
                return null;
        }
    }

    public void a(a var1, String var2) {
        PictureCustomWrapper var3 = this.a(var1);
        var3.setText(var2);
        if (var2 != null && var2.length() > 0) {
            var3.setSize(var3.getPreferredSize());
            var3.setVisible(true);
        } else if (var3.getIcon() == null || var3.getIcon().getIconHeight() <= 0 || var3.getIcon().getIconWidth() <= 0) {
            var3.setSize(0, 0);
            var3.setVisible(false);
        }

    }

    public void b(a var1, String var2) {
        this.a(var1).setImage(var2);
    }

    public void a(a var1, Icon var2) {
        PictureCustomWrapper var3 = this.a(var1);
        var3.setIcon(var2);
        if (var2 != null && var2.getIconHeight() > 0 && var2.getIconWidth() > 0) {
            var3.setSize(var3.getPreferredSize());
            var3.setVisible(true);
        } else if (var3.getText() == null || var3.getText().length() <= 0) {
            var3.setSize(0, 0);
            var3.setVisible(false);
        }

    }

    public void setBackgroundImage(String var1) {
        this.eLO.setImage(var1);
    }

    public void a(Icon var1) {
        this.eLO.setIcon(var1);
        if (var1 != null) {
            this.eLO.setSize(var1.getIconWidth(), var1.getIconHeight());
            this.setPreferredSize(this.getPreferredSize());
            ((PanelCustomWrapper) this.eLQ).setPreferredSize(this.eLO.getPreferredSize());
        }

    }

    public IconViewerCustomWrapper bHq() {
        return this.eLO;
    }

    public String b(a var1) {
        return this.a(var1).getText();
    }

    public void bHr() {
    }

    public void a(b var1) {
        a[] var5;
        int var4 = (var5 = a.values()).length;

        for (int var3 = 0; var3 < var4; ++var3) {
            a var2 = var5[var3];
            this.a(var2, var1.b(var2));
            this.a(var2, var1.c(var2));
        }

        this.a(var1.deh());
    }

    public String getElementName() {
        return "labeled-iconImage";
    }

    public void lB(int var1) {
        if (this.eLO != null) {
            this.eLO.lB(var1);
        }

    }

    public static enum a {
        ddC,
        ddD,
        ddE,
        ddF;
    }

    public abstract static class b {
        public String b(a var1) {
            return null;
        }

        public Icon c(a var1) {
            return null;
        }

        public Icon deh() {
            return null;
        }
    }
}
