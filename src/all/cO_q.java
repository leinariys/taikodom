package all;

import taikodom.render.DrawContext;
import taikodom.render.RenderView;
import taikodom.render.graphics2d.RGraphics2;
import taikodom.render.textures.RenderTarget;
import taikodom.render.textures.SimpleTexture;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.basic.BasicInternalFrameUI;
import javax.vecmath.Vector2f;
import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyVetoException;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * class test_GLCanvas
 */
public class cO_q {
    private static final Logger logger = Logger.getLogger(cO_q.class.getName());
    private static final int fKx = 300;
    private static final int fKF = 4;
    private static int fKy = 0;
    private final k fKN = new k((k) null);
    private RGraphics2 fKu;
    private JDesktopPane desktop;
    private SimpleTexture texture;
    private boolean initialized;
    private int width;
    private int height;
    private boolean fKv = false;
    private final Frame frame = new Frame() {


        public boolean isShowing() {
            return true;
        }

        public boolean isVisible() {
            if (cO_q.this.frame.isFocusableWindow() && (new Throwable()).getStackTrace()[1].getMethodName().startsWith("requestFocus")) {
                return false;
            } else {
                return cO_q.this.initialized || super.isVisible();
            }
        }

        public Graphics getGraphics() {
            if (!cO_q.this.fKv) {
                return cO_q.this.fKu == null ? super.getGraphics() : cO_q.this.fKu.create();
            } else {
                return super.getGraphics();
            }
        }

        public boolean isFocused() {
            return true;
        }
    };
    private Component fKz;
    private Component fKA;
    private int fKB;
    private int fKC = 0;
    private int fKD = 0;
    private long fKE = 0L;
    private int clickCount = 0;
    private Vector2f fKG = new Vector2f();
    private boolean fKH = true;
    private boolean fKI = false;
    private int fKJ = -1;
    private int fKK = -1;
    private int fKL = -1;
    private int fKM = -1;
    private Component fKO;
    private long fKP;
    private long fKQ;
    private RenderTarget renderTarget;

    public cO_q(String var1) {
        this.frame.setName(var1);
        this.frame.setFocusableWindowState(false);
        Frame frame1 = this.frame;
        this.frame.setUndecorated(true);
        setNullBordAndFalseOpaque((Container) frame1);
        this.desktop = new JDesktopPane() {
            public void paint(Graphics var1) {
                if (!cO_q.this.caG()) {
                    var1.clearRect(0, 0, this.getWidth(), this.getHeight());
                }

                super.paint(var1);
            }

            public boolean isOptimizedDrawingEnabled() {
                return false;
            }
        };

        (new l((l) null)).b(this.desktop);
        Color var3 = new Color(0, 0, 0, 0);
        this.desktop.setBackground(var3);
        this.desktop.setFocusable(true);
        this.desktop.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent var1) {
                cO_q.this.desktop.requestFocusInWindow();
            }
        });

        String gg = System.getProperty("os.name");

        if (System.getProperty("os.name").toLowerCase().indexOf("mac") < 0) {
            JInternalFrame var4 = new JInternalFrame();
            var4.setUI(new BasicInternalFrameUI(var4) {
                protected void installComponents() {
                }
            });
            var4.setOpaque(false);
            var4.setBackground((Color) null);
            var4.getContentPane().setLayout(new BorderLayout());
            var4.getContentPane().add(this.desktop, "Center");
            var4.setVisible(true);
            var4.setBorder((Border) null);
            frame1.add(var4);
        } else {
            frame1.add(this.desktop, "Center");
        }

        this.frame.setSize(100, 100);
        this.frame.validate();
        RepaintManager.currentManager(this.desktop).setDoubleBufferingEnabled(false);
    }

    private static void setNullBordAndFalseOpaque(Container var0) {
        if (var0 != null) {
            var0.setBackground((Color) null);
            if (var0 instanceof JComponent) {
                JComponent var1 = (JComponent) var0;
                var1.setOpaque(false);
            }

            setNullBordAndFalseOpaque(var0.getParent());
        }

    }

    private static int g(int var0, boolean var1) {
        int var2 = 1;
        for (var2 = 1; var2 < var0; var2 <<= 1) {
            ;
        }
        return var2;
    }

    public boolean caG() {
        return this.fKv;
    }

    /**
     * @deprecated
     */
    public void eT(boolean var1) {
        this.fKv = var1;
        this.frame.setVisible(var1);
        this.frame.repaint();
    }

    public void c(int var1, int var2, boolean var3) {
        if (this.initialized) {
            throw new IllegalStateException("may be called only once");
        } else {
            this.width = g(var1, var3);
            this.height = g(var2, var3);
        }
    }

    protected void caH() {
    }

    public void a(final char var1, final int var2, final boolean var3) {
        try {
            SwingUtilities.invokeAndWait(new Runnable() {
                public void run() {
                    cO_q.this.a(var2, var3, var1);
                }
            });
        } catch (InterruptedException var5) {
            logger.logp(Level.SEVERE, this.getClass().toString(), "onKey(character, keyCode, pressed)", "Exception", var5);
        } catch (InvocationTargetException var6) {
            logger.logp(Level.SEVERE, this.getClass().toString(), "onKey(character, keyCode, pressed)", "Exception", var6);
        }

    }

    public void a(final int var1, final boolean var2, int var3, int var4) {
        this.a(var3, var4, this.fKG);
        final int var5 = (int) this.fKG.x;
        final int var6 = (int) this.fKG.y;

        try {
            SwingUtilities.invokeAndWait(new Runnable() {
                public void run() {
                    cO_q.this.a(var5, var6, var2, var1);
                }
            });
        } catch (InterruptedException var8) {
            logger.logp(Level.SEVERE, this.getClass().toString(), "onButton(swingButton, pressed, x, y)", "Exception", var8);
        } catch (InvocationTargetException var9) {
            logger.logp(Level.SEVERE, this.getClass().toString(), "onButton(swingButton, pressed, x, y)", "Exception", var9);
        }

    }

    public void r(final int var1, int var2, int var3) {
        this.a(var2, var3, this.fKG);
        final int var4 = (int) this.fKG.x;
        final int var5 = (int) this.fKG.y;

        try {
            SwingUtilities.invokeAndWait(new Runnable() {
                public void run() {
                    cO_q.this.s(var1, var4, var5);
                }
            });
        } catch (InterruptedException var7) {
            logger.logp(Level.SEVERE, this.getClass().toString(), "onWheel(wheelDelta, x, y)", "Exception", var7);
        } catch (InvocationTargetException var8) {
            logger.logp(Level.SEVERE, this.getClass().toString(), "onWheel(wheelDelta, x, y)", "Exception", var8);
        }

    }

    public void c(int var1, int var2, int var3, int var4) {
        this.a(var3, var4, this.fKG);
        final int var5 = (int) this.fKG.x;
        final int var6 = (int) this.fKG.y;

        try {
            SwingUtilities.invokeAndWait(new Runnable() {
                public void run() {
                    cO_q.this.a(var5, var6, false, 0);
                }
            });
        } catch (InterruptedException var8) {
            logger.logp(Level.SEVERE, this.getClass().toString(), "onMove(xDelta, yDelta, newX, newY)", "Exception", var8);
        } catch (InvocationTargetException var9) {
            logger.logp(Level.SEVERE, this.getClass().toString(), "onMove(xDelta, yDelta, newX, newY)", "Exception", var9);
        }

    }

    private void a(int var1, boolean var2, char var3) {
        if (var1 != 0) {
            Object var4 = this.getFocusOwner();
            if (var4 == null) {
                var4 = this.desktop;
            }

            if (var3 == 0) {
                var3 = '\uffff';
            }

            if (var4 != null) {
                if (var2) {
                    KeyEvent var5 = new KeyEvent((Component) var4, 401, System.currentTimeMillis(), this.qT(-1), var1, var3);
                    this.a((Component) var4, var5);
                    if (var3 != '\uffff') {
                        this.a((Component) var4, new KeyEvent((Component) var4, 400, System.currentTimeMillis(), this.qT(-1), 0, var3));
                    }
                }

                if (!var2) {
                    this.a((Component) var4, new KeyEvent((Component) var4, 402, System.currentTimeMillis(), this.qT(-1), var1, var3));
                }
            }
        }

    }

    private void a(Component var1, AWTEvent var2) {
        if (this.caJ() == null || SwingUtilities.isDescendingFrom(var1, this.caJ())) {
            if (!SwingUtilities.isEventDispatchThread()) {
                throw new IllegalStateException("not in swing thread!");
            }

            var1.dispatchEvent(var2);
        }

    }

    private void s(int var1, int var2, int var3) {
        Object var4 = this.fKz != null ? this.fKz : this.a(var2, var3, this.desktop, false);
        if (var4 == null) {
            var4 = this.desktop;
        }

        Point var5 = this.convertPoint(this.desktop, var2, var3, (Component) var4);
        MouseWheelEvent var6 = new MouseWheelEvent((Component) var4, 507, System.currentTimeMillis(), this.qT(-1), var5.x, var5.y, 1, false, 0, Math.abs(var1), var1 > 0 ? -1 : 1);
        this.a((Component) var4, var6);
    }

    private Point convertPoint(Component var1, int var2, int var3, Component var4) {
        if (this.fKH) {
            try {
                return SwingUtilities.convertPoint(var1, var2, var3, (Component) var4);
            } catch (InternalError var5) {
                this.fKH = false;
            }
        }

        if (var4 != null) {
            while (var4 != var1) {
                var2 -= ((Component) var4).getX();
                var3 -= ((Component) var4).getY();
                if (((Component) var4).getParent() == null) {
                    break;
                }

                var4 = ((Component) var4).getParent();
            }
        }

        return new Point(var2, var3);
    }

    private void a(int var1, int var2, boolean var3, int var4) {
        Component var5 = this.a(var1, var2, this.desktop, false);
        int var6;
        if (var4 > 0) {
            var6 = var3 ? 501 : 502;
        } else {
            var6 = this.qU(0) == 0 ? 503 : 506;
        }

        long var7 = System.currentTimeMillis();
        if (this.fKz != var5) {
            Point var9;
            while (this.fKz != null && (var5 == null || !SwingUtilities.isDescendingFrom(var5, this.fKz))) {
                var9 = this.convertPoint(this.desktop, var1, var2, this.fKz);
                this.a(this.fKz, this.qT(var4), var9);
                this.fKz = this.fKz.getParent();
            }

            var9 = this.convertPoint(this.desktop, var1, var2, this.fKz);
            if (this.fKz == null) {
                this.fKz = this.desktop;
            }

            this.a(var5, this.fKz, this.qT(var4), var9);
            this.fKz = var5;
            this.fKC = Integer.MIN_VALUE;
            this.fKD = Integer.MIN_VALUE;
            this.fKE = 0L;
        }

        if (var5 != null) {
            boolean var14 = false;
            if (var4 > 0) {
                if (var3) {
                    this.fKA = var5;
                    this.fKB = var4;
                    this.fKC = var1;
                    this.fKD = var2;
                    this.n(this.a(var1, var2, this.desktop, true));
                } else if (this.fKB == var4 && this.fKA != null) {
                    var5 = this.fKA;
                    this.fKA = null;
                    if (Math.abs(this.fKC - var1) <= 4 && Math.abs(this.fKD - var2) < 4) {
                        if (this.fKE + 300L > var7) {
                            ++this.clickCount;
                        } else {
                            this.clickCount = 1;
                        }

                        var14 = true;
                        this.fKE = var7;
                    }

                    this.fKC = Integer.MIN_VALUE;
                    this.fKD = Integer.MIN_VALUE;
                }
            } else if (this.fKA != null) {
                var5 = this.fKA;
            }

            Point var10 = this.convertPoint(this.desktop, var1, var2, var5);
            MouseEvent var11 = new MouseEvent(var5, var6, var7, this.qT(var4), var10.x, var10.y, this.clickCount, var4 == 2 && var3, var4 >= 0 ? var4 : 0);
            this.a(var5, var11);
            if (var14) {
                var5 = this.a(var1, var2, this.desktop, true);
                Point var12 = this.convertPoint(this.desktop, var1, var2, var5);
                MouseEvent var13 = new MouseEvent(var5, 500, var7, this.qT(var4), var12.x, var12.y, this.clickCount, false, var4);
                this.a(var5, var13);
            }
        } else if (var3) {
            this.n((Component) null);
        }

    }

    public void n(Component var1) {
        if (var1 == null || var1.isFocusable()) {
            for (Object var2 = var1; var2 != null; var2 = ((Component) var2).getParent()) {
                if (var2 instanceof JInternalFrame) {
                    try {
                        ((JInternalFrame) var2).setSelected(true);
                    } catch (PropertyVetoException var4) {
                        logger.logp(Level.SEVERE, this.getClass().toString(), "setFocusOwner(Component comp)", "Exception", var4);
                    }
                }
            }

            this.frame.setFocusableWindowState(true);
            Component var5 = this.getFocusOwner();
            if (var1 == this.desktop) {
                var1 = null;
            }

            if (var5 != var1) {
                if (var5 != null) {
                    this.a(var5, new FocusEvent(var5, 1005, false, var1));
                }

                KeyboardFocusManager.getCurrentKeyboardFocusManager().clearGlobalFocusOwner();
                if (var1 != null) {
                    this.a(var1, new FocusEvent(var1, 1004, false, var5));
                }
            }

            this.frame.setFocusableWindowState(false);
        }

        this.fKI = var1 == null;
    }

    private int qT(int var1) {
        byte var2 = 0;
        return var2 | this.qU(var1);
    }

    private int qU(int var1) {
        byte var2 = 0;
        return var2;
    }

    public void a(int var1, int var2, Vector2f var3) {
        if (this.fKJ == var1 && this.fKL == var2) {
            var3.x = (float) this.fKK;
            var3.y = (float) this.fKM;
        } else {
            this.fKJ = var1;
            this.fKL = var2;
        }

        this.fKM = var2;
        this.fKK = var1;
        var3.set((float) var1, (float) var2);
    }

    public Component ac(int var1, int var2) {
        Component var3 = this.a(var1, var2, this.desktop, true);
        return var3 != this.desktop ? var3 : null;
    }

    private Component a(int var1, int var2, Component var3, boolean var4) {
        if (var4 && var3 instanceof JRootPane) {
            JRootPane var5 = (JRootPane) var3;
            var3 = var5.getContentPane();
        }

        Object var12 = var3;
        if (!((Component) var3).contains(var1, var2)) {
            var12 = null;
        } else {
            synchronized (((Component) var3).getTreeLock()) {
                if (var3 instanceof Container) {
                    Container var7 = (Container) var3;
                    int var8 = var7.getComponentCount();

                    for (int var9 = 0; var9 < var8; ++var9) {
                        Component var10 = var7.getComponent(var9);
                        if (var10 != null && var10.isVisible() && var10.contains(var1 - var10.getX(), var2 - var10.getY())) {
                            var12 = var10;
                            break;
                        }
                    }
                }
            }
        }

        if (var12 != null) {
            if (var3 instanceof JTabbedPane && var12 != var3) {
                var12 = ((JTabbedPane) var3).getSelectedComponent();
            }

            var1 -= ((Component) var12).getX();
            var2 -= ((Component) var12).getY();
        }

        return (Component) (var12 != var3 && var12 != null ? this.a(var1, var2, (Component) var12, var4) : var12);
    }

    private void a(Component var1, Component var2, int var3, Point var4) {
        if (var1 != null && var1 != var2) {
            this.a(var1.getParent(), var2, var3, var4);
            var4 = this.convertPoint(var2, var4.x, var4.y, var1);
            MouseEvent var5 = new MouseEvent(var1, 504, System.currentTimeMillis(), var3, var4.x, var4.y, 0, false, 0);
            this.a(var1, var5);
        }

    }

    private void a(Component var1, int var2, Point var3) {
        MouseEvent var4 = new MouseEvent(var1, 505, System.currentTimeMillis(), var2, var3.x, var3.y, 1, false, 0);
        this.a(var1, var4);
    }

    public void a(Renderer var1) {
    }

    /**
     * cal
     *
     * @return
     */
    public JDesktopPane getDesktopPane() {
        return this.desktop;
    }

    public Component getFocusOwner() {
        return !this.fKI ? this.frame.getFocusOwner() : null;
    }

    private int qV(int var1) {
        switch (var1) {
            case 0:
                return 1;
            case 1:
                return 2;
            case 2:
                return 3;
            default:
                return 0;
        }
    }

    public Component caJ() {
        return this.fKO;
    }

    public void o(Component var1) {
        this.fKO = var1;
    }

    public void dispose() {
        if (this.desktop != null) {
            try {
                SwingUtilities.invokeAndWait(new Runnable() {
                    public void run() {
                        cO_q.this.desktop.removeAll();
                        cO_q.this.frame.dispose();
                    }
                });
            } catch (InterruptedException var2) {
                logger.logp(Level.SEVERE, this.getClass().toString(), "dispose()", "Exception", var2);
            } catch (InvocationTargetException var3) {
                logger.logp(Level.SEVERE, this.getClass().toString(), "dispose()", "Exception", var3);
            }

            this.desktop = null;
            --fKy;
            if (fKy == 0) {
                PopupFactory.setSharedInstance(new PopupFactory());
            }
        }

    }

    public void r(float var1, float var2) {
        throw new UnsupportedOperationException("resizing JMEDesktop not yet implemented!");
    }

    public void draw(DrawContext var1) {/**GL наследует интерфейс GLBase, который содержит методы для создания всех объектов контекста OpenGL
     * седержит методы для рисования примитивов (линии треугольники)
     * @return
     */
        javax.media.opengl.GL var2 = var1.getGL();
        RenderView var3 = var1.getRenderView();
        var2.glMatrixMode(5888);
        var2.glLoadIdentity();
        var2.glMatrixMode(5889);
        var2.glLoadIdentity();
        var2.glViewport(0, 0, this.width, var3.getViewport().height);
        var2.glDisable(2929);
        var2.glOrtho(0.0D, (double) var3.getViewport().width, (double) var3.getViewport().height, 0.0D, -1.0D, 1.0D);
        var2.glMatrixMode(5888);
        var2.glCullFace(0);
        var2.glBlendFunc(1, 771);
        var2.glDisableClientState(32884);
        var2.glDisableClientState(32888);
        var2.glDisableClientState(32886);
        var2.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
        if (this.fKu == null) {
            this.fKu = new RGraphics2(var1);
        }

        this.fKu.reset();
        this.desktop.paint(this.fKu.create());
        this.fKu.getGuiScene().draw();
    }

    private static class m extends Popup {
        private static final Integer dIb = Integer.MAX_VALUE;
        private final JComponent dIc;
        JPanel dve = new JPanel(new BorderLayout());

        public m(JComponent var1) {
            this.dIc = var1;
            (new l((l) null)).b(this.dve);
        }

        public void a(Component var1, Component var2, int var3, int var4) {
            this.dve.setVisible(false);
            this.dIc.add(this.dve, dIb);
            this.dve.removeAll();
            this.dve.add(var2, "Center");
            if (var2 instanceof JComponent) {
                JComponent var5 = (JComponent) var2;
                var5.setDoubleBuffered(false);
            }

            this.dve.setSize(this.dve.getPreferredSize());
            var4 = Math.min(var4, this.dIc.getHeight() - this.dve.getHeight());
            var3 = Math.min(var3, this.dIc.getWidth() - this.dve.getWidth());
            this.dve.setLocation(var3, var4);
            var2.invalidate();
            this.dve.validate();
        }

        public void show() {
            this.dve.setVisible(true);
        }

        public void hide() {
            Rectangle var1 = this.dve.getBounds();
            this.dIc.remove(this.dve);
            this.dIc.repaint(var1);
        }
    }

    private static class j extends PopupFactory {
        private final PopupFactory aUu = new PopupFactory();

        public Popup getPopup(Component var1, Component var2, int var3, int var4) {
            do {
                if (var1 instanceof JDesktopPane) {
                    m var5 = new m((JComponent) var1);
                    var5.a((Component) var1, var2, var3, var4);
                    return var5;
                }

                var1 = ((Component) var1).getParent();
            } while (var1 != null);

            cO_q.logger.warning("jME Popup creation failed, default popup created - desktop not found in component hierarchy of " + var1);
            return this.aUu.getPopup((Component) var1, var2, var3, var4);
        }
    }

    private static class l implements ContainerListener {
        private l() {
        }

        // $FF: synthetic method
        l(l var1) {
            this();
        }

        public void componentAdded(ContainerEvent var1) {
            Component var2 = var1.getChild();
            this.j(var2);
        }

        private void j(Component var1) {
            if (var1 instanceof Container) {
                Container var2 = (Container) var1;
                this.b(var2);
                var2.addContainerListener(this);
            }

            if (var1 instanceof JScrollPane) {
                JScrollPane var3 = (JScrollPane) var1;
                this.a(var3.getViewport());
            }

        }

        private void b(Container var1) {
            var1.addContainerListener(this);

            for (int var2 = 0; var2 < var1.getComponentCount(); ++var2) {
                this.j(var1.getComponent(var2));
            }

        }

        private void c(Container var1) {
            var1.removeContainerListener(this);

            for (int var2 = 0; var2 < var1.getComponentCount(); ++var2) {
                this.k(var1.getComponent(var2));
            }

        }

        private void a(JViewport var1) {
            for (int var2 = 0; var2 < var1.getChangeListeners().length; ++var2) {
                ChangeListener var3 = var1.getChangeListeners()[var2];
                if (var3 instanceof a) {
                    return;
                }
            }

            var1.addChangeListener(new a(var1));
        }

        public void componentRemoved(ContainerEvent var1) {
            Component var2 = var1.getChild();
            this.k(var2);
        }

        private void k(Component var1) {
            if (var1 instanceof Container) {
                Container var2 = (Container) var1;
                this.c(var2);
            }

        }

        private static class a implements ChangeListener {
            private final Component component;

            public a(Component var1) {
                this.component = var1;
            }

            public void stateChanged(ChangeEvent var1) {
                this.component.repaint();
            }
        }
    }

    private class k implements Runnable {
        private boolean cWM;

        private k() {
            this.cWM = false;
        }

        // $FF: synthetic method
        k(k var2) {
            this();
        }

        public void run() {
            synchronized (cO_q.this.fKN) {
                this.notifyAll();
                if (this.cWM) {
                    try {
                        this.cWM = false;
                        cO_q.this.fKN.wait(200L);
                    } catch (InterruptedException var3) {
                        cO_q.logger.logp(Level.SEVERE, this.getClass().toString(), "run()", "Exception", var3);
                    }
                }

            }
        }
    }

    private final class n implements Runnable {
        private Rectangle ddg;
        private amC ioA;

        private n(Rectangle var2, amC var3) {
            this.ddg = var2;
            this.ioA = var3;
        }

        public void run() {
            cO_q.this.fKu.reset();
            cO_q.this.fKu.getContext().startRendering();

            try {
                cO_q.this.desktop.paint(cO_q.this.fKu.create());
                Rectangle var1 = cO_q.this.desktop.getBounds();
                this.ddg.x = var1.x;
                this.ddg.y = var1.y;
                this.ddg.width = var1.width;
                this.ddg.height = var1.height;
            } finally {
                cO_q.this.fKu.getContext().stopRendering();
            }

        }
    }
}
