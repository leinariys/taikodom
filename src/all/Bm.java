package all;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;

public abstract class Bm extends ND {
    protected Kt stack = Kt.bcE();
    protected Vec3f cUp = new Vec3f();
    protected Vec3f cUq = new Vec3f();
    protected adJ BA;

    protected Bm(adJ var1) {
        this.BA = var1;
    }

    protected void growBoundingBox(ajK var1, BoundingBox var2) {
        Vec3f var3 = new Vec3f();
        Vec3f var4 = new Vec3f();
        this.BA.x(var3, var4);
        var2.bP(var3);
        var2.bQ(var4);
    }

    public Vec3f localGetSupportingVertex(Vec3f var1) {
        this.stack.bcF();

        Vec3f var8;
        try {
            Vec3f var2 = (Vec3f) this.stack.bcH().get();
            Vec3f var3 = (Vec3f) this.stack.bcH().get();
            xf_g var4 = (xf_g) this.stack.bcJ().get();
            var4.setIdentity();
            b var5 = new b(var1, var4);
            Vec3f var6 = this.stack.bcH().h(1.0E30F, 1.0E30F, 1.0E30F);
            var2.negate(var6);
            this.a(var5, var2, var6);
            var3.set(var5.cnE());
            var8 = (Vec3f) this.stack.bcH().aq(var3);
        } finally {
            this.stack.bcG();
        }

        return var8;
    }

    public Vec3f localGetSupportingVertexWithoutMargin(Vec3f var1) {
        assert false;

        return this.localGetSupportingVertex(var1);
    }

    public void aPc() {
        this.stack.bcH().push();

        try {
            for (int var1 = 0; var1 < 3; ++var1) {
                Vec3f var2 = this.stack.bcH().h(0.0F, 0.0F, 0.0F);
                JL.a(var2, var1, 1.0F);
                Vec3f var3 = this.stack.bcH().ac(this.localGetSupportingVertex(var2));
                JL.a(this.cUq, var1, JL.b(var3, var1) + this.collisionMargin);
                JL.a(var2, var1, -1.0F);
                var3.set(this.localGetSupportingVertex(var2));
                JL.a(this.cUp, var1, JL.b(var3, var1) - this.collisionMargin);
            }
        } finally {
            this.stack.bcH().pop();
        }

    }

    public void getAabb(xf_g var1, Vec3f var2, Vec3f var3) {
        this.stack.bcF();

        try {
            Vec3f var4 = (Vec3f) this.stack.bcH().get();
            Vec3f var5 = (Vec3f) this.stack.bcH().get();
            var5.sub(this.cUq, this.cUp);
            var5.scale(0.5F);
            Vec3f var6 = (Vec3f) this.stack.bcH().get();
            var6.add(this.cUq, this.cUp);
            var6.scale(0.5F);
            ajD var7 = this.stack.bcK().g(var1.bFF);
            rS.b(var7);
            Vec3f var8 = this.stack.bcH().ac(var6);
            var1.G(var8);
            Vec3f var9 = (Vec3f) this.stack.bcH().get();
            var7.getRow(0, var4);
            var9.x = var4.dot(var5);
            var7.getRow(1, var4);
            var9.y = var4.dot(var5);
            var7.getRow(2, var4);
            var9.z = var4.dot(var5);
            var9.add(this.stack.bcH().h(this.getMargin(), this.getMargin(), this.getMargin()));
            var2.sub(var8, var9);
            var3.add(var8, var9);
        } finally {
            this.stack.bcG();
        }

    }

    public void a(aTC_q var1, Vec3f var2, Vec3f var3) {
        a var4 = new a(var1, var2, var3);
        this.BA.a(var4, var2, var3);
    }

    public void calculateLocalInertia(float var1, Vec3f var2) {
        assert false;

        var2.set(0.0F, 0.0F, 0.0F);
    }

    public Vec3f getLocalScaling() {
        return this.BA.getScaling();
    }

    public void setLocalScaling(Vec3f var1) {
        this.BA.setScaling(var1);
        this.aPc();
    }

    public adJ aQF() {
        return this.BA;
    }

    private static class a implements SP {
        public final Vec3f cam = new Vec3f();
        public final Vec3f can = new Vec3f();
        public aTC_q BB;

        public a(aTC_q var1, Vec3f var2, Vec3f var3) {
            this.BB = var1;
            this.cam.set(var2);
            this.can.set(var3);
        }

        public void b(Vec3f[] var1, int var2, int var3) {
            if (aRO.a(var1, this.cam, this.can)) {
                this.BB.a(var1, var2, var3);
            }

        }
    }

    private class b implements aTC_q {
        public final xf_g cIf = new xf_g();
        public final Vec3f gir = new Vec3f();
        private final Vec3f gip = new Vec3f(0.0F, 0.0F, 0.0F);
        public float giq = -1.0E30F;

        public b(Vec3f var2, xf_g var3) {
            this.cIf.a(var3);
            rS.a(this.gir, var2, this.cIf.bFF);
        }

        public void a(Vec3f[] var1, int var2, int var3) {
            for (int var4 = 0; var4 < 3; ++var4) {
                float var5 = this.gir.dot(var1[var4]);
                if (var5 > this.giq) {
                    this.giq = var5;
                    this.gip.set(var1[var4]);
                }
            }

        }

        public Vec3f cnD() {
            Bm.this.stack.bcH().push();

            Vec3f var3;
            try {
                Vec3f var1 = Bm.this.stack.bcH().ac(this.gip);
                this.cIf.G(var1);
                var3 = (Vec3f) Bm.this.stack.bcH().aq(var1);
            } finally {
                Bm.this.stack.bcH().pop();
            }

            return var3;
        }

        public Vec3f cnE() {
            return this.gip;
        }
    }
}
