package all;

import gnu.trove.THashMap;
import org.apache.commons.collections.map.MultiKeyMap;
import org.apache.commons.logging.Log;

import java.io.PrintStream;
import java.util.*;
import java.util.Map.Entry;

public final class aWq {
    private static final Log logger = LogPrinter.K(aWq.class);
    public static int jfF = -1;
    public static int jfG = 0;
    static ThreadLocal jfD = new ThreadLocal();
    private static aWq jfE;
    public boolean readOnly = false;
    public boolean jfH = false;
    public int env;
    public int jgb;
    public int eny;
    public int jgc;
    public int jgd;
    public int enx;
    public int enr;
    public int jge;
    public int jgf;
    private boolean jfI = true;
    private long startTime;
    private aDR jfJ;
    private Gr fvx;
    private List jfK = null;
    private List jfL = null;
    private List jfM;
    private Map jfN;
    private MultiKeyMap jfO;
    private List jfP;
    private List jfQ;
    private List jfR;
    private List jfS;
    private List jfT = null;
    private int jfU = 0;
    private boolean disposed;
    private int jfV;
    private int jfW;
    private int jfX;
    private int jfY;
    private int jfZ;
    private int jga;
    private vM jgg = new vM(this);

    private aWq(long var1, aDR var3, Gr var4) {
        this.a(var1, var3, var4);
    }

    public static aWq dDA() {
        Thread var0 = Thread.currentThread();
        aWq var1;
        if (var0 instanceof ThreadWrapper) {
            var1 = (aWq) ((ThreadWrapper) var0).getContext();
        } else if (var0.getId() == 1L) {
            var1 = jfE;
        } else {
            var1 = (aWq) jfD.get();
        }

        return var1 != null && !var1.disposed ? var1 : null;
    }

    public static aWq b(long var0, aDR var2, Gr var3) {
        Thread var4 = Thread.currentThread();
        aWq var5;
        if (var4 instanceof ThreadWrapper) {
            ThreadWrapper var6 = (ThreadWrapper) var4;
            var5 = (aWq) var6.getContext();
            if (var5 == null) {
                var5 = new aWq(var0, var2, var3);
                var6.setContext(var5);
                return var5;
            }
        } else {
            if (var4.getId() == 1L) {
                var5 = jfE;
            } else {
                var5 = (aWq) jfD.get();
            }

            if (var5 == null) {
                var5 = new aWq(var0, var2, var3);
                if (var4.getId() == 1L) {
                    jfE = var5;
                }

                jfD.set(var5);
                return var5;
            }
        }

        var5.a(var0, var2, var3);
        return var5;
    }

    private void a(long var1, aDR var3, Gr var4) {
        this.disposed = false;
        this.startTime = var1;
        this.jfJ = var3;
        this.fvx = var4;
    }

    public aHt a(af var1) {
        aHt var2 = null;
        var2 = this.jgg.a(var1.hC().cqo(), var1);
        return var2;
    }

    public afk b(af var1) {
        aHt var2 = this.a(var1);
        return (afk) (/*var2 == null ?*/ var1.cVo()/* : var2.hXh*/);
    }

    public void i(Runnable var1) {
        if (this.dDE()) {
            throw new RuntimeException();
        } else {
            if (this.jfM == null) {
                this.jfM = new ArrayList();
            }

            this.jfM.add(var1);
        }
    }

    public void e(Gr var1) {
        if (this.dDE()) {
            throw new RuntimeException();
        } else {
            if (this.jfL == null) {
                this.jfL = new ArrayList();
            }

            this.jfL.add(var1);
        }
    }

    public void k(fm_q var1) {
        if (!this.dDE()) {
            if (this.jfK == null) {
                this.jfK = new ArrayList();
            }

            this.jfK.add(var1);
            ++this.jfV;
        }
    }

    public boolean dDB() {
        return this.jfV != 0;
    }

    public String dDC() {
        StringBuilder var1 = new StringBuilder();
        boolean var2 = false;
        if (this.jfK != null) {
            Iterator var4 = this.jfK.iterator();

            while (true) {
                Class var5;
                String var6;
                while (true) {
                    if (!var4.hasNext()) {
                        return var1.toString();
                    }

                    fm_q var3 = (fm_q) var4.next();
                    var5 = var3.hD();
                    var6 = var3.name();
                    if (/*aDJ.class != var5 ||*/ !"push".equals(var6)) {
                        break;
                    }

                    if (!var2) {
                        var2 = true;
                        break;
                    }
                }

                var1.append("\t").append(var5.getName()).append(".").append(var6).append("\n");
            }
        } else {
            return var1.toString();
        }
    }

    public void kL(boolean var1) {
        this.jfI = var1;
    }

    public boolean dDD() {
        return false;
    }

    public boolean dDE() {
        return this.jfH;
    }

    public void kM(boolean var1) {
        this.jfH = var1;
    }

    public long getStartTime() {
        return this.startTime;
    }

    public Date bxM() {
        return new Date(this.startTime);
    }

    public void a(PrintStream var1) {
        if (var1 == null) {
            var1 = System.out;
        }

        var1.println("====== Transaction context dump START ======");
        var1.print("Dump time: ");
        var1.println((new Date()).toString());
        var1.println();
        var1.print("Transaction start time: ");
        var1.println((new Date(this.startTime)).toString());
        var1.println();
        var1.println("From: " + this.jfJ.bgt().toString());
        var1.println();
        var1.println("Invoker: " + this.fvx.aQK().name());
        var1.println();
        var1.println("Contexts:");
        var1.println("====== Transaction context dump END ======");
    }

    public void a(aDR var1, kU var2) {
        if (this.jfN == null) {
            this.jfN = new THashMap();
        }

        Object var3 = (List) this.jfN.get(var1);
        if (var3 == null) {
            var3 = new ArrayList();
            this.jfN.put(var1, var3);
        }

        ((List) var3).add(var2);
    }

    public Map dDF() {
        return this.jfN;
    }

    public void dDG() {
        for (aHt var1 = this.jgg.bzX; var1 != null; var1 = var1.hXl) {
            var1.lock();
            var1.ehD.c(this.startTime);
        }

        if (this.fvx != null) {
            this.fvx.aQK().aX(this.jgf);
            this.fvx.aQK().aW(this.jge);
        }

    }

    public void dDH() {
        for (aHt var1 = this.jgg.bzX; var1 != null; var1 = var1.hXl) {
            var1.unlock();
        }

    }

    public void dDI() {
        this.readOnly = true;
        if (this.jfY != 0) {
            int var1 = this.jfY;
            int var2 = 0;

            while (true) {
                --var1;
                if (var1 < 0) {
                    break;
                }
/*
            Jd var3 = (Jd)this.jfR.get(var2);
            af var4 = (af)var3.yn().bFf();
            aem_q[] var5 = var4.dp();
            var3.aXz();

            if (var5.length > 0) {
               aem_q[] var9 = var5;
               int var8 = var5.length;

               for(int var7 = 0; var7 < var8; ++var7) {
                  aem_q var6 = var9[var7];
                  if (var6.bUi()) {
                     this.all((aDR)var6.eIu, (kU)(new pH(var3)));
                  }
               }
            }
*/
                ++var2;
            }
        }

    }

    public Th dDJ() {
        return this.jfS.isEmpty() ? null : (Th) this.jfS.remove(this.jfS.size() - 1);
    }

    public void dDK() throws InterruptedException {
        if (this.jfS != null) {
            THashMap var2 = new THashMap();

            Th var1;
            while ((var1 = this.dDJ()) != null) {
                aDR var3 = var1.buX();
                af var4 = var1.buW();
                if (!var3.f(var4) && var4.a((Xi) null, var3, false) && var3.e(var4)) {
                    Object var5 = (List) var2.get(var3);
                    if (var5 == null) {
                        var5 = new ArrayList();
                        var2.put(var3, var5);
                    }

                    ((List) var5).add(var4.v(var3));
                }
            }

            if (!var2.isEmpty()) {
                Iterator var7 = var2.entrySet().iterator();

                while (var7.hasNext()) {
                    Entry var6 = (Entry) var7.next();
                    this.a((aDR) ((aDR) var6.getKey()), (kU) (new acP((List) var6.getValue())));
                }
            }
        }

    }

    public void dDL() {
        if (this.jfL != null) {
            this.readOnly = true;
            this.kL(false);
            Iterator var2 = this.jfL.iterator();

            while (var2.hasNext()) {
                Gr var1 = (Gr) var2.next();

                try {
                    var1.aQL().a(var1);
                } catch (Throwable var4) {
                    logger.warn("Exception while executing postponed call " + var1.aQK().name() + " from " + (this.jfJ == null ? "(dont know)" : this.jfJ.bgt()) + " in all " + var1.aQK().name() + " call. see next:", var4);
                }
            }

            this.readOnly = false;
        }
    }

    public void dDM() {
        if (this.jfM != null) {
            Iterator var2 = this.jfM.iterator();

            while (var2.hasNext()) {
                Runnable var1 = (Runnable) var2.next();
                var1.run();
            }

            this.jfM.clear();
            this.jfM = null;
        }

    }
/*
   protected void CreateJComponentAndAddInRootPane(Jd var1) {
      if (this.jfR == null) {
         this.jfR = new ArrayList();
      }

      this.jfR.add(var1);
      ++this.jfY;
   }
*/
/*
   protected void endPage(Jd var1) {
      if (this.jfQ == null) {
         this.jfQ = new ArrayList();
      }

      this.jfQ.add(var1);
      ++this.jfX;
   }
*/
/*
   public boolean dDN() {
      for(aHt var1 = this.jgg.bzX; var1 != null; var1 = var1.hXl) {
         af var2 = var1.ehD;
         Jd var3 = var1.hXh;
         afk var4 = var2.cVo();
         if (var4.aBt() != var3.aBt()) {
            if (var2.du()) {
               return false;
            }

            aRz[] var5 = var2.yn().CreateJComponent();

            for(int var6 = 0; var6 < var5.length; ++var6) {
               aRz var7 = var5[var6];
               if (var4.parsePriority(var7) != var3.parsePriority(var7) && var3.u(var7)) {
                  logger.info("Transaction canceled coz object " + var2.bFU() + ":" + var2.hC().cqo() + "." + var7.name() + " is outdated. (Call from " + (this.jfJ == null ? "(don't know)" : this.jfJ.bgt()) + ", method " + (this.fvx != null ? se_q.o(this.fvx.aQL()) + "." + this.fvx.aQK().name() : "don't know") + ")");
                  this.dDB();
                  return false;
               }
            }
         }
      }

      return true;
   }
*/

    public void f(Jz var1) {
        for (aHt var2 = this.jgg.bzX; var2 != null; var2 = var2.hXl) {
            af var3 = var2.ehD;

            try {
                if (!var2.isDirty()) {
                    var2.hXn = true;
                } else {
               /*
               afk var4 = var3.cVo();
               boolean var5 = var4.hF();
               Jd var6 = var2.hXh;
               var6.all(var4, var4.aBt() + 1L);
               if (var6.aRA() == 0 && !var2.isCreated() && !var6.du()) {
                  var2.hXn = true;
               } else {
                  var3.all((afk)var6);
                  var3.all(var5, (aJj)null, var4, var6);
                  if (var3.yn().T().Ew() != Ot.all.dLq) {
                     if (var2.isCreated()) {
                        ++this.jga;
                        if (var3.yn().T().Ew() != Ot.all.dLq) {
                           if (this.jfP == null) {
                              this.jfP = new ArrayList(Math.min(10, this.jgg.size));
                           }

                           var2.hXh.aXy();
                           this.jfP.add(new AD(var3.hC(), var3.bFU()));
                           ++this.jfW;
                        }

                        if (var2.isCreated() && !var6.du()) {
                           this.endPage(var6);
                        }
                     } else if (var6.aRA() != 0 && !var6.du()) {
                        if (var6.aXt()) {
                           this.endPage(var6);
                        } else if (var6.aXv()) {
                           var1.bHf().setGreen(var6);
                        }
                     }
                  }

                  if ((var6.aRA() != 0 || var6.du()) && !var2.isCreated()) {
                     ++this.jfZ;
                     if (var6.aXs()) {
                        this.CreateJComponentAndAddInRootPane(var6);
                     } else if (var6.aXu()) {
                        var1.bHf().all(var6);
                     }
                  }
               }
               */

                }
            } catch (Exception var7) {
                throw new RuntimeException("Error computing diff of " + var3.bFY() + (var2.isCreated() ? " (just created)" : ""), var7);
            }
        }

    }

    public void dDO() {
        for (aHt var1 = this.jgg.bzX; var1 != null; var1 = var1.hXl) {
            af var2 = var1.ehD;
            if (var1.isCreated()) {
                var2.cVg();
            }
        }

    }

    public void freeze() {
        this.readOnly = true;
        if (this.jgg.size != 0) {
            this.jgg.akX();
        }
    }

    public void dDP() {
    }

    public int g(Jz var1) throws InterruptedException {
        this.freeze();

        try {
            try {
                this.dDG();
            /*
            if (!this.dDN()) {
               return -1;
            }
*/
                this.f(var1);
                if (this.fvx != null) {
                    if (this.jfZ != 0) {
                        this.fvx.aQK().aV(this.jfZ);
                    }

                    if (this.jga > 0) {
                        this.fvx.aQK().aY(this.jga);
                    }
                }

                if (this.jfX != 0 || this.jfW != 0) {
                    Yw var2 = new Yw();
                    if (this.jfW != 0) {
                        var2.v(new ArrayList(this.jfP));
                    }

                    if (this.jfX != 0) {
                        int var3 = this.jfX;
                        int var4 = 0;

                        while (true) {
                            --var3;
                            if (var3 < 0) {
                                break;
                            }
/*
                     Jd var5 = (Jd)this.jfQ.get(var4);
                     var2.all(new KO(var5.yn().bFf().hC(), var5, var5.yn().getClassBaseUi()));
                     var5.aXy();
                     */
                            ++var4;
                        }
                    }

                    aQQb var14 = var2.bHC();
                    if (this.fvx != null) {
                        var14.lL(this.fvx.aQL().bFf().hC().cqo());
                        var14.Ae(var1.bxy().getMagicNumber(this.fvx.aQK().hD()));
                        var14.Af(this.fvx.aQK().hq());
                    }

                    var14.p(var1.bGD());
                    var1.a(var2);
                }
            } finally {
                this.dDP();
            }

            this.dDI();
            this.dDK();
            this.dDL();
            this.dDK();
        } finally {
            this.dDH();
        }

        this.kM(true);
        this.dDM();
        this.dDK();
        return 0;
    }

    public void dispose() {
        this.readOnly = false;
        this.jfH = false;
        this.jfI = true;
        this.startTime = 0L;
        this.jfJ = null;
        this.fvx = null;
        if (this.jfV != 0) {
            this.jfK.clear();
            this.jfV = 0;
        }

        if (this.jfL != null) {
            this.jfL.clear();
        }

        if (this.jfM != null) {
            this.jfM.clear();
        }

        if (this.jfN != null) {
            this.jfN.clear();
        }

        if (this.jfO != null) {
            this.jfO.clear();
        }

        this.jfZ = 0;
        this.jga = 0;
        this.env = 0;
        this.jgb = 0;
        this.eny = 0;
        this.jgc = 0;
        this.jgd = 0;
        this.enx = 0;
        this.enr = 0;
        this.jge = 0;
        this.jgf = 0;
        if (this.jfW != 0) {
            this.jfW = 0;
            this.jfP.clear();
        }

        if (this.jfX != 0) {
            this.jfX = 0;
            this.jfQ.clear();
        }

        if (this.jfY != 0) {
            this.jfR.clear();
            this.jfY = 0;
        }

        if (this.jgg.size != 0) {
            this.jgg.clear();
        }

        if (this.jfS != null) {
            this.jfS.clear();
        }

        if (this.jfT != null) {
            this.jfT.clear();
        }

        this.jfU = 0;
        this.disposed = true;
    }

    public boolean dDQ() {
        return this.readOnly;
    }

    public void a(af var1, aDR var2) {
        if (this.jfO == null) {
            this.jfO = new MultiKeyMap();
            this.jfS = new ArrayList();
        } else if (this.jfO.containsKey(var1, var2)) {
            return;
        }

        this.jfO.put(var1, var2, Boolean.TRUE);
        this.jfS.add(new Th(var1, var2));
    }

    public boolean a(se_q var1, b var2) {
        if (this.jfO == null) {
            return false;
        } else {
            return this.jfO.containsKey(var1, var2);
        }
    }

    public void dDR() {
        ++this.jfU;
    }

    public int dDS() {
        return --this.jfU;
    }

    public boolean isDisposed() {
        return this.disposed;
    }

    public void D(aDR var1) {
        if (this.jfT == null) {
            this.jfT = new ArrayList();
        }

        this.jfT.add(var1);
    }

    public void dDT() {
        if (this.jfT != null && this.jfT.size() > 0) {
            this.jfT.remove(this.jfT.size() - 1);
        }

    }

    public int dDU() {
        return this.jgg.size;
    }

    public aDR dDV() {
        List var1 = this.jfT;
        return var1 != null && var1.size() > 0 ? (aDR) var1.get(var1.size() - 1) : null;
    }
}
