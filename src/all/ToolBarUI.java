package all;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicToolBarUI;
import java.awt.*;

/**
 * ToolBarUI
 */
public class ToolBarUI extends BasicToolBarUI implements aIh {
    private ComponentManager Rp;

    public ToolBarUI(JToolBar var1) {
        this.Rp = new ComponentManager("toolbar", var1);
        var1.setOpaque(false);
    }

    public static ComponentUI createUI(JComponent var0) {
        return new ToolBarUI((JToolBar) var0);
    }

    public ComponentManager wy() {
        return this.Rp;
    }

    public Dimension getPreferredSize(JComponent var1) {
        return this.wy().c(var1, super.getPreferredSize(var1));
    }

    public Dimension getMinimumSize(JComponent var1) {
        return this.wy().b(var1, super.getMinimumSize(var1));
    }

    public Dimension getMaximumSize(JComponent var1) {
        return this.wy().a(var1, super.getMaximumSize(var1));
    }

    public void paint(Graphics var1, JComponent var2) {
        super.paint(var1, var2);
    }

    public void update(Graphics var1, JComponent var2) {
        super.paint(var1, var2);
    }

    /**
     * TODO похлже это public pv wy()
     *
     * @return Графический элемент
     */
    @Override
    public aeK wz() {
        return this.Rp;
    }
}
