package all;

import javax.swing.text.Element;
import javax.swing.text.html.HTML.Attribute;
import javax.swing.text.html.ImageView;
import java.awt.*;

public class Ve extends ImageView {
    public Ve(Element var1) {
        super(var1);
    }

    public Image getImage() {
        String var1 = (String) this.getElement().getAttributes().getAttribute(Attribute.SRC);
        if (var1 == null) {
            return null;
        } else {
            try {
                return BaseUItegXML.thisClass.getLoaderImage().getImage(var1);
            } catch (IllegalArgumentException var2) {
                return null;
            }
        }
    }
}
