package all;

import java.lang.reflect.Type;

public interface fm_q extends cc {
    int SKIP = 0;
    int JR = 1;
    int JS = 2;
    int JT = 3;

    boolean pm();

    boolean pn();

    boolean po();

    boolean pp();

    boolean pq();

    boolean pr();

    boolean ps();

    boolean pt();

    boolean pu();

    boolean pv();

    boolean pw();

    boolean px();

    boolean hm();

    boolean py();

    boolean isBlocking();

    int pz();

    int pA();

    int hq();

    boolean pB();

    boolean pC();

    boolean pD();

    boolean pE();

    boolean pF();

    boolean pG();

    boolean pH();

    boolean pI();

    // pc.all pJ();

    String getDisplayName();

    boolean isEditable();

    String pK();

    Class getReturnType();

    Class[] getParameterTypes();

    boolean pL();

    Kd[] pM();

    Object pN();

//   horizon pO();

    Class hD();

    void A(long var1);

    void B(long var1);

    int getExecuteCount();

    long getExecuteTime();

    long getTransactionTime();

    int getTransactionCount();

    void pP();

    void aK(int var1);

    void aL(int var1);

    void aM(int var1);

    void aN(int var1);

    void aO(int var1);

    void aP(int var1);

    void aQ(int var1);

    void aR(int var1);

    void aS(int var1);

    void aT(int var1);

    void aU(int var1);

    int getInfraMethodsCalled();

    int pQ();

    int pR();

    int pS();

    int getObjectsTouchedInPostponed();

    int getObjectsTouchedInTransaction();

    int pT();

    int getTransactionalGets();

    int getTransactionalGetsInPostponed();

    int getTransactionalMethodsCalled();

    int getTransactionalSets();

    int getTransactionalSetsInPostponed();

    int getObjectsChangedInTransaction();

    int getReadLocksInTransaction();

    int getWriteLocksInTransaction();

    int getObjectsCreatedInTransaction();

    void aV(int var1);

    void aW(int var1);

    void aX(int var1);

    void aY(int var1);

    Type getGenericReturnType();

    void pU();

    void pV();

    long pW();

    long pX();

    int pY();

    boolean pZ();

    long qa();
}
