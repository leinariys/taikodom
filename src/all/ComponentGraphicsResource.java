package all;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.*;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Список
 * class pv
 * ключ = значение
 * Список графический компонент = обработчик событий
 */
public class ComponentGraphicsResource extends AbstractMap implements Serializable, ConcurrentMap {

    static final i cIk;
    static final i cIl;
    static final int DEFAULT_INITIAL_CAPACITY = 16;//НАЧАЛЬНАЯ ПОТЕНЦИАЛ ПО УМОЛЧАНИЮ
    static final float DEFAULT_LOAD_FACTOR = 0.75F;//ФАКТОР ЗАГРУЗКИ ПО УМОЛЧАНИЮ
    static final int cIm = 16;
    static final int MAXIMUM_CAPACITY = 1073741824;//МАКСИМАЛЬНАЯ ЕМКОСТЬ | 2^30 |  1073741824 bytes = 1048567 KB = 1024 MB = 1GB
    static final int cIn = 65536;// 2^16
    static final int cIo = 2;

    static {
        cIk = i.eeK;
        cIl = i.eeJ;
    }

    final int cIp;
    final int cIq;
    final j[] cIr;
    boolean cIs;
    transient Set keySet;
    transient Set entrySet;
    transient Collection values;

    public ComponentGraphicsResource(int var1, float var2, int var3, i var4, i var5, EnumSet var6) {
        if (var2 > 0.0F && var1 >= 0 && var3 > 0) {
            if (var3 > cIn) {
                var3 = cIn;
            }

            int var7 = 0;

            int var8;
            for (var8 = 1; var8 < var3; var8 <<= 1) {
                ++var7;
            }

            this.cIq = 32 - var7;
            this.cIp = var8 - 1;
            this.cIr = j.no(var8);
            if (var1 > MAXIMUM_CAPACITY) {
                var1 = MAXIMUM_CAPACITY;
            }

            int var9 = var1 / var8;
            if (var9 * var8 < var1) {
                ++var9;
            }

            int var10;
            for (var10 = 1; var10 < var9; var10 <<= 1) {
                ;
            }

            this.cIs = var6 != null && var6.contains(h.dRd);

            for (int var11 = 0; var11 < this.cIr.length; ++var11) {
                this.cIr[var11] = new j(var10, var2, var4, var5, this.cIs);
            }

        } else {
            throw new IllegalArgumentException();
        }
    }

    public ComponentGraphicsResource(int var1, float var2, int var3) {
        this(var1, var2, var3, cIk, cIl, (EnumSet) null);
    }

    public ComponentGraphicsResource(int var1, float var2) {
        this(var1, var2, 16);
    }

    public ComponentGraphicsResource(int var1, i var2, i var3) {
        this(var1, DEFAULT_LOAD_FACTOR, 16, var2, var3, (EnumSet) null);
    }

    public ComponentGraphicsResource(int var1) {
        this(var1, DEFAULT_LOAD_FACTOR, 16);
    }

    public ComponentGraphicsResource() {
        this(16, DEFAULT_LOAD_FACTOR, 16);
    }

    public ComponentGraphicsResource(Map var1) {
        this(Math.max((int) ((float) var1.size() / DEFAULT_LOAD_FACTOR) + 1, 16), DEFAULT_LOAD_FACTOR, 16);
        this.putAll(var1);
    }

    private static int hash(int var0) {
        var0 += var0 << 15 ^ -12931;
        var0 ^= var0 >>> 10;
        var0 += var0 << 3;
        var0 ^= var0 >>> 6;
        var0 += (var0 << 2) + (var0 << 14);
        return var0 ^ var0 >>> 16;
    }

    final j gO(int var1) {
        return this.cIr[var1 >>> this.cIq & this.cIp];
    }

    private int M(Object var1) {
        return hash(this.cIs ? System.identityHashCode(var1) : var1.hashCode());
    }

    public boolean isEmpty() {
        j[] var1 = this.cIr;
        int[] var2 = new int[var1.length];
        int var3 = 0;

        int var4;
        for (var4 = 0; var4 < var1.length; ++var4) {
            if (var1[var4].count != 0) {
                return false;
            }

            var3 += var2[var4] = var1[var4].modCount;
        }

        if (var3 != 0) {
            for (var4 = 0; var4 < var1.length; ++var4) {
                if (var1[var4].count != 0 || var2[var4] != var1[var4].modCount) {
                    return false;
                }
            }
        }

        return true;
    }

    public int size() {
        j[] var1 = this.cIr;
        long var2 = 0L;
        long var4 = 0L;
        int[] var6 = new int[var1.length];

        int var7;
        for (var7 = 0; var7 < 2; ++var7) {
            var4 = 0L;
            var2 = 0L;
            int var8 = 0;

            int var9;
            for (var9 = 0; var9 < var1.length; ++var9) {
                var2 += (long) var1[var9].count;
                var8 += var6[var9] = var1[var9].modCount;
            }

            if (var8 != 0) {
                for (var9 = 0; var9 < var1.length; ++var9) {
                    var4 += (long) var1[var9].count;
                    if (var6[var9] != var1[var9].modCount) {
                        var4 = -1L;
                        break;
                    }
                }
            }

            if (var4 == var2) {
                break;
            }
        }

        if (var4 != var2) {
            var2 = 0L;

            for (var7 = 0; var7 < var1.length; ++var7) {
                var1[var7].lock();
            }

            for (var7 = 0; var7 < var1.length; ++var7) {
                var2 += (long) var1[var7].count;
            }

            for (var7 = 0; var7 < var1.length; ++var7) {
                var1[var7].unlock();
            }
        }

        return var2 > 2147483647L ? Integer.MAX_VALUE : (int) var2;
    }

    public Object get(Object var1) {
        int var2 = this.M(var1);
        return this.gO(var2).get(var1, var2);
    }

    public boolean containsKey(Object var1) {
        int var2 = this.M(var1);
        return this.gO(var2).a(var1, var2);
    }

    public boolean containsValue(Object var1) {
        if (var1 == null) {
            throw new NullPointerException();
        } else {
            j[] var2 = this.cIr;
            int[] var3 = new int[var2.length];

            int var4;
            int var5;
            int var7;
            for (var4 = 0; var4 < 2; ++var4) {
                var5 = 0;

                int var10000;
                for (int var6 = 0; var6 < var2.length; ++var6) {
                    var10000 = var2[var6].count;
                    var5 += var3[var6] = var2[var6].modCount;
                    if (var2[var6].containsValue(var1)) {
                        return true;
                    }
                }

                boolean var11 = true;
                if (var5 != 0) {
                    for (var7 = 0; var7 < var2.length; ++var7) {
                        var10000 = var2[var7].count;
                        if (var3[var7] != var2[var7].modCount) {
                            var11 = false;
                            break;
                        }
                    }
                }

                if (var11) {
                    return false;
                }
            }

            for (var4 = 0; var4 < var2.length; ++var4) {
                var2[var4].lock();
            }

            boolean var10 = false;

            try {
                for (var5 = 0; var5 < var2.length; ++var5) {
                    if (var2[var5].containsValue(var1)) {
                        var10 = true;
                        break;
                    }
                }
            } finally {
                for (var7 = 0; var7 < var2.length; ++var7) {
                    var2[var7].unlock();
                }

            }

            return var10;
        }
    }

    public boolean contains(Object var1) {
        return this.containsValue(var1);
    }

    public Object put(Object var1, Object var2) {
        if (var2 == null) {
            throw new NullPointerException();
        } else {
            int var3 = this.M(var1);
            return this.gO(var3).a(var1, var3, var2, false);
        }
    }

    public Object putIfAbsent(Object var1, Object var2) {
        if (var2 == null) {
            throw new NullPointerException();
        } else {
            int var3 = this.M(var1);
            return this.gO(var3).a(var1, var3, var2, true);
        }
    }

    public void putAll(Map var1) {
        Iterator var3 = var1.entrySet().iterator();

        while (var3.hasNext()) {
            Entry var2 = (Entry) var3.next();
            this.put(var2.getKey(), var2.getValue());
        }

    }

    public Object remove(Object var1) {
        int var2 = this.M(var1);
        return this.gO(var2).b(var1, var2, (Object) null, false);
    }

    public boolean remove(Object var1, Object var2) {
        int var3 = this.M(var1);
        if (var2 == null) {
            return false;
        } else {
            return this.gO(var3).b(var1, var3, var2, false) != null;
        }
    }

    public boolean replace(Object var1, Object var2, Object var3) {
        if (var2 != null && var3 != null) {
            int var4 = this.M(var1);
            return this.gO(var4).a(var1, var4, var2, var3);
        } else {
            throw new NullPointerException();
        }
    }

    public Object replace(Object var1, Object var2) {
        if (var2 == null) {
            throw new NullPointerException();
        } else {
            int var3 = this.M(var1);
            return this.gO(var3).a(var1, var3, var2);
        }
    }

    public void clear() {
        for (int var1 = 0; var1 < this.cIr.length; ++var1) {
            this.cIr[var1].clear();
        }

    }

    public void aHa() {
        for (int var1 = 0; var1 < this.cIr.length; ++var1) {
            this.cIr[var1].buA();
        }

    }

    public Set keySet() {
        Set var1 = this.keySet;
        return var1 != null ? var1 : (this.keySet = new k());
    }

    public Collection values() {
        Collection var1 = this.values;
        return var1 != null ? var1 : (this.values = new r());
    }

    public Set entrySet() {
        Set var1 = this.entrySet;
        return var1 != null ? var1 : (this.entrySet = new b());
    }

    public Enumeration keys() {
        return new e();
    }

    public Enumeration elements() {
        return new a();
    }

    private void writeObject(ObjectOutputStream var1) throws IOException {
        var1.defaultWriteObject();

        for (int var2 = 0; var2 < this.cIr.length; ++var2) {
            j var3 = this.cIr[var2];
            var3.lock();

            try {
                p[] var4 = var3.efF;

                for (int var5 = 0; var5 < var4.length; ++var5) {
                    for (p var6 = var4[var5]; var6 != null; var6 = var6.hWH) {
                        Object var7 = var6.key();
                        if (var7 != null) {
                            var1.writeObject(var7);
                            var1.writeObject(var6.value());
                        }
                    }
                }
            } finally {
                var3.unlock();
            }
        }

        var1.writeObject((Object) null);
        var1.writeObject((Object) null);
    }

    private void readObject(ObjectInputStream var1) throws IOException, ClassNotFoundException {
        var1.defaultReadObject();

        for (int var2 = 0; var2 < this.cIr.length; ++var2) {
            this.cIr[var2].a(new p[1]);
        }

        while (true) {
            Object var4 = var1.readObject();
            Object var3 = var1.readObject();
            if (var4 == null) {
                return;
            }

            this.put(var4, var3);
        }
    }

    public static enum h {
        dRd;
    }

    public static enum i {
        eeJ,
        eeK,
        eeL;
    }

    interface f {
        int Bi();

        Object Bj();
    }

    static final class p {
        final Object Ya;
        final int hash;
        final p hWH;
        volatile Object hWG;

        p(Object var1, int var2, p var3, Object var4, i var5, i var6, ReferenceQueue var7) {
            this.hash = var2;
            this.hWH = var3;
            this.Ya = this.a(var1, var5, var7);
            this.hWG = this.b(var4, var6, var7);
        }

        static final p[] yf(int var0) {
            return new p[var0];
        }

        final Object a(Object var1, i var2, ReferenceQueue var3) {
            if (var2 == i.eeK) {
                return new n(var1, this.hash, var3);
            } else {
                return var2 == i.eeL ? new l(var1, this.hash, var3) : var1;
            }
        }

        final Object b(Object var1, i var2, ReferenceQueue var3) {
            if (var2 == i.eeK) {
                return new o(var1, this.Ya, this.hash, var3);
            } else {
                return var2 == i.eeL ? new c(var1, this.Ya, this.hash, var3) : var1;
            }
        }

        final Object key() {
            return this.Ya instanceof Reference ? ((Reference) this.Ya).get() : this.Ya;
        }

        final Object value() {
            return this.aH(this.hWG);
        }

        final Object aH(Object var1) {
            return var1 instanceof Reference ? ((Reference) var1).get() : var1;
        }

        final void c(Object var1, i var2, ReferenceQueue var3) {
            this.hWG = this.b(var1, var2, var3);
        }
    }

    static final class j extends ReentrantLock implements Serializable {
        private static final long serialVersionUID = 2249069246763182397L;
        final float loadFactor;
        final i efH;
        final i efI;
        final boolean cIs;
        transient volatile int count;
        transient int modCount;
        transient int threshold;
        transient volatile p[] efF;
        transient volatile ReferenceQueue efG;

        j(int var1, float var2, i var3, i var4, boolean var5) {
            this.loadFactor = var2;
            this.efH = var3;
            this.efI = var4;
            this.cIs = var5;
            this.a(p.yf(var1));
        }

        static final j[] no(int var0) {
            return new j[var0];
        }

        private boolean f(Object var1, Object var2) {
            return this.cIs ? var1 == var2 : var1.equals(var2);
        }

        void a(p[] var1) {
            this.threshold = (int) ((float) var1.length * this.loadFactor);
            this.efF = var1;
            this.efG = new ReferenceQueue();
        }

        p np(int var1) {
            p[] var2 = this.efF;
            return var2[var1 & var2.length - 1];
        }

        p a(Object var1, int var2, p var3, Object var4) {
            return new p(var1, var2, var3, var4, this.efH, this.efI, this.efG);
        }

        Object a(p var1) {
            this.lock();

            Object var3;
            try {
                this.buA();
                var3 = var1.value();
            } finally {
                this.unlock();
            }

            return var3;
        }

        Object get(Object var1, int var2) {
            if (this.count != 0) {
                for (p var3 = this.np(var2); var3 != null; var3 = var3.hWH) {
                    if (var3.hash == var2 && this.f(var1, var3.key())) {
                        Object var4 = var3.hWG;
                        if (var4 != null) {
                            return var3.aH(var4);
                        }

                        return this.a(var3);
                    }
                }
            }

            return null;
        }

        boolean a(Object var1, int var2) {
            if (this.count != 0) {
                for (p var3 = this.np(var2); var3 != null; var3 = var3.hWH) {
                    if (var3.hash == var2 && this.f(var1, var3.key())) {
                        return true;
                    }
                }
            }

            return false;
        }

        boolean containsValue(Object var1) {
            if (this.count != 0) {
                p[] var2 = this.efF;
                int var3 = var2.length;

                for (int var4 = 0; var4 < var3; ++var4) {
                    for (p var5 = var2[var4]; var5 != null; var5 = var5.hWH) {
                        Object var6 = var5.hWG;
                        Object var7;
                        if (var6 == null) {
                            var7 = this.a(var5);
                        } else {
                            var7 = var5.aH(var6);
                        }

                        if (var1.equals(var7)) {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        boolean a(Object var1, int var2, Object var3, Object var4) {
            this.lock();

            try {
                this.buA();

                p var5;
                for (var5 = this.np(var2); var5 != null && (var5.hash != var2 || !this.f(var1, var5.key())); var5 = var5.hWH) {
                    ;
                }

                boolean var6 = false;
                if (var5 != null && var3.equals(var5.value())) {
                    var6 = true;
                    var5.c(var4, this.efI, this.efG);
                }

                boolean var8 = var6;
                return var8;
            } finally {
                this.unlock();
            }
        }

        Object a(Object var1, int var2, Object var3) {
            this.lock();

            Object var7;
            try {
                this.buA();

                p var4;
                for (var4 = this.np(var2); var4 != null && (var4.hash != var2 || !this.f(var1, var4.key())); var4 = var4.hWH) {
                    ;
                }

                Object var5 = null;
                if (var4 != null) {
                    var5 = var4.value();
                    var4.c(var3, this.efI, this.efG);
                }

                var7 = var5;
            } finally {
                this.unlock();
            }

            return var7;
        }

        Object a(Object var1, int var2, Object var3, boolean var4) {
            this.lock();

            Object var12;
            try {
                this.buA();
                int var5 = this.count;
                if (var5++ > this.threshold) {
                    int var6 = this.buz();
                    if (var6 > 0) {
                        this.count = (var5 -= var6) - 1;
                    }
                }

                p[] var15 = this.efF;
                int var7 = var2 & var15.length - 1;
                p var8 = var15[var7];

                p var9;
                for (var9 = var8; var9 != null && (var9.hash != var2 || !this.f(var1, var9.key())); var9 = var9.hWH) {
                    ;
                }

                Object var10;
                if (var9 != null) {
                    var10 = var9.value();
                    if (!var4) {
                        var9.c(var3, this.efI, this.efG);
                    }
                } else {
                    var10 = null;
                    ++this.modCount;
                    var15[var7] = this.a(var1, var2, var8, var3);
                    this.count = var5;
                }

                var12 = var10;
            } finally {
                this.unlock();
            }

            return var12;
        }

        int buz() {
            p[] var1 = this.efF;
            int var2 = var1.length;
            if (var2 >= MAXIMUM_CAPACITY) {
                return 0;
            } else {
                p[] var3 = p.yf(var2 << 1);
                this.threshold = (int) ((float) var3.length * this.loadFactor);
                int var4 = var3.length - 1;
                int var5 = 0;

                for (int var6 = 0; var6 < var2; ++var6) {
                    p var7 = var1[var6];
                    if (var7 != null) {
                        p var8 = var7.hWH;
                        int var9 = var7.hash & var4;
                        if (var8 == null) {
                            var3[var9] = var7;
                        } else {
                            p var10 = var7;
                            int var11 = var9;

                            p var12;
                            for (var12 = var8; var12 != null; var12 = var12.hWH) {
                                int var13 = var12.hash & var4;
                                if (var13 != var11) {
                                    var11 = var13;
                                    var10 = var12;
                                }
                            }

                            var3[var11] = var10;

                            for (var12 = var7; var12 != var10; var12 = var12.hWH) {
                                Object var16 = var12.key();
                                if (var16 == null) {
                                    ++var5;
                                } else {
                                    int var14 = var12.hash & var4;
                                    p var15 = var3[var14];
                                    var3[var14] = this.a(var16, var12.hash, var15, var12.value());
                                }
                            }
                        }
                    }
                }

                this.efF = var3;
                return var5;
            }
        }

        Object b(Object var1, int var2, Object var3, boolean var4) {
            this.lock();

            Object var16;
            try {
                if (!var4) {
                    this.buA();
                }

                int var5 = this.count - 1;
                p[] var6 = this.efF;
                int var7 = var2 & var6.length - 1;
                p var8 = var6[var7];

                p var9;
                for (var9 = var8; var9 != null && var1 != var9.Ya && (var4 || var2 != var9.hash || !this.f(var1, var9.key())); var9 = var9.hWH) {
                    ;
                }

                Object var10 = null;
                if (var9 != null) {
                    Object var11 = var9.value();
                    if (var3 == null || var3.equals(var11)) {
                        var10 = var11;
                        ++this.modCount;
                        p var12 = var9.hWH;

                        for (p var13 = var8; var13 != var9; var13 = var13.hWH) {
                            Object var14 = var13.key();
                            if (var14 == null) {
                                --var5;
                            } else {
                                var12 = this.a(var14, var13.hash, var12, var13.value());
                            }
                        }

                        var6[var7] = var12;
                        this.count = var5;
                    }
                }

                var16 = var10;
            } finally {
                this.unlock();
            }

            return var16;
        }

        final void buA() {
            f var1;
            while ((var1 = (f) this.efG.poll()) != null) {
                this.b(var1.Bj(), var1.Bi(), (Object) null, true);
            }

        }

        void clear() {
            if (this.count != 0) {
                this.lock();

                try {
                    p[] var1 = this.efF;

                    for (int var2 = 0; var2 < var1.length; ++var2) {
                        var1[var2] = null;
                    }

                    ++this.modCount;
                    this.efG = new ReferenceQueue();
                    this.count = 0;
                } finally {
                    this.unlock();
                }
            }

        }
    }

    static class d implements Serializable, Entry {
        private static final long serialVersionUID = -8499721149061103585L;
        private final Object key;
        private Object value;

        public d(Object var1, Object var2) {
            this.key = var1;
            this.value = var2;
        }

        public d(Entry var1) {
            this.key = var1.getKey();
            this.value = var1.getValue();
        }

        private static boolean eq(Object var0, Object var1) {
            return var0 == null ? var1 == null : var0.equals(var1);
        }

        public Object getKey() {
            return this.key;
        }

        public Object getValue() {
            return this.value;
        }

        public Object setValue(Object var1) {
            Object var2 = this.value;
            this.value = var1;
            return var2;
        }

        public boolean equals(Object var1) {
            if (!(var1 instanceof Entry)) {
                return false;
            } else {
                Entry var2 = (Entry) var1;
                return eq(this.key, var2.getKey()) && eq(this.value, var2.getValue());
            }
        }

        public int hashCode() {
            return (this.key == null ? 0 : this.key.hashCode()) ^ (this.value == null ? 0 : this.value.hashCode());
        }

        public String toString() {
            return this.key + "=" + this.value;
        }
    }

    static final class l extends SoftReference implements f {
        final int hash;

        l(Object var1, int var2, ReferenceQueue var3) {
            super(var1, var3);
            this.hash = var2;
        }

        public final int Bi() {
            return this.hash;
        }

        public final Object Bj() {
            return this;
        }
    }

    static final class c extends SoftReference implements f {
        final Object Ya;
        final int hash;

        c(Object var1, Object var2, int var3, ReferenceQueue var4) {
            super(var1, var4);
            this.Ya = var2;
            this.hash = var3;
        }

        public final int Bi() {
            return this.hash;
        }

        public final Object Bj() {
            return this.Ya;
        }
    }

    static final class n extends WeakReference implements f {
        final int hash;

        n(Object var1, int var2, ReferenceQueue var3) {
            super(var1, var3);
            this.hash = var2;
        }

        public final int Bi() {
            return this.hash;
        }

        public final Object Bj() {
            return this;
        }
    }

    static final class o extends WeakReference implements f {
        final Object Ya;
        final int hash;

        o(Object var1, Object var2, int var3, ReferenceQueue var4) {
            super(var1, var4);
            this.Ya = var2;
            this.hash = var3;
        }

        public final int Bi() {
            return this.hash;
        }

        public final Object Bj() {
            return this.Ya;
        }
    }

    final class q extends m implements Iterator {
        q() {
            super();
        }

        public Entry next() {
            p var1 = super.cIT();
            return ComponentGraphicsResource.this.new g(var1.key(), var1.value());
        }
    }

    final class b extends AbstractSet {
        public Iterator iterator() {
            return ComponentGraphicsResource.this.new q();
        }

        public boolean contains(Object var1) {
            if (!(var1 instanceof Entry)) {
                return false;
            } else {
                Entry var2 = (Entry) var1;
                Object var3 = ComponentGraphicsResource.this.get(var2.getKey());
                return var3 != null && var3.equals(var2.getValue());
            }
        }

        public boolean remove(Object var1) {
            if (!(var1 instanceof Entry)) {
                return false;
            } else {
                Entry var2 = (Entry) var1;
                return ComponentGraphicsResource.this.remove(var2.getKey(), var2.getValue());
            }
        }

        public int size() {
            return ComponentGraphicsResource.this.size();
        }

        public boolean isEmpty() {
            return ComponentGraphicsResource.this.isEmpty();
        }

        public void clear() {
            ComponentGraphicsResource.this.clear();
        }
    }

    abstract class m {
        int hfQ;
        int hfR;
        p[] hfS;
        p hfT;
        p hfU;
        Object currentKey;

        m() {
            this.hfQ = ComponentGraphicsResource.this.cIr.length - 1;
            this.hfR = -1;
            this.advance();
        }

        public boolean hasMoreElements() {
            return this.hasNext();
        }

        final void advance() {
            if (this.hfT == null || (this.hfT = this.hfT.hWH) == null) {
                while (this.hfR >= 0) {
                    if ((this.hfT = this.hfS[this.hfR--]) != null) {
                        return;
                    }
                }

                while (true) {
                    j var1;
                    do {
                        if (this.hfQ < 0) {
                            return;
                        }

                        var1 = ComponentGraphicsResource.this.cIr[this.hfQ--];
                    } while (var1.count == 0);

                    this.hfS = var1.efF;

                    for (int var2 = this.hfS.length - 1; var2 >= 0; --var2) {
                        if ((this.hfT = this.hfS[var2]) != null) {
                            this.hfR = var2 - 1;
                            return;
                        }
                    }
                }
            }
        }

        public boolean hasNext() {
            while (this.hfT != null) {
                if (this.hfT.key() != null) {
                    return true;
                }

                this.advance();
            }

            return false;
        }

        p cIT() {
            do {
                if (this.hfT == null) {
                    throw new NoSuchElementException();
                }

                this.hfU = this.hfT;
                this.currentKey = this.hfU.key();
                this.advance();
            } while (this.currentKey == null);

            return this.hfU;
        }

        public void remove() {
            if (this.hfU == null) {
                throw new IllegalStateException();
            } else {
                ComponentGraphicsResource.this.remove(this.currentKey);
                this.hfU = null;
            }
        }
    }

    final class e extends m implements Enumeration, Iterator {
        e() {
            super();
        }

        public Object next() {
            return super.cIT().key();
        }

        public Object nextElement() {
            return super.cIT().key();
        }
    }

    final class k extends AbstractSet {
        public Iterator iterator() {
            return ComponentGraphicsResource.this.new e();
        }

        public int size() {
            return ComponentGraphicsResource.this.size();
        }

        public boolean isEmpty() {
            return ComponentGraphicsResource.this.isEmpty();
        }

        public boolean contains(Object var1) {
            return ComponentGraphicsResource.this.containsKey(var1);
        }

        public boolean remove(Object var1) {
            return ComponentGraphicsResource.this.remove(var1) != null;
        }

        public void clear() {
            ComponentGraphicsResource.this.clear();
        }
    }

    final class a extends m implements Enumeration, Iterator {
        a() {
            super();
        }

        public Object next() {
            return super.cIT().value();
        }

        public Object nextElement() {
            return super.cIT().value();
        }
    }

    final class r extends AbstractCollection {
        public Iterator iterator() {
            return ComponentGraphicsResource.this.new a();
        }

        public int size() {
            return ComponentGraphicsResource.this.size();
        }

        public boolean isEmpty() {
            return ComponentGraphicsResource.this.isEmpty();
        }

        public boolean contains(Object var1) {
            return ComponentGraphicsResource.this.containsValue(var1);
        }

        public void clear() {
            ComponentGraphicsResource.this.clear();
        }
    }

    final class g extends d {

        g(Object var2, Object var3) {
            super(var2, var3);
        }

        public Object setValue(Object var1) {
            if (var1 == null) {
                throw new NullPointerException();
            } else {
                Object var2 = super.setValue(var1);
                ComponentGraphicsResource.this.put(this.getKey(), var1);
                return var2;
            }
        }
    }
}
