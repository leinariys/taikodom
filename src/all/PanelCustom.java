package all;

import javax.swing.*;
import java.awt.*;

/**
 * Базовые элементы интерфейса
 * тег panel JPanel aCo
 * class BaseUItegXML PanelCustom  BaseItemFactory
 */
public class PanelCustom extends BaseItemFactory {
    /**
     * Создание базового Component интерфейса
     * Вызывается из class BaseItem
     *
     * @param var1
     * @param var2
     * @param var3
     * @return
     */
    @Override
    protected JComponent CreatComponentCustom(MapKeyValue var1, Container var2, XmlNode var3) {
        JComponent var4 = new PanelCustomWrapper();
        this.CreatChildComponentCustom(var1, var4, var3); //я добавил не помогло
        return var4;
    }

}
