package all;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Анимация
 */
public class aDX {
    private static Map iDP = new HashMap();

    static {
        iDP.put("", aGg.hOF);
        iDP.put("NONE", aGg.hOF);
        iDP.put("STRONG_IN", aGg.hOJ);
        iDP.put("STRONG_OUT", aGg.hOK);
        iDP.put("STRONG_IN_OUT", aGg.hOL);
        iDP.put("REGULAR_IN", aGg.hOG);
        iDP.put("REGULAR_OUT", aGg.hOH);
        iDP.put("REGULAR_IN_OUT", aGg.hOI);
        iDP.put("BACK_IN", aGg.hOM);
        iDP.put("BACK_OUT", aGg.hON);
        iDP.put("BACK_IN_OUT", aGg.hOO);
        iDP.put("ELASTIC_IN", aGg.hOP);
        iDP.put("ELASTIC_OUT", aGg.hOQ);
        iDP.put("ELASTIC_IN_OUT", aGg.hOR);
    }

    private final jL_q iDQ;
    private final JComponent component;
    private Timer timer;
    private long startTime;
    private int iDR;
    private int iDS;
    private int iDT;
    private aGg iDU;
    private GJ atu;

    public aDX(JComponent var1, String var2, final jL_q var3, final GJ var4, boolean var5) {
        this.component = var1;
        this.iDQ = var3;
        String[] var6 = var2.split(";");
        this.parse(var6[0]);
        if (var6.length > 1) {
            StringBuffer var7 = new StringBuffer(var6[1]);

            for (int var8 = 2; var8 < var6.length; ++var8) {
                var7.append(";" + var6[var8]);
            }

            final String var9 = new String(var7.toString());
            this.atu = new GJ() {
                public void a(JComponent var1) {
                    new aDX(var1, var9, var3, var4, true);
                }
            };
        } else {
            this.atu = var4;
        }

        System.out.flush();
        if (var5) {
            this.dpf();
        }

    }

    public aDX(JComponent var1, String var2, jL_q var3, GJ var4) {
        this(var1, var2, var3, var4, true);
    }

    public void dpf() {
        this.startTime = System.currentTimeMillis();
        this.timer = new Timer(10, new ActionListener() {
            public void actionPerformed(ActionEvent var1) {
                aDX.this.gZ();
            }
        });
        this.timer.setInitialDelay(0);
        this.timer.start();
    }

    private void parse(String var1) {
        Pattern var2 = Pattern.compile("\\[(-?\\d{0,10})\\.\\.(-?\\d{0,10})\\] dur (\\d{1,10}).?([A-Za-z_]*)");
        Matcher var3 = var2.matcher(var1);
        if (!var3.find()) {
            throw new IllegalArgumentException("Parse error: " + var1);
        } else {
            if (var3.group(1).length() == 0) {
                this.iDR = (int) this.iDQ.a(this.component);
            } else {
                this.iDR = Integer.parseInt(var3.group(1));
            }

            this.iDS = Integer.parseInt(var3.group(2));
            this.iDT = Integer.parseInt(var3.group(3));
            this.iDU = (aGg) iDP.get(var3.group(4).toUpperCase());
        }
    }

    protected void gZ() {
        int var1 = (int) (System.currentTimeMillis() - this.startTime);
        float var2 = (float) this.iDU.aD(var1, this.iDT) / (float) this.iDT;
        int var3 = (int) ((float) this.iDR + (float) (this.iDS - this.iDR) * var2);
        this.iDQ.a(this.component, (float) var3);
        if (var1 >= this.iDT) {
            this.timer.stop();
            if (this.atu != null) {
                this.atu.a(this.component);
            }
        }

    }

    public void cAD() {
        if (this.atu != null) {
            this.atu.a(this.component);
        }

        this.kill();
    }

    public void kill() {
        this.atu = null;
        this.iDT = 0;
        if (this.timer != null) {
            this.timer.stop();
        }

    }

    public jL_q iU() {
        return this.iDQ;
    }
}
