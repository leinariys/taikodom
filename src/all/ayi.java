package all;

import com.hoplon.geometry.Vec3f;

public class ayi {
    public final Vec3f fTI = new Vec3f();
    public final Vec3f fTJ = new Vec3f();
    public final Vec3f gSH = new Vec3f();
    public final Vec3f gSI = new Vec3f();
    public final Vec3f fTL = new Vec3f();
    public float gSJ;
    public float gSK;
    public float gSL;
    public Object gSM;
    public int lifeTime;

    public ayi() {
    }

    public ayi(Vec3f var1, Vec3f var2, Vec3f var3, float var4) {
        this.b(var1, var2, var3, var4);
    }

    public void b(Vec3f var1, Vec3f var2, Vec3f var3, float var4) {
        this.fTI.set(var1);
        this.fTJ.set(var2);
        this.fTL.set(var3);
        this.gSJ = var4;
    }

    public float getDistance() {
        return this.gSJ;
    }

    public void setDistance(float var1) {
        this.gSJ = var1;
    }

    public int getLifeTime() {
        return this.lifeTime;
    }

    public void a(ayi var1) {
        this.fTI.set(var1.fTI);
        this.fTJ.set(var1.fTJ);
        this.gSI.set(var1.gSI);
        this.gSH.set(var1.gSH);
        this.fTL.set(var1.fTL);
        this.gSJ = var1.gSJ;
        this.gSK = var1.gSK;
        this.gSL = var1.gSL;
        this.gSM = var1.gSM;
        this.lifeTime = var1.lifeTime;
    }

    public Vec3f cDp() {
        return this.gSI;
    }

    public Vec3f cDq() {
        return this.gSH;
    }
}
