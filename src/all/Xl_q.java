package all;

public final class Xl_q {
    public static Xl_q eIw = new Xl_q("GrannyDontAllowUncopiedTail");
    public static Xl_q eIx = new Xl_q("GrannyAllowUncopiedTail");
    private static Xl_q[] eIy;
    private static int pF;

    static {
        eIy = new Xl_q[]{eIw, eIx};
        pF = 0;
    }

    private final int pG;
    private final String pH;

    private Xl_q(String var1) {
        this.pH = var1;
        this.pG = pF++;
    }

    private Xl_q(String var1, int var2) {
        this.pH = var1;
        this.pG = var2;
        pF = var2 + 1;
    }

    private Xl_q(String var1, Xl_q var2) {
        this.pH = var1;
        this.pG = var2.pG;
        pF = this.pG + 1;
    }

    public static Xl_q pb(int var0) {
        if (var0 < eIy.length && var0 >= 0 && eIy[var0].pG == var0) {
            return eIy[var0];
        } else {
            for (int var1 = 0; var1 < eIy.length; ++var1) {
                if (eIy[var1].pG == var0) {
                    return eIy[var1];
                }
            }

            throw new IllegalArgumentException("No enum " + Xl_q.class + " with value " + var0);
        }
    }

    public final int swigValue() {
        return this.pG;
    }

    public String toString() {
        return this.pH;
    }
}
