package all;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;
import java.security.InvalidParameterException;
import java.util.*;
import java.util.concurrent.Executor;

public class aBg {
    private Map hfZ;
    private List hga;

    public aBg() {
        this(false);
    }

    public aBg(boolean var1) {
        this.hfZ = new HashMap();
        this.hga = new ArrayList();
        if (var1) {
            this.aqT();
        }

    }

    private void aqT() {
        MBeanServer var1 = ManagementFactory.getPlatformMBeanServer();
        if (var1 != null) {
            SoftTimerConsole var2 = new SoftTimerConsole();
            ObjectName var3 = null;

            try {
                var3 = new ObjectName("Bitverse:name=SoftTimer");
                if (!var1.isRegistered(var3)) {
                    var1.registerMBean(var2, var3);
                }
            } catch (Exception var5) {
                System.out.println("Exception-" + var5);
            }

        }
    }

    public void jB(long var1) {
        Map var3 = this.hfZ;
        synchronized (this.hfZ) {
            Iterator var4 = this.hfZ.values().iterator();

            while (var4.hasNext()) {
                InternalTimer var5 = (InternalTimer) var4.next();
                if (var5.isCanceled()) {
                    var4.remove();
                } else if (var5.ih(var1)) {
                    this.hga.add(var5);
                } else if (var5.isCanceled()) {
                    var4.remove();
                }
            }
        }

        if (!this.hga.isEmpty()) {
            Iterator var7 = this.hga.iterator();

            while (var7.hasNext()) {
                InternalTimer var8 = (InternalTimer) var7.next();
                var8.run();
                if (var8.isCanceled()) {
                    var7.remove();
                }
            }

            this.hga.clear();
        }

    }

    public void a(long var1, Executor var3) {
        Map var4 = this.hfZ;
        synchronized (this.hfZ) {
            Iterator var5 = this.hfZ.values().iterator();

            while (var5.hasNext()) {
                InternalTimer var6 = (InternalTimer) var5.next();
                if (var6.isCanceled()) {
                    var5.remove();
                } else if (var6.ih(var1)) {
                    this.hga.add(var6);
                } else if (var6.isCanceled()) {
                    var5.remove();
                }
            }
        }

        if (!this.hga.isEmpty()) {
            Iterator var8 = this.hga.iterator();

            while (var8.hasNext()) {
                InternalTimer var9 = (InternalTimer) var8.next();
                if (var9.isCanceled()) {
                    var8.remove();
                } else {
                    var3.execute(var9);
                }
            }

            this.hga.clear();
        }

    }

    public void a(aen var1) {
        Map var2 = this.hfZ;
        synchronized (this.hfZ) {
            this.hfZ.remove(var1);
        }
    }

    public aen b(String var1, aen var2, long var3) {
        return this.a(var1, var2, var3, false);
    }

    public aen a(String var1, aen var2, long var3, boolean var5) {
        var2.reset(false);
        var2.canceled(false);
        Map var6 = this.hfZ;
        synchronized (this.hfZ) {
            this.hfZ.put(var2, new InternalTimer(var1, var2, var3, var5, true));
            return var2;
        }
    }

    public aen a(String var1, aen var2, long var3) {
        var2.reset(false);
        var2.canceled(false);
        Map var5 = this.hfZ;
        synchronized (this.hfZ) {
            this.hfZ.put(var2, new InternalTimer(var1, var2, var3, false, false));
            return var2;
        }
    }

    public interface InternalTimerMBean {
        boolean isRunning();

        long getAccumulator();

        long getInterval();
    }

    public interface SoftTimerConsoleMBean {
        int getTimersCount();

        List getTimers();
    }

    public class InternalTimer implements InternalTimerMBean, Runnable {
        private final aen ghx;
        private final long fLF;
        private final boolean ghz;
        private final boolean Df;
        private long ghy = 0L;
        private boolean running = false;
        private String name;

        public InternalTimer(String var2, aen var3, long var4, boolean var6, boolean var7) {
            if (var4 <= 0L) {
                throw new InvalidParameterException("interval must be > 0");
            } else {
                this.name = var2;
                this.ghx = var3;
                this.fLF = var4;
                this.ghz = var6;
                this.Df = var7;
            }
        }

        public boolean isRunning() {
            return this.running;
        }

        public long getAccumulator() {
            return this.ghy;
        }

        public long getInterval() {
            return this.fLF;
        }

        public boolean ih(long var1) {
            if (this.ghx.isCanceled()) {
                return false;
            } else if (this.ghx.isReset()) {
                this.ghy = 0L;
                this.ghx.reset(false);
                return false;
            } else {
                this.ghy += var1;
                return this.ghy >= this.fLF;
            }
        }

        public void run() {
            while (true) {
                if (this.ghy >= this.fLF) {
                    this.ghy -= this.fLF;
                    if (!this.ghz && this.ghy >= this.fLF) {
                        this.ghy %= this.fLF;
                    }

                    try {
                        this.running = true;
                        this.ghx.run();
                    } finally {
                        this.running = false;
                        if (!this.Df) {
                            this.ghx.canceled(true);
                        }

                    }

                    if (this.Df) {
                        continue;
                    }
                }

                return;
            }
        }

        public boolean isCanceled() {
            return this.ghx.isCanceled();
        }
    }

    public class SoftTimerConsole implements SoftTimerConsoleMBean {
        public int getTimersCount() {
            return aBg.this.hfZ.size();
        }

        public List getTimers() {
            ArrayList var1 = new ArrayList();
            int var2 = 0;
            synchronized (aBg.this.hfZ) {
                Iterator var5 = aBg.this.hfZ.values().iterator();

                while (true) {
                    if (!var5.hasNext()) {
                        break;
                    }

                    InternalTimer var4 = (InternalTimer) var5.next();
                    var1.add(String.format("%s: id:%CreateJComponent acc:%CreateJComponent interval:%CreateJComponent %s", var4.name, var2++, var4.getAccumulator(), var4.getInterval(), var4.isRunning() ? "running" : "not running"));
                }
            }

            Collections.sort(var1);
            return var1;
        }
    }
}
