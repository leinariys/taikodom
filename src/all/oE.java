package all;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 *
 */
public class oE {
    protected Set aPQ = new HashSet();
    private ArrayList aPR = new ArrayList();

    /**
     * @param var1 Пример file:/C:/,,,/taikodom/addon/login/login.xml
     * @param var2
     */
    public synchronized void a(URL var1, ahq_q var2) {
        this.aPQ.add(new a(var1, var2));
    }

    public synchronized void b(URL var1, ahq_q var2) {
        this.aPQ.remove(new a(var1, var2));
    }

    public void run() {
        this.aPR.clear();
        synchronized (this) {
            Iterator var3 = this.aPQ.iterator();

            while (true) {
                if (!var3.hasNext()) {
                    break;
                }

                a var2 = (a) var3.next();
                this.aPR.add(var2);
            }
        }

        Iterator var8 = this.aPR.iterator();

        while (var8.hasNext()) {
            a var1 = (a) var8.next();
            if (var1.file != null) {
                long var9 = var1.file.lastModified();
                if (var1.Ne != var9) {
                    var1.Ne = var9;

                    try {
                        var1.eqy.a(var1.eqx);
                    } catch (RuntimeException var6) {
                        this.b(var1.eqx, var1.eqy);
                        throw var6;
                    }
                }
            }
        }

        this.aPR.clear();
    }

    protected static class a {
        URL eqx;//file:/C:/,,,/taikodom/addon/login/login.xml
        File file;//file:/C:/,,,/taikodom/addon/login/login.xml
        ahq_q eqy;
        long Ne;

        public a(URL var1, ahq_q var2) {
            this.eqx = var1;
            this.eqy = var2;
            if ("file".equals(var1.getProtocol())) {
                try {
                    this.file = new File(var1.toURI());
                    this.Ne = this.file.lastModified();
                } catch (URISyntaxException var4) {
                    var4.printStackTrace();
                }
            }

        }
    }
}
