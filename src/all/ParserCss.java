package all;

import org.w3c.dom.css.CSSRule;
import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.css.CSSStyleSheet;
import org.w3c.dom.css.CSSValue;

import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.util.Properties;
import java.util.Stack;

public class ParserCss {
    private static final String className = ParseSpecificationCSS2.class.getName();
    private static ThreadLocal ess = new ThreadLocal() {
        @Override
        protected Reference<ParserCss> initialValue() {
            return new SoftReference<>(new ParserCss());
        }
    };
    private IParser parser = null;
    private SS qq = null;
    private CSSRule cssRule = null;

    public ParserCss() {
        try {
            setProperty("org.w3c.css.sac.parser", className);
            BI var1 = new BI();
            this.parser = new ParseSpecificationCSS2(); //var1.azZ();
        } catch (Exception var2) {
            var2.printStackTrace();
        }

    }

    public static ParserCss getParserCss() {
        Reference var0 = (Reference) ess.get();
        ParserCss var1 = (ParserCss) var0.get();
        if (var0 == null) {
            var1 = new ParserCss();
            ess.set(new SoftReference(var1));
        }

        return var1;
    }

    public static void setProperty(String var0, String var1) {
        Properties var2 = System.getProperties();
        var2.put(var0, var1);
        System.setProperties(var2);
    }

    /**
     * Парсинг файла с CSS стилями
     *
     * @param var1 адрес ресурса
     * @return
     */
    public CSSStyleSheet parseCSS(InputSource var1) {
        if (this.parser == null) {
            System.err.println("Null parser!");
        }

        DocumentHandlerWrapper var2 = new DocumentHandlerWrapper();
        this.parser.setDocumentHandler((DocumentHandler) var2);
        this.parser.parseStyleSheet(var1);
        return (CSSStyleSheet) var2.getRoot();
    }

    public CSSStyleDeclaration getCSSStyleDeclaration(InputSource var1) {
        aHx var2 = new aHx((CSSRule) null);
        this.a((CSSStyleDeclaration) var2, (InputSource) var1);
        return var2;
    }

    public void a(CSSStyleDeclaration var1, InputSource var2) {
        Stack var3 = new Stack();
        var3.push(var1);
        DocumentHandlerWrapper var4 = new DocumentHandlerWrapper(var3);
        this.parser.setDocumentHandler((DocumentHandler) var4);
        this.parser.parseStyleDeclaration(var2);
    }

    public CSSValue e(InputSource var1) {
        DocumentHandlerWrapper var2 = new DocumentHandlerWrapper();
        this.parser.setDocumentHandler((DocumentHandler) var2);
        return new Bj(this.parser.parsePropertyValue(var1));
    }

    public CSSRule f(InputSource var1) {
        DocumentHandlerWrapper var2 = new DocumentHandlerWrapper();
        this.parser.setDocumentHandler((DocumentHandler) var2);
        this.parser.parseRule(var1);
        return (CSSRule) var2.getRoot();
    }

    public SelectorList g(InputSource var1) {
        DocumentHandler var2 = new DocumentHandlerWrapper();
        this.parser.setDocumentHandler(var2);
        return this.parser.parseSelectors(var1);
    }

    public void a(SS var1) {
        this.qq = var1;
    }

    public void b(CSSRule var1) {
        this.cssRule = var1;
    }

    class DocumentHandlerWrapper implements DocumentHandler {
        private Stack listCssData;//стек исходных данных и стек разбитых данных
        private Object selectorAndValue = null;

        public DocumentHandlerWrapper(Stack var2) {
            this.listCssData = var2;
        }

        public DocumentHandlerWrapper() {
            this.listCssData = new Stack();
        }

        public Object getRoot() {
            return this.selectorAndValue;
        }

        public void startDocument(InputSource var1) {
            if (this.listCssData.empty()) {
                SS var2 = new SS();//css файл без формирования в одну строчку
                ParserCss.this.qq = var2;
                RK var3 = new RK();//css разбитый на селекторы
                var2.a(var3);
                this.listCssData.push(var2);
                this.listCssData.push(var3);
            }

        }

        public void endDocument(InputSource var1) {
            this.listCssData.pop();
            this.selectorAndValue = this.listCssData.pop();
        }

        public void comment(String var1) {
        }

        public void ignorableAtRule(String var1) {
            aTF var2 = new aTF(ParserCss.this.qq, (CSSRule) null, var1);
            if (!this.listCssData.empty()) {
                ((RK) this.listCssData.peek()).add(var2);
            } else {
                this.selectorAndValue = var2;
            }

        }

        public void namespaceDeclaration(String var1, String var2) {
        }

        public void importStyle(String var1, SACMediaList var2, String var3) {
            cf var4 = new cf(ParserCss.this.qq, (CSSRule) null, var1, new zw_q(var2));
            if (!this.listCssData.empty()) {
                ((RK) this.listCssData.peek()).add(var4);
            } else {
                this.selectorAndValue = var4;
            }

        }

        public void startMedia(SACMediaList var1) {
            aNI var2 = new aNI(ParserCss.this.qq, (CSSRule) null, new zw_q(var1));
            if (!this.listCssData.empty()) {
                ((RK) this.listCssData.peek()).add(var2);
            }

            RK var3 = new RK();
            var2.a(var3);
            this.listCssData.push(var2);
            this.listCssData.push(var3);
        }

        public void endMedia(SACMediaList var1) {
            this.listCssData.pop();
            this.selectorAndValue = this.listCssData.pop();
        }

        public void startPage(String var1, String var2) {
            aie var3 = new aie(ParserCss.this.qq, (CSSRule) null, var1, var2);
            if (!this.listCssData.empty()) {
                ((RK) this.listCssData.peek()).add(var3);
            }

            aHx var4 = new aHx(var3);
            var3.a(var4);
            this.listCssData.push(var3);
            this.listCssData.push(var4);
        }

        public void endPage(String var1, String var2) {
            this.listCssData.pop();
            this.selectorAndValue = this.listCssData.pop();
        }

        public void startFontFace() {
            FW var1 = new FW(ParserCss.this.qq, (CSSRule) null);
            if (!this.listCssData.empty()) {
                ((RK) this.listCssData.peek()).add(var1);
            }

            aHx var2 = new aHx(var1);
            var1.a(var2);
            this.listCssData.push(var1);
            this.listCssData.push(var2);
        }

        public void endFontFace() {
            this.listCssData.pop();
            this.selectorAndValue = this.listCssData.pop();
        }

        public void startSelector(SelectorList var1) {
            aQJ_q var2 = new aQJ_q(ParserCss.this.qq, (CSSRule) null, var1);
            if (!this.listCssData.empty()) {
                ((RK) this.listCssData.peek()).add(var2);
            }

            aHx var3 = new aHx(var2);
            var2.a(var3);
            this.listCssData.push(var2);
            this.listCssData.push(var3);
        }

        public void endSelector(SelectorList var1) {
            this.listCssData.pop();
            this.selectorAndValue = this.listCssData.pop();
        }

        public void property(String var1, LexicalUnit var2, boolean var3) {
            aHx var4 = (aHx) this.listCssData.peek();
            var4.add(new PropertyCss(var1, new Bj(var2), var3));
        }
    }
}
