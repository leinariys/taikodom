package all;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;

public class alI extends adK {

    private float radius;

    public alI() {
    }

    public alI(float var1) {
        this.radius = var1;
        super.setVolume((float) ((double) (1.3333334F * var1 * var1 * var1) * 3.141592653589793D));
    }

    public float getOuterSphereRadius() {
        return this.radius;
    }

    public float getRadius() {
        return this.radius;
    }

    public void setRadius(float var1) {
        this.radius = var1;
        super.setVolume((float) ((double) (1.3333334F * var1 * var1 * var1) * 3.141592653589793D));
    }

    public String toString() {
        return "Sphere [radius:" + this.radius + "]";
    }

    public byte getShapeType() {
        return 1;
    }

    protected void growBoundingBox(ajK var1, BoundingBox var2) {
        var2.addPoint(var1.m03 + this.getRadius(), var1.m13 + this.getRadius(), var1.m23 + this.getRadius());
        var2.addPoint(var1.m03 + -this.getRadius(), var1.m13 + -this.getRadius(), var1.m23 + -this.getRadius());
    }

    public void getFarthest(Vec3f var1, Vec3f var2) {
        var2.set(var1);
        var2.normalize();
        var2.mT(this.getRadius());
    }

    public RY toMesh() {
        float var1 = this.radius;
        byte var2 = 6;
        byte var3 = 6;
        Vec3f[] var4 = new Vec3f[var2 * (var3 + 1)];
        int[] var5 = new int[var2 * var3 * 6];
        int var6 = 0;
        int var7 = 0;

        for (int var8 = 0; var8 <= var3; ++var8) {
            float var9 = (float) ((double) var1 * Math.cos((double) var8 * 3.141592653589793D / (double) var3));
            float var10 = (float) ((double) var1 * Math.sin((double) var8 * 3.141592653589793D / (double) var3));

            for (int var11 = 0; var11 < var2; ++var11) {
                float var12 = (float) ((double) var10 * Math.cos((double) (var11 * 2) * 3.141592653589793D / (double) var2));
                float var13 = (float) ((double) var10 * Math.sin((double) (var11 * 2) * 3.141592653589793D / (double) var2));
                var4[var6++] = new Vec3f(var12, var9, var13);
                if (var8 > 0) {
                    int var14 = (var11 + 1) % var2;
                    var5[var7++] = var8 * var2 + var14;
                    var5[var7++] = var8 * var2 + var11;
                    var5[var7++] = (var8 - 1) * var2 + var11;
                    var5[var7++] = var8 * var2 + var14;
                    var5[var7++] = (var8 - 1) * var2 + var11;
                    var5[var7++] = (var8 - 1) * var2 + var14;
                }
            }
        }

        return new RY(var4, var5);
    }

    public void calculateLocalInertia(float var1, Vec3f var2) {
        float var3 = 0.4F * var1 * this.getMargin() * this.getMargin();
        var2.set(var3, var3, var3);
    }

    public float getMargin() {
        return this.getRadius();
    }

    public void setMargin(float var1) {
        super.setMargin(var1);
    }

    public Vec3f localGetSupportingVertexWithoutMargin(Vec3f var1) {
        return Bb.dMU;
    }

    public void batchedUnitVectorGetSupportingVertexWithoutMargin(Vec3f[] var1, Vec3f[] var2, int var3) {
        for (int var4 = 0; var4 < var3; ++var4) {
            var2[var4].set(0.0F, 0.0F, 0.0F);
        }

    }
}
