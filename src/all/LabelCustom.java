package all;

import javax.swing.*;
import java.awt.*;

public final class LabelCustom extends BaseItem {
    /**
     * Создание базового Component интерфейса
     * Вызывается из class BaseItem
     *
     * @param var1
     * @param var2
     * @param var3
     * @return
     */
    @Override
    protected JComponent CreatComponentCustom(MapKeyValue var1, Container var2, XmlNode var3) {
        return new LabelCustomWrapper();
    }

    protected void a(MapKeyValue var1, JLabel var2, XmlNode var3) {
        super.applyAttribute(var1, (Component) var2, (XmlNode) var3);
        String var4 = var3.getAttribute("text");
        if (var4 != null) {
            var2.setText(var4);
        }

        XmlNode var5 = var3.c(0, "text", (String) null, (String) null);
        if (var5 != null) {
            var2.setText(var5.getText());
        }

        var4 = var3.getAttribute("horizontalTextPosition");
        String var6;
        if (var4 != null) {
            var6 = var4.toLowerCase();
            if (AbstractButtonCustom.biH.containsKey(var6)) {
                var2.setHorizontalTextPosition(AbstractButtonCustom.biH.get(var6));
            }
        }

        var4 = var3.getAttribute("verticalTextPosition");
        if (var4 != null) {
            var6 = var4.toLowerCase();
            if (AbstractButtonCustom.biI.containsKey(var6)) {
                var2.setVerticalTextPosition(AbstractButtonCustom.biI.get(var6));
            }
        }

    }

}
