package all;

import org.w3c.dom.css.CSSFontFaceRule;
import org.w3c.dom.css.CSSRule;
import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.css.CSSStyleSheet;

import java.io.Serializable;
import java.io.StringReader;

public class FW implements Serializable, CSSFontFaceRule {
    private SS qq = null;
    private CSSRule qr = null;
    private aHx cXh = null;

    public FW(SS var1, CSSRule var2) {
        this.qq = var1;
        this.qr = var2;
    }

    public short getType() {
        return 5;
    }

    public String getCssText() {
        return "@font-face " + this.getStyle().getCssText();
    }

    public void setCssText(String var1) {
        if (this.qq != null && this.qq.isReadOnly()) {
            throw new qN((short) 7, 2);
        } else {
            try {
                InputSource var2 = new InputSource(new StringReader(var1));
                ParserCss var3 = ParserCss.getParserCss();
                CSSRule var4 = var3.f(var2);
                if (var4.getType() == 5) {
                    this.cXh = ((FW) var4).cXh;
                } else {
                    throw new qN((short) 13, 8);
                }
            } catch (CSSException var5) {
                throw new qN((short) 12, 0, var5.getMessage());
            }/* catch (IOException var6) {
            throw new qN((short)12, 0, var6.getMessage());
         }*/
        }
    }

    public CSSStyleSheet getParentStyleSheet() {
        return this.qq;
    }

    public CSSRule getParentRule() {
        return this.qr;
    }

    public CSSStyleDeclaration getStyle() {
        return this.cXh;
    }

    public void a(aHx var1) {
        this.cXh = var1;
    }
}
