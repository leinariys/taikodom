package all;

import com.hoplon.geometry.Vec3f;

public abstract class Pg {
    protected final Kt stack = Kt.bcE();

    public abstract void i(Vec3f var1, Vec3f var2, Vec3f var3);

    public abstract void a(Vec3f var1, Vec3f var2, float var3, int var4, Vec3f var5);

    public abstract void gj(String var1);

    public abstract void c(Vec3f var1, String var2);

    public abstract void mt(int var1);

    public abstract int bnd();

    public void j(Vec3f var1, Vec3f var2, Vec3f var3) {
        this.stack.bcH().push();

        try {
            Vec3f var4 = this.stack.bcH().ac(var2);
            var4.sub(var1);
            var4.scale(0.5F);
            Vec3f var5 = this.stack.bcH().ac(var2);
            var5.add(var1);
            var5.scale(0.5F);
            Vec3f var8 = this.stack.bcH().h(1.0F, 1.0F, 1.0F);
            Vec3f var9 = (Vec3f) this.stack.bcH().get();
            Vec3f var10 = (Vec3f) this.stack.bcH().get();

            for (int var6 = 0; var6 < 4; ++var6) {
                for (int var7 = 0; var7 < 3; ++var7) {
                    var9.set(var8.x * var4.x, var8.y * var4.y, var8.z * var4.z);
                    var9.add(var5);
                    int var11 = var7 % 3;
                    JL.b(var8, var11, -1.0F);
                    var10.set(var8.x * var4.x, var8.y * var4.y, var8.z * var4.z);
                    var10.add(var5);
                    this.i(var9, var10, var3);
                }

                var8.set(-1.0F, -1.0F, -1.0F);
                if (var6 < 3) {
                    JL.b(var8, var6, -1.0F);
                }
            }
        } finally {
            this.stack.bcH().pop();
        }

    }
}
