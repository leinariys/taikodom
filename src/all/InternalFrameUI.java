package all;

import javax.swing.*;
import javax.swing.event.MouseInputAdapter;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicInternalFrameTitlePane;
import javax.swing.plaf.basic.BasicInternalFrameUI;
import java.awt.*;
import java.awt.event.MouseEvent;
//import javax.swing.plaf.basic.BasicInternalFrameUI.BorderListener;

/**
 * InternalFrameUI
 */
public class InternalFrameUI extends BasicInternalFrameUI implements aIh {
    private final JInternalFrame hUF;
    private ComponentManager Rp;

    public InternalFrameUI(JInternalFrame var1) {
        super(var1);
        this.hUF = var1;
        var1.getContentPane().setBackground(new Color(0, 0, 0, 0));
        var1.setOpaque(false);
        this.Rp = new ComponentManager("window", var1);
    }

    public static ComponentUI createUI(JComponent var0) {
        return new InternalFrameUI((JInternalFrame) var0);
    }

    // $FF: synthetic method
    static JInternalFrame a(InternalFrameUI var0) {
        return var0.frame;
    }

    // $FF: synthetic method
    static BasicInternalFrameTitlePane b(InternalFrameUI var0) {
        return var0.titlePane;
    }

    protected void installListeners() {
        super.installListeners();
        this.hUF.addComponentListener(new os(this.Rp));
    }

    protected void uninstallListeners() {
        super.uninstallListeners();
        this.hUF.removeComponentListener(new os(this.Rp));
    }

    public void update(Graphics var1, JComponent var2) {
        RM.a((aIh) this, (Graphics) var1, (JComponent) var2);
        this.paint(var1, var2);
    }

    protected void installKeyboardActions() {
        super.installKeyboardActions();
        ActionMap var1 = SwingUtilities.getUIActionMap(this.frame);
        if (var1 != null) {
            var1.remove("showSystemMenu");
        }

    }

    protected void uninstallComponents() {
        this.titlePane = null;
        super.uninstallComponents();
    }

    protected JComponent createNorthPane(JInternalFrame var1) {
        return new BasicInternalFrameTitlePaneCustomWrapper(var1);
    }

    protected MouseInputAdapter createBorderListener(JInternalFrame var1) {
        return new a((a) null);
    }

    public ComponentManager wy() {
        return this.Rp;
    }

    public Dimension getPreferredSize(JComponent var1) {
        return this.wy().c(var1, super.getPreferredSize(var1));
    }

    public Dimension getMinimumSize(JComponent var1) {
        return this.wy().b(var1, super.getMinimumSize(var1));
    }

    public Dimension getMaximumSize(JComponent var1) {
        return this.wy().a(var1, super.getMaximumSize(var1));
    }

    /**
     * TODO похлже это public pv wy()
     *
     * @return Графический элемент
     */
    @Override
    public aeK wz() {
        return this.Rp;
    }

    private class a extends BorderListener {
        private a() {
            super();
        }

        // $FF: synthetic method
        a(a var2) {
            this();
        }

        Rectangle cwp() {
            Rectangle var1 = null;
            Icon var2 = InternalFrameUI.a(InternalFrameUI.this).getFrameIcon();
            if (var2 != null) {
                int var3 = InternalFrameUI.b(InternalFrameUI.this).getHeight() / 2 - var2.getIconHeight() / 2;
                var1 = new Rectangle(5, var3, var2.getIconWidth(), var2.getIconHeight());
            }

            return var1;
        }

        public void mouseClicked(MouseEvent var1) {
            if (var1.getClickCount() == 2 && var1.getSource() == InternalFrameUI.this.getNorthPane() && InternalFrameUI.a(InternalFrameUI.this).isClosable() && !InternalFrameUI.a(InternalFrameUI.this).isIcon()) {
                Rectangle var2 = this.cwp();
                if (var2 != null && var2.contains(var1.getX(), var1.getY())) {
                    InternalFrameUI.a(InternalFrameUI.this).doDefaultCloseAction();
                } else {
                    super.mouseClicked(var1);
                }
            } else {
                super.mouseClicked(var1);
            }

        }
    }
}
