package all;

import java.io.*;
import java.util.UUID;

public class nR {
    public static String aT(String var0) {
        return b(new File(var0));
    }

    public static String b(File var0) {
        String var1;
        try {
            BufferedReader var2 = new BufferedReader(new InputStreamReader(new FileInputStream(var0)));
            var1 = var2.readLine();
        } catch (Exception var3) {
            var1 = c(var0);
        }

        return var1;
    }

    public static String aU(String var0) {
        return c(new File(var0));
    }

    public static String c(File var0) {
        String var1 = RI();
        BufferedWriter var2 = null;
        try {
            var2 = new BufferedWriter(new FileWriter(var0));

            var2.write(var1);
            var2.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return var1;
    }

    public static String RI() {
        UUID var0 = UUID.randomUUID();
        return var0.toString();
    }
}
