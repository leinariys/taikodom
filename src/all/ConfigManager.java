package all;

import gnu.trove.THashMap;

import java.io.*;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Менеджер настроек
 */
public class ConfigManager {
    public static String NAME = "CONFIG_MANAGER";
    private static ConfigManager confManag;
    /**
     * Список настроек
     * Ключ render, input, network
     * Значение список настроек
     */
    public Map sectionOptions = new THashMap();

    /**
     * Создаём менеджер настроек
     *
     * @return Менеджер настроек
     */
    public static ConfigManager initConfigManager() {
        if (confManag == null) {
            confManag = new ConfigManager();
        }
        return confManag;
    }

    protected static ConfigManager cHv() {
        return new ConfigManager();
    }

    public void clear() {
        this.sectionOptions.clear();
    }

    /**
     * Чтение файла настроек
     *
     * @param file config/devel.ini
     * @throws IOException
     */
    public void loadConfig(String file) throws IOException {
        BufferedReader buffer = new BufferedReader(new FileReader(file));
        ConfigManagerSection section = null;
        int indexLine = 0;
        while (true) {
            String line;
            do {
                if ((line = buffer.readLine()) == null)//Читаем строчку
                {
                    return;
                }
                ++indexLine;
                line = line.trim();
            } while (line.length() == 0 || line.startsWith("#"));//Если это пустая страка или начало # читаем следующую строку

            if (line.startsWith("["))//Если это раздел настроек
            {
                if (!line.endsWith("]"))//Если нет символа окончания раздела
                {
                    System.err.println("WARNING: Badly formatted config section on " + file + ";" + Integer.toString(indexLine));
                    line.concat("]");//Добавляем символ
                }

                String sectionName = line.substring(1, line.length() - 1);
                ConfigManagerSection sectionTemp = (ConfigManagerSection) this.sectionOptions.get(sectionName);//Проверяем добавлен ли такой раздел
                if (sectionTemp == null) //Нет такого раздела
                {
                    sectionTemp = new ConfigManagerSection();//Создаём хранилище параметров раздела настроек
                    this.sectionOptions.put(sectionName, sectionTemp);//render, input, network
                }

                //Текой раздел есть
                section = sectionTemp;

            } else {//Если это не раздел настроек
                int indexAssignment = line.indexOf("=");
                if (indexAssignment == -1) {
                    System.err.print("Error at config file, invalid string: " + line);
                } else {
                    String keyProperty = line.substring(0, indexAssignment);//Название настройки
                    String valueProperty = line.substring(indexAssignment + 1);//Значение настройки
                    if (keyProperty.length() > 0) {
                        if (section == null) //Если раздел настроек не задан
                        {
                            System.err.println("WARNING: Found an entry before all section on " + file + ":" + Integer.toString(indexLine));
                        } else if (valueProperty.startsWith("\"")) {
                            if (!valueProperty.endsWith("\"")) {
                                System.err.println("WARNING: Incorrectly terminated string - missing closing quotes on " + file + ":" + Integer.toString(indexLine));
                                valueProperty.concat("\"");
                            }

                            section.setPropertyString(keyProperty, valueProperty.substring(1, valueProperty.length() - 1));
                        } else if (valueProperty.matches("-?[0-9]*\\.[0-9]+[f]?")) {
                            section.setPropertyFloat(keyProperty, Float.valueOf(valueProperty));
                        } else if (valueProperty.matches("-?[0-9]*")) {
                            if (valueProperty != null && valueProperty.length() > 0) {
                                section.setPropertyInteger(keyProperty, Integer.valueOf(valueProperty));
                            }
                        } else if (!valueProperty.equalsIgnoreCase("true") && !valueProperty.equalsIgnoreCase("yes") && !valueProperty.equalsIgnoreCase("y") && !valueProperty.equalsIgnoreCase("on")) {
                            if (!valueProperty.equalsIgnoreCase("false") && !valueProperty.equalsIgnoreCase("no") && !valueProperty.equalsIgnoreCase("n") && !valueProperty.equalsIgnoreCase("off")) {
                                System.err.println("WARNING: Incorrectly formatted value on " + file + ":" + Integer.toString(indexLine));
                            } else {
                                section.setPropertyBoolean(keyProperty, Boolean.FALSE);
                            }
                        } else {
                            section.setPropertyBoolean(keyProperty, Boolean.TRUE);
                        }
                    } else {
                        System.err.println("WARNING: Zero-length key on " + file + ":" + Integer.toString(indexLine));
                    }
                }
            }
        }
    }

    private void saveConfig(String var1, boolean var2) throws IOException {
        BufferedWriter buffer = null;
        Iterator sectionsName = this.sectionOptions.keySet().iterator();

        label49:
        while (sectionsName.hasNext()) {
            String sectionName = (String) sectionsName.next();
            boolean var7 = false;
            ConfigManagerSection var8 = (ConfigManagerSection) this.sectionOptions.get(sectionName);
            Set var9 = var8.getPropertys().entrySet();
            Iterator sectionsProperty = var9.iterator();

            while (true) {
                Entry property;
                ConfigManagerProperty var12;
                do {
                    if (!sectionsProperty.hasNext()) {
                        if (var7) {
                            buffer.newLine();
                        }
                        continue label49;
                    }

                    property = (Entry) sectionsProperty.next();
                    var12 = (ConfigManagerProperty) property.getValue();
                } while (var2 && !var12.isDefaultConfig());

                if (buffer == null) {
                    buffer = new BufferedWriter(new FileWriter(var1));
                    buffer.write("# Saved automatically by ConfigManager");
                    buffer.newLine();
                    buffer.write("# " + (new Date()).toString());
                    buffer.newLine();
                    buffer.newLine();
                }

                if (!var7) {
                    var7 = true;
                    buffer.write("[" + sectionName + "]");
                    buffer.newLine();
                }

                buffer.write((String) property.getKey() + "=");
                Class valuePropertyClass = var12.getTypeProperty();
                if (valuePropertyClass == String.class) {
                    buffer.write("\"");
                }

                buffer.write(var12.getValueProperty().toString());
                if (valuePropertyClass == String.class) {
                    buffer.write("\"");
                }

                buffer.newLine();
            }
        }

        if (buffer != null) {
            buffer.close();
        }
    }

    public void saveConfig(String var1) throws IOException {
        this.saveConfig(var1, false);
    }

    public void loadConfigIni(String var1) throws IOException {
        this.saveConfig(var1, true);
    }

    /**
     * Получаем настройки указанного раздела
     *
     * @param sectionName имя рездела
     * @return
     */
    public ConfigManagerSection getSection(String sectionName) {
        ConfigManagerSection section = (ConfigManagerSection) this.sectionOptions.get(sectionName);//Получаем раздел
        if (section == null) {
            throw new InvalidSectionNameException("[" + sectionName + "]");
        } else {
            return section;
        }
    }

    public void readExternal(ObjectInput var1) {
    }

    public ConfigManager copyConfigManager() {
        ConfigManager configManager = new ConfigManager();
        Iterator sections = this.sectionOptions.entrySet().iterator();

        while (sections.hasNext()) {
            Entry var3 = (Entry) sections.next();
            configManager.sectionOptions.put((String) var3.getKey(), ((ConfigManagerSection) var3.getValue()).clone());
        }
        return configManager;
    }

    public void setMarkAsDefaultConfig() {
        Iterator sections = this.sectionOptions.entrySet().iterator();

        while (sections.hasNext()) {
            Entry section = (Entry) sections.next();
            ((ConfigManagerSection) section.getValue()).setMarkAsDefaultConfig();
        }
    }

    public String kX(String var1) {
        return nR.b(new File(var1));
    }

}
