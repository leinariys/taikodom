package all;

public enum vI {
    bzG(1),
    bzH(3) {
        public int t(int var1, int var2) {
            return var1 - var2;
        }
    },
    bzI(0) {
        public int t(int var1, int var2) {
            return (var1 - var2) / 2;
        }
    },
    bzJ(0) {
        public int u(int var1, int var2) {
            return var1;
        }
    };

    private final int bzK;

    private vI(int var3) {
        this.bzK = var3;
    }

    // $FF: synthetic method
    vI(int var3, vI var4) {
        this(var3);
    }

    public int akW() {
        return this.bzK;
    }

    public int t(int var1, int var2) {
        return 0;
    }

    public int u(int var1, int var2) {
        return var2;
    }
}
