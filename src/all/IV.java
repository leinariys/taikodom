package all;

import com.hoplon.geometry.Vec3f;

public final class IV implements ti {

    public final double x;
    public final double y;
    public final double z;

    public IV(IV var1) {
        this.x = var1.x;
        this.y = var1.y;
        this.z = var1.z;
    }

    public IV(Vector3dOperations var1) {
        this.x = var1.x;
        this.y = var1.y;
        this.z = var1.z;
    }

    public IV(double var1, double var3, double var5) {
        this.x = var1;
        this.y = var3;
        this.z = var5;
    }

    public IV(Vec3f var1) {
        this.x = (double) var1.x;
        this.y = (double) var1.y;
        this.z = (double) var1.z;
    }

    public static double a(IV var0, IV var1) {
        return Math.pow((var0.x - var1.x) * (var0.x - var1.x) + (var0.y - var1.y) * (var0.y - var1.y) + (var0.z - var1.z) * (var0.z - var1.z), 0.5D);
    }

    public static double b(IV var0, IV var1) {
        return (var0.x - var1.x) * (var0.x - var1.x) + (var0.y - var1.y) * (var0.y - var1.y) + (var0.z - var1.z) * (var0.z - var1.z);
    }

    public static IV a(IV var0, IV var1, IV var2) {
        IV var3 = var2.d(var0);
        IV var4 = var1.d(var0).aXh();
        double var5 = var0.d(var1).length();
        double var7 = var4.a(var3);
        if (var7 <= 0.0D) {
            return var0;
        } else if (var7 >= var5) {
            return var1;
        } else {
            IV var9 = var4.A(var7);
            IV var10 = var0.c(var9);
            return var10;
        }
    }

    public IV A(double var1) {
        return new IV(this.x * var1, this.y * var1, this.z * var1);
    }

    public IV b(IV var1) {
        return new IV(this.x * var1.x, this.y * var1.y, this.z * var1.z);
    }

    public IV c(IV var1) {
        return new IV(this.x + var1.x, this.y + var1.y, this.z + var1.z);
    }

    public IV d(IV var1) {
        return new IV(this.x - var1.x, this.y - var1.y, this.z - var1.z);
    }

    public double lengthSquared() {
        return this.x * this.x + this.y * this.y + this.z * this.z;
    }

    public IV e(IV var1) {
        return new IV(this.y * var1.z - this.z * var1.y, this.z * var1.x - this.x * var1.z, this.x * var1.y - this.y * var1.x);
    }

    public String toString() {
        return "(" + this.x + ", " + this.y + ", " + this.z + ")";
    }

    public IV aXh() {
        double var1 = this.x * this.x + this.y * this.y + this.z * this.z;
        if (var1 > 0.0D && var1 != 1.0D) {
            double var3 = Math.sqrt(var1);
            return new IV(this.x / var3, this.y / var3, this.z / var3);
        } else {
            return this;
        }
    }

    public double length() {
        return Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
    }

    public double a(IV var1) {
        return this.x * var1.x + this.y * var1.y + this.z * var1.z;
    }

    public double get(int var1) {
        switch (var1) {
            case 0:
                return this.x;
            case 1:
                return this.y;
            case 2:
                return this.z;
            default:
                throw new IllegalArgumentException();
        }
    }

    public double adw() {
        return this.x;
    }

    public double adx() {
        return this.y;
    }

    public double ady() {
        return this.z;
    }
}
