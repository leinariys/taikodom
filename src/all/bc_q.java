package all;

import com.hoplon.geometry.Vec3f;

import javax.vecmath.*;
import java.io.Serializable;
import java.nio.FloatBuffer;

public class bc_q implements afA_q, Serializable {
    public ajD mX = new ajD();
    public Vector3dOperations position = new Vector3dOperations();

    public bc_q() {
        this.mX.setIdentity();
    }

    public bc_q(ajK var1) {
        this.set(var1);
    }

    public bc_q(float var1, float var2, float var3, float var4, float var5, float var6, float var7, float var8, float var9, double var10, double var12, double var14) {
        this.mX.m00 = var1;
        this.mX.m10 = var4;
        this.mX.m20 = var7;
        this.mX.m01 = var2;
        this.mX.m11 = var5;
        this.mX.m21 = var8;
        this.mX.m02 = var3;
        this.mX.m12 = var6;
        this.mX.m22 = var9;
        this.position.x = var10;
        this.position.y = var12;
        this.position.z = var14;
    }

    public bc_q(bc_q var1) {
        this.b(var1);
    }

    public bc_q(OrientationBase var1, Vector3dOperations var2) {
        this.setOrientation(var1);
        this.setTranslation(var2);
    }

    private static void a(bc_q var0, bc_q var1, bc_q var2) {
        ajD var3 = var0.mX;
        ajD var4 = var1.mX;
        float var5 = var3.m00 * var4.m00 + var3.m01 * var4.m10 + var3.m02 * var4.m20;
        float var6 = var3.m10 * var4.m00 + var3.m11 * var4.m10 + var3.m12 * var4.m20;
        float var7 = var3.m20 * var4.m00 + var3.m21 * var4.m10 + var3.m22 * var4.m20;
        float var8 = var3.m00 * var4.m01 + var3.m01 * var4.m11 + var3.m02 * var4.m21;
        float var9 = var3.m10 * var4.m01 + var3.m11 * var4.m11 + var3.m12 * var4.m21;
        float var10 = var3.m20 * var4.m01 + var3.m21 * var4.m11 + var3.m22 * var4.m21;
        float var11 = var3.m00 * var4.m02 + var3.m01 * var4.m12 + var3.m02 * var4.m22;
        float var12 = var3.m10 * var4.m02 + var3.m11 * var4.m12 + var3.m12 * var4.m22;
        float var13 = var3.m20 * var4.m02 + var3.m21 * var4.m12 + var3.m22 * var4.m22;
        Vector3dOperations var14 = var1.position;
        Vector3dOperations var15 = var0.position;
        double var16 = (double) var3.m00 * var14.x + (double) var3.m01 * var14.y + (double) var3.m02 * var14.z + var15.x;
        double var18 = (double) var3.m10 * var14.x + (double) var3.m11 * var14.y + (double) var3.m12 * var14.z + var15.y;
        double var20 = (double) var3.m20 * var14.x + (double) var3.m21 * var14.y + (double) var3.m22 * var14.z + var15.z;
        ajD var22 = var2.mX;
        var22.m00 = var5;
        var22.m10 = var6;
        var22.m20 = var7;
        var22.m01 = var8;
        var22.m11 = var9;
        var22.m21 = var10;
        var22.m02 = var11;
        var22.m12 = var12;
        var22.m22 = var13;
        var2.position.x = var16;
        var2.position.y = var18;
        var2.position.z = var20;
    }

    static boolean isZero(float var0) {
        return var0 < 1.0E-5F && var0 > -1.0E-5F;
    }

    public void transform(Vector3f var1, Vector3f var2) {
        float var3 = (float) ((double) (var1.x * this.mX.m00 + var1.y * this.mX.m01 + var1.z * this.mX.m02) + this.position.x);
        float var4 = (float) ((double) (var1.x * this.mX.m10 + var1.y * this.mX.m11 + var1.z * this.mX.m12) + this.position.y);
        var2.z = (float) ((double) (var1.x * this.mX.m20 + var1.y * this.mX.m21 + var1.z * this.mX.m22) + this.position.z);
        var2.y = var4;
        var2.x = var3;
    }

    public void transform(Vector3f var1) {
        this.transform(var1, var1);
    }

    public void a(Vector3f var1, Vector3d var2) {
        var2.x = (double) (var1.x * this.mX.m00 + var1.y * this.mX.m01 + var1.z * this.mX.m02) + this.position.x;
        var2.y = (double) (var1.x * this.mX.m10 + var1.y * this.mX.m11 + var1.z * this.mX.m12) + this.position.y;
        var2.z = (double) (var1.x * this.mX.m20 + var1.y * this.mX.m21 + var1.z * this.mX.m22) + this.position.z;
    }

    public void multiply3x3(Vec3f var1, Vec3f var2) {
        var2.a((ajD) this.mX, var1);
    }

    public void multiply3x3(Vector3dOperations var1, Vector3dOperations var2) {
        var2.a((ajD) this.mX, (Tuple3d) var1);
    }

    public void multiply3x3(Vec3f var1, Vector3dOperations var2) {
        var2.a((ajD) this.mX, (Tuple3f) var1);
    }

    public void a(Vector3dOperations var1, Vector3dOperations var2) {
        float var3 = 1.0F / (this.mX.m00 * this.mX.m00 + this.mX.m10 * this.mX.m10 + this.mX.m20 * this.mX.m20);
        float var4 = 1.0F / (this.mX.m01 * this.mX.m01 + this.mX.m11 * this.mX.m11 + this.mX.m21 * this.mX.m21);
        float var5 = 1.0F / (this.mX.m02 * this.mX.m02 + this.mX.m12 * this.mX.m12 + this.mX.m22 * this.mX.m22);
        float var6 = this.mX.m00 * var3;
        float var7 = this.mX.m10 * var4;
        float var8 = this.mX.m20 * var5;
        float var9 = this.mX.m01 * var3;
        float var10 = this.mX.m11 * var4;
        float var11 = this.mX.m21 * var5;
        float var12 = this.mX.m02 * var3;
        float var13 = this.mX.m12 * var4;
        float var14 = this.mX.m22 * var5;
        double var15 = -(this.position.x * (double) this.mX.m00 + this.position.y * (double) this.mX.m10 + this.position.z * (double) this.mX.m20);
        double var17 = -(this.position.x * (double) this.mX.m01 + this.position.y * (double) this.mX.m11 + this.position.z * (double) this.mX.m21);
        double var19 = -(this.position.x * (double) this.mX.m02 + this.position.y * (double) this.mX.m12 + this.position.z * (double) this.mX.m22);
        double var21 = var1.x;
        double var23 = var1.y;
        double var25 = var1.z;
        var2.x = var21 * (double) var6 + var23 * (double) var7 + var25 * (double) var8 + var15;
        var2.y = var21 * (double) var9 + var23 * (double) var10 + var25 * (double) var11 + var17;
        var2.z = var21 * (double) var12 + var23 * (double) var13 + var25 * (double) var14 + var19;
    }

    public void a(Vector3dOperations var1, Vec3f var2) {
        float var3 = 1.0F / (this.mX.m00 * this.mX.m00 + this.mX.m10 * this.mX.m10 + this.mX.m20 * this.mX.m20);
        float var4 = 1.0F / (this.mX.m01 * this.mX.m01 + this.mX.m11 * this.mX.m11 + this.mX.m21 * this.mX.m21);
        float var5 = 1.0F / (this.mX.m02 * this.mX.m02 + this.mX.m12 * this.mX.m12 + this.mX.m22 * this.mX.m22);
        float var6 = this.mX.m00 * var3;
        float var7 = this.mX.m10 * var4;
        float var8 = this.mX.m20 * var5;
        float var9 = this.mX.m01 * var3;
        float var10 = this.mX.m11 * var4;
        float var11 = this.mX.m21 * var5;
        float var12 = this.mX.m02 * var3;
        float var13 = this.mX.m12 * var4;
        float var14 = this.mX.m22 * var5;
        double var15 = -(this.position.x * (double) this.mX.m00 + this.position.y * (double) this.mX.m10 + this.position.z * (double) this.mX.m20);
        double var17 = -(this.position.x * (double) this.mX.m01 + this.position.y * (double) this.mX.m11 + this.position.z * (double) this.mX.m21);
        double var19 = -(this.position.x * (double) this.mX.m02 + this.position.y * (double) this.mX.m12 + this.position.z * (double) this.mX.m22);
        double var21 = var1.x;
        double var23 = var1.y;
        double var25 = var1.z;
        var2.x = (float) (var21 * (double) var6 + var23 * (double) var7 + var25 * (double) var8 + var15);
        var2.y = (float) (var21 * (double) var9 + var23 * (double) var10 + var25 * (double) var11 + var17);
        var2.z = (float) (var21 * (double) var12 + var23 * (double) var13 + var25 * (double) var14 + var19);
    }

    public void b(Vec3f var1, Vec3f var2) {
        float var3 = 1.0F / (this.mX.m00 * this.mX.m00 + this.mX.m10 * this.mX.m10 + this.mX.m20 * this.mX.m20);
        float var4 = 1.0F / (this.mX.m01 * this.mX.m01 + this.mX.m11 * this.mX.m11 + this.mX.m21 * this.mX.m21);
        float var5 = 1.0F / (this.mX.m02 * this.mX.m02 + this.mX.m12 * this.mX.m12 + this.mX.m22 * this.mX.m22);
        float var6 = this.mX.m00 * var3;
        float var7 = this.mX.m10 * var4;
        float var8 = this.mX.m20 * var5;
        float var9 = this.mX.m01 * var3;
        float var10 = this.mX.m11 * var4;
        float var11 = this.mX.m21 * var5;
        float var12 = this.mX.m02 * var3;
        float var13 = this.mX.m12 * var4;
        float var14 = this.mX.m22 * var5;
        double var15 = -(this.position.x * (double) this.mX.m00 + this.position.y * (double) this.mX.m10 + this.position.z * (double) this.mX.m20);
        double var17 = -(this.position.x * (double) this.mX.m01 + this.position.y * (double) this.mX.m11 + this.position.z * (double) this.mX.m21);
        double var19 = -(this.position.x * (double) this.mX.m02 + this.position.y * (double) this.mX.m12 + this.position.z * (double) this.mX.m22);
        double var21 = (double) var1.x;
        double var23 = (double) var1.y;
        double var25 = (double) var1.z;
        var2.x = (float) (var21 * (double) var6 + var23 * (double) var7 + var25 * (double) var8 + var15);
        var2.y = (float) (var21 * (double) var9 + var23 * (double) var10 + var25 * (double) var11 + var17);
        var2.z = (float) (var21 * (double) var12 + var23 * (double) var13 + var25 * (double) var14 + var19);
    }

    public void a(Vec3f var1, Vector3dOperations var2) {
        float var3 = 1.0F / (this.mX.m00 * this.mX.m00 + this.mX.m10 * this.mX.m10 + this.mX.m20 * this.mX.m20);
        float var4 = 1.0F / (this.mX.m01 * this.mX.m01 + this.mX.m11 * this.mX.m11 + this.mX.m21 * this.mX.m21);
        float var5 = 1.0F / (this.mX.m02 * this.mX.m02 + this.mX.m12 * this.mX.m12 + this.mX.m22 * this.mX.m22);
        float var6 = this.mX.m00 * var3;
        float var7 = this.mX.m10 * var4;
        float var8 = this.mX.m20 * var5;
        float var9 = this.mX.m01 * var3;
        float var10 = this.mX.m11 * var4;
        float var11 = this.mX.m21 * var5;
        float var12 = this.mX.m02 * var3;
        float var13 = this.mX.m12 * var4;
        float var14 = this.mX.m22 * var5;
        double var15 = -(this.position.x * (double) this.mX.m00 + this.position.y * (double) this.mX.m10 + this.position.z * (double) this.mX.m20);
        double var17 = -(this.position.x * (double) this.mX.m01 + this.position.y * (double) this.mX.m11 + this.position.z * (double) this.mX.m21);
        double var19 = -(this.position.x * (double) this.mX.m02 + this.position.y * (double) this.mX.m12 + this.position.z * (double) this.mX.m22);
        double var21 = (double) var1.x;
        double var23 = (double) var1.y;
        double var25 = (double) var1.z;
        var2.x = (double) ((float) (var21 * (double) var6 + var23 * (double) var7 + var25 * (double) var8 + var15));
        var2.y = (double) ((float) (var21 * (double) var9 + var23 * (double) var10 + var25 * (double) var11 + var17));
        var2.z = (double) ((float) (var21 * (double) var12 + var23 * (double) var13 + var25 * (double) var14 + var19));
    }

    public void getX(Vec3f var1) {
        var1.x = this.mX.m00;
        var1.y = this.mX.m10;
        var1.z = this.mX.m20;
    }

    public void getY(Vec3f var1) {
        var1.x = this.mX.m01;
        var1.y = this.mX.m11;
        var1.z = this.mX.m21;
    }

    public void getZ(Vec3f var1) {
        var1.x = this.mX.m02;
        var1.y = this.mX.m12;
        var1.z = this.mX.m22;
    }

    public void getTranslation(Vector3dOperations var1) {
        var1.x = this.position.x;
        var1.y = this.position.y;
        var1.z = this.position.z;
    }

    public void getTranslation(Vec3f var1) {
        var1.x = (float) this.position.x;
        var1.y = (float) this.position.y;
        var1.z = (float) this.position.z;
    }

    public void setX(Vec3f var1) {
        this.mX.m00 = var1.x;
        this.mX.m10 = var1.y;
        this.mX.m20 = var1.z;
    }

    public void setX(float var1, float var2, float var3) {
        this.mX.m00 = var1;
        this.mX.m10 = var2;
        this.mX.m20 = var3;
    }

    public void setY(Vec3f var1) {
        this.mX.m01 = var1.x;
        this.mX.m11 = var1.y;
        this.mX.m21 = var1.z;
    }

    public void setY(float var1, float var2, float var3) {
        this.mX.m01 = var1;
        this.mX.m11 = var2;
        this.mX.m21 = var3;
    }

    public void setZ(Vec3f var1) {
        this.mX.m02 = var1.x;
        this.mX.m12 = var1.y;
        this.mX.m22 = var1.z;
    }

    public void setZ(float var1, float var2, float var3) {
        this.mX.m02 = var1;
        this.mX.m12 = var2;
        this.mX.m22 = var3;
    }

    public void setTranslation(double var1, double var3, double var5) {
        this.position.x = var1;
        this.position.y = var3;
        this.position.z = var5;
    }

    public void setTranslation(Vector3dOperations var1) {
        this.position.x = var1.x;
        this.position.y = var1.y;
        this.position.z = var1.z;
    }

    public void setTranslation(Vec3f var1) {
        this.position.x = (double) var1.x;
        this.position.y = (double) var1.y;
        this.position.z = (double) var1.z;
    }

    public boolean hasRotation() {
        float var4 = -this.mX.m12;
        float var1;
        if (var4 <= -1.0F) {
            var1 = -1.570796F;
        } else if ((double) var4 >= 1.0D) {
            var1 = 1.570796F;
        } else {
            var1 = (float) Math.asin((double) var4);
        }

        float var2;
        float var3;
        if (var4 > 0.9999F) {
            var3 = 0.0F;
            var2 = (float) Math.atan2((double) (-this.mX.m02), (double) this.mX.m00);
        } else {
            var2 = (float) Math.atan2((double) this.mX.m02, (double) this.mX.m22);
            var3 = (float) Math.atan2((double) this.mX.m10, (double) this.mX.m11);
        }

        return var2 != 0.0F || var3 != 0.0F || var1 != 0.0F;
    }

    public void a(bc_q var1) {
        ajD var2 = this.mX;
        float var3 = 1.0F / (var2.m00 * var2.m00 + var2.m10 * var2.m10 + var2.m20 * var2.m20);
        float var4 = 1.0F / (var2.m01 * var2.m01 + var2.m11 * var2.m11 + var2.m21 * var2.m21);
        float var5 = 1.0F / (var2.m02 * var2.m02 + var2.m12 * var2.m12 + var2.m22 * var2.m22);
        ajD var6 = var1.mX;
        var6.m00 = var2.m00 * var3;
        var6.m01 = var2.m10 * var4;
        var6.m02 = var2.m20 * var5;
        var6.m10 = var2.m01 * var3;
        var6.m11 = var2.m11 * var4;
        var6.m12 = var2.m21 * var5;
        var6.m20 = var2.m02 * var3;
        var6.m21 = var2.m12 * var4;
        var6.m22 = var2.m22 * var5;
        double var7 = this.position.x * (double) var3;
        double var9 = this.position.y * (double) var4;
        double var11 = this.position.z * (double) var5;
        var1.position.x = -(var7 * (double) var2.m00 + var9 * (double) var2.m10 + var11 * (double) var2.m20);
        var1.position.y = -(var7 * (double) var2.m01 + var9 * (double) var2.m11 + var11 * (double) var2.m21);
        var1.position.z = -(var7 * (double) var2.m02 + var9 * (double) var2.m12 + var11 * (double) var2.m22);
    }

    public float determinant3x3() {
        ajD var1 = this.mX;
        return var1.m00 * (var1.m11 * var1.m22 - var1.m12 * var1.m21) + var1.m01 * (var1.m12 * var1.m20 - var1.m10 * var1.m22) + var1.m02 * (var1.m10 * var1.m21 - var1.m11 * var1.m20);
    }

    public boolean isValidRotation() {
        float var1 = Math.abs(1.0F - this.determinant3x3());
        return (double) var1 >= 1.0E-6D && var1 == var1;
    }

    public String toString() {
        return String.format(" %4.4f %4.4f %4.4f %4.4f\n %4.4f %4.4f %4.4f %4.4f\n %4.4f %4.4f %4.4f %4.4f\n %4.4f %4.4f %4.4f %4.4f\n", this.mX.m00, this.mX.m01, this.mX.m02, this.position.x, this.mX.m10, this.mX.m11, this.mX.m12, this.position.y, this.mX.m20, this.mX.m21, this.mX.m22, this.position.z, 0.0F, 0.0F, 0.0F, 1.0F);
    }

    public void b(bc_q var1) {
        this.mX.set(var1.mX);
        this.position.aA(var1.position);
    }

    public void set(ajK var1) {
        this.mX.m00 = var1.m00;
        this.mX.m10 = var1.m10;
        this.mX.m20 = var1.m20;
        this.mX.m01 = var1.m01;
        this.mX.m11 = var1.m11;
        this.mX.m21 = var1.m21;
        this.mX.m02 = var1.m02;
        this.mX.m12 = var1.m12;
        this.mX.m22 = var1.m22;
        this.position.x = (double) var1.m03;
        this.position.y = (double) var1.m13;
        this.position.z = (double) var1.m23;
    }

    public void set3x3(ajK var1) {
        this.mX.m00 = var1.m00;
        this.mX.m10 = var1.m10;
        this.mX.m20 = var1.m20;
        this.mX.m01 = var1.m01;
        this.mX.m11 = var1.m11;
        this.mX.m21 = var1.m21;
        this.mX.m02 = var1.m02;
        this.mX.m12 = var1.m12;
        this.mX.m22 = var1.m22;
    }

    public bc_q a(ajK var1) {
        this.set3x3(var1);
        return this;
    }

    public bc_q c(bc_q var1) {
        this.b(var1);
        return this;
    }

    public void multiply3x4(Vector3dOperations var1, Vector3dOperations var2) {
        double var3 = var1.x;
        double var5 = var1.y;
        double var7 = var1.z;
        var2.x = var3 * (double) this.mX.m00 + var5 * (double) this.mX.m01 + var7 * (double) this.mX.m02 + this.position.x;
        var2.y = var3 * (double) this.mX.m10 + var5 * (double) this.mX.m11 + var7 * (double) this.mX.m12 + this.position.y;
        var2.z = var3 * (double) this.mX.m20 + var5 * (double) this.mX.m21 + var7 * (double) this.mX.m22 + this.position.z;
    }

    public void multiply3x4(Vec3f var1, Vector3dOperations var2) {
        double var3 = (double) var1.x;
        double var5 = (double) var1.y;
        double var7 = (double) var1.z;
        var2.x = var3 * (double) this.mX.m00 + var5 * (double) this.mX.m01 + var7 * (double) this.mX.m02 + this.position.x;
        var2.y = var3 * (double) this.mX.m10 + var5 * (double) this.mX.m11 + var7 * (double) this.mX.m12 + this.position.y;
        var2.z = var3 * (double) this.mX.m20 + var5 * (double) this.mX.m21 + var7 * (double) this.mX.m22 + this.position.z;
    }

    public void a(aJF var1, aJF var2) {
        float var3 = var1.x;
        float var4 = var1.y;
        float var5 = var1.z;
        var2.x = (float) ((double) (var3 * this.mX.m00 + var4 * this.mX.m01 + var5 * this.mX.m02) + this.position.x);
        var2.y = (float) ((double) (var3 * this.mX.m10 + var4 * this.mX.m11 + var5 * this.mX.m12) + this.position.y);
        var2.z = (float) ((double) (var3 * this.mX.m20 + var4 * this.mX.m21 + var5 * this.mX.m22) + this.position.z);
        var2.w = 1.0F;
    }

    public void a(aJD_q var1, aJD_q var2) {
        double var3 = var1.x;
        double var5 = var1.y;
        double var7 = var1.z;
        var2.x = var3 * (double) this.mX.m00 + var5 * (double) this.mX.m01 + var7 * (double) this.mX.m02 + this.position.x;
        var2.y = var3 * (double) this.mX.m10 + var5 * (double) this.mX.m11 + var7 * (double) this.mX.m12 + this.position.y;
        var2.z = var3 * (double) this.mX.m20 + var5 * (double) this.mX.m21 + var7 * (double) this.mX.m22 + this.position.z;
        var2.w = 1.0D;
    }

    public void normalizedMultiply3x4(Vec3f var1, Vector3dOperations var2) {
        float var3 = var1.x;
        float var4 = var1.y;
        float var5 = var1.z;
        float var6 = (float) (1.0D / Math.sqrt((double) (this.mX.m00 * this.mX.m00 + this.mX.m01 * this.mX.m01 + this.mX.m02 * this.mX.m02)));
        float var7 = (float) (1.0D / Math.sqrt((double) (this.mX.m10 * this.mX.m10 + this.mX.m11 * this.mX.m11 + this.mX.m02 * this.mX.m12)));
        float var8 = (float) (1.0D / Math.sqrt((double) (this.mX.m20 * this.mX.m20 + this.mX.m21 * this.mX.m21 + this.mX.m22 * this.mX.m22)));
        var2.x = (double) (var3 * this.mX.m00 * var6 + var4 * this.mX.m01 * var7 + var5 * this.mX.m02 * var8);
        var2.y = (double) (var3 * this.mX.m10 * var6 + var4 * this.mX.m11 * var7 + var5 * this.mX.m12 * var8);
        var2.z = (double) (var3 * this.mX.m20 * var6 + var4 * this.mX.m12 * var7 + var5 * this.mX.m22 * var8);
    }

    public void normalizedMultiply3x4(Vec3f var1, Vec3f var2) {
        float var3 = var1.x;
        float var4 = var1.y;
        float var5 = var1.z;
        float var6 = (float) (1.0D / Math.sqrt((double) (this.mX.m00 * this.mX.m00 + this.mX.m01 * this.mX.m01 + this.mX.m02 * this.mX.m02)));
        float var7 = (float) (1.0D / Math.sqrt((double) (this.mX.m10 * this.mX.m10 + this.mX.m11 * this.mX.m11 + this.mX.m02 * this.mX.m12)));
        float var8 = (float) (1.0D / Math.sqrt((double) (this.mX.m20 * this.mX.m20 + this.mX.m21 * this.mX.m21 + this.mX.m22 * this.mX.m22)));
        var2.x = var3 * this.mX.m00 * var6 + var4 * this.mX.m01 * var7 + var5 * this.mX.m02 * var8;
        var2.y = var3 * this.mX.m10 * var6 + var4 * this.mX.m11 * var7 + var5 * this.mX.m12 * var8;
        var2.z = var3 * this.mX.m20 * var6 + var4 * this.mX.m12 * var7 + var5 * this.mX.m22 * var8;
    }

    public void y(float var1) {
        this.mX.rotateX(var1 * 0.017453292F);
    }

    public void z(float var1) {
        this.mX.rotateY(var1 * 0.017453292F);
    }

    public void A(float var1) {
        this.mX.rotateZ(var1 * 0.017453292F);
    }

    public void setIdentity() {
        this.mX.setIdentity();
        this.position.set(0.0D, 0.0D, 0.0D);
    }

    public void asColumnMajorBuffer(FloatBuffer var1) {
        var1.put(this.mX.m00);
        var1.put(this.mX.m10);
        var1.put(this.mX.m20);
        var1.put(0.0F);
        var1.put(this.mX.m01);
        var1.put(this.mX.m11);
        var1.put(this.mX.m21);
        var1.put(0.0F);
        var1.put(this.mX.m02);
        var1.put(this.mX.m12);
        var1.put(this.mX.m22);
        var1.put(0.0F);
        var1.put((float) this.position.x);
        var1.put((float) this.position.y);
        var1.put((float) this.position.z);
        var1.put(1.0F);
    }

    public void setOrientation(OrientationBase var1) {
        float var2 = var1.w;
        float var3 = var1.x;
        float var4 = var1.y;
        float var5 = var1.z;
        double var6 = (double) (var2 * var2 + var3 * var3 + var4 * var4 + var5 * var5);
        if (!isZero(1.0F - (float) var6)) {
            double var8 = 1.0D / Math.sqrt(var6);
            var2 = (float) ((double) var2 * var8);
            var3 = (float) ((double) var3 * var8);
            var4 = (float) ((double) var4 * var8);
            var5 = (float) ((double) var5 * var8);
        }

        float var17 = var3 * var3;
        float var9 = var3 * var4;
        float var10 = var3 * var5;
        float var11 = var2 * var3;
        float var12 = var4 * var4;
        float var13 = var4 * var5;
        float var14 = var4 * var2;
        float var15 = var5 * var5;
        float var16 = var5 * var2;
        this.mX.m00 = 1.0F - 2.0F * (var12 + var15);
        this.mX.m01 = 2.0F * (var9 - var16);
        this.mX.m02 = 2.0F * (var10 + var14);
        this.mX.m10 = 2.0F * (var9 + var16);
        this.mX.m11 = 1.0F - 2.0F * (var17 + var15);
        this.mX.m12 = 2.0F * (var13 - var11);
        this.mX.m20 = 2.0F * (var10 - var14);
        this.mX.m21 = 2.0F * (var13 + var11);
        this.mX.m22 = 1.0F - 2.0F * (var17 + var12);
    }

    public void a(Vector3dOperations var1) {
        this.mX.transform(var1);
        var1.add(this.position);
    }

    public void inverse() {
        this.mX.transpose();
        this.position.scale(-1.0D);
        this.mX.transform(this.position);
    }

    public void d(bc_q var1) {
        this.b(var1);
        this.inverse();
    }

    public void e(bc_q var1) {
        a(this, var1, this);
    }

    public void a(bc_q var1, bc_q var2) {
        a(var1, var2, this);
    }

    public Quat4f c(Quat4f var1) {
        var1.set(this.mX);
        return var1;
    }

    public void a(OrientationBase var1) {
        this.setOrientation(var1);
    }

    public OrientationBase toQuaternion() {
        return new OrientationBase(this.mX);
    }

    public Vec3f getVectorX() {
        Vec3f var1 = new Vec3f();
        this.getX(var1);
        return var1;
    }

    public Vec3f getVectorY() {
        Vec3f var1 = new Vec3f();
        this.getY(var1);
        return var1;
    }

    public Vec3f getVectorZ() {
        Vec3f var1 = new Vec3f();
        this.getZ(var1);
        return var1;
    }

    public double f(bc_q var1) {
        return Math.sqrt(this.g(var1));
    }

    public double g(bc_q var1) {
        double var2 = this.position.x - var1.position.x;
        var2 *= var2;
        double var4 = this.position.y - var1.position.y;
        var4 *= var4;
        double var6 = this.position.z - var1.position.z;
        var6 *= var6;
        return var2 + var4 + var6;
    }

    public void a(ajD var1) {
        this.mX.set(var1);
    }

    public double gk() {
        return Math.sqrt((double) (this.mX.m00 * this.mX.m00 + this.mX.m10 * this.mX.m10 + this.mX.m20 * this.mX.m20));
    }

    public double gm() {
        return Math.sqrt((double) (this.mX.m01 * this.mX.m01 + this.mX.m11 * this.mX.m11 + this.mX.m21 * this.mX.m21));
    }

    public double gn() {
        return Math.sqrt((double) (this.mX.m02 * this.mX.m02 + this.mX.m12 * this.mX.m12 + this.mX.m22 * this.mX.m22));
    }

    public void B(float var1) {
        this.mX.m00 *= var1;
        this.mX.m10 *= var1;
        this.mX.m20 *= var1;
    }

    public void C(float var1) {
        this.mX.m01 *= var1;
        this.mX.m11 *= var1;
        this.mX.m21 *= var1;
    }

    public void D(float var1) {
        this.mX.m02 *= var1;
        this.mX.m12 *= var1;
        this.mX.m22 *= var1;
    }

    public void b(Vector3dOperations var1, Vector3dOperations var2) {
        var2.x = var1.x * (double) this.mX.m00 + var1.y * (double) this.mX.m01 + var1.z * (double) this.mX.m02 + this.position.x;
        var2.y = var1.x * (double) this.mX.m10 + var1.y * (double) this.mX.m11 + var1.z * (double) this.mX.m12 + this.position.y;
        var2.z = var1.x * (double) this.mX.m20 + var1.y * (double) this.mX.m21 + var1.z * (double) this.mX.m22 + this.position.z;
    }

    public void b(aJF var1, aJF var2) {
        bc_q var3 = new bc_q();
        this.a(var3);
        var3.a(var1, var2);
    }

    public void b(aJD_q var1, aJD_q var2) {
        bc_q var3 = new bc_q();
        this.a(var3);
        var3.a(var1, var2);
    }

    public void a(float var1, float var2, float var3, float var4, float var5, float var6, float var7, float var8, float var9) {
        this.mX.m00 = var1;
        this.mX.m10 = var2;
        this.mX.m20 = var3;
        this.mX.m01 = var4;
        this.mX.m11 = var5;
        this.mX.m21 = var6;
        this.mX.m02 = var7;
        this.mX.m12 = var8;
        this.mX.m22 = var9;
    }

    public void readExternal(Vm_q var1) {
        this.mX = (ajD) var1.he("orientation");
        this.position = (Vector3dOperations) var1.he("position");
    }

    public void writeExternal(att var1) {
        var1.a("orientation", (afA_q) this.mX);
        var1.a("position", (afA_q) this.position);
    }
}
