package all;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.*;

public class gi implements Serializable {

    private static final String Pz = "The Nestable implementation passed to_q the NestableDelegate(Nestable) constructor must extend java.lang.Throwable";
    private static boolean PB = true;
    private static boolean PC = true;
    private Throwable PA = null;

    public gi(aIa var1) {
        if (var1 instanceof Throwable) {
            this.PA = (Throwable) var1;
        } else {
            throw new IllegalArgumentException("The Nestable implementation passed to_q the NestableDelegate(Nestable) constructor must extend java.lang.Throwable");
        }
    }

    public String bg(int var1) {
        Throwable var2 = this.bh(var1);
        return aIa.class.isInstance(var2) ? ((aIa) var2).bg(0) : var2.getMessage();
    }

    public String getMessage(String var1) {
        StringBuffer var2 = new StringBuffer();
        if (var1 != null) {
            var2.append(var1);
        }

        Throwable var3 = afC.getCause(this.PA);
        if (var3 != null) {
            String var4 = var3.getMessage();
            if (var4 != null) {
                if (var1 != null) {
                    var2.append(": ");
                }

                var2.append(var4);
            }
        }

        return var2.length() > 0 ? var2.toString() : null;
    }

    public String[] getMessages() {
        Throwable[] var1 = this.vo();
        String[] var2 = new String[var1.length];

        for (int var3 = 0; var3 < var1.length; ++var3) {
            var2[var3] = aIa.class.isInstance(var1[var3]) ? ((aIa) var1[var3]).bg(0) : var1[var3].getMessage();
        }

        return var2;
    }

    public Throwable bh(int var1) {
        if (var1 == 0) {
            return this.PA;
        } else {
            Throwable[] var2 = this.vo();
            return var2[var1];
        }
    }

    public int vn() {
        return afC.g(this.PA);
    }

    public Throwable[] vo() {
        return afC.h(this.PA);
    }

    public int a(Class var1, int var2) {
        if (var2 < 0) {
            throw new IndexOutOfBoundsException("The start index was out of bounds: " + var2);
        } else {
            Throwable[] var3 = afC.h(this.PA);
            if (var2 >= var3.length) {
                throw new IndexOutOfBoundsException("The start index was out of bounds: " + var2 + " >= " + var3.length);
            } else {
                for (int var4 = var2; var4 < var3.length; ++var4) {
                    if (var3[var4].getClass().equals(var1)) {
                        return var4;
                    }
                }

                return -1;
            }
        }
    }

    public void printStackTrace() {
        this.printStackTrace(System.err);
    }

    public void printStackTrace(PrintStream var1) {
        synchronized (var1) {
            PrintWriter var3 = new PrintWriter(var1, false);
            this.printStackTrace(var3);
            var3.flush();
        }
    }

    public void printStackTrace(PrintWriter var1) {
        Throwable var2 = this.PA;
        if (afC.bVB()) {
            if (var2 instanceof aIa) {
                ((aIa) var2).c(var1);
            } else {
                var2.printStackTrace(var1);
            }

        } else {
            ArrayList var3;
            for (var3 = new ArrayList(); var2 != null; var2 = afC.getCause(var2)) {
                String[] var4 = this.b(var2);
                var3.add(var4);
            }

            String var11 = "Caused by: ";
            if (!PB) {
                var11 = "Rethrown as: ";
                Collections.reverse(var3);
            }

            if (PC) {
                this.d(var3);
            }

            synchronized (var1) {
                Iterator var6 = var3.iterator();

                while (var6.hasNext()) {
                    String[] var7 = (String[]) var6.next();
                    int var8 = 0;

                    for (int var9 = var7.length; var8 < var9; ++var8) {
                        var1.println(var7[var8]);
                    }

                    if (var6.hasNext()) {
                        var1.print(var11);
                    }
                }

            }
        }
    }

    protected String[] b(Throwable var1) {
        StringWriter var2 = new StringWriter();
        PrintWriter var3 = new PrintWriter(var2, true);
        if (var1 instanceof aIa) {
            ((aIa) var1).c(var3);
        } else {
            var1.printStackTrace(var3);
        }

        return afC.iF(var2.getBuffer().toString());
    }

    protected void d(List var1) {
        int var2 = var1.size();

        for (int var3 = var2 - 1; var3 > 0; --var3) {
            String[] var4 = (String[]) var1.get(var3);
            String[] var5 = (String[]) var1.get(var3 - 1);
            ArrayList var6 = new ArrayList((Collection) Arrays.asList(var4));
            ArrayList var7 = new ArrayList((Collection) Arrays.asList(var5));
            afC.d(var6, var7);
            int var8 = var4.length - var6.size();
            if (var8 > 0) {
                var6.add("\t... " + var8 + " more");
                var1.set(var3, (String[]) var6.toArray(new String[var6.size()]));
            }
        }

    }
}
