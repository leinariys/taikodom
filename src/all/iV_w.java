package all;

import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * class LoginAddon
 */
public class iV_w extends WA {

    private final ahM Zl;

    public iV_w() {
        this((ahM) null);
    }

    public iV_w(ahM var1) {
        this.Zl = var1;
    }

    public ahM BJ() {
        return this.Zl;
    }

    public void readExternal(ObjectInput var1) {
        throw new UnsupportedOperationException("Do not use this class serverside");
    }

    public void writeExternal(ObjectOutput var1) {
        throw new UnsupportedOperationException("Do not use this class serverside");
    }
}
