package all;

import gnu.trove.THashMap;
import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.css.CSSValue;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class aca extends avI {
    protected Map cache;//Список свойств селектора Пример color-alpha: 1; ...
    protected List list;//Список CSS стилей которы применимы к графическому объекты Обычно  core.css и css файл аддона например login.css
    private aeK fao;//кистема событий графичекого элемента

    public aca(aeK var1, CSSStyleDeclaration var2) {
        this.fao = var1;
        aQ var3 = new aQ((aQ) null);
        this.cache = new THashMap();
        if (this.fao != null && var2 != null) {
            var3.c(var2);
        }

    }

    public aca(aeK var1) {
        this.fao = var1;
        this.cache = new THashMap();
        this.list = new ArrayList(2);
        aQ var2 = new aQ((aQ) null);

        avI var3;
        for (Object var4 = var1; var4 != null; var4 = ((asi) var4).Vm()) {
            var3 = ((aeK) var4).getCssNode();
            if (var3 != null && !this.list.contains(var3)) {
                this.list.add(var3);
                var3.a(this.fao, (aKV_q) var2, false);//работа с заголовки селекторов
            }
        }

        var3 = BaseUItegXML.thisClass.getCssNodeCore();//получили ядро CSS стилей
        if (var3 != null && !this.list.contains(var3)) {
            this.list.add(var3);
            var3.a(this.fao, (aKV_q) var2, false);
        }

    }

    //Проверка задан ли такой css параметр
    public CSSValue a(RC var1, String var2, boolean var3) {
        CSSValue var4 = null;
        if (var1 == this.fao) {
            var4 = (CSSValue) this.cache.get(var2);
        } else if (this.list != null) {
            Iterator var6 = this.list.iterator();

            while (var6.hasNext()) {
                avI var5 = (avI) var6.next();
                var4 = var5.a(var1, var2, var3);
                if (var4 != null) {
                    return var4;
                }
            }
        }
        //Проверка был ли найден css стиль
        return var3 && (var4 == null || "".equals(var4.getCssText())) && var1.Vm() != null ? this.a((RC) var1.Vm(), var2, var3) : var4;
    }

    public boolean a(RC var1, aKV_q var2, boolean var3) {
        return false;
    }

    public aeK bOk() {
        return this.fao;
    }

    public void b(aeK var1) {
        this.fao = var1;
    }

    private final class aQ implements aKV_q {
        private aQ() {
        }

        // $FF: synthetic method
        aQ(aQ var2) {
            this();
        }

        public boolean c(CSSStyleDeclaration var1) {
            if (var1 == null) {
                return true;
            } else {
                aHx var2 = (aHx) var1;
                int var3 = var1.getLength();

                while (true) {
                    --var3;
                    if (var3 < 0) {
                        return true;
                    }

                    PropertyCss var4 = var2.yh(var3);
                    if (var4 != null && var4.getCssValue() != null && !aca.this.cache.containsKey(var4.getName())) {
                        aca.this.cache.put(var4.getName(), var4.getCssValue());
                    }
                }
            }
        }
    }
}
