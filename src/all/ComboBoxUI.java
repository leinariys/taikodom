package all;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicComboBoxRenderer.UIResource;
import javax.swing.plaf.basic.BasicComboBoxUI;
import javax.swing.plaf.basic.BasicComboPopup;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * ComboBoxUI
 */
public class ComboBoxUI extends BasicComboBoxUI implements aIh {
    private final JComboBox dgo;
    protected boolean dgp;
    private ComponentManager Rp;

    public ComboBoxUI(JComboBox var1) {
        this.dgo = var1;
        this.Rp = new ComponentManager("combobox", var1);
        this.currentValuePane = new aqV() {
            private static final long serialVersionUID = 1L;

            public void paintComponent(Graphics var1, Component var2, Container var3, int var4, int var5, int var6, int var7, boolean var8) {
                int var9 = ComboBoxUI.this.dgp ? 32 : 0;
                this.grc.q(32, var9);
                aeK var10 = ComponentManager.getCssHolder(var2);
                if (var10 != null) {
                    var10.q(32, var9);
                }

                super.paintComponent(var1, var2, var3, var4, var5, var6, var7, var8);
                if (var10 != null) {
                    var10.q(0, 0);
                }

            }
        };
        var1.setOpaque(false);
    }

    public static ComponentUI createUI(JComponent var0) {
        return new ComboBoxUI((JComboBox) var0);
    }

    // $FF: synthetic method
    static JComboBox b(ComboBoxUI var0) {
        return var0.comboBox;
    }

    public void update(Graphics var1, JComponent var2) {
        RM.a((aIh) this, (Graphics) var1, (JComponent) var2);
        this.paint(var1, var2);
    }

    public void installUI(JComponent var1) {
        super.installUI(var1);
        var1.addMouseListener(new MouseAdapter() {
            public void mouseEntered(MouseEvent var1) {
                if (!ComboBoxUI.this.dgp) {
                    ComboBoxUI.this.dgp = true;
                    ComboBoxUI.b(ComboBoxUI.this).repaint(150L);
                }

            }

            public void mouseExited(MouseEvent var1) {
                if (ComboBoxUI.this.dgp) {
                    ComboBoxUI.this.dgp = false;
                    ComboBoxUI.b(ComboBoxUI.this).repaint(150L);
                }

            }
        });
    }

    public void uninstallUI(JComponent var1) {
        super.uninstallUI(var1);
    }

    public Dimension getMinimumSize(JComponent var1) {
        RM.a((aIh) this, (JComponent) var1);
        return this.wy().b(var1, super.getMinimumSize(var1));
    }

    public Dimension getMaximumSize(JComponent var1) {
        RM.a((aIh) this, (JComponent) var1);
        return this.wy().a(var1, super.getMaximumSize(var1));
    }

    public Dimension getPreferredSize(JComponent var1) {
        RM.a((aIh) this, (JComponent) var1);
        return this.Rp.c(var1, super.getPreferredSize(var1));
    }

    public ComponentManager wy() {
        return this.Rp;
    }

    protected ComboBoxEditor createEditor() {
        ComboBoxEditor var1 = super.createEditor();
        Component var2 = var1.getEditorComponent();
        if (var2 instanceof JTextField) {
            ((JTextField) var1.getEditorComponent()).setBorder(new BorderWrapper());
        }

        return var1;
    }

    protected ListCellRenderer createRenderer() {
        a var1 = new a((a) null);
        return var1;
    }

    protected JButton createArrowButton() {
        JButton var1 = new JButton();
        var1.setFocusable(false);
        this.dgo.add(var1);
        return var1;
    }

    public void paint(Graphics var1, JComponent var2) {
        super.paint(var1, var2);
    }

    public void paintCurrentValue(Graphics var1, Rectangle var2, boolean var3) {
        super.paintCurrentValue(var1, var2, var3);
    }

    /**
     * TODO похлже это public pv wy()
     *
     * @return Графический элемент
     */
    @Override
    public aeK wz() {
        return this.Rp;
    }

    private final class a extends UIResource implements IComponentCustomWrapper {
        private static final long serialVersionUID = 1L;

        private a() {
        }

        // $FF: synthetic method
        a(a var2) {
            this();
        }

        public Container getParent() {
            return ComboBoxUI.this.dgo;
        }

        public String getElementName() {
            return "text";
        }
    }

    protected class b extends BasicComboPopup {
        private static final long serialVersionUID = 1L;

        public b(JComboBox var2) {
            super(var2);
        }
    }
}
