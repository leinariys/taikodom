package all;

import javax.swing.*;
import java.awt.*;

public class SliderCustom extends BaseItem {
    private static final LogPrinter logger = LogPrinter.setClass(SliderCustom.class);

    /**
     * Создание базового Component интерфейса
     * Вызывается из class BaseItem
     *
     * @param var1
     * @param var2
     * @param var3
     * @return
     */
    @Override
    protected JComponent CreatComponentCustom(MapKeyValue var1, Container var2, XmlNode var3) {
        return new JSlider();
    }

    protected void a(MapKeyValue var1, JSlider var2, XmlNode var3) {
        super.applyAttribute(var1, (Component) var2, (XmlNode) var3);
        Integer var4 = checkValueAttribut((XmlNode) var3, (String) "min", (LogPrinter) logger);
        if (var4 != null) {
            var2.setMinimum(var4.intValue());
        }

        var4 = checkValueAttribut((XmlNode) var3, (String) "max", (LogPrinter) logger);
        if (var4 != null) {
            var2.setMaximum(var4.intValue());
        }

        var4 = checkValueAttribut((XmlNode) var3, (String) "value", (LogPrinter) logger);
        if (var4 != null) {
            var2.setValue(var4.intValue());
        }

        var4 = checkValueAttribut((XmlNode) var3, (String) "major-ticks", (LogPrinter) logger);
        if (var4 != null) {
            var2.setMajorTickSpacing(var4.intValue());
        }

        var4 = checkValueAttribut((XmlNode) var3, (String) "minor-ticks", (LogPrinter) logger);
        if (var4 != null) {
            var2.setMinorTickSpacing(var4.intValue());
        }

        if ("true".equals(var3.getAttribute("snap-to_q-ticks"))) {
            var2.setSnapToTicks(true);
        }

        if ("true".equals(var3.getAttribute("show-ticks"))) {
            var2.setPaintTicks(true);
        }

        String var5 = var3.getAttribute("orientation");
        var2.setOrientation("vertical".equals(var5) ? 1 : 0);
    }
}
