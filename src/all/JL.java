package all;

import com.hoplon.geometry.Vec3f;

import javax.vecmath.Vector4f;

public class JL {
    public static int W(Vec3f var0) {
        byte var1 = -1;
        float var2 = -1.0E30F;
        if (var0.x > var2) {
            var1 = 0;
            var2 = var0.x;
        }

        if (var0.y > var2) {
            var1 = 1;
            var2 = var0.y;
        }

        if (var0.z > var2) {
            var1 = 2;
            var2 = var0.z;
        }

        return var1;
    }

    public static int b(Vector4f var0) {
        byte var1 = -1;
        float var2 = -1.0E30F;
        if (var0.x > var2) {
            var1 = 0;
            var2 = var0.x;
        }

        if (var0.y > var2) {
            var1 = 1;
            var2 = var0.y;
        }

        if (var0.z > var2) {
            var1 = 2;
            var2 = var0.z;
        }

        if (var0.w > var2) {
            var1 = 3;
            var2 = var0.w;
        }

        return var1;
    }

    public static int c(Vector4f var0) {
        Vector4f var1 = new Vector4f(var0);
        var1.absolute();
        return b(var1);
    }

    public static float b(Vec3f var0, int var1) {
        switch (var1) {
            case 0:
                return var0.x;
            case 1:
                return var0.y;
            case 2:
                return var0.z;
            default:
                throw new InternalError();
        }
    }

    public static void a(Vec3f var0, int var1, float var2) {
        switch (var1) {
            case 0:
                var0.x = var2;
                break;
            case 1:
                var0.y = var2;
                break;
            case 2:
                var0.z = var2;
                break;
            default:
                throw new InternalError();
        }

    }

    public static void b(Vec3f var0, int var1, float var2) {
        switch (var1) {
            case 0:
                var0.x *= var2;
                break;
            case 1:
                var0.y *= var2;
                break;
            case 2:
                var0.z *= var2;
                break;
            default:
                throw new InternalError();
        }

    }

    public static void a(Vec3f var0, Vec3f var1, Vec3f var2, float var3) {
        float var4 = 1.0F - var3;
        var0.x = var4 * var1.x + var3 * var2.x;
        var0.y = var4 * var1.y + var3 * var2.y;
        var0.z = var4 * var1.z + var3 * var2.z;
    }

    public static void f(Vec3f var0, Vec3f var1, Vec3f var2) {
        var0.x = var1.x + var2.x;
        var0.y = var1.y + var2.y;
        var0.z = var1.z + var2.z;
    }

    public static void f(Vec3f var0, Vec3f var1, Vec3f var2, Vec3f var3) {
        var0.x = var1.x + var2.x + var3.x;
        var0.y = var1.y + var2.y + var3.y;
        var0.z = var1.z + var2.z + var3.z;
    }

    public static void a(Vec3f var0, Vec3f var1, Vec3f var2, Vec3f var3, Vec3f var4) {
        var0.x = var1.x + var2.x + var3.x + var4.x;
        var0.y = var1.y + var2.y + var3.y + var4.y;
        var0.z = var1.z + var2.z + var3.z + var4.z;
    }

    public static void g(Vec3f var0, Vec3f var1, Vec3f var2) {
        var0.x = var1.x * var2.x;
        var0.y = var1.y * var2.y;
        var0.z = var1.z * var2.z;
    }

    public static void h(Vec3f var0, Vec3f var1, Vec3f var2) {
        var0.x = var1.x / var2.x;
        var0.y = var1.y / var2.y;
        var0.z = var1.z / var2.z;
    }

    public static void o(Vec3f var0, Vec3f var1) {
        var0.x = Math.min(var0.x, var1.x);
        var0.y = Math.min(var0.y, var1.y);
        var0.z = Math.min(var0.z, var1.z);
    }

    public static void p(Vec3f var0, Vec3f var1) {
        var0.x = Math.max(var0.x, var1.x);
        var0.y = Math.max(var0.y, var1.y);
        var0.z = Math.max(var0.z, var1.z);
    }

    public static float a(Vector4f var0, Vec3f var1) {
        return var0.x * var1.x + var0.y * var1.y + var0.z * var1.z;
    }

    public static float b(Vector4f var0, Vector4f var1) {
        return var0.x * var1.x + var0.y * var1.y + var0.z * var1.z;
    }

    public static float d(Vector4f var0) {
        return var0.x * var0.x + var0.y * var0.y + var0.z * var0.z;
    }

    public static void e(Vector4f var0) {
        float var1 = (float) (1.0D / Math.sqrt((double) (var0.x * var0.x + var0.y * var0.y + var0.z * var0.z)));
        var0.x *= var1;
        var0.y *= var1;
        var0.z *= var1;
    }

    public static void a(Vec3f var0, Vector4f var1, Vector4f var2) {
        float var3 = var1.y * var2.z - var1.z * var2.y;
        float var4 = var2.x * var1.z - var2.z * var1.x;
        var0.z = var1.x * var2.y - var1.y * var2.x;
        var0.x = var3;
        var0.y = var4;
    }
}
