package all;

import gnu.trove.THashSet;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class Kd {
    private static final THashSet dnK = new THashSet();
    public static Kd dnL = new Kd();
    static LogPrinter logger = LogPrinter.K(Kd.class);
    protected Class clazz;
    private boolean agW;
    private Object defaultValue = null;
    private Class[] aha;
    private boolean ahc;
    private boolean dnM = true;

    protected static final void o(String var0, Object var1) {
        Class var2 = var1.getClass();
        if (!dnK.contains(var2)) {
            dnK.add(var2);
            logger.info(var0 + var2);
        }

    }

    protected static final void d(String var0, Class var1) {
        if (!dnK.contains(var1)) {
            dnK.add(var1);
            logger.info(var0 + var1);
        }

    }

    public static int b(ObjectInput var0, int var1) throws IOException {
        if (var1 <= 255) {
            return var0.readUnsignedByte();
        } else if (var1 <= 65535) {
            return var0.readUnsignedShort();
        } else {
            return var1 <= 16777215 ? var0.readUnsignedShort() << 8 | var0.readUnsignedByte() : var0.readInt();
        }
    }

    public static void a(ObjectOutput var0, int var1, int var2) throws IOException {
        if (var1 <= 255) {
            if (var2 < 0 || var2 > 255) {
                throw new IllegalArgumentException();
            }

            var0.writeByte(var2);
        } else if (var1 <= 65535) {
            if (var2 < 0 || var2 > 65535) {
                throw new IllegalArgumentException();
            }

            var0.writeShort(var2);
        } else if (var1 <= 16777215) {
            if (var2 < 0 || var2 > 16777215) {
                throw new IllegalArgumentException();
            }

            var0.writeShort(var2 >> 8);
            var0.writeByte(var2 & 255);
        } else {
            var0.writeInt(255);
        }

    }

    public boolean isImmutable() {
        return false;
    }

    void a(Class var1, Class[] var2) {
        this.aha = var2;
        this.clazz = var1;
        this.ahc = aOm.class.isAssignableFrom(var1);
        this.agW = AO.class.isAssignableFrom(var1);
    }

    public Class getType() {
        return this.clazz;
    }

    public boolean isCollection() {
        return this.agW;
    }

    public Object getDefaultValue() {
        return this.defaultValue;
    }

    public void A(Object var1) {
        this.defaultValue = var1;
    }

    public String toString() {
        return this.clazz.getName();
    }

    public Class[] Eq() {
        return this.aha;
    }

    public void a(ObjectOutput var1, Object var2) throws IOException {
        if (this.dnM && var2 != null) {
            o("Consider making all special serializer for ", var2);
            this.dnM = false;
        }

        var1.writeObject(var2);
    }

    public Object b(ObjectInput var1) throws IOException, ClassNotFoundException {
        return var1.readObject();
    }

    public Object z(Object var1) {
        if (var1 == null) {
            return null;
        } else if (this.ahc) {
            return ((aOm) var1).Jq();
        } else {
            throw new RuntimeException(new CloneNotSupportedException());
        }
    }

    public boolean yl() {
        return this.ahc;
    }

    public Object b(Xf_q var1) {
        return null;
    }

    public void a(att var1, Object var2) {
        if (this.dnM && var2 != null) {
            d("Consider making all special asym serializer for ", this.clazz);
            this.dnM = false;
        }

        var1.g("unknown", var2);
    }

    public Object a(String var1, Vm_q var2) {
        var2.hg(var1);
        Object var3 = this.a(var2);
        var2.pe();
        return var3;
    }

    public Object a(Vm_q var1) {
        return var1.hd("unknown");
    }
}
