package all;

import org.w3c.dom.css.CSSStyleDeclaration;
import taikodom.render.RenderView;
import taikodom.render.helpers.RenderTask;
import taikodom.render.scene.SoundObject;

import javax.swing.*;
import javax.swing.event.MouseInputAdapter;
import java.awt.*;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Базовые элементы интерфейса
 * aiW
 */
public class BaseUItegXML //extends xe
{
    private static final String typeCharacter = "utf-8";
    /**
     * Базовые элементы интерфейса
     */
    public static BaseUItegXML thisClass;
    private static LogPrinter logger = LogPrinter.setClass(xe.class);
    /**
     * Панель окна
     */
    private JRootPane rootPane = new JRootPane();
    private LoaderFileXML loaderFileXml;
    /**
     * Ядро CSS стилей
     */
    private avI cssNodeCore;
    /**
     * класс озвучки интерфейса class Ix
     */
    private Ma ghn;
    private ILoaderImageInterface loaderImage;
    private age_q fontFamily;
    private LoaderCss loaderCss;
    /**
     * Список Базовые элементы интерфейса
     * Соотношение XML тегов и графических Component
     */
    private MapKeyValue listTagAsClass;
    private List ghr = new ArrayList();
    private JDesktopPane jDesktopPane;
    private int ght = 0;
    private MouseInputAdapter ghu = new MouseInputAdapter() {
    };
    private aDX ghv;

    /**
     * Базовые элементы интерфейса
     */
    public BaseUItegXML() {
        if (thisClass == null) {
            thisClass = this;
        }

        this.loaderFileXml = new LoaderFileXML();
        this.listTagAsClass = new MapKeyValue(this);
        this.listTagAsClass.add("GridBagLayout", (IBaseLayout) (new GridBagLayoutCustom()));
        this.listTagAsClass.add("BorderLayout", (IBaseLayout) (new BorderLayoutCustom()));
        this.listTagAsClass.add("GridLayout", (IBaseLayout) (new GridLayoutCustom()));
        this.listTagAsClass.add("FlowLayout", (IBaseLayout) (new FlowLayoutCustom()));
        this.listTagAsClass.add("BoxLayout", (IBaseLayout) (new BoxLayoutCustom()));
        this.listTagAsClass.add("CardLayout", (IBaseLayout) (new CardLayoutCustom()));
        this.listTagAsClass.add("OverlayLayout", (IBaseLayout) (new OverlayLayoutCustom()));
        this.listTagAsClass.add("fbox", (BaseItem) (new FBoxCustom()));
        this.listTagAsClass.add("gbox", (BaseItem) (new GBoxCustom()));
        this.listTagAsClass.add("hbox", (BaseItem) (new HBoxCustom()));
        this.listTagAsClass.add("vbox", (BaseItem) (new VBoxCustom()));
        this.listTagAsClass.add("panel", (BaseItem) (new PanelCustom()));
        this.listTagAsClass.add("desktoppane", (BaseItem) (new DesktoppaneCustom()));
        this.listTagAsClass.add("scrollable", (BaseItem) (new ScrollableCustom()));
        this.listTagAsClass.add("table", (BaseItem) (new TableCustom()));
        this.listTagAsClass.add("window", (BaseItem) (new WindowCustom()));
        this.listTagAsClass.add("tabpane", (BaseItem) (new TabpaneCustom()));
        this.listTagAsClass.add("list", (BaseItem) (new ListCustom()));
        this.listTagAsClass.add("stack", (BaseItem) (new StackCustom()));
        this.listTagAsClass.add("repeater", (BaseItem) (new RepeaterCustom()));
        this.listTagAsClass.add("tree", (BaseItem) (new TreeCustom()));
        this.listTagAsClass.add("split", (BaseItem) (new SplitCustom()));
        this.listTagAsClass.add("splitpane", (BaseItem) (new SplitCustom()));
        this.listTagAsClass.add("label", (BaseItem) (new LabelCustom()));
        this.listTagAsClass.add("button", (BaseItem) (new ButtonCustom()));
        this.listTagAsClass.add("toggle-button", (BaseItem) (new ToggleButtonCustom()));
        this.listTagAsClass.add("textfield", (BaseItem) (new TextfieldCustom()));
        this.listTagAsClass.add("editbox", (BaseItem) (new TextfieldCustom()));
        this.listTagAsClass.add("checkbox", (BaseItem) (new CheckboxCustom()));
        this.listTagAsClass.add("combobox", (BaseItem) (new ComboBoxCustom()));
        this.listTagAsClass.add("radiobutton", (BaseItem) (new RadiobuttonCustom()));
        this.listTagAsClass.add("textarea", (BaseItem) (new TextAreaCustom()));
        this.listTagAsClass.add("editor", (BaseItem) (new EditorCustom()));
        this.listTagAsClass.add("html", (BaseItem) (new HtmlCustom()));
        this.listTagAsClass.add("toolbar", (BaseItem) (new ToolBarCustom()));
        this.listTagAsClass.add("formattedfield", (BaseItem) (new FormattedFieldCustom()));
        this.listTagAsClass.add("passwordfield", (BaseItem) (new PasswordfieldCustom()));
        this.listTagAsClass.add("spinner", (BaseItem) (new SpinnerCustom()));
        this.listTagAsClass.add("picture", (BaseItem) (new PictureCustom()));
        this.listTagAsClass.add("item-iconImage", (BaseItem) (new ItemIconCustom()));
        this.listTagAsClass.add("labeled-iconImage", (BaseItem) (new LabeledIconCustom()));
        this.listTagAsClass.add("iconImage-viewer", (BaseItem) (new IconViewerCustom()));
        this.listTagAsClass.add("progress", (BaseItem) (new ProgressCustom()));
        this.listTagAsClass.add("slider", (BaseItem) (new SliderCustom()));
        this.listTagAsClass.add("scrollbar", (BaseItem) (new ScrollBarCustom()));
        this.listTagAsClass.add("component3d", (BaseItem) (new Component3dCustom()));
        this.listTagAsClass.add("layeredpane", (BaseItem) (new LayeredPaneCustom()));
        this.listTagAsClass.add("taskpane", (BaseItem) (new TaskPaneCustom()));
        this.listTagAsClass.add("taskpane-container", (BaseItem) (new TaskPaneContainerCustom()));
        this.loaderImage = new LoaderImagePng();
        this.cssNodeCore = new CssNode();

/*
      try {
         if (!(UIManager.getLookAndFeel() instanceof BasicLookAndFeelSpaceEngine)) {
            UIManager.setLookAndFeel(new BasicLookAndFeelSpaceEngine());
         }
      } catch (UnsupportedLookAndFeelException var2) {
         var2.printStackTrace();
      }
*/
    }

    public LoaderCss getLoaderCss() {
        if (this.loaderCss == null) {
            this.loaderCss = new LoaderCss();
        }
        return this.loaderCss;
    }

    public void setLoaderCss(LoaderCss var1) {
        this.loaderCss = var1;
    }

    public void a(Object var1, Component var2, File var3) {
        avI var4 = ComponentManager.getCssHolder(var2).getCssNode();
        if (var4 != null) {
            avI var5 = this.a(var1, var3);
            ((Ll) var4).b(var5);
        } else {
            Ll var7 = new Ll();
            avI var6 = this.a(var1, var3);
            var7.b(this.getCssNodeCore());
            var7.b(var6);
        }

    }

    public void a(Object var1, Component var2, String var3) {
        avI var4 = ComponentManager.getCssHolder(var2).getCssNode();
        if (var4 != null) {
            avI var5 = this.loadCSS(var1, var3);
            ((Ll) var4).b(var5);
        } else {
            Ll var7 = new Ll();
            avI var6 = this.loadCSS(var1, var3);
            var7.b(this.getCssNodeCore());
            var7.b(var6);
        }

    }

    public Component e(File var1) {
        try {
            return this.CreateJComponent(var1.toURI().toURL());
        } catch (MalformedURLException var3) {
            throw new RuntimeException(var3);
        }
    }

    /**
     * Создание Component,
     *
     * @param pathFile адреса пути .xml файла
     * @return
     */
    public JComponent CreateJComponent(URL pathFile) {
        return this.CreateJComponent(this.listTagAsClass, pathFile);
    }

    /**
     * Создание графического представления из .xml файла
     *
     * @param listTagAsClass Соотношение XML тегов и графических Component
     * @param pathFile       Пример file:/C:/.../taikodom/addon/login/login.xml
     * @return
     */
    public JComponent CreateJComponent(MapKeyValue listTagAsClass, URL pathFile) {
        XmlNode xmlNode = null;//Класс обёртка .xml файла графики аддона

        try {
            xmlNode = this.loaderFileXml.loadFileXml(pathFile.openStream(), typeCharacter);
        } catch (IOException error) {
            logger.error("Error opening XML file '" + pathFile + "'.", error);
            return null;
        }
        if (xmlNode == null) {
            return null;
        } else {
            //получили класс тега
            BaseItem baseUi = listTagAsClass.getClassBaseUi(xmlNode.getTegName().toLowerCase());//getTegName = window  toLowerCase = window
            Component var4;

            // System.out.println("DEBUG6 -" + ((aCo)var5).findJLabel("versionLabel") );

            if (baseUi == null) {
                if (!"import".equals(xmlNode.getTegName())) {
                    logger.error("There is BaseUItegXML component named '" + xmlNode.getTegName() + "'");
                    return null;
                }

                try {
                    var4 = this.CreateJComponent(new URL(pathFile, xmlNode.getAttribute("src")));
                } catch (MalformedURLException var7) {
                    throw new RuntimeException(var7);
                }
            } else {
                //Создание графического Component в баззовом класе тега, присоединение css и системы событий
                var4 = baseUi.CreateUI(new MapKeyValue(listTagAsClass, pathFile), this.rootPane, xmlNode);
            }

            JDesktopPane var6 = (JDesktopPane) this.rootPane.getContentPane();
            if (var4 instanceof JInternalFrame) {
                var6.add(var4);
                var6.moveToFront(var4);//перейти на передний план
            } else {//Добавления графикического компонента в рут панель
                var6.add(var4);
            }

            return (JComponent) var4;
        }
    }

    /**
     * Создание Component, Формирование адреса пути .xml файла в зависимости от класса
     *
     * @param objClass LoginAddon.class
     * @param fileName login.xml
     * @return
     */
    public Component CreateJComponent(Object objClass, String fileName) {
        URL pathFile = null;//Путь к файлу класса
        if (objClass instanceof Class) {
            pathFile = ((Class) objClass).getResource(((Class) objClass).getSimpleName() + ".class");
        } else {
            pathFile = (objClass != null ? objClass.getClass().getResource(".") : null);
        }

        try {
            return this.CreateJComponent(new URL(pathFile, fileName));
        } catch (MalformedURLException error) {
            throw new RuntimeException(error);
        }
    }

    public Component CreateJComponentAndAddInRootPane(Object var1, String var2) {
        XmlNode var3 = null;

        try {
            var3 = this.loaderFileXml.loadFileXml((InputStream) (new ByteArrayInputStream(var2.getBytes(typeCharacter))), (String) typeCharacter);
        } catch (IOException var7) {
            logger.error("Error parsing XML", var7);
            return null;
        }

        if (var3 == null) {
            return null;
        } else {
            BaseItem var4 = this.listTagAsClass.getClassBaseUi(var3.getTegName().toLowerCase());
            if (var4 == null) {
                logger.error("There is test_GLCanvas component named '" + var3.getTegName() + "'");
                return null;
            } else {
                Class var5 = var1 instanceof Class ? (Class) var1 : (var1 != null ? var1.getClass() : null);
                Component var6 = var4.CreateUI(new MapKeyValue(this.listTagAsClass, var5.getResource(".")), this.rootPane, var3);
                if (var6 instanceof JInternalFrame) {
                    this.rootPane.getLayeredPane().add(var6);
                } else {
                    this.rootPane.getContentPane().add(var6);//Добавить в панель содержимого
                }

                return var6;
            }
        }
    }

    public avI getCssNodeCore() {
        return this.cssNodeCore;
    }

    /**
     * Установка Ядро CSS стилей
     *
     * @param cssNode Ядро CSS стилей
     */
    public void setCssNodeCore(avI cssNode) {
        this.cssNodeCore = cssNode;
        if (this.rootPane != null) {
            this.rootPane.updateUI();
            aeK var2 = ComponentManager.getCssHolder(this.rootPane);
            if (var2 != null) {
                var2.setCssNode(this.cssNodeCore);
            }
        }
    }

    public ILoaderImageInterface getLoaderImage() {
        return this.loaderImage;
    }

    public void setLoaderImage(ILoaderImageInterface var1) {
        this.loaderImage = var1;
    }

    public JRootPane getRootPane() {
        return this.rootPane;
    }

    /**
     * Задать панель окна
     *
     * @param var1 javax.swing.JRootPane
     */
    public void setRootPane(JRootPane var1) {
        this.rootPane = var1;
        var1.updateUI();
        aeK var2 = ComponentManager.getCssHolder(this.rootPane);
        if (var2 != null) {
            var2.setCssNode(this.cssNodeCore);
        }
    }

    public JDesktopPane setupJDesktopPaneInRootPane() {
        if (this.jDesktopPane == null || !(this.rootPane.getGlassPane() instanceof JDesktopPane)) {
            this.jDesktopPane = new JDesktopPane();
            this.jDesktopPane.setLayout((LayoutManager) null);
            this.jDesktopPane.setBounds(this.rootPane.getBounds());
            this.rootPane.setGlassPane(this.jDesktopPane);
            this.jDesktopPane.setName("!!!!setupJDesktopPaneInRootPane!!!!");
        }

        return this.jDesktopPane;
    }

    public synchronized void a(WindowCustomWrapper var1, boolean var2) {
        JLayeredPane var3 = this.setupJDesktopPaneInRootPane();
        if (var2) {
            this.rootPane.getLayeredPane().remove(var1);
            var3.add(var1, new Integer(this.ght++), -1);
            var1.setFocusable(true);
            var1.setResizable(false);
            var3.setLayer(var1, JLayeredPane.MODAL_LAYER.intValue(), 0);
            var1.requestFocus();
        } else {
            if (var1.isVisible()) {
                this.rootPane.getLayeredPane().add(var1);
            }
            var3.remove(var1);
            --this.ght;
        }
        ComponentManager.getCssHolder(var3).setCssNode(ComponentManager.getCssHolder(var1).getCssNode());
        this.bE(var2);
    }

    public synchronized void bE(boolean var1) {
        final JLayeredPane var2 = this.setupJDesktopPaneInRootPane();
        final aeK var3 = ComponentManager.getCssHolder(var2);
        if (var1) {
            var3.setAttribute("name", "modal");
            var3.setAlpha(0.0F);
            var2.repaint();
            if (this.ghv != null) {
                this.ghv.kill();
            }

            if (this.ght == 1) {
                this.ghv = new aDX(var2, "[0..255] dur 100", kk_q.asS, new GJ() {
                    public void a(JComponent var1) {
                        var3.setAlpha(1.0F);
                        BaseUItegXML.this.ghv = null;
                    }
                });
                var2.addMouseMotionListener(this.ghu);
            }

            var2.setVisible(true);
            var2.repaint(50L);
        } else if (this.ght <= 0) {
            for (int var4 = 0; var4 < var2.getComponentCount(); ++var4) {
                var2.remove(var4);
            }

            var3.setAttribute("name", "");
            this.ght = 0;
            if (this.ghv != null) {
                this.ghv.kill();
            }

            this.ghv = new aDX(var2, "[255..0] dur 500", kk_q.asS, new GJ() {
                public void a(JComponent var1) {
                    var2.setVisible(false);
                    var3.setAttribute("class", "");
                    var2.removeMouseMotionListener(BaseUItegXML.this.ghu);
                    BaseUItegXML.this.ghv = null;
                }
            });
        }

    }

    public boolean anx() {
        return this.ght > 0;
    }

    public avI a(Object var1, File var2) {
        return this.getLoaderCss().a(var1, var2);
    }

    /**
     * Загрузка CSS стилей
     *
     * @param var1 null или  file:/C://addon/debugtools/taikodomversion/taikodomversion.xml
     * @param var2 res://data/styles/core.css или taikodomversion.css
     * @return
     */
    public avI loadCSS(Object var1, String var2) {
        return this.getLoaderCss().loadFileCSS(var1, var2);
    }

    public CSSStyleDeclaration aY(String var1) {
        return this.getLoaderCss().aY(var1);
    }

    //Имя шрифта, размер шрифта и стиль шрифта
    public Font getFont(String var1, int var2, int var3) {
        return this.fontFamily == null ? new Font(var1, var2, var3) : this.fontFamily.b(var1, var2, var3, false);
    }

    public age_q cnj() {
        return this.fontFamily;
    }

    public void a(age_q var1) {
        this.fontFamily = var1;
    }

    public avI b(URL var1) {
        try {
            return this.getLoaderCss().loadCSS(var1);
        } catch (IOException var3) {
            throw new RuntimeException("Error loading css: " + var1, var3);
        }
    }

    public MapKeyValue cnk() {
        return this.listTagAsClass;
    }

    /**
     * Список Базовые элементы интерфейса
     */
    public MapKeyValue cnl() {
        return this.listTagAsClass;
    }

    public void a(RenderTask var1) {
        List var2 = this.ghr;
        synchronized (this.ghr) {
            if (!this.ghr.contains(var1)) {
                this.ghr.add(var1);
            }

        }
    }

    public void a(RenderView var1) {
        List var3 = this.ghr;
        ArrayList var2;
        synchronized (this.ghr) {
            var2 = new ArrayList(this.ghr);
            this.ghr.clear();
        }

        Iterator var4 = var2.iterator();

        while (var4.hasNext()) {
            RenderTask var6 = (RenderTask) var4.next();
            var6.run(var1);
        }

    }

    public SoundObject cx(String var1) {
        Ma var2 = this.any();
        return var2 != null && !"none".equals(var1) ? var2.cx(var1) : null;
    }

    public SoundObject e(String var1, boolean var2) {
        Ma var3 = this.any();
        return var3 != null && !"none".equals(var1) ? var3.e(var1, var2) : null;
    }

    public Ma any() {
        return this.ghn;
    }

    /**
     * Принять ссылку на класс озвучки интерфейса
     *
     * @param var1 class Ix
     */
    public void a(Ma var1) {
        this.ghn = var1;
    }
}
