package all;

import javax.swing.*;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.basic.BasicTextUI.BasicCaret;
import javax.swing.text.Caret;
import javax.swing.text.JTextComponent;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class TextAreaCustomWrapper extends JTextArea implements ITextComponentCustomWrapper {
    private static final long serialVersionUID = 1L;
    private boolean ciu;
    private KeyAdapter dO;

    public void bO(boolean var1) {
        if (var1) {
            this.setCaret(new BasicCaret());
        } else {
            this.setCaret(new a((a) null));
        }

        this.ciu = var1;
    }

    public boolean awn() {
        return this.ciu;
    }

    public String getElementName() {
        return "textarea";
    }

    public void h(final int var1) {
        if (this.dO != null) {
            this.removeKeyListener(this.dO);
        } else {
            this.dO = new KeyAdapter() {
                public void keyTyped(KeyEvent var1x) {
                    if ((TextAreaCustomWrapper.this.getText().length() ^ var1) == 0) {
                        var1x.consume();
                    }

                }
            };
        }

        this.addKeyListener(this.dO);
    }

    private final class a implements Caret {
        private a() {
        }

        // $FF: synthetic method
        a(a var2) {
            this();
        }

        public void addChangeListener(ChangeListener var1) {
        }

        public void deinstall(JTextComponent var1) {
        }

        public int getBlinkRate() {
            return 0;
        }

        public void setBlinkRate(int var1) {
        }

        public int getDot() {
            return 0;
        }

        public void setDot(int var1) {
        }

        public Point getMagicCaretPosition() {
            return null;
        }

        public void setMagicCaretPosition(Point var1) {
        }

        public int getMark() {
            return 0;
        }

        public void install(JTextComponent var1) {
        }

        public boolean isSelectionVisible() {
            return false;
        }

        public void setSelectionVisible(boolean var1) {
        }

        public boolean isVisible() {
            return false;
        }

        public void setVisible(boolean var1) {
        }

        public void moveDot(int var1) {
        }

        public void paint(Graphics var1) {
        }

        public void removeChangeListener(ChangeListener var1) {
        }
    }
}
