package all;

import java.io.PrintStream;
import java.io.PrintWriter;

public class XU extends UnsupportedOperationException implements aIa {

    private gi eJS = new gi(this);
    private Throwable cause;

    public XU() {
        super("Code is not implemented");
    }

    public XU(String var1) {
        super(var1 == null ? "Code is not implemented" : var1);
    }

    public XU(Throwable var1) {
        super("Code is not implemented");
        this.cause = var1;
    }

    public XU(String var1, Throwable var2) {
        super(var1 == null ? "Code is not implemented" : var1);
        this.cause = var2;
    }

    public XU(Class var1) {
        super(var1 == null ? "Code is not implemented" : "Code is not implemented in " + var1);
    }

    public Throwable getCause() {
        return this.cause;
    }

    public String getMessage() {
        if (super.getMessage() != null) {
            return super.getMessage();
        } else {
            return this.cause != null ? this.cause.toString() : null;
        }
    }

    public String bg(int var1) {
        return var1 == 0 ? super.getMessage() : this.eJS.bg(var1);
    }

    public String[] getMessages() {
        return this.eJS.getMessages();
    }

    public Throwable bh(int var1) {
        return this.eJS.bh(var1);
    }

    public int vn() {
        return this.eJS.vn();
    }

    public Throwable[] vo() {
        return this.eJS.vo();
    }

    public int N(Class var1) {
        return this.eJS.a(var1, 0);
    }

    public int a(Class var1, int var2) {
        return this.eJS.a(var1, var2);
    }

    public void printStackTrace() {
        this.eJS.printStackTrace();
    }

    public void printStackTrace(PrintStream var1) {
        this.eJS.printStackTrace(var1);
    }

    public void printStackTrace(PrintWriter var1) {
        this.eJS.printStackTrace(var1);
    }

    public final void c(PrintWriter var1) {
        super.printStackTrace(var1);
    }
}
