package all;

import javax.vecmath.*;
import java.nio.DoubleBuffer;

public class aJD_q extends Vector4d //implements afA_q
{

    public aJD_q() {
    }

    public aJD_q(double[] var1) {
        super(var1);
    }

    public aJD_q(Vector4f var1) {
        super(var1);
    }

    public aJD_q(Vector4d var1) {
        super(var1);
    }

    public aJD_q(Tuple4f var1) {
        super(var1);
    }

    public aJD_q(Tuple4d var1) {
        super(var1);
    }

    public aJD_q(Tuple3d var1) {
        super(var1);
    }

    public aJD_q(double var1, double var3, double var5, double var7) {
        super(var1, var3, var5, var7);
    }

    public aJD_q Y(double var1) {
        double var3 = this.x * var1;
        double var5 = this.y * var1;
        double var7 = this.z * var1;
        double var9 = this.w * var1;
        return new aJD_q(var3, var5, var7, var9);
    }

    public aJD_q mR(float var1) {
        this.x *= (double) var1;
        this.y *= (double) var1;
        this.z *= (double) var1;
        this.w *= (double) var1;
        return this;
    }

    public DoubleBuffer a(DoubleBuffer var1) {
        var1.clear();
        var1.put(this.x);
        var1.put(this.y);
        var1.put(this.z);
        var1.put(this.w);
        var1.flip();
        return var1;
    }
/*TODO на удаление
   public void readExternal(Vm_q var1) {
      this.x = var1.ha("x");
      this.y = var1.ha("y");
      this.z = var1.ha("z");
      this.w = var1.ha("w");
   }

   public void writeExternal(att var1) {
      var1.all("x", this.x);
      var1.all("y", this.y);
      var1.all("z", this.z);
      var1.all("w", this.w);
   }*/

}
