package all;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicSpinnerUI;
import java.awt.*;

/**
 * SpinnerUI
 */
public class SpinnerUI extends BasicSpinnerUI implements aIh {
    private aeK BM;

    public SpinnerUI(JSpinner var1) {
        var1.setBorder(new BorderWrapper());
        this.BM = new ComponentManager("spinner", var1);
    }

    public static ComponentUI createUI(JComponent var0) {
        return new SpinnerUI((JSpinner) var0);
    }

    /**
     * TODO похлже это public pv wy()
     *
     * @return Графический элемент
     */
    @Override
    public aeK wz() {
        return this.BM;
    }

    public Dimension getPreferredSize(JComponent var1) {
        return this.wz().c(var1, super.getPreferredSize(var1));
    }

    public Dimension getMinimumSize(JComponent var1) {
        return this.wz().b(var1, super.getMinimumSize(var1));
    }

    public Dimension getMaximumSize(JComponent var1) {
        return this.wz().a(var1, super.getMaximumSize(var1));
    }

    protected Component createNextButton() {
        JButton var1 = new JButton();
        var1.setFocusable(false);
        ComponentManager.getCssHolder(var1).setAttribute("name", "next");
        this.spinner.add(var1);
        this.installNextButtonListeners(var1);
        return var1;
    }

    protected Component createPreviousButton() {
        JButton var1 = new JButton();
        var1.setFocusable(false);
        ComponentManager.getCssHolder(var1).setAttribute("name", "previous");
        this.spinner.add(var1);
        this.installPreviousButtonListeners(var1);
        return var1;
    }

    public void update(Graphics var1, JComponent var2) {
        this.paint(var1, var2);
    }
}
