package all;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicTextAreaUI;
import javax.swing.text.JTextComponent;
import java.awt.*;

/**
 * TextAreaUI
 */
public class TextAreaUI extends BasicTextAreaUI implements aIh {
    private final JTextArea bHW;
    private final Ek bHX;
    private ComponentManager Rp;

    public TextAreaUI(JTextArea var1) {
        this.bHW = var1;
        var1.setWrapStyleWord(true);
        this.Rp = new aix("textarea", var1);
        var1.setSelectionColor(new Color(12, 92, 111, 255));
        this.bHX = new Ek(this.Rp);
    }

    public static ComponentUI createUI(JComponent var0) {
        return new TextAreaUI((JTextArea) var0);
    }

    protected void installListeners() {
        super.installListeners();
        this.bHW.addKeyListener(this.bHX);
        this.bHW.addMouseListener(this.bHX);
    }

    protected void uninstallListeners() {
        super.uninstallListeners();
        this.bHW.removeKeyListener(this.bHX);
        this.bHW.removeKeyListener(this.bHX);
    }

    public void update(Graphics var1, JComponent var2) {
        RM.a((aIh) this, (Graphics) var1, (JTextComponent) ((JTextComponent) var2));
        super.paint(var1, var2);
    }

    protected void paintBackground(Graphics var1) {
    }

    public Dimension getMinimumSize(JComponent var1) {
        RM.a((aIh) this, (JTextComponent) ((JTextComponent) var1));
        return this.wy().b(var1, super.getMinimumSize(var1));
    }

    public Dimension getMaximumSize(JComponent var1) {
        RM.a((aIh) this, (JTextComponent) ((JTextComponent) var1));
        return this.wy().a(var1, super.getMaximumSize(var1));
    }

    public Dimension getPreferredSize(JComponent var1) {
        RM.a((aIh) this, (JTextComponent) ((JTextComponent) var1));
        return this.Rp.c(var1, super.getPreferredSize(var1));
    }

    public ComponentManager wy() {
        return this.Rp;
    }

    /**
     * TODO похлже это public pv wy()
     *
     * @return Графический элемент
     */
    @Override
    public aeK wz() {
        return this.Rp;
    }
}
