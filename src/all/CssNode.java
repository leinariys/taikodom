package all;

import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.css.CSSStyleSheet;
import org.w3c.dom.css.CSSValue;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Обёрта файла .CSS стиля
 */
public class CssNode extends avI {
    /**
     * Список CSS ролей = Css файлов
     */
    private List listCssRule = new ArrayList();
    private List ihU = new ArrayList();
    /**
     * Список заголовков селекторов name и не только
     */
    private List listTitleSelector = new ArrayList();
    private EQ ihV;

    public CssNode() {
        this.ihV = new EQ(this);
    }

    public CssNode(EQ var1) {
        this.ihV = var1;
        var1.setCurrentCssNode(this);
    }

    /**
     * Чтение файла
     *
     * @param var1
     * @throws UnsupportedEncodingException
     */
    public void loadFileCSS(InputStream var1) throws UnsupportedEncodingException {
        this.listCssRule.clear();
        this.ihV.parse(new InputStreamReader(var1, "utf-8"));
    }

    public void parse(Reader var1) {
        this.ihV.parse(var1);
    }

    public void a(RC var1) {
        this.ihV.a(var1);
    }

    public CSSValue a(RC var1, String var2, boolean var3) {
        tX var4 = new tX(this.listTitleSelector);
        CSSStyleDeclaration var5 = var4.a(var1, (String) var2, var3);
        if (var5 == null) {
            return null;
        } else {
            CSSValue var6 = var5.getPropertyCSSValue(var2);
            if (var6 == null) {
                return var3 && var1.Vm() != null ? this.a((RC) var1.Vm(), var2, var3) : null;
            } else {
                return var6;
            }
        }
    }

    /**
     * Добавить в список - селектор и его параметры
     *
     * @param var1
     */
    public void add(CSSStyleSheet var1) {
        this.listCssRule.add(var1);
    }

    public void a(asi var1) {
        this.ihU.add(var1);
    }

    public boolean b(asi var1) {
        return this.ihU.contains(var1);
    }

    /**
     * Добавить в список заголовки селекторов
     *
     * @param var1
     */
    public void add(lD var1) {
        this.listTitleSelector.add(var1);
    }

    public boolean a(RC var1, aKV_q var2, boolean var3) {
        tX var4 = new tX(this.listTitleSelector);
        return var4.a(var1, (aKV_q) var2, var3);
    }
}
