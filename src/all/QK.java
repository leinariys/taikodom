package all;

import java.util.HashMap;
import java.util.Map;

/**
 * Джостик константы клавиш
 * class CommandTranslatorAddon
 */
public class QK {
    public static Map Ow = new HashMap();
    public static Map Ox = new HashMap();
    public static int dWG = 1;
    public static int dWH = 2;
    public static int dWI = 4;
    public static int dWJ = 8;
    public static int dWK = 3;
    public static int dWL = 9;
    public static int dWM = 6;
    public static int dWN = 12;
    public static String OC = "UNDEFINED";

    static {
        Ow.put(Integer.valueOf(dWG), "JOY_HAT_UP");
        Ow.put(Integer.valueOf(dWH), "JOY_HAT_RIGHT");
        Ow.put(Integer.valueOf(dWI), "JOY_HAT_DOWN");
        Ow.put(Integer.valueOf(dWJ), "JOY_HAT_LEFT");
        Ow.put(Integer.valueOf(dWK), "JOY_HAT_UP_RIGHT");
        Ow.put(Integer.valueOf(dWL), "JOY_HAT_UP_LEFT");
        Ow.put(Integer.valueOf(dWM), "JOY_HAT_DOWN_RIGHT");
        Ow.put(Integer.valueOf(dWN), "JOY_HAT_DOWN_LEFT");
        Ox.put("JOY_HAT_UP", Integer.valueOf(dWG));
        Ox.put("JOY_HAT_RIGHT", Integer.valueOf(dWH));
        Ox.put("JOY_HAT_DOWN", Integer.valueOf(dWI));
        Ox.put("JOY_HAT_LEFT", Integer.valueOf(dWJ));
        Ox.put("JOY_HAT_UP_RIGHT", Integer.valueOf(dWK));
        Ox.put("JOY_HAT_UP_LEFT", Integer.valueOf(dWL));
        Ox.put("JOY_HAT_DOWN_RIGHT", Integer.valueOf(dWM));
        Ox.put("JOY_HAT_DOWN_LEFT", Integer.valueOf(dWN));
    }

    public static int ao(String var0) {
        Integer var1 = (Integer) Ox.get(var0);
        return var1 != null ? var1.intValue() : 0;
    }

    public static String bf(int var0) {
        String var1 = (String) Ow.get(var0);
        return var1 != null ? var1 : OC;
    }

    public static int n(float var0, float var1) {
        if (var0 == 0.0F) {
            if (var1 > 0.0F) {
                return dWI;
            }

            if (var1 < 0.0F) {
                return dWG;
            }
        }

        if (var1 == 0.0F) {
            if (var0 > 0.0F) {
                return dWH;
            }

            if (var0 < 0.0F) {
                return dWJ;
            }
        }

        if (var1 > 0.0F) {
            if (var0 > 0.0F) {
                return dWM;
            }

            if (var0 < 0.0F) {
                return dWN;
            }
        }

        if (var1 < 0.0F) {
            if (var0 > 0.0F) {
                return dWK;
            }

            if (var0 < 0.0F) {
                return dWL;
            }
        }

        return 0;
    }
}
