package all;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Gq_q implements an {
    static LogPrinter logger = LogPrinter.K(Gq_q.class);
    private final Object root;
    private final ain ecK;
    private Map ecJ = new HashMap();

    public Gq_q(ain var1, Object var2) {
        this.ecK = var1;
        this.root = var2;
        this.bsD();
    }

    private void bsD() {
        if (this.ecK != null) {
            ain[] var1 = this.ecK.getChildren();
            if (var1 == null) {
                logger.error("No files found in 'components' directory. The directory itself might be missing");
            } else {
                ain[] var5 = var1;
                int var4 = var1.length;

                for (int var3 = 0; var3 < var4; ++var3) {
                    ain var2 = var5[var3];
                    if (var2.isDir()) {
                        ain[] var6 = var2.a(new FY() {
                            public boolean a(ain var1, String var2) {
                                return var2.endsWith(".xml");
                            }
                        });
                        if (var6.length == 0) {
                            logger.warn("test_GLCanvas components found in " + var2.getName());
                        }

                        ain[] var10 = var6;
                        int var9 = var6.length;

                        for (int var8 = 0; var8 < var9; ++var8) {
                            ain var7 = var10[var8];

                            try {
                                String var11 = this.a(var7);
                                this.start(var11);
                            } catch (Exception var12) {
                                logger.warn("Error loading components", var12);
                            }
                        }
                    } else {
                        logger.info("Inoring file components/" + var2.getName());
                    }
                }

            }
        }
    }

    public String a(ain var1) throws UX {/*
      azH var2 = new azH(var1, this.root, this);
      if (this.ecJ.containsKey(var2.cHn())) {
         throw new UX("Component '" + var2.cHn() + "' already added");
      } else {
         this.ecJ.put(var2.cHn(), var2);
         return var2.cHn();
      }*/
        return "";
    }

    public void stop(String var1) {
        //((azH)this.ecJ.get(var1)).stop();
    }

    public void start(String var1) {
        //((azH)this.ecJ.get(var1)).start();
    }

    public void o(String var1) {
        //azH var2 = (azH)this.ecJ.get(var1);
        //var2.stop();
        //var2.start();
    }

    public void dispose() {
        Iterator var2 = this.ecJ.values().iterator();

        while (var2.hasNext()) {
            //azH var1 = (azH)var2.next();
            //var1.stop();
        }

    }

    public void q(String var1) {
        this.stop(var1);
        this.ecJ.remove(var1);
    }

    public String r(String var1) throws UX {
        return this.a(this.ecK.concat(var1));
    }
}
