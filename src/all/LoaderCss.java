package all;

import org.w3c.dom.css.CSSStyleDeclaration;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.net.URL;


public class LoaderCss implements aGe {
    public avI loadCSS(URL pathFile) throws IOException {
        CssNode var2 = new CssNode();
        new EQ(var2);
        if (pathFile == null) {
            throw new NullPointerException("Null url at CssLoaderImpl.loadCSS()");
        } else {
            var2.loadFileCSS(pathFile.openStream());
            return var2;
        }
    }

    public avI a(Object var1, File var2) {
        try {
            avI var3 = this.loadCSS(var2.toURI().toURL());
            return var3;
        } catch (IOException var4) {
            var4.printStackTrace();
            return null;
        }
    }

    /**
     * Разбор адреса ресурса ядра CSS стилей
     *
     * @param var1     null или  file:/C://addon/debugtools/taikodomversion/taikodomversion.xml
     * @param pathFile res://data/styles/core.css или taikodomversion.css
     * @return
     */
    public avI loadFileCSS(Object var1, String pathFile) {
        try {
            avI var3;
            if (var1 instanceof File) {
                File var4 = new File(((File) var1).getParent(), pathFile);
                var3 = this.loadCSS(var4.toURI().toURL());
            } else if (pathFile.startsWith("res://")) {//я добавил
                URL var1674 = new FileControl(pathFile.replaceAll("^res://", "")).getFile().toURI().toURL();
                var3 = this.loadCSS(var1674);
            } else if (pathFile.contains("://")) {
                var3 = this.loadCSS(new URL(pathFile));

            } else if (var1 instanceof Class) {
                var3 = this.loadCSS(((Class) var1).getResource(pathFile));
            } else if (var1 instanceof URL) {
                var3 = this.loadCSS(new URL((URL) var1, pathFile));
            } else {
                var3 = this.loadCSS(var1.getClass().getResource(pathFile));
            }

            return var3;
        } catch (IOException var5) {
            var5.printStackTrace();
            return null;
        }
    }

    public CSSStyleDeclaration aY(String var1) {
        ParserCss var2 = ParserCss.getParserCss();

        CSSStyleDeclaration var3 = var2.getCSSStyleDeclaration(new InputSource(new StringReader("{" + var1 + "}")));
        if (var3 != null) {
            EQ.parseRule(var3);
        }

        return var3;
    }
}
