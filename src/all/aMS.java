package all;

/**
 * Наследует интервйс interface fo который наследуют аддоны
 */
public interface aMS {
    /**
     * Передаём в аддон логирование и локолизацию
     *
     * @param var1
     */
    void InitAddon(vW var1);

    /**
     * Остановить работу аддона
     */
    void stop();
}
