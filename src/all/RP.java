package all;

import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Блокировщик ресурса и его кэш
 */
public class RP {
    /**
     * Кэш для блокировщика, 1-ресурс заблокирован, 0-Ресурс разблокирован
     */
    private ThreadLocal hIa = new ThreadLocal() {
        @Override
        protected Integer initialValue() {
            return Integer.valueOf(0);
        }
    };

    /**
     * Блокировцик ресурса
     */
    private ReentrantReadWriteLock hIb = new ReentrantReadWriteLock(false);

    /**
     * Ссылка на блакировщик ресурса и его кэш
     */
    private RP.a hIc = new RP.a();

    /**
     * Блокировка ресурса на частную запись
     */
    public void cXZ() {
        this.hIb.writeLock().lock();
    }

    /**
     * Блокировка ресурса на нечастную запись
     *
     * @return true-Если блокировки небыло, false-если блокировка уже есть
     */
    public boolean cYa() {
        return this.hIb.writeLock().tryLock();
    }

    /**
     * Разблокировать ресурса на запись
     */
    public void cYb() {
        this.hIb.writeLock().unlock();
    }

    /**
     * Получить ссылку на блакировщик ресурса и его кэш
     *
     * @return блакировщик ресурса и его кэш
     */
    public Xn cYc() {
        return this.hIc;
    }

    /**
     * Передача потока
     *
     * @param var1 Поток
     */
    public void h(Thread var1) {
    }

    private class a implements Xn {
        /**
         * Устанавливает значение для кэша потока +1
         * Блокирует ресурс на нечастрое чтение
         */
        public void brK() {
            RP.this.hIa.set(((Integer) RP.this.hIa.get()).intValue() + 1);
            RP.this.hIb.readLock().lock();
        }


        /**
         * Устанавливает значение для кэша потока -1
         * Разблокируем ресурс на чтение
         *
         * @return true-разблокирован, false-не был заблокирован
         */
        public boolean brL() {
            int var1 = ((Integer) RP.this.hIa.get()).intValue();
            if (var1 > 0) {
                RP.this.hIa.set(var1 - 1);
                RP.this.hIb.readLock().unlock();
                return true;
            } else {
                return false;
            }
        }
    }
}
