package all;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicTreeUI;
import java.awt.*;

/**
 * TreeUI
 */
public class TreeUI extends BasicTreeUI implements aIh {
    private aeK BM;
    private aeK iyc;
    private aeK iyd;

    public TreeUI(final JTree var1) {
        var1.setBorder(new BorderWrapper());
        var1.setOpaque(false);
        this.BM = new ComponentManager("tree", var1);
        this.iyc = new ComponentManager("button-expand", new JButton() {
            public Container getParent() {
                return var1;
            }
        });
        this.iyd = new ComponentManager("button-collapse", new JButton() {
            public Container getParent() {
                return var1;
            }
        });
        this.setHashColor(new Color(12, 92, 111, 255));
    }

    public static ComponentUI createUI(JComponent var0) {
        return new TreeUI((JTree) var0);
    }

    /**
     * TODO похлже это public pv wy()
     *
     * @return Графический элемент
     */
    @Override
    public aeK wz() {
        return this.BM;
    }

    public Dimension getPreferredSize(JComponent var1) {
        return this.wz().c(var1, super.getPreferredSize(var1));
    }

    public Dimension getMinimumSize(JComponent var1) {
        return this.wz().b(var1, super.getMinimumSize(var1));
    }

    public Dimension getMaximumSize(JComponent var1) {
        return this.wz().a(var1, super.getMaximumSize(var1));
    }

    public void paint(Graphics var1, JComponent var2) {
        RM.a((aIh) this, (Graphics) var1, (JComponent) var2);
        super.paint(var1, var2);
    }

    public Icon getExpandedIcon() {
        return this.iyd.Vp().getIcon();
    }

    public Icon getCollapsedIcon() {
        return this.iyc.Vp().getIcon();
    }
}
