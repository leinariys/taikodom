package all;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class aUU extends WA implements Externalizable {
    public static short iXP = 0;
    public static short iXQ = 1;
    public static short iXR = 2;
    public static short iXS = 5;
    private short iXT;

    public aUU() {
    }

    /**
     * Установить значение
     *
     * @param var1
     */
    public aUU(short var1) {
        this.iXT = var1;
    }

    /**
     * Получить значение
     *
     * @return
     */
    public short dAD() {
        return this.iXT;
    }

    public void writeExternal(ObjectOutput var1) throws IOException {
        var1.writeShort(this.iXT);
    }

    public void readExternal(ObjectInput var1) throws IOException {
        this.iXT = var1.readShort();
    }
}
