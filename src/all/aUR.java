package all;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class aUR {
    public static String propertyKey = "component.views";
    private List iXK;

    public aUR(JComponent var1, List var2) {
        this.iXK = var2;
    }

    public akP nQ(String var1) {
        int var2 = this.iXK.size();

        for (int var3 = 0; var3 < var2; ++var3) {
            akP var4 = (akP) this.iXK.get(var3);
            if (var1.equals(var4.getName())) {
                return var4;
            }
        }

        return null;
    }

    public Component nR(String var1) {
        akP var2 = this.nQ(var1);
        return var2 != null ? var2.getComponent() : null;
    }

    public Component dAx() {
        return ((akP) this.iXK.get(0)).getComponent();
    }
}
