package all;

import javax.imageio.ImageIO;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

public class LoaderImagePng implements ILoaderImageInterface {
    private Map asx = new HashMap();

    public Image getImage(String pathFile) {
        if (pathFile == null) {
            return null;
        } else if ("none".equals(pathFile)) {
            return null;
        } else {
            if (pathFile.startsWith("res://")) {
                pathFile = pathFile.substring(5);
            }

            if (pathFile.startsWith("/imageset_") && pathFile.indexOf("material") > 0) {//Я добавил
                pathFile = "/data/gui/imageset" + pathFile.replaceAll("material/", "");
            }

            pathFile = pathFile + ".png";
            Object var2 = (Image) this.asx.get(pathFile);
            if (var2 == null) {
                try {
                    var2 = ImageIO.read(ILoaderImageInterface.class.getResourceAsStream(pathFile));
                } catch (Exception var3) {
                    System.err.println("Error reading image: " + pathFile);
                    return null;
                }

                this.asx.put(pathFile, var2);
            }

            return (Image) var2;
        }
    }

    public Cursor getNull(String var1, Point var2) {
        return null;
    }
}
