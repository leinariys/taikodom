package all;

import com.hoplon.geometry.Vec3f;

public final class IW implements te_w {
    private static final long serialVersionUID = 1L;
    public final float x;
    public final float y;
    public final float z;

    public IW(IW var1) {
        this.x = var1.x;
        this.y = var1.y;
        this.z = var1.z;
    }

    public IW(float var1, float var2, float var3) {
        this.x = var1;
        this.y = var2;
        this.z = var3;
    }

    public IW(Vec3f var1) {
        this.x = var1.x;
        this.y = var1.y;
        this.z = var1.z;
    }

    public static float a(IW var0, IW var1) {
        return (float) Math.pow((double) ((var0.x - var1.x) * (var0.x - var1.x) + (var0.y - var1.y) * (var0.y - var1.y) + (var0.z - var1.z) * (var0.z - var1.z)), 0.5D);
    }

    public static float b(IW var0, IW var1) {
        return (var0.x - var1.x) * (var0.x - var1.x) + (var0.y - var1.y) * (var0.y - var1.y) + (var0.z - var1.z) * (var0.z - var1.z);
    }

    public static IW a(IW var0, IW var1, IW var2) {
        IW var3 = var2.d(var0);
        IW var4 = var1.d(var0).aXi();
        float var5 = var0.d(var1).length();
        float var6 = var4.a(var3);
        if (var6 <= 0.0F) {
            return var0;
        } else if (var6 >= var5) {
            return var1;
        } else {
            IW var7 = var4.fH(var6);
            IW var8 = var0.c(var7);
            return var8;
        }
    }

    public IW fH(float var1) {
        return new IW(this.x * var1, this.y * var1, this.z * var1);
    }

    public IW b(IW var1) {
        return new IW(this.x * var1.x, this.y * var1.y, this.z * var1.z);
    }

    public IW c(IW var1) {
        return new IW(this.x + var1.x, this.y + var1.y, this.z + var1.z);
    }

    public IW d(IW var1) {
        return new IW(this.x - var1.x, this.y - var1.y, this.z - var1.z);
    }

    public float lengthSquared() {
        return this.x * this.x + this.y * this.y + this.z * this.z;
    }

    public IW e(IW var1) {
        return new IW(this.y * var1.z - this.z * var1.y, this.z * var1.x - this.x * var1.z, this.x * var1.y - this.y * var1.x);
    }

    public String toString() {
        return "(" + this.x + ", " + this.y + ", " + this.z + ")";
    }

    public IW aXi() {
        float var1 = this.x * this.x + this.y * this.y + this.z * this.z;
        if (var1 > 0.0F && var1 != 1.0F) {
            float var2 = (float) Math.sqrt((double) var1);
            return new IW(this.x / var2, this.y / var2, this.z / var2);
        } else {
            return this;
        }
    }

    public float length() {
        return (float) Math.sqrt((double) (this.x * this.x + this.y * this.y + this.z * this.z));
    }

    public float a(IW var1) {
        return this.x * var1.x + this.y * var1.y + this.z * var1.z;
    }

    public float a(te_w var1) {
        return this.x * var1.ada() + this.y * var1.adb() + this.z * var1.adc();
    }

    public float T(Vec3f var1) {
        return this.x * var1.x + this.y * var1.y + this.z * var1.z;
    }

    public float get(int var1) {
        switch (var1) {
            case 0:
                return this.x;
            case 1:
                return this.y;
            case 2:
                return this.z;
            default:
                throw new IllegalArgumentException();
        }
    }

    public float ada() {
        return this.x;
    }

    public float adb() {
        return this.y;
    }

    public float adc() {
        return this.z;
    }
}
