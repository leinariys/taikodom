package all;

import gnu.trove.TObjectIntHashMap;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;

public class ListCustom extends BaseItem {
    private static final LogPrinter logger = LogPrinter.setClass(ListCustom.class);
    static TObjectIntHashMap selectionModel = new TObjectIntHashMap();

    static {
        selectionModel.put("NO_SELECTION", -10);
        selectionModel.put("SINGLE_SELECTION", 0);
        selectionModel.put("SINGLE_INTERVAL_SELECTION", 1);
        selectionModel.put("MULTIPLE_INTERVAL_SELECTION", 2);
        selectionModel.put("HORIZONTAL_WRAP", 2);
        selectionModel.put("VERTICAL_WRAP", 1);
        selectionModel.put("VERTICAL", 0);
    }

    /**
     * Создание базового Component интерфейса
     * Вызывается из class BaseItem
     *
     * @param var1
     * @param var2
     * @param var3
     * @return
     */
    @Override
    protected JComponent CreatComponentCustom(MapKeyValue var1, Container var2, XmlNode var3) {
        return new ListCustomWrapper(new DefaultListModel());
    }

    public ListCustomWrapper i(MapKeyValue var1, Container var2, XmlNode var3) {
        ListCustomWrapper var4 = (ListCustomWrapper) super.CreateUI(var1, var2, var3);
        this.CreatChildren(var1, (JComponent) var4, var3);
        return var4;
    }

    protected void a(MapKeyValue var1, ListCustomWrapper var2, XmlNode var3) {
        super.applyAttribute(var1, (Component) var2, (XmlNode) var3);
        ArrayList var4 = null;
        Iterator var6 = var3.getChildrenTag().iterator();

        while (var6.hasNext()) {
            XmlNode var5 = (XmlNode) var6.next();
            if ("listItem".equalsIgnoreCase(var5.getTegName())) {
                if (var4 == null) {
                    var4 = new ArrayList();
                }

                var4.add(var5.getAttribute("text"));
            }
        }

        if (var4 != null) {
            var2.setListData(var4.toArray(new String[var4.size()]));
        }

        String var9 = var3.getAttribute("selectionMode");
        if (var9 != null) {
            var2.setSelectionMode(selectionModel.get(var9.replace('-', '_').toUpperCase()));
        }

        var9 = var3.getAttribute("prototypeCellValue");
        int var10;
        if (var9 != null) {
            try {
                var10 = Integer.parseInt(var9);
                var2.setPrototypeCellValue(var10);
            } catch (NumberFormatException var8) {
                logger.warn(var8);
            }
        }

        var9 = var3.getAttribute("layoutOrientation");
        if (var9 != null) {
            var2.setLayoutOrientation(selectionModel.get(var9.replace('-', '_').toUpperCase()));
            var2.setVisibleRowCount(-1);
        }

        var9 = var3.getAttribute("visibleRowCount");
        if (var9 != null) {
            try {
                var10 = Integer.parseInt(var9);
                var2.setVisibleRowCount(var10);
            } catch (NumberFormatException var7) {
                logger.warn(var7);
            }
        }

    }
}
