package all;

/**
 * MouseButtonEvent
 * class CommandTranslatorAddon
 */
public final class mu extends aOG {
    public static adl aCM = new adl("MouseButtonEvent");
    private int button;
    private boolean aCJ;
    private float x;
    private float y;
    private int aCK;
    private int aCL;
    private int modifiers;

    public mu(int var1, boolean var2, float var3, float var4, int var5, int var6, int var7) {
        this.button = var1;
        this.aCJ = var2;
        this.x = var3;
        this.y = var4;
        this.aCK = var5;
        this.aCL = var6;
        this.modifiers = var7;
    }

    public final int getButton() {
        return this.button;
    }

    public final boolean getState() {
        return this.aCJ;
    }

    public final boolean isPressed() {
        return this.aCJ;
    }

    public final float getX() {
        return this.x;
    }

    public final float getY() {
        return this.y;
    }

    public final int getScreenX() {
        return this.aCK;
    }

    public final int getScreenY() {
        return this.aCL;
    }

    public adl Fp() {
        return aCM;
    }

    public int getModifiers() {
        return this.modifiers;
    }
}
