package all;

import gnu.trove.THashMap;
import taikodom.infra.script.I18NString;
import taikodom.render.BinkTexture;
import taikodom.render.TexBackedImage;
import taikodom.render.enums.TexMagFilter;
import taikodom.render.enums.TexMinFilter;
import taikodom.render.textures.BinkDataSource;
import taikodom.render.textures.FlashTexture;

import java.awt.*;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.Map;

public class LoaderImageBikSwfPng implements ILoaderImageInterface {
    private ain rootPathRender;
    private Map gAJ = Collections.synchronizedMap(new THashMap());
    private Map gAK = Collections.synchronizedMap(new THashMap());

    public LoaderImageBikSwfPng(EngineGraphics var1) {
        this.gAK.put("default-cursor", Cursor.getPredefinedCursor(0));
        this.gAK.put("crosshair-cursor", Cursor.getPredefinedCursor(1));
        this.gAK.put("text-cursor", Cursor.getPredefinedCursor(2));
        this.gAK.put("wait-cursor", Cursor.getPredefinedCursor(3));
        this.gAK.put("sw-resize-cursor", Cursor.getPredefinedCursor(4));
        this.gAK.put("se_q-resize-cursor", Cursor.getPredefinedCursor(5));
        this.gAK.put("nw-resize-cursor", Cursor.getPredefinedCursor(6));
        this.gAK.put("ne-resize-cursor", Cursor.getPredefinedCursor(7));
        this.gAK.put("createProcessingInstructionSelector-resize-cursor", Cursor.getPredefinedCursor(8));
        this.gAK.put("s-resize-cursor", Cursor.getPredefinedCursor(9));
        this.gAK.put("w-resize-cursor", Cursor.getPredefinedCursor(10));
        this.gAK.put("CreateJComponentAndAddInRootPane-resize-cursor", Cursor.getPredefinedCursor(11));
        this.gAK.put("hand-cursor", Cursor.getPredefinedCursor(12));
        this.gAK.put("move-cursor", Cursor.getPredefinedCursor(13));
    }

    /**
     * Загрузка формата .bik
     *
     * @param var1
     * @return
     */
    private Image loadBik(String var1) {
        var1 = this.eH(var1);
        //Пример /data/gui/imageset/Splash_Screen.bik

        // OJ.wBinkSoundUseDirectSound(0);
        // fN var2 = OJ.CreateJComponent(this.rootPathRender.concat(var1).getFile().getPath(), 1048576L);
        BinkDataSource var3 = new BinkDataSource(1056768L, 1056768L);
        var3.setFileName(var1);
        BinkTexture var4 = new BinkTexture(var3);
        var4.setName(var1);

        var4.setMagFilter(TexMagFilter.LINEAR);
        var4.setMinFilter(TexMinFilter.LINEAR);

        float var5;
        float var6;
        if (var4.getType() == 3553) {
            var5 = (float) var4.getVideoSizeX() / (float) var4.getSizeX();
            var6 = (float) var4.getVideoSizeY() / (float) var4.getSizeY();
        } else {
            var5 = 1.0F;
            var6 = 1.0F;
        }

        TexBackedImage var7 = new TexBackedImage(var4.getSizeY(), var4.getSizeX(), var5, var6, var4);
        return var7;
    }

    /**
     * Попытаться получить локализованный файл
     *
     * @param var1 Пример /data/gui/imageset/Splash_Screen.bik
     * @return
     */
    private String eH(String var1) {
        String var2 = I18NString.getCurrentLocation();//Язык локализации
        if (var2 != null) {
            String var3 = var1.replaceAll("([.][^.]+)$", "_" + var2 + "$1");
            //Пример /data/gui/imageset/Splash_Screen_en.bik
            if (this.rootPathRender.concat(var3).exists()) {
                var1 = var3;
            }
        }
        return var1;
    }

    /**
     * Загрузка формата .swf
     *
     * @param var1
     * @return
     */
    private Image loadSwf(String var1) {
        var1 = this.eH(var1);
        ain var2 = this.rootPathRender.concat(var1);
        ByteBuffer var3 = var2.BI();
        FlashTexture var4 = new FlashTexture(new ol(var3, var2.length()));
        var4.setName(var1);
        TexBackedImage var5 = new TexBackedImage(var4.getSizeY(), var4.getSizeX(), var4);
        return var5;
    }

    /**
     * Загрузка графического ресурса
     *
     * @param pathFile Пример /data/gui/imageset/Splash_Screen.bik
     * @return
     */
    public Image getImage(String pathFile) {
        if (pathFile == null) {
            return null;
        } else if ("none".equals(pathFile)) {
            return null;
        }

        //Формирование адреса
        String var2 = pathFile;
        if (pathFile.startsWith("res://")) {
            var2 = pathFile.substring("res://".length());
        }
        String var3 = var2;
        Image var4 = (Image) this.gAJ.get(var2);//Поиск графики среди загруженных
        Image var5 = var4 != null ? var4 : null;

        if (var5 != null) {
            return var5;
        } else if (var2.endsWith(".bik"))//Проверяем окончания
        {
            return this.loadBik(var2);
        } else if (var2.endsWith(".swf")) {
            return this.loadSwf(var2);
        } else {
            if (!var2.startsWith("data/"))//Смотрим если начало не
            {  //Пример imageset_window_neo/material/window_content_upper_left
                var2 = "/data/gui/imageset/" + var2.replaceAll("[/]material", "");
                //Пример /data/gui/imageset/imageset_window_neo/window_content_upper_left
            } else {
                var2 = "/" + var2.replaceAll("[/]material", "");
            }

            String var6 = I18NString.getCurrentLocation();//Язык локализации
            var4 = (Image) this.gAJ.get(var2);//Поиск графики среди загруженных
            var5 = var4 != null ? var4 : null;
            if (var5 != null) {
                return var5;
            } else {
                //FileControl
                ain var7 = this.rootPathRender.concat(var2 + "_" + var6 + ".png");
                //Пример .\data\gui\imageset\imageset_window_neo\window_content_upper_left_en.png

                var4 = (Image) this.gAJ.get(var7.getPath());//Поиск графики среди загруженных
                var5 = var4 != null ? var4 : null;
                if (var5 != null) {
                    return var5;
                } else {
                    Image var8;
                    if (var7.exists())//Есть ли файл
                    {
                        var2 = var2 + "_" + var6 + ".png";
                        var8 = Toolkit.getDefaultToolkit().createImage(var7.getByte());
                    } else {
                        var2 = var2 + ".png";
                        //Пример /data/gui/imageset/imageset_window_neo/window_content_upper_left.png

                        var7 = this.rootPathRender.concat(var2);
                        //Пример .\data\gui\imageset\imageset_window_neo\window_content_upper_left.png

                        if (!var7.exists()) {
                            var8 = null;
                            throw new IllegalArgumentException("Image '" + var2 + "' was not found in resources dir");
                        }
                        var8 = Toolkit.getDefaultToolkit().createImage(var7.getByte());
                    }
                    MediaTracker var9 = new MediaTracker(new Label());
                    var9.addImage(var8, 0);

                    try {
                        var9.waitForID(0, 10000L);
                    } catch (InterruptedException var11) {
                        throw new RuntimeException(var11);
                    }

                    this.gAJ.put(var3, var8);
                    this.gAJ.put(var2, var8);
                    this.gAJ.put(var7.getPath(), var8);
                    return var8;
                }
            }
        }
    }

    public void setRootPathRender(ain var1) {
        this.rootPathRender = var1;
    }

    public Cursor getNull(String var1, Point var2) {
        String var3;
        if (var2 == null || var2.x == 0 && var2.y == 0) {
            var3 = var1;
        } else {
            var3 = var1 + ":" + var2;
        }

        Cursor var4 = (Cursor) this.gAK.get(var3);
        if (var4 != null) {
            return var4;
        } else {
            Image var5 = this.getImage(var1);
            var4 = Toolkit.getDefaultToolkit().createCustomCursor(var5, var2, var1);
            this.gAK.put(var3, var4);
            return var4;
        }
    }
}
