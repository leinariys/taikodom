package all;

import javax.vecmath.Tuple2d;
import javax.vecmath.Tuple2f;
import javax.vecmath.Vector2d;
import javax.vecmath.Vector2f;

public class aQKa extends Vector2f implements afA_q {
    public aQKa() {
    }

    public aQKa(float[] var1) {
        super(var1);
    }

    public aQKa(Vector2f var1) {
        super(var1);
    }

    public aQKa(Vector2d var1) {
        super(var1);
    }

    public aQKa(Tuple2f var1) {
        super(var1);
    }

    public aQKa(Tuple2d var1) {
        super(var1);
    }

    public aQKa(float var1, float var2) {
        super(var1, var2);
    }

    public aQKa mU(float var1) {
        aQKa var2 = new aQKa();
        var2.x = this.x * var1;
        var2.y = this.y * var1;
        return var2;
    }

    public void add(double var1, double var3) {
        this.x = (float) ((double) this.x + var1);
        this.y = (float) ((double) this.y + var3);
    }

    public aQKa f(double var1, double var3) {
        this.x = (float) ((double) this.x + var1);
        this.y = (float) ((double) this.y + var3);
        return this;
    }

    public aQKa a(Tuple2f var1) {
        this.x -= var1.x;
        this.y -= var1.y;
        return this;
    }

    public aQKa g(double var1, double var3) {
        aQKa var5 = new aQKa(this);
        var5.x = (float) ((double) var5.x + var1);
        var5.y = (float) ((double) var5.y + var3);
        return var5;
    }

    public void readExternal(Vm_q var1) {
        this.x = var1.gZ("x");
        this.y = var1.gZ("y");
    }

    public void writeExternal(att var1) {
        var1.a("x", this.x);
        var1.a("y", this.y);
    }
}
