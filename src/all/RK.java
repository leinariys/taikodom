package all;

import org.w3c.dom.css.CSSRule;
import org.w3c.dom.css.CSSRuleList;

import java.io.Serializable;
import java.util.ArrayList;

public class RK extends ArrayList implements Serializable, CSSRuleList {
    private static final long serialVersionUID = 1L;

    public RK() {
        super(1);
    }

    public int getLength() {
        return this.size();
    }

    public CSSRule item(int var1) {
        return (CSSRule) this.get(var1);
    }

    public void add(CSSRule var1, int var2) {
        super.add(var2, var1);
    }

    public void delete(int var1) {
        this.remove(var1);
    }

    public String toString() {
        StringBuffer var1 = new StringBuffer();

        for (int var2 = 0; var2 < this.getLength(); ++var2) {
            var1.append(this.item(var2).toString()).append("\r\n");
        }

        return var1.toString();
    }
}
