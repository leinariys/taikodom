package all;

public class UX extends Exception {
    private static final long serialVersionUID = 1L;

    public UX() {
    }

    public UX(String var1, Throwable var2) {
        super(var1, var2);
    }

    public UX(String var1) {
        super(var1);
    }

    public UX(Throwable var1) {
        super(var1);
    }
}
