package all;

import javax.swing.*;
import java.awt.*;

public class GBoxCustom extends BaseItemFactory {
    private static final LogPrinter logger = LogPrinter.setClass(GBoxCustom.class);

    /**
     * Создание базового Component интерфейса
     * Вызывается из class BaseItem
     *
     * @param var1
     * @param var2
     * @param var3
     * @return
     */
    @Override
    protected JComponent CreatComponentCustom(MapKeyValue var1, Container var2, XmlNode var3) {
        return new GBoxCustomWrapper();
    }

    protected void a(MapKeyValue var1, GBoxCustomWrapper var2, XmlNode var3) {
        super.createLayout(var1, var2, var3);
        Integer var4 = checkValueAttribut(var3, "cols", logger);
        if (var4 != null) {
            var2.setColumns(var4.intValue());
        }
    }
}
