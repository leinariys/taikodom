package all;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Фабрика потоков для очереди потоков
 * NT
 */
public class PoolThread {
    /**
     * Настройка
     */
    private static final String lineSeparator = System.getProperty("line.separator");

    public static Thread initThreadWrapper(Runnable var0, String var1) {
        return new ThreadWrapper(var0, var1);
    }

    public static ThreadPoolExecutor a(int var0, int var1, long var2, TimeUnit var4, BlockingQueue var5, String var6, boolean var7) {
        return new ThreadPoolExecutor(var0, var1, var2, var4, var5, new ThreadFac(var6, var7));
    }

    public static void aC(Object var0) {
        try {
            var0.wait();
        } catch (InterruptedException var2) {
            throw new RuntimeException("wait interrupted", var2);
        }
    }

    public static void a(Object var0, long var1) {
        try {
            var0.wait(var1);
        } catch (InterruptedException var4) {
            throw new RuntimeException("wait interrupted", var4);
        }
    }

    public static void sleep(long var0) {
        try {
            Thread.sleep(var0);
        } catch (InterruptedException var3) {
            throw new RuntimeException("sleep interrupted", var3);
        }
    }

    public static Thread g(Runnable var0) {
        ThreadWrapper var1 = new ThreadWrapper(var0);
        var1.setDaemon(true);
        var1.start();
        return var1;
    }

    public static Thread b(Runnable var0, String var1) {
        ThreadWrapper var2 = new ThreadWrapper(var0, var1);
        var2.setDaemon(true);
        var2.start();
        return var2;
    }

    public static void d(Thread var0) {
        try {
            var0.join();
        } catch (InterruptedException var2) {
            throw new RuntimeException("join interrupted", var2);
        }
    }

    /**
     * Статистика работы фабрики потоков
     *
     * @return
     */
    public static String cIF() {
        StringBuilder var0 = new StringBuilder();
        var0.append("THREAD COUNT: " + Thread.activeCount() + lineSeparator + lineSeparator);
        Map var1 = Thread.getAllStackTraces();
        var0.append("NON DAEMON THREADS:" + lineSeparator);
        boolean var2 = false;
        Iterator var4 = var1.keySet().iterator();

        while (var4.hasNext()) {
            Thread var3 = (Thread) var4.next();
            if (var3.isAlive() && !var3.isDaemon()) {
                var2 = true;
                var0.append("   * " + var3.getName() + lineSeparator);
            }
        }

        if (!var2) {
            var0.append(" ->  All active Threads are of daemon type" + lineSeparator);
        }

        var0.append(lineSeparator);
        var0.append("STACK TRACES:" + lineSeparator);
        int var8 = 0;
        Iterator var5 = var1.entrySet().iterator();

        while (var5.hasNext()) {
            Entry var9 = (Entry) var5.next();
            Thread var6 = (Thread) var9.getKey();
            StackTraceElement[] var7 = (StackTraceElement[]) var9.getValue();
            var0.append(var8++ + " - " + a(var6, var7));
        }

        return var0.toString();
    }

    /**
     * Часть сообщения для статистики
     *
     * @param var0 Поток
     * @return Часть сообщения
     */
    public static String e(Thread var0) {
        return a(var0, var0.getStackTrace());
    }

    /**
     * Часть сообщения для статистики
     *
     * @param var0 Поток
     * @param var1
     * @return Часть сообщения
     */
    public static String a(Thread var0, StackTraceElement[] var1) {
        StringBuilder var2 = new StringBuilder();
        var2.append(var0.getName() + ":" + lineSeparator);
        if (var1.length > 0) {
            for (int var3 = 0; var3 < var1.length; ++var3) {
                var2.append("\tat " + var1[var3] + lineSeparator);
            }
        } else {
            var2.append("\t<NO STACK TRACE INFORMATION>" + lineSeparator);
        }

        return var2.toString();
    }

    /**
     * Тест работы фабрики потоков
     */
    public static void main(String[] var0) {
        Runnable var1 = new Runnable() {
            public void run() {
                try {
                    Thread.sleep(1000L);
                } catch (InterruptedException var1) {
                }
            }
        };
        Thread var2 = new Thread(var1, "T1-nondaemon");
        var2.setDaemon(false);
        Thread var3 = new Thread(var1, "T2-daemon");
        var3.setDaemon(true);
        Thread var4 = new Thread(var1, "T3-doesnothing");
        var2.start();
        var3.start();
        var4.start();

        try {
            Thread.sleep(100L);
        } catch (InterruptedException var5) {
        }

        System.out.println(cIF());
    }

    public static class ThreadFac implements ThreadFactory {
        /**
         * Имя потока
         */
        String prefix;
        boolean isDaemon;
        /**
         * Атомная операция
         * <getByteBuffer>Можно безопасно выполнять при параллельных
         * вычислениях не используя блокировщик и синхронизацию
         */
        AtomicInteger atomicInteger = new AtomicInteger();

        /**
         * Задать Параметры потока
         *
         * @param var1 Имя потока Worker/
         * @param var2 isDaemon
         */
        public ThreadFac(String var1, boolean var2) {
            if (var1 == null) {
                this.prefix = "Thread";
            } else {
                this.prefix = var1;
            }
            this.isDaemon = var2;
        }

        /**
         * Задать параметры потока стандартные
         */
        public ThreadFac() {
            this.prefix = "Thread";
            this.isDaemon = false;
        }

        public Thread newThread(Runnable var1) {
            ThreadWrapper var2 = new ThreadWrapper(var1, this.prefix + " " + this.atomicInteger.incrementAndGet());
            var2.setDaemon(this.isDaemon);
            return var2;
        }
    }
}
