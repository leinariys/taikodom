package all;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * Контейнер сообщения диалогового окна не локализованный
 * class MessageDialogAddon
 */
public class Nn extends WA implements Externalizable {
    /**
     * Текст сообщения
     */
    private String message;
    /**
     * Параметры
     */
    private Object[] dmZ;
    /**
     * Заголовок
     */
    private atitle grl;

    /**
     * Задать настройки сообщения
     *
     * @param var1 Заголовок
     * @param var2 Текст сообщения
     * @param var3 Параметры
     */
    public Nn(atitle var1, String var2, Object... var3) {
        this.grl = var1;
        this.message = var2;
        this.dmZ = var3;
    }

    /**
     * Получить заголовок
     *
     * @return
     */
    public atitle crM() {
        return this.grl;
    }

    /**
     * Получить Текст сообщения
     *
     * @return
     */
    public String getMessage() {
        return this.message;
    }

    /**
     * Получить параметры
     *
     * @return
     */
    public Object[] getParams() {
        return this.dmZ;
    }

    public void readExternal(ObjectInput var1) {
        throw new UnsupportedOperationException("Do not use this class serverside");
    }

    public void writeExternal(ObjectOutput var1) {
        throw new UnsupportedOperationException("Do not use this class serverside");
    }

    /**
     * Заголовок диалогового окна
     */
    public static enum atitle {
        info("Info"),
        question("Question"), /*Выбор действия ок или нет*/
        warning("Warning"),
        error("Error");
        /**
         * Заголовок
         */
        public final String title;

        private atitle(String var3) {
            this.title = var3;
        }
    }
}
