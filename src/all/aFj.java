package all;

import java.io.IOException;
import java.io.OutputStream;

public class aFj extends OutputStream {
    private final OutputStream out;
    private int oZ = 48151623;

    public aFj(OutputStream var1) {
        this.out = var1;
    }

    public static int aC(int var0, int var1) {
        return var0 * 9932467 + 7890661 + (var1 & 255);
    }

    public void close() throws IOException {
        this.out.close();
    }

    public void flush() throws IOException {
        this.out.flush();
    }

    public void write(int var1) throws IOException {
        int var2 = (var1 & 255 ^ this.oZ & 255) & 255;
        this.out.write(var2);
        this.oZ = aC(this.oZ, var1);
    }
}
