package all;

//import com.hoplon.commons.TaikodomLogger.TaikodomLoggerConsole;

import org.apache.commons.logging.Log;

import javax.management.MBeanServer;
import java.io.*;
import java.lang.management.ManagementFactory;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;


public class LogPrinter implements Log {
    public static final byte eqK = 2;
    public static final byte eqL = 4;
    public static final byte eqM = 8;
    public static final byte eqN = 16;
    public static final byte eqO = 32;
    public static final byte eqP = 64;
    public static final byte eqQ = 126;
    public static final byte eqR = 120;
    private static final SimpleDateFormat dBH = new SimpleDateFormat(
            "yyyy-MM-dd HH:mm:ss.SSS");
    private static final SimpleDateFormat eqV = new SimpleDateFormat(
            "yyyyMMdd-HHmmss-SSS");
    public static byte eqS = 120;
    public static byte eqT = 0;
    public static boolean eqU = false;
    private static Map<Class<?>, LogPrinter> eqW;
    private static Properties eqX;
    private static boolean eqY = true;
    private static String eqZ = null;
    private PrintWriter printWriter = new PrintWriter(System.out, true);
    private String eqD = "[TRACE]";
    private String eqE = "[DEBUG]";
    private String eqF = "[INFO ]";
    private String eqG = "[WARN ]";
    private String eqH = "[ERROR]";
    private String eqI = "[FATAL]";
    private byte eqJ;
    private String era;

    private LogPrinter(Class<?> paramClass) {
        int i = paramClass.getName().lastIndexOf('.');
        this.era = (i == -1 ? paramClass.getName() :
                paramClass.getName().substring(i + 1));

        byte b = gM(this.era);
        if (b == 0) {
            b(eqT);
        } else {
            b(b);
        }
    }

    /**
     * Класс подписывается на логирование
     *
     * @param paramClass
     * @return
     */
    public static LogPrinter K(Class<?> paramClass) {
        return setClass(paramClass);
    }

    public static LogPrinter setClass(Class<?> paramClass) {
        if (eqY) {
            eqY = false;
            byt();
        }

        if (eqW == null) {
            aqT();
            eqW = new ConcurrentHashMap();
        }
        LogPrinter localUR = (LogPrinter) eqW.get(paramClass);
        if (localUR == null) {
            localUR = new LogPrinter(paramClass);
            eqW.put(paramClass, localUR);
        }
        return localUR;
    }

    private static void byt() {
        eqX = new Properties();
        File localFile = new File("taikodomlogger.properties");
        if (!localFile.exists())
            localFile = new File("res/client/config", "taikodomlogger.properties");
        if (!localFile.exists()) {
            System.out.println("TaikodomLogger: test_GLCanvas taikodomlogger.properties found, using default: info,warn,error,fatal");
        } else {
            try {
                eqX.load(new FileInputStream(localFile));
            } catch (FileNotFoundException localFileNotFoundException) {
            } catch (IOException localIOException) {
                localIOException.printStackTrace();
            }
        }

        byte i = gM("levels");
        eqT = i != 0 ? i : 120;

        String str = System.getProperty("TaikodomLogger.DISABLE");
        eqU = (str != null) && (str.equalsIgnoreCase("true"));
    }

    private static byte gM(String paramString) {
        byte b = 0;
        if (eqX != null) {
            String str1 = eqX.getProperty(paramString, null);
            if (str1 != null) {
                String[] arrayOfString = str1.split(",");
                for (int i = 0; i < arrayOfString.length; i++) {
                    String str2 = arrayOfString[i];
                    if (str2.equals("trace")) {
                        b = (byte) (b | 0x2);
                    } else if (str2.equals("debug")) {
                        b = (byte) (b | 0x4);
                    } else if (str2.equals("info")) {
                        b = (byte) (b | 0x8);
                    } else if (str2.equals("warn")) {
                        b = (byte) (b | 0x10);
                    } else if (str2.equals("error")) {
                        b = (byte) (b | 0x20);
                    } else if (str2.equals("fatal")) {
                        b = (byte) (b | 0x40);
                    }
                }
            }
        }
        return b;
    }

    private static synchronized void aqT() {
        MBeanServer localMBeanServer = ManagementFactory.getPlatformMBeanServer();
        if (localMBeanServer == null) {
            return;
        }
    }

    /**
     * Логирование
     *
     * @param paramString1
     * @param paramString2
     * @return
     * @throws FileNotFoundException
     * @throws UnsupportedEncodingException
     */
    public static PrintWriter createLogFile(String paramString1, String paramString2) throws FileNotFoundException, UnsupportedEncodingException {
        String str1 = System.getProperty("log-dir", "log");
        String str2 = str1 + "/" + paramString1 + "-" +
                eqV.format(new Date(System.currentTimeMillis())) +
                "." + paramString2;
        new File(str2).getParentFile().mkdirs();
        System.out.println("Opening log file: " + str2);
        return new PrintWriter(new OutputStreamWriter(new FileOutputStream(
                str2, true), "UTF-8"), true);
    }

    public static void gN(String paramString) {
        eqZ = paramString;
    }

    public void b(byte paramByte) {
        this.eqJ = paramByte;
    }

    public void c(byte paramByte) {
        this.eqJ = ((byte) (this.eqJ | paramByte));
    }

    public void d(byte paramByte) {
        this.eqJ = ((byte) (this.eqJ & (paramByte ^ 0xFFFFFFFF)));
    }

    private void r(String paramString, Object paramObject) {
        if (eqU) {
            return;
        }
        if ((paramObject instanceof Throwable)) {
            a(paramString, paramObject.toString(), (Throwable) paramObject);
        } else {
            this.printWriter.println(format(paramString, paramObject));
            if ((eqZ != null) && (paramObject != null) &&
                    (paramObject.toString().indexOf(eqZ) != -1)) {
                Thread.dumpStack();
            }
        }
    }

    private void a(String paramString, Object paramObject, Throwable paramThrowable) {
        if (eqU) {
            return;
        }
        this.printWriter.println(format(paramString, paramObject));
        paramThrowable.printStackTrace(this.printWriter);
    }

    private String format(String paramString, Object paramObject) {
        long l = System.currentTimeMillis();
        return Thread.currentThread().getId() + " " +
                dBH.format(new Date(l)) + " " + paramString + " " +
                this.era + ": " + paramObject;
    }

    public void debug(Object paramObject) {
        if (isDebugEnabled()) {
            r(this.eqE, paramObject);
        }
    }

    public void debug(Object paramObject, Throwable paramThrowable) {
        if (isDebugEnabled()) {
            a(this.eqE, paramObject, paramThrowable);
        }
    }

    public void error(Object paramObject) {
        if (isErrorEnabled()) {
            r(this.eqH, paramObject);
        }
    }

    public void error(Object paramObject, Throwable paramThrowable) {
        if (isErrorEnabled()) {
            a(this.eqH, paramObject, paramThrowable);
        }
    }

    public void fatal(Object paramObject) {
        if (isFatalEnabled()) {
            r(this.eqI, paramObject);
        }
    }

    public void fatal(Object paramObject, Throwable paramThrowable) {
        if (isFatalEnabled()) {
            a(this.eqI, paramObject, paramThrowable);
        }
    }

    public void info(Object paramObject) {
        if (isInfoEnabled()) {
            r(this.eqF, paramObject);
        }
    }

    public void info(Object paramObject, Throwable paramThrowable) {
        if (isInfoEnabled()) {
            a(this.eqF, paramObject, paramThrowable);
        }
    }

    public void trace(Object paramObject) {
        if (isTraceEnabled()) {
            r(this.eqD, paramObject);
        }
    }

    public void trace(Object paramObject, Throwable paramThrowable) {
        if (isTraceEnabled()) {
            a(this.eqD, paramObject, paramThrowable);
        }
    }

    public void warn(Object paramObject) {
        if (isWarnEnabled()) {
            r(this.eqG, paramObject);
        }
    }

    public void warn(Object paramObject, Throwable paramThrowable) {
        if (isWarnEnabled()) {
            a(this.eqG, paramObject, paramThrowable);
        }
    }

    public boolean isDebugEnabled() {
        return (!eqU) && ((this.eqJ & 0x4) == 4);
    }

    public boolean isErrorEnabled() {
        return (!eqU) && ((this.eqJ & 0x20) == 32);
    }

    public boolean isFatalEnabled() {
        return (!eqU) && ((this.eqJ & 0x40) == 64);
    }

    public boolean isInfoEnabled() {
        return (!eqU) && ((this.eqJ & 0x8) == 8);
    }

    public boolean isTraceEnabled() {
        return (!eqU) && ((this.eqJ & 0x2) == 2);
    }

    public boolean isWarnEnabled() {
        return (!eqU) && ((this.eqJ & 0x10) == 16);
    }
}

