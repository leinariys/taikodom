package all;

import taikodom.infra.script.I18NString;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * Контейнер сообщения диалогового окна локализованный
 * class MessageDialogAddon
 */
public class aqX extends WA implements Externalizable {

    private I18NString auP;
    /**
     * Параметры
     */
    private Object[] dmZ;
    /**
     * Заголовок
     */
    private Nn.atitle grl;

    public aqX() {
    }

    /**
     * Задать настройки сообщения
     *
     * @param var1 Заголовок
     * @param var2 Текст сообщения локализованный
     * @param var3 Параметры
     */
    public aqX(Nn.atitle var1, I18NString var2, Object... var3) {
        this.grl = var1;
        this.auP = var2;
        this.dmZ = var3;
    }

    /**
     * Получить заголовок
     *
     * @return
     */
    public Nn.atitle crM() {
        return this.grl;
    }

    /**
     * Получить Текст сообщения локализованный
     *
     * @return
     */
    public I18NString KI() {
        return this.auP;
    }

    /**
     * Получить параметры
     *
     * @return
     */
    public Object[] getParams() {
        return this.dmZ;
    }

    /**
     * Чтение с файла
     *
     * @param var1
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public void readExternal(ObjectInput var1) throws IOException, ClassNotFoundException {
        this.auP = (I18NString) var1.readObject();
        this.dmZ = (Object[]) var1.readObject();
    }

    /**
     * Запись в файл
     *
     * @param var1
     * @throws IOException
     */
    public void writeExternal(ObjectOutput var1) throws IOException {
        var1.writeObject(this.auP);
        var1.writeObject(this.dmZ);
    }
}
