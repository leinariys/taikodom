package all;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.awt.*;

public final class TextAreaCustom extends TextComponentCustom {
    /**
     * Создание базового Component интерфейса
     * Вызывается из class BaseItem
     *
     * @param var1
     * @param var2
     * @param var3
     * @return
     */
    @Override
    protected JComponent CreatComponentCustom(MapKeyValue var1, Container var2, XmlNode var3) {
        return new TextAreaCustomWrapper();
    }

    protected void a(MapKeyValue var1, TextAreaCustomWrapper var2, XmlNode var3) {
        super.a(var1, (JTextComponent) var2, var3);
        if ("true".equalsIgnoreCase(var3.getAttribute("multiline"))) {
            var2.setWrapStyleWord(true);
            var2.setLineWrap(true);
        }

        if ("false".equals(var3.getAttribute("selectable"))) {
            var2.bO(false);
        }

        Integer var4 = checkValueAttribut((XmlNode) var3, (String) "rows", (LogPrinter) logger);
        if (var4 != null) {
            var2.setRows(var4.intValue());
        }

        var4 = checkValueAttribut((XmlNode) var3, (String) "cols", (LogPrinter) logger);
        if (var4 != null) {
            var2.setColumns(var4.intValue());
        }

    }
}
