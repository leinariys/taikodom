package all;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicDesktopPaneUI;
import java.awt.*;

/**
 * DesktopPaneUI
 */
public class DesktopPaneUI extends BasicDesktopPaneUI implements aIh {
    private ComponentManager Rp;

    public DesktopPaneUI(JDesktopPane var1) {
        this.Rp = new ComponentManager("desktoppane", var1);
        var1.setOpaque(false);
    }

    public static ComponentUI createUI(JComponent var0) {
        return new DesktopPaneUI((JDesktopPane) var0);
    }

    public void update(Graphics var1, JComponent var2) {
        this.paint(var1, var2);
    }

    public void paint(Graphics var1, JComponent var2) {
        RM.a((aIh) this, (Graphics) var1, (JComponent) var2);
        RM.a((ComponentUI) this, (Graphics) var1, (JComponent) var2);
        super.paint(var1, var2);
        RM.a((ComponentUI) this, (Graphics) var1, (JComponent) var2);
    }

    public Dimension getPreferredSize(JComponent var1) {
        return this.wy().c(var1, super.getPreferredSize(var1));
    }

    public Dimension getMinimumSize(JComponent var1) {
        return this.wy().b(var1, super.getMinimumSize(var1));
    }

    public Dimension getMaximumSize(JComponent var1) {
        return this.wy().a(var1, super.getMaximumSize(var1));
    }

    public ComponentManager wy() {
        return this.Rp;
    }

    public boolean contains(JComponent var1, int var2, int var3) {
        return ComponentManager.getCssHolder(var1).Vs() ? super.contains(var1, var2, var3) : false;
    }

    /**
     * TODO похлже это public pv wy()
     *
     * @return Графический элемент
     */
    @Override
    public aeK wz() {
        return this.Rp;
    }
}
