package all;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreePath;
import java.awt.event.MouseEvent;

public class acz extends JTree {
    private static final long serialVersionUID = 1L;
    private DefaultMutableTreeNode fbY;
    private TreePath fbZ;
    private DefaultTreeModel fca;

    public acz() {
        this.setCellRenderer(new TE());
        this.fbY = new DefaultMutableTreeNode("hidden root");
        this.fbZ = new TreePath(this.fbY);
        this.fca = new DefaultTreeModel(this.fbY);
        this.setModel(this.fca);
        this.setToggleClickCount(1);
        this.setRowHeight(0);
        this.setRootVisible(false);
        this.setShowsRootHandles(true);
    }

    public void a(MutableTreeNode var1) {
        this.fbY.add(var1);
    }

    public void bOW() {
        this.fbY.removeAllChildren();
        this.fca.reload();
    }

    public void bOX() {
        this.expandPath(this.fbZ);
    }

    public void expandAll() {
        this.expandPath(this.fbZ);

        for (int var1 = 0; var1 < this.getRowCount(); ++var1) {
            this.expandRow(var1);
        }

    }

    public TreePath bOY() {
        return this.fbZ;
    }

    public String getToolTipText(MouseEvent var1) {
        if (wz.d(this) != null) {
            TreePath var2 = this.getPathForLocation(var1.getPoint().x, var1.getPoint().y);
            if (var2 != null) {
                DefaultMutableTreeNode var3 = (DefaultMutableTreeNode) var2.getLastPathComponent();
                if (var3 != null) {
                    Object var4 = var3.getUserObject();
                    if (var4 != null) {
                        return var4.toString();
                    }
                }
            }
        }

        return super.getToolTipText(var1);
    }
}
