package all;

import javax.swing.*;
import java.awt.*;

public class LabeledIconCustom extends BaseItemFactory {
    /**
     * Создание базового Component интерфейса
     * Вызывается из class BaseItem
     *
     * @param var1
     * @param var2
     * @param var3
     * @return
     */
    @Override
    protected JComponent CreatComponentCustom(MapKeyValue var1, Container var2, XmlNode var3) {
        return new LabeledIconCustomWrapper();
    }

    protected void a(MapKeyValue var1, LabeledIconCustomWrapper var2, XmlNode var3) {
        super.createLayout(var1, var2, var3);
        String var4 = var3.getAttribute("ne-text");
        if (var4 != null) {
            var2.a(LabeledIconCustomWrapper.a.ddD, var4);
        }

        var4 = var3.getAttribute("nw-text");
        if (var4 != null) {
            var2.a(LabeledIconCustomWrapper.a.ddC, var4);
        }

        var4 = var3.getAttribute("se-text");
        if (var4 != null) {
            var2.a(LabeledIconCustomWrapper.a.ddF, var4);
        }

        var4 = var3.getAttribute("sw-text");
        if (var4 != null) {
            var2.a(LabeledIconCustomWrapper.a.ddE, var4);
        }

        var4 = var3.getAttribute("ne-iconImage");
        if (var4 != null) {
            var2.b(LabeledIconCustomWrapper.a.ddD, var4);
        }

        var4 = var3.getAttribute("nw-iconImage");
        if (var4 != null) {
            var2.b(LabeledIconCustomWrapper.a.ddC, var4);
        }

        var4 = var3.getAttribute("se-iconImage");
        if (var4 != null) {
            var2.b(LabeledIconCustomWrapper.a.ddF, var4);
        }

        var4 = var3.getAttribute("sw-iconImage");
        if (var4 != null) {
            var2.b(LabeledIconCustomWrapper.a.ddE, var4);
        }

        var4 = var3.getAttribute("bg-image");
        if (var4 != null) {
            var2.setBackgroundImage(var4);
        }

    }
}
