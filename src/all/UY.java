package all;

import javax.swing.*;

public class UY extends ComponentManager {
    public UY(String var1, AbstractButton var2) {
        super(var1, var2);
    }

    public int Vi() {
        int var1 = super.Vi();
        var1 &= -64;
        ButtonModel var2 = ((AbstractButton) this.getCurrentComponent()).getModel();
        if (var2.isArmed()) {
            var1 |= 1;
        }

        if (var2.isSelected()) {
            var1 |= 2;
        }

        if (var2.isEnabled()) {
            var1 |= 4;
        } else {
            var1 |= 8;
        }

        if (var2.isRollover()) {
            var1 |= 32;
        } else {
            var1 &= -33;
        }

        if (var2.isPressed()) {
            var1 |= 16;
        }

        return var1;
    }
}
