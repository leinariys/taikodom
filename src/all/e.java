package all;

import taikodom.addon.messagedialog.MessageDialogAddon;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

/**
 * Окно регистрации
 * class LoginAddon
 */
public class e {
    private static final String aof = "createUser.xml";
    private final vW kj;
    private final String location;
    private final String urlCreateUser;
    private WindowCustomWrapper windowCreateUser;
    private PasswordFieldCustomWrapper passwordField;
    private PasswordFieldCustomWrapper confirmPasswordField;
    private TextfieldCustomWrapper emailField;
    private JSpinner dayField;
    private JSpinner monthField;
    private JSpinner yearField;
    private TextfieldCustomWrapper usernameField;
    private JCheckBox newsletterField;
    private HashMap aor;
    private JRadioButton maleField;
    private JRadioButton femaleField;
    private xt loadingIcon;
    private aen aov = new all.e.a();
    private JLabel loadingImage;
    private JButton acceptButton;
    private JButton denyButton;
    private JButton eulaButtonField;
    private JCheckBox eulaField;

    public e(vW var1, String location, String urlCreateUser) {
        this.kj = var1;
        this.location = location;
        this.urlCreateUser = urlCreateUser;
        this.windowCreateUser = (WindowCustomWrapper) var1.bHv().CreatJComponentFromXML("createUser.xml");
        this.getLinksObject();
        this.windowCreateUser.pack();
        this.windowCreateUser.setModal(true);
        this.windowCreateUser.center();
    }

    private void getLinksObject() {
        this.usernameField = this.windowCreateUser.findJTextField("username");
        this.passwordField = (PasswordFieldCustomWrapper) this.windowCreateUser.findJComponent("password");
        this.confirmPasswordField = (PasswordFieldCustomWrapper) this.windowCreateUser.findJComponent("confirmPassword");
        this.emailField = this.windowCreateUser.findJTextField("email");
        this.dayField = (JSpinner) this.windowCreateUser.findJComponent("day");
        this.monthField = (JSpinner) this.windowCreateUser.findJComponent("month");
        this.yearField = (JSpinner) this.windowCreateUser.findJComponent("year");
        this.newsletterField = (JCheckBox) this.windowCreateUser.findJComponent("newsletter");
        this.eulaField = (JCheckBox) this.windowCreateUser.findJComponent("eula");
        this.maleField = (JRadioButton) this.windowCreateUser.findJComponent("male");
        this.femaleField = (JRadioButton) this.windowCreateUser.findJComponent("female");
        ButtonGroup group = new ButtonGroup();
        group.add(this.maleField);
        group.add(this.femaleField);
        this.eulaButtonField = this.windowCreateUser.findJButton("eulaButton");
        this.eulaButtonField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent var1) {
                e.this.kj.aVU().h(new iV_w());
            }
        });
        this.acceptButton = this.windowCreateUser.findJButton("acceptButton");
        this.acceptButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent var1) {
                e.this.Ib();
            }
        });
        this.denyButton = this.windowCreateUser.findJButton("denyButton");
        this.denyButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent var1) {
                e.this.Ia();
            }
        });
        this.HZ();
        this.loadingIcon = new xt(this.kj.bHv().adz().getImage(ImagesetGui.imagesetLogin.getPath() + "loading_icon"));
        this.loadingImage = this.windowCreateUser.findJLabel("loadingImage");
        this.loadingImage.setIcon(this.loadingIcon);
    }

    private void HZ() {
        this.aor = new HashMap();
        this.aor.put("name", (TextAreaCustomWrapper) this.windowCreateUser.findJComponent("nameError"));
        this.aor.put("email", (TextAreaCustomWrapper) this.windowCreateUser.findJComponent("emailError"));
        this.aor.put("password", (TextAreaCustomWrapper) this.windowCreateUser.findJComponent("passwordError"));
        this.aor.put("birthdate", (TextAreaCustomWrapper) this.windowCreateUser.findJComponent("birthdateError"));
        this.aor.put("gender", (TextAreaCustomWrapper) this.windowCreateUser.findJComponent("genderError"));
    }

    public void xK() {
        this.windowCreateUser.setVisible(true);
        this.usernameField.requestFocusInWindow();
    }

    public void Ia() {
        this.windowCreateUser.setVisible(false);
    }


    public void Ib() {
        Thread var1 = new Thread("Help page") {
            public void run() {
                e.this.Ic();
            }
        };
        var1.setDaemon(true);
        var1.start();
    }

    /**
     * Создание нового пользователя
     */
    public void Ic() {
        String mesError = this.checkingFillingFields();//проверка заполнения полей
        if (mesError != null) {//есть сообщение с ошибкой
            this.showMessageDialog(Nn.atitle.error, mesError);
        } else {
            String mesUserCreat = this.requestUserCreat();
            if (mesUserCreat.length() == 0) {
                this.showMessageDialog(Nn.atitle.error, this.kj.translate("site-server-offline"));
            } else if (mesUserCreat.equalsIgnoreCase("OK")) {
                this.If();
            } else {
                this.aH(mesUserCreat);
            }

        }
    }

    private String Id() {
        return this.dayField.getValue() + "-" + this.monthField.getValue() + "-" + this.yearField.getValue();
    }

    private void aH(String mesUserCreat) {
        this.Ie();

        try {
            StringTokenizer var2 = new StringTokenizer(mesUserCreat, "||");

            while (var2.hasMoreTokens()) {
                String var3 = var2.nextToken();
                if (var3.length() != 0) {
                    int var4 = var3.indexOf(":");
                    if (var4 != -1) {
                        String var5 = var3.substring(0, var4);
                        String var6 = var3.substring(var4 + 1, var3.length());
                        if (this.aor.containsKey(var5)) {
                            TextAreaCustomWrapper var7 = (TextAreaCustomWrapper) this.aor.get(var5);
                            var7.append(var6 + "\n");
                            ComponentManager.getCssHolder(var7).setAttribute("class", "error");
                        }
                    }
                }
            }
        } catch (Exception var8) {
            var8.printStackTrace();
        }

    }

    private void Ie() {
        Collection var1 = this.aor.values();
        Iterator var3 = var1.iterator();

        while (var3.hasNext()) {
            TextAreaCustomWrapper var2 = (TextAreaCustomWrapper) var3.next();
            var2.setText("");
            ComponentManager.getCssHolder(var2).setAttribute("class", "tip");
        }

    }

    private void If() {
        this.Ia();
        this.kj.aVU().h(new IN("7mGNCLelYRDnmYH2Aw"));
        this.showMessageDialog(Nn.atitle.info, this.kj.translate("user-created-msg"));
    }

    private String requestUserCreat() {
        this.startLoadingAndUpdateMarket();
        String response = VN.execute(this.urlCreateUser, new Object[]{"locale", this.location, "external_agent", "true", "name", this.usernameField.getText(), "email", this.emailField.getText(), "password", new String(this.passwordField.getPassword()), "birthdate", this.Id(), "gender", this.Ih(), "newsletter", this.newsletterField.isSelected()});
        this.stopLoading();
        return response;
    }

    private String Ih() {
        String var1 = "male";
        if (this.femaleField.isSelected()) {
            var1 = "female";
        }

        return var1;
    }

    /**
     * Проверка введенных данных
     *
     * @return
     */
    private String checkingFillingFields() {
        Stack var1 = new Stack();
        if (this.usernameField.getText().isEmpty()) {
            var1.push(this.kj.translate("Login"));
        }

        if (this.emailField.getText().isEmpty()) {
            var1.push(this.kj.translate("Email"));
        }

        if (this.passwordField.getPassword().length == 0) {
            var1.push(this.kj.translate("Password"));
        }

        if (this.confirmPasswordField.getPassword().length == 0) {
            var1.push(this.kj.translate("Confirmation"));
        }

        if (!this.maleField.isSelected() && !this.femaleField.isSelected()) {
            var1.push(this.kj.translate("Gender"));
        }

        if (var1.size() <= 0) {
            if (!Arrays.equals(this.passwordField.getPassword(), this.confirmPasswordField.getPassword())) {
                return this.kj.translate("error-pass-not-equal");
            } else {
                return !this.eulaField.isSelected() ? this.kj.translate("error-eula-not-accepted") : null;
            }
        } else {
            String var2 = this.kj.translate("error-following-fields");

            String var3;
            for (Iterator var4 = var1.iterator(); var4.hasNext(); var2 = var2 + "\n - " + var3) {
                var3 = (String) var4.next();
            }

            return var2;
        }
    }


    /**
     * Обновить задачу загрузки рынка
     */
    private void startLoadingAndUpdateMarket() {
        this.acceptButton.setEnabled(false);
        this.denyButton.setEnabled(false);
        this.loadingImage.setVisible(true);
        this.loadingIcon.start();
        this.kj.alf().b("UpdateMarketLoadingTask", this.aov, 10L);
    }

    private void stopLoading() {
        this.acceptButton.setEnabled(true);
        this.denyButton.setEnabled(true);
        this.kj.alf().a(this.aov);
        this.loadingIcon.stop();
        this.loadingImage.setVisible(false);
    }

    private void showMessageDialog(Nn.atitle var1, String var2) {
        ((MessageDialogAddon) this.kj.U(MessageDialogAddon.class)).a(var1, var2, this.kj.translate("OK"));
    }

    private class a extends aen {
        public void run() {
            e.this.loadingImage.repaint();
        }
    }
}
