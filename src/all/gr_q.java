package all;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class gr_q extends akO {
    private static final long serialVersionUID = 1L;
    private long PY;
    private long PZ;
    private long Qa;

    public gr_q() {
    }

    public gr_q(long var1, long var3, long var5) {
        this.PY = var1;
        this.PZ = var3;
        this.Qa = var5;
    }

    public static long getSerialVersionUID() {
        return 1L;
    }

    public void readExternal(ObjectInput var1) throws IOException {
        this.PY = var1.readLong();
        this.PZ = var1.readLong();
        this.Qa = var1.readLong();
    }

    public void writeExternal(ObjectOutput var1) throws IOException {
        var1.writeLong(this.PY);
        var1.writeLong(this.PZ);
        var1.writeLong(this.Qa);
    }

    public long vJ() {
        return this.PY;
    }

    public long vK() {
        return this.PZ;
    }

    public long vL() {
        return this.Qa;
    }
}
