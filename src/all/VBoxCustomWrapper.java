package all;

import javax.swing.plaf.ComponentUI;
import java.awt.*;

public class VBoxCustomWrapper extends BoxCustomWrapper {


    protected Dimension b(int var1, int var2) {
        int var3 = this.getComponentCount();
        int var4 = 0;
        int var5 = 0;

        for (int var6 = 0; var6 < var3; ++var6) {
            Component var7 = this.getComponent(var6);
            Dimension var8 = this.d(var7, var1, var2);
            var5 += var8.height;
            var4 = Math.max(var4, var8.width);
        }

        return new Dimension(var4, var5);
    }

    protected Dimension a(int var1, int var2) {
        int var3 = this.getComponentCount();
        int var4 = 0;
        int var5 = 0;

        for (int var6 = 0; var6 < var3; ++var6) {
            Component var7 = this.getComponent(var6);
            Dimension var8 = this.c(var7, var1, var2);
            var5 += var8.height;
            var4 = Math.max(var4, var8.width);
        }

        return new Dimension(var4, var5);
    }

    public void layout() {
        int var1 = this.getComponentCount();
        PropertiesUiFromCss var2 = PropertiesUiFromCss.a((ComponentUI) this.getUI());
        vI var3 = var2.atR();
        aRU var4 = var2.aty();
        Insets var5 = this.amu();
        Dimension var6 = this.getSize();
        Dimension var7 = new Dimension(var6.width - var5.right - var5.left, var6.height - var5.top - var5.bottom);
        int var8 = var2.ats().getVertical().jp((float) var7.height);
        int var9 = var8;
        b var10 = new b((float) (var7.height - var8), var1);

        int var11;
        for (var11 = 0; var11 < var1; ++var11) {
            var10.a(var11, this.getComponent(var11), var7.width, var7.height - var9);
        }

        var10.Mq();
        var11 = var3.t(var7.height, (int) var10.ghM + var9);

        for (int var12 = 0; var12 < var1; ++var12) {
            Component var13 = this.getComponent(var12);
            if (var10.bjD[var12]) {
                Dimension var14 = var10.bjC[var12];
                int var15 = Math.min(var7.width, var14.width);
                int var16 = var10.ghH[var12];
                Point var17 = this.a(var13, var7.width, var7.height);
                int var18 = var4.t(var7.width, var15);
                int var19 = var4.u(var7.width, var15);
                var13.setBounds(var18 + var17.x + var5.left, var11 + var17.y + var5.top, var19, var16);
                var11 += var16 + var8;
            }
        }

    }

    public String getElementName() {
        return "vbox";
    }
}
