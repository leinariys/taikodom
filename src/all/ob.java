package all;

import com.hoplon.geometry.Vec3f;

public abstract class ob extends yK {
    public abstract Vec3f localGetSupportingVertex(Vec3f var1);

    public abstract Vec3f localGetSupportingVertexWithoutMargin(Vec3f var1);

    public abstract void batchedUnitVectorGetSupportingVertexWithoutMargin(Vec3f[] var1, Vec3f[] var2, int var3);

    public abstract void getAabbSlow(xf_g var1, Vec3f var2, Vec3f var3);

    public abstract float getMargin();

    public abstract void setMargin(float var1);

    public abstract int getNumPreferredPenetrationDirections();

    public abstract void getPreferredPenetrationDirection(int var1, Vec3f var2);
}
