package all;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.PrintStream;

/**
 * class pp Чтение настроек аддонов
 */
public interface aVf {
    Object bHx();

    void dispose();

    AddonManager getAddonManager();

    PrintStream bHw();

    /**
     * Получить ссылку на Загрузчик ресурсов аддона
     *
     * @return
     */
    tj bHv();

    void c(String var1, String... var2);

    Object U(Class var1);

    String getLineConfig(String var1, String var2);

    void e(String var1, String var2, String var3);

    void P(Object var1);

    void Q(Object var1);

    InputStream hE(String var1) throws FileNotFoundException;

    aPE a(long var1, aPE var3);

    void a(String var1, akT var2);

    /**
     * ссылка на Загруженный класс аддона
     */
    aMS bHy();

    aVW bHz();

    InputStream f(Object var1, String var2) throws FileNotFoundException;

    /**
     * Перевести сообщение
     *
     * @param var1 ключ значение сообщения
     * @return Пример Email sent. Check your inbox.
     */
    String translate(String var1);

    String translate(String var1, Object... var2);

    String V(String var1, String var2);

    String a(String var1, String var2, Object... var3);

    String a(Class var1, String var2, Object... var3);

    String a(Class var1, String var2, String var3, Object... var4);

    ajJ_q aVU();

    void restart();
}
