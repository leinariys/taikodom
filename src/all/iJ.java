package all;

import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * class LoginAddon
 */
public class iJ extends WA {

    private final ahM Zl;

    public iJ() {
        this((ahM) null);
    }

    public iJ(ahM var1) {
        this.Zl = var1;
    }

    public ahM BJ() {
        return this.Zl;
    }

    public void readExternal(ObjectInput var1) {
        throw new UnsupportedOperationException("Do not use this class serverside");
    }

    public void writeExternal(ObjectOutput var1) {
        throw new UnsupportedOperationException("Do not use this class serverside");
    }
}
