package all;

public class Xz extends Zf {
    private static final long serialVersionUID = 1L;
    public final fm_q eJi;
    public final Class agS;
    public final long agR;
    public final boolean eJj;
    public int eJk;

    public Xz(arL var1, fm_q var2) {
        this(var1, var2, false);
    }

    public Xz(arL var1, fm_q var2, boolean var3) {
        this.eJi = var2;
        this.eJk = var1.PM().bGU().HX();
        this.agS = var1.yn().getClass();
        this.agR = var1.hC().cqo();
        this.eNk = Thread.currentThread().getId();
        this.eJj = var3;
    }

    public String toString() {
        return this.timestamp + " T:" + this.eNk + " TT_q:" + this.eJk + " " + this.eJi.name() + " on " + this.agS + ":" + this.agR;
    }
}
