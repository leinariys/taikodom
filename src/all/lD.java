package all;

import org.w3c.dom.css.CSSRule;
import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.css.CSSStyleSheet;

/**
 * Обёртка CSS селектора
 */
public class lD {
    public CSSStyleSheet cssStyleSheet;
    public SelectorList selectorList;//Количество селекторов
    public CSSRule cssRule;
    /**
     * Перечисление всех свойств css селектора
     */
    public CSSStyleDeclaration cssStyleDeclaration;

    @Override
    public String toString() {
        return this.selectorList.toString();
    }
}
