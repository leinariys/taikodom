package all;

public class sT_q implements SelectorFactory {
    public IConditionalSelector createConditionalSelector(ISimpleSelector var1, Condition var2) {
        return new ConditionalSelector(var1, var2);
    }

    public ISimpleSelector createAnyNodeSelector() {
        throw new CSSException(CSSException.SAC_NOT_SUPPORTED_ERR);
    }

    public ISimpleSelector createRootNodeSelector() {
        throw new CSSException(CSSException.SAC_NOT_SUPPORTED_ERR);
    }

    public INegativeSelector createNegativeSelector(ISimpleSelector var1) {
        throw new CSSException(CSSException.SAC_NOT_SUPPORTED_ERR);
    }

    public IElementSelector createElementSelector(String var1, String var2) {
        if (var1 != null) {
            throw new CSSException(CSSException.SAC_NOT_SUPPORTED_ERR);
        } else {
            return new ElementSelector(var2);
        }
    }

    public ICharacterDataSelector createTextNodeSelector(String var1) {
        throw new CSSException(CSSException.SAC_NOT_SUPPORTED_ERR);
    }

    public ICharacterDataSelector createCDataSectionSelector(String var1) {
        throw new CSSException(CSSException.SAC_NOT_SUPPORTED_ERR);
    }

    public IProcessingInstructionSelector createProcessingInstructionSelector(String var1, String var2) {
        throw new CSSException(CSSException.SAC_NOT_SUPPORTED_ERR);
    }

    public ICharacterDataSelector createCommentSelector(String var1) {
        throw new CSSException(CSSException.SAC_NOT_SUPPORTED_ERR);
    }

    public IElementSelector o(String var1, String var2) {
        if (var1 != null) {
            throw new CSSException(CSSException.SAC_NOT_SUPPORTED_ERR);
        } else {
            return new ElementSelectorUri(var2);
        }
    }

    public IDescendantSelector createDescendantSelector(ISelector var1, ISimpleSelector var2) {
        return new DescendantSelector(var1, var2);
    }

    public IDescendantSelector createChildSelector(ISelector var1, ISimpleSelector var2) {
        return new DescendantSelectorChild(var1, var2);
    }

    public ISiblingSelector createDirectAdjacentSelector(short var1, ISelector var2, ISimpleSelector var3) {
        return new SiblingSelector(var1, var2, var3);
    }
}
