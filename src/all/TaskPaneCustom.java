package all;

import javax.swing.*;
import java.awt.*;

public class TaskPaneCustom extends BaseItemFactory {
    /**
     * Создание базового Component интерфейса
     * Вызывается из class BaseItem
     *
     * @param var1
     * @param var2
     * @param var3
     * @return
     */
    @Override
    protected JComponent CreatComponentCustom(MapKeyValue var1, Container var2, XmlNode var3) {
        return new TaskPaneCustomWrapper();
    }

    protected void a(MapKeyValue var1, TaskPaneCustomWrapper var2, XmlNode var3) {
        super.createLayout(var1, var2, var3);
        String var4 = var3.getAttribute("title");
        if (var4 != null) {
            var2.setTitle(var4);
        }

        if ("false".equals(var3.getAttribute("pinnable"))) {
            var2.jR(false);
        }

        if ("true".equals(var3.getAttribute("collapsed"))) {
            var2.collapse();
        } else if ("true".equals(var3.getAttribute("pinned")) && var2.LW()) {
            var2.aw(true);
        }

        if ("false".equals(var3.getAttribute("resizable"))) {
            var2.setResizable(false);
        }

    }
}
