package all;

import com.hoplon.geometry.Vec3f;

public abstract class aTrQ implements aTC_q {
    public final xf_g iOS = new xf_g();
    public final xf_g iOT = new xf_g();
    public final xf_g iOU = new xf_g();
    public azD iOR;
    public float r;

    public aTrQ(azD var1, xf_g var2, xf_g var3, xf_g var4) {
        this.iOR = var1;
        this.iOS.a(var2);
        this.iOT.a(var3);
        this.iOU.a(var4);
        this.r = 1.0F;
    }

    public void a(Vec3f[] var1, int var2, int var3) {
        Triangle var4 = new Triangle(var1[0], var1[1], var1[2]);
        ES var5 = new ES();
        aCf_q var6 = new aCf_q(this.iOR, var4, var5);
        acr.a var7 = new acr.a();
        var7.fkF = 1.0F;
        if (var6.a(this.iOS, this.iOT, this.iOU, this.iOU, var7) && var7.Oj.lengthSquared() > 1.0E-4F && var7.fkF < this.r) {
            this.iOS.bFF.transform(var7.Oj);
            var7.Oj.normalize();
            this.a(var7.Oj, var7.fkE, var7.fkF, var2, var3);
        }

    }

    public abstract float a(Vec3f var1, Vec3f var2, float var3, int var4, int var5);
}
