package all;

/**
 * class InterfaceSFXAddon
 */
public class dG {
    private final String file;
    private final String handle;

    public dG(aeO var1) {
        this.file = var1.getFile();
        this.handle = var1.getHandle();
    }

    public dG(String var1, String var2) {
        this.file = var1;
        this.handle = var2;
    }

    public String getFile() {
        return this.file;
    }

    public String getHandle() {
        return this.handle;
    }
}
