package all;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class azy extends kU implements Externalizable {
    private static final long serialVersionUID = 1L;
    private kU[] gZJ;

    public azy() {
    }

    public azy(kU[] var1) {
        this.gZJ = var1;
    }

    public kU[] cFV() {
        return this.gZJ;
    }

    public void readExternal(ObjectInput var1) throws IOException, ClassNotFoundException {
        super.readExternal(var1);
        int var2 = var1.readInt();
        kU[] var3 = this.gZJ = new kU[var2];

        for (int var4 = 0; var4 < var2; ++var4) {
            try {
                var3[var4] = (kU) aUu_q.iXh.b(var1);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

    }

    public void writeExternal(ObjectOutput var1) throws IOException {
        super.writeExternal(var1);
        kU[] var2 = this.gZJ;
        int var3 = var2.length;
        var1.writeInt(var3);

        for (int var4 = 0; var4 < var3; ++var4) {
            aUu_q.iXh.a((ObjectOutput) var1, (Object) var2[var4]);
        }

    }
}
