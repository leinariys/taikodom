package all;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

class aif implements um {
    public static byte idA = 11;
    public static byte idB = 12;
    public static byte idC = 13;
    public static byte idD = 14;
    public static byte idE = 21;
    public static byte idF = 22;
    public static byte idG = 23;
    public static byte idH = 24;
    private final se_q cVz;
    private final um idL;
    ReentrantLock idJ = new ReentrantLock(false);
    private Map idI = new HashMap();
    private axl idK;

    aif(se_q var1, um var2) {
        this.cVz = var1;
        this.idL = var2;
    }

    public Ue a(axl var1) {
        this.idK = var1;
        return new Ue() {
            public void bxm() {
                aif.this.cVz.PM().bGU().b(aif.this.cVz.hE(), (kU) aif.this.e((byte) 23));
            }

            public void bxk() {
                aif.this.cVz.PM().bGU().b(aif.this.cVz.hE(), (kU) aif.this.e((byte) 21));
            }

            public void bxl() {
                aif.this.cVz.PM().bGU().b(aif.this.cVz.hE(), (kU) aif.this.e((byte) 22));
            }

            public void bxn() {
                aif.this.cVz.PM().bGU().b(aif.this.cVz.hE(), (kU) aif.this.e((byte) 24));
            }
        };
    }

    private Bl_q e(byte var1) {
        return new Bl_q(this.cVz.yn(), var1);
    }

    public akO a(aDR var1, Bl_q var2) {
        switch (var2.ayq()) {
            case 11:
                this.idK.cbt();
                break;
            case 12:
                this.idK.cbw();
                break;
            case 13:
                this.idK.cbu();
                break;
            case 14:
                this.idK.cbv();
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            default:
                break;
            case 21:
                this.y(var1).bxk();
                break;
            case 22:
                this.y(var1).bxl();
                break;
            case 23:
                this.y(var1).bxm();
                break;
            case 24:
                this.y(var1).bxn();
        }

        return null;
    }

    private Ue y(final aDR var1) {
        this.idJ.lock();

        Ue var4;
        try {
            Ue var2 = (Ue) this.idI.get(var1);
            if (var2 == null) {
                var2 = this.idL.a(new axl() {
                    public void cbt() {
                        aif.this.cVz.PM().bGU().b(var1.bgt(), (kU) aif.this.e((byte) 11));
                    }

                    public void cbu() {
                        aif.this.cVz.PM().bGU().b(var1.bgt(), (kU) aif.this.e((byte) 13));
                    }

                    public void cbv() {
                        aif.this.cVz.PM().bGU().b(var1.bgt(), (kU) aif.this.e((byte) 14));
                    }

                    public void cbw() {
                        aif.this.cVz.PM().bGU().b(var1.bgt(), (kU) aif.this.e((byte) 12));
                    }
                });
                this.idI.put(var1, var2);
            }

            var4 = var2;
        } finally {
            this.idJ.unlock();
        }

        return var4;
    }
}
