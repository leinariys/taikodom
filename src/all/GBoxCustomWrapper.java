package all;

import javax.swing.plaf.ComponentUI;
import java.awt.*;

public class GBoxCustomWrapper extends BoxCustomWrapper {
    protected int cols = 1;

    protected Component aI(int var1, int var2) {
        int var3 = var2 * this.cols + var1;
        return var3 < this.getComponentCount() ? this.getComponent(var3) : null;
    }

    protected Dimension b(int var1, int var2) {
        int var3 = this.getComponentCount();
        int var4 = 0;
        int var5 = 0;
        int var6 = this.getRows();

        for (int var7 = 0; var7 < var6; ++var7) {
            int var8 = 0;
            int var9 = 0;

            for (int var10 = 0; var10 < this.cols; ++var10) {
                Component var11 = this.aI(var10, var7);
                if (var11 != null && var11.isVisible()) {
                    Dimension var12 = this.d(var11, var1 / var3, var2);
                    var8 += var12.width;
                    var9 = Math.max(var9, var12.height);
                }
            }

            var4 = Math.max(var4, var8);
            var5 += var9;
        }

        return new Dimension(var4, var5);
    }

    private int getRows() {
        int var1 = this.getComponentCount();
        return var1 / this.cols + (var1 % this.cols > 0 ? 1 : 0);
    }

    protected Dimension a(int var1, int var2) {
        int var3 = this.getComponentCount();
        int var4 = 0;
        int var5 = 0;
        int var6 = this.getRows();

        for (int var7 = 0; var7 < var6; ++var7) {
            int var8 = 0;
            int var9 = 0;

            for (int var10 = 0; var10 < this.cols; ++var10) {
                Component var11 = this.aI(var10, var7);
                if (var11 != null && var11.isVisible()) {
                    Dimension var12 = this.c(var11, var1 / var3, var2);
                    var8 += var12.width;
                    var9 = Math.max(var9, var12.height);
                }
            }

            var4 = Math.max(var4, var8);
            var5 += var9;
        }

        return new Dimension(var4, var5);
    }

    public void layout() {
        PropertiesUiFromCss var1 = PropertiesUiFromCss.a((ComponentUI) this.getUI());
        aRU var2 = var1.aty();
        vI var3 = var1.atR();
        Insets var4 = this.amu();
        Dimension var5 = this.getSize();
        Dimension var6 = new Dimension(var5.width - var4.right - var4.left, var5.height - var4.top - var4.bottom);
        int var7 = this.getRows();
        int var8 = this.getColumns();
        BoxCustomWrapper.a var9 = new BoxCustomWrapper.a(var6, var8, var7);
        Dimension var10 = new Dimension(0, 0);

        for (int var11 = 0; var11 < var7; ++var11) {
            for (int var12 = 0; var12 < var8; ++var12) {
                Component var13 = this.aI(var12, var11);
                if (var13 != null && var13.isVisible()) {
                    var9.a(var12, var11, var6, var13);
                } else {
                    var9.azY[var12][var11] = var10;
                }
            }
        }

        var9.Mq();
        float var26 = var9.azW.ghL - var9.azW.ghM;
        float var27 = var9.azW.ghL / (float) var8;

        for (int var28 = 0; var28 < var8 && var26 > 0.0F; ++var28) {
            if (var28 < var9.azW.ghH.length) {
                int var14 = var9.azW.ghH[var28];
                var9.azW.ghH[var28] = (int) Math.max((float) var9.azW.ghH[var28], var27);
                var26 -= (float) (var9.azW.ghH[var28] - var14);
            }
        }

        float var29 = var9.azX.ghL - var9.azX.ghM;
        float var30 = var9.azX.ghL / (float) var8;

        int var15;
        int var16;
        for (var15 = 0; var15 < var7 && var29 > 0.0F; ++var15) {
            if (var15 < var9.azX.ghH.length) {
                var16 = var9.azX.ghH[var15];
                var9.azX.ghH[var15] = (int) Math.max((float) var9.azX.ghH[var15], var30);
                var29 -= (float) (var9.azX.ghH[var15] - var16);
            }
        }

        var15 = var4.top;

        for (var16 = 0; var16 < var7; ++var16) {
            int var17 = var4.left;

            for (int var18 = 0; var18 < var8; ++var18) {
                Component var19 = this.aI(var18, var16);
                if (var19 != null) {
                    Dimension var20 = this.a(var9.azY[var18][var16], var9.azW.ghH[var18], var9.azX.ghH[var16]);
                    Point var21 = this.a(var19, var6.width, var6.height);
                    int var22 = var17 + var2.t(var9.azW.ghH[var18], var20.width);
                    int var23 = var15 + var3.t(var9.azX.ghH[var16], var20.height);
                    int var24 = var2 == aRU.iLi ? var9.azW.ghH[var18] : var20.width;
                    int var25 = var3 == vI.bzJ ? var9.azX.ghH[var16] : var20.height;
                    var19.setBounds(var22 + var21.x, var23 + var21.y, var24, var25);
                    var17 += var9.azW.ghH[var18];
                }
            }

            var15 += var9.azX.ghH[var16];
        }

    }

    private Dimension a(Dimension var1, int var2, int var3) {
        return new Dimension(Math.min(var1.width, var2), Math.min(var1.height, var3));
    }

    public int getColumns() {
        return this.cols;
    }

    public void setColumns(int var1) {
        this.cols = var1;
    }

    public String getElementName() {
        return "gbox";
    }
}
