package all;

import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class FBoxCustomWrapper extends BoxCustomWrapper {
    private int B;

    protected Dimension a(int var1, int var2) {
        int var3 = 0;
        int var4 = 0;
        int var5 = 0;
        int var6 = 0;
        int var7 = 0;
        Insets var8 = this.amu();
        Dimension var9 = this.getSize();
        Dimension var10 = new Dimension(var9.width - var8.right - var8.left, var9.height - var8.top - var8.bottom);
        int var11 = this.getComponentCount();

        for (int var12 = 0; var12 < var11; ++var12) {
            Component var13 = this.getComponent(var12);
            if (var13.isVisible()) {
                Dimension var14 = this.c(var13, var1, var2);
                var3 = Math.max(var3, var14.width);
                if (var4 + var14.width > var10.width) {
                    var5 = Math.max(var5, var4);
                    var6 += var7 + this.B;
                    var4 = 0;
                    var7 = 0;
                }

                var4 += var14.width + this.B;
                var7 = Math.max(var14.height, var7);
            }
        }

        Math.max(var5, var4);
        var6 += var7;
        return new Dimension(var3, var6);
    }

    protected Dimension b(int var1, int var2) {
        int var3 = 0;
        int var4 = 0;
        int var5 = 0;
        int var6 = 0;
        int var7 = this.getComponentCount();

        for (int var8 = 0; var8 < var7; ++var8) {
            Component var9 = this.getComponent(var8);
            if (var9.isVisible()) {
                Dimension var10 = this.d(var9, var1, var2);
                if (var3 + var10.width > var1) {
                    var4 = Math.max(var4, var3);
                    var5 += var6 + this.B;
                    var3 = 0;
                    var6 = 0;
                }

                var3 += var10.width + this.B;
                var6 = Math.max(var10.height, var6);
            }
        }

        var4 = Math.max(var4, var3);
        var5 += var6;
        return new Dimension(var4, var5);
    }

    public void layout() {
        Insets var1 = this.amu();
        Dimension var2 = this.getSize();
        Dimension var3 = new Dimension(var2.width - var1.right - var1.left, var2.height - var1.top - var1.bottom);
        ArrayList var4 = new ArrayList();
        ArrayList var5 = new ArrayList();
        ArrayList var6 = new ArrayList();
        int var7 = 0;
        int var8 = var3.width;
        int var9 = 0;
        int var10 = 0;
        int var11 = this.getComponentCount();

        int var12;
        for (var12 = 0; var12 < var11; ++var12) {
            Component var13 = this.getComponent(var12);
            if (var13.isVisible()) {
                Dimension var14 = this.d(var13, var3.width, var3.height);
                if (var7 + var14.width > var8) {
                    var6.add(var9);
                    var10 = Math.max(var10, var5.size());
                    var4.add(var5);
                    var5 = new ArrayList();
                    var7 = 0;
                    var9 = 0;
                }

                var7 += var14.width + this.B;
                var9 = Math.max(var9, var14.height);
                var5.add(var13);
            }
        }

        if (var5.size() > 0) {
            var4.add(var5);
            var6.add(var9);
        }

        var12 = 0;

        for (Iterator var19 = var4.iterator(); var19.hasNext(); var12 += ((Integer) var6.remove(0)).intValue() + this.B) {
            List var18 = (List) var19.next();
            var7 = 0;

            Dimension var17;
            for (Iterator var16 = var18.iterator(); var16.hasNext(); var7 += var17.width + this.B) {
                Component var15 = (Component) var16.next();
                var17 = this.d(var15, var3.width, var3.height);
                var15.setBounds(var7 + var1.left, var12 + var1.top, var17.width, var17.height);
            }
        }

    }

    public int getGap() {
        return this.B;
    }

    public void setGap(int var1) {
        this.B = var1;
        this.layout();
    }

    public String getElementName() {
        return "fbox";
    }
}
