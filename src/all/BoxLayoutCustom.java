package all;

import gnu.trove.TObjectIntHashMap;
import org.w3c.dom.css.CSSStyleDeclaration;

import javax.swing.*;
import java.awt.*;
import java.io.StringReader;

public class BoxLayoutCustom extends BaseLayoutCssValue {
    private static final TObjectIntHashMap location = new TObjectIntHashMap();
    private static LogPrinter logger = LogPrinter.K(BoxLayoutCustom.class);

    static {
        location.put("X_AXIS", 0);
        location.put("Y_AXIS", 1);
        location.put("LINE_AXIS", 2);
        location.put("PAGE_AXIS", 3);
    }

    public LayoutManager createLayout(Container var1, String var2) {
        int var3 = 0;
        if (var2 != null && !(var2 = var2.trim()).isEmpty()) {
            try {
                CSSStyleDeclaration var4 = ParserCss.getParserCss().getCSSStyleDeclaration(new InputSource(new StringReader(var2)));
                var3 = this.getValueCss(var4, location, "axis", var3);
            } catch (Exception var5) {
                logger.warn(var5);
            }
        }

        return new BoxLayout(var1, var3);
    }
}
