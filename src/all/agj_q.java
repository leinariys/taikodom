package all;

public class agj_q {
    private long fxB;
    private long fxC;
    private long fxD;
    private long fxE;
    private long fxF;
    private long fxG = 0L;

    public agj_q(long var1) {
        this.fxB = var1;
    }

    public float hf(long var1) {
        long var3 = System.currentTimeMillis();
        if (this.fxD == 0L) {
            this.fxD = var3;
            this.fxC = var1;
            this.fxF = var3;
            this.fxE = var1;
            return (float) this.fxG;
        } else {
            if (var3 >= this.fxF + this.fxB) {
                this.fxD = this.fxF;
                this.fxC = this.fxE;
                this.fxF = var3;
                this.fxE = var1;
            }

            if (var3 >= this.fxD + this.fxB) {
                this.fxG = (long) ((float) (var1 - this.fxC) * 1000.0F / (float) (var3 - this.fxD));
            }

            return (float) this.fxG;
        }
    }

    public float bWB() {
        return (float) this.fxG;
    }
}
