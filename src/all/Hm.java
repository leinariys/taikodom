package all;

import org.junit.Assert;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.*;

/**
 *
 */
public class Hm {
    /**
     *
     */
    public static Charset utf8 = Charset.forName("UTF-8");

    /**
     *
     */
    private static String SEPARATOR = System.getProperty("line.separator");

    /**
     * @param var0
     * @param var1
     * @return
     * @throws FileNotFoundException
     */
    public static List a(String var0, FilenameFilter var1) throws FileNotFoundException {
        File var2 = new File(var0);
        return b(var2, var1);
    }

    /**
     * @param var0
     * @param var1
     * @return
     */
    public static File[] a(File var0, final String var1) {
        return var0.listFiles(new FilenameFilter() {
            public boolean accept(File var1x, String var2) {
                return var2.startsWith(var1);
            }
        });
    }

    /**
     * @param var0
     * @param var1
     * @return
     * @throws FileNotFoundException
     */
    public static List b(File var0, FilenameFilter var1) throws FileNotFoundException {
        if (!var0.exists()) {
            throw new FileNotFoundException("The file or directory " + var0.getAbsolutePath() + " was not found");
        } else {
            return c(var0, var1);
        }
    }

    /**
     * @param var0
     * @param var1
     * @return
     */
    private static List c(File var0, FilenameFilter var1) {
        Object var2;
        if (var0.isFile()) {
            if (var1.accept(var0, var0.getName())) {
                var2 = Collections.singletonList(var0);
            } else {
                var2 = Collections.emptyList();
            }
        } else if (var0.getName().equals("CVS")) {
            var2 = Collections.emptyList();
        } else {
            var2 = new ArrayList();
            File[] var3 = var0.listFiles();
            File[] var7 = var3;
            int var6 = var3.length;

            for (int var5 = 0; var5 < var6; ++var5) {
                File var4 = var7[var5];
                ((List) var2).addAll(c(var4, var1));
            }
        }

        return (List) var2;
    }

    /**
     * @param var0
     * @param var1
     * @param var2
     * @return
     * @throws IOException
     */
    public static boolean a(File var0, String var1, Charset var2) throws IOException {
        if (!var0.exists()) {
            return true;
        } else {
            InputStreamReader var3 = new InputStreamReader(new FileInputStream(var0), var2);
            StringBuffer var4 = new StringBuffer();
            char[] var5 = new char[512];

            int var6;
            while ((var6 = var3.read(var5)) > 0) {
                var4.append(var5, 0, var6);
            }

            var3.close();
            return !var1.equals(var4.toString());
        }
    }

    /**
     * @param var0
     * @param var1
     * @param var2
     * @return
     */
    public static File t(String var0, String var1, String var2) {
        String var3 = mY(var1);
        File var4 = new File(var0, var3);
        if (!var4.isDirectory()) {
            var4.mkdirs();
        }

        File var5 = new File(var4.getAbsoluteFile(), var2 + ".java");
        return var5;
    }

    /**
     * @param var0
     * @return
     */
    public static String mY(String var0) {
        StringTokenizer var1 = new StringTokenizer(var0, ".");
        StringBuffer var2 = new StringBuffer();

        while (var1.hasMoreElements()) {
            String var3 = var1.nextToken();
            var2.append(var3);
            if (var1.hasMoreElements()) {
                var2.append(File.separatorChar);
            }
        }

        return var2.toString();
    }

    /**
     * @param var0
     * @param var1
     * @return
     * @throws IOException
     */
    public static String a(File var0, Charset var1) throws IOException {
        if (!var0.exists()) {
            return null;
        } else {
            InputStreamReader var2 = new InputStreamReader(new FileInputStream(var0), var1);
            StringBuffer var3 = new StringBuffer();
            char[] var4 = new char[512];

            int var5;
            while ((var5 = var2.read(var4)) > 0) {
                var3.append(var4, 0, var5);
            }

            var2.close();
            return var3.toString();
        }
    }

    /**
     * @param var0
     * @param var1
     * @return
     * @throws IOException
     */
    public static String a(InputStream var0, Charset var1) throws IOException {
        InputStreamReader var2 = new InputStreamReader(var0, var1);
        StringBuffer var3 = new StringBuffer();
        char[] var4 = new char[512];

        int var5;
        while ((var5 = var2.read(var4)) > 0) {
            var3.append(var4, 0, var5);
        }

        var2.close();
        return var3.toString();
    }

    /**
     * @param var0
     * @param var1
     * @throws IOException
     */
    public static void b(File var0, String var1) throws IOException {
        a((File) var0, (Charset) null, (String) var1);
    }

    public static void a(File var0, Charset var1, String var2) throws IOException {
        OutputStreamWriter var3;
        if (var1 != null) {
            var3 = new OutputStreamWriter(new FileOutputStream(var0), var1);
        } else {
            var3 = new OutputStreamWriter(new FileOutputStream(var0));
        }

        var3.write(var2);
        var3.flush();
        var3.close();
    }

    /**
     * @param var0
     * @param var1
     * @param var2
     * @return
     * @throws IOException
     */
    public static boolean a(File var0, File var1, boolean var2) throws IOException {
        boolean var3 = true;
        if (var0.isDirectory()) {
            var1.mkdirs();
            File[] var4 = var0.listFiles();
            if (var4 != null) {
                File[] var8 = var4;
                int var7 = var4.length;

                for (int var6 = 0; var6 < var7; ++var6) {
                    File var5 = var8[var6];
                    var3 &= a(var5, new File(var1, var5.getName()), var2);
                }
            }
        } else {
            try {
                copyFile(var0, var1, true);
            } catch (IOException var9) {
                if (!var2) {
                    throw var9;
                }

                var3 = false;
            }
        }

        return var3;
    }

    /**
     * @param var0
     * @param var1
     * @throws IOException
     */
    public static void copyFile(File var0, File var1) throws IOException {
        copyFile(var0, var1, false);
    }

    /**
     * @param var0
     * @param var1
     * @param var2
     * @throws IOException
     */
    public static void copyFile(File var0, File var1, boolean var2) throws IOException {
        if (var1.getParent() != null && var2) {
            var1.getParentFile().mkdirs();
        }

        FileInputStream var3 = new FileInputStream(var0);
        FileOutputStream var4 = null;

        try {
            var4 = new FileOutputStream(var1);
            byte[] var5 = new byte[4096];

            while (true) {
                int var6 = var3.read(var5);
                if (var6 == -1) {
                    return;
                }

                var4.write(var5, 0, var6);
            }
        } finally {
            try {
                var3.close();
            } finally {
                if (var4 != null) {
                    var4.close();
                }

            }
        }
    }

    /**
     * Копирование данных из одного потока в другой
     *
     * @param var0
     * @param var1
     * @throws IOException
     */
    public static void copyStream(InputStream var0, OutputStream var1) throws IOException {
        byte[] var2 = new byte[4096];

        while (true) {
            int var3 = var0.read(var2);
            if (var3 < 0) {
                return;
            }

            var1.write(var2, 0, var3);
        }
    }

    /**
     * @param var0
     * @param var1
     * @return
     */
    public static boolean a(File var0, boolean var1) {
        boolean var2 = true;
        if (var0.isDirectory() && var1) {
            File[] var3 = var0.listFiles();
            if (var3 != null) {
                File[] var7 = var3;
                int var6 = var3.length;

                for (int var5 = 0; var5 < var6; ++var5) {
                    File var4 = var7[var5];
                    var2 &= a(var4, true);
                }
            }
        }

        return var2 && var0.delete();
    }

    /**
     * @param var0
     * @param var1
     * @param var2
     * @throws IOException
     */
    public static void a(Properties var0, File var1, String var2) throws IOException {
        Properties var3 = new Properties() {
            public synchronized Enumeration keys() {
                TreeSet var1 = new TreeSet();
                var1.addAll((Collection) super.keySet());
                final Iterator var2 = var1.iterator();
                return new Enumeration() {
                    public boolean hasMoreElements() {
                        return var2.hasNext();
                    }

                    public Object nextElement() {
                        return var2.next();
                    }
                };
            }
        };
        var3.putAll(var0);
        ByteArrayOutputStream var4 = new ByteArrayOutputStream();
        var3.store(var4, var2);
        String var5 = "8859_1";
        String var6 = var4.toString(var5);
        int var7 = 0;
        if (var2 != null) {
            var7 = var6.indexOf(SEPARATOR);
        }

        var7 = var6.indexOf(SEPARATOR, var7 + 1);
        var6 = var6.substring(var7 + SEPARATOR.length());
        if (a(var1, var6, Charset.forName(var5))) {
            FileOutputStream var8 = new FileOutputStream(var1);
            var8.write(var6.getBytes(var5));
            var8.close();
        }
    }

    /**
     * @param var0
     * @param var1
     * @return
     * @throws IOException
     */
    public static boolean aU(String var0, String var1) throws IOException {
        return d(new File(var0), new File(var1));
    }

    /**
     * @param var0
     * @param var1
     * @return
     * @throws IOException
     */
    public static boolean d(File var0, File var1) throws IOException {

        String var2 = var0.getCanonicalPath() + File.separator;
        return var1.getCanonicalPath().startsWith(var2);

    }

    /**
     * @param var0
     * @param var1
     */
    public static void aV(String var0, String var1) {
        byte[] var2 = getByte(new File(var0));
        byte[] var3 = getByte(new File(var1));
        Assert.assertTrue(Arrays.equals(var2, var3));
    }

    /**
     * @param var0
     * @return
     */
    public static ByteBuffer getByteBuffer(File var0) {
        int var1 = (int) var0.length();
        ByteBuffer var2 = ByteBuffer.allocateDirect(var1).order(ByteOrder.nativeOrder());

        try {
            FileChannel var3 = (new FileInputStream(var0)).getChannel();

            while (var1 > 0) {
                try {
                    int var4 = var3.read(var2);
                    if (var4 < 0) {
                        throw new EOFException();
                    }

                    var1 -= var4;
                } catch (IOException var6) {
                    var6.printStackTrace();
                }
            }
        } catch (FileNotFoundException var7) {
            var7.printStackTrace();
        }

        return var2;
    }

    /**
     * Получить массив byte
     *
     * @param var0
     * @return
     */
    public static byte[] getByte(File var0) {
        try {
            RandomAccessFile var1 = new RandomAccessFile(var0, "r");

            byte[] var2;
            try {
                var1.seek(0L);
                var2 = new byte[(int) var1.length()];
                var1.readFully(var2);
            } finally {
                var1.close();
            }

            return var2;
        } catch (FileNotFoundException var8) {
            var8.printStackTrace();
        } catch (IOException var9) {
            var9.printStackTrace();
        }

        return null;
    }

    /**
     * @param var0
     * @return
     * @throws IOException
     */
    public static byte[] e(InputStream var0) throws IOException {
        ByteArrayOutputStream var1 = new ByteArrayOutputStream();
        copyStream(var0, var1);
        return var1.toByteArray();
    }

    /**
     * @param var0
     * @param var1
     */
    public static void readFully(InputStream var0, byte[] var1) {
        try {
            int var2;
            for (int var3 = 0; (var2 = var0.read(var1, var3, var1.length - var3)) > 0; var3 += var2) {
                ;
            }

        } catch (IOException var4) {
            throw new RuntimeException(var4);
        }
    }

    /**
     * Чтение файлов с диска
     *
     * @param var0 Поток для чтения
     * @param var1 кодировка
     * @return
     */
    public static String c(InputStream var0, String var1) {
        try {
            ByteArrayOutputStream var2 = new ByteArrayOutputStream();
            byte[] var3 = new byte[4096];

            int var4;
            while ((var4 = var0.read(var3)) > 0) {
                var2.write(var3, 0, var4);
            }

            return new String(var2.toByteArray(), var1);
        } catch (IOException var5) {
            var5.printStackTrace();
            return null;
        }
    }

    /**
     * Записать массив byte
     *
     * @param var0 Путь к файлу
     * @param var1 массив byte
     */
    public static void setByte(File var0, byte[] var1) {
        try {
            FileOutputStream var2 = new FileOutputStream(var0);
            var2.write(var1);
            var2.flush();
            var2.close();
        } catch (IOException var3) {
            throw new RuntimeException(var3);
        }
    }
}
