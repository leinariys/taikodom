package all;

import com.hoplon.geometry.Vec3f;

public class Triangle extends aVx {
    public final Vec3f[] eeQ = new Vec3f[]{new Vec3f(), new Vec3f(), new Vec3f()};

    public Triangle() {
    }

    public Triangle(Vec3f var1, Vec3f var2, Vec3f var3) {
        this.eeQ[0].set(var1);
        this.eeQ[1].set(var2);
        this.eeQ[2].set(var3);
    }

    public void k(Vec3f var1, Vec3f var2, Vec3f var3) {
        this.eeQ[0].set(var1);
        this.eeQ[1].set(var2);
        this.eeQ[2].set(var3);
    }

    public int getNumVertices() {
        return 3;
    }

    public Vec3f nj(int var1) {
        return this.eeQ[var1];
    }

    public void b(int var1, Vec3f var2) {
        var2.set(this.eeQ[var1]);
    }

    public tK arV() {
        return tK.bsB;
    }

    public int abC() {
        return 3;
    }

    public void a(int var1, Vec3f var2, Vec3f var3) {
        this.b(var1, var2);
        this.b((var1 + 1) % 3, var3);
    }

    public void getAabb(xf_g var1, Vec3f var2, Vec3f var3) {
        this.getAabbSlow(var1, var2, var3);
    }

    public Vec3f localGetSupportingVertexWithoutMargin(Vec3f var1) {
        this.stack.bcH().push();

        Vec3f var4;
        try {
            Vec3f var2 = this.stack.bcH().h(var1.dot(this.eeQ[0]), var1.dot(this.eeQ[1]), var1.dot(this.eeQ[2]));
            var4 = this.eeQ[JL.W(var2)];
        } finally {
            this.stack.bcH().pop();
        }

        return var4;
    }

    public void batchedUnitVectorGetSupportingVertexWithoutMargin(Vec3f[] var1, Vec3f[] var2, int var3) {
        this.stack.bcH().push();

        try {
            Vec3f var4 = (Vec3f) this.stack.bcH().get();

            for (int var5 = 0; var5 < var3; ++var5) {
                Vec3f var6 = var1[var5];
                var4.set(var6.dot(this.eeQ[0]), var6.dot(this.eeQ[1]), var6.dot(this.eeQ[2]));
                var2[var5].set(this.eeQ[JL.W(var4)]);
            }
        } finally {
            this.stack.bcH().pop();
        }

    }

    public void a(Vec3f var1, Vec3f var2, int var3) {
        this.b(var3, var1, var2);
    }

    public int abB() {
        return 1;
    }

    public void af(Vec3f var1) {
        this.stack.bcH().push();

        try {
            Vec3f var2 = (Vec3f) this.stack.bcH().get();
            Vec3f var3 = (Vec3f) this.stack.bcH().get();
            var2.sub(this.eeQ[1], this.eeQ[0]);
            var3.sub(this.eeQ[2], this.eeQ[0]);
            var1.cross(var2, var3);
            var1.normalize();
        } finally {
            this.stack.bcH().pop();
        }

    }

    public void b(int var1, Vec3f var2, Vec3f var3) {
        this.af(var2);
        var3.set(this.eeQ[0]);
    }

    public void calculateLocalInertia(float var1, Vec3f var2) {
        assert false;

        var2.set(0.0F, 0.0F, 0.0F);
    }

    public boolean b(Vec3f var1, float var2) {
        this.stack.bcH().push();

        try {
            Vec3f var3 = (Vec3f) this.stack.bcH().get();
            this.af(var3);
            float var4 = var1.dot(var3);
            float var5 = this.eeQ[0].dot(var3);
            var4 -= var5;
            if (var4 >= -var2 && var4 <= var2) {
                int var6 = 0;

                while (true) {
                    if (var6 >= 3) {
                        return true;
                    }

                    Vec3f var7 = (Vec3f) this.stack.bcH().get();
                    Vec3f var8 = (Vec3f) this.stack.bcH().get();
                    this.a(var6, var7, var8);
                    Vec3f var9 = (Vec3f) this.stack.bcH().get();
                    var9.sub(var8, var7);
                    Vec3f var10 = (Vec3f) this.stack.bcH().get();
                    var10.cross(var9, var3);
                    var10.normalize();
                    var4 = var1.dot(var10);
                    float var11 = var7.dot(var10);
                    var4 -= var11;
                    if (var4 < -var2) {
                        break;
                    }

                    ++var6;
                }
            }
        } finally {
            this.stack.bcH().pop();
        }

        return false;
    }

    public String getName() {
        return "Triangle";
    }

    public int getNumPreferredPenetrationDirections() {
        return 2;
    }

    public void getPreferredPenetrationDirection(int var1, Vec3f var2) {
        this.af(var2);
        if (var1 != 0) {
            var2.scale(-1.0F);
        }

    }
}
