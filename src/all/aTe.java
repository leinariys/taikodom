package all;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class aTe extends kU implements Externalizable {

    private String str;
    private Object iOo;

    public aTe() {
    }

    public aTe(String var1, Object var2) {
        this.str = var1;
        this.iOo = var2;
    }

    public void writeExternal(ObjectOutput var1) throws IOException {
        super.writeExternal(var1);
        var1.writeUTF(this.str != null ? this.str : "");
        var1.writeObject(this.iOo);
    }

    public void readExternal(ObjectInput var1) throws IOException, ClassNotFoundException {
        super.readExternal(var1);
        this.str = var1.readUTF();
        this.iOo = var1.readObject();
    }

    public Object dvz() {
        return this.iOo;
    }

    public String getStr() {
        return this.str;
    }
}
