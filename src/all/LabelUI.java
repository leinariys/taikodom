package all;

import sun.swing.SwingUtilities2;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicLabelUI;
import java.awt.*;

/**
 * LabelUI
 */
public class LabelUI extends BasicLabelUI implements aIh {
    private ComponentManager Rp;
    private int gtR = -1;

    public LabelUI(JComponent var1) {
        this.Rp = (ComponentManager) ComponentManager.getCssHolder(var1);
        if (this.Rp == null) {
            this.Rp = new ComponentManager("label", var1);
        }
        var1.setBorder(BorderWrapper.getInstance());
    }

    public static ComponentUI createUI(JComponent var0) {
        return new LabelUI(var0);
    }

    public ComponentManager wy() {
        return this.Rp;
    }

    public Dimension getPreferredSize(JComponent var1) {
        RM.a((aIh) this, (JLabel) ((JLabel) var1));
        return this.Rp.c(var1, super.getPreferredSize(var1));
    }

    public Dimension getMinimumSize(JComponent var1) {
        RM.a((aIh) this, (JLabel) ((JLabel) var1));
        return this.wy().b(var1, super.getMinimumSize(var1));
    }

    public Dimension getMaximumSize(JComponent var1) {
        RM.a((aIh) this, (JLabel) ((JLabel) var1));
        return this.wy().a(var1, super.getMaximumSize(var1));
    }

    public void update(Graphics var1, JComponent var2) {
        RM.a((aIh) this, (Graphics) var1, (JLabel) ((JLabel) var2));
        this.paint(var1, var2);
    }

    protected void paintDisabledText(JLabel var1, Graphics var2, String var3, int var4, int var5) {
        super.paintEnabledText(var1, var2, var3, var4, var5);
    }

    protected void paintEnabledText(JLabel var1, Graphics var2, String var3, int var4, int var5) {
        if (this.gtR < 0) {
            super.paintEnabledText(var1, var2, var3, var4, var5);
        } else {
            int var6 = var1.getDisplayedMnemonicIndex();
            var2.setColor(var1.getForeground());
            SwingUtilities2.drawStringUnderlineCharAt(var1, var2, var3.substring(0, Math.min(this.gtR, var3.length())), var6, var4, var5);
        }
    }

    public int ctz() {
        return this.gtR;
    }

    public void uk(int var1) {
        this.gtR = var1;
    }

    /**
     * TODO похлже это public pv wy()
     *
     * @return Графический элемент
     */
    @Override
    public aeK wz() {
        return this.Rp;
    }
}
