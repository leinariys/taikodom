package all;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class Ek implements KeyListener, MouseListener {
    private static final BaseUItegXML aPy = BaseUItegXML.thisClass;
    private final ComponentManager Rp;

    public Ek(ComponentManager var1) {
        this.Rp = var1;
    }

    private boolean a(KeyEvent var1) {
        switch (var1.getKeyCode()) {
            case 16:
            case 17:
            case 18:
            case 524:
            case 525:
                return true;
            default:
                return false;
        }
    }

    public void keyPressed(KeyEvent var1) {
        if (!this.a(var1)) {
            aPy.cx(this.Rp.Vr().atZ());
        }

    }

    public void keyReleased(KeyEvent var1) {
        if (!this.a(var1)) {
            aPy.cx(this.Rp.Vr().atY());
        }

    }

    public void keyTyped(KeyEvent var1) {
        if (!this.a(var1)) {
            aPy.cx(this.Rp.Vr().aua());
        }

    }

    public void mouseClicked(MouseEvent var1) {
        aPy.cx(this.Rp.Vr().atX());
    }

    public void mouseEntered(MouseEvent var1) {
        aPy.e(this.Rp.Vr().atU(), false);
    }

    public void mouseExited(MouseEvent var1) {
    }

    public void mousePressed(MouseEvent var1) {
        aPy.cx(this.Rp.Vr().atV());
    }

    public void mouseReleased(MouseEvent var1) {
        aPy.cx(this.Rp.Vr().atW());
    }
}
