package all;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.Arrays;

public final class aCE implements Gr, Externalizable {

    private transient Xf_q hwA;
    private transient fm_q aVR;
    private transient Object[] args;
    private transient Object[] hwB;

    public aCE() {
    }

    public aCE(Xf_q var1, fm_q var2) {
        this.hwA = var1;
        this.aVR = var2;
        this.args = null;
    }

    public aCE(Xf_q var1, fm_q var2, Object[] var3) {
        this.hwA = var1;
        this.aVR = var2;
        this.args = var3;
    }

    public Object[] getArgs() {
        return this.args;
    }

    public fm_q aQK() {
        return this.aVR;
    }

    public Xf_q aQL() {
        return this.hwA;
    }

    public String toString() {
        return "MethodInvoker[name=" + this.aVR.name() + ", args=" + Arrays.toString(this.getArgs()) + "]";
    }

    public void writeExternal(ObjectOutput var1) throws IOException {
        GZ.dah.a((ObjectOutput) var1, (Object) this.hwA);
        Kd.a(var1, this.hwA.U().length, this.aVR.hq());
        Kd[] var2 = this.aVR.pM();
        Object[] var3 = this.hwB == null ? this.args : this.hwB;

        for (int var4 = 0; var4 < var2.length; ++var4) {
            Object var5 = var3[var4];
            var2[var4].a(var1, var5);
        }

    }

    public void readExternal(ObjectInput var1) throws IOException, ClassNotFoundException {
        this.hwA = (Xf_q) GZ.dah.b(var1);
        int var2 = Kd.b(var1, this.hwA.U().length);
        this.aVR = this.hwA.U()[var2];
        Kd[] var3 = this.aVR.pM();
        Object[] var4 = this.args = new Object[var3.length];

        for (int var5 = 0; var5 < var3.length; ++var5) {
            try {
                Object var6 = var3[var5].b(var1);
                var4[var5] = var6;
            } catch (RuntimeException var7) {
                throw new RuntimeException("Error reading parameters of " + this.hwA.getClass() + ":" + this.hwA.bFf().hC() + " " + this.aVR.hD() + "." + this.aVR.name() + ":" + this.aVR.hq() + " argument " + var5 + " : " + this.aVR.getParameterTypes()[var5], var7);
            } catch (IOException var8) {
                throw new RuntimeException("Error reading parameters of " + this.hwA.getClass() + ":" + this.hwA.bFf().hC() + " " + this.aVR.hD() + "." + this.aVR.name() + ":" + this.aVR.hq() + " argument " + var5 + " : " + this.aVR.getParameterTypes()[var5], var8);
            }
        }

    }

    public Object[] aQM() {
        if (this.hwB == null && this.args != null) {
            this.hwB = new Object[this.args.length];
            System.arraycopy(this.args, 0, this.hwB, 0, this.args.length);
        }

        return this.hwB;
    }

    public void l(Object[] var1) {
        this.hwB = var1;
    }

    public void m(Xf_q var1) {
        this.hwA = var1;
    }

    public int hq() {
        return this.aVR.hq();
    }
}
