package all;

import java.util.ListResourceBundle;

public class zU_q extends ListResourceBundle {
    static final Object[][] contents = new Object[][]{{"s0", "Syntax error"}, {"s1", "Array out of bounds error"}, {"s2", "This style sheet is read only"}, {"s3", "The text does not represent an unknown rule"}, {"s4", "The text does not represent all style rule"}, {"s5", "The text does not represent all charset rule"}, {"s6", "The text does not represent an import rule"}, {"s7", "The text does not represent all media rule"}, {"s8", "The text does not represent all font face rule"}, {"s9", "The text does not represent all page rule"}, {"s10", "This isn't all Float type"}, {"s11", "This isn't all String type"}, {"s12", "This isn't all Counter type"}, {"s13", "This isn't all Rect type"}, {"s14", "This isn't an RGBColor type"}, {"s15", "CreatAllComponentCustom charset rule must be the first rule"}, {"s16", "CreatAllComponentCustom charset rule already exists"}, {"s17", "An import rule must preceed all other rules"}, {"s18", "The specified type was not found"}};

    public Object[][] getContents() {
        return contents;
    }
}
