package all;

public class Hr implements ahw_q {
    private long offset;
    private boolean stopped;
    private ahw_q cVg;
    private double dbw;

    public Hr(ahw_q var1) {
        this.cVg = var1;
    }

    public Hr() {
        // this(atM.cVg);
    }

    public long currentTimeMillis() {
        if (!this.stopped) {
            return this.dbw != 0.0D && this.dbw != 1.0D ? (long) (this.dbw * (double) this.cVg.currentTimeMillis()) + this.offset : this.cVg.currentTimeMillis() + this.offset;
        } else {
            return this.offset;
        }
    }

    public long getOffset() {
        return this.offset;
    }

    public void setOffset(long var1) {
        this.offset = var1;
    }

    public void eO(long var1) {
        this.offset += var1;
    }

    public void eP(long var1) {
        this.offset = var1 - this.currentTimeMillis();
    }

    public boolean isStopped() {
        return this.stopped;
    }

    public void cN(boolean var1) {
        this.stopped = var1;
    }

    public float getFactor() {
        return (float) this.dbw;
    }

    public void setFactor(float var1) {
        this.dbw = (double) var1;
    }

    public void z(double var1) {
        this.dbw = var1;
    }
}
