package all;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Диагностика работы потоков, статистика использования
 */
public class WatchDog {
    private static ThreadLocal hBd = new ThreadLocal() {
        @Override
        protected Map initialValue() {
            return new HashMap();
        }
    };
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS ");
    private Thread mainWatchDog;
    private Thread dumperWatchDog;
    private Map hBb = new WeakHashMap();
    private long hBc = 0L;
    /**
     * millis - время сна потока
     */
    private long fKU = 100L;
    /**
     * Интервал логирования
     */
    private double fKV = 6.0E10D;
    private PrintWriter eqC;
    /**
     * Запись в файл
     */
    private hi fKW;

    public WatchDog() {
        this.setPrintWriter(new PrintWriter(System.out, true));
    }

    public static void main(String[] var0) throws InterruptedException {
        WatchDog var1 = new WatchDog();
        var1.start();
        while (true) {
            xA(4);
            var1.f(Thread.currentThread());
            var1.reset();
        }
    }

    private static void xA(int var0) throws InterruptedException {
        Thread.sleep(100L);
        if (var0 > 0) {
            xA(var0 - 1);
            xB(var0 - 1);
        }
    }

    private static void xB(int var0) throws InterruptedException {
        xA(var0);
    }

    public static void c(Class var0, String var1, Object... var2) {
        ((Map) hBd.get()).put(var1, var2);
    }

    public static void l(Class var0, String var1) {
        ((Map) hBd.get()).remove(var1);
    }

    /**
     * Запуск потока ститистики
     */
    public void start() {
        if (this.mainWatchDog == null) {
            this.mainWatchDog = new Thread("Watch dog Thread") {
                public void run() {
                    while (true) {
                        WatchDog.this.doRun();
                    }
                }
            };
            this.dumperWatchDog = new WatchDog.a("Watchdog dumper thread");
            this.dumperWatchDog.setDaemon(true);
            this.mainWatchDog.setDaemon(true);
            this.mainWatchDog.start();
            this.dumperWatchDog.start();
        }
    }

    public void stop() {
        if (this.mainWatchDog != null) {
            this.mainWatchDog.interrupt();
            this.mainWatchDog = null;
        }

    }

    public boolean isRunning() {
        return this.mainWatchDog != null;
    }

    private void doRun() {
        try {
            Thread.sleep(this.fKU);
        } catch (InterruptedException var1) {
            return;
        }

        this.caL();
    }

    public synchronized void caL() {
        ++this.hBc;
        Map var1 = Thread.getAllStackTraces();

        Map.Entry var2;
        CK var4;
        for (Iterator var3 = var1.entrySet().iterator(); var3.hasNext(); var4.a((StackTraceElement[]) var2.getValue())) {
            var2 = (Map.Entry) var3.next();
            var4 = (CK) this.hBb.get(var2.getKey());
            if (var4 == null) {
                this.hBb.put((Thread) var2.getKey(), var4 = new CK((Thread) var2.getKey()));
            }
        }

    }

    /**
     * Вывод статистики в файл
     *
     * @param var1 Статистика по потоку
     */
    public synchronized void f(Thread var1) {
        try {
            this.fKW.az(this.dateFormat.format(new Date(System.currentTimeMillis())));
            Runtime var2 = Runtime.getRuntime();
            long var3 = var2.maxMemory();
            long var5 = var2.freeMemory();
            long var7 = var2.totalMemory();
            long var9 = var7 - var5;
            this.eqC.println();
            this.eqC.printf("Max Memory:   %,14d\r\n", var3);
            this.eqC.printf("Memory Left:  %,14d  %,4.1f%% of max\r\n", var3 - var7 + var5, (float) (var3 - var7 + var5) * 100.0F / (float) var3);
            this.eqC.printf("Aloc. Memory: %,14d  %,4.1f%% of max\r\n", var7, (float) var7 * 100.0F / (float) var3);
            this.eqC.printf("Free Memory:  %,14d  %,4.1f%% of max %,4.1f%% of aloc.\r\n", var5, (float) var5 * 100.0F / (float) var3, (float) var5 * 100.0F / (float) var7);
            this.eqC.printf("Used Memory:  %,14d  %,4.1f%% of max %,4.1f%% of aloc.\r\n", var9, (float) var9 * 100.0F / (float) var3, (float) var9 * 100.0F / (float) var7);
            this.eqC.println();
            if (var1 == null) {
                TreeSet var11 = new TreeSet(new Comparator() {
                    @Override
                    public int compare(Object o1, Object o2) {
                        return a((Thread) o1, (Thread) o2);
                    }

                    public int a(Thread var1, Thread var2) {
                        return (int) (var1.getId() - var2.getId());
                    }
                });
                var11.addAll((Collection) this.hBb.keySet());
                Iterator var13 = var11.iterator();

                while (var13.hasNext()) {
                    Thread var12 = (Thread) var13.next();
                    if (var12 != null && var12 != this.dumperWatchDog && var12 != this.mainWatchDog) {
                        this.g(var12);
                    }
                }

                this.eqC.flush();
            } else {
                this.g(var1);
            }
        } catch (Exception var14) {
            var14.printStackTrace();
        }
    }

    protected void g(Thread var1) {
        CK var2 = (CK) this.hBb.get(var1);
        if (var2 != null) {
            StringWriter var3 = new StringWriter();
            PrintWriter var4 = new PrintWriter(var3);
            var2.dump(var4);
            var4.flush();
            this.eqC.println(var3.toString());
            this.eqC.flush();
        }
    }

    public synchronized void c(Thread var1) {
        this.hBb.remove(var1);
    }

    public synchronized void reset() {
        this.hBb.clear();
        this.hBc = 0L;
    }

    public void setPrintWriter(PrintWriter var1) {
        this.fKW = new hi(var1);
        this.eqC = new PrintWriter(this.fKW);
    }

    /**
     * millis - время сна потока
     *
     * @param var1
     */
    public void ht(long var1) {
        this.fKU = var1;
    }

    /**
     * Интервал логирования
     *
     * @param var1
     */
    public void K(double var1) {
        this.fKV = var1;
    }

    final class a extends Thread {
        private long fFx = System.nanoTime();

        a(String var2) {
            super(var2);
        }

        public void run() {
            try {
                while (true) {
                    this.doRun();
                }
            } catch (InterruptedException var2) {
                var2.printStackTrace();
            }
        }

        private void doRun() throws InterruptedException {
            Thread.sleep(1000L);
            long var1 = System.nanoTime();
            if ((double) var1 > (double) this.fFx + WatchDog.this.fKV) {
                WatchDog.this.eqC.println("-------------------- Thread Dump -------------------- ");
                WatchDog.this.f((Thread) null);
                WatchDog.this.eqC.println("------------------Thread Dump End ------------------ ");
                WatchDog.this.reset();
                this.fFx = var1;
            }

        }
    }
}
