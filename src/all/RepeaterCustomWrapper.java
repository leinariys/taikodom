package all;

import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.util.*;

/**
 *
 */
public class RepeaterCustomWrapper extends JComponent implements IContainerCustomWrapper {
    protected static final LogPrinter logger = LogPrinter.setClass(RepeaterCustomWrapper.class);

    protected MapKeyValue buU;
    protected a buV;
    protected List buW;
    protected Container buX;
    protected Map buY;
    protected List buZ;

    public RepeaterCustomWrapper(MapKeyValue var1, XmlNode var2) {
        this.buZ = new ArrayList();
        this.buU = var1;
        this.buW = new ArrayList(var2.getChildrenTag());
        this.buY = new HashMap();
    }

    public RepeaterCustomWrapper(MapKeyValue var1, Container var2, XmlNode var3) {
        this(var1, var3);
        while (var2 != null && var2 instanceof RepeaterCustomWrapper) {
            var2 = ((RepeaterCustomWrapper) var2).aiS();
        }
        this.buX = var2;
    }

    protected Container aiS() {
        return this.buX;
    }

    public void a(a var1) {
        this.buV = var1;
    }

    public void k(List var1) {
        this.buW = var1;
    }

    public List G(Object var1) {
        if (this.buX == null) {
            return null;
        } else {
            ArrayList var2 = new ArrayList(this.buW.size());
            Iterator var4 = this.buW.iterator();

            while (var4.hasNext()) {
                XmlNode var3 = (XmlNode) var4.next();
                BaseItem var5 = this.buU.getClassBaseUi(var3.getTegName());
                if (var5 == null) {
                    logger.error("There is test_GLCanvas component named: '" + var3.getTegName() + "'");
                } else {
                    Component var6 = this.buX.add(var5.CreateUI(this.buU, this, var3));
                    if (this.buV != null) {
                        this.buV.a(var1, var6);
                    }
                    var2.add(var6);
                }
            }

            this.buY.put(var1, var2);
            this.buX.validate();
            return var2;
        }
    }

    public List get(Object var1) {
        return (List) this.buY.get(var1);
    }

    public List H(Object var1) {
        if (this.buX == null) {
            return null;
        } else {
            List var2 = (List) this.buY.remove(var1);
            if (var2 == null) {
                return null;
            } else {
                Iterator var4 = var2.iterator();

                while (var4.hasNext()) {
                    Component var3 = (Component) var4.next();
                    this.buX.remove(var3);
                }
                return var2;
            }
        }
    }

    public void clear() {
        Iterator var2 = this.buY.values().iterator();
        while (var2.hasNext()) {
            ArrayList var1 = (ArrayList) var2.next();
            Iterator var4 = var1.iterator();

            while (var4.hasNext()) {
                Component var3 = (Component) var4.next();
                this.buX.remove(var3);
                if (var3 instanceof RepeaterCustomWrapper) {
                    ((RepeaterCustomWrapper) var3).clear();
                    this.buZ.remove(var3);
                }
            }
        }

        this.buY.clear();
        var2 = this.buZ.iterator();
        while (var2.hasNext()) {
            RepeaterCustomWrapper var5 = (RepeaterCustomWrapper) var2.next();
            var5.clear();
        }
        this.buZ.clear();
    }

    public void a(RepeaterCustomWrapper var1) {
        this.buZ.add(var1);
    }

    public void setEnabled(boolean var1) {
    }

    public void setFocusable(boolean var1) {
    }

    public void destroy() {
    }

    public JButton findJButton(String var1) {
        return (JButton) this.findJComponent(var1);
    }

    public JComboBox findJComboBox(String var1) {
        return (JComboBox) this.findJComponent(var1);
    }

    private List aiT() {
        ArrayList var1 = new ArrayList();
        Iterator var3 = this.buY.values().iterator();

        while (var3.hasNext()) {
            ArrayList var2 = (ArrayList) var3.next();
            var1.addAll(var2);
        }

        return var1;
    }

    public Component findJComponent(String var1) {
        Iterator var3 = this.aiT().iterator();
        while (var3.hasNext()) {
            Component var2 = (Component) var3.next();
            if (var1.equals(var2.getName())) {
                return var2;
            }

            if (var2 instanceof IContainerCustomWrapper) {
                Component var4 = ((IContainerCustomWrapper) var2).findJComponent(var1);
                if (var4 != null) {
                    return var4;
                }
            }
        }
        return null;
    }

    public IContainerCustomWrapper ce(String var1) {
        Iterator var3 = this.aiT().iterator();
        while (var3.hasNext()) {
            Component var2 = (Component) var3.next();
            if (var2 instanceof IContainerCustomWrapper) {
                if (var1.equals(var2.getName())) {
                    return (IContainerCustomWrapper) var2;
                }

                IContainerCustomWrapper var4 = ((IContainerCustomWrapper) var2).ce(var1);
                if (var4 != null) {
                    return var4;
                }
            }
        }

        return null;
    }

    public JLabel findJLabel(String var1) {
        return (JLabel) this.findJComponent(var1);
    }

    public ProgressBarCustomWrapper findJProgressBar(String var1) {
        return (ProgressBarCustomWrapper) this.findJComponent(var1);
    }

    public RepeaterCustomWrapper ch(String var1) {
        if (var1 == null) {
            return null;
        } else {
            Iterator var3 = this.buZ.iterator();
            while (var3.hasNext()) {
                RepeaterCustomWrapper var2 = (RepeaterCustomWrapper) var3.next();
                if (var1.equals(var2.getName())) {
                    return var2;
                }
            }
            Component[] var5 = this.getComponents();
            int var4 = var5.length;
            for (int var8 = 0; var8 < var4; ++var8) {
                Component var7 = var5[var8];
                if (var7 instanceof IContainerCustomWrapper) {
                    RepeaterCustomWrapper var6 = ((IContainerCustomWrapper) var7).ch(var1);
                    if (var6 != null) {
                        return var6;
                    }
                }
            }
            return null;
        }
    }

    public TextfieldCustomWrapper findJTextField(String var1) {
        return (TextfieldCustomWrapper) this.findJComponent(var1);
    }

    public void pack() {
    }

    public String getElementName() {
        return "repeater";
    }

    public void Kk() {
        if (this.buX instanceof IContainerCustomWrapper) {
            ((IContainerCustomWrapper) this.buX).Kk();
        }
    }

    public void Kl() {
        if (this.buX instanceof IContainerCustomWrapper) {
            ((IContainerCustomWrapper) this.buX).Kl();
        }
    }

    public interface a {
        void a(Object var1, Component var2);
    }
}
