package all;

import java.io.*;
import java.util.HashMap;
import java.util.List;

public abstract class ZS //implements findJComboBox //TODO На удаление
{
    private static final long serialVersionUID = 1L;
    public static HashMap eRE = new HashMap();
    private final transient a eRU = new a(this);
    protected int eRF;
    protected boolean eRG;
    protected boolean eRH;
    protected boolean eRI;
    protected boolean eRJ;
    protected boolean eRK;
    protected boolean eRL;
    protected String name;
    protected String cUW;
    protected fr eRM;
    protected transient ahP[] eRN;
    protected transient aOH eRO;
    protected transient boolean eRP = false;
    protected oB_q eRQ;
    protected Ot eRR;
    protected long eRS;
    protected m eRT;

    public ZS(String var1, int var2, boolean var3) {
        this.eRF = var2;
        this.eRG = var3;
        this.name = var1;
    }

    public ZS() {
    }

    public boolean equals(Object var1) {
        if (this == var1) {
            return true;
        } else if (var1 == null) {
            return false;
        } else if (this.getClass() != var1.getClass()) {
            return false;
        } else {
            ZS var2 = (ZS) var1;
            return this.eRF == var2.eRF;
        }
    }

    public int hashCode() {
        return this.eRF;
    }

    public boolean hk() {
        return this.eRL;
    }

    public boolean hl() {
        return this.eRK;
    }

    public boolean hm() {
        return this.eRG;
    }

    public boolean hn() {
        return this.eRJ;
    }

    public boolean ho() {
        return this.eRH;
    }

    public long qa() {
        if (this.eRT == null) {
            return -1L;
        } else {
            if (this.eRS == 0L) {
                this.eRS = this.eRT.timeout();
            }

            return this.eRS;
        }
    }

    public int hq() {
        return this.eRF;
    }

    public String name() {
        return this.name;
    }

    public String getName() {
        return this.name;
    }

    public Object writeReplace() {
        return this.eRU;
    }

    public oB_q bJF() {
        return this.eRQ;
    }

    public Ot bJG() {
        return this.eRR;
    }

    public void pq(int var1) {
        this.eRF = var1;
    }

    public ahP[] hi() {
        if (this.eRN != null) {
            return this.eRN;
        } else if (!this.eRJ) {
            return this.eRN = new ahP[0];
        } else {
            List var1 = aKU.dgK();
            String[] var5;
            int var4 = (var5 = this.eRM.qf()).length;

            for (int var3 = 0; var3 < var4; ++var3) {
                String var2 = var5[var3];
                HashMap var7 = eRE;
                fm_q var6;
                synchronized (eRE) {
                    var6 = (fm_q) eRE.get(var2);
                }

                if (var6 != null) {
                    var1.add(new qv(var6));
                } else {
                    System.err.println("Warning: replication rule " + var2 + " not found for " + var6);
                }
            }

            return this.eRN = (ahP[]) var1.toArray(new ahP[var1.size()]);
        }
    }

    public sJ hj() {
        if (this.eRP) {
            return this.eRO;
        } else if (!this.eRJ) {
            return null;
        } else {
            String var1 = this.eRM.qg();
            if (var1 != null && var1.length() != 0) {
                HashMap var3 = eRE;
                fm_q var2;
                synchronized (eRE) {
                    var2 = (fm_q) eRE.get(var1);
                }

                if (var2 == null) {
                    System.err.println("Warning: replication rule " + var1 + " not found for " + var2);
                } else {
                    this.eRO = new aOH(var2);
                }

                this.eRP = true;
                return this.eRO;
            } else {
                return null;
            }
        }
    }

    public boolean hp() {
        return this.eRI;
    }

    public String hr() {
        return this.cUW;
    }

    public static class a implements Externalizable, Serializable {
        private static final long serialVersionUID = 1L;
        private transient ZS gjm;

        public a() {
        }

        public a(ZS var1) {
            this.gjm = var1;
        }

        public void readExternal(ObjectInput var1) throws IOException {
            String var2 = var1.readUTF();
            this.gjm = (ZS) ZS.eRE.get(var2);
        }

        public void writeExternal(ObjectOutput var1) throws IOException {
            var1.writeUTF(this.gjm.cUW);
        }

        public Object readResolve() {
            return this.gjm;
        }
    }
}
