package all;

import java.io.OutputStream;
import java.nio.ByteBuffer;

/**
 *
 */
public final class akK extends OutputStream {
    /**
     * Буффер
     */
    private ByteBuffer fUn;

    public akK() {
        this(16);
    }

    /**
     * @param var1
     */
    public akK(int var1) {
        if (var1 < 0) {
            throw new IllegalArgumentException("Invalid size: " + var1);
        } else {
            this.fUn = ByteBuffer.allocateDirect(var1);//Выделяет новый буфер прямого байта.
        }
    }

    /**
     * @param var1
     */
    public final void write(int var1) {
        this.ensure(this.size() + 1);
        this.fUn.put((byte) var1);
    }

    /**
     * @param var1
     * @param var2
     * @param var3
     */
    public final void write(byte[] var1, int var2, int var3) {
        if (var3 != 0) {
            this.ensure(this.size() + var3);
            this.fUn.put(var1, var2, var3);
        }
    }

    /**
     * @param var1
     */
    private final void ensure(int var1) {
        if (var1 > this.fUn.capacity()) {
            ByteBuffer var2 = ByteBuffer.allocateDirect(Math.max(this.fUn.position() * 3 / 2, var1));
            this.fUn.flip();
            var2.put(this.fUn);
            this.fUn = var2;
        }

    }

    /**
     *
     */
    public final void reset() {
        this.fUn.clear();
    }

    /**
     * @return
     */
    public final int size() {
        return this.fUn.position();
    }

    /**
     * Получить буффер
     *
     * @return
     */
    public final ByteBuffer chm() {
        return this.fUn;
    }

    /**
     * @return
     */
    public String toString() {
        return super.toString();
    }
}
