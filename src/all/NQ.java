package all;

import javax.swing.*;
import java.awt.*;

//Конструктор всплывающего окна
public class NQ extends Popup {
    private static final Integer dIb = Integer.MAX_VALUE;
    private final JComponent dIc;
    JPanel dve = new JPanel(new BorderLayout());

    public NQ(JComponent var1) {
        this.dIc = var1;
    }

    public void a(Component var1, Component var2, int var3, int var4) {
        this.dve.setVisible(false);
        this.dIc.add(this.dve, dIb);
        this.dve.removeAll();
        this.dve.add(var2, "Center");
        if (var2 instanceof JComponent) {
            ((JComponent) var2).setDoubleBuffered(false);
        }

        this.dve.setSize(this.dve.getPreferredSize());
        Point var5 = this.dIc.getLocationOnScreen();
        var4 = Math.min(var4 - var5.y, this.dIc.getHeight() - this.dve.getHeight());
        var3 = Math.min(var3 - var5.x, this.dIc.getWidth() - this.dve.getWidth());
        this.dve.setLocation(var3, var4);
        var2.invalidate();
        this.dve.validate();
        if (var1 instanceof Container) {
            ((Container) var1).setComponentZOrder(this.dve, 0);
        }

    }

    public void show() {
        this.dve.setVisible(true);
    }

    public void hide() {
        Rectangle var1 = this.dve.getBounds();
        this.dIc.remove(this.dve);
        this.dIc.repaint(var1);
    }
}
