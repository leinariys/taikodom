package all;

import java.io.File;
import java.io.FilenameFilter;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;

/**
 * Addon manager
 * class fn
 */
public class AddonManager implements Executor {
    static LogPrinter logger = LogPrinter.K(AddonManager.class);
    private final aWb dei;
    /**
     * путь к списку аддонов taikodom.xml
     */
    private File deh = new File("addons");
    private aBg dej = new aBg();
    private Map dek = new ConcurrentHashMap();
    private ajJ_q del;
    private boolean dem;
    private Map den = new HashMap();
    /**
     * Базовые элементы интерфейса
     */
    private BaseUItegXML aPy;

    /**
     * Addon manager
     *
     * @param var1
     */
    public AddonManager(aWb var1) {
        var1.setAddonManager(this);
        this.del = var1.getEventManager();
        logger.info("Starting addon manager");
        this.dei = var1;
        this.dej.b("Addon loader task", new aen() {
            public void run() {
                AddonManager.this.aVN();
            }
        }, 1000L);
    }

    public Collection aVL() {
        return new ArrayList(this.dek.values());
    }

    /**
     * @return class aUM
     */
    public aWb aVM() {
        return this.dei;
    }

    public aBg alf() {
        return this.dej;
    }

    public void dispose() {
        this.aVQ();
    }

    /**
     * Работа в другом потоке
     */
    protected void aVN() {
        if (this.dem) {
            if (this.aVO()) {
                HashMap var1 = new HashMap(this.dek);
                File[] var5;
                int var4 = (var5 = this.aVP()).length;
                for (int var3 = 0; var3 < var4; ++var3) {
                    File var2 = var5[var3]; // addons/taikodom.xml
                    try {
                        aVW var6 = (aVW) var1.remove(var2.getName());
                        if (var6 == null) {
                            var6 = this.f(var2);
                            this.dek.put(var2.getName(), var6);
                        }

                        this.a(var6);
                    } catch (Exception var7) {
                        logger.error("Error reading file: " + var2, var7);
                    }
                }

                Iterator var9 = var1.keySet().iterator();

                while (var9.hasNext()) {
                    String var8 = (String) var9.next();
                    this.dek.remove(var8);
                }
            }
        }
    }

    private aVW f(File var1) {
        String var2 = var1.getName();
        if (!var2.endsWith(".xml") && (!var2.startsWith("dl-") || !var2.endsWith(".jar"))) {
            throw new IllegalArgumentException("not .js or .groovy: " + var2);
        } else {
            return new wm(this, var1);
        }
    }

    /**
     * запустить загрузку аддонов
     *
     * @param var1
     */
    private void a(aVW var1) {
        try {
            var1.dCM();//запустить загрузку аддонов
        } /*catch (IOException var3) {
         logger.warn("Error reading addons/" + var1.getFile().getTegName() + " (" + var3.getMessage() + ")");
      } */ catch (Throwable var4) {
            logger.warn("Error parsing addons/" + var1.getFile().getName(), var4);
        }

    }

    /**
     * Проверяем наличеа папки
     *
     * @return
     */
    private boolean aVO() {
        File var1 = this.aVR();
        if (!var1.exists()) {
            logger.warn("Directory '" + var1 + "' not found");
            return false;
        } else if (!var1.isDirectory()) {
            logger.warn("'" + var1 + "' is not aQ directory");
            return false;
        } else {
            return true;
        }
    }

    private File[] aVP() {
        File var1 = this.aVR();
        File[] var2 = var1.listFiles(new FilenameFilter() {
            public boolean accept(File var1, String var2) {
                return var2.endsWith(".js") || var2.endsWith(".groovy") || var2.endsWith(".xml") || var2.startsWith("dl-") && var2.endsWith(".jar");
            }
        });
        return var2;
    }

    public void aVQ() {
        Iterator var2 = this.dek.values().iterator();

        while (var2.hasNext()) {
            aVW var1 = (aVW) var2.next();
            var1.dispose();
        }

        this.dek.clear();
    }

    public void step(float var1) {
        this.dej.a((long) (var1 * 1000.0F), this);
    }

    public void execute(Runnable var1) {
        try {
            var1.run();
        } catch (Exception var3) {
            logger.warn("error executing timer", var3);
        }

    }

    public void b(String var1, String... var2) {
        Iterator var4 = this.dek.values().iterator();

        while (var4.hasNext()) {
            aVW var3 = (aVW) var4.next();
            var3.b(var1, var2);
        }

    }

    public Object y(Class var1) {
        Iterator var3 = this.dek.values().iterator();

        aVW var2;
        do {
            if (!var3.hasNext()) {
                return null;
            }

            var2 = (aVW) var3.next();
            if (var2 instanceof wm) {
                wm var4 = (wm) var2;
                Object var5 = var4.aR(var1);
                if (var5 != null) {
                    return var5;
                }
            }
        } while (!var1.isInstance(var2));

        System.out.println();
        return var2;
    }

    public List z(Class var1) {
        ArrayList var2 = new ArrayList();
        Iterator var4 = this.dek.values().iterator();

        while (var4.hasNext()) {
            aVW var3 = (aVW) var4.next();
            if (var3 instanceof wm) {
                wm var5 = (wm) var3;
                List var6 = var5.aS(var1);
                if (var6 != null) {
                    var2.addAll(var6);
                }
            }

            if (var1.isInstance(var3)) {
                System.out.println();
                var2.add(var3);
            }
        }

        return var2;
    }

    /**
     * Получить путь к списку аддонов taikodom.xml
     *
     * @return
     */
    public File aVR() {
        return this.deh;
    }

    /**
     * Установить путь к списку аддонов taikodom.xml
     *
     * @param var1
     */
    public void g(File var1) {
        this.deh = var1;
    }

    /**
     * Резрешить работу по чтению списка аддонов taikodom.xml
     */
    public void aVS() {
        this.dem = true;
    }

    /*
       public boolean aVT() {
          return this.dei.cnn();
       }
    */
    public ajJ_q aVU() {
        return this.del;
    }

    public void aVV() {
        Iterator var2 = this.dek.values().iterator();

        while (var2.hasNext()) {
            aVW var1 = (aVW) var2.next();
            var1.aVV();
        }

    }

    public Object n(String var1, Object var2) {
        return this.den.put(var1, var2);
    }

    public Object eB(String var1) {
        return this.den.get(var1);
    }

    /**
     * Базовые элементы интерфейса
     *
     * @return
     */
    public BaseUItegXML getBaseUItagXML() {
        return this.aPy;
    }

    /**
     * Базовые элементы интерфейса
     *
     * @param var1
     */
    public void b(BaseUItegXML var1) {
        this.aPy = var1;
    }

    public ain aVX() {
        return this.dei.getRootPathRender();
    }

    public ain aVY() {
        return this.dei.getRootPath();
    }
}
