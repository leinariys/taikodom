package all;

import javax.swing.*;
import java.awt.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Менеджер настроек Базовые элементы интерфейса
 * class BaseUItegXML
 * графического Container
 */
public abstract class BaseItem {
    private static final LogPrinter logger = LogPrinter.setClass(BaseItem.class);

    protected static Integer checkValueAttribut(XmlNode var0, String var1, LogPrinter var2) {
        String var3 = var0.getAttribute(var1);
        if (var3 == null) {
            return null;
        } else {
            int var4;
            try {
                var4 = Integer.parseInt(var3);
            } catch (NumberFormatException var6) {
                var2.error("Invalid parameter. Attribute: '" + var1 + "', value: '" + var3 + "'", var6);
                return null;
            }

            return var4;
        }
    }

    /**
     * Создание графического Component, работа с CSS и системой событий
     *
     * @param var1 ключ - значение,  класса перевода и путь
     * @param var2 Рут панель Пример avax.swing.JRootPane[,0,0,1024x780,invalid,layout=javax.swing.JRootPane$RootLayout,alignmentX=0.0,alignmentY=0.0,border=all.BorderWrapper@f34226,flags=16777664,maximumSize=,minimumSize=,preferredSize=]
     * @param var3 Пример <window class="empty" transparent="true" alwaysOnTop="true" focusable="false" layout="GridLayout" css="taikodomversion.css">...</window>
     * @return
     */
    public Component CreateUI(MapKeyValue var1, Container var2, XmlNode var3) {
        Component var4 = this.CreatComponentCustom(var1, var2, var3);//root
        String cssStyle = var3.getAttribute("css");//taikodomversion.css
        if (cssStyle != null) {
            avI var6 = var1.getBaseUiTegXml().loadCSS((Object) var1.getPathFile(), (String) cssStyle);
            aeK var7 = ComponentManager.getCssHolder(var4);
            var7.setCssNode(var6);//Установить класс CSS стиль
            var7.clear();
        }
        //children ????
        this.applyAttribute(var1, var4, var3);//Создание в JComponent других JComponent согласно структуре XML файла
        return var4;
    }

    /**
     * Создать базовый класс компонента
     * Реализован в каждом классе Базовые элементы интерфейса BaseItem
     *
     * @param var1
     * @param var2
     * @param var3
     * @return
     */
    protected abstract JComponent CreatComponentCustom(MapKeyValue var1, Container var2, XmlNode var3);

    /**
     * Устанавливаем настройки системы событий графического Component и его самого
     *
     * @param var1 ключ - значение,  класса перевода и путь
     * @param var2 Крафический Component
     * @param var3 Пример <window class="empty" transparent="true" alwaysOnTop="true" focusable="false" layout="GridLayout" css="taikodomversion.css">...</window>
     */
    protected void applyAttribute(MapKeyValue var1, Component var2, XmlNode var3) {
        aeK var4 = ComponentManager.getCssHolder(var2);//ПОлучили систему событий тега?
        String var5 = var3.getAttribute("name"); //Пример name="version"
        if (var5 != null) {
            var2.setName(var5);//Присваивае графическому Component  имя например version
        }

        var5 = var3.getAttribute("enableMouseOver");//Добавить/удалить Обработчик события курсор над объектом
        if (var5 != null) {
            var4.aP("true".equals(var5));
        }

        if ("false".equals(var3.getAttribute("focusable"))) //может ли компонент получить фокус
        {
            var2.setFocusable(false);
        }

        if ("false".equals(var3.getAttribute("enabled"))) //включенный ?
        {
            var2.setEnabled(false);
        }

        var5 = var3.getAttribute("visible");
        if (var5 != null) {
            var2.setVisible("true".equals(var5));
        }

        if (var2 instanceof JComponent) {
            var5 = var3.getAttribute("tooltipProvider");
            if (var5 != null) {
                ((JComponent) var2).setToolTipText("");
                wz.a((JComponent) var2, var5);
            }

            var5 = var3.getAttribute("tooltip");
            if (var5 != null) {
                ((JComponent) var2).setToolTipText(var5);
            } else {
                var5 = var3.getAttribute("enableTooltip");
                if ("true".equals(var5)) {
                    ((JComponent) var2).setToolTipText("");
                }
            }
        }

        if (var4 != null) {
            String var6 = var3.getAttribute("style"); //style="vertical-align:fill; text-align:fill"
            if (var6 != null) {
                var4.setStyle(var6);
            }
            //Передать в систему событий тега все атрибуты
            var4.setAttributes(var3.getAttributes());

            if ("true".equals(var3.getAttribute("debug"))) {
                var4.setAttribute("debug", "true");
            }
        }
    }

    /**
     * Создание в JComponent других JComponent согласно структуре XML файла
     *
     * @param var1
     * @param var2 Родитель JComponent
     * @param var3 структура XML
     * @return
     */
    protected boolean CreatChildren(MapKeyValue var1, JComponent var2, XmlNode var3) {//children
        ArrayList var4 = null;//Список графических компонентов
        Iterator var6 = var3.getChildrenTag().iterator();

        while (true) {
            XmlNode var5;
            XmlNode var7;
            Component var9;
            while (true) {
                do {
                    if (!var6.hasNext())//hasNext - следующий есть
                    {
                        if (var4 != null) {//Проверяем что в списке есть компоненты, которые надо отображать
                            var2.putClientProperty("component.views", new aUR(var2, var4));
                            return true;
                        }
                        return false;
                    }
                    var5 = (XmlNode) var6.next();
                } while (!"view".equals(var5.getTegName()) && var5.getChildrenTag().size() == 0);//нет потомков и не view

                if (var5.getChildrenTag().size() > 1) {//Игнорирование представления более чем одним компонентом
                    logger.warn("Ignoring view with more than one component");
                }
                //TODO добавить шаги
                var7 = (XmlNode) var5.getChildrenTag().get(0);
                BaseItem var8 = var1.getClassBaseUi(var7.getTegName().toLowerCase());//Получить класс по имени тега в xml
                if (var8 != null) {
                    var9 = var8.CreateUI(var1, var2, var7);
                    break;
                }

                if ("import".equals(var7.getTegName())) {
                    try {
                        URL var10 = new URL(var1.getPathFile(), var7.getAttribute("src"));
                        var9 = var1.getBaseUiTegXml().CreateJComponent((new MapKeyValue(var1, var10)), (URL) var10);
                        break;
                    } catch (MalformedURLException var11) {
                        throw new RuntimeException(var11);
                    }
                }

                logger.error("There is test_GLCanvas component named '" + var7.getTegName() + "'");
            }

            if (var4 == null) {
                var4 = new ArrayList(var7.getChildrenTag().size());
            }

            akP var12 = new akP(var5.getAttribute("name"), var9);
            var12.setAttributes(var7.getAttributes());
            var4.add(var12);
        }
    }
}
