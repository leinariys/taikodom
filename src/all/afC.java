package all;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

public final class afC {
    static final String fuS = " [wrapped] ";
    private static final String fuR = System.getProperty("line.separator");
    private static final String[] fuT = new String[]{"getCause", "getNextException", "getTargetException", "getException", "getSourceException", "getRootCause", "getCausedByException", "getNested"};
    private static final Method fuU;

    static {
        Method var0;
        try {
            var0 = Throwable.class.getMethod("getCause", (Class[]) null);
        } catch (Exception var1) {
            System.out.println("Exception-" + var1);
            var0 = null;
        }

        fuU = var0;
    }

    public static Throwable getCause(Throwable var0) {
        return a(var0, fuT);
    }

    public static Throwable a(Throwable var0, String[] var1) {
        if (var0 == null) {
            return null;
        } else {
            Throwable var2 = e(var0);
            if (var2 == null) {
                if (var1 == null) {
                    var1 = fuT;
                }

                for (int var3 = 0; var3 < var1.length; ++var3) {
                    String var4 = var1[var3];
                    if (var4 != null) {
                        var2 = a(var0, var4);
                        if (var2 != null) {
                            break;
                        }
                    }
                }

                if (var2 == null) {
                    var2 = b(var0, "detail");
                }
            }

            return var2;
        }
    }

    public static Throwable d(Throwable var0) {
        Throwable var1 = getCause(var0);
        if (var1 != null) {
            for (var0 = var1; (var0 = getCause(var0)) != null; var1 = var0) {
                ;
            }
        }

        return var1;
    }

    private static Throwable e(Throwable var0) {
        if (var0 instanceof aIa) {
            return ((aIa) var0).getCause();
        } else if (var0 instanceof SQLException) {
            return ((SQLException) var0).getNextException();
        } else {
            return var0 instanceof InvocationTargetException ? ((InvocationTargetException) var0).getTargetException() : null;
        }
    }

    private static Throwable a(Throwable var0, String var1) {
        Method var2 = null;

        try {
            var2 = var0.getClass().getMethod(var1, (Class[]) null);
        } catch (NoSuchMethodException var6) {
            System.out.println("Exception-" + var6);
        } catch (SecurityException var7) {
            System.out.println("Exception-" + var7);
        }

        if (var2 != null && Throwable.class.isAssignableFrom(var2.getReturnType())) {
            try {
                return (Throwable) var2.invoke(var0);
            } catch (IllegalAccessException var3) {
                System.out.println("Exception-" + var3);
            } catch (IllegalArgumentException var4) {
                System.out.println("Exception-" + var4);
            } catch (InvocationTargetException var5) {
                System.out.println("Exception-" + var5);
            }
        }

        return null;
    }

    private static Throwable b(Throwable var0, String var1) {
        Field var2 = null;

        try {
            var2 = var0.getClass().getField(var1);
        } catch (NoSuchFieldException var5) {
            System.out.println("Exception-" + var5);
        } catch (SecurityException var6) {
            System.out.println("Exception-" + var6);
        }

        if (var2 != null && Throwable.class.isAssignableFrom(var2.getType())) {
            try {
                return (Throwable) var2.get(var0);
            } catch (IllegalAccessException var3) {
                System.out.println("Exception-" + var3);
            } catch (IllegalArgumentException var4) {
                System.out.println("Exception-" + var4);
            }
        }

        return null;
    }

    public static boolean bVB() {
        return fuU != null;
    }

    public static boolean f(Throwable var0) {
        if (var0 == null) {
            return false;
        } else if (var0 instanceof aIa) {
            return true;
        } else if (var0 instanceof SQLException) {
            return true;
        } else if (var0 instanceof InvocationTargetException) {
            return true;
        } else if (bVB()) {
            return true;
        } else {
            Class var1 = var0.getClass();
            int var2 = 0;

            for (int var3 = fuT.length; var2 < var3; ++var2) {
                try {
                    Method var4 = var1.getMethod(fuT[var2], (Class[]) null);
                    if (var4 != null && Throwable.class.isAssignableFrom(var4.getReturnType())) {
                        return true;
                    }
                } catch (NoSuchMethodException var7) {
                    System.out.println("Exception-" + var7);
                } catch (SecurityException var8) {
                    System.out.println("Exception-" + var8);
                }
            }

            try {
                Field var9 = var1.getField("detail");
                if (var9 != null) {
                    return true;
                }
            } catch (NoSuchFieldException var5) {
                System.out.println("Exception-" + var5);
            } catch (SecurityException var6) {
                System.out.println("Exception-" + var6);
            }

            return false;
        }
    }

    public static int g(Throwable var0) {
        int var1;
        for (var1 = 0; var0 != null; var0 = getCause(var0)) {
            ++var1;
        }

        return var1;
    }

    public static Throwable[] h(Throwable var0) {
        ArrayList var1;
        for (var1 = new ArrayList(); var0 != null; var0 = getCause(var0)) {
            var1.add(var0);
        }

        return (Throwable[]) var1.toArray(new Throwable[var1.size()]);
    }

    public static int a(Throwable var0, Class var1) {
        return a(var0, var1, 0);
    }

    public static int a(Throwable var0, Class var1, int var2) {
        if (var0 == null) {
            return -1;
        } else {
            if (var2 < 0) {
                var2 = 0;
            }

            Throwable[] var3 = h(var0);
            if (var2 >= var3.length) {
                return -1;
            } else {
                for (int var4 = var2; var4 < var3.length; ++var4) {
                    if (var3[var4].getClass().equals(var1)) {
                        return var4;
                    }
                }

                return -1;
            }
        }
    }

    public static void i(Throwable var0) {
        a(var0, System.err);
    }

    public static void a(Throwable var0, PrintStream var1) {
        if (var0 != null) {
            if (var1 == null) {
                throw new IllegalArgumentException("The PrintStream must not be null");
            } else {
                String[] var2 = j(var0);

                for (int var3 = 0; var3 < var2.length; ++var3) {
                    var1.println(var2[var3]);
                }

                var1.flush();
            }
        }
    }

    public static void a(Throwable var0, PrintWriter var1) {
        if (var0 != null) {
            if (var1 == null) {
                throw new IllegalArgumentException("The PrintWriter must not be null");
            } else {
                String[] var2 = j(var0);

                for (int var3 = 0; var3 < var2.length; ++var3) {
                    var1.println(var2[var3]);
                }

                var1.flush();
            }
        }
    }

    public static String[] j(Throwable var0) {
        if (var0 == null) {
            return new String[0];
        } else {
            Throwable[] var1 = h(var0);
            int var2 = var1.length;
            ArrayList var3 = new ArrayList();
            List var4 = l(var1[var2 - 1]);
            int var5 = var2;

            while (true) {
                --var5;
                if (var5 < 0) {
                    return (String[]) var3.toArray(new String[0]);
                }

                List var6 = var4;
                if (var5 != 0) {
                    var4 = l(var1[var5 - 1]);
                    d(var6, var4);
                }

                if (var5 == var2 - 1) {
                    var3.add(var1[var5].toString());
                } else {
                    var3.add(" [wrapped] " + var1[var5].toString());
                }

                for (int var7 = 0; var7 < var6.size(); ++var7) {
                    var3.add((String) var6.get(var7));
                }
            }
        }
    }

    public static void d(List var0, List var1) {
        if (var0 != null && var1 != null) {
            int var2 = var0.size() - 1;

            for (int var3 = var1.size() - 1; var2 >= 0 && var3 >= 0; --var3) {
                String var4 = (String) var0.get(var2);
                String var5 = (String) var1.get(var3);
                if (var4.equals(var5)) {
                    var0.remove(var2);
                }

                --var2;
            }

        } else {
            throw new IllegalArgumentException("The List must not be null");
        }
    }

    public static String getStackTrace(Throwable var0) {
        StringWriter var1 = new StringWriter();
        PrintWriter var2 = new PrintWriter(var1, true);
        var0.printStackTrace(var2);
        return var1.getBuffer().toString();
    }

    public static String k(Throwable var0) {
        StringWriter var1 = new StringWriter();
        PrintWriter var2 = new PrintWriter(var1, true);
        Throwable[] var3 = h(var0);

        for (int var4 = 0; var4 < var3.length; ++var4) {
            var3[var4].printStackTrace(var2);
            if (f(var3[var4])) {
                break;
            }
        }

        return var1.getBuffer().toString();
    }

    public static String[] b(Throwable var0) {
        return var0 == null ? new String[0] : iF(getStackTrace(var0));
    }

    static String[] iF(String var0) {
        String var1 = fuR;
        StringTokenizer var2 = new StringTokenizer(var0, var1);
        LinkedList var3 = new LinkedList();

        while (var2.hasMoreTokens()) {
            var3.add(var2.nextToken());
        }

        return (String[]) var3.toArray(new String[var3.size()]);
    }

    static List l(Throwable var0) {
        String var1 = getStackTrace(var0);
        String var2 = fuR;
        StringTokenizer var3 = new StringTokenizer(var1, var2);
        LinkedList var4 = new LinkedList();
        boolean var5 = false;

        while (var3.hasMoreTokens()) {
            String var6 = var3.nextToken();
            int var7 = var6.indexOf("at");
            if (var7 != -1 && var6.substring(0, var7).trim().length() == 0) {
                var5 = true;
                var4.add(var6);
            } else if (var5) {
                break;
            }
        }

        return var4;
    }
}
