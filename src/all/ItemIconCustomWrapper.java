package all;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class ItemIconCustomWrapper extends JPanel implements IComponentCustomWrapper, Icon {
    private static final long serialVersionUID = 1L;
    private static final String Tb = "res://imageset_items/";
    private static final String Tc = "res://data/gui/imageset/imageset_items/";
    private static final String dLx = "ico_fra_medium_enabled";
    private static final String dLy = "ico_fra_medium_over";
    private static final String dLz = "ico_fra_medium_small";
    private static final String dLA = "ico_fra_medium_medium";
    private static final String dLB = "ico_fra_medium_big";
    private static final String dLC = "ico_fra_medium_red";
    private static final int dLD = 0;
    private static final int dLE = 1;
    private Dimension size;
    private Image Td;
    private Image[] dLF;
    private Image dLG;
    private Image dLH;
    private Image dLI;
    private Object data;
    private String Te;
    private boolean disabled;
    private b dLJ;
    private int dLK = 0;
    private ILoaderImageInterface Th = BaseUItegXML.thisClass.getLoaderImage();

    public ItemIconCustomWrapper() {
        this.installListeners();
        this.xN();
    }

    private void xN() {
        this.dLF = new Image[2];
        this.dLF[0] = this.Th.getImage("res://imageset_items/ico_fra_medium_enabled");
        this.dLF[1] = this.Th.getImage("res://imageset_items/ico_fra_medium_over");
        this.dLG = null;
        this.dLJ = b.eJK;
        this.dLH = this.Th.getImage("res://imageset_items/ico_fra_medium_red");
        this.size = new Dimension();
        this.size.width = this.dLF[1].getWidth(this);
        this.size.height = this.dLF[1].getHeight(this);
    }

    public int getIconHeight() {
        return this.size.height;
    }

    public int getIconWidth() {
        return this.size.width;
    }

    private void installListeners() {
        this.addMouseListener(new MouseListener() {
            public void mouseClicked(MouseEvent var1) {
            }

            public void mouseEntered(MouseEvent var1) {
                ItemIconCustomWrapper.this.dLK = 1;
            }

            public void mouseExited(MouseEvent var1) {
                ItemIconCustomWrapper.this.dLK = 0;
            }

            public void mousePressed(MouseEvent var1) {
            }

            public void mouseReleased(MouseEvent var1) {
            }
        });
    }

    public void paintIcon(Component var1, Graphics var2, int var3, int var4) {
        if (this.dLF != null) {
            int var5 = this.size.width;
            int var6 = this.size.height;
            if (this.Td != null) {
                var2.drawImage(this.Td, var3 + 3, var4 + 3, var3 + var5 - 3, var4 + var6 - 3, 0, 0, this.Td.getWidth(var1), this.Td.getHeight(var1), var1);
            }

            var2.drawImage(this.dLF[this.dLK], var3, var4, var3 + var5, var4 + var6, 0, 0, this.dLF[this.dLK].getWidth(var1), this.dLF[this.dLK].getHeight(var1), var1);
            if (this.dLG != null) {
                var2.drawImage(this.dLG, var3, var4, var3 + var5, var4 + var6, 0, 0, this.dLG.getWidth(var1), this.dLG.getHeight(var1), var1);
            }

            if (this.dLH != null && this.disabled) {
                var2.drawImage(this.dLH, var3, var4, var3 + var5, var4 + var6, 0, 0, this.dLH.getWidth(var1), this.dLH.getHeight(var1), var1);
            }

            if (this.dLI != null) {
                var2.drawImage(this.dLI, var3, var4, var3 + var5, var4 + var6, 0, 0, this.dLI.getWidth(var1), this.dLI.getHeight(var1), var1);
            }

            if (this.Te != null) {
                this.Te.length();
            }

        }
    }

    public void paint(Graphics var1) {
        this.paintIcon(this, var1, 0, 0);
    }

    public void paintAll(Graphics var1) {
        this.paintIcon(this, var1, 0, 0);
    }

    public void aw(String var1) {
        try {
            this.setImage(this.Th.getImage("res://data/gui/imageset/imageset_items/" + var1));
        } catch (Exception var2) {
            ;
        }

    }

    public void setImage(Image var1) {
        this.Td = var1;
    }

    public void ax(String var1) {
        this.Te = var1;
    }

    public String blM() {
        return this.Te;
    }

    public boolean isDisabled() {
        return this.disabled;
    }

    public void setDisabled(boolean var1) {
        this.disabled = var1;
    }

    public void a(b var1) {
        this.dLJ = var1;
        if (this.dLJ == b.eJK) {
            this.dLG = null;
        } else if (this.dLJ == b.eJL) {
            this.dLG = this.Th.getImage("res://imageset_items/ico_fra_medium_small");
        } else if (this.dLJ == b.eJM) {
            this.dLG = this.Th.getImage("res://imageset_items/ico_fra_medium_medium");
        } else if (this.dLJ == b.eJN) {
            this.dLG = this.Th.getImage("res://imageset_items/ico_fra_medium_big");
        }

    }

    public b blN() {
        return this.dLJ;
    }

    public void bo(int var1) {
        if (var1 > 0) {
            this.ax(String.valueOf(var1));
        } else {
            this.ax((String) null);
        }

    }

    public Object getData() {
        return this.data;
    }

    public void setData(Object var1) {
        this.data = var1;
    }

    public Dimension getPreferredSize() {
        return this.size;
    }

    public String getElementName() {
        return "item-iconImage";
    }

    public static enum b {
        eJK,
        eJL,
        eJM,
        eJN;
    }
}
