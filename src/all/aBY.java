package all;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class aBY implements Comparable {
    private final Md fsB;
    public int hmH;
    private long hmG;
    private Set elements = new HashSet();

    public aBY(Md var1) {
        this.fsB = var1;
    }

    public int a(aBY var1) {
        int var2 = (int) (var1.hmG - this.hmG);
        if (var2 == 0) {
            var2 = this.bUY().b(var1.bUY());
            if (var2 == 0) {
                return System.identityHashCode(this) - System.identityHashCode(var1);
            }
        }

        return var2;
    }

    public void b(StackTraceElement var1) {
        ++this.hmG;
        this.elements.add(var1);
    }

    public long cMQ() {
        return this.hmG;
    }

    public void jL(long var1) {
        this.hmG = var1;
    }

    public String toString() {
        String var1 = this.fsB.getDeclaringClass() + "." + this.fsB.getMethodName();
        Iterator var3 = this.elements.iterator();

        while (var3.hasNext()) {
            StackTraceElement var2 = (StackTraceElement) var3.next();
            if (var2.getFileName() != null) {
                if (var2.getLineNumber() > 0) {
                    var1 = var1 + "(" + var2.getFileName() + ":" + var2.getLineNumber() + ")";
                } else {
                    var1 = var1 + "(" + var2.getFileName() + ")";
                }
            }
        }

        return var1;
    }

    public Md bUY() {
        return this.fsB;
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }
}
