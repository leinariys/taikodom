package all;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class Bl_q extends aOv implements Externalizable {
    byte cli;
    private Xf_q Uk;

    public Bl_q() {
    }

    public Bl_q(Xf_q var1, byte var2) {
        this.Uk = var1;
        this.cli = var2;
    }

    public int ayq() {
        return this.cli;
    }

    public Xf_q yz() {
        return this.Uk;
    }

    public void readExternal(ObjectInput var1) throws IOException, ClassNotFoundException {
        super.readExternal(var1);
        this.Uk = (Xf_q) GZ.dah.b(var1);
        this.cli = var1.readByte();
    }

    public void writeExternal(ObjectOutput var1) throws IOException {
        super.writeExternal(var1);
        GZ.dah.a((ObjectOutput) var1, (Object) this.Uk);
        var1.writeByte(this.cli);
    }
}
