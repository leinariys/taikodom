package all;

import taikodom.infra.comm.network.ClientAlreadyConnectedException;
import taikodom.infra.comm.network.InvalidUsernamePasswordException;
import taikodom.infra.comm.network.ServerLoginException;
import taikodom.infra.comm.network.UserAccountNotActivatedException;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

//Подключение к серверу
public class aqx_q extends dm_q {
    static LogPrinter logger = LogPrinter.K(aqx_q.class);
    // $FF: synthetic field
    private static int[] xK;
    private final boolean auG;
    private String username;
    private String password;
    private boolean edK;
    private int gqH;
    private long xI;
    private ha gqI;
    private LZ gqJ;
    private CyclicBarrier gqK;
    private int gjl;

    // live.server2.taikodom.com, 15225, null, null, false, 1.0.4933.55
    public aqx_q(String var1, int var2, String var3, String var4, boolean var5, String var6) throws Exception {
        this(var1, var2, var3, var4, var5, var6, true);
    }

    //// live.server2.taikodom.com, 15225, null, null, false, 1.0.4933.55, true
    public aqx_q(String var1, int var2, String var3, String var4, boolean var5, String var6, boolean var7) throws Exception {
        this.gqK = new CyclicBarrier(2);
        this.auG = var7;
        this.username = var3;
        this.password = var4;
        this.edK = var5;
        logger.info("Opening channel");
        SocketChannel var8 = SocketChannel.open();

        try {
            logger.info("Trying to connect to " + var1 + ":" + var2);
            var8.connect(new InetSocketAddress(var1, var2));//попытка подключения
            this.yX = InetAddress.getByName(var1);
        } catch (IOException var13) {
            var8.close();
            throw new cU_q(var1 + ":" + var2, var13);
        }

        var8.configureBlocking(false);
        logger.info("Channel opened");//Подключение успешно
        this.versionString = var6;
        this.gqI = mP.a(all.b.a.k, var8.socket().getInetAddress());
        this.gqJ = new LZ(this.gqI, this, var8, var2);
        this.yQ.a((XF_w) this.gqJ);//Создание потока для прослушивания
        this.yQ.start();//Прослушивание порта запуск потока

        try {
            logger.info("Waiting connection.");//Ожидание соединения
            this.gqK.await(1L, TimeUnit.SECONDS);
        } catch (InterruptedException var10) {
            var10.printStackTrace();
        } catch (BrokenBarrierException var11) {
            var11.printStackTrace();
        } catch (TimeoutException var12) {
            throw new cU_q("Timeout waiting for network layer, server is not responding.");//Таймаут ожидания сетевого уровня, сервер не отвечает
        }

        if (var7) {
            switch (ji()[om_q.a_q.values()[this.gqH].ordinal()]) {
                case 1:
                    break;
                case 2:
                    throw new InvalidUsernamePasswordException();
                case 3:
                    throw new ClientAlreadyConnectedException();
                case 4:
                    throw new ServerLoginException();
                case 5:
                    throw new UserAccountNotActivatedException();
                case 6:
                    throw new aua_w();
                case 7:
                    throw new ayM();
                case 8:
                    throw new axc();
                case 9:
                    throw new adC();
                case 10:
                    throw new aGJ(this.gjl);
                default:
                    throw new RuntimeException("TcpClientNetwork protocol error!");
            }
        }

    }

    // $FF: synthetic method
    static int[] ji() {
        int[] var10000 = xK;
        if (xK != null) {
            return var10000;
        } else {
            int[] var0 = new int[om_q.a_q.values().length];

            try {
                var0[om_q.a_q.aNI.ordinal()] = 4;
            } catch (NoSuchFieldError var10) {
                ;
            }

            try {
                var0[om_q.a_q.aNM.ordinal()] = 8;
            } catch (NoSuchFieldError var9) {
                ;
            }

            try {
                var0[om_q.a_q.aNL.ordinal()] = 7;
            } catch (NoSuchFieldError var8) {
                ;
            }

            try {
                var0[om_q.a_q.aNG.ordinal()] = 2;
            } catch (NoSuchFieldError var7) {
                ;
            }

            try {
                var0[om_q.a_q.aNF.ordinal()] = 1;
            } catch (NoSuchFieldError var6) {
                ;
            }

            try {
                var0[om_q.a_q.aNO.ordinal()] = 10;
            } catch (NoSuchFieldError var5) {
                ;
            }

            try {
                var0[om_q.a_q.aNH.ordinal()] = 3;
            } catch (NoSuchFieldError var4) {
                ;
            }

            try {
                var0[om_q.a_q.aNK.ordinal()] = 6;
            } catch (NoSuchFieldError var3) {
                ;
            }

            try {
                var0[om_q.a_q.aNN.ordinal()] = 9;
            } catch (NoSuchFieldError var2) {
                ;
            }

            try {
                var0[om_q.a_q.aNJ.ordinal()] = 5;
            } catch (NoSuchFieldError var1) {
                ;
            }

            xK = var0;
            return var0;
        }
    }

    public void a(LZ var1, JB var2) throws IOException, InterruptedException {
        if (var1.bgq() != LZ.a_q.eQr) {
            logger.info("Received: " + var2.getLength() + " bytes " + var1.bgq());
        }

        super.a(var1, var2);
    }

    public void a(all.b var1, byte[] var2, int var3, int var4) throws IOException {
        if (this.gqJ.bgq() != LZ.a_q.eQr) {
            logger.info("Sending: " + var4 + " bytes " + this.gqJ.bgq());
        }

        if (var1 == null) {
            var1 = this.gqI;
        }

        super.a((all.b) var1, var2, var3, var4);
    }

    protected void d(LZ var1, JB var2) throws IOException {
        aMf var3 = new aMf(var2.cJn(), var2.getOffset(), var2.getLength());
        var3.readInt();
        int var4 = var3.readInt();
        this.gjl = var3.readInt() + 1;
        this.gqH = var4;
        if (var4 == om_q.a_q.aNF.ordinal()) {
            this.f(var1);
        } else {
            var1.a(LZ.a_q.eQs);

            try {
                this.gqK.await();
            } catch (InterruptedException var6) {
                var6.printStackTrace();
            } catch (BrokenBarrierException var7) {
                var7.printStackTrace();
            }

            var1.bgv();
            this.close();
        }

    }

    private void f(LZ var1) {
        var1.a(LZ.a_q.eQr);
        this.d(var1);
        this.xI = System.currentTimeMillis();

        try {
            this.gqK.await();
        } catch (InterruptedException var3) {
            var3.printStackTrace();
        } catch (BrokenBarrierException var4) {
            var4.printStackTrace();
        }

    }

    protected void R(String var1) {
        super.R(var1);
        this.gqH = om_q.a_q.aNL.ordinal();
        this.crr();
    }

    private void crr() {
        if (!this.gqK.isBroken()) {
            try {
                this.gqK.await();
            } catch (InterruptedException var2) {
                var2.printStackTrace();
            } catch (BrokenBarrierException var3) {
                var3.printStackTrace();
            }
        } else {
            System.out.println("broken barrier");
        }

    }

    protected void c(LZ var1, JB var2) throws IOException {
        int var3 = this.a(LZ.a_q.eQo, var2);
        if (var3 == 0) {
            if (this.auG) {
                ala var4 = new ala();
                var4.writeUTF(this.username);
                var4.writeUTF(this.password);
                var4.writeBoolean(this.edK);
                byte[] var5 = var4.toByteArray();
                var1.a(LZ.a_q.eQp);
                this.e(var1, new JB(var5));
            } else {
                this.f(var1);
            }
        } else {
            var1.bgv();
            this.gqH = om_q.a_q.aNL.ordinal();
            this.crr();
        }

    }

    protected void e(LZ var1, JB var2) throws IOException {
        if (var1.bgq() != LZ.a_q.eQr) {
            logger.info("Sending: " + var2.getLength() + " bytes " + var1.bgq());
        }

        super.e(var1, var2);
    }

    protected void e(LZ var1) {
        try {
            super.e(var1);
        } finally {
            //try {
            this.yQ.close();
         /*} catch (IOException var7) {
            var7.printStackTrace();
         }*/

        }

    }

    public long jf() {
        return (long) this.gqJ.ayj();
    }

    public long je() {
        return (long) this.gqJ.ayl();
    }

    public long jc() {
        return (long) this.gqJ.ayk();
    }

    public long jg() {
        return (long) this.gqJ.gG();
    }

    public long jd() {
        return (long) this.gqJ.gI();
    }

    public long jb() {
        return (long) this.gqJ.gH();
    }

    public long ja() {
        return this.xI;
    }
}
