package all;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import java.util.HashMap;
import java.util.Map;

public class UIDefaultsUI extends UIDefaults {

    Map iTa = new HashMap();

    public UIDefaultsUI(int initialCapacity, float loadFactor) {
        super(initialCapacity, loadFactor);
        this.iTa.put("javax.swing.JLabel", new a_q() {
            @Override
            public ComponentUI createUI(JComponent var1) {
                return new LabelUI(var1);
            }
        });
    }

    @Override
    public ComponentUI getUI(JComponent var1) {
        Class var2 = var1.getClass();

        do {
            a_q var3 = (a_q) this.iTa.get(var2.getName());
            if (var3 != null) {
                return var3.createUI(var1);
            }
        } while (var2 != Object.class);

        return super.getUI(var1);
    }

    public abstract static class a_q {
        public abstract ComponentUI createUI(JComponent var1);
    }
}
