package all;

public class BI {
    public IParser azZ() throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        String var1 = System.getProperty("org.w3c.css.sac.parser");
        if (var1 == null) {
            throw new NullPointerException("No value for sac.parser property");
        } else {
            return (IParser) Class.forName(var1).newInstance();
        }
    }
}
