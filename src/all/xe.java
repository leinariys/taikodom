package all;

import org.w3c.dom.css.CSSStyleDeclaration;
import taikodom.render.RenderView;
import taikodom.render.helpers.RenderTask;
import taikodom.render.scene.SoundObject;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.net.URL;

/**
 * Базовые элементы интерфейса
 * class BaseUItegXML
 */
public abstract class xe {
    /**Базовые элементы интерфейса
     */
    //static xe thisClass;
/*
   public static void add(xe var0) {
      thisClass = var0;
   }
*/

    /**
     * Базовые элементы интерфейса
     *
     * @return
     */
   /*
   public static xe anu()
   {
      if (thisClass == null)
      {
         thisClass = new BaseUItegXML();
      }
      return thisClass;
   }
*/
    public abstract void a(Object var1, Component var2, File var3);

    public abstract void a(Object var1, Component var2, String var3);

    public abstract Component c(URL var1);

    public abstract Component e(File var1);

    public abstract Component d(Object var1, String var2);

    public abstract Component e(Object var1, String var2);

    public abstract avI anv();

    public abstract ILoaderImageInterface adz();

    public abstract JRootPane getRootPane();

    public abstract void setRootPane(JRootPane var1);

    public abstract avI a(Object var1, File var2);

    /**
     * Загрузка CSS стилей
     *
     * @param var1 null или  file:/C://addon/debugtools/taikodomversion/taikodomversion.xml
     * @param var2 res://data/styles/core.css или taikodomversion.css
     * @return
     */
    public abstract avI a(Object var1, String var2);

    public abstract CSSStyleDeclaration aY(String var1);

    public abstract void a(ILoaderImageInterface var1);

    public abstract Font getFont(String var1, int var2, int var3);

    public abstract void a(age_q var1);

    /**
     * @param var1 Пример TranslationParseContext: class taikodom.addon.debugtools.taikodomversion.TaikodomVersionAddon
     * @param var2 Пример file:/C:/.../taikodom/addon/debugtools/taikodomversion/taikodomversion.xml
     * @return
     */
    public abstract Component CreatComponent(MapKeyValue var1, URL var2);

    public abstract void a(RenderTask var1);

    public abstract void a(RenderView var1);

    public abstract void a(WindowCustomWrapper var1, boolean var2);

    protected abstract JLayeredPane anw();

    public abstract void bE(boolean var1);

    public abstract boolean anx();

    public abstract SoundObject cx(String var1);

    public abstract SoundObject e(String var1, boolean var2);

    public abstract Ma any();

    /**
     * Принять ссылку на класс озвучки интерфейса
     *
     * @param var1 class Ix
     */
    public abstract void a(Ma var1);
}
