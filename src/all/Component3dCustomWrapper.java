package all;

import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import taikodom.render.RenderView;
import taikodom.render.SceneView;
import taikodom.render.StepContext;
import taikodom.render.TexBackedImage;
import taikodom.render.camera.OrbitalCamera;
import taikodom.render.enums.LightType;
import taikodom.render.helpers.RenderTask;
import taikodom.render.scene.RLight;
import taikodom.render.scene.RSceneAreaInfo;
import taikodom.render.scene.Scene;
import taikodom.render.scene.SceneObject;
import taikodom.render.textures.BaseTexture;
import taikodom.render.textures.RenderTarget;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Component3dCustomWrapper extends PanelCustomWrapper {
    private static final int gsT = 50;
    private int gsU = this.getWidth();
    private int gsV = this.getHeight();
    private long startTime = System.currentTimeMillis();
    private long cYd = System.currentTimeMillis();
    private float gsW = 0.033F;
    private float gsX;
    private SceneView sceneView = new SceneView();
    private StepContext stepContext;
    private RenderTarget renderTarget;
    private OrbitalCamera gsY;
    private BaseTexture gsZ;
    private TexBackedImage gta;
    private Timer timer;
    private boolean gtb = false;
    private boolean aRG = false;
    private RenderTask gtc = new RenderTask() {
        public void run(RenderView var1) {
            Component3dCustomWrapper.this.b(var1);
        }
    };
    private RenderTask gtd = new RenderTask() {
        public void run(RenderView var1) {
            if (Component3dCustomWrapper.this.getWidth() >= 0 && Component3dCustomWrapper.this.getHeight() >= 0) {
                if (Component3dCustomWrapper.this.renderTarget != null) {
                    Component3dCustomWrapper.this.renderTarget.releaseReferences(var1.getDrawContext().getGL());
                    Component3dCustomWrapper.this.renderTarget = null;
                    Component3dCustomWrapper.this.gsZ = null;
                    Component3dCustomWrapper.this.gta = null;
                }

                Component3dCustomWrapper.this.gsU = Component3dCustomWrapper.this.getWidth();
                Component3dCustomWrapper.this.gsV = Component3dCustomWrapper.this.getHeight();
                Component3dCustomWrapper.this.c(var1);
            }
        }
    };
    private RenderTask gte = new RenderTask() {
        public void run(RenderView var1) {
            if (Component3dCustomWrapper.this.sceneView != null) {
                Component3dCustomWrapper.this.sceneView.getScene().removeAllChildren();
                Component3dCustomWrapper.this.sceneView = null;
            }

            if (Component3dCustomWrapper.this.renderTarget != null) {
                Component3dCustomWrapper.this.renderTarget.releaseReferences(var1.getDrawContext().getGL());
                Component3dCustomWrapper.this.renderTarget = null;
                Component3dCustomWrapper.this.gsZ = null;
                Component3dCustomWrapper.this.gta = null;
            }

            if (Component3dCustomWrapper.this.timer != null) {
                Component3dCustomWrapper.this.timer.stop();
                Component3dCustomWrapper.this.timer = null;
            }

        }
    };

    public Component3dCustomWrapper() {
        this.sceneView.getCamera().setAspect((float) this.gsU / (float) this.gsV);
        this.sceneView.setViewport(0, 0, this.gsU, this.gsV);
        this.sceneView.setAllowImpostors(false);
        this.sceneView.setClearColorBuffer(true);
        this.sceneView.setClearDepthBuffer(true);
        this.sceneView.setClearStencilBuffer(true);
        this.sceneView.disable();
        this.stepContext = new StepContext();
    }

    public boolean csl() {
        return this.aRG;
    }

    public void gf(boolean var1) {
        this.aRG = var1;
    }

    public void setVisible(boolean var1) {
        super.setVisible(var1);
        if (var1) {
            this.kA(this.gsW);
        } else if (this.timer != null) {
            this.timer.stop();
            this.timer = null;
        }

    }

    public void kA(float var1) {
        this.gsW = var1;
        if (this.timer != null) {
            this.timer.stop();
        }

        short var2 = 2000;
        this.timer = new Timer((int) (var1 * (float) var2), new ActionListener() {
            public void actionPerformed(ActionEvent var1) {
                Component3dCustomWrapper.this.csm();
                Component3dCustomWrapper.this.repaint();
                if (!Component3dCustomWrapper.this.isShowing()) {
                    Component3dCustomWrapper.this.timer.stop();
                }

            }
        });
        this.timer.start();
    }

    private void csm() {
        BaseUItegXML.thisClass.a(this.gtc);
    }

    private void b(RenderView var1) {
        if (this.renderTarget == null && this.getWidth() > 0 && this.getHeight() > 0) {
            this.c(var1);
        }

        javax.media.opengl.GL var2 = var1.getDrawContext().getGL();
        var2.glPushAttrib(524288);
        if (this.sceneView != null && this.sceneView.isEnabled()) {
            long var3 = System.currentTimeMillis();
            this.stepContext.reset();
            float var5 = (float) (var3 - this.cYd) * 0.001F;
            if (this.gsY != null) {
                this.gsY.setYaw(this.gsY.getYaw() + this.gsX * var5);
                this.gsY.step(this.stepContext);
                this.stepContext.setCamera(this.gsY);
            }

            this.stepContext.setDeltaTime(var5);
            this.sceneView.getScene().step(this.stepContext);
            this.csn();
            this.sceneView.getCamera().setAspect((float) this.getWidth() / (float) this.getHeight());
            this.sceneView.setViewport(0, 0, this.gsU, this.gsV);
            var1.render(this.sceneView, (double) ((float) (var3 - this.startTime) * 0.001F), var5);
            this.cYd = var3;
            var2.glPopAttrib();
            this.aRG = true;
        }
    }

    private void csn() {
        if (this.gsU <= 0 || this.gsV <= 0) {
            this.gsU = this.getWidth();
            this.gsV = this.getHeight();
            this.sceneView.getCamera().setAspect((float) this.gsU / (float) this.gsV);
        }

    }

    private void c(RenderView var1) {
        if (this.renderTarget == null) {
            this.renderTarget = new RenderTarget();
            this.csn();
            this.renderTarget.create(this.gsU, this.gsV, true, var1.getDrawContext(), false);
            this.renderTarget.bind(var1.getDrawContext());
            var1.getDrawContext().getGL().glClearColor(0.0F, 0.0F, 0.0F, 0.0F);
            var1.getDrawContext().getGL().glClear(16384);
            this.renderTarget.copyBufferToTexture(var1.getDrawContext());
            this.renderTarget.unbind(var1.getDrawContext());
            this.gsZ = this.renderTarget.getTexture();
            this.sceneView.setRenderTarget(this.renderTarget);
            this.sceneView.getSceneViewQuality().setShaderQuality(var1.getDrawContext().getMaxShaderQuality());
            if (this.gsZ.getTarget() == 34037) {
                this.gta = new TexBackedImage(this.gsU, this.gsV, (float) this.gsZ.getSizeX(), (float) this.gsZ.getSizeY(), this.gsZ);
            } else {
                this.gta = new TexBackedImage(this.gsU, this.gsV, (float) this.gsU / (float) this.gsZ.getSizeX(), (float) this.gsV / (float) this.gsZ.getSizeY(), this.gsZ);
            }

            this.sceneView.enable();
            this.b(var1);
        }

    }

    public SceneView getSceneView() {
        return this.sceneView;
    }

    public void destroy() {
        if (this.timer != null) {
            this.timer.stop();
            this.timer = null;
        }

        BaseUItegXML.thisClass.a(this.gte);
    }

    public Scene getScene() {
        return this.sceneView.getScene();
    }

    public String getElementName() {
        return "component3d";
    }

    protected void paintComponent(Graphics var1) {
        Insets var2 = this.getInsets();
        int var3 = this.getWidth();
        int var4 = this.getHeight();
        if (this.timer != null && !this.timer.isRunning()) {
            this.timer.start();
        }

        if (this.gta != null && this.aRG) {
            var1.drawImage(this.gta, var2.left, var4 - var2.bottom, var3 - var2.right, var2.top, 0, 0, this.gta.getWidth(), this.gta.getHeight(), this);
        }

    }

    public void n(SceneObject var1) {
        if (var1 != null) {
            float var2 = var1.computeLocalSpaceAABB().length();
            float var3 = var1.getAABB().diq() / 2.0F;
            double var4 = this.sceneView.getDistanceForWidthSize((double) var2, (float) Math.min(this.getWidth(), this.getHeight()));
            this.sceneView.getCamera().setPosition(0.0D, 0.0D, var4 + (double) var3);
        }

    }

    private void cso() {
        if (this.getWidth() >= 0 && this.getHeight() >= 0) {
            if (this.gtb || Math.abs(this.getWidth() - this.gsU) >= 50 || Math.abs(this.getHeight() - this.gsV) >= 50) {
                this.gsU = this.getWidth();
                this.gsV = this.getHeight();
                this.sceneView.setViewport(0, 0, this.gsU, this.gsV);
                this.sceneView.getCamera().setAspect((float) this.gsU / (float) this.gsV);
                this.sceneView.getCamera().setNearPlane(0.1F);
                this.sceneView.getCamera().setFarPlane(99999.0F);
                BaseUItegXML.thisClass.a(this.gtd);
                this.gtb = false;
            }
        }
    }

    public void setSize(int var1, int var2) {
        super.setSize(var1, var2);
        this.cso();
    }

    public void setSize(Dimension var1) {
        this.setSize(var1.width, var1.height);
    }

    public void setBounds(int var1, int var2, int var3, int var4) {
        super.setBounds(var1, var2, var3, var4);
        this.cso();
    }

    public void setBounds(Rectangle var1) {
        super.setBounds(var1);
        this.cso();
    }

    public void reshape(int var1, int var2, int var3, int var4) {
        super.reshape(var1, var2, var3, var4);
        this.cso();
    }

    public void resize(Dimension var1) {
        super.resize(var1);
        this.cso();
    }

    public void resize(int var1, int var2) {
        super.resize(var1, var2);
        this.cso();
    }

    public void a(SceneObject var1, float var2) {
        this.a(var1, var2, a.ja);
    }

    public void b(SceneObject var1, float var2) {
        this.a(var1, var2, a.jb);
    }

    public void c(SceneObject var1, float var2) {
        this.a(var1, var2, a.jc);
    }

    private void a(SceneObject var1, float var2, a var3) {
        if (var1 != null) {
            this.gsY = new OrbitalCamera();
            this.gsY.setTarget(var1);
            this.gsY.setAspect((float) this.gsU / (float) this.gsV);
            this.sceneView.setViewport(0, 0, this.gsU, this.gsV);
            float var4 = var1.computeLocalSpaceAABB().length();
            double var5 = this.sceneView.getDistanceForWidthSize((double) var4, (float) Math.min(this.getWidth(), this.getHeight()));
            float var7;
            if (var3 == a.ja) {
                var7 = var1.getAABB().dio() / 2.0F;
                this.gsY.setDistance(new Vector3dOperations(var5 + (double) var7, 0.0D, 0.0D));
            } else if (var3 == a.jb) {
                var7 = var1.getAABB().dip() / 2.0F;
                this.gsY.setDistance(new Vector3dOperations(0.0D, var5 + (double) var7, 0.0D));
            } else {
                var7 = var1.getAABB().diq() / 2.0F;
                this.gsY.setDistance(new Vector3dOperations(0.0D, 0.0D, var5 + (double) var7));
            }

            this.n(var1);
            this.sceneView.setCamera(this.gsY);
            this.gsX = var2;
        }

    }

    public void csp() {
        RLight var1 = new RLight();
        var1.setMainLight(true);
        var1.setType(LightType.DIRECTIONAL_LIGHT);
        this.getScene().addChild(var1);
        var1.setDirection(new Vec3f(0.0F, 0.0F, 1.0F));
        RSceneAreaInfo var2 = new RSceneAreaInfo();
        var2.setFogStart(10000.0F);
        var2.setFogEnd(100000.0F);
        var2.setAmbientColor(new Color(0.3F, 0.3F, 0.3F, 1.0F));
        var2.setFogColor(new Color(0.0F, 0.0F, 0.0F, 0.0F));
        var2.setName("Areainfo for all 3D GUI component");
        var2.setSize(new Vector3dOperations(10000.0D, 10000.0D, 10000.0D));
    }

    public OrbitalCamera csq() {
        return this.gsY;
    }

    public void csr() {
        this.gtb = true;
        this.cso();
    }

    private static enum a {
        ja,
        jb,
        jc;
    }
}
