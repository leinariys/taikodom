package all;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

public class kz extends awd {
    private b.a auC;
    private dm_q auD;
    private int[] auE;
    private int[] auF;
    private boolean auG;

    public kz(ServerSocketChannel var1, b.a var2, dm_q var3, int[] var4, int[] var5) {
        this(var1, var2, var3, var4, var5, true);
    }

    public kz(ServerSocketChannel var1, b.a var2, dm_q var3, int[] var4, int[] var5, boolean var6) {
        super(var1);
        this.auG = true;
        this.auC = var2;
        this.auD = var3;
        this.auG = var6;
        if (var4 != null && var4.length == 4) {
            this.auE = var4;
        } else {
            this.auE = null;
        }

        if (var5 != null && var5.length == 4) {
            this.auF = var5;
        } else {
            this.auF = null;
        }

    }

    public kz(ServerSocketChannel var1, b.a var2, dm_q var3) {
        super(var1);
        this.auG = true;
        this.auC = var2;
        this.auD = var3;
        this.auE = null;
        this.auF = null;
    }

    public dm_q KB() {
        return this.auD;
    }

    public void a(dm_q var1) {
        this.auD = var1;
    }

    protected yV a(SocketChannel var1) {
        ha var2 = mP.a(this.auC, var1.socket().getInetAddress());
        var2.setInfo(var1.toString());
        LZ var3 = new LZ(var2, this.auD, var1, this.gLA.socket().getLocalPort(), this.auG);
        var2.x(var3);
        return var3;
    }

    protected void KC() throws IOException {
        SocketChannel var1 = this.gLA.accept();
        InetSocketAddress var2 = (InetSocketAddress) var1.socket().getRemoteSocketAddress();
        if (this.auE == null && this.auF == null) {
            var1.configureBlocking(false);
            this.yQ.a((XF_w) this.a(var1));
        } else {
            byte[] var3 = var2.getAddress().getAddress();

            for (int var4 = 0; var4 < 4; ++var4) {
                if ((var3[var4] & this.auF[var4]) != (this.auE[var4] & this.auF[var4])) {
                    var1.close();
                    return;
                }
            }

            var1.configureBlocking(false);
            this.yQ.a((XF_w) this.a(var1));
        }
    }

    public int[] KD() {
        return this.auE;
    }

    public int[] KE() {
        return this.auF;
    }
}
