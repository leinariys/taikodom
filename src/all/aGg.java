package all;

public class aGg {
    protected static final int hOv = 0;
    protected static final int hOw = 1;
    protected static final int hOx = 2;
    protected static final int hOy = 0;
    protected static final int hOz = 1;
    protected static final int hOA = 2;
    protected static final int hOB = 3;
    protected static final int hOC = 4;
    protected static final int hOD = 5;
    protected static final int hOE = 6;
    public static aGg hOF = new aGg(0, 0);
    public static aGg hOG = new aGg(0, 1);
    public static aGg hOH = new aGg(1, 1);
    public static aGg hOI = new aGg(2, 1);
    public static aGg hOJ = new aGg(0, 4);
    public static aGg hOK = new aGg(1, 4);
    public static aGg hOL = new aGg(2, 4);
    public static aGg hOM = new aGg(0, 5);
    public static aGg hON = new aGg(1, 5);
    public static aGg hOO = new aGg(2, 5);
    public static aGg hOP = new aGg(0, 6);
    public static aGg hOQ = new aGg(1, 6);
    public static aGg hOR = new aGg(2, 6);
    private final int type;
    private final int hOS;
    private final float hOT;

    protected aGg() {
        this(1, 0);
    }

    protected aGg(int var1) {
        this(var1, 0);
    }

    public aGg(int var1, int var2) {
        this(var1, var2, 1.0D);
    }

    public aGg(int var1, int var2, double var3) {
        this.type = var1;
        this.hOS = var2;
        this.hOT = (float) var3;
    }

    public aGg(aGg var1, double var2) {
        this(var1.type, var1.hOS, var2);
    }

    public int aD(int var1, int var2) {
        if (var1 > 0 && var2 > 0) {
            if (var1 >= var2) {
                return var2;
            } else {
                float var3 = (float) var1 / (float) var2;
                float var4;
                switch (this.type) {
                    case 0:
                        var4 = this.mE(var3);
                        break;
                    case 1:
                        var4 = 1.0F - this.mE(1.0F - var3);
                        break;
                    case 2:
                        if (var3 < 0.5F) {
                            var4 = this.mE(2.0F * var3) / 2.0F;
                        } else {
                            var4 = 1.0F - this.mE(2.0F - 2.0F * var3) / 2.0F;
                        }
                        break;
                    default:
                        var4 = var3;
                }

                if (this.hOT != 1.0F) {
                    var4 = this.hOT * var4 + (1.0F - this.hOT) * var3;
                }

                return Math.round(var4 * (float) var2);
            }
        } else {
            return 0;
        }
    }

    protected float mE(float var1) {
        float var2;
        float var3;
        switch (this.hOS) {
            case 0:
            default:
                return var1;
            case 1:
                return var1 * var1;
            case 2:
                return var1 * var1 * var1;
            case 3:
                var2 = var1 * var1;
                return var2 * var2;
            case 4:
                var2 = var1 * var1;
                return var2 * var2 * var1;
            case 5:
                var2 = var1 * var1;
                var3 = var2 * var1;
                return var3 + var2 - var1;
            case 6:
                var2 = var1 * var1;
                var3 = var2 * var1;
                float var4 = var2 * (2.0F * var3 + var2 - 4.0F * var1 + 2.0F);
                float var5 = (float) (-Math.sin((double) var1 * 3.5D * 3.141592653589793D));
                return var4 * var5;
        }
    }
}
