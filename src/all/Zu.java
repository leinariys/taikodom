package all;

import java.util.Collection;
import java.util.Iterator;

public abstract class Zu {
    protected static final String eNB = "";
    protected aMQ_q eNC = new aMQ_q();

    /**
     * Добавить в список
     *
     * @param var1 Привер  endVideo из аддона SplashScreen
     * @param var2 Привер ExclusiveVideoPlayer
     * @param var3 Обработчик события
     */
    public void a(String var1, Class var2, aVH var3) {
        if (var3 == null) {
            throw new NullPointerException("Listener can not be null");
        } else {
            this.eNC.put(new aMapse(var1, var2), var3);
        }
    }

    /**
     * Добавить в список
     *
     * @param var1 Привер ExclusiveVideoPlayer
     * @param var2 Обработчик события
     */
    public void a(Class var1, aVH var2) {
        this.a("", var1, var2);
    }

    public void a(aVH var1) {
        this.eNC.aQ(var1);
    }

    public void b(Class var1, aVH var2) {
        this.b("", var1, var2);
    }

    public void b(String var1, Class var2, aVH var3) {
        this.eNC.k(new aMapse(var1, var2), var3);
    }

    protected void l(String var1, Object var2) {
        try {
            Collection var3 = this.eNC.aR(new aMapse(var1, var2.getClass()));
            if (var3 != null) {
                Iterator var5 = var3.iterator();

                while (var5.hasNext()) {
                    aVH var4 = (aVH) var5.next();
                    var4.k(var1, var2);
                }
            }
        } catch (RuntimeException var6) {
            var6.printStackTrace();
        }

    }

    protected void m(String var1, Object var2) {
        Collection var3 = this.eNC.aR(new aMapse(var1, var2.getClass()));
        if (var3 != null) {
            Iterator var5 = var3.iterator();

            while (var5.hasNext()) {
                aVH var4 = (aVH) var5.next();

                try {
                    var4.f(var1, var2);
                } catch (RuntimeException var7) {
                    var7.printStackTrace();
                }
            }
        }

    }

    /**
     * Создание строки ключ = значение
     */
    protected class aMapse {
        /**
         * Ключ
         */
        private Object key;
        /**
         * Значение
         */
        private Class clazz;

        /**
         * Создание строки ключ = значение
         *
         * @param var1 Пример endVideo из аддона SplashScreen
         * @param var2 Привер class taikodom.render.ExclusiveVideoPlayer
         */
        public aMapse(Object var1, Class var2) {
            this.key = var1;
            this.clazz = var2;
        }

        public boolean equals(Object var1) {
            aMapse var2 = (aMapse) var1;
            return this.key.equals(var2.key) && this.clazz.equals(var2.clazz);
        }

        public int hashCode() {
            return this.key.hashCode() * 31 + this.clazz.hashCode();
        }

        public Object getKey() {
            return this.key;
        }

        public Class getClazz() {
            return this.clazz;
        }
    }
}
