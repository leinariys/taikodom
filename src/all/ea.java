package all;

import org.apache.commons.logging.Log;
import taikodom.infra.comm.transport.msgs.TransportCommand;
import taikodom.infra.comm.transport.msgs.TransportResponse;

import java.io.EOFException;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;

public class ea implements QF {
    private static final Log ecg = LogPrinter.K(a.class);
    private static final Log ech = LogPrinter.K(c.class);
    static LogPrinter logger = LogPrinter.K(ea.class);
    private final zC anY;
    private final Object ecp;
    public Thread ecn;
    public Thread eco;
    private UV_q eci;
    private aGP ecj;
    private ArrayBlockingQueue eck;
    private Map ecl = new HashMap();
    private Map ecm = new HashMap();
    private ThreadLocal ecq = new ThreadLocal() {
        protected Integer initialValue() {
            return ea.this.bsv();
        }
    };
    private Fh_q ecr;
    private boolean disposed;
    private boolean ecs;

    public ea(zC var1, Object var2, UV_q var3, aGP var4) {
        this.anY = var1;
        this.ecp = var2;
        this.eci = var3;
        this.ecj = var4;
        this.eck = new ArrayBlockingQueue(131072);
        PoolThread.b(new a((a) null), "Distribuitor Transport Reader " + this.hashCode());
        PoolThread.b(new c((c) null), "Distribuitor Transport Writer " + this.hashCode());
    }

    public static xu_q a(aMf var0) throws EOFException {
        int var1 = var0.a(2);
        Object var2;
        switch (var1) {
            case 0:
                var2 = new TransportCommand(var0);
                break;
            case 1:
                var2 = new TransportResponse(var0);
                break;
            default:
                throw new IllegalArgumentException();
        }

        return (xu_q) var2;
    }

    public Object bst() {
        return this.ecp;
    }

    protected boolean isDisposed() {
        return this.disposed;
    }

    public void dispose() {
        if (!this.disposed) {
            logger.info("disposing transport");
            this.disposed = true;
            if (this.ecn != null) {
                this.ecn.interrupt();
                this.ecn = null;
            }

            if (this.eco != null) {
                this.eco.interrupt();
                this.eco = null;
            }

            //   try {
            this.eci.close();
       /*  } catch (IOException var8) {
            ;
         }*/

            // try {
            this.ecj.close();
        /* } catch (IOException var7) {
            ;
         }*/

            Map var1 = this.ecm;
            synchronized (this.ecm) {
                Collection var2 = this.ecm.values();
                Iterator var4 = var2.iterator();

                while (var4.hasNext()) {
                    d var3 = (d) var4.next();
                    synchronized (var3) {
                        var3.notify();
                    }
                }

            }
        }
    }

    public int HX() {
        return ((Integer) this.ecq.get()).intValue();
    }

    private void f(all.b var1, TransportCommand var2) {
        this.ecq.set(var2.getThreadId());
        TransportResponse var3 = this.anY.d(var1, var2);
        if (var3 != null) {
            var3.setMessageId(var2.getMessageId());
            this.c(var1, var3);
        }

    }

    private void g(all.b var1, TransportCommand var2) {
        this.anY.e(var1, var2);
    }

    public d a(all.b var1, TransportCommand var2, boolean var3) {
        return new d(var1, var2, var3);
    }

    public TransportResponse b(all.b var1, TransportCommand var2) {
        d var3 = this.a(var1, var2, true);
        var3.cZz();
        return var3.cZB();
    }

    private void bsu() {
        if (this.isDisposed()) {
            throw new mH("Transport is disposed.");
        }
    }

    public void a(all.b var1, TransportCommand var2) {
        if (this.ecs && !var2.isBlocking()) {
            try {
                if (var2.getType() == 2) {
                    this.ecj.b(var2.getOutBuffer(), 0, var2.getOutLength());
                } else {
                    this.ecj.a(var1, var2.getOutBuffer(), 0, var2.getOutLength());
                }
            } catch (IOException var4) {
                throw new RuntimeException(var4);
            }
        } else {
            d var3 = this.a(var1, var2, false);
            var3.cZz();
        }

    }

    public void c(all.b var1, TransportResponse var2) {
        this.bsu();
        if (this.ecq.get() != null && ((Integer) this.ecq.get()).intValue() != 0) {
            if (this.ecs) {
                try {
                    this.eck.put(new b(var1, var2));
                } catch (InterruptedException var5) {
                    var5.printStackTrace();
                }
            } else {
                try {
                    this.ecj.a(var1, var2.getOutBuffer(), 0, var2.getOutLength());
                } catch (IOException var4) {
                    throw new RuntimeException(var4);
                }
            }

        } else {
            throw new RuntimeException("At this point, threadId should have been be properly set");
        }
    }

    public void HY() {
        Thread var1 = this.ecn;
        Thread var2 = this.eco;
        this.dispose();

        try {
            if (var1 != null) {
                var1.join(1000L);
            }

            if (var2 != null) {
                var2.join(1000L);
            }
        } catch (InterruptedException var4) {
            var4.printStackTrace();
        }

    }

    private int bsv() {
        return (int) (Math.random() * (double) this.bst().hashCode() * (double) Thread.currentThread().hashCode());
    }

    public Object a(aEc var1) {
        return var1;
    }

    public Object a(amr var1) {
        return var1;
    }

    public Fh_q bsw() {
        return this.ecr;
    }

    public void a(Fh_q var1) {
        this.ecr = var1;
    }

    public boolean bsx() {
        return this.ecs;
    }

    public void ak(boolean var1) {
        this.ecs = var1;
    }

    public void b(all.b var1, TransportResponse var2) {
        this.c(var1, var2);
    }

    private class d {
        private final TransportCommand gqU;
        private final all.b dyE;
        private TransportResponse dyr = null;
        private TransportCommand hNS;
        private Thread thread;
        private d hNT = null;
        private boolean hNU = false;
        private all.b gqV;
        private boolean avU;

        public d(all.b var2, TransportCommand var3, boolean var4) {
            this.dyE = var2;
            this.gqU = var3;
            this.avU = var4;
            this.thread = Thread.currentThread();
        }

        public boolean isBlocking() {
            return this.avU;
        }

        public boolean cZw() {
            return this.hNU;
        }

        public Thread getThread() {
            return this.thread;
        }

        public TransportCommand cZx() {
            return this.gqU;
        }

        public TransportResponse cZy() {
            return this.dyr;
        }

        private synchronized void d(all.b var1, TransportResponse var2) {
            this.gqV = var1;
            this.dyr = var2;
            this.notify();
        }

        private synchronized void a(TransportCommand var1) {
            this.hNS = var1;
            this.notify();
        }

        public void cZz() {
            ea.this.bsu();
            if (this.isBlocking() && (ea.this.ecq.get() == null || ((Integer) ea.this.ecq.get()).intValue() != this.gqU.getThreadId())) {
                ea.this.ecq.set(this.gqU.getThreadId());
            }

            if (ea.this.ecr != null) {
                ea.this.ecr.hd(this.gqU.getThreadId());
            }

            try {
                ea.this.eck.put(this);
            } catch (InterruptedException var2) {
                var2.printStackTrace();
            }

        }

        public void cZA() {
            ea.this.bsu();
            this.hNU = true;
            if (this.gqU.getThreadId() == 0) {
                if (ea.this.ecq.get() == null || ((Integer) ea.this.ecq.get()).intValue() == 0) {
                    ea.this.ecq.set(ea.this.bsv());
                }

                this.gqU.setThreadId(((Integer) ea.this.ecq.get()).intValue());
            }

            if (ea.this.ecr != null) {
                ea.this.ecr.hd(this.gqU.getThreadId());
            }

            try {
                ea.this.eck.put(this);
            } catch (InterruptedException var2) {
                var2.printStackTrace();
            }

        }

        public synchronized TransportResponse cZB() {
            while (!this.hasResponse()) {
                if (this.hNS == null && !this.hasResponse()) {
                    if (ea.this.isDisposed()) {
                        throw new RuntimeException("Transport disposed.");
                    }

                    PoolThread.aC(this);
                    if (ea.this.isDisposed()) {
                        throw new RuntimeException("Transport disposed.");
                    }
                }

                if (this.hNS != null) {
                    ea.this.f(this.dyE, this.hNS);
                    this.hNS = null;
                }
            }

            return this.dyr;
        }

        public synchronized boolean hasResponse() {
            return this.dyr != null;
        }

        public d cZC() {
            return this.hNT;
        }

        public void a(d var1) {
            this.hNT = var1;
        }
    }

    private class a implements Runnable {
        private a() {
        }

        // $FF: synthetic method
        a(a var2) {
            this();
        }

        public void run() {
            ea.this.ecn = Thread.currentThread();

            try {
                while (this.isRunning()) {
                    if (ea.ecg.isDebugEnabled()) {
                        ea.ecg.debug("waiting...");
                    }

                    aBB var1 = ea.this.eci.jG();
                    if (var1 != null) {
                        if (ea.ecg.isDebugEnabled()) {
                            ea.ecg.debug("payload:" + var1.cJn());
                        }

                        xu_q var2;
                        try {
                            aMf var3 = new aMf(var1.cJn(), var1.getOffset(), var1.getLength());
                            var2 = ea.a(var3);
                        } catch (Throwable var16) {
                            var16.printStackTrace();
                            continue;
                        }

                        var2.setReceivedTimeMilis(System.currentTimeMillis());
                        if (ea.ecg.isDebugEnabled()) {
                            ea.ecg.debug("incoming msg: " + var2.getThreadId() + " - " + var2.getClass().getName());
                        }

                        try {
                            if (var2 instanceof TransportResponse) {
                                this.a(var1.cJl(), (TransportResponse) var2);
                            } else if (var2 instanceof TransportCommand) {
                                this.c(var1.cJl(), (TransportCommand) var2);
                            }
                        } catch (RuntimeException var15) {
                            var15.printStackTrace();
                        }
                    }
                }
            } catch (RuntimeException var17) {
                ea.ecg.warn("transport reader interrupted");
            } /*catch (EOFException var18) {
            if (this.isRunning()) {
               var18.printStackTrace();
            }
         } catch (IOException var19) {
            if (this.isRunning()) {
               var19.printStackTrace();
            }
         } catch (ClassNotFoundException var20) {
            if (this.isRunning()) {
               var20.printStackTrace();
            }
         } catch (InterruptedException var21) {
            ea.logger.info("Transport interrupted");
         }*/ catch (Exception var23) {
                ea.ecg.error("Uncaught exception: ", var23);
            } finally {
                ea.ecg.info("stopped");
                ea.this.dispose();
            }

        }

        private boolean isRunning() {
            return ea.this.ecn == Thread.currentThread();
        }

        private void a(all.b var1, TransportResponse var2) {
            synchronized (ea.this.ecm) {
                d var4 = (d) ea.this.ecm.remove(var2.getThreadId());
                if (var4 == null) {
                    ea.this.ecq.set(Integer.valueOf(0));
                } else {
                    d var5 = var4.cZC();
                    if (var5 == null) {
                        ea.this.ecq.set(Integer.valueOf(0));
                    } else {
                        ea.this.ecm.put(var2.getThreadId(), var5);
                    }
                }
            }

            d var3;
            synchronized (ea.this.ecl) {
                var3 = (d) ea.this.ecl.remove(var2.getMessageId());
                ea.this.ecl.notify();
            }

            if (var3 == null) {
                throw new RuntimeException("No envelope found for messageId " + var2.getMessageId() + "\n" + var2);
            } else {
                var3.d(var1, var2);
            }
        }

        private void c(all.b var1, TransportCommand var2) {
            d var3 = null;
            if (var2.isBlocking()) {
                synchronized (ea.this.ecm) {
                    if (ea.this.ecm.containsKey(var2.getThreadId())) {
                        if (ea.ecg.isDebugEnabled()) {
                            ea.ecg.debug("Thread " + var2.getThreadId() + " has WinInet waiting call, using the same thread");
                        }

                        var3 = (d) ea.this.ecm.get(var2.getThreadId());
                    }
                }
            }

            if (var3 == null) {
                ea.this.g(var1, var2);
            } else {
                var3.a(var2);
            }

        }
    }

    public class b {
        all.b dyq;
        TransportResponse dyr;

        public b(all.b var2, TransportResponse var3) {
            this.dyq = var2;
            this.dyr = var3;
        }
    }

    private class c implements Runnable {
        private c() {
        }

        // $FF: synthetic method
        c(c var2) {
            this();
        }

        public void run() {
            ea.this.eco = Thread.currentThread();

            try {
                List var1 = aKU.dgK();

                while (this.isRunning()) {
                    ea.ech.debug("waiting...");
                    var1.clear();
                    if (ea.this.eck.size() > 0) {
                        ea.this.eck.drainTo(var1);
                    } else {
                        var1.add(ea.this.eck.take());
                    }

                    Iterator var3 = var1.iterator();

                    while (var3.hasNext()) {
                        Object var2 = var3.next();
                        if (var2 instanceof d) {
                            d var4 = (d) var2;
                            TransportCommand var5 = var4.cZx();
                            if (var4.isBlocking()) {
                                synchronized (ea.this.ecl) {
                                    ea.this.ecl.put(var5.getMessageId(), var4);
                                }

                                synchronized (ea.this.ecm) {
                                    if (ea.this.ecm.containsKey(var5.getThreadId())) {
                                        var4.a((d) ea.this.ecm.get(var5.getThreadId()));
                                    }

                                    ea.this.ecm.put(var5.getThreadId(), var4);
                                }
                            }

                            try {
                                if (var4.cZw()) {
                                    ea.this.ecj.b(var5.getOutBuffer(), 0, var5.getOutLength());
                                } else {
                                    ea.this.ecj.a(var4.dyE, var5.getOutBuffer(), 0, var5.getOutLength());
                                }
                            } catch (IOException var17) {
                                ea.logger.warn("Error writting message", var17);
                            }
                        } else if (var2 instanceof b) {
                            b var26 = (b) var2;
                            TransportResponse var27 = var26.dyr;
                            ea.this.ecj.a(var26.dyq, var27.getOutBuffer(), 0, var27.getOutLength());
                        }
                    }
                }
            } catch (RuntimeException var20) {
                ea.ech.warn("queue disposed");
            } catch (IOException var22) {
                var22.printStackTrace();
            } catch (InterruptedException var23) {
                ea.logger.warn("writer interrupted");
                ea.this.dispose();
            } catch (Exception var24) {
                ea.ecg.error("Uncaught exception: ", var24);
            } finally {
                ea.ech.info("stopped");
                ea.this.dispose();
            }

        }

        private boolean isRunning() {
            return ea.this.eco == Thread.currentThread();
        }
    }
}
