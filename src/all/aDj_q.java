package all;

public class aDj_q {
    private int[] hyt = new int[16];
    private int size;

    public void add(int var1) {
        if (this.size == this.hyt.length) {
            this.expand();
        }

        this.hyt[this.size++] = var1;
    }

    private void expand() {
        int[] var1 = new int[this.hyt.length << 1];
        System.arraycopy(this.hyt, 0, var1, 0, this.hyt.length);
        this.hyt = var1;
    }

    public int remove(int var1) {
        if (var1 >= this.size) {
            throw new IndexOutOfBoundsException();
        } else {
            int var2 = this.hyt[var1];
            System.arraycopy(this.hyt, var1 + 1, this.hyt, var1, this.size - var1 - 1);
            --this.size;
            return var2;
        }
    }

    public int get(int var1) {
        if (var1 >= this.size) {
            throw new IndexOutOfBoundsException();
        } else {
            return this.hyt[var1];
        }
    }

    public void set(int var1, int var2) {
        if (var1 >= this.size) {
            throw new IndexOutOfBoundsException();
        } else {
            this.hyt[var1] = var2;
        }
    }

    public int size() {
        return this.size;
    }
}
