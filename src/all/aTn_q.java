package all;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class aTn_q extends aOv implements Externalizable {
    private static final long serialVersionUID = 1L;
    private aCE iOx;

    public aTn_q() {
    }

    public aTn_q(Gr var1) {
        this.iOx = (aCE) var1;
    }

    public Xf_q yz() {
        return this.iOx.aQL();
    }

    public Gr dvF() {
        return this.iOx;
    }

    public void readExternal(ObjectInput var1) throws IOException, ClassNotFoundException {
        super.readExternal(var1);
        this.iOx = new aCE();
        try {
            this.iOx.readExternal(var1);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        this.iOx.aQK().pU();
    }

    public void writeExternal(ObjectOutput var1) throws IOException {
        this.iOx.aQK().pV();
        super.writeExternal(var1);
        this.iOx.writeExternal(var1);
    }

    public String toString() {
        return this.getClass().getName() + "-" + this.iOx;
    }

    public akO a(b var1, Jz var2) {
        return ((se_q) this.iOx.aQL().bFf()).a(var1, this);
    }
}
