package all;

/**
 * class CommandTranslatorAddon akH
 */
public class InvalidSectionNameException extends RuntimeException {

    private String entryName;

    public InvalidSectionNameException(String var1) {
        this.entryName = var1;
    }

    public final String getEntryName() {
        return this.entryName;
    }
}
