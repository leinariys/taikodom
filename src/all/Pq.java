package all;

import taikodom.render.DrawContext;
import taikodom.render.graphics2d.DrawNode;
import taikodom.render.graphics2d.RGuiScene;

import java.awt.*;

public class Pq extends DrawNode {
    private final Color color;

    public Pq(Color var1) {
        this.color = var1;
    }

    public void draw(DrawContext var1, RGuiScene var2) {
        var1.getGL().glColor4f((float) this.color.getRed() / 255.0F, (float) this.color.getGreen() / 255.0F, (float) this.color.getBlue() / 255.0F, (float) this.color.getAlpha() / 255.0F);
    }
}
