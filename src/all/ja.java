package all;

/**
 * class CommandTranslatorAddon
 */
public class ja {
    public static int ca(int var0) {
        switch (var0) {
            case 0:
                return -1;
            case 1:
                return 0;
            case 2:
                return 1;
            case 3:
                return 2;
            default:
                throw new RuntimeException("Unknown button: " + var0);
        }
    }

    public static int cb(int var0) {
        switch (var0) {
            case -1:
                return 0;
            case 0:
                return 1;
            case 1:
                return 2;
            case 2:
                return 3;
            default:
                throw new RuntimeException("Unknown button: " + var0);
        }
    }
}
