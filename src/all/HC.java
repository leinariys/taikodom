package all;

import java.awt.*;

public interface HC {
    /**
     * SelectionCursor _enabled
     *
     * @return
     */
    Cursor aRO();

    /**
     * SelectionCursor _combat
     *
     * @return
     */
    Cursor aRP();

    /**
     * HiddenCursor
     *
     * @return
     */
    Cursor aRQ();

    /**
     * SelectionCursor _enabled
     */
    void aRR();

    /**
     * HiddenCursor
     */
    void aRS();

    /**
     * SelectionCursor _combat
     */
    void aRT();
}
