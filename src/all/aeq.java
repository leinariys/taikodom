package all;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * class acs
 */
public class aeq extends WA implements Externalizable {
    private static final long serialVersionUID = 1L;
    private String command;
    private String param;

    public aeq() {
    }

    public aeq(String var1, String var2) {
        this.command = var1;
        this.param = var2;
    }

    public String getEvent() {
        return this.command;
    }

    public String bUu() {
        return this.param;
    }

    public boolean equals(Object var1) {
        if (!(var1 instanceof aeq)) {
            return false;
        } else {
            aeq var2 = (aeq) var1;
            if (this.param != null) {
                if (!this.equals(var2.param)) {
                    return false;
                }
            } else if (var2.param != null) {
                return false;
            }

            if (this.command != null) {
                if (this.equals(var2.command)) {
                    return true;
                }
            } else if (var2.command == null) {
                return true;
            }

            return false;
        }
    }

    public int hashCode() {
        return (this.param != null ? this.param.hashCode() : 0) * 31 + (this.command != null ? this.command.hashCode() : 0);
    }

    public void readExternal(ObjectInput var1) throws IOException, ClassNotFoundException {
        this.command = (String) var1.readObject();
        this.param = (String) var1.readObject();
    }

    public void writeExternal(ObjectOutput var1) throws IOException {
        var1.writeObject(this.command);
        var1.writeObject(this.param);
    }
}
