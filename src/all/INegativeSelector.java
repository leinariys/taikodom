package all;

//package org.w3c.css.sac;
public interface INegativeSelector extends ISimpleSelector {
    ISimpleSelector getSimpleSelector();
}
