package all;

public class BM_q {
    private static final double izm = 0.001D;
    private Vector3dOperations izn = new Vector3dOperations();
    private double izo = 0.0D;

    public void normalize() {
        double var1 = this.izn.lengthSquared();
        if (var1 > 0.0D) {
            double var3 = 1.0D / Math.sqrt(var1);
            this.izn.x *= var3;
            this.izn.y *= var3;
            this.izn.z *= var3;
            this.izo *= var3;
        }

    }

    public a aM(Vector3dOperations var1) {
        double var2 = var1.x * this.izn.x + var1.y * this.izn.y + var1.z * this.izn.z + this.izo;
        if (var2 > 0.001D) {
            return a.cqn;
        } else {
            return var2 < -0.001D ? a.cqo : a.cqm;
        }
    }

    public void f(Vector3dOperations var1, Vector3dOperations var2, Vector3dOperations var3) {
        double var4 = var2.x - var1.x;
        double var6 = var2.z - var1.z;
        double var8 = var2.z - var1.z;
        double var10 = var3.x - var1.x;
        double var12 = var3.z - var1.z;
        double var14 = var3.z - var1.z;
        this.izn.x = var6 * var14 - var8 * var12;
        this.izn.y = var8 * var10 - var4 * var14;
        this.izn.z = var4 * var12 - var6 * var10;
        this.izn.normalize();
        this.izo = -(this.izn.x * var1.x + this.izn.y * var1.y + this.izn.z * var1.z);
    }

    public void a(BM_q var1) {
        this.izn.aA(var1.izn);
        this.izo = var1.izo;
    }

    public void b(aTw var1) {
        this.izn.aA(var1.vO().dfR());
        this.izo = (double) var1.getDistance();
    }

    public void setNormal(double var1, double var3, double var5) {
        this.izn.set(var1, var3, var5);
    }

    public Vector3dOperations getNormal() {
        return this.izn;
    }

    public void setNormal(Vector3dOperations var1) {
        this.izn.aA(var1);
    }

    public void ac(double var1) {
        this.izo = var1;
    }

    public double dnz() {
        return this.izo;
    }

    public void c(double var1, double var3, double var5, double var7) {
        this.izn.x = var1;
        this.izn.y = var3;
        this.izn.z = var5;
        this.izo = var7;
    }

    public double aN(Vector3dOperations var1) {
        return var1.x * this.izn.x + var1.y * this.izn.y + var1.z * this.izn.z + this.izo;
    }

    public String toString() {
        return "Normal: " + this.izn + ", CreateJComponent:" + this.izo;
    }

    public static enum a {
        cqm,
        cqn,
        cqo;
    }
}
