package all;

import taikodom.render.RenderView;
import taikodom.render.SceneView;
import taikodom.render.helpers.RenderTask;
import taikodom.render.scene.SSoundSource;
import taikodom.render.scene.Scene;
import taikodom.render.scene.SoundObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Озвучка интерфейса
 */
public class Ix implements Ma {
    private static final float dfE = 100.0F;
    private static final String dfF = "data/audio/interface/interface_sfx.pro";
    private static final LogPrinter logger = LogPrinter.setClass(Ix.class);
    private static Ix dfG;
    private static Map dfH = new HashMap();
    private SceneView dfI;
    /**
     * графического движка
     */

    private EngineGraphics engineGraphics = acs.ald().getEngineGraphics();

    private Ix() {
        if (this.engineGraphics != null) {
            this.engineGraphics.bV(dfF);
            this.dfI = new SceneView();
            this.dfI.setClearColorBuffer(false);
            this.dfI.setClearDepthBuffer(false);
            this.dfI.setClearStencilBuffer(false);
            this.engineGraphics.a(this.dfI);
        }

    }

    /**
     * СОздать экземпляр класса озвучки интерфейса
     *
     * @return
     */
    public static Ix aWp() {
        if (dfG == null) {
            dfG = new Ix();
        }
        return dfG;
    }

    public void A(String var1, String var2) {
        ILoaderTrail var3 = acs.ald().getLoaderTrail();
        var3.a(var1, var2, (Scene) null, new anC(), "Sound player impl");
    }
/*
   public SoundObject as(tC var1) {
      if (this.engineGraphics == null) {
         return null;
      } else {
         return var1 != null ? this.B(var1.getFile(), var1.getHandle()) : null;
      }
   }*/

    public SoundObject B(String var1, String var2) {
        if (this.engineGraphics == null) {
            return null;
        } else {
            if (var1 != null) {
                acs.ald().getLoaderTrail().a(var1, (String) null, (Scene) null, (NJ) null, "SoundPlayer");
            }

            return this.eC(var2);
        }
    }

    private SoundObject j(String var1, final boolean var2) {
        if (this.engineGraphics == null) {
            return null;
        } else if (this.engineGraphics.aeg() <= 0.0F) {
            return null;
        } else {
            SoundObject var3 = (SoundObject) dfH.get(var1);
            if (var3 == null) {
                var3 = (SoundObject) this.engineGraphics.bT(var1);
                if (var3 == null) {
                    logger.warn("Failed to create " + var1);
                    return null;
                }

                if (var3 instanceof SSoundSource) {
                    ((SSoundSource) var3).setDisposeAfterPlaying(false);
                }

                dfH.put(var1, var3);
            }

            var3.setGain(this.engineGraphics.aeg());
            SoundObject finalVar = var3;
            RenderTask var6 = new RenderTask() {
                public void run(RenderView var1) {
                    Scene var2x = Ix.this.dfI.getScene();
                    if (var2) {
                        if (var2x.hasChild(finalVar)) {
                            finalVar.play();
                        } else {
                            finalVar.play();
                            var2x.addChild(finalVar);
                        }
                    } else if (var2x.hasChild(finalVar)) {
                        if (!finalVar.isPlaying() || finalVar.getSamplePosition() > dfE) {
                            SSoundSource var3x = (SSoundSource) finalVar.cloneAsset();
                            var3x.setGain(Ix.this.engineGraphics.aeg());
                            var3x.play();
                            var2x.addChild(var3x);
                            Ix.dfH.put(var3x.getName(), var3x);
                            finalVar.fadeOut(0.05F);
                        }
                    } else {
                        finalVar.play();
                        var2x.addChild(finalVar);
                    }

                }
            };
            this.engineGraphics.a(var6);
            return var3;
        }
    }

    public SoundObject eC(String var1) {
        return this.j(var1, true);
    }

    public SoundObject cx(String var1) {
        return this.e(var1, true);
    }

    public SoundObject C(String var1, String var2) {
        return this.B(var2, var1);
    }

    public SoundObject e(String var1, boolean var2) {
        return var1 != null && !"".equals(var1) ? this.j(var1, var2) : null;
    }
}
