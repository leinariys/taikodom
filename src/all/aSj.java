package all;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * http запросы
 * class LoginAddon
 */
public class aSj {
    public static String a(final String var0, final String var1, File var2) {
        final Semaphore var3 = new Semaphore(1);
        final String[] var4 = new String[1];
        String var5 = bd(var0, var1);
        File var6 = new File(var2, aK.w(var5) + ".cache");

        try {
            var3.acquire(1);
            Thread var7 = new Thread() {
                public void run() {
                    var4[0] = aSj.S(var0, var1);
                    var3.release();
                }
            };
            var7.setDaemon(true);
            var7.start();
            if (var3.tryAcquire(5L, TimeUnit.SECONDS) && var4[0] != null) {
                try {
                    var2.mkdirs();
                    Hm.b(var6, var4[0]);
                } catch (Exception var9) {
                    var9.printStackTrace();
                }

                return var4[0];
            } else {
                return Hm.a(var6, Charset.forName("utf-8"));
            }
        } catch (Exception var10) {
            var10.printStackTrace();
            return null;
        }
    }

    public static String S(String var0, String var1) {
        String var2 = null;
        if (var0.startsWith("http://")) {
            try {
                String var3 = bd(var0, var1);
                URL var4 = new URL(var3);
                URLConnection var5 = var4.openConnection();
                BufferedReader var6 = new BufferedReader(new InputStreamReader(var5.getInputStream()));
                StringBuffer var7 = new StringBuffer();

                String var8;
                while ((var8 = var6.readLine()) != null) {
                    var7.append(var8);
                }

                var6.close();
                var2 = var7.toString();
                switch (((HttpURLConnection) var5).getResponseCode()) {
                    case 404:
                    case 503:
                        return null;
                }
            } catch (Exception var9) {
                var9.printStackTrace();
            }
        }

        return var2;
    }

    private static String bd(String var0, String var1) {
        new StringBuffer();
        String var2 = var0;
        if (var1 != null && var1.length() > 0) {
            var2 = var0 + "?" + var1;
        }

        return var2;
    }

    public static void a(Reader var0, URL var1, Writer var2) throws Exception {
        HttpURLConnection var3 = null;

        try {
            var3 = (HttpURLConnection) var1.openConnection();

            try {
                var3.setRequestMethod("POST");
            } catch (ProtocolException var32) {
                throw new Exception("Shouldn't happen: HttpURLConnection doesn't support POST??", var32);
            }

            var3.setDoOutput(true);
            var3.setDoInput(true);
            var3.setUseCaches(false);
            var3.setAllowUserInteraction(false);
            var3.setRequestProperty("Content-type", "text/xml; charset=UTF-8");
            OutputStream var4 = var3.getOutputStream();

            try {
                OutputStreamWriter var5 = new OutputStreamWriter(var4, "UTF-8");
                a(var0, var5);
                var5.close();
            } catch (IOException var31) {
                throw new Exception("IOException while posting data", var31);
            } finally {
                if (var4 != null) {
                    var4.close();
                }

            }

            InputStream var37 = var3.getInputStream();

            try {
                InputStreamReader var6 = new InputStreamReader(var37);
                a(var6, var2);
                var6.close();
            } catch (IOException var30) {
                throw new Exception("IOException while reading response", var30);
            } finally {
                if (var37 != null) {
                    var37.close();
                }
            }
        } catch (IOException var35) {
            throw new Exception("Connection error (is server running at " + var1 + " ?): " + var35);
        } finally {
            if (var3 != null) {
                var3.disconnect();
            }
        }
    }

    private static void a(Reader var0, Writer var1) throws IOException {
        char[] var2 = new char[1024];
        boolean var3 = false;

        int var4;
        while ((var4 = var0.read(var2)) >= 0) {
            var1.write(var2, 0, var4);
        }
        var1.flush();
    }
}
