package all;

import com.hoplon.geometry.Vec3f;

public interface akr {
    void reset();

    void p(Vec3f var1, Vec3f var2, Vec3f var3);

    boolean aK(Vec3f var1);

    float cgi();

    boolean cgj();

    int a(Vec3f[] var1, Vec3f[] var2, Vec3f[] var3);

    boolean aL(Vec3f var1);

    void aM(Vec3f var1);

    boolean cgk();

    void D(Vec3f var1, Vec3f var2);

    int numVertices();
}
