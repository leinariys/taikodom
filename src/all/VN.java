package all;

import java.io.*;
import java.net.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

/**
 * class akH
 */
public class VN {
    private static Random random = new Random();
    URLConnection urlConnection;
    OutputStream os;
    Map evI;
    String evJ;

    public VN(URLConnection var1) {
        this.os = null;
        this.evI = new HashMap();
        this.evJ = "---------------------------" + bCQ() + bCQ() + bCQ();
        this.urlConnection = var1;
        var1.setDoOutput(true);
        var1.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + this.evJ);
    }

    public VN(URL url) throws IOException {
        this(url.openConnection());
    }

    public VN(String var1) throws IOException {
        this(new URL(var1));
    }

    protected static String bCQ() {
        return Long.toString(random.nextLong(), 36);
    }

    private static void getInputStream(InputStream var0, OutputStream var1) throws IOException {
        byte[] var2 = new byte[500000];
        int var4 = 0;
        int var3;
        synchronized (var0) {
            while ((var3 = var0.read(var2, 0, var2.length)) >= 0) {
                var1.write(var2, 0, var3);
                var4 += var3;
            }
        }

        var1.flush();
        var2 = (byte[]) null;
    }

    public static InputStream getInputStream(URL url, Map params) throws IOException {
        return (new VN(url)).setParam(params);
    }

    public static InputStream getInputStream(URL url, Object[] params) throws IOException {
        return (new VN(url)).setParam(params);
    }

    public static InputStream getInputStream(URL var0, Map var1, Map var2) throws IOException {
        return (new VN(var0)).getInputStream(var1, var2);
    }

    public static InputStream getInputStream(URL var0, String[] var1, Object[] var2) throws IOException {
        return (new VN(var0)).getInputStream(var1, var2);
    }

    public static InputStream getInputStream(URL var0, String var1, Object var2) throws IOException {
        return (new VN(var0)).s(var1, var2);
    }

    public static InputStream getInputStream(URL var0, String var1, Object var2, String var3, Object var4) throws IOException {
        return (new VN(var0)).getInputStream(var1, var2, var3, var4);
    }

    public static InputStream getInputStream(URL var0, String var1, Object var2, String var3, Object var4, String var5, Object var6) throws IOException {
        return (new VN(var0)).getInputStream(var1, var2, var3, var4, var5, var6);
    }

    public static InputStream getInputStream(URL var0, String var1, Object var2, String var3, Object var4, String var5, Object var6, String var7, Object var8) throws IOException {
        return (new VN(var0)).getInputStream(var1, var2, var3, var4, var5, var6, var7, var8);
    }

    public static String execute(String url, Object[] params) {
        StringBuilder result = new StringBuilder();

        try {
            InputStream inputStream = getInputStream(new URL(url), params);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            String resultBuffer;
            while ((resultBuffer = bufferedReader.readLine()) != null) {
                result.append(decode(resultBuffer.trim()));
            }
        } catch (MalformedURLException var6) {
            ;
        } catch (IOException var7) {
            ;
        }

        return result.toString();
    }

    public static String encode(String var0) {
        return URLEncoder.encode(var0);
    }

    public static String decode(String var0) throws UnsupportedEncodingException {
        return URLDecoder.decode(var0, "UTF-8");
    }

    public static String S(String var0, String var1) {
        String var2 = null;
        if (var0.startsWith("http://")) {
            try {
                new StringBuffer();
                String var3 = var0;
                if (var1 != null && var1.length() > 0) {
                    var3 = var0 + "?" + var1;
                }

                URL var4 = new URL(var3);
                URLConnection var5 = var4.openConnection();
                BufferedReader var6 = new BufferedReader(new InputStreamReader(var5.getInputStream()));
                StringBuffer var7 = new StringBuffer();

                String var8;
                while ((var8 = var6.readLine()) != null) {
                    var7.append(var8);
                }

                var6.close();
                var2 = var7.toString();
            } catch (Exception var10) {
                ;
            }
        }

        try {
            var2 = decode(var2);
        } catch (UnsupportedEncodingException var9) {
            ;
        }

        return var2;
    }

    public static String h(String var0, Object[] var1) {
        String var2 = "";

        // try {
        for (int var3 = 0; var3 < var1.length - 1; var3 += 2) {
            if (var2.length() > 0) {
                var2 = var2 + "&";
            }

            var2 = var2 + var1[var3].toString() + "=" + encode((String) var1[var3 + 1]);
        }
     /* } catch (UnsupportedEncodingException var4) {
         ;
      }*/

        return S(var0, var2);
    }

    protected void connect() throws IOException {
        if (this.os == null) {
            this.os = this.urlConnection.getOutputStream();
        }

    }

    protected void write(char var1) throws IOException {
        this.connect();
        this.os.write(var1);
    }

    protected void write(String var1) throws IOException {
        this.connect();
        this.os.write(var1.getBytes());
    }

    protected void bCP() throws IOException {
        this.connect();
        this.write("\r\n");
    }

    protected void writeln(String var1) throws IOException {
        this.connect();
        this.write(var1);
        this.bCP();
    }

    private void bCR() throws IOException {
        this.write("--");
        this.write(this.evJ);
    }

    private void bCS() {
        StringBuffer var1 = new StringBuffer();
        Iterator var2 = this.evI.entrySet().iterator();

        while (var2.hasNext()) {
            Entry var3 = (Entry) var2.next();
            var1.append(var3.getKey().toString() + "=" + var3.getValue());
            if (var2.hasNext()) {
                var1.append("; ");
            }
        }

        if (var1.length() > 0) {
            this.urlConnection.setRequestProperty("Cookie", var1.toString());
        }

    }

    public void R(String var1, String var2) {
        this.evI.put(var1, var2);
    }

    public void r(Map var1) {
        if (var1 != null) {
            this.evI.putAll(var1);
        }
    }

    public void i(String[] var1) {
        if (var1 != null) {
            for (int var2 = 0; var2 < var1.length - 1; var2 += 2) {
                this.R(var1[var2], var1[var2 + 1]);
            }

        }
    }

    private void hq(String var1) throws IOException {
        this.bCP();
        this.write("Content-Disposition: form-data; name=\"");
        this.write(var1);
        this.write('"');
    }

    public void setParameter(String var1, String var2) throws IOException {
        this.bCR();
        this.hq(encode(var1));
        this.bCP();
        this.bCP();
        this.writeln(encode(var2));
    }

    public void getInputStream(String var1, String var2, InputStream var3) throws IOException {
        this.bCR();
        this.hq(var1);
        this.write("; filename=\"");
        this.write(var2);
        this.write('"');
        this.bCP();
        this.write("Content-Type: ");
        String var4 = URLConnection.guessContentTypeFromName(var2);
        if (var4 == null) {
            var4 = "application/octet-stream";
        }

        this.writeln(var4);
        this.bCP();
        getInputStream(var3, this.os);
        this.bCP();
    }

    public void getInputStream(String var1, File var2) throws IOException {
        this.getInputStream((String) var1, (String) var2.getPath(), (InputStream) (new FileInputStream(var2)));
    }

    public void setParameter(String var1, Object var2) throws IOException {
        if (var2 instanceof File) {
            this.getInputStream(var1, (File) var2);
        } else {
            this.setParameter(var1, var2.toString());
        }

    }

    public void s(Map var1) throws IOException {
        if (var1 != null) {
            Iterator var2 = var1.entrySet().iterator();

            while (var2.hasNext()) {
                Entry var3 = (Entry) var2.next();
                this.setParameter(var3.getKey().toString(), var3.getValue());
            }

        }
    }

    public void setParameters(Object[] var1) throws IOException {
        if (var1 != null) {
            for (int var2 = 0; var2 < var1.length - 1; var2 += 2) {
                this.setParameter(var1[var2].toString(), var1[var2 + 1]);
            }

        }
    }

    public InputStream bCT() throws IOException {
        this.bCR();
        this.writeln("--");
        this.os.close();
        return this.urlConnection.getInputStream();
    }

    public InputStream setParam(Map var1) throws IOException {
        this.s(var1);
        return this.bCT();
    }

    public InputStream setParam(Object[] params) throws IOException {
        this.setParameters(params);
        return this.bCT();
    }

    public InputStream getInputStream(Map var1, Map var2) throws IOException {
        this.r(var1);
        this.s(var2);
        return this.bCT();
    }

    public InputStream getInputStream(String[] var1, Object[] var2) throws IOException {
        this.i(var1);
        this.setParameters(var2);
        return this.bCT();
    }

    public InputStream s(String var1, Object var2) throws IOException {
        this.setParameter(var1, var2);
        return this.bCT();
    }

    public InputStream getInputStream(String var1, Object var2, String var3, Object var4) throws IOException {
        this.setParameter(var1, var2);
        return this.s(var3, var4);
    }

    public InputStream getInputStream(String var1, Object var2, String var3, Object var4, String var5, Object var6) throws IOException {
        this.setParameter(var1, var2);
        return this.getInputStream(var3, var4, var5, var6);
    }

    public InputStream getInputStream(String var1, Object var2, String var3, Object var4, String var5, Object var6, String var7, Object var8) throws IOException {
        this.setParameter(var1, var2);
        return this.getInputStream(var3, var4, var5, var6, var7, var8);
    }
}
