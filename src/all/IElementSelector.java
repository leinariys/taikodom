package all;

//package org.w3c.css.sac;
public interface IElementSelector extends ISimpleSelector {
    String getNamespaceURI();

    String getLocalName();
}
