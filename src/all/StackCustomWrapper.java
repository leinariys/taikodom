package all;

import javax.swing.plaf.ComponentUI;
import java.awt.*;

public class StackCustomWrapper extends BoxCustomWrapper {
    private static final long serialVersionUID = 1L;

    protected Dimension b(int var1, int var2) {
        int var3 = this.getComponentCount();
        int var4 = 0;
        int var5 = 0;

        for (int var6 = 0; var6 < var3; ++var6) {
            Component var7 = this.getComponent(var6);
            if (var7.isVisible()) {
                Dimension var8 = this.d(var7, var1, var2);
                Point var9 = this.a(var7, var1, var2);
                var4 = Math.max(var4, var8.width + var9.x);
                var5 = Math.max(var5, var8.height + var9.y);
            }
        }

        return new Dimension(var4, var5);
    }

    protected Dimension a(int var1, int var2) {
        int var3 = this.getComponentCount();
        int var4 = 0;
        int var5 = 0;

        for (int var6 = 0; var6 < var3; ++var6) {
            Component var7 = this.getComponent(var6);
            if (var7.isVisible()) {
                Dimension var8 = this.c(var7, var1, var2);
                Point var9 = this.a(var7, var1, var2);
                var4 = Math.max(var4, var8.width + var9.x);
                var5 = Math.max(var5, var8.height + var9.y);
            }
        }

        return new Dimension(var4, var5);
    }

    public void layout() {
        int var1 = this.getComponentCount();
        Rectangle var2 = this.getBounds();
        PropertiesUiFromCss var3 = PropertiesUiFromCss.a((ComponentUI) this.getUI());
        aRU var4 = var3.aty();
        vI var5 = var3.atR();

        for (int var6 = 0; var6 < var1; ++var6) {
            Component var7 = this.getComponent(var6);
            if (var7.isVisible()) {
                Dimension var8 = this.d(var7, var2.width, var2.height);
                Point var9 = this.a(var7, var2.width, var2.height);
                this.a(var7, var4, var5, var9.x, var9.y, var2.width, var2.height, var8.width, var8.height);
            }
        }

    }

    public String getElementName() {
        return "stack";
    }
}
