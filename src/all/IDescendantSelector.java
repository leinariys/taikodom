package all;

//package org.w3c.css.sac;
public interface IDescendantSelector extends ISelector {
    ISelector getAncestorSelector();

    ISimpleSelector getSimpleSelector();
}
