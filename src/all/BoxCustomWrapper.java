package all;

import javax.swing.border.Border;
import javax.swing.plaf.ComponentUI;
import java.awt.*;

public abstract class BoxCustomWrapper extends PanelCustomWrapper {

    protected boolean bCM;

    public Dimension preferredLayoutSize(Container var1) {
        int var2 = 0;
        int var3 = 0;
        if (var1.getParent() != null) {
            Dimension var4 = var1.getParent().getPreferredSize();
            var2 = var4.width;
            var3 = var4.height;
        }

        PropertiesUiFromCss var5 = PropertiesUiFromCss.a((ComponentUI) this.getUI());
        if (var5 != null) {
            if (var5.atS() != Tu.fEC) {
                var2 = var5.atS().jp((float) var2);
            }

            if (var5.atx() != Tu.fEC) {
                var3 = var5.atx().jp((float) var3);
            }
        }

        return this.getPreferredSize(var2, var3);
    }

    public Dimension getPreferredSize(int var1, int var2) {
        Dimension var3 = this.amt();
        Dimension var4 = this.getMinimumSize(var1, var2);
        Dimension var5 = var4;
        PropertiesUiFromCss var6 = PropertiesUiFromCss.a((ComponentUI) this.getUI());
        if (var6 != null) {
            int var7 = var4.width;
            int var8 = var4.height;
            if (var6.atS() != Tu.fEC) {
                var7 = var6.atS().jp((float) var1);
                if (var6.atx() != Tu.fEC) {
                    var8 = var6.atx().jp((float) var2);
                } else {
                    var8 = this.b(var7 - var3.width, var2 - var3.height).height + var3.height;
                }
            } else if (var6.atx() != Tu.fEC) {
                var8 = var6.atx().jp((float) var2);
                var7 = this.b(var1 - var3.width, var8 - var3.width).width + var3.width;
            }

            Dimension var9 = this.y(var1, var2);
            int var10 = Math.max(Math.min(var7, var9.width), var4.width);
            int var11 = Math.max(Math.min(var8, var9.height), var4.height);
            var5 = new Dimension(var10, var11);
        }

        return var5;
    }

    private Dimension amt() {
        if (this.getBorder() != null) {
            Insets var1 = this.getBorder().getBorderInsets(this);
            Dimension var2 = new Dimension(var1.left + var1.right, var1.top + var1.bottom);
            return var2;
        } else {
            return new Dimension(0, 0);
        }
    }

    private Dimension y(int var1, int var2) {
        return new Dimension(10000, 10000);
    }

    public Dimension minimumSize() {
        return this.getMinimumSize(0, 0);
    }

    public Dimension getMinimumSize(int var1, int var2) {
        Dimension var3 = this.amt();
        Dimension var4 = this.a(var1 - var3.width, var2 - var3.height);
        var4.width += var3.width;
        var4.height += var3.height;
        PropertiesUiFromCss var5 = PropertiesUiFromCss.a((ComponentUI) this.getUI());
        if (var5 != null) {
            int var6 = var4.width;
            int var7 = var4.height;
            if (var5.atI() != Tu.fEC) {
                var6 = var5.atI().jp((float) var1);
            }

            if (var5.atH() != Tu.fEC) {
                var7 = var5.atH().jp((float) var2);
            }

            var4 = new Dimension(var6, var7);
        }

        return var4;
    }

    protected abstract Dimension a(int var1, int var2);

    protected abstract Dimension b(int var1, int var2);

    protected Point a(Component var1, int var2, int var3) {
        int var4 = 0;
        int var5 = 0;
        PropertiesUiFromCss var6 = PropertiesUiFromCss.g(var1);
        if (var6 != null) {
            var4 = var6.atz().jp((float) var2);
            var5 = var6.atQ().jp((float) var3);
        }

        Point var7 = new Point(var4, var5);
        return var7;
    }

    protected Insets amu() {
        Border var1 = this.getBorder();
        if (var1 != null) {
            Insets var2 = var1.getBorderInsets(this);
            return var2;
        } else {
            return new Insets(0, 0, 0, 0);
        }
    }

    protected int amv() {
        if (this.bCM) {
            return this.getComponentCount();
        } else {
            int var1 = 0;
            int var2 = this.getComponentCount();

            while (true) {
                --var2;
                if (var2 < 0) {
                    return var1;
                }

                if (this.getComponent(var2).isVisible()) {
                    ++var1;
                }
            }
        }
    }

    protected void a(Component var1, aRU var2, vI var3, int var4, int var5, int var6, int var7, int var8, int var9) {
        int var10 = var2.t(var6, var8) + var4;
        int var11 = var3.t(var7, var9) + var5;
        int var12 = var2.u(var6, var8);
        int var13 = var3.u(var7, var9);
        var1.setBounds(var10, var11, var12, var13);
    }

    public void setBounds(Rectangle var1) {
        Dimension var2 = this.getMinimumSize(0, 0);
        super.setBounds(new Rectangle(var1.x, var1.y, Math.max(var1.width, var2.width), Math.max(var1.height, var2.height)));
    }

    public boolean amw() {
        return this.bCM;
    }

    public void bB(boolean var1) {
        this.bCM = var1;
    }

    public Dimension preferredSize() {
        Container var1 = this.getParent();
        return var1 != null ? this.getPreferredSize(var1.getWidth(), var1.getHeight()) : this.getPreferredSize(0, 0);
    }

    private Dimension b(Component var1, int var2, int var3) {
        return var1 instanceof BoxCustomWrapper ? ((BoxCustomWrapper) var1).y(var2, var3) : var1.getMaximumSize();
    }

    protected Dimension c(Component var1, int var2, int var3) {
        return var1 instanceof BoxCustomWrapper ? ((BoxCustomWrapper) var1).getMinimumSize(var2, var3) : var1.getMinimumSize();
    }

    protected Dimension d(Component var1, int var2, int var3) {
        return var1 instanceof BoxCustomWrapper ? ((BoxCustomWrapper) var1).getPreferredSize(var2, var3) : var1.getPreferredSize();
    }

    protected static class d {
        public Dimension iyP;
        public Dimension iyQ;
        public Dimension iyR;
    }

    protected static class c {
        public int[] ghH;
        public float[] ghI;
        public float[] ghJ;
        public float[] ghK;
        public float ghL;
        public float ghM;

        public c(float var1, int var2) {
            this.ghL = var1;
            this.ghH = new int[var2];
            this.ghI = new float[var2];
            this.ghJ = new float[var2];
            this.ghK = new float[var2];
        }

        public void Mq() {
            int var1 = this.ghJ.length;
            int var2 = 0;

            for (int var3 = 0; var3 < var1; ++var3) {
                var2 = (int) ((float) var2 + this.ghJ[var3]);
            }

            float var7 = this.ghL - (float) var2;
            int var4;
            float var5;
            float var6;
            if (var7 > 0.0F) {
                for (var4 = 0; var4 < var1; ++var4) {
                    var5 = this.ghJ[var4];
                    var6 = (float) Math.round(aip.clamp(var5, Math.min(var5 + var7, this.ghK[var4]), this.ghI[var4]));
                    var7 -= var6 - var5;
                    this.ghH[var4] = (int) var6;
                }
            } else if (var7 <= 0.0F) {
                var4 = var1;

                while (true) {
                    --var4;
                    if (var4 < 0) {
                        break;
                    }

                    var5 = this.ghJ[var4];
                    var6 = (float) Math.round(aip.clamp(var5, Math.min(var5 + var7, this.ghK[var4]), this.ghI[var4]));
                    var7 -= var6 - var5;
                    this.ghH[var4] = (int) var6;
                }
            }

            this.ghM = this.ghL - var7;
        }
    }

    protected class a {
        c azW;
        c azX;
        Dimension[][] azY;

        public a(Dimension var2, int var3, int var4) {
            this.azW = new c((float) var2.width, var3);
            this.azX = new c((float) var2.height, var4);
            this.azY = new Dimension[var3][var4];
        }

        public void a(int var1, int var2, Dimension var3, Component var4) {
            Dimension var5 = this.azY[var1][var2] = BoxCustomWrapper.this.d(var4, var3.width, var3.height);
            Dimension var6 = BoxCustomWrapper.this.c(var4, var3.width, var3.height);
            Dimension var7 = BoxCustomWrapper.this.b(var4, var3.width, var3.height);
            this.azW.ghK[var1] = Math.max((float) var7.width, this.azW.ghK[var1]);
            this.azW.ghJ[var1] = Math.max((float) var6.width, this.azW.ghJ[var1]);
            this.azW.ghI[var1] = Math.max((float) var5.width, this.azW.ghI[var1]);
            this.azX.ghK[var2] = Math.max((float) var7.height, this.azX.ghK[var2]);
            this.azX.ghJ[var2] = Math.max((float) var6.height, this.azX.ghJ[var2]);
            this.azX.ghI[var2] = Math.max((float) var5.height, this.azX.ghI[var2]);
        }

        public void Mq() {
            this.azW.Mq();
            this.azX.Mq();
        }
    }

    protected class b extends c {
        public Dimension[] bjC;
        public boolean[] bjD;

        public b(float var2, int var3) {
            super(var2, var3);
            this.bjC = new Dimension[var3];
            this.bjD = new boolean[var3];
        }

        public void a(int var1, Component var2, int var3, int var4) {
            if (BoxCustomWrapper.this.bCM || var2.isVisible()) {
                this.bjD[var1] = true;
                this.ghI[var1] = (float) (this.bjC[var1] = BoxCustomWrapper.this.d(var2, var3, var4)).height;
                this.ghJ[var1] = (float) BoxCustomWrapper.this.c(var2, var3, var4).height;
                this.ghK[var1] = (float) BoxCustomWrapper.this.b(var2, var3, var4).height;
            }

        }

        public void b(int var1, Component var2, int var3, int var4) {
            if (BoxCustomWrapper.this.bCM || var2.isVisible()) {
                this.bjD[var1] = true;
                this.ghI[var1] = (float) (this.bjC[var1] = BoxCustomWrapper.this.d(var2, var3, var4)).width;
                this.ghJ[var1] = (float) BoxCustomWrapper.this.c(var2, var3, var4).width;
                this.ghK[var1] = (float) BoxCustomWrapper.this.b(var2, var3, var4).width;
            }

        }
    }
}
