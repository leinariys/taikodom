package all;

import taikodom.render.DrawContext;
import taikodom.render.graphics2d.DrawNode;
import taikodom.render.graphics2d.RGuiScene;

import java.awt.*;

public final class Ko extends DrawNode {
    private int dpJ;
    private int dpK;
    private int dpL;
    private int dpM;

    public Ko(Rectangle var1) {
        this.dpJ = var1.x;
        this.dpL = var1.y;
        this.dpM = var1.width;
        this.dpK = var1.height;
    }

    public void draw(DrawContext var1, RGuiScene var2) {
        int var3 = var1.getRenderView().getViewport().height;
        var1.getGL().glScissor(this.dpJ, var3 - (this.dpK + this.dpL), this.dpM, this.dpK);
    }
}
