package all;

import com.hoplon.geometry.Vec3f;

import javax.vecmath.*;
import java.nio.FloatBuffer;

public class ajK extends Matrix4f implements afA_q {

    public ajK() {
        this.setIdentity();
    }

    public ajK(float[] var1) {
        super(var1[0], var1[4], var1[8], var1[12], var1[1], var1[5], var1[9], var1[13], var1[2], var1[6], var1[10], var1[14], var1[3], var1[7], var1[11], var1[15]);
    }

    public ajK(Matrix4d var1) {
        super(var1);
    }

    public ajK(ajK var1) {
        super(var1);
    }

    public ajK(Quat4f var1, Vector3f var2, float var3) {
        super(var1, var2, var3);
    }

    public ajK(Matrix3f var1, Vector3f var2, float var3) {
        super(var1, var2, var3);
    }

    public ajK(float var1, float var2, float var3, float var4, float var5, float var6, float var7, float var8, float var9, float var10, float var11, float var12, float var13, float var14, float var15, float var16) {
        super(var1, var5, var9, var13, var2, var6, var10, var14, var3, var7, var11, var15, var4, var8, var12, var16);
    }

    public static ajK b(ajK var0, ajK var1) {
        ajK var2 = new ajK();
        b(var0, var1, var2);
        return var2;
    }

    private static ajK b(ajK var0, ajK var1, ajK var2) {
        float var3 = var0.m00 * var1.m00 + var0.m01 * var1.m10 + var0.m02 * var1.m20;
        float var4 = var0.m10 * var1.m00 + var0.m11 * var1.m10 + var0.m12 * var1.m20;
        float var5 = var0.m20 * var1.m00 + var0.m21 * var1.m10 + var0.m22 * var1.m20;
        float var6 = var0.m00 * var1.m01 + var0.m01 * var1.m11 + var0.m02 * var1.m21;
        float var7 = var0.m10 * var1.m01 + var0.m11 * var1.m11 + var0.m12 * var1.m21;
        float var8 = var0.m20 * var1.m01 + var0.m21 * var1.m11 + var0.m22 * var1.m21;
        float var9 = var0.m00 * var1.m02 + var0.m01 * var1.m12 + var0.m02 * var1.m22;
        float var10 = var0.m10 * var1.m02 + var0.m11 * var1.m12 + var0.m12 * var1.m22;
        float var11 = var0.m20 * var1.m02 + var0.m21 * var1.m12 + var0.m22 * var1.m22;
        float var12 = var0.m00 * var1.m03 + var0.m01 * var1.m13 + var0.m02 * var1.m23 + var0.m03;
        float var13 = var0.m10 * var1.m03 + var0.m11 * var1.m13 + var0.m12 * var1.m23 + var0.m13;
        float var14 = var0.m20 * var1.m03 + var0.m21 * var1.m13 + var0.m22 * var1.m23 + var0.m23;
        var2.m00 = var3;
        var2.m10 = var4;
        var2.m20 = var5;
        var2.m01 = var6;
        var2.m11 = var7;
        var2.m21 = var8;
        var2.m02 = var9;
        var2.m12 = var10;
        var2.m22 = var11;
        var2.m03 = var12;
        var2.m13 = var13;
        var2.m23 = var14;
        var2.m32 = var2.m31 = var2.m30 = 0.0F;
        var2.m33 = 1.0F;
        return var2;
    }

    public static ajK c(ajK var0, ajK var1, ajK var2) {
        float var3 = var0.m00 * var1.m00 + var0.m01 * var1.m10 + var0.m02 * var1.m20 + var0.m03 * var1.m30;
        float var4 = var0.m10 * var1.m00 + var0.m11 * var1.m10 + var0.m12 * var1.m20 + var0.m13 * var1.m30;
        float var5 = var0.m20 * var1.m00 + var0.m21 * var1.m10 + var0.m22 * var1.m20 + var0.m23 * var1.m30;
        float var6 = var0.m30 * var1.m00 + var0.m31 * var1.m10 + var0.m32 * var1.m20 + var0.m33 * var1.m30;
        float var7 = var0.m00 * var1.m01 + var0.m01 * var1.m11 + var0.m02 * var1.m21 + var0.m03 * var1.m31;
        float var8 = var0.m10 * var1.m01 + var0.m11 * var1.m11 + var0.m12 * var1.m21 + var0.m13 * var1.m31;
        float var9 = var0.m20 * var1.m01 + var0.m21 * var1.m11 + var0.m22 * var1.m21 + var0.m23 * var1.m31;
        float var10 = var0.m30 * var1.m01 + var0.m31 * var1.m11 + var0.m32 * var1.m21 + var0.m33 * var1.m31;
        float var11 = var0.m00 * var1.m02 + var0.m01 * var1.m12 + var0.m02 * var1.m22 + var0.m03 * var1.m32;
        float var12 = var0.m10 * var1.m02 + var0.m11 * var1.m12 + var0.m12 * var1.m22 + var0.m13 * var1.m32;
        float var13 = var0.m20 * var1.m02 + var0.m21 * var1.m12 + var0.m22 * var1.m22 + var0.m23 * var1.m32;
        float var14 = var0.m30 * var1.m02 + var0.m31 * var1.m12 + var0.m32 * var1.m22 + var0.m33 * var1.m32;
        float var15 = var0.m00 * var1.m03 + var0.m01 * var1.m13 + var0.m02 * var1.m23 + var0.m03 * var1.m33;
        float var16 = var0.m10 * var1.m03 + var0.m11 * var1.m13 + var0.m12 * var1.m23 + var0.m13 * var1.m33;
        float var17 = var0.m20 * var1.m03 + var0.m21 * var1.m13 + var0.m22 * var1.m23 + var0.m23 * var1.m33;
        float var18 = var0.m30 * var1.m03 + var0.m31 * var1.m13 + var0.m32 * var1.m23 + var0.m33 * var1.m33;
        var2.m00 = var3;
        var2.m10 = var4;
        var2.m20 = var5;
        var2.m30 = var6;
        var2.m01 = var7;
        var2.m11 = var8;
        var2.m21 = var9;
        var2.m31 = var10;
        var2.m02 = var11;
        var2.m12 = var12;
        var2.m22 = var13;
        var2.m32 = var14;
        var2.m03 = var15;
        var2.m13 = var16;
        var2.m23 = var17;
        var2.m33 = var18;
        return var2;
    }

    public static void d(ajK var0, ajK var1, ajK var2) {
        var2.m00 = var0.m00 * var1.m00 + var0.m01 * var1.m10 + var0.m02 * var1.m20;
        var2.m10 = var0.m10 * var1.m00 + var0.m11 * var1.m10 + var0.m12 * var1.m20;
        var2.m20 = var0.m20 * var1.m00 + var0.m21 * var1.m10 + var0.m22 * var1.m20;
        var2.m01 = var0.m00 * var1.m01 + var0.m01 * var1.m11 + var0.m02 * var1.m21;
        var2.m11 = var0.m10 * var1.m01 + var0.m11 * var1.m11 + var0.m12 * var1.m21;
        var2.m21 = var0.m20 * var1.m01 + var0.m21 * var1.m11 + var0.m22 * var1.m21;
        var2.m02 = var0.m00 * var1.m02 + var0.m01 * var1.m12 + var0.m02 * var1.m22;
        var2.m12 = var0.m10 * var1.m02 + var0.m11 * var1.m12 + var0.m12 * var1.m22;
        var2.m22 = var0.m20 * var1.m02 + var0.m21 * var1.m12 + var0.m22 * var1.m22;
        var2.m03 = var0.m00 * var1.m03 + var0.m01 * var1.m13 + var0.m02 * var1.m23 + var0.m03;
        var2.m13 = var0.m10 * var1.m03 + var0.m11 * var1.m13 + var0.m12 * var1.m23 + var0.m13;
        var2.m23 = var0.m20 * var1.m03 + var0.m21 * var1.m13 + var0.m22 * var1.m23 + var0.m23;
    }

    public static ajK g(ajK var0, ajK var1) {
        ajK var2 = new ajK();
        e(var0, var1, var2);
        return var2;
    }

    private static void e(ajK var0, ajK var1, ajK var2) {
        float var3 = var0.m00 * var1.m00 + var0.m01 * var1.m10 + var0.m02 * var1.m20;
        float var4 = var0.m10 * var1.m00 + var0.m11 * var1.m10 + var0.m12 * var1.m20;
        float var5 = var0.m20 * var1.m00 + var0.m21 * var1.m10 + var0.m22 * var1.m20;
        float var6 = var0.m00 * var1.m01 + var0.m01 * var1.m11 + var0.m02 * var1.m21;
        float var7 = var0.m10 * var1.m01 + var0.m11 * var1.m11 + var0.m12 * var1.m21;
        float var8 = var0.m20 * var1.m01 + var0.m21 * var1.m11 + var0.m22 * var1.m21;
        float var9 = var0.m00 * var1.m02 + var0.m01 * var1.m12 + var0.m02 * var1.m22;
        float var10 = var0.m10 * var1.m02 + var0.m11 * var1.m12 + var0.m12 * var1.m22;
        float var11 = var0.m20 * var1.m02 + var0.m21 * var1.m12 + var0.m22 * var1.m22;
        var2.m00 = var3;
        var2.m10 = var4;
        var2.m20 = var5;
        var2.m01 = var6;
        var2.m11 = var7;
        var2.m21 = var8;
        var2.m02 = var9;
        var2.m12 = var10;
        var2.m22 = var11;
    }

    private static void h(ajK var0, ajK var1) {
        float var2 = var0.m00;
        float var3 = var0.m01;
        float var4 = var0.m02;
        float var5 = var0.m03;
        float var6 = var0.m10;
        float var7 = var0.m11;
        float var8 = var0.m12;
        float var9 = var0.m13;
        float var10 = var0.m20;
        float var11 = var0.m21;
        float var12 = var0.m22;
        float var13 = var0.m23;
        float var14 = 1.0F / (var2 * var2 + var6 * var6 + var10 * var10);
        float var15 = 1.0F / (var3 * var3 + var7 * var7 + var11 * var11);
        float var16 = 1.0F / (var4 * var4 + var8 * var8 + var12 * var12);
        var1.m00 = var2 * var14;
        var1.m01 = var6 * var15;
        var1.m02 = var10 * var16;
        var1.m10 = var3 * var14;
        var1.m11 = var7 * var15;
        var1.m12 = var11 * var16;
        var1.m20 = var4 * var14;
        var1.m21 = var8 * var15;
        var1.m22 = var12 * var16;
        float var17 = var5 * var14;
        float var18 = var9 * var15;
        float var19 = var13 * var16;
        var1.m03 = -(var17 * var2 + var18 * var6 + var19 * var10);
        var1.m13 = -(var17 * var3 + var18 * var7 + var19 * var11);
        var1.m23 = -(var17 * var4 + var18 * var8 + var19 * var12);
    }

    public static void f(ajK var0, ajK var1, ajK var2) {
        float var3 = var0.m00;
        float var4 = var0.m01;
        float var5 = var0.m02;
        float var6 = var0.m03;
        float var7 = var0.m10;
        float var8 = var0.m11;
        float var9 = var0.m12;
        float var10 = var0.m13;
        float var11 = var0.m20;
        float var12 = var0.m21;
        float var13 = var0.m22;
        float var14 = var0.m23;
        float var15 = 1.0F / (var3 * var3 + var7 * var7 + var11 * var11);
        float var16 = 1.0F / (var4 * var4 + var8 * var8 + var12 * var12);
        float var17 = 1.0F / (var5 * var5 + var9 * var9 + var13 * var13);
        float var18 = var3 * var15;
        float var19 = var7 * var16;
        float var20 = var11 * var17;
        float var21 = var4 * var15;
        float var22 = var8 * var16;
        float var23 = var12 * var17;
        float var24 = var5 * var15;
        float var25 = var9 * var16;
        float var26 = var13 * var17;
        float var27 = var6 * var15;
        float var28 = var10 * var16;
        float var29 = var14 * var17;
        float var30 = -(var27 * var3 + var28 * var7 + var29 * var11);
        float var31 = -(var27 * var4 + var28 * var8 + var29 * var12);
        float var32 = -(var27 * var5 + var28 * var9 + var29 * var13);
        float var33 = var18 * var1.m00 + var19 * var1.m10 + var20 * var1.m20;
        float var34 = var21 * var1.m00 + var22 * var1.m10 + var23 * var1.m20;
        float var35 = var24 * var1.m00 + var25 * var1.m10 + var26 * var1.m20;
        float var36 = var18 * var1.m01 + var19 * var1.m11 + var20 * var1.m21;
        float var37 = var21 * var1.m01 + var22 * var1.m11 + var23 * var1.m21;
        float var38 = var24 * var1.m01 + var25 * var1.m11 + var26 * var1.m21;
        float var39 = var18 * var1.m02 + var19 * var1.m12 + var20 * var1.m22;
        float var40 = var21 * var1.m02 + var22 * var1.m12 + var23 * var1.m22;
        float var41 = var24 * var1.m02 + var25 * var1.m12 + var26 * var1.m22;
        float var42 = var18 * var1.m03 + var19 * var1.m13 + var20 * var1.m23 + var30;
        float var43 = var21 * var1.m03 + var22 * var1.m13 + var23 * var1.m23 + var31;
        float var44 = var24 * var1.m03 + var25 * var1.m13 + var26 * var1.m23 + var32;
        var2.m00 = var33;
        var2.m10 = var34;
        var2.m20 = var35;
        var2.m01 = var36;
        var2.m11 = var37;
        var2.m21 = var38;
        var2.m02 = var39;
        var2.m12 = var40;
        var2.m22 = var41;
        var2.m03 = var42;
        var2.m13 = var43;
        var2.m23 = var44;
        var2.m32 = var2.m31 = var2.m30 = 0.0F;
        var2.m33 = 1.0F;
    }

    public void c(float[] var1) {
        super.m00 = var1[0];
        super.m01 = var1[1];
        super.m02 = var1[2];
        super.m03 = var1[3];
        super.m10 = var1[4];
        super.m11 = var1[5];
        super.m12 = var1[6];
        super.m13 = var1[7];
        super.m20 = var1[8];
        super.m21 = var1[9];
        super.m22 = var1[10];
        super.m23 = var1[11];
        super.m30 = var1[12];
        super.m31 = var1[13];
        super.m32 = var1[14];
        super.m33 = var1[15];
    }

    public float[] ceB() {
        float[] var1 = new float[]{super.m00, super.m01, super.m02, super.m03, super.m10, super.m11, super.m12, super.m13, super.m20, super.m21, super.m22, super.m23, super.m30, super.m31, super.m32, super.m33};
        return var1;
    }

    public Vector3dOperations a(Tuple3d var1) {
        return new Vector3dOperations(var1.x * (double) this.m00 + var1.y * (double) this.m01 + var1.z * (double) this.m02 + (double) this.m03, var1.x * (double) this.m10 + var1.y * (double) this.m11 + var1.z * (double) this.m12 + (double) this.m13, var1.x * (double) this.m20 + var1.y * (double) this.m21 + var1.z * (double) this.m22 + (double) this.m23);
    }

    public Vec3f a(Tuple3f var1) {
        Vec3f var2 = new Vec3f();
        this.a((Tuple3f) var1, (Tuple3f) var2);
        return var2;
    }

    public void b(Tuple3d var1) {
        this.a(var1, var1);
    }

    public void b(Tuple3f var1) {
        this.a(var1, var1);
    }

    public void a(Tuple3f var1, Tuple3f var2) {
        float var3 = var1.x;
        float var4 = var1.y;
        float var5 = var1.z;
        var2.x = var3 * this.m00 + var4 * this.m01 + var5 * this.m02;
        var2.y = var3 * this.m10 + var4 * this.m11 + var5 * this.m12;
        var2.z = var3 * this.m20 + var4 * this.m21 + var5 * this.m22;
    }

    public void a(Tuple3d var1, Tuple3d var2) {
        double var3 = var1.x;
        double var5 = var1.y;
        double var7 = var1.z;
        var2.x = var3 * (double) this.m00 + var5 * (double) this.m01 + var7 * (double) this.m02;
        var2.y = var3 * (double) this.m10 + var5 * (double) this.m11 + var7 * (double) this.m12;
        var2.z = var3 * (double) this.m20 + var5 * (double) this.m21 + var7 * (double) this.m22;
    }

    public Vec3f c(Tuple3f var1) {
        float var2 = var1.x;
        float var3 = var1.y;
        float var4 = var1.z;
        return new Vec3f(var2 * this.m00 + var3 * this.m01 + var4 * this.m02 + this.m03, var2 * this.m10 + var3 * this.m11 + var4 * this.m12 + this.m13, var2 * this.m20 + var3 * this.m21 + var4 * this.m22 + this.m23);
    }

    public void c(Tuple3d var1) {
        this.b(var1, var1);
    }

    public void d(Tuple3f var1) {
        this.b(var1, var1);
    }

    public void b(Tuple3d var1, Tuple3d var2) {
        double var3 = var1.x;
        double var5 = var1.y;
        double var7 = var1.z;
        var2.x = var3 * (double) this.m00 + var5 * (double) this.m01 + var7 * (double) this.m02 + (double) this.m03;
        var2.y = var3 * (double) this.m10 + var5 * (double) this.m11 + var7 * (double) this.m12 + (double) this.m13;
        var2.z = var3 * (double) this.m20 + var5 * (double) this.m21 + var7 * (double) this.m22 + (double) this.m23;
    }

    public void b(Tuple3f var1, Tuple3f var2) {
        float var3 = var1.x;
        float var4 = var1.y;
        float var5 = var1.z;
        var2.x = var3 * this.m00 + var4 * this.m01 + var5 * this.m02 + this.m03;
        var2.y = var3 * this.m10 + var4 * this.m11 + var5 * this.m12 + this.m13;
        var2.z = var3 * this.m20 + var4 * this.m21 + var5 * this.m22 + this.m23;
    }

    public Vec3f ceC() {
        return new Vec3f(this.m00, this.m10, this.m20);
    }

    public Vec3f ceD() {
        return new Vec3f(this.m01, this.m11, this.m21);
    }

    public Vec3f ceE() {
        return new Vec3f(this.m02, this.m12, this.m22);
    }

    public String toString() {
        return String.format(" %4.4f %4.4f %4.4f %4.4f\n %4.4f %4.4f %4.4f %4.4f\n %4.4f %4.4f %4.4f %4.4f\n %4.4f %4.4f %4.4f %4.4f\n", this.m00, this.m01, this.m02, this.m03, this.m10, this.m11, this.m12, this.m13, this.m20, this.m21, this.m22, this.m23, this.m30, this.m31, this.m32, this.m33);
    }

    public String ceG() {
        return String.format(" %4d %4d %4d %4d\n %4d %4d %4d %4d\n %4d %4d %4d %4d\n %4d %4d %4d %4d\n", Float.floatToRawIntBits(this.m00), Float.floatToRawIntBits(this.m01), Float.floatToRawIntBits(this.m02), Float.floatToRawIntBits(this.m03), Float.floatToRawIntBits(this.m10), Float.floatToRawIntBits(this.m11), Float.floatToRawIntBits(this.m12), Float.floatToRawIntBits(this.m13), Float.floatToRawIntBits(this.m20), Float.floatToRawIntBits(this.m21), Float.floatToRawIntBits(this.m22), Float.floatToRawIntBits(this.m23), Float.floatToRawIntBits(this.m30), Float.floatToRawIntBits(this.m31), Float.floatToRawIntBits(this.m32), Float.floatToRawIntBits(this.m33));
    }

    public void rotateX(float var1) {
        float var2 = var1 * 0.017453292F;
        ajK var3 = new ajK();
        var3.setIdentity();
        float var4 = (float) Math.sin((double) var2);
        float var5 = (float) Math.cos((double) var2);
        var3.m11 = var5;
        var3.m12 = -var4;
        var3.m21 = var4;
        var3.m22 = var5;
        this.set(b(this, var3));
    }

    public void jI(float var1) {
        float var2 = var1 * 0.017453292F;
        ajK var3 = new ajK();
        var3.setIdentity();
        float var4 = (float) Math.sin((double) var2);
        float var5 = (float) Math.cos((double) var2);
        var3.m11 = var5;
        var3.m12 = -var4;
        var3.m21 = var4;
        var3.m22 = var5;
        this.set(b(var3, this));
    }

    public void rotateY(float var1) {
        float var2 = var1 * 0.017453292F;
        ajK var3 = new ajK();
        var3.setIdentity();
        float var4 = (float) Math.sin((double) var2);
        float var5 = (float) Math.cos((double) var2);
        var3.m00 = var5;
        var3.m02 = var4;
        var3.m20 = -var4;
        var3.m22 = var5;
        this.set(b(this, var3));
    }

    public void rotateZ(float var1) {
        float var2 = var1 * 0.017453292F;
        ajK var3 = new ajK();
        var3.setIdentity();
        float var4 = (float) Math.sin((double) var2);
        float var5 = (float) Math.cos((double) var2);
        var3.m00 = var5;
        var3.m01 = -var4;
        var3.m10 = var4;
        var3.m11 = var5;
        this.set(b(this, var3));
    }

    public void jJ(float var1) {
        float var2 = var1 * 0.017453292F;
        ajK var3 = new ajK();
        var3.setIdentity();
        float var4 = (float) Math.sin((double) var2);
        float var5 = (float) Math.cos((double) var2);
        var3.m00 = var5;
        var3.m01 = -var4;
        var3.m10 = var4;
        var3.m11 = var5;
        this.set(b(var3, this));
    }

    public void aD(Vec3f var1) {
        this.m00 = var1.x;
        this.m10 = var1.y;
        this.m20 = var1.z;
    }

    public void setX(float var1, float var2, float var3) {
        this.m00 = var1;
        this.m10 = var2;
        this.m20 = var3;
    }

    public void setY(float var1, float var2, float var3) {
        this.m01 = var1;
        this.m11 = var2;
        this.m21 = var3;
    }

    public void setZ(float var1, float var2, float var3) {
        this.m02 = var1;
        this.m12 = var2;
        this.m22 = var3;
    }

    public void o(float var1, float var2, float var3) {
        this.m03 = var1;
        this.m13 = var2;
        this.m23 = var3;
    }

    public void aE(Vec3f var1) {
        this.setColumn(1, var1.x, var1.y, var1.z, 0.0F);
    }

    public void aF(Vec3f var1) {
        this.setColumn(2, var1.x, var1.y, var1.z, 0.0F);
    }

    public void setTranslation(Vec3f var1) {
        this.setColumn(3, var1.x, var1.y, var1.z, 1.0F);
    }

    public ajK aG(Vec3f var1) {
        this.setColumn(3, var1.x, var1.y, var1.z, 1.0F);
        return this;
    }

    public void p(float var1, float var2, float var3) {
        this.m03 = var1;
        this.m13 = var2;
        this.m23 = var3;
    }

    public void setTranslation(Vector3dOperations var1) {
        this.m03 = (float) var1.x;
        this.m13 = (float) var1.y;
        this.m23 = (float) var1.z;
    }

    public Vec3f ceH() {
        return new Vec3f(this.m03, this.m13, this.m23);
    }

    public void getTranslation(Vec3f var1) {
        var1.x = this.m03;
        var1.y = this.m13;
        var1.z = this.m23;
    }

    public ajK c(ajK var1, ajK var2) {
        b(var1, var2, this);
        return this;
    }

    public void d(ajK var1, ajK var2) {
        b(var1, var2, this);
    }

    public ajK e(ajK var1, ajK var2) {
        e(var1, var2, this);
        return this;
    }

    public void f(ajK var1, ajK var2) {
        e(var1, var2, this);
    }

    public ajK g(ajK var1) {
        e(var1, this, this);
        return this;
    }

    public ajK h(ajK var1) {
        b(this, var1, this);
        return this;
    }

    public void a(float var1, float var2, float var3, float var4, float var5, float var6, float var7, float var8, float var9, float var10, float var11, float var12, float var13, float var14, float var15, float var16) {
        this.m00 = var1;
        this.m01 = var5;
        this.m02 = var9;
        this.m03 = var13;
        this.m10 = var2;
        this.m11 = var6;
        this.m12 = var10;
        this.m13 = var14;
        this.m20 = var3;
        this.m21 = var7;
        this.m22 = var11;
        this.m23 = var15;
        this.m30 = var4;
        this.m31 = var8;
        this.m32 = var12;
        this.m33 = var16;
    }

    public ajK b(float var1, float var2, float var3, float var4, float var5, float var6, float var7, float var8, float var9, float var10, float var11, float var12, float var13, float var14, float var15, float var16) {
        this.m00 = var1;
        this.m01 = var2;
        this.m02 = var3;
        this.m03 = var4;
        this.m10 = var5;
        this.m11 = var6;
        this.m12 = var7;
        this.m13 = var8;
        this.m20 = var9;
        this.m21 = var10;
        this.m22 = var11;
        this.m23 = var12;
        this.m30 = var13;
        this.m31 = var14;
        this.m32 = var15;
        this.m33 = var16;
        return this;
    }

    public Vec3f aH(Vec3f var1) {
        float var2 = 1.0F / (this.m00 * this.m00 + this.m10 * this.m10 + this.m20 * this.m20);
        float var3 = 1.0F / (this.m01 * this.m01 + this.m11 * this.m11 + this.m21 * this.m21);
        float var4 = 1.0F / (this.m02 * this.m02 + this.m12 * this.m12 + this.m22 * this.m22);
        float var5 = this.m00 * var2;
        float var6 = this.m10 * var3;
        float var7 = this.m20 * var4;
        float var8 = this.m01 * var2;
        float var9 = this.m11 * var3;
        float var10 = this.m21 * var4;
        float var11 = this.m02 * var2;
        float var12 = this.m12 * var3;
        float var13 = this.m22 * var4;
        float var14 = -(this.m03 * this.m00 + this.m13 * this.m10 + this.m23 * this.m20);
        float var15 = -(this.m03 * this.m01 + this.m13 * this.m11 + this.m23 * this.m21);
        float var16 = -(this.m03 * this.m02 + this.m13 * this.m12 + this.m23 * this.m22);
        float var17 = var1.x;
        float var18 = var1.y;
        float var19 = var1.z;
        return new Vec3f(var17 * var5 + var18 * var6 + var19 * var7 + var14, var17 * var8 + var18 * var9 + var19 * var10 + var15, var17 * var11 + var18 * var12 + var19 * var13 + var16);
    }

    public Vec3f aI(Vec3f var1) {
        float var2 = 1.0F / (this.m00 * this.m00 + this.m10 * this.m10 + this.m20 * this.m20);
        float var3 = 1.0F / (this.m01 * this.m01 + this.m11 * this.m11 + this.m21 * this.m21);
        float var4 = 1.0F / (this.m02 * this.m02 + this.m12 * this.m12 + this.m22 * this.m22);
        float var5 = this.m00 * var2;
        float var6 = this.m10 * var3;
        float var7 = this.m20 * var4;
        float var8 = this.m01 * var2;
        float var9 = this.m11 * var3;
        float var10 = this.m21 * var4;
        float var11 = this.m02 * var2;
        float var12 = this.m12 * var3;
        float var13 = this.m22 * var4;
        float var14 = var1.x;
        float var15 = var1.y;
        float var16 = var1.z;
        return new Vec3f(var14 * var5 + var15 * var6 + var16 * var7, var14 * var8 + var15 * var9 + var16 * var10, var14 * var11 + var15 * var12 + var16 * var13);
    }

    public boolean hasRotation() {
        float var4 = -this.m12;
        float var1;
        if (var4 <= -1.0F) {
            var1 = -1.570796F;
        } else if ((double) var4 >= 1.0D) {
            var1 = 1.570796F;
        } else {
            var1 = (float) Math.asin((double) var4);
        }

        float var2;
        float var3;
        if (var4 > 0.9999F) {
            var3 = 0.0F;
            var2 = (float) Math.atan2((double) (-this.m02), (double) this.m00);
        } else {
            var2 = (float) Math.atan2((double) this.m02, (double) this.m22);
            var3 = (float) Math.atan2((double) this.m10, (double) this.m11);
        }

        return var2 != 0.0F || var3 != 0.0F || var1 != 0.0F;
    }

    public ajK ceI() {
        ajK var1 = new ajK();
        float var2 = 1.0F / (this.m00 * this.m00 + this.m10 * this.m10 + this.m20 * this.m20);
        float var3 = 1.0F / (this.m01 * this.m01 + this.m11 * this.m11 + this.m21 * this.m21);
        float var4 = 1.0F / (this.m02 * this.m02 + this.m12 * this.m12 + this.m22 * this.m22);
        var1.m00 = this.m00 * var2;
        var1.m01 = this.m10 * var3;
        var1.m02 = this.m20 * var4;
        var1.m10 = this.m01 * var2;
        var1.m11 = this.m11 * var3;
        var1.m12 = this.m21 * var4;
        var1.m20 = this.m02 * var2;
        var1.m21 = this.m12 * var3;
        var1.m22 = this.m22 * var4;
        float var5 = this.m03 * var2;
        float var6 = this.m13 * var3;
        float var7 = this.m23 * var4;
        var1.m03 = -(var5 * this.m00 + var6 * this.m10 + var7 * this.m20);
        var1.m13 = -(var5 * this.m01 + var6 * this.m11 + var7 * this.m21);
        var1.m23 = -(var5 * this.m02 + var6 * this.m12 + var7 * this.m22);
        return var1;
    }

    /**
     * @deprecated
     */
    @Deprecated
    public void i(ajK var1) {
        h(this, var1);
    }

    public void j(ajK var1) {
        h(var1, this);
    }

    public void ceJ() {
        h(this, this);
    }

    public ajK k(ajK var1) {
        h(var1, this);
        return this;
    }

    public ajK ceK() {
        h(this, this);
        return this;
    }

    public void ceL() {
        h(this, this);
    }

    public ajK l(ajK var1) {
        f(this, var1, this);
        return this;
    }

    public void a(FloatBuffer var1) {
        float var2 = 1.0F / (this.m00 * this.m00 + this.m10 * this.m10 + this.m20 * this.m20);
        float var3 = 1.0F / (this.m01 * this.m01 + this.m11 * this.m11 + this.m21 * this.m21);
        float var4 = 1.0F / (this.m02 * this.m02 + this.m12 * this.m12 + this.m22 * this.m22);
        var1.put(this.m00 * var2);
        var1.put(this.m01 * var2);
        var1.put(this.m02 * var2);
        var1.put(0.0F);
        var1.put(this.m10 * var3);
        var1.put(this.m11 * var3);
        var1.put(this.m12 * var3);
        var1.put(0.0F);
        var1.put(this.m20 * var4);
        var1.put(this.m21 * var4);
        var1.put(this.m22 * var4);
        var1.put(0.0F);
        float var5 = this.m03 * var2;
        float var6 = this.m13 * var3;
        float var7 = this.m23 * var4;
        var1.put(-(var5 * this.m00 + var6 * this.m10 + var7 * this.m20));
        var1.put(-(var5 * this.m01 + var6 * this.m11 + var7 * this.m21));
        var1.put(-(var5 * this.m02 + var6 * this.m12 + var7 * this.m22));
        var1.put(1.0F);
    }

    public FloatBuffer b(FloatBuffer var1) {
        var1.clear();
        this.asColumnMajorBuffer(var1);
        var1.flip();
        return var1;
    }

    public void asColumnMajorBuffer(FloatBuffer var1) {
        var1.put(this.m00);
        var1.put(this.m10);
        var1.put(this.m20);
        var1.put(this.m30);
        var1.put(this.m01);
        var1.put(this.m11);
        var1.put(this.m21);
        var1.put(this.m31);
        var1.put(this.m02);
        var1.put(this.m12);
        var1.put(this.m22);
        var1.put(this.m32);
        var1.put(this.m03);
        var1.put(this.m13);
        var1.put(this.m23);
        var1.put(this.m33);
    }

    public void aJ(Vec3f var1) {
        this.B(var1, var1);
    }

    public void V(Vector3dOperations var1) {
        this.e(var1, var1);
    }

    public void B(Vec3f var1, Vec3f var2) {
        float var3 = 1.0F / (this.m00 * this.m00 + this.m10 * this.m10 + this.m20 * this.m20);
        float var4 = 1.0F / (this.m01 * this.m01 + this.m11 * this.m11 + this.m21 * this.m21);
        float var5 = 1.0F / (this.m02 * this.m02 + this.m12 * this.m12 + this.m22 * this.m22);
        float var6 = this.m00 * var3;
        float var7 = this.m10 * var4;
        float var8 = this.m20 * var5;
        float var9 = this.m01 * var3;
        float var10 = this.m11 * var4;
        float var11 = this.m21 * var5;
        float var12 = this.m02 * var3;
        float var13 = this.m12 * var4;
        float var14 = this.m22 * var5;
        float var15 = this.m03 * var3;
        float var16 = this.m13 * var4;
        float var17 = this.m23 * var5;
        float var18 = -(var15 * this.m00 + var16 * this.m10 + var17 * this.m20);
        float var19 = -(var15 * this.m01 + var16 * this.m11 + var17 * this.m21);
        float var20 = -(var15 * this.m02 + var16 * this.m12 + var17 * this.m22);
        float var21 = var1.x;
        float var22 = var1.y;
        float var23 = var1.z;
        var2.x = var21 * var6 + var22 * var7 + var23 * var8 + var18;
        var2.y = var21 * var9 + var22 * var10 + var23 * var11 + var19;
        var2.z = var21 * var12 + var22 * var13 + var23 * var14 + var20;
    }

    public float determinant3x3() {
        return this.m00 * (this.m11 * this.m22 - this.m12 * this.m21) + this.m01 * (this.m12 * this.m20 - this.m10 * this.m22) + this.m02 * (this.m10 * this.m21 - this.m11 * this.m20);
    }

    public boolean isValidRotation() {
        float var1 = Math.abs(1.0F - this.determinant3x3());
        return (double) var1 >= 1.0E-6D && var1 == var1;
    }

    public void ceM() {
        this.set(0.0F);
        this.m00 = 1.0F;
        this.m11 = 1.0F;
        this.m22 = 1.0F;
        this.m33 = 1.0F;
    }

    public ajK ceN() {
        this.ceM();
        return this;
    }

    public void e(Vector3dOperations var1, Vector3dOperations var2) {
        float var3 = 1.0F / (this.m00 * this.m00 + this.m10 * this.m10 + this.m20 * this.m20);
        float var4 = 1.0F / (this.m01 * this.m01 + this.m11 * this.m11 + this.m21 * this.m21);
        float var5 = 1.0F / (this.m02 * this.m02 + this.m12 * this.m12 + this.m22 * this.m22);
        float var6 = this.m00 * var3;
        float var7 = this.m10 * var4;
        float var8 = this.m20 * var5;
        float var9 = this.m01 * var3;
        float var10 = this.m11 * var4;
        float var11 = this.m21 * var5;
        float var12 = this.m02 * var3;
        float var13 = this.m12 * var4;
        float var14 = this.m22 * var5;
        double var15 = var1.x;
        double var17 = var1.y;
        double var19 = var1.z;
        var2.x = var15 * (double) var6 + var17 * (double) var7 + var19 * (double) var8;
        var2.y = var15 * (double) var9 + var17 * (double) var10 + var19 * (double) var11;
        var2.z = var15 * (double) var12 + var17 * (double) var13 + var19 * (double) var14;
    }

    public void C(Vec3f var1, Vec3f var2) {
        float var3 = 1.0F / (this.m00 * this.m00 + this.m10 * this.m10 + this.m20 * this.m20);
        float var4 = 1.0F / (this.m01 * this.m01 + this.m11 * this.m11 + this.m21 * this.m21);
        float var5 = 1.0F / (this.m02 * this.m02 + this.m12 * this.m12 + this.m22 * this.m22);
        float var6 = this.m00 * var3;
        float var7 = this.m10 * var4;
        float var8 = this.m20 * var5;
        float var9 = this.m01 * var3;
        float var10 = this.m11 * var4;
        float var11 = this.m21 * var5;
        float var12 = this.m02 * var3;
        float var13 = this.m12 * var4;
        float var14 = this.m22 * var5;
        float var15 = var1.x;
        float var16 = var1.y;
        float var17 = var1.z;
        var2.x = var15 * var6 + var16 * var7 + var17 * var8;
        var2.y = var15 * var9 + var16 * var10 + var17 * var11;
        var2.z = var15 * var12 + var16 * var13 + var17 * var14;
    }

    public ajK i(ajK var1, ajK var2) {
        this.d(var2, var1);
        this.ceL();
        return this;
    }

    public boolean ceO() {
        return this.jK(1.0E-6F);
    }

    public boolean jK(float var1) {
        if (Math.abs(this.m00 * this.m01 + this.m10 * this.m11 + this.m20 * this.m21) > var1) {
            return false;
        } else if (Math.abs(this.m00 * this.m02 + this.m10 * this.m12 + this.m20 * this.m22) > var1) {
            return false;
        } else if (Math.abs(this.m01 * this.m02 + this.m11 * this.m12 + this.m21 * this.m22) > var1) {
            return false;
        } else if (Math.abs(1.0F - (this.m00 * this.m00 + this.m10 * this.m10 + this.m20 * this.m20)) > var1) {
            return false;
        } else if (Math.abs(1.0F - (this.m01 * this.m01 + this.m11 * this.m11 + this.m21 * this.m21)) > var1) {
            return false;
        } else {
            return Math.abs(1.0F - (this.m02 * this.m02 + this.m12 * this.m12 + this.m22 * this.m22)) <= var1;
        }
    }

    public boolean ceP() {
        float var1 = this.m00;
        float var2 = this.m10;
        float var3 = this.m20;
        float var4 = this.m01;
        float var5 = this.m11;
        float var6 = this.m21;
        float var7 = this.m02;
        float var8 = this.m12;
        float var9 = this.m22;
        float var10 = var2 * var6 - var3 * var5;
        float var11 = var3 * var4 - var1 * var6;
        float var12 = var1 * var5 - var2 * var4;
        float var13 = var10 * var7 + var11 * var8 + var12 * var9;
        return var13 > 0.0F;
    }

    public ajK m(ajK var1) {
        this.set(var1);
        return this;
    }

    public void a(Matrix3f var1) {
        this.m00 = var1.m00;
        this.m01 = var1.m01;
        this.m02 = var1.m02;
        this.m10 = var1.m10;
        this.m11 = var1.m11;
        this.m12 = var1.m12;
        this.m20 = var1.m20;
        this.m21 = var1.m21;
        this.m22 = var1.m22;
    }

    public ajK n(ajK var1) {
        this.invert(var1);
        return this;
    }

    public ajK ceQ() {
        this.invert();
        return this;
    }

    public ajK ceR() {
        ajK var1 = new ajK();
        var1.invert(this);
        return var1;
    }

    public void c(aJF var1, aJF var2) {
        float var3 = var1.x;
        float var4 = var1.y;
        float var5 = var1.z;
        float var6 = var1.w;
        var2.x = var3 * this.m00 + var4 * this.m01 + var5 * this.m02 + var6 * this.m03;
        var2.y = var3 * this.m10 + var4 * this.m11 + var5 * this.m12 + var6 * this.m13;
        var2.z = var3 * this.m20 + var4 * this.m21 + var5 * this.m22 + var6 * this.m23;
        var2.w = var3 * this.m30 + var4 * this.m31 + var5 * this.m32 + var6 * this.m33;
    }

    public void c(aJD_q var1, aJD_q var2) {
        double var3 = var1.x;
        double var5 = var1.y;
        double var7 = var1.z;
        double var9 = var1.w;
        var2.x = var3 * (double) this.m00 + var5 * (double) this.m01 + var7 * (double) this.m02 + var9 * (double) this.m03;
        var2.y = var3 * (double) this.m10 + var5 * (double) this.m11 + var7 * (double) this.m12 + var9 * (double) this.m13;
        var2.z = var3 * (double) this.m20 + var5 * (double) this.m21 + var7 * (double) this.m22 + var9 * (double) this.m23;
        var2.w = var3 * (double) this.m30 + var5 * (double) this.m31 + var7 * (double) this.m32 + var9 * (double) this.m33;
    }

    public void f(float var1, float var2, float var3, float var4, float var5, float var6) {
        this.m00 = 2.0F / (var1 - var2);
        this.m10 = 0.0F;
        this.m20 = 0.0F;
        this.m30 = 0.0F;
        this.m01 = 0.0F;
        this.m11 = 2.0F / (var4 - var3);
        this.m21 = 0.0F;
        this.m31 = 0.0F;
        this.m02 = 0.0F;
        this.m12 = 0.0F;
        this.m22 = -2.0F / (var6 - var5);
        this.m32 = 0.0F;
        this.m03 = -(var1 + var2) / (var1 - var2);
        this.m13 = -(var4 + var3) / (var4 - var3);
        this.m23 = -(var6 + var5) / (var6 - var5);
        this.m33 = 1.0F;
    }

    public void n(float var1, float var2, float var3, float var4) {
        float var8 = (float) ((double) var3 * aip.O((double) (var1 / 2.0F)));
        float var7 = -var8;
        float var5 = var7 * var2;
        float var6 = var8 * var2;
        this.a(2.0F * var3 / (var6 - var5), 0.0F, 0.0F, 0.0F, 0.0F, 2.0F * var3 / (var8 - var7), 0.0F, 0.0F, (var6 + var5) / (var6 - var5), (var8 + var7) / (var8 - var7), -((var4 + var3) / (var4 - var3)), -1.0F, 0.0F, 0.0F, -(2.0F * var4 * var3 / (var4 - var3)), 0.0F);
    }

    public void readExternal(Vm_q var1) {
        this.m00 = var1.gZ("m00");
        this.m01 = var1.gZ("m01");
        this.m02 = var1.gZ("m02");
        this.m03 = var1.gZ("m03");
        this.m10 = var1.gZ("m10");
        this.m11 = var1.gZ("m11");
        this.m12 = var1.gZ("m12");
        this.m13 = var1.gZ("m13");
        this.m20 = var1.gZ("m20");
        this.m21 = var1.gZ("m21");
        this.m22 = var1.gZ("m22");
        this.m23 = var1.gZ("m23");
        this.m30 = var1.gZ("m30");
        this.m31 = var1.gZ("m31");
        this.m32 = var1.gZ("m32");
        this.m33 = var1.gZ("m33");
    }

    public void writeExternal(att var1) {
        var1.a("m00", this.m00);
        var1.a("m01", this.m01);
        var1.a("m02", this.m02);
        var1.a("m03", this.m03);
        var1.a("m10", this.m10);
        var1.a("m11", this.m11);
        var1.a("m12", this.m12);
        var1.a("m13", this.m13);
        var1.a("m20", this.m20);
        var1.a("m21", this.m21);
        var1.a("m22", this.m22);
        var1.a("m23", this.m23);
        var1.a("m30", this.m30);
        var1.a("m31", this.m31);
        var1.a("m32", this.m32);
        var1.a("m33", this.m33);
    }

    public boolean anA() {
        float var1 = this.m00 + this.m01 + this.m02 + this.m03 + this.m10 + this.m11 + this.m12 + this.m13 + this.m20 + this.m21 + this.m22 + this.m23 + this.m30 + this.m31 + this.m32 + this.m33;
        return !Float.isNaN(var1) && !Float.isInfinite(var1);
    }
}
