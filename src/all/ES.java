package all;

import com.hoplon.geometry.Vec3f;

public class ES implements akr {
    private static final int gZr = 5;
    private static final int gZs = 0;
    private static final int gZt = 1;
    private static final int gZu = 2;
    private static final int gZv = 3;
    public final Vec3f[] gZw = new Vec3f[5];
    public final Vec3f[] gZx = new Vec3f[5];
    public final Vec3f[] gZy = new Vec3f[5];
    public final Vec3f gZz = new Vec3f();
    public final Vec3f gZA = new Vec3f();
    public final Vec3f gZB = new Vec3f();
    public final Vec3f gZC = new Vec3f();
    public final b gZE = new b();
    protected final QW gZq = Ks.D(b.class);
    public int numVertices;
    public boolean gZD;
    public boolean gZF;
    protected Kt stack = Kt.bcE();

    public ES() {
        for (int var1 = 0; var1 < 5; ++var1) {
            this.gZw[var1] = new Vec3f();
            this.gZx[var1] = new Vec3f();
            this.gZy[var1] = new Vec3f();
        }

    }

    public void wD(int var1) {
        assert this.numVertices > 0;

        --this.numVertices;
        this.gZw[var1].set(this.gZw[this.numVertices]);
        this.gZx[var1].set(this.gZx[this.numVertices]);
        this.gZy[var1].set(this.gZy[this.numVertices]);
    }

    public void a(a var1) {
        if (this.numVertices() >= 4 && !var1.cUo) {
            this.wD(3);
        }

        if (this.numVertices() >= 3 && !var1.cUn) {
            this.wD(2);
        }

        if (this.numVertices() >= 2 && !var1.cUm) {
            this.wD(1);
        }

        if (this.numVertices() >= 1 && !var1.cUl) {
            this.wD(0);
        }

    }

    public boolean cFU() {
        this.stack = this.stack.bcD();
        this.stack.bcH().push();

        boolean var15;
        try {
            if (this.gZF) {
                Vec3f var1 = (Vec3f) this.stack.bcH().get();
                Vec3f var2 = (Vec3f) this.stack.bcH().get();
                Vec3f var3 = (Vec3f) this.stack.bcH().get();
                Vec3f var4 = (Vec3f) this.stack.bcH().get();
                Vec3f var5 = (Vec3f) this.stack.bcH().get();
                this.gZE.reset();
                this.gZF = false;
                Vec3f var6;
                Vec3f var7;
                Vec3f var8;
                Vec3f var9;
                Vec3f var10;
                switch (this.numVertices()) {
                    case 0:
                        this.gZD = false;
                        break;
                    case 1:
                        this.gZz.set(this.gZx[0]);
                        this.gZA.set(this.gZy[0]);
                        this.gZB.sub(this.gZz, this.gZA);
                        this.gZE.reset();
                        this.gZE.m(1.0F, 0.0F, 0.0F, 0.0F);
                        this.gZD = this.gZE.isValid();
                        break;
                    case 2:
                        var6 = this.gZw[0];
                        var7 = this.gZw[1];
                        var8 = (Vec3f) this.stack.bcH().get();
                        var9 = this.stack.bcH().h(0.0F, 0.0F, 0.0F);
                        var10 = (Vec3f) this.stack.bcH().get();
                        var10.sub(var9, var6);
                        Vec3f var18 = (Vec3f) this.stack.bcH().get();
                        var18.sub(var7, var6);
                        float var12 = var18.dot(var10);
                        if (var12 > 0.0F) {
                            float var13 = var18.dot(var18);
                            if (var12 < var13) {
                                var12 /= var13;
                                var1.scale(var12, var18);
                                var10.sub(var1);
                                this.gZE.fcc.cUl = true;
                                this.gZE.fcc.cUm = true;
                            } else {
                                var12 = 1.0F;
                                var10.sub(var18);
                                this.gZE.fcc.cUm = true;
                            }
                        } else {
                            var12 = 0.0F;
                            this.gZE.fcc.cUl = true;
                        }

                        this.gZE.m(1.0F - var12, var12, 0.0F, 0.0F);
                        var1.scale(var12, var18);
                        var8.add(var6, var1);
                        var1.sub(this.gZx[1], this.gZx[0]);
                        var1.scale(var12);
                        this.gZz.add(this.gZx[0], var1);
                        var1.sub(this.gZy[1], this.gZy[0]);
                        var1.scale(var12);
                        this.gZA.add(this.gZy[0], var1);
                        this.gZB.sub(this.gZz, this.gZA);
                        this.a(this.gZE.fcc);
                        this.gZD = this.gZE.isValid();
                        break;
                    case 3:
                        var6 = this.stack.bcH().h(0.0F, 0.0F, 0.0F);
                        var7 = this.gZw[0];
                        var8 = this.gZw[1];
                        var9 = this.gZw[2];
                        this.a(var6, var7, var8, var9, this.gZE);
                        var2.scale(this.gZE.fcd[0], this.gZx[0]);
                        var3.scale(this.gZE.fcd[1], this.gZx[1]);
                        var4.scale(this.gZE.fcd[2], this.gZx[2]);
                        JL.f(this.gZz, var2, var3, var4);
                        var2.scale(this.gZE.fcd[0], this.gZy[0]);
                        var3.scale(this.gZE.fcd[1], this.gZy[1]);
                        var4.scale(this.gZE.fcd[2], this.gZy[2]);
                        JL.f(this.gZA, var2, var3, var4);
                        this.gZB.sub(this.gZz, this.gZA);
                        this.a(this.gZE.fcc);
                        this.gZD = this.gZE.isValid();
                        break;
                    case 4:
                        var6 = this.stack.bcH().h(0.0F, 0.0F, 0.0F);
                        var7 = this.gZw[0];
                        var8 = this.gZw[1];
                        var9 = this.gZw[2];
                        var10 = this.gZw[3];
                        boolean var11 = this.a(var6, var7, var8, var9, var10, this.gZE);
                        if (var11) {
                            var2.scale(this.gZE.fcd[0], this.gZx[0]);
                            var3.scale(this.gZE.fcd[1], this.gZx[1]);
                            var4.scale(this.gZE.fcd[2], this.gZx[2]);
                            var5.scale(this.gZE.fcd[3], this.gZx[3]);
                            JL.a(this.gZz, var2, var3, var4, var5);
                            var2.scale(this.gZE.fcd[0], this.gZy[0]);
                            var3.scale(this.gZE.fcd[1], this.gZy[1]);
                            var4.scale(this.gZE.fcd[2], this.gZy[2]);
                            var5.scale(this.gZE.fcd[3], this.gZy[3]);
                            JL.a(this.gZA, var2, var3, var4, var5);
                            this.gZB.sub(this.gZz, this.gZA);
                            this.a(this.gZE.fcc);
                            this.gZD = this.gZE.isValid();
                        } else if (this.gZE.fce) {
                            this.gZD = false;
                        } else {
                            this.gZD = true;
                            this.gZB.set(0.0F, 0.0F, 0.0F);
                        }
                        break;
                    default:
                        this.gZD = false;
                }
            }

            var15 = this.gZD;
        } finally {
            this.stack.bcH().pop();
        }

        return var15;
    }

    public boolean a(Vec3f var1, Vec3f var2, Vec3f var3, Vec3f var4, b var5) {
        this.stack.bcH().push();

        try {
            var5.fcc.reset();
            Vec3f var6 = (Vec3f) this.stack.bcH().get();
            var6.sub(var3, var2);
            Vec3f var7 = (Vec3f) this.stack.bcH().get();
            var7.sub(var4, var2);
            Vec3f var8 = (Vec3f) this.stack.bcH().get();
            var8.sub(var1, var2);
            float var9 = var6.dot(var8);
            float var10 = var7.dot(var8);
            if (var9 <= 0.0F && var10 <= 0.0F) {
                var5.fcb.set(var2);
                var5.fcc.cUl = true;
                var5.m(1.0F, 0.0F, 0.0F, 0.0F);
            } else {
                Vec3f var11 = (Vec3f) this.stack.bcH().get();
                var11.sub(var1, var3);
                float var12 = var6.dot(var11);
                float var13 = var7.dot(var11);
                if (var12 >= 0.0F && var13 <= var12) {
                    var5.fcb.set(var3);
                    var5.fcc.cUm = true;
                    var5.m(0.0F, 1.0F, 0.0F, 0.0F);
                } else {
                    float var14 = var9 * var13 - var12 * var10;
                    if (var14 <= 0.0F && var9 >= 0.0F && var12 <= 0.0F) {
                        float var28 = var9 / (var9 - var12);
                        var5.fcb.scaleAdd(var28, var6, var2);
                        var5.fcc.cUl = true;
                        var5.fcc.cUm = true;
                        var5.m(1.0F - var28, var28, 0.0F, 0.0F);
                    } else {
                        Vec3f var15 = (Vec3f) this.stack.bcH().get();
                        var15.sub(var1, var4);
                        float var16 = var6.dot(var15);
                        float var17 = var7.dot(var15);
                        if (var17 >= 0.0F && var16 <= var17) {
                            var5.fcb.set(var4);
                            var5.fcc.cUn = true;
                            var5.m(0.0F, 0.0F, 1.0F, 0.0F);
                        } else {
                            float var18 = var16 * var10 - var9 * var17;
                            float var19;
                            if (var18 <= 0.0F && var10 >= 0.0F && var17 <= 0.0F) {
                                var19 = var10 / (var10 - var17);
                                var5.fcb.scaleAdd(var19, var7, var2);
                                var5.fcc.cUl = true;
                                var5.fcc.cUn = true;
                                var5.m(1.0F - var19, 0.0F, var19, 0.0F);
                            } else {
                                var19 = var12 * var17 - var16 * var13;
                                float var20;
                                if (var19 <= 0.0F && var13 - var12 >= 0.0F && var16 - var17 >= 0.0F) {
                                    var20 = (var13 - var12) / (var13 - var12 + (var16 - var17));
                                    Vec3f var29 = (Vec3f) this.stack.bcH().get();
                                    var29.sub(var4, var3);
                                    var5.fcb.scaleAdd(var20, var29, var3);
                                    var5.fcc.cUm = true;
                                    var5.fcc.cUn = true;
                                    var5.m(0.0F, 1.0F - var20, var20, 0.0F);
                                } else {
                                    var20 = 1.0F / (var19 + var18 + var14);
                                    float var21 = var18 * var20;
                                    float var22 = var14 * var20;
                                    Vec3f var23 = (Vec3f) this.stack.bcH().get();
                                    Vec3f var24 = (Vec3f) this.stack.bcH().get();
                                    var23.scale(var21, var6);
                                    var24.scale(var22, var7);
                                    JL.f(var5.fcb, var2, var23, var24);
                                    var5.fcc.cUl = true;
                                    var5.fcc.cUm = true;
                                    var5.fcc.cUn = true;
                                    var5.m(1.0F - var21 - var22, var21, var22, 0.0F);
                                }
                            }
                        }
                    }
                }
            }
        } finally {
            this.stack.bcH().pop();
        }

        return true;
    }

    public int b(Vec3f var1, Vec3f var2, Vec3f var3, Vec3f var4, Vec3f var5) {
        this.stack.bcH().push();

        try {
            Vec3f var6 = (Vec3f) this.stack.bcH().get();
            Vec3f var7 = (Vec3f) this.stack.bcH().get();
            var7.sub(var3, var2);
            var6.sub(var4, var2);
            var7.cross(var7, var6);
            var6.sub(var1, var2);
            float var8 = var6.dot(var7);
            var6.sub(var5, var2);
            float var9 = var6.dot(var7);
            if (var9 * var9 >= 9.999999E-9F) {
                int var11 = var8 * var9 < 0.0F ? 1 : 0;
                return var11;
            }
        } finally {
            this.stack.bcH().pop();
        }

        return -1;
    }

    public boolean a(Vec3f var1, Vec3f var2, Vec3f var3, Vec3f var4, Vec3f var5, b var6) {
        this.stack.bcH().push();
        b var7 = (b) this.gZq.get();
        var7.reset();

        try {
            Vec3f var8 = (Vec3f) this.stack.bcH().get();
            Vec3f var10000 = (Vec3f) this.stack.bcH().get();
            var10000 = (Vec3f) this.stack.bcH().get();
            var6.fcb.set(var1);
            var6.fcc.reset();
            var6.fcc.cUl = true;
            var6.fcc.cUm = true;
            var6.fcc.cUn = true;
            var6.fcc.cUo = true;
            int var9 = this.b(var1, var2, var3, var4, var5);
            int var10 = this.b(var1, var2, var4, var5, var3);
            int var11 = this.b(var1, var2, var5, var3, var4);
            int var12 = this.b(var1, var3, var5, var4, var2);
            if (var9 >= 0 && var10 >= 0 && var11 >= 0 && var12 >= 0) {
                if (var9 != 0 || var10 != 0 || var11 != 0 || var12 != 0) {
                    float var13 = Float.MAX_VALUE;
                    Vec3f var14;
                    float var15;
                    if (var9 != 0) {
                        this.a(var1, var2, var3, var4, var7);
                        var14 = this.stack.bcH().ac(var7.fcb);
                        var8.sub(var14, var1);
                        var15 = var8.dot(var8);
                        if (var15 < var13) {
                            var13 = var15;
                            var6.fcb.set(var14);
                            var6.fcc.reset();
                            var6.fcc.cUl = var7.fcc.cUl;
                            var6.fcc.cUm = var7.fcc.cUm;
                            var6.fcc.cUn = var7.fcc.cUn;
                            var6.m(var7.fcd[0], var7.fcd[1], var7.fcd[2], 0.0F);
                        }
                    }

                    if (var10 != 0) {
                        this.a(var1, var2, var4, var5, var7);
                        var14 = this.stack.bcH().ac(var7.fcb);
                        var8.sub(var14, var1);
                        var15 = var8.dot(var8);
                        if (var15 < var13) {
                            var13 = var15;
                            var6.fcb.set(var14);
                            var6.fcc.reset();
                            var6.fcc.cUl = var7.fcc.cUl;
                            var6.fcc.cUn = var7.fcc.cUm;
                            var6.fcc.cUo = var7.fcc.cUn;
                            var6.m(var7.fcd[0], 0.0F, var7.fcd[1], var7.fcd[2]);
                        }
                    }

                    if (var11 != 0) {
                        this.a(var1, var2, var5, var3, var7);
                        var14 = this.stack.bcH().ac(var7.fcb);
                        var8.sub(var14, var1);
                        var15 = var8.dot(var8);
                        if (var15 < var13) {
                            var13 = var15;
                            var6.fcb.set(var14);
                            var6.fcc.reset();
                            var6.fcc.cUl = var7.fcc.cUl;
                            var6.fcc.cUm = var7.fcc.cUn;
                            var6.fcc.cUo = var7.fcc.cUm;
                            var6.m(var7.fcd[0], var7.fcd[2], 0.0F, var7.fcd[1]);
                        }
                    }

                    if (var12 != 0) {
                        this.a(var1, var3, var5, var4, var7);
                        var14 = this.stack.bcH().ac(var7.fcb);
                        var8.sub(var14, var1);
                        var15 = var8.dot(var8);
                        if (var15 < var13) {
                            var6.fcb.set(var14);
                            var6.fcc.reset();
                            var6.fcc.cUm = var7.fcc.cUl;
                            var6.fcc.cUn = var7.fcc.cUn;
                            var6.fcc.cUo = var7.fcc.cUm;
                            var6.m(0.0F, var7.fcd[0], var7.fcd[2], var7.fcd[1]);
                        }
                    }

                    if (var6.fcc.cUl && var6.fcc.cUm && var6.fcc.cUn && var6.fcc.cUo) {
                        ;
                    }

                    return true;
                }
            } else {
                var6.fce = true;
            }
        } finally {
            this.stack.bcH().pop();
            this.gZq.release(var7);
        }

        return false;
    }

    public void reset() {
        this.gZD = false;
        this.numVertices = 0;
        this.gZF = true;
        this.gZC.set(1.0E30F, 1.0E30F, 1.0E30F);
        this.gZE.reset();
    }

    public void p(Vec3f var1, Vec3f var2, Vec3f var3) {
        this.gZC.set(var1);
        this.gZF = true;
        this.gZw[this.numVertices].set(var1);
        this.gZx[this.numVertices].set(var2);
        this.gZy[this.numVertices].set(var3);
        ++this.numVertices;
    }

    public boolean aK(Vec3f var1) {
        boolean var2 = this.cFU();
        var1.set(this.gZB);
        return var2;
    }

    public float cgi() {
        int var2 = this.numVertices();
        float var3 = 0.0F;

        for (int var1 = 0; var1 < var2; ++var1) {
            float var4 = this.gZw[var1].lengthSquared();
            if (var3 < var4) {
                var3 = var4;
            }
        }

        return var3;
    }

    public boolean cgj() {
        return this.numVertices == 4;
    }

    public int a(Vec3f[] var1, Vec3f[] var2, Vec3f[] var3) {
        for (int var4 = 0; var4 < this.numVertices(); ++var4) {
            var3[var4].set(this.gZw[var4]);
            var1[var4].set(this.gZx[var4]);
            var2[var4].set(this.gZy[var4]);
        }

        return this.numVertices();
    }

    public boolean aL(Vec3f var1) {
        boolean var2 = false;
        int var4 = this.numVertices();

        for (int var3 = 0; var3 < var4; ++var3) {
            if (this.gZw[var3].equals(var1)) {
                var2 = true;
            }
        }

        if (var1.equals(this.gZC)) {
            return true;
        } else {
            return var2;
        }
    }

    public void aM(Vec3f var1) {
        var1.set(this.gZB);
    }

    public boolean cgk() {
        return this.numVertices() == 0;
    }

    public void D(Vec3f var1, Vec3f var2) {
        this.cFU();
        var1.set(this.gZz);
        var2.set(this.gZA);
    }

    public int numVertices() {
        return this.numVertices;
    }

    public static class b {
        public final Vec3f fcb = new Vec3f();
        public final a fcc = new a();
        public final float[] fcd = new float[4];
        public boolean fce;

        public void reset() {
            this.fce = false;
            this.m(0.0F, 0.0F, 0.0F, 0.0F);
            this.fcc.reset();
        }

        public boolean isValid() {
            boolean var1 = this.fcd[0] >= 0.0F && this.fcd[1] >= 0.0F && this.fcd[2] >= 0.0F && this.fcd[3] >= 0.0F;
            return var1;
        }

        public void m(float var1, float var2, float var3, float var4) {
            this.fcd[0] = var1;
            this.fcd[1] = var2;
            this.fcd[2] = var3;
            this.fcd[3] = var4;
        }
    }

    public static class a {
        public boolean cUl;
        public boolean cUm;
        public boolean cUn;
        public boolean cUo;

        public void reset() {
            this.cUl = false;
            this.cUm = false;
            this.cUn = false;
            this.cUo = false;
        }
    }
}
