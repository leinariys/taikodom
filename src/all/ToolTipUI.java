package all;

import sun.swing.SwingUtilities2;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicToolTipUI;
import javax.swing.text.View;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * ToolTipUI
 */
public class ToolTipUI extends BasicToolTipUI implements aIh {
    private static final Color cds = new Color(0, 0, 0, 0);
    private final JToolTip iNi;
    private ComponentManager Rp;
    private JComponent iNj;
    private Timer iNk;

    public ToolTipUI(JToolTip var1) {
        this.iNi = var1;
        this.iNi.setOpaque(false);
        this.iNi.setBorder(new BorderWrapper());
        this.Rp = new ComponentManager("tooltip", this.iNi);
        this.iNk = new Timer(500, new ActionListener() {
            public void actionPerformed(ActionEvent var1) {
                if (!ToolTipUI.this.iNi.isVisible()) {
                    ToolTipUI.this.iNk.stop();
                    ToolTipUI.this.iNj = null;
                } else if (ToolTipUI.this.iNj == null || !ToolTipUI.this.iNj.isShowing()) {
                    ToolTipUI.this.iNk.stop();
                    ToolTipUI.this.iNi.setVisible(false);
                    ToolTipUI.this.iNj = null;
                }

            }
        });
        PropertyChangeListener var2 = new PropertyChangeListener() {
            public void propertyChange(PropertyChangeEvent var1) {
                if (var1.getNewValue() instanceof JComponent) {
                    JComponent var2 = (JComponent) var1.getNewValue();
                    wz var3 = wz.d(var2);
                    if (var3 != null) {
                        Component var4 = var3.b(var2);
                        if (var4 != null) {
                            ToolTipUI.this.iNi.setLayout(new BorderLayout());
                            ToolTipUI.this.iNi.add(var4);
                            ToolTipUI.this.iNj = var2;
                            ToolTipUI.this.iNk.restart();
                            String var5 = ComponentManager.getCssHolder(var4).getAttribute("tooltipClass");
                            if (var5 != null) {
                                ComponentManager.getCssHolder(ToolTipUI.this.iNi).setAttribute("class", var5);
                            }

                            Container var6 = ToolTipUI.this.iNi.getParent();
                            if (var6 != null) {
                                ComponentManager.getCssHolder(var6).setAttribute("class", "tooltipParent");
                            }
                        } else {
                            ToolTipUI.this.iNi.setVisible(false);
                        }
                    }

                }
            }
        };
        this.iNi.addPropertyChangeListener("component", var2);
    }

    public static ComponentUI createUI(JComponent var0) {
        return new ToolTipUI((JToolTip) var0);
    }

    public ComponentManager wy() {
        return this.Rp;
    }

    public void paint(Graphics var1, JComponent var2) {
        Object var3 = var2.getBorder();

        if (!(var3 instanceof BorderWrapper)) {
            var3 = new BorderWrapper();
            var2.setBorder((Border) var3);
        }

        PropertiesUiFromCss var4 = this.Rp.Vp();
        RM.a((Graphics) var1, (Component) var2, (PropertiesUiFromCss) var4);
        var1.setFont(var4.getFont());
        if (var2.getComponentCount() <= 0) {
            FontMetrics var5 = SwingUtilities2.getFontMetrics(var2, var1, var4.getFont());
            var1.setColor(cds);
            var1.setColor(var4.getColor());
            String var6 = ((JToolTip) var2).getTipText();
            if (var6 == null) {
                var6 = "";
            }

            Insets var7 = ((Border) var3).getBorderInsets(var2);
            Dimension var8 = var2.getSize();
            Rectangle var9 = new Rectangle(var7.left, var7.top, var8.width - (var7.left + var7.right), var8.height - (var7.top + var7.bottom));
            View var10 = (View) var2.getClientProperty("html");
            if (var10 != null) {
                var1.setFont(var4.getFont());
                var1.setColor(var4.getColor());
                var10.paint(var1, var9);
            } else {
                var1.setFont(var4.getFont());
                var1.setColor(var4.getShadowColor());
                SwingUtilities2.drawString(var2, var1, var6, (int) ((float) var9.x + var4.atO()), (int) ((float) var9.y + var4.atP()) + var5.getAscent());
                var1.setColor(var4.getColor());
                SwingUtilities2.drawString(var2, var1, var6, var9.x, var9.y + var5.getAscent());
            }

        }
    }

    public Dimension getPreferredSize(JComponent var1) {
        if (var1.getComponentCount() > 0) {
            return null;
        } else {
            PropertiesUiFromCss var2 = this.Rp.Vp();
            Font var3 = var2.getFont();
            FontMetrics var4 = var1.getFontMetrics(var3);
            Object var5 = var1.getBorder();

            if (!(var5 instanceof BorderWrapper)) {
                var5 = new BorderWrapper();
                var1.setBorder((Border) var5);
            }

            Insets var6 = ((Border) var5).getBorderInsets(var1);
            Dimension var7 = new Dimension(var6.left + var6.right, var6.top + var6.bottom);
            String var8 = ((JToolTip) var1).getTipText();
            if (var8 != null && !var8.equals("")) {
                View var9 = (View) var1.getClientProperty("html");
                if (var9 != null) {
                    var7.width = (int) ((float) var7.width + (float) ((int) var9.getPreferredSpan(0)) + var2.atO());
                    var7.height = (int) ((float) var7.height + (float) ((int) var9.getPreferredSpan(1)) + var2.atP());
                } else {
                    var7.width = (int) ((float) var7.width + (float) SwingUtilities2.stringWidth(var1, var4, var8) + var2.atO());
                    var7.height = (int) ((float) var7.height + (float) var4.getHeight() + var2.atP());
                }
            } else {
                var8 = "";
            }

            return var7;
        }
    }

    /**
     * TODO похлже это public pv wy()
     *
     * @return Графический элемент
     */
    @Override
    public aeK wz() {
        return this.Rp;
    }
}
