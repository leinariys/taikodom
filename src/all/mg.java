package all;

/**
 * KeyboardEvent
 * class CommandTranslatorAddon
 */
public final class mg extends aOG {
    public static adl aBH = new adl("KeyboardEvent");
    private int aBD;
    private boolean aBE;
    private int aBF;
    private int modifiers;
    private boolean aBG;

    public mg(int var1, boolean var2, int var3, int var4, boolean var5) {
        this.aBD = var1;
        this.aBE = var2;
        this.aBF = var3;
        this.modifiers = var4;
        this.aBG = var5;
    }

    public static adl Ni() {
        return aBH;
    }

    public final int Nh() {
        return this.aBD;
    }

    public final boolean getState() {
        return this.aBE;
    }

    public final int getUnicode() {
        return this.aBF;
    }

    public adl Fp() {
        return aBH;
    }

    public int getModifiers() {
        return this.modifiers;
    }

    public boolean isPressed() {
        return this.aBE;
    }

    public boolean isTyped() {
        return this.aBG;
    }
}
