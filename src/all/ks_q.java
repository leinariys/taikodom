package all;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public abstract class ks_q {
    public abstract int Ka();

    public abstract int Kb();

    public abstract int Kc();

    public abstract int aT(float var1);

    public abstract float cF(int var1);

    public abstract void a(ObjectOutput var1, float var2) throws IOException;

    public abstract float d(ObjectInput var1) throws IOException;

    public abstract int Kd();

    public abstract int Ke();

    public abstract float Kf();

    public abstract float Kg();

    public abstract float Kh();

    public abstract boolean Ki();

    public String getName() {
        return this.getClass().getSimpleName();
    }

    public String toString() {
        StringBuilder var1 = new StringBuilder();
        var1.append(this.getName()).append(": ");
        if (this.Ki()) {
            var1.append("1+");
        }

        var1.append(this.Ke()).append('+');
        var1.append(this.Kd());
        var1.append('=').append(this.Kc());
        var1.append(" Exp:[").append(this.Kb()).append(';').append(this.Ka()).append("] ");
        int var2 = this.Kc();
        if (this.Ki()) {
            var1.append('S');
            --var2;
            if (var2 % 4 == 0) {
                var1.append(' ');
            }
        }

        int var3;
        for (var3 = 0; var3 < this.Ke(); ++var3) {
            var1.append('E');
            --var2;
            if (var2 % 4 == 0) {
                var1.append(' ');
            }
        }

        for (var3 = 0; var3 < this.Kd(); ++var3) {
            var1.append('M');
            --var2;
            if (var2 % 4 == 0 && var2 > 0) {
                var1.append(' ');
            }
        }

        return var1.toString();
    }

    public abstract boolean Kj();
}
