// parseStyleSheet.java
//Тест засыпания главного потока
public class test_thread_main_sleep {
    public static void main(String[] paramArrayOfString) throws InterruptedException {
        long l = System.nanoTime();
        for (int i = 1; i < 2000; i++) {
            Thread.sleep(1000L);
            System.out.printf("Elapsed: %f endPage seconds %setBoundsss CreateJComponent ticks\n", new Object[]{
                    Double.valueOf((System.nanoTime() - l) * 1.0E-9D), Integer.valueOf(i)});
        }
    }
}
