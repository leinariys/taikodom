import all.*;
import taikodom.addon.login.LoginAddon;
import taikodom.render.JGLDesktop;

import javax.media.opengl.GLCapabilities;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.net.URL;
import java.util.Iterator;

public class test_MyGLCanvas {
    public static void main(String[] var0) {
        JFrame frameWin = new JFrame("Taikodom");//Заголовок окна
        frameWin.setResizable(false);//Устанавливает, изменяется ли этот кадр пользователем.
        frameWin.setSize(700, 700);//Задать размер окна
        frameWin.setVisible(true);

        GLCapabilities setingsGL = new GLCapabilities();//Определяет набор возможностей OpenGL.
        setingsGL.setDoubleBuffered(true);//Включает или отключает двойную буферизацию.
        setingsGL.setHardwareAccelerated(true);//Включает или отключает аппаратное ускорение.
        setingsGL.setSampleBuffers(true);//Указывает, должны ли быть выделены буферы выборки для полного сглаживания сцены (FSAA) для этой возможности.
        setingsGL.setNumSamples(0);//Если буферы выборки включены, указывается количество выделенных буферов.
        setingsGL.setDepthBits(24);//Устанавливает количество бит, запрошенных для буфера глубины.
        setingsGL.setStencilBits(8);//Устанавливает количество бит, запрошенных для буфера трафарета.
        setingsGL.setRedBits(8);//Устанавливает количество бит, запрошенных для красного компонента цветового буфера.
        setingsGL.setBlueBits(8);//Устанавливает количество бит, запрошенных для синего компонента цветового буфера.
        setingsGL.setGreenBits(8);//Устанавливает количество бит, запрошенных для зеленого компонента цветового буфера.
        setingsGL.setAlphaBits(8);//Устанавливает количество бит, запрошенных для альфа-компонента цветового буфера.

        JGLDesktop polotnoJGL = new JGLDesktop(setingsGL, null, null, GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice());//создаём объект openGL
        polotnoJGL.setSize(700, 700);//Задаём размер JGLDesktop
        frameWin.setContentPane(polotnoJGL.getDesktopPane());//Добавляем  полотно в окно


        EngineGame engineGame = new EngineGame(new FileControl("."), new FileControl("."));
        URL.setURLStreamHandlerFactory(hP.ze());//Задает URL-адрес приложения для Java Virtual Machine.
        hP.ze().a("res", new StreamRootPathRender(new FileControl(".")));

        engineGame.setEventManager(new EventManager() {
            @Override
            public void d(final String var1, final Object var2) {
                super.d(var1, var2);
            }
        });
        engineGame.setConfigManager(ConfigManager.initConfigManager());

        final oE var2wef = new oE();

        engineGame.a(var2wef);

        BaseUItegXML var1dd = new BaseUItegXML();

        RP dwerwkl = new RP();

        // метод из класса перепределён? class LoaderCss
        var1dd.setLoaderCss(new LoaderCss() {
            public avI loadCSS(URL var1) throws IOException {
                if ("res".equals(var1.getProtocol())) {
                    String var2x = var1.toString();
                    //  var2x.replaceAll("^res://", "") =  data/styles/core.css
                    var1 = new FileControl(".").concat(var2x.replaceAll("^res://", "")).getFile().toURI().toURL();
                    //var1 = file:/C:/Projects/Java/TaikodomGameLIVE/./data/styles/core.css
                }

                final CssNode var3 = new CssNode();
                var3.loadFileCSS(var1.openStream());
                if ("file".equals(var1.getProtocol())) {
                    var2wef.a(var1, new ahq_q() {
                        @Override
                        public void a(URL var1) {
                            try {
                                dwerwkl.cYc().brL();

                                try {
                                    var3.loadFileCSS(var1.openStream());
                                    Iterator var3x = ComponentManager.Vn().iterator();

                                    while (var3x.hasNext()) {
                                        aeK var2x = (aeK) var3x.next();
                                        var2x.clear();
                                        var2x.getCurrentComponent().invalidate();
                                        var2x.getCurrentComponent().repaint();
                                    }
                                } finally {
                                    dwerwkl.cYc().brK();
                                }
                            } catch (IOException var9) {
                                var9.printStackTrace();
                            } /*catch (InterruptedException var10) {
                           return;
                        }*/

                        }
                    });
                }

                return var3;
            }
        });


        avI var3ff = var1dd.loadCSS((Object) null, (String) "res://data/styles/core.css");
        var1dd.setCssNodeCore(var3ff);

        var1dd.setRootPane(frameWin.getRootPane());


        AddonManager addonManager = new AddonManager(new aUM(engineGame));
        addonManager.b(var1dd);//Базовые элементы интерфейса
        engineGame.setAddonManager(addonManager);
        //fghh.


        LoginAddon sda = new LoginAddon();
        GK var4 = new GK(engineGame, addonManager, null, sda);//логирование и локализация
        ((xa) var4.aVU()).setxa(addonManager.aVU());
        ((pp) var4).setAVU(new Ih(addonManager.aVU()));

        JButton sdds = new JButton();


        SplitCustomWrapper dsfs = new SplitCustomWrapper();

        polotnoJGL.getDesktopPane().add(sdds);
        dsfs.setBounds(10, 10, 50, 50);
        //  sda.add(var4);//Передаём в аддон логирование и локализацию

    }
}
