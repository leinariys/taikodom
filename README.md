## Полезные ссылки
* https://cloud.mail.ru/public/JENg/8s3anH5NK
* https://cloud.mail.ru/public/9osq/7zpodPSJs
* https://cloud.mail.ru/public/HQVq/YgztUSsqF

---
## Адреса и порты
Домен www.taikodom.com.br

**Запросы при старте клиента**

* GET [Full request URI: http://www.taikodom.com.br/server_status_en.txt]
* GET [Full request URI: http://www.taikodom.com.br/release_notes_en.txt]

**Регистация:**

POST [Full request URI: http://www.taikodom.com.br/users/game_create_user]
- locale
- external_agent
- name
- email
- password
- birthdate
- gender
- newsletter

Ответ при ошибке:
Server is offline. Please try again later.

**Лицензия EULA**
* GET [Full request URI: http://www.taikodom.com.br/end_user_license_in_game?locale=en]

**Условия использования в игре**
* GET [Full request URI: http://www.taikodom.com.br/terms_of_use_in_game?locale=en]

**Кредиты в игре**
* GET [Full request URI: http://www.taikodom.com.br/credits_in_game/]

**Авторизация (ввод пороля)**

* Запрос на live.server2.taikodom.com:15225

Server down for maintenance. We'll soon be back. Thanks for your comprehension.

**Классы вызовов**

* \a\ac.java http://taikodom.com.br/game_billing
* \a\lu.java http://taikodom.com.br/game_billing        
* \a\aDk.java http://www.taikodom.com.br/end_user_license_in_game_v  http://www.taikodom.com.br/terms_of_use_in_game_v                
* \a\akH.java http://www.taikodom.com.br/   http://www.taikodom.com.br/ http://www.taikodom.com.br/        
* \taikodom\addon\credits\CreditsAddon.java http://www.taikodom.com.br/credits_in_game/
* \taikodom\addon\help\TmpHelpAddon.java    http://www.taikodom.com.br/help_in_game?locale=" + I18NString.cGs();
* \taikodom\addon\login\LoginAddon.java$n   http://www.taikodom.com.br/" + paramString, "locale=" +







